IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CheckUserAvailibility]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_CheckUserAvailibility
END
GO
CREATE PROC USP_CheckUserAvailibility
@UserName nvarchar(MAX)
AS
/*
USP_CheckUserAvailibility '10001'
*/
SET NOCOUNT ON;
BEGIN
	SELECT COUNT(*) AS COUNTS FROM Webx_Master_Users WITH(NOLOCK) WHERE UserId = @UserName
END