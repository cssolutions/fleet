/*

BEGIN TRAN
--ROLLBACK
EXEC usp_webx_STATE_Insert_NewPortal	'<State><Code>0</Code><Name>Arkesh</Name><FreightRate>45.00</FreightRate><FreightRateType>F</FreightRateType><Category>A</Category><ServiceTaxFlag>Y</ServiceTaxFlag><ActiveFlag>Y</ActiveFlag><entryby>10001</entryby></State>'
										,'<ArrayOfStateDocument><StateDocument><Require></Require><Srno>0</Srno><Form>A</Form><Permit></Permit><Strem></Strem><InOutBound>O</InOutBound></StateDocument><StateDocument><Require></Require><Srno>0</Srno><Form>A</Form><Permit></Permit><Strem></Strem><InOutBound>I</InOutBound></StateDocument></ArrayOfStateDocument>'

EXEC usp_webx_STATE_Insert_NewPortal	'<State><Code>62</Code><Name>Arkesh</Name><FreightRate>45.00</FreightRate><FreightRateType>F</FreightRateType><Category>A</Category><ServiceTaxFlag>Y</ServiceTaxFlag><ActiveFlag>Y</ActiveFlag><entryby>10001</entryby></State>'
										,'<ArrayOfStateDocument><StateDocument><Require></Require><Srno>238</Srno><Form>A</Form><Permit></Permit><Strem></Strem><InOutBound>I</InOutBound></StateDocument><StateDocument><Require></Require><Srno>239</Srno><Form>A</Form><Permit></Permit><Strem></Strem><InOutBound>O</InOutBound></StateDocument></ArrayOfStateDocument>'

*/

ALTER PROCEDURE usp_webx_STATE_Insert_NewPortal
                         
@strXMLStateHdr xml,                        
@strXMLStateDocument xml                        
AS
BEGIN
                                             
DECLARE @hDoc1 int;
DECLARE @stcd varchar(max),@Code varchar(max);
EXEC sp_xml_preparedocument	@hDoc1 OUTPUT
							,@strXMLStateHdr

SELECT
	@stcd = COUNT(*) + 1
FROM Webx_State

SELECT
	@Code = Code
FROM OPENXML(@hDoc1, 'State', 2)
WITH
(Code VARCHAR(50) 'Code')

----------------------------Record Update--------------------------------------------
IF (@Code > 0) BEGIN
UPDATE webx_state
SET	stnm = T.stnm
	,frt_rate = T.frt_rate
	,rate_type = T.rate_type
	,entryby = T.entryby
	,entrydt = T.entrydt
	,lasteditby = T.lasteditby
	,lasteditdate = T.lasteditdate
	,activeflag = T.activeflag
	,stax_exmpt_yn = T.stax_exmpt_yn
	,Category = T.Category
FROM (SELECT
		stnm
		,frt_rate
		,rate_type
		,entryby
		,entrydt
		,lasteditby
		,lasteditdate
		,activeflag
		,stax_exmpt_yn
		,Category
	FROM OPENXML(@hDoc1, 'State', 2)
	WITH
	(stnm VARCHAR(50) 'Name',
	frt_rate NUMERIC 'FreightRate', rate_type VARCHAR(20) 'FreightRateType', entryby VARCHAR(50) 'entryby',
	entrydt DATETIME 'entrydt', lasteditby VARCHAR(50) 'UpdateBy', lasteditdate DATETIME 'UpdateDate',
	activeflag VARCHAR(1) 'ActiveFlag', stax_exmpt_yn VARCHAR(1) 'ServiceTaxFlag', Category VARCHAR(5) 'Category')) T
WHERE srno = @Code
DECLARE @hDoc2 INT;
EXEC sp_xml_preparedocument	@hDoc2 OUTPUT
							,@strXMLStateDocument

SELECT
	@stcd = STCD
FROM webx_state
WHERE srno = @Code

IF CAST(@strXMLStateDocument AS VARCHAR(MAX)) <> '<ArrayOfStateDocument></ArrayOfStateDocument>' BEGIN
UPDATE webx_state_document
SET	STCSTREQ = T.STCSTREQ
	,STFORM = T.STFORM
	,STPERMIT = T.STPERMIT
	,STREM = T.STREM
	,inoutbound = T.inoutbound
FROM (SELECT
		Srno
		,STCSTREQ
		,STFORM
		,STPERMIT
		,STREM
		,inoutbound
	FROM OPENXML(@hDoc2, 'ArrayOfStateDocument/StateDocument', 2)
	WITH
	(Srno VARCHAR(MAX) 'Srno',STCSTREQ VARCHAR(1) 'Require', STFORM VARCHAR(30) 'Form',
	STPERMIT VARCHAR(1) 'Permit', STREM VARCHAR(150) 'Strem', inoutbound VARCHAR(2) 'InOutBound')) T
WHERE STCD = @stcd AND webx_state_document.srno=T.Srno

END
SELECT
	ISNULL(@stcd, '') AS 'MSTID'
	,'Done' AS TranXaction
END
----------------------------Record Save--------------------------------------------
IF (@Code = 0) BEGIN
INSERT INTO webx_state (stcd, stnm, frt_rate, rate_type, entryby, entrydt, lasteditby, lasteditdate, activeflag, stax_exmpt_yn, Category)
	SELECT
		@stcd
		,stnm
		,frt_rate
		,rate_type
		,entryby
		,entrydt
		,lasteditby
		,lasteditdate
		,activeflag
		,stax_exmpt_yn
		,Category
	FROM OPENXML(@hDoc1, 'State', 2)
	WITH
	(stnm VARCHAR(50) 'Name',
	frt_rate NUMERIC 'FreightRate', rate_type VARCHAR(20) 'FreightRateType', entryby VARCHAR(50) 'entryby',
	entrydt DATETIME 'entrydt', lasteditby VARCHAR(50) 'UpdateBy', lasteditdate DATETIME 'UpdateDate',
	activeflag VARCHAR(1) 'ActiveFlag', stax_exmpt_yn VARCHAR(1) 'ServiceTaxFlag', Category VARCHAR(5) 'Category')

--DECLARE @hDoc2 INT;
EXEC sp_xml_preparedocument	@hDoc2 OUTPUT
							,@strXMLStateDocument

DELETE FROM webx_state_document
WHERE STCD = @stcd

IF CAST(@strXMLStateDocument AS VARCHAR(MAX)) <> '<ArrayOfStateDocument></ArrayOfStateDocument>' BEGIN
INSERT INTO webx_state_document (STCD, STCSTREQ, STFORM, STPERMIT, STREM, inoutbound)
	SELECT
		@stcd
		,STCSTREQ
		,STFORM
		,STPERMIT
		,STREM
		,inoutbound
	FROM OPENXML(@hDoc2, 'ArrayOfStateDocument/StateDocument', 2)
	WITH
	(STCSTREQ VARCHAR(1) 'Require', STFORM VARCHAR(30) 'Form',
	STPERMIT VARCHAR(1) 'Permit', STREM VARCHAR(150) 'Strem', inoutbound VARCHAR(2) 'InOutBound')
END
SELECT
	ISNULL(@stcd, '') AS 'MSTID'
	,'Done' AS TranXaction
END

EXEC USP_StateMaster_List
EXEC USP_StateMasterDocument_List
END_PROC:
END