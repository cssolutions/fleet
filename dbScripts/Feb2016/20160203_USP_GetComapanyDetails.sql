IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetComapanyDetails]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_GetComapanyDetails
END
GO
CREATE PROC USP_GetComapanyDetails
AS
/*
USP_GetComapanyDetails
*/
SET NOCOUNT ON;
BEGIN
	SELECT * FROM WEBX_COMPANY_MASTER WITH(NOLOCK)
END

