IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetLocationDetails]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_GetLocationDetails
END
GO
CREATE PROC USP_GetLocationDetails
AS
/*
USP_GetLocationDetails
*/
SET NOCOUNT ON;
BEGIN

SELECT * FROM webx_location WITH(NOLOCK)

END