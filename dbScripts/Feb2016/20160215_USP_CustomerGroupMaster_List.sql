IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CustomerGroupMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_CustomerGroupMaster_List
END
GO

/*  
exec USP_CustomerGroupMaster_List
*/  
CREATE PROC [dbo].[USP_CustomerGroupMaster_List]
AS
SET NOCOUNT ON;
BEGIN
SELECT
	*
FROM webx_GRPMST WITH (NOLOCK)
END