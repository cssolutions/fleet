IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckDuplicateGeneralMaster]') AND type IN (N'P'))
BEGIN
	DROP PROC CheckDuplicateGeneralMaster
END
GO
/*
EXEC CheckDuplicateGeneralMaster 'South','ZONE'
*/
CREATE PROC CheckDuplicateGeneralMaster    
    
@CodeDesc VARCHAR(50),    
@CodeType VARCHAR(50)    
--@CodeId VARCHAR(50)    
    
AS    
SET NOCOUNT ON;
BEGIN    
	SELECT COUNT(*) FROM Webx_Master_General WITH(NOLOCK) WHERE CodeDesc=LTRIM(RTRIM(@CodeDesc)) AND CodeType=LTRIM(RTRIM(@CodeType))-- AND CodeId!=LTRIM(RTRIM(@CodeId))
END