IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_Insert_MenuRights]') AND type IN (N'P'))
BEGIN
	DROP PROC Usp_Insert_MenuRights
END
GO
CREATE PROC Usp_Insert_MenuRights    
@MenuRightsInsertXML TEXT,
@UserID VARCHAR(500)    
AS
      
BEGIN TRANSACTION                       
DECLARE @Status INT, @Message VARCHAR(500)                                          
SELECT @Message='',@Status=0                                          
DECLARE @hdoc1 INT                                          
EXEC sp_xml_preparedocument @hdoc1 OUTPUT, @MenuRightsInsertXML                                          
    
BEGIN TRY    
BEGIN    
    
/* Inster Data from CYGNUS_Master_Menu_Access to CYGNUS_Master_Menu_Access_Edit where the userid is @userId*/    
INSERT INTO CYGNUS_Master_Menu_Access_Edit SELECT * FROM CYGNUS_Master_Menu_Access WHERE UserId=@UserID    
    
/* Delete Data from CYGNUS_Master_Menu_Access where the userid is @userId*/    
DELETE FROM CYGNUS_Master_Menu_Access WHERE UserId = @UserID    
    
/* Inster Data New Menu Rights of the Current user*/    
INSERT INTO CYGNUS_Master_Menu_Access(UserId,MenuId,HasAccess)                      
    
SELECT @UserID,MenuId,HasAccess                                   
FROM OPENXML(@hdoc1, 'ArrayOfCYGNUS_Master_Menu_Access/CYGNUS_Master_Menu_Access', 2)                            
WITH       
(       
   MenuId int 'MenuId',                      
   HasAccess VARCHAR(10) 'HasAccess'               
)                            
END                  
   IF (@@ERROR <> 0) GOTO QuitWithRollback                                 
                 
SELECT @Status=1,@Message='Done'                                          
                
END TRY                  
                  
BEGIN CATCH                  
 SELECT @Status=0,@Message='Error : '+ERROR_MESSAGE() +' At Line : '+ CONVERT(VARCHAR,ERROR_LINE())                                          
INSERT INTO CYGNUS_Error_History(ModuleName,TransactionType,ErrorMessage,EntryDate) select 'Menu Access','Insert',@Message,GETDATE()                  
   IF (@@ERROR <> 0) GOTO QuitWithRollback                  
END CATCH                                          
                        
   IF (@@ERROR <> 0) GOTO QuitWithRollback                                 
                  
--exec Usp_GetLocationObject @EnquiryID                                          
                                          
SELECT TranXaction=@Message,Status=@Status           
  COMMIT TRANSACTION                                                        
  GOTO EndSave                   
  QUITWITHROLLBACK:                                                        
  IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION                                                        
SELECT TranXaction=@Message,STATUS=@Status              
  ENDSAVE: