USE [Apmdrs]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_StateMasterDocument_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_StateMasterDocument_List
END
GO
/*  
exec USP_StateMasterDocument_List
*/  
CREATE PROC USP_StateMasterDocument_List
AS  
SET NOCOUNT ON;
BEGIN
	SELECT * FROM Webx_State_Document WITH(NOLOCK)
END