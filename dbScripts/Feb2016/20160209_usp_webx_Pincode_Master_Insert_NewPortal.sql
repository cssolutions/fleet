SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webx_Pincode_Master_Insert_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC usp_webx_Pincode_Master_Insert_NewPortal
END
GO          
/*

BEGIN TRAN
--ROLLBACK
EXEC usp_webx_Pincode_Master_Insert_NewPortal	'<PincodeMaster><pincode>395006</pincode><StateCode>44</StateCode><cityname>3147</cityname><Area>hgjgg</Area><ActiveFlag>Y</ActiveFlag><EntryBy>10001</EntryBy><LocCode></LocCode><District></District><Region></Region><Service_Type></Service_Type></PincodeMaster>'

EXEC usp_webx_Pincode_Master_Insert_NewPortal	'<PincodeMaster><pincode>395004</pincode><StateCode>05</StateCode><cityname>1588</cityname><Area>KATARGAM</Area><ActiveFlag>Y</ActiveFlag><EntryBy>10001</EntryBy><LocCode></LocCode><District></District><Region></Region><Service_Type></Service_Type></PincodeMaster>'

*/

CREATE PROCEDURE usp_webx_Pincode_Master_Insert_NewPortal
                         
@strXMLStateHdr xml           
AS
BEGIN
                                             
DECLARE @hDoc1 int;
DECLARE @FLAG varchar(2), @Pincode varchar(MAX);
EXEC sp_xml_preparedocument	@hDoc1 OUTPUT
							,@strXMLStateHdr

SELECT
	@Pincode = pincode
FROM OPENXML(@hDoc1, 'PincodeMaster', 2)
WITH
(pincode VARCHAR(50) 'pincode')

SELECT
	@FLAG = COUNT(*)
FROM webx_pincode_master
WHERE pincode = @Pincode

----------------------------Record Update--------------------------------------------
IF (@FLAG > 0) BEGIN
UPDATE webx_pincode_master
SET	StateCode = T.StateCode
	,cityname = T.cityname
	,Area = T.Area
	,ActiveFlag = T.ActiveFlag
	,EntryBy = T.EntryBy
	,Entrydt = T.Entrydt
	,LocCode = T.LocCode
	,District = T.District
	,Region = T.Region
	,Service_Type = T.Service_Type
FROM (SELECT
		StateCode
		,cityname
		,Area
		,ActiveFlag
		,EntryBy
		,Entrydt
		,LocCode
		,District
		,Region
		,Service_Type
	FROM OPENXML(@hDoc1, 'PincodeMaster', 2)
	WITH
	(StateCode VARCHAR(50) 'StateCode',
	cityname NUMERIC 'cityname', Area VARCHAR(20) 'Area', ActiveFlag VARCHAR(50) 'ActiveFlag',
	EntryBy VARCHAR(50) 'EntryBy', Entrydt DATETIME 'Entrydt', LocCode VARCHAR(50) 'LocCode',
	District VARCHAR(1) 'District', Region VARCHAR(1) 'Region', Service_Type VARCHAR(5) 'Service_Type')) T
WHERE pincode = @Pincode

SELECT
	ISNULL(@Pincode, '') AS 'MSTID'
	,'Done' AS TranXaction
END
----------------------------Record Save--------------------------------------------
IF (@FLAG = 0) BEGIN
INSERT INTO webx_pincode_master (Pincode, StateCode, cityname, Area, ActiveFlag, EntryBy, Entrydt, LocCode, District, Region, Service_Type)
	SELECT
		@Pincode
		,StateCode
		,cityname
		,Area
		,ActiveFlag
		,EntryBy
		,Entrydt
		,LocCode
		,District
		,Region
		,Service_Type
	FROM OPENXML(@hDoc1, 'PincodeMaster', 2)
	WITH
	(StateCode VARCHAR(50) 'StateCode',
	cityname NUMERIC 'cityname', Area VARCHAR(20) 'Area', ActiveFlag VARCHAR(50) 'ActiveFlag',
	EntryBy VARCHAR(50) 'EntryBy', Entrydt DATETIME 'Entrydt', LocCode VARCHAR(50) 'LocCode',
	District VARCHAR(1) 'District', Region VARCHAR(1) 'Region', Service_Type VARCHAR(5) 'Service_Type')

SELECT
	ISNULL(@Pincode, '') AS 'MSTID'
	,'Done' AS TranXaction
END

EXEC USP_PincodeMaster_List
END_PROC:
END