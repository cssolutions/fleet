IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GeneralMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_GeneralMaster_List
END
GO
/*  
exec USP_GeneralMaster_List
*/  
CREATE PROC USP_GeneralMaster_List
AS  
SET NOCOUNT ON;
BEGIN
	SELECT CodeType,CodeId,CodeDesc,CodeAccess,StatusCode,EntryDate,EntryBy,LastUpdatedDate,LastUpdatedBy,noofdigits,noofchar,codefor FROM Webx_Master_General WITH(NOLOCK)
END