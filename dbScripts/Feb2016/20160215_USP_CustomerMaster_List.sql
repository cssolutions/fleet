IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CustomerMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_CustomerMaster_List
END
GO

/*  
exec USP_CustomerMaster_List
*/  
CREATE PROC [dbo].[USP_CustomerMaster_List]
AS
SET NOCOUNT ON;
BEGIN
SELECT
	*
FROM webx_CUSTHDR WITH (NOLOCK)
END