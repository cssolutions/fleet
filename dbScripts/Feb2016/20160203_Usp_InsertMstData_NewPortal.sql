IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_InsertMstData_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC Usp_InsertMstData_NewPortal
END
GO
CREATE PROC Usp_InsertMstData_NewPortal
@Mst TEXT ,                  
@Entry_EditFlag VARCHAR(10) ,  
@Finyear VARCHAR(4)                       
                           
AS                                
SET NOCOUNT ON;
  ---Step 1                                             
  --Start Transaction------
  BEGIN TRANSACTION
	BEGIN TRY
			
			DECLARE @ReturnCode INT,@MstID VARCHAR(200),@MstNAME   VARCHAR(200),@MstType   VARCHAR(200)  ,@StatusCode VARCHAR(6),@EntryBy VARCHAR(50)                           
			SELECT @ReturnCode = 0                              
			DECLARE @hDoc1 INT                                
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @Mst     
                      
			 /***********************************Edit Flag Is E *************************************************/             
			 SELECT @MstID=CodeId,@MstNAME=CodeDesc,@MstType=CodeType ,@StatusCode=StatusCode ,@EntryBy=EntryBy            
			   FROM OPENXML(@hDoc1,'root/MST',2)                                 
			   WITH                                 
			   (                                
					CodeId VARCHAR(15) 'CodeId',                                
					CodeDesc VARCHAR(100) 'CodeDesc'  ,            
					CodeType VARCHAR(100) 'CodeType' ,           
					StatusCode VARCHAR(100) 'StatusCode',
					EntryBy  VARCHAR(200) 'EntryBy'                     
				)
				   
				IF @Entry_EditFlag='US'                  
				 BEGIN                  
				  UPDATE  Webx_Master_General SET StatusCode=@StatusCode,LastUpdatedDate=GETDATE(),LastUpdatedBy=@EntryBy WHERE CodeType=@MstType  and CodeId=@MstID            
				 END                   
				ELSE          
				 BEGIN          
				  IF @Entry_EditFlag='U'                  
					BEGIN                  
						DELETE FROM Webx_Master_General WHERE CodeType=@MstType  and CodeId=@MstID            
					END   
				  ELSE  
					BEGIN  
						EXEC [Usp_Update_Docno] @MstType,@Finyear,'',''    
					END      

				 /************************************************************************************/  
   
				IF(@MstID IS NULL OR @MstID = '')
					BEGIN
						CREATE TABLE #tmpNextCode
						(
						 CodeId VARCHAR(MAX)
						)

						INSERT INTO #tmpNextCode
						EXEC [Usp_SP_GetNextDocumentCode] '',@Finyear,@MstType ,''

						DECLARE @CodeId AS VARCHAR(MAX)
						SELECT @CodeId = CodeId FROM #tmpNextCode
						--SELECT @CodeId 
						SET @MstID = @CodeId
					END
                   
				   INSERT INTO Webx_Master_General (CodeType,CodeId,CodeDesc,CodeAccess,StatusCode,EntryDate,EntryBy)                               
				   SELECT                               
				   CodeType,@MstID,CodeDesc,'U',StatusCode,getdate(),EntryBy                  
				   FROM OPENXML(@hDoc1,'root/MST',2)                               
				   WITH                               
				   (                              
						CodeType VARCHAR(15) 'CodeType',                              
						CodeId VARCHAR(100) 'CodeId',               
						CodeDesc VARCHAR(2000) 'CodeDesc',                                
						StatusCode VARCHAR(100) 'StatusCode',                
						EntryBy  VARCHAR(200) 'EntryBy'                 
				   )
   
				   EXEC [Usp_Update_Docno] @MstType,@Finyear,'',''
              
				END
				SELECT isnull(@MstID,'')+':'+isnull(@MstNAME,'')  AS  'MSTID','Done' AS TranXaction 
				EXEC USP_GeneralMaster_List
		COMMIT TRANSACTION;
	END TRY
	BEGIN CATCH
		SELECT @MstID+':'+@MstNAME  AS  'MSTID','Not Done' AS TranXaction,@ReturnCode
		ROLLBACK TRANSACTION;
	END CATCH;