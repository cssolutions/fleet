
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_CYGNUS_GetUserMenuRight_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC Usp_CYGNUS_GetUserMenuRight_NewPortal
END
GO

CREATE PROC Usp_CYGNUS_GetUserMenuRight_NewPortal
AS
/*
Usp_CYGNUS_GetUserMenuRights_New
*/
SET NOCOUNT ON;
BEGIN
	--IF @Type = 'BindMenu'
		SELECT a.MenuId,b.UserId,DisplayName,ParentID,NavigationURL,DisplayRank,HasAccess=isnull(HasAccess,'0'),MenuLevel 
		FROM CYGNUS_MASTER_MENU a         
		LEFT OUTER JOIN CYGNUS_Master_Menu_Access b on a.Menuid=B.Menuid --and userId=@UserId        
		WHERE IsActive=1 --and b.HasAccess =1
	--IF @Type = 'MenuRightsModule'
	--	SELECT a.MenuId,b.UserId,DisplayName,ParentID,NavigationURL,DisplayRank,HasAccess=isnull(HasAccess,'0'),MenuLevel
	--	FROM CYGNUS_MASTER_MENU a         
	--	LEFT OUTER JOIN CYGNUS_Master_Menu_Access b on a.Menuid=B.Menuid --and userId=@UserId        
	--	WHERE IsActive=1
END