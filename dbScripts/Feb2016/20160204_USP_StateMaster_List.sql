USE [Apmdrs]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_StateMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_StateMaster_List
END
GO
/*  
exec USP_StateMaster_List
*/  
CREATE PROC USP_StateMaster_List
AS  
SET NOCOUNT ON;
BEGIN
	SELECT * FROM Webx_State WITH(NOLOCK)
END