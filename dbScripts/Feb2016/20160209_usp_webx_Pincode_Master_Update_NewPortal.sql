USE [Apmdrs]
GO
/****** Object:  StoredProcedure [dbo].[usp_webx_Pincode_Master_Update]    Script Date: 2/9/2016 3:35:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_webx_Pincode_Master_Update_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC usp_webx_Pincode_Master_Update_NewPortal
END
GO      
/*        
----------------------------------------------------------------------------------------------------        
-- Date Created: Friday, January 16, 2009        
        
-- Created By:  (Tarun)        
-- Purpose: Inserts a record into the webx_pincode_master        
----------------------------------------------------------------------------------------------------        
*/        
        
CREATE PROCEDURE [dbo].[usp_webx_Pincode_Master_Update_NewPortal]        
(        
           @pincode int,       
           @LocCode varchar(25),       
           @cityname varchar(50),        
           @StateCode varchar(50),        
           @EntryBy varchar(50),        
           @Entrydt datetime,        
           @ActiveFlag varchar(2),         
           @Area varchar(100),    
           @LocFlag varchar(2)           
)        
AS        
  
 IF @LocFlag = 'N'   
      BEGIN
UPDATE [dbo].[webx_pincode_master]
SET	[Area] = @Area
	,[EntryBy] = @EntryBy
	,[Entrydt] = @Entrydt
	,[ActiveFlag] = @ActiveFlag
WHERE [pincode] = @pincode
AND [cityname] = @cityname
AND [StateCode] = @StateCode;
END ELSE BEGIN
UPDATE [dbo].[webx_pincode_Loc_master]
SET	[Area] = @Area
	,[EntryBy] = @EntryBy
	,[Entrydt] = GETDATE()
	,[ActiveFlag] = @ActiveFlag
WHERE [pincode] = @pincode
AND [LocCode] = @LocCode;
END