IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetMenus_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_GetMenus_NewPortal
END
GO
CREATE PROC USP_GetMenus_NewPortal
AS 
/*
USP_GetMenus_NewPortal
*/
SET NOCOUNT ON;
BEGIN
	SELECT * FROM CYGNUS_Master_Menu WITH(NOLOCK)
END