IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_GetUserDetails]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_GetUserDetails
END
GO
CREATE PROC USP_GetUserDetails
AS
/*
USP_GetUserDetails
*/
SET NOCOUNT ON;
BEGIN

SELECT * FROM WebX_Master_Users WITH(NOLOCK)

END