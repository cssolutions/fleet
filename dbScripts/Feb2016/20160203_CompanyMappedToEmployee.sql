IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompanyMappedToEmployee]') AND TYPE IN (N'P'))
BEGIN
	DROP PROC CompanyMappedToEmployee
END
GO
CREATE PROCEDURE CompanyMappedToEmployee
@EmpCode VARCHAR(50)  
AS  
SET NOCOUNT ON  
  
DECLARE @CompList VARCHAR(8000),@SQL VARCHAR(8000)  
  
BEGIN  
	SELECT @CompList=BranchCode FROM WebX_Master_Users WITH(NOLOCK) WHERE userid=@EmpCode  
	SELECT @SQL='SELECT A.COMPANY_CODE AS CODE, A.COMPANY_NAME AS NAME  
	FROM WEBX_COMPANY_MASTER A  
	WHERE A.ACTIVEFLAG=''Y''  
	AND A.COMPANY_CODE IN ('+ CHAR(39) +replace( replace(replace(@CompList,' ,',','),',',CHAR(39)+','+CHAR(39)) + CHAR(39),' ',' ')+')'  
EXEC (@SQL)  
--SELECT @SQL  
END