USE [Apmdrs]
GO

/****** Object:  StoredProcedure [dbo].[USP_PincodeMaster_List]    Script Date: 2/9/2016 6:43:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_PincodeMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_PincodeMaster_List
END
GO     
/*  
exec USP_PincodeMaster_List
*/  
CREATE PROC [dbo].[USP_PincodeMaster_List]
AS
SET NOCOUNT ON;
BEGIN
SELECT
	*
FROM webx_pincode_master WITH (NOLOCK)
END
GO