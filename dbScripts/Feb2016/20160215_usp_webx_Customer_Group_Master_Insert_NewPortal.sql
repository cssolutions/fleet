IF  EXISTS (SELECT
		*
	FROM sys.objects
	WHERE object_id = OBJECT_ID(N'[dbo].[usp_webx_Customer_Group_Master_Insert_NewPortal]')
	AND type IN (N'P')) BEGIN
DROP PROC usp_webx_Customer_Group_Master_Insert_NewPortal
END
GO

/*

BEGIN TRAN
--ROLLBACK
EXEC [usp_webx_Customer_Group_Master_Insert_NewPortal]	'<CustomerGroupMaster><GRPCD></GRPCD><GRPNM>10001</GRPNM><GRP_Pwd>Rvm@#$321</GRP_Pwd><locregion></locregion><ActiveFlag>Y</ActiveFlag><OLD_GRPCD></OLD_GRPCD><UPDTBY>10001</UPDTBY><UPDTON>2/13/2016 3:53:35 PM</UPDTON ><isSysGenerated>Y</isSysGenerated></CustomerGroupMaster>'

EXEC [usp_webx_Customer_Group_Master_Insert_NewPortal]	'<CustomerGroupMaster><GRPCD>C0016</GRPCD><GRPNM>ARKESH2</GRPNM><GRP_Pwd>12356</GRP_Pwd><locregion></locregion><ActiveFlag>Y</ActiveFlag><OLD_GRPCD></OLD_GRPCD><UPDTBY>10001</UPDTBY><UPDTON>2/13/2016 3:53:35 PM</UPDTON ><isSysGenerated>Y</isSysGenerated></CustomerGroupMaster>'

*/
CREATE PROCEDURE [dbo].[usp_webx_Customer_Group_Master_Insert_NewPortal]  
@strXMLStateHdr xml           
AS
BEGIN
                                             
DECLARE @hDoc1 int;
DECLARE @FLAG varchar(2), @Groupcode varchar(MAX);
EXEC sp_xml_preparedocument	@hDoc1 OUTPUT
							,@strXMLStateHdr

SELECT
	@Groupcode = GRPCD
FROM OPENXML(@hDoc1, 'CustomerGroupMaster', 2)
WITH
(GRPCD VARCHAR(50) 'GRPCD')

SELECT
	@FLAG = COUNT(*)
FROM webx_GRPMST
WHERE GRPCD = @Groupcode

----------------------------Record Update--------------------------------------------
IF (@FLAG > 0) BEGIN
UPDATE webx_GRPMST
SET	GRPNM = T.GRPNM
	,GRP_Pwd = T.GRP_Pwd
	,locregion = T.locregion
	,ActiveFlag = T.ActiveFlag
	,OLD_GRPCD = T.OLD_GRPCD
	,UPDTBY = T.UPDTBY
	,UPDTON = T.UPDTON
	,isSysGenerated = T.isSysGenerated
FROM (SELECT
		GRPNM
		,GRP_Pwd
		,locregion
		,ActiveFlag
		,OLD_GRPCD
		,UPDTBY
		,UPDTON
		,isSysGenerated
	FROM OPENXML(@hDoc1, 'CustomerGroupMaster', 2)
	WITH
	(GRPNM VARCHAR(50) 'GRPNM',
	GRP_Pwd VARCHAR(50) 'GRP_Pwd', locregion VARCHAR(20) 'locregion', ActiveFlag VARCHAR(50) 'ActiveFlag',
	OLD_GRPCD VARCHAR(50) 'OLD_GRPCD', UPDTBY VARCHAR(50) 'UPDTBY', UPDTON DATETIME 'UPDTON',
	isSysGenerated VARCHAR(1) 'isSysGenerated')) T
WHERE GRPCD = @Groupcode

SELECT
	ISNULL(@Groupcode, '') AS 'MSTID'
	,'Done' AS TranXaction
END
----------------------------Record Save--------------------------------------------
CREATE TABLE #tmpNextCode(CodeId VARCHAR(MAX))
INSERT INTO #tmpNextCode
EXEC USP_GetNextDocumentCode	'HQTR'
								,''
								,'C'
								,'C001'
DECLARE @CodeId AS VARCHAR(MAX)
SELECT
	@CodeId = CodeId
FROM #tmpNextCode

IF (@FLAG = 0) BEGIN
INSERT INTO webx_GRPMST (GRPCD, GRPNM, GRP_Pwd, locregion, ActiveFlag, OLD_GRPCD, UPDTBY, UPDTON, isSysGenerated)
	SELECT
		@Groupcode
		,GRPNM
		,GRP_Pwd
		,locregion
		,ActiveFlag
		,OLD_GRPCD
		,UPDTBY
		,UPDTON
		,isSysGenerated
	FROM OPENXML(@hDoc1, 'CustomerGroupMaster', 2)
	WITH
	(GRPNM VARCHAR(50) 'GRPNM',
	GRP_Pwd VARCHAR(50) 'GRP_Pwd', locregion VARCHAR(20) 'locregion', ActiveFlag VARCHAR(50) 'ActiveFlag',
	OLD_GRPCD VARCHAR(50) 'OLD_GRPCD', UPDTBY VARCHAR(50) 'UPDTBY', UPDTON DATETIME 'UPDTON',
	isSysGenerated VARCHAR(1) 'isSysGenerated')

SELECT
	ISNULL(@Groupcode, '') AS 'MSTID'
	,'Done' AS TranXaction
END
EXEC Usp_Update_Docno	'C'
						,''
						,'HQTR'
						,'C001'
EXEC USP_CustomerGroupMaster_List
END_PROC:
END