IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[USP_CityMaster_List]') AND type IN (N'P'))
BEGIN
	DROP PROC USP_CityMaster_List
END
GO
/*  
exec USP_CityMaster_List
*/  
CREATE PROC USP_CityMaster_List
AS  
SET NOCOUNT ON;
BEGIN
	SELECT * FROM webx_citymaster WITH(NOLOCK)
END