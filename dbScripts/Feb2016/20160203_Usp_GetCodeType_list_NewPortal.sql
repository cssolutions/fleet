IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Usp_GetCodeType_list_NewPortal]') AND type IN (N'P'))
BEGIN
	DROP PROC Usp_GetCodeType_list_NewPortal
END
GO
/*
Usp_GetCodeType_list_NewPortal 'STATE'
*/
CREATE PROCEDURE Usp_GetCodeType_list_NewPortal
AS
SET NOCOUNT ON;
BEGIN

	SELECT SrNo,HeaderCode,HeaderDesc,HeaderAccess,ID_desc,Name_Desc,Activeflag_YN,ToSupport FROM WebX_Master_CodeTypes WITH(NOLOCK)

END