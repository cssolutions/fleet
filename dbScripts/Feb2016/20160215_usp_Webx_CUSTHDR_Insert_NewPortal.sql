USE Apmdrs
IF  EXISTS (SELECT
		*
	FROM sys.objects
	WHERE object_id = OBJECT_ID(N'[dbo].[usp_Webx_CUSTHDR_Insert_NewPortal]')
	AND type IN (N'P')) BEGIN
DROP PROC usp_Webx_CUSTHDR_Insert_NewPortal
END
GO
/*

BEGIN TRAN
--ROLLBACK
EXEC [usp_Webx_CUSTHDR_Insert_NewPortal]	'<?xml version="1.0"?><DocumentElement xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><GRPCD>C0004</GRPCD><CUSTNM>Arkesh</CUSTNM><CUSTCAT /><TBBCRDAYS xsi:nil="true" /><TOPAYCRDAYS xsi:nil="true" /><OCTCRDAYS xsi:nil="true" /><UPDTON xsi:nil="true" /><DKTCHG xsi:nil="true" /><FOVOWNER xsi:nil="true" /><FOVCARRIER xsi:nil="true" /><CSTNO>9725682497</CSTNO><EMAILIDS>arkeshkorat404@gmail.com</EMAILIDS><CUST_ACTIVE>Y</CUST_ACTIVE><MOBSERV_ENABLED>Y</MOBSERV_ENABLED><MOBILENO>9725682497</MOBILENO><STNUM>9725682497</STNUM><opendebit xsi:nil="true" /><opencredit xsi:nil="true" /><CUSTLOC>AHMD</CUSTLOC><CustAddress>A-503, Golden Plaza, Ankur Char Rasta,A. K. Road, Surat</CustAddress><city>Surat</city><telno>9725682497</telno><pincode>395008</pincode><pan_no>9725682497</pan_no><ServiceOptFor /><ServiceTaxNumber>9725682497</ServiceTaxNumber><Industry>5</Industry><FaxNo>9725682497</FaxNo><Website>9725682497</Website><Address_Bill>A-503, Golden Plaza, Ankur Char Rasta,A. K. Road, Surat</Address_Bill><City_Bill>Surat</City_Bill><Pincode_Bill>395008</Pincode_Bill><BankName>9725682497</BankName><BranchName>9725682497</BranchName><BankAccountNo>9725682497</BankAccountNo><Decision_Name>9725682497</Decision_Name><Decision_Designation>9725682497</Decision_Designation><Decision_Mobile>9725682497</Decision_Mobile><Decision_Email>9725682497</Decision_Email><Turnover>9725682497</Turnover><TIN_No>9725682497</TIN_No><Ownership>5</Ownership><IsSpecialBill_YN>Y</IsSpecialBill_YN><Bill_Part1_Percentage xsi:nil="true" /><Bill_Part2_Percentage xsi:nil="true" /><IsFovApplied xsi:nil="true" /><Paid>N</Paid><TBB>N</TBB><ToPay>N</ToPay><FOC>N</FOC><Transportation>N</Transportation><Warehouse>N</Warehouse></DocumentElement>'

BEGIN TRAN
--ROLLBACK
EXEC [usp_Webx_CUSTHDR_Insert_NewPortal]	'<?xml version="1.0"?><DocumentElement xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><GRPCD>C0004</GRPCD><CUSTCD>C00015399</CUSTCD><CUSTNM>BAJAJ ELECTRICALS LTD (NAVIMUMBAI)</CUSTNM><CUSTPASS>C00015399</CUSTPASS><CUSTCAT /><TBBCRDAYS xsi:nil="true" /><TOPAYCRDAYS xsi:nil="true" /><OCTCRDAYS xsi:nil="true" /><UPDTON xsi:nil="true" /><DKTCHG xsi:nil="true" /><FOVOWNER xsi:nil="true" /><FOVCARRIER xsi:nil="true" /><CSTNO>NA</CSTNO><EMAILIDS>info@bajaj.com</EMAILIDS><CUST_ACTIVE>N</CUST_ACTIVE><MOBSERV_ENABLED>Y</MOBSERV_ENABLED><MOBILENO>2224060400</MOBILENO><STNUM>NA</STNUM><opendebit xsi:nil="true" /><opencredit xsi:nil="true" /><CUSTLOC>AGRA</CUSTLOC><CustAddress>8TH OFFICE LEVEL,(10TH FLOOR),RUSTUMJEE ASPIREE,BHAU SHANKAR YAGNIK MARG,OFF EASTERN EXPRESS HIGHWAY,SION (EAST),MUMBAI-400 022</CustAddress><city>NAVI MUMBAI</city><telno>022/24060 4000</telno><pincode>400022</pincode><ServiceOptFor /><ServiceTaxNumber>NA</ServiceTaxNumber><Industry>14</Industry><FaxNo>0</FaxNo><Website>abc@gmail.com</Website><Address_Bill>8TH OFFICE LEVEL,(10TH FLOOR),RUSTUMJEE ASPIREE,BHAU SHANKAR YAGNIK MARG,OFF EASTERN EXPRESS HIGHWAY,SION (EAST),MUMBAI-400 022</Address_Bill><City_Bill>NAVI MUMBAI</City_Bill><Pincode_Bill>400022</Pincode_Bill><BankName>NA</BankName><BranchName>NAVIMUMBAI</BranchName><BankAccountNo>NA</BankAccountNo><Decision_Name>ASHISH</Decision_Name><Decision_Designation>DM</Decision_Designation><Decision_Mobile>9312236691</Decision_Mobile><Decision_Email>ashishmishra@agarwalpackers.com</Decision_Email><Turnover>0</Turnover><TIN_No>NA</TIN_No><Ownership>1</Ownership><IsSpecialBill_YN>N</IsSpecialBill_YN><Bill_Part1_Percentage xsi:nil="true" /><Bill_Part2_Percentage xsi:nil="true" /><IsFovApplied xsi:nil="true" /><Paid>Y</Paid><TBB>Y</TBB><ToPay>Y</ToPay><FOC>Y</FOC><Transportation>Y</Transportation><Warehouse>Y</Warehouse></DocumentElement>'

*/  
CREATE PROCEDURE [dbo].[usp_Webx_CUSTHDR_Insert_NewPortal]          
@strXMLStateHdr xml           
AS
BEGIN
                                             
DECLARE @hDoc1 int;
DECLARE @FLAG varchar(2), @Groupcode varchar(MAX),@Customercode varchar(MAX);
EXEC sp_xml_preparedocument	@hDoc1 OUTPUT
							,@strXMLStateHdr

SELECT
	@Groupcode = GRPCD
	,@Customercode = CUSTCD
FROM OPENXML(@hDoc1, 'DocumentElement', 2)
WITH
(GRPCD VARCHAR(50) 'GRPCD', CUSTCD VARCHAR(50) 'CUSTCD')

SELECT
	@FLAG = COUNT(*)
FROM webx_CUSTHDR
WHERE CUSTCD = @Customercode

----------------------------Record Update--------------------------------------------
IF (@FLAG > 0) BEGIN
UPDATE webx_CUSTHDR
SET	GRPCD = T.GRPCD
	,CUSTNM = T.CUSTNM
	,CUSTPASS = T.CUSTPASS
	,CUSTCAT = T.CUSTCAT
	,TBBELEGIBILITY = T.TBBELEGIBILITY
	,TOPAYELEGIBILITY = T.TOPAYELEGIBILITY
	,OCTROIELIGIBILITY = T.OCTROIELIGIBILITY
	,UPDTBY = T.UPDTBY
	,UPDTON = T.UPDTON
	,RATETYPE = T.RATETYPE
	,RATEMATRIX = T.RATEMATRIX
	,SERVICETAX = T.SERVICETAX
	,BDEREF = T.BDEREF
	,CSTNO = T.CSTNO
	,EMAILIDS = T.EMAILIDS
	,CUST_ACTIVE = T.CUST_ACTIVE
	,MOBSERV_ENABLED = T.MOBSERV_ENABLED
	,MOBILENO = T.MOBILENO
	,STNUM = T.STNUM
	,OLD_GRPCD = T.OLD_GRPCD
	,OLD_CUSTCD = T.OLD_CUSTCD
	,contract_made = T.contract_made
	,CUSTLOC = T.CUSTLOC
	,InvoicewiseBill_YN = T.InvoicewiseBill_YN
	,CustAddress = T.CustAddress
	,city = T.city
	,telno = T.telno
	,pincode = T.pincode
	,pan_no = T.pan_no
	,ServiceOptFor = T.ServiceOptFor
	,svc_opted = T.svc_opted
	,ServiceTaxNumber = T.ServiceTaxNumber
	,CntLoc = T.CntLoc
	,Industry = T.Industry
	,FaxNo = T.FaxNo
	,Website = T.Website
	,Address_Bill = T.Address_Bill
	,City_Bill = T.City_Bill
	,Pincode_Bill = T.Pincode_Bill
	,BankName = T.BankName
	,BranchName = T.BranchName
	,BankAccountNo = T.BankAccountNo
	,Decision_Name = T.Decision_Name
	,Decision_Designation = T.Decision_Designation
	,Decision_Mobile = T.Decision_Mobile
	,Decision_Email = T.Decision_Email
	,Turnover = T.Turnover
	,TIN_No = T.TIN_No
	,Ownership = T.Ownership
	,isSysGenerated = T.isSysGenerated
	,IsSpecialBill_YN = T.IsSpecialBill_YN
	,WebRefNo = T.WebRefNo
	,IsFovApplied = T.IsFovApplied

FROM (SELECT
		GRPCD
		,CUSTNM
		,CUSTPASS
		,CUSTCAT
		,TBBELEGIBILITY
		,TOPAYELEGIBILITY
		,OCTROIELIGIBILITY
		,UPDTBY
		,UPDTON
		,RATETYPE
		,RATEMATRIX
		,SERVICETAX
		,BDEREF
		,CSTNO
		,EMAILIDS
		,CUST_ACTIVE
		,MOBSERV_ENABLED
		,MOBILENO
		,STNUM
		,OLD_GRPCD
		,OLD_CUSTCD
		,contract_made
		,CUSTLOC
		,InvoicewiseBill_YN
		,CustAddress
		,city
		,telno
		,pincode
		,pan_no
		,ServiceOptFor
		,svc_opted
		,ServiceTaxNumber
		,CntLoc
		,Industry
		,FaxNo
		,Website
		,Address_Bill
		,City_Bill
		,Pincode_Bill
		,BankName
		,BranchName
		,BankAccountNo
		,Decision_Name
		,Decision_Designation
		,Decision_Mobile
		,Decision_Email
		,Turnover
		,TIN_No
		,Ownership
		,isSysGenerated
		,IsSpecialBill_YN
		,WebRefNo
		,IsFovApplied
	FROM OPENXML(@hDoc1, 'DocumentElement', 2)
	WITH
	(GRPCD VARCHAR(MAX) 'GRPCD', CUSTNM VARCHAR(MAX) 'CUSTNM', CUSTPASS VARCHAR(MAX) 'CUSTPASS', CUSTCAT VARCHAR(MAX) 'CUSTCAT', TBBELEGIBILITY VARCHAR(MAX) 'TBBELEGIBILITY', TOPAYELEGIBILITY VARCHAR(MAX) 'TOPAYELEGIBILITY', OCTROIELIGIBILITY VARCHAR(MAX) 'OCTROIELIGIBILITY', UPDTBY VARCHAR(MAX) 'UPDTBY', UPDTON VARCHAR(MAX) 'UPDTON', RATETYPE VARCHAR(MAX) 'RATETYPE', RATEMATRIX VARCHAR(MAX) 'RATEMATRIX', SERVICETAX VARCHAR(MAX) 'SERVICETAX', BDEREF VARCHAR(MAX) 'BDEREF', CSTNO VARCHAR(MAX) 'CSTNO', EMAILIDS VARCHAR(MAX) 'EMAILIDS', CUST_ACTIVE VARCHAR(MAX) 'CUST_ACTIVE', MOBSERV_ENABLED VARCHAR(MAX) 'MOBSERV_ENABLED', MOBILENO VARCHAR(MAX) 'MOBILENO', STNUM VARCHAR(MAX) 'STNUM', OLD_GRPCD VARCHAR(MAX) 'OLD_GRPCD', OLD_CUSTCD VARCHAR(MAX) 'OLD_CUSTCD', contract_made VARCHAR(MAX) 'contract_made', CUSTLOC VARCHAR(MAX) 'CUSTLOC', InvoicewiseBill_YN VARCHAR(MAX) 'InvoicewiseBill_YN', CustAddress VARCHAR(MAX) 'CustAddress', city VARCHAR(MAX) 'city', telno VARCHAR(MAX) 'telno', pincode VARCHAR(MAX) 'pincode', pan_no VARCHAR(MAX) 'pan_no', ServiceOptFor VARCHAR(MAX) 'ServiceOptFor', svc_opted VARCHAR(MAX) 'svc_opted', ServiceTaxNumber VARCHAR(MAX) 'ServiceTaxNumber', CntLoc VARCHAR(MAX) 'CntLoc', Industry VARCHAR(MAX) 'Industry', FaxNo VARCHAR(MAX) 'FaxNo', Website VARCHAR(MAX) 'Website', Address_Bill VARCHAR(MAX) 'Address_Bill', City_Bill VARCHAR(MAX) 'City_Bill', Pincode_Bill VARCHAR(MAX) 'Pincode_Bill', BankName VARCHAR(MAX) 'BankName', BranchName VARCHAR(MAX) 'BranchName', BankAccountNo VARCHAR(MAX) 'BankAccountNo', Decision_Name VARCHAR(MAX) 'Decision_Name', Decision_Designation VARCHAR(MAX) 'Decision_Designation', Decision_Mobile VARCHAR(MAX) 'Decision_Mobile', Decision_Email VARCHAR(MAX) 'Decision_Email', Turnover numeric(12,2) 'Turnover', TIN_No VARCHAR(MAX) 'TIN_No', Ownership VARCHAR(MAX) 'Ownership', isSysGenerated VARCHAR(MAX) 'isSysGenerated', IsSpecialBill_YN VARCHAR(MAX) 'IsSpecialBill_YN', WebRefNo VARCHAR(MAX) 'WebRefNo', IsFovApplied VARCHAR(MAX) 'IsFovApplied')) T
WHERE CUSTCD = @Customercode

SELECT
	ISNULL(@Customercode, '') AS 'MSTID'
	,'Done' AS TranXaction
END
----------------------------Record Save--------------------------------------------
IF (@FLAG = 0) BEGIN
----------------------------Customer Code Generation-------------------------------
CREATE TABLE #tmpNextCode(CodeId VARCHAR(MAX))
INSERT INTO #tmpNextCode
EXEC USP_GetNextDocumentCode	'HQTR'
								,''
								,'CUST'
								,'C001'
DECLARE @CustCode AS VARCHAR(MAX)
SELECT
	@CustCode = CodeId
FROM #tmpNextCode
SELECT
	@Customercode = @GroupCode + @CustCode
----------------------------Customer Code Generation End----------------------------
INSERT INTO webx_CUSTHDR (GRPCD,CUSTCD,CUSTPASS,CUSTNM,CUSTCAT,TBBELEGIBILITY,TOPAYELEGIBILITY,OCTROIELIGIBILITY,UPDTBY,UPDTON,RATETYPE,RATEMATRIX,SERVICETAX,BDEREF,CSTNO,EMAILIDS,CUST_ACTIVE,MOBSERV_ENABLED,MOBILENO,STNUM,OLD_GRPCD,OLD_CUSTCD,contract_made,CUSTLOC,InvoicewiseBill_YN,CustAddress,city,telno,pincode,pan_no,ServiceOptFor,svc_opted,ServiceTaxNumber,CntLoc,Industry,FaxNo,Website,Address_Bill,City_Bill,Pincode_Bill,BankName,BranchName,BankAccountNo,Decision_Name,Decision_Designation,Decision_Mobile,Decision_Email,Turnover,TIN_No,Ownership,isSysGenerated,IsSpecialBill_YN,WebRefNo,IsFovApplied)
	SELECT
		GRPCD
		,@Customercode
		,@Customercode
		,CUSTNM
		,CUSTCAT
		,TBBELEGIBILITY
		,TOPAYELEGIBILITY
		,OCTROIELIGIBILITY
		,UPDTBY
		,UPDTON
		,RATETYPE
		,RATEMATRIX
		,SERVICETAX
		,BDEREF
		,CSTNO
		,EMAILIDS
		,CUST_ACTIVE
		,MOBSERV_ENABLED
		,MOBILENO
		,STNUM
		,OLD_GRPCD
		,OLD_CUSTCD
		,contract_made
		,CUSTLOC
		,InvoicewiseBill_YN
		,CustAddress
		,city
		,telno
		,pincode
		,pan_no
		,ServiceOptFor
		,svc_opted
		,ServiceTaxNumber
		,CntLoc
		,Industry
		,FaxNo
		,Website
		,Address_Bill
		,City_Bill
		,Pincode_Bill
		,BankName
		,BranchName
		,BankAccountNo
		,Decision_Name
		,Decision_Designation
		,Decision_Mobile
		,Decision_Email
		,Turnover
		,TIN_No
		,Ownership
		,isSysGenerated
		,IsSpecialBill_YN
		,WebRefNo
		,IsFovApplied
	FROM OPENXML(@hDoc1, 'DocumentElement', 2)
	WITH
	(GRPCD VARCHAR(50) 'GRPCD', CUSTNM VARCHAR(MAX) 'CUSTNM', CUSTCAT VARCHAR(MAX) 'CUSTCAT', TBBELEGIBILITY VARCHAR(MAX) 'TBBELEGIBILITY', TOPAYELEGIBILITY VARCHAR(MAX) 'TOPAYELEGIBILITY', OCTROIELIGIBILITY VARCHAR(MAX) 'OCTROIELIGIBILITY', UPDTBY VARCHAR(MAX) 'UPDTBY', UPDTON VARCHAR(MAX) 'UPDTON', RATETYPE VARCHAR(MAX) 'RATETYPE', RATEMATRIX VARCHAR(MAX) 'RATEMATRIX', SERVICETAX VARCHAR(MAX) 'SERVICETAX', BDEREF VARCHAR(MAX) 'BDEREF', CSTNO VARCHAR(MAX) 'CSTNO', EMAILIDS VARCHAR(MAX) 'EMAILIDS', CUST_ACTIVE VARCHAR(MAX) 'CUST_ACTIVE', MOBSERV_ENABLED VARCHAR(MAX) 'MOBSERV_ENABLED', MOBILENO VARCHAR(MAX) 'MOBILENO', STNUM VARCHAR(MAX) 'STNUM', OLD_GRPCD VARCHAR(MAX) 'OLD_GRPCD', OLD_CUSTCD VARCHAR(MAX) 'OLD_CUSTCD', contract_made VARCHAR(MAX) 'contract_made', CUSTLOC VARCHAR(MAX) 'CUSTLOC', InvoicewiseBill_YN VARCHAR(MAX) 'InvoicewiseBill_YN', CustAddress VARCHAR(MAX) 'CustAddress', city VARCHAR(MAX) 'city', telno VARCHAR(MAX) 'telno', pincode VARCHAR(MAX) 'pincode', pan_no VARCHAR(MAX) 'pan_no', ServiceOptFor VARCHAR(MAX) 'ServiceOptFor', svc_opted VARCHAR(MAX) 'svc_opted', ServiceTaxNumber VARCHAR(MAX) 'ServiceTaxNumber', CntLoc VARCHAR(MAX) 'CntLoc', Industry VARCHAR(MAX) 'Industry', FaxNo VARCHAR(MAX) 'FaxNo', Website VARCHAR(MAX) 'Website', Address_Bill VARCHAR(MAX) 'Address_Bill', City_Bill VARCHAR(MAX) 'City_Bill', Pincode_Bill VARCHAR(MAX) 'Pincode_Bill', BankName VARCHAR(MAX) 'BankName', BranchName VARCHAR(MAX) 'BranchName', BankAccountNo VARCHAR(MAX) 'BankAccountNo', Decision_Name VARCHAR(MAX) 'Decision_Name', Decision_Designation VARCHAR(MAX) 'Decision_Designation', Decision_Mobile VARCHAR(MAX) 'Decision_Mobile', Decision_Email VARCHAR(MAX) 'Decision_Email', Turnover numeric(12,2) 'Turnover', TIN_No VARCHAR(MAX) 'TIN_No', Ownership VARCHAR(MAX) 'Ownership', isSysGenerated VARCHAR(MAX) 'isSysGenerated', IsSpecialBill_YN VARCHAR(MAX) 'IsSpecialBill_YN', WebRefNo VARCHAR(MAX) 'WebRefNo', IsFovApplied VARCHAR(MAX) 'IsFovApplied')

SELECT
	ISNULL(@Customercode, '') AS 'MSTID'
	,'Done' AS TranXaction
END
EXEC Usp_Update_Docno	'CUST'
						,''
						,'HQTR'
						,'C001'
EXEC USP_CustomerMaster_List
END_PROC:
END