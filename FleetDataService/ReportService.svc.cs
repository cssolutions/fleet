﻿using Fleet.Classes;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ReportService.svc or ReportService.svc.cs at the Solution Explorer and start debugging.
    public class ReportService : IReportService
    {

        GeneralFuncations GF = new GeneralFuncations();

        #region Report Setting

        #region Parameter Set


        public List<Master_ParameterSet_Details> USP_GETParameterDetails_ForSet(int Id)
        {
            string SQRY = "exec USP_GETParameterDetails_ForSet '" + Id.ToString() + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<Master_ParameterSet_Details> EnquiryList = DataRowToObject.CreateListFromTable<Master_ParameterSet_Details>(Dt1);
            return EnquiryList;

        }

        public List<Master_ParameterSet> Usp_GetAllParameterSet()
        {
            string SQRY = "exec Usp_GetAllParameterSet";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<Master_ParameterSet> ParameterSetList = DataRowToObject.CreateListFromTable<Master_ParameterSet>(Dt1);
            return ParameterSetList;
        }

        public DataTable AddEditParameterSet(int Id, string XMLHeader, string XMLDetails)
        {
            string SQRY = "exec Usp_Insert_Master_ParameterSet @ID='" + Id + "',@HeaderXML='" + XMLHeader + "',@DetailXML='" + XMLDetails + "'";
            DataTable Dt = GF.getdatetablefromQuery(SQRY);
            // return Dt.Rows[0][0].ToString();

            return Dt;
        }

        #endregion


        #region Add Report

        public DataTable GetParentMenu(string Id)
        {
            string SQRY = "exec USP_GetParentMenu '" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }



        #endregion


        #region Add Parameter

        public DataTable GetParenters()
        {
            string SQRY = "exec Usp_GetAllParameter";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion


        #endregion

        #region Required Methods

        public List<Customer> GetCustomerListFromSearch(string Serach, string SearchType)
        {
            string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + Serach + "','1','" + SearchType + "'  ";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<Customer> GetCustomerVendorList = DataRowToObject.CreateListFromTable<Customer>(Dt);

            return GetCustomerVendorList;
        }

        public class Customer
        {
            public string CUSTCD { get; set; }
            public string CUSTNM { get; set; }
        }

        #endregion

        #region Standard Report Data


        public List<Master_ParameterSet_Details> GETParameterDetails(int Id)
        {
            string SQRY = "exec USP_GETParameterDetails '" + Id.ToString() + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<Master_ParameterSet_Details> EnquiryList = DataRowToObject.CreateListFromTable<Master_ParameterSet_Details>(Dt1);
            return EnquiryList;

        }


        public List<Master_Parameter_Inputs> GetSubParameter(int Id)
        {
            string SQRY = "exec USP_GetInputParameters '" + Id.ToString() + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<Master_Parameter_Inputs> EnquiryList = DataRowToObject.CreateListFromTable<Master_Parameter_Inputs>(Dt1);
            return EnquiryList;

        }


        public CYGNUS_Master_Reports GetReportDetails(int Id)
        {
            string SQRY = "select * from CYGNUS_Master_Reports Where Id= '" + Id.ToString() + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<CYGNUS_Master_Reports> EnquiryList = DataRowToObject.CreateListFromTable<CYGNUS_Master_Reports>(Dt1);
            return EnquiryList.First();

        }

        public List<CYGNUS_Master_Reports> GetReportList(string ReportType, string ReportSubType,string UserName,int Type)
        {
            //string SQRY = "select * from CYGNUS_Master_Reports Where ReportType= '" + ReportType.ToString() + "' and  ReportSubType= '" + ReportSubType.ToString() + "'";
            string SQRY = "EXEC USP_GetUserWiseReportAccessList '" + UserName + "','" + ReportType + "','" + ReportSubType + "','" + Type + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<CYGNUS_Master_Reports> EnquiryList = DataRowToObject.CreateListFromTable<CYGNUS_Master_Reports>(Dt1);
            return EnquiryList;

        }


        public List<CYGNUS_Master_Reports_Parameters> GetReportParameters(int Id)
        {
            string SQRY = "select * from CYGNUS_Master_Reports_Parameters Where ReportId= '" + Id.ToString() + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<CYGNUS_Master_Reports_Parameters> EnquiryList = DataRowToObject.CreateListFromTable<CYGNUS_Master_Reports_Parameters>(Dt1);
            return EnquiryList;

        }


        public List<ReportField> GetReportFields(string SQRY)
        {
            //string SQRY = "exec   USP_GET_Report_Fields 1,'" + SubReportType + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<ReportField> EnquiryList = DataRowToObject.CreateListFromTable<ReportField>(Dt1);
            return EnquiryList;

        }


        public List<ReportParameterValue> GetDynamicParameterValue(string SQRY)
        {
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<ReportParameterValue> EnquiryList = DataRowToObject.CreateListFromTable<ReportParameterValue>(Dt1);
            return EnquiryList;

        }

        public void InsertReportHistory(string ReportId, string ParameterId, string ParameterValue, string EntryBy)
        {

            string SQRY = "INSERT INTO [dbo].[CYGNUS_Report_History] ([ReportId] ,[ParameterId],[ParameterValue],[EntryBy]) select '" + ReportId + "','" + ParameterId + "','" + ParameterValue + "','" + EntryBy + "' ";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);


        }



        public class ReportField
        {
            public string Value { get; set; }
            public string Text { get; set; }
            public string ReprotHeader { get; set; }
            public string SubReportType { get; set; }
            public string MatchedFiledTypeCode { get; set; }
            public bool IsSelected { get; set; }
        }


        public class ReportParameterValue
        {
            public string Value { get; set; }
            public string Text { get; set; }
        }

        #endregion
    }
}
