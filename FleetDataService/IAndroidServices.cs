﻿using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml.Serialization;
//using static FleetDataService.AndroidServices;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAndroidServices" in both code and config file together.
    [ServiceContract]
    public interface IAndroidServices
    {

        #region International RCPL Login

        [OperationContract]
        [WebInvoke(Method = "POST",
        ResponseFormat = WebMessageFormat.Json,
        RequestFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream RCPL_Login(Stream stream);

        #endregion

        #region Get POD APK Version

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string GetPODAPPVersion();

        #endregion

        #region CHECK GC VALIDATION FOR POD UPDATE

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream CheckGCValidationForPODUpdate(string GCNO);

        #endregion

        #region Get POD GC list

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream GC_POD_List(string BRCD, string UserName, string CompanyCode, string FromDate, string ToDate, string GCNo);

        #endregion

        #region GET GC FOR POD UPDATE
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream GetGCForPODUpdate(string GCNO, string ImageName);

        #endregion

        #region  POD Upload

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/POD_UploadImage")]
        //string POD_UploadImage(Stream fileContents);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/POD_UploadImage")]
        string POD_UploadImage(Stream fileContents);


        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/POD_UploadImage123")]
        string POD_UploadImage123(Stream fileContents);

        #endregion

        #region Tracking APi
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string TrackingWebsite_API_Json(string DOCKNO);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Xml, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string TrackingWebsite_API_Xml(string DOCKNO);
        #endregion

        #region POD_Image_Link
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        string POD_Image_Link(string DOCKNO);
        #endregion


        #region PASSWORD UPDATE
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream PasswordUpdate(string UserName, string NewPass);
        #endregion

        #region UndeliveredGC
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        //Stream UnDeliveredGC(string UserName);
        Stream UnDeliveredGC(string StartDate, string EndDate, string DOCKNO, string InvoiceNO, string UserName);
        #endregion

        #region POD Upload List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Get_PODUpload_Listing(string DockNo);

        #endregion

        #region Customer RelationShip(Pickup Request) LOGIN

        #region Customer Login

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream RCPL_Customer_Login(string UserName, string Pass);

        #endregion

        #region Get City List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream City_List();

        #endregion

        #region Get State List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream State_List();

        #endregion

        #region Get Pincode List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Pincode_List(string CityCode);

        #endregion

        #region PRQ Submit

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream PRQ_Submit_API(PickupRequest PRQ);

        #endregion

        #region Get PRQ List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream PRQ_List(string FromDate = null, string ToDate = null, string PRQNo = null, string CustomerCode = null, string Driver = null,
            string SuperVisor = null, string Status = null, string BRCD = null, string UserName = null, string CompanyCode = null,
            string IMEINo = null, string CustType = null);

        #endregion

        #region Add/Update New Token Service

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Token_Generation_API(NotiFication Notification);

        #endregion

        #region Driver Login

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream RCPL_Driver_Login(string UserId, string Password);

        #endregion

        #region Update PRQ Status

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        //Stream Update_PRQ_Status_API(PRQ_History PRQ_History);

        #endregion

        #region Update Vehicle Driver Assign PRQ

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream Vehicle_Driver_Assign_PRQ_API(PRQ_Vehicle_Driver_Assign OBJ);

        #endregion

        #region Get PRQ Status List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream PRQ_Status_List(string Status_Type);

        #endregion

        #region Customer Prefered Location Submit

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream CustomerPreferedLocation_Submit_API(CUST_PrefLocation PreferedLoc);

        #endregion

        #region Customer Prefered Location List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream PreferedLocation_List(string CustCode, string BRCD);

        #endregion

        #region Get Customer Address List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream CustAddress_List(string CustomerCode, string Brcd, string PreferedLocation);

        #endregion


        #region Assign Remove Driver Submit

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream PRQAssignRemoveDriver(AssignRemoveDriver AssignRemoveDriver);

        #endregion

        #region Complete PRQ Submit

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream CompletePRQSubmit(CompletePRQ CompletePRQ);

        #endregion

        #endregion


        #region Quick Docket API

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        Stream QuickDocketGeneration(QuickDocketViewModel QuickDocket);

        #endregion


        #region API Docket Tracking

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream DocketTracking(string DocketNo);

        #endregion

        #region Change Customer PassWord

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Updte_Cust_Pass_API(Stream Cust);

        #endregion

        #region Get DRS Docket List

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "DRS_DOCKET_List_API")]
        Stream DRS_DOCKET_List_API(Stream Cust);

        #endregion

        #region Submit DRS Delivered

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "DRS_DOCKET_Delivered_API")]
        Stream DRS_DOCKET_Delivered_API(Stream DRS_Delivered);

        #endregion

        #region Check Docket Status API

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "Check_Docket_Status_API")]
        Stream Check_Docket_Status_API(Stream Docket_st);

        #endregion

        #region Get Vehicle Type list

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Vehicle_Type_List();

        #endregion

        #region Get Branch Wise Vehicle Driver List

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Vehicle_Driver_List(string UserName, string PrqNo);

        #endregion

        #region Get Drive Assign PRQ

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream Driver_AssignPRQ_List(string DriverID, string UserName, string BRCD, string PRQNo);

        #endregion

        [OperationContract]
        void DoWork();

        #region Get Customer APP Version Check

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        Stream GetCustomerAPPVersion();

        #endregion

        
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped, UriTemplate = "/uploadImage")]
        string uploadImage(Stream fileContents);
    }

    #region Add/Update New Token Service

    [DataContract]
    public class NotiFication
    {
        [DataMember]
        public CYGNUS_Notification_Token Obj_NotiFication { get; set; }
    }

    #endregion

    #region Customer Prefered Location Submit

    [DataContract]
    public class CUST_PrefLocation
    {
        [DataMember]
        public CYGNUS_Customer_PreferedLocation Obj_CCPL { get; set; }
    }

    #endregion

    #region Update PRQ Status

    [DataContract]
    public class PRQ_History
    {
        [DataMember]
        public CYGNUS_PRQ_History Obj_PRQ_History { get; set; }
    }

    #endregion

    #region PRQ Submit

    [DataContract]
    public class PickupRequest
    {
        [DataMember]
        public CYGNUSPickupRequest Obj_CPR { get; set; }
    }

    #endregion

    #region Update Vehicle Driver Assign PRQ

    [DataContract]
    public class PRQ_Vehicle_Driver_Assign
    {
        [DataMember]
        public string VehicleNo { get; set; }

        [DataMember]
        public string AssignBy { get; set; }

        [DataMember]
        public string DriverID { get; set; }
        [DataMember]

        public string DriverName { get; set; }

        [DataMember]
        public string PRQNo { get; set; }
    }

    #endregion


    #region Assign Remove Driver Submit

    [DataContract]
    public class AssignRemoveDriver
    {
        [DataMember]
        public string DriverID { get; set; }

        [DataMember]
        public string VehicleNo { get; set; }

        [DataMember]
        public string UserName { get; set; }
        [DataMember]

        public string Brcd { get; set; }

        [DataMember]
        public string PRQNo { get; set; }

        [DataMember]
        public string Flag { get; set; }
    }

    #endregion


    #region Complete PRQ Submit

    [DataContract]
    public class CompletePRQ
    {
        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Brcd { get; set; }

        [DataMember]
        public string DriverId { get; set; }

        [DataMember]
        public string PrqNo { get; set; }

        [DataMember]
        public string ActualWeight { get; set; }

        [DataMember]
        public string ActualPKGS { get; set; }
    }

    #endregion


    #region Quick Docket Generation

    [DataContract]
    public class QuickDocketViewModel
    {
        [DataMember]    
        public string Address { get; set; }
        [DataMember]
        public string CustomerCode { get; set; }
        [DataMember]
        public string LandMark { get; set; }
        [DataMember]
        public string City { get; set; }
        [DataMember]
        public string Pincode { get; set; }
        [DataMember]
        public string FinStartYear { get; set; }
        [DataMember]
        public string FinEndYear { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string MobileNo { get; set; }
        //[DataMember]
        //public string VehicleType { get; set; }
        [DataMember]
        public decimal Weight { get; set; }
        [DataMember]
        public int PKGS { get; set; }
        [DataMember]
        public string ImeiNo { get; set; }
        [DataMember]
        public string OriginLat { get; set; }
        [DataMember]
        public string OriginLong { get; set; }
        [DataMember]
        public string Orgncd { get; set; }
        [DataMember]
        public string Remarks { get; set; }
        [DataMember]
        public string Destcd { get; set; }
    }

    #endregion
}
