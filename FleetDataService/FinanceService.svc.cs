﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using Fleet.ViewModels;
using CYGNUS.ViewModels;
using System.Xml;
using System.Data.SqlClient;
using CYGNUS.Controllers;
using Microsoft.ApplicationBlocks.Data;
using FleetDataService.ViewModels;


namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FinanceService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FinanceService.svc or FinanceService.svc.cs at the Solution Explorer and start debugging.
    public class FinanceService : IFinanceService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region Billing


        #region Bill Generation

        public List<Webx_Master_General> GetModeWiseStax()
        {
            string QueryString = "Select distinct CodeId=/*Trans_Type+'~'+*/convert(varchar,StaxRate),CodeDesc=CodeDesc +' With Stax '+convert(varchar,StaxRate)+' %'  from VW_ModeWise_TaxRate";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> itmList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return itmList;
        }

        public List<Webx_Master_General> GetModeWiseStaxwithType()
        {
            string QueryString = "Select distinct CodeId=Trans_Type+'~'+convert(varchar,StaxRate),CodeDesc=CodeDesc +' With Stax '+convert(varchar,StaxRate)+' %'  from VW_ModeWise_TaxRate";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> itmList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return itmList;
        }

        public List<BillDocket> GetBillDocket(BillingQueryModel BQM)
        {
            //START GST Changes By Chirag D

            string QueryString = "";
            if (BQM.IsDAMBill)
            {
                QueryString = "exec webx_UNI_DAM_BillGeneration_new '" + BQM.FromDate.ToString("dd MMM yyyy") + "','" + BQM.ToDate.ToString("dd MMM yyyy") + "','" + BQM.PartyType + "','" + BQM.BillingParty + "','" + BQM.CurLocation + "','" + BQM.Paybas + "','" + BQM.DocketNo + "','" + BQM.DeliveryDate.ToString("dd MMM yyyy") + "'";
            }
            else
            {

                //QueryString = "exec webx_UNI_BillGeneration_New '" + BQM.FromDate.ToString("dd MMM yyyy") + "','" + BQM.ToDate.ToString("dd MMM yyyy") + "','" + BQM.BillingParty + "','" + BQM.CurLocation + "','" + BQM.CurLocation + "','" + BQM.Paybas + "','" + BQM.Trans_Type + "','" + BQM.BusinessType + "','" + BQM.Bill_POD_Approval_YN + "','" + BQM.manual_dockno + "','" + BQM.StaxYN + "','" + BQM.StaxRate + "','" + BQM.GC.CustomerCode + "','" + BQM.GC.StateCode + "','" + BQM.GC.GSTType + "','" + BQM.GC.IsGSTApply + "','" + BQM.GC.DateType + "','" + BQM.GC.Service_Class + "' ";
                QueryString = "exec webx_UNI_BillGeneration_new '" + BQM.FromDate.ToString("dd MMM yyyy") + "','" + BQM.ToDate.ToString("dd MMM yyyy") + "','" + BQM.BillingParty + "','" + BQM.CurLocation + "','" + BQM.CurLocation + "','" + BQM.Paybas + "','" + BQM.Trans_Type + "','" + BQM.BusinessType + "','" + BQM.Bill_POD_Approval_YN + "','" + BQM.DocketNo + "','" + BQM.StaxYN + "','" + BQM.StaxRate + "','" + BQM.GC.CustomerCode + "','" + BQM.GC.StateCode + "','" + BQM.GC.GSTType + "','" + BQM.GC.IsGSTApply + "','" + BQM.GC.DateType + "' ";

            }
            //END GST Changes By Chirag D

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillDocket> itmList = DataRowToObject.CreateListFromTable<BillDocket>(DT);
            return itmList;
        }

        public String CheckDocumentNoForSuppBill(string docNo,string docType,string finyear)
        {
            string SQR = "exec Usp_CheckDocumentForSupplementryBill '"+ docType + "','"+ docNo + "','"+ finyear + "'";
            return GF.executeScalerQuery(SQR);
        }

        public String CheckDocumentNoForSuppBill(string dockno, string PartyCode, string GSTType, string StateCode)
        {
            string SQR = "SELECT CNT=Count(*) FROM WebX_Trans_Docket_Status A WITH(NOLOCK) INNER JOIN WebX_Master_Docket B WITH(NOLOCK) ON A.DOCKNO = B.DOCKNO AND A.DOCKSF = B.DOCKSF " +
                         "INNER JOIN WebX_Master_Docket_Charges C WITH(NOLOCK) ON B.DOCKNO = C.DOCKNO " +
                         "WHERE ISNULL(A.Cancelled,'N')='N' AND  A.DOCKNO = '" + dockno + "' AND B.PARTY_CODE = '" + PartyCode + "' AND C.GSTType = '" + GSTType + "' AND B.OriginStateCode='" + StateCode + "'";

            return GF.executeScalerQuery(SQR);
        }

        public Webx_Master_General CheckOctroiDocketNo(string dockno, string partytyp1, string partytype, string party, string FinYear, string CurrentBranchCode)
        {
            //string SQR = "exec USP_CheckDocketNo_For_OctroiBill '" + dockno + "','" + partytyp1 + "','" + party + "','" + partytype + "','" + FinYear + "','" + CurrentBranchCode + "'";
            //DataTable Dt = GF.GetDataTableFromSP(SQR);

            string SQRY_Status = "exec USP_Oct_Docket_Validation '" + dockno + "','" + partytyp1 + "','" + party + "','" + partytype + "'";
            int Id_Status = GF.SaveRequestServices(SQRY_Status.Replace("'", "''"), "CheckOctroiDocketN1", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY_Status);

            Webx_Master_General WMD = new Webx_Master_General();
            //WMD.CodeType = Dt.Rows[0][2].ToString();

            if (Dt.Rows[0][0].ToString() == "F")
            {
                WMD.CodeId = "F";
                WMD.CodeDesc = Dt.Rows[0][1].ToString();
            }
            else
            {
                string strRule = "select * from webx_modules_rules where Module_Name ='Prepare Octroi Bill' and Rule_Desc = 'For Octroi Bill - Booking Location Octroi Bill Generated'";
                DataTable dtRule = GF.GetDataTableFromSP(strRule);

                if (dtRule.Rows.Count > 0)
                {
                    if (dtRule.Rows[0]["RULE_Y_N"].ToString() == "Y")
                    {
                        string strLocation = " SELECT * FROM WEBX_MASTER_DOCKET WHERE DOCKNO ='" + dockno + "' AND ORGNCD = '" + CurrentBranchCode + "'";
                        DataTable dtLocation = GF.GetDataTableFromSP(strLocation);
                        if (dtLocation.Rows.Count > 0)
                        {
                            WMD.CodeId = "T";
                            WMD.CodeDesc = "";
                        }
                        else
                        {
                            WMD.CodeId = "F";
                            WMD.CodeDesc = "Invalid Location ";
                        }
                    }
                    else
                    {
                        string strLocation = " SELECT * FROM WEBX_MASTER_DOCKET WHERE DOCKNO ='" + dockno + "' ";
                        DataTable dtLocation = GF.GetDataTableFromSP(strLocation);
                        if (dtLocation.Rows.Count > 0)
                        {
                            WMD.CodeId = "T";
                            WMD.CodeDesc = "";
                        }
                        else
                        {
                            WMD.CodeId = "F";
                            WMD.CodeDesc = "Document Not Valid";
                        }
                    }

                }
                else
                {
                    WMD.CodeId = "F";
                    WMD.CodeDesc = "Document Not Valid";
                }
            }


            return WMD;
        }

        public List<BillCustDetails> GetBillCustDetails(string CustomerCode)
        {
            string QueryString = "exec Usp_Cust_Billdetails '" + CustomerCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillCustDetails> itmList = DataRowToObject.CreateListFromTable<BillCustDetails>(DT);
            return itmList;
        }

        public List<BillCustDetails> GetBillCustDetails(string CustomerCode, string Location)
        {
            string QueryString = "exec WebxNet_OutStd_Cust_billLoc '" + Location + "','" + CustomerCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillCustDetails> itmList = DataRowToObject.CreateListFromTable<BillCustDetails>(DT);

            if (itmList.Count == 0) 
                itmList.Add(new BillCustDetails());

            return itmList;
        }

        public DataTable BillSubmit(string BillLocation, string XmlBillMst, string XmlBillDet, string FinYear, string Businesstype, string BillType, bool IsGSTApply = false, string StateCode = null)
        {
            string QueryString = "";
            if (BillType == "2")
                QueryString = "exec Usp_Bill_Generate_NewPortal '" + BillLocation.Trim() + "','" + XmlBillMst.ReplaceSpecialCharacters() + "','" + XmlBillDet.ReplaceSpecialCharacters() + "','" + FinYear.Substring(2, 2).Trim() + "','" + Businesstype.Trim() + "'," + IsGSTApply + ",'" + StateCode + "'";
            else if (BillType == "13")
                QueryString = "exec Usp_Demurrage_Bill_Generate_NewPortal '" + BillLocation.Trim() + "','" + XmlBillMst.ReplaceSpecialCharacters() + "','" + XmlBillDet.ReplaceSpecialCharacters() + "','" + FinYear.Substring(2, 2).Trim() + "','" + Businesstype.Trim() + "'";
            else if (BillType == "OCT")
                QueryString = "exec usp_Prepare_OctroiBill_Generate_NewPortal '" + BillLocation.Trim() + "','" + XmlBillMst.ReplaceSpecialCharacters() + "','" + XmlBillDet.ReplaceSpecialCharacters() + "','" + FinYear.Substring(2, 2).Trim() + "','" + Businesstype.Trim() + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "BillSubmit_" + BillType, "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        public DataTable CustomerDetails(string PTMSCD)
        {
            string sql = "select custnm,custaddress,cstno,emailids from webx_custhdr WHERE CUSTCD = '" + PTMSCD + "'";
            DataTable DT = GF.GetDataTableFromSP(sql);
            return DT;
        }
        public DataTable InsertSupBillDetails(string Xml_MultiLrThc_Details, string Xml_Other_Details, SqlTransaction trans)
        {
            string sql = "";
            DataTable DT = new DataTable();
            string Billno = "";
            try
            {
                sql = "EXEC usp_Generate_SupplimentaryBill '" + Xml_MultiLrThc_Details.Replace("&", "&amp;").Trim() + "','" + Xml_Other_Details.Replace("&", "&amp;").Trim() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertSupplimentaryBill", "", "");
                DT = GF.GetDataTableFromSP(sql);
                Billno = DT.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                DataRow newRow = DT.NewRow();
                newRow["Billno"] = "0";
                newRow["Status"] = "NotDone";
                DT.Rows.Add(newRow);
            }
            return DT;
        }

        public DataTable InsertFleetIncomeBillDetails(string Xml_MultiLrThc_Details, string Xml_Other_Details)
        {
            string sql = "";
            DataTable DT = new DataTable();
            string Billno = "";
            try
            {
                sql = "EXEC usp_Generate_FleetIncomeBill '" + Xml_MultiLrThc_Details.Replace("&", "&amp;").Trim() + "','" + Xml_Other_Details.Replace("&", "&amp;").Trim() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertFleetIncomeBill", "", "");
                DT = GF.GetDataTableFromSP(sql);
                Billno = DT.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                DataRow newRow = DT.NewRow();
                newRow["Billno"] = "0";
                newRow["Status"] = "NotDone";
                DT.Rows.Add(newRow);
            }
            return DT;
        }

        #endregion

        #endregion

        #region CNote/Bill Finalization

        public DataTable GetDocketFinalizationDetails(FinalizationModel objFM, string type)
        {
            string QueryString = "EXEC USP_DOCUMENT_FOR_FINALIZATION_NewPortal '" + objFM.Type + "','" + GF.FormateDate(objFM.FromDate) + "','" + GF.FormateDate(objFM.ToDate) + "','" + objFM.PartyCode + "','" + objFM.DocumentNo + "','" + objFM.BaseFinYear + "','" + objFM.SelectionType + "','" + objFM.RegionCode + "','" + objFM.LocationCode + "','1'";
            if (type == "2")
            {
                QueryString = "EXEC USP_DOCUMENT_FOR_FINALIZATION_NewPortal_PreviousYear '" + objFM.Type + "','" + GF.FormateDate(objFM.FromDate) + "','" + GF.FormateDate(objFM.ToDate) + "','" + objFM.PartyCode + "','" + objFM.DocumentNo + "','" + objFM.BaseFinYear + "','" + objFM.SelectionType + "','" + objFM.RegionCode + "','" + objFM.LocationCode + "'";
            }
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GetDocketFinalizationDetails", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            //List<CNoteFinalizationViewModel> itmList = DataRowToObject.CreateListFromTable<CNoteFinalizationViewModel>(DT);
            return DT;
        }

        public DataTable CNoteFinalizationSubmit(string docketNo, string FinYear, DateTime Finalized_Date, string UserName = null, string Location = null)
        {
            string STR_Query = "EXEC USP_Docket_Finalization_MVCPortal '" + docketNo + "','" + FinYear + "','" + UserName + "','" + Location + "','" + Finalized_Date + "'";
            //old USP_Docket_Finalization_NewPortal
            //string Str = "UPDATE DBO.WebX_Trans_Docket_Status WITH(ROWLOCK) SET IsNewCP_Finalized=1,finalized ='Y',Finalized_Date=Getdate()";
            //Str = Str + " WHERE dockno='" + docketNo + "' AND docksf='.';";
            ////int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "CNoteFinalizationSubmit - UPDATE", "", "");
            ////DataTable DT = GF.GetDataTableFromSP(Str);
            //Str = Str + " EXEC USP_Docket_Finalization_NewPortal '" + docketNo + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(STR_Query.Replace("'", "''"), "CNoteFinalizationSubmit", "", "");
            DataTable DT = GF.GetDataTableFromSP(STR_Query);
            return DT;
        }

        public DataTable BillFinalizationSubmit(string FinalizedBill, string FinYear)
        {
            string Str = "EXEC USP_Bill_Finalization_MVCPortal '" + FinalizedBill + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "BillFinalizationSubmit", "", "");
            DataTable DT = GF.GetDataTableFromSP(Str);
            return DT;
        }
        public DataTable MRFinalizationSubmit(string FinalizedMR, string FinYear)
        {
            string Str = "EXEC USP_MR_Finalization '" + FinalizedMR + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "MRFinalizationSubmit", "", "");
            DataTable DT = GF.GetDataTableFromSP(Str);
            return DT;
        }


        #endregion

        #region Bill/BillEntry Adjustment

        public DataTable GetPaymentBillDetails(string BillNo, string BillType)
        {
            string QueryString = "EXEC USP_GetBillAdjustDetails '" + BillNo + "','" + BillType + "'";
            //int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GetPaymentBiilDetails", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable BillEntryAdjustmentSubmit(string Mst, string Det, string BeDet, string BillDet, string FinYear, string Entry_EditFlag, string Company_Code)
        {
            string QueryString = "EXEC Usp_Insert_AdjData '" + Mst + "','" + Det + "','" + BeDet + "','" + BillDet + "','" + FinYear + "','" + Entry_EditFlag + "','" + Company_Code + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "BillEntryAdjustmentSubmit", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            //List<CNoteFinalizationViewModel> itmList = DataRowToObject.CreateListFromTable<CNoteFinalizationViewModel>(DT);
            return DT;
        }

        #endregion

        #region AdjustedBillCancellation

        public List<BillEntryModel> GetAdjustedBillNoPaymentDetails(BillEntryAdjustmentViewModel model)
        {
            string QueryString = "EXEC USP_Get_Adjusted_Bill_Cancellation_Details_NewPortal '" + model.AdjustmentBillNo + "','BENO'";
            //int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GetPaymentBiilDetails", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillEntryModel> BillList = DataRowToObject.CreateListFromTable<BillEntryModel>(DT);
            return BillList;

        }
        public List<BillEntryModel> GetAdjustedBillNoIncomeDetails(BillEntryAdjustmentViewModel model)
        {
            string QueryString = "EXEC USP_Get_Adjusted_Bill_Cancellation_Details_NewPortal '" + model.AdjustmentBillNo + "','BILLNO'";
            //int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GetPaymentBiilDetails", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillEntryModel> BillList = DataRowToObject.CreateListFromTable<BillEntryModel>(DT);
            return BillList;

        }

        public DataTable AddBillAdjustmentCancellation(string AdjustmentBillNo, DateTime CancellationDate, string CancellationReason, string BaseUserName, string Finyear)
        {
            string QueryString = "EXEC USP_Adjusted_Bill_Cancellation_NewPortal '" + AdjustmentBillNo + "','" + CancellationDate + "','" + CancellationReason + "','" + BaseUserName + "','" + Finyear + "'";
            //int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GetPaymentBiilDetails", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        #endregion

        #region Income View Print

        public List<webx_BILLMST> Get_BillViewPrintList(string FromDate, string ToDate, string billno, string mbillno, string BillingParty, string billtype, string billstatus, string baselocationcode)
        {
            string QueryString = "EXEC USP_GetBillViewPrintList '" + FromDate + "','" + ToDate + "','" + billno + "','" + mbillno + "','" + BillingParty + "','" + billtype + "','" + billstatus + "','" + baselocationcode + "'";
            //string QueryString = "";
            //if ((billno != "") && (billno != null))
            //{
            //    QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,bill_cancel,BGENEMPCD,BILLSTATUS from webx_billmst where billno='" + billno + "'";
            //}
            //else if ((mbillno != "") && (mbillno != null))
            //{
            //    QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,bill_cancel,BGENEMPCD,BILLSTATUS from webx_billmst where manualbillno='" + mbillno + "' ";
            //}
            //else
            //{
            //    if ((BillingParty != "") && (BillingParty != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd in ('" + BillingParty + "') And paybas='" + billtype + "' and BILLSTATUS like '" + billstatus + "' And (convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //    else if ((billtype != "") && (billtype != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd like '%%' And paybas='" + billtype + "' and BILLSTATUS like '" + billstatus + "' And (convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //    else if ((BillingParty != "") && (BillingParty != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd in ('" + BillingParty + "') and paybas like '%' and BILLSTATUS like '" + billstatus + "' And (convert(varchar,BGNDT,106) between Convert(datetime,'" + ToDate + "',106) and convert(datetime,'" + FromDate + "',106))";
            //    }
            //    else if ((BillingParty != null) && (billtype != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd in ('" + BillingParty + "') And paybas='" + billtype + "' And ( convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //    else if ((BillingParty != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd in ('" + BillingParty + "') and paybas like '%' And ( convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";

            //    }
            //    else if ((billtype != null) && (billtype != ""))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd like '%%' And paybas='" + billtype + "' And ( convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //    else if ((billstatus != "") && (billstatus != null))
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd like '%%' and paybas like '%' and BILLSTATUS like '" + billstatus + "' And (convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //    else
            //    {
            //        QueryString = "select BILLNO,BBRCD,ptmscd,(ptmscd+': '+PTMSNM) as PTMSNM,PAYBAS,BILLAMT as BILAMT,BGNDT,BSBDT,BDUEDT,BCLDT,bcbsdt,ptmsbrcd,billsubbrcd,billcolbrcd,BILLSTATUS from webx_billmst where ptmscd like '%%' and paybas like '%' And ( convert(varchar,BGNDT,106) between Convert(datetime,'" + FromDate + "',106) and convert(datetime,'" + ToDate + "',106))";
            //    }
            //}
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_BILLMST> itmList = DataRowToObject.CreateListFromTable<webx_BILLMST>(DT);
            return itmList;
        }

        public List<WEBX_MR_VPLIST> Get_MRViewPrintList(string FromDate, string ToDate, string billno, string mbillno, string billtype, string BaseLocationCode, string BaseFinYear)
        {
            string QueryString = "exec WEBX_MR_vplist '" + FromDate + "','" + ToDate + "','" + billno + "','" + mbillno + "','','" + billtype + "','" + BaseLocationCode + "','" + BaseFinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<WEBX_MR_VPLIST> itmList = DataRowToObject.CreateListFromTable<WEBX_MR_VPLIST>(DT);
            return itmList;
        }

        public List<Webx_Master_General> GetSubBranchFromBranch(string Serach)
        {
            string SQRY = "select top 1 CodeId='All',CodeDesc='All' from webx_location union select loccode,LOCNAME=LocCode+' : '+LocName  from webx_location where Report_loc='" + Serach + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> ListName = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1);
            ListName = ListName.ToList();
            return ListName;
        }

        public List<vw_CreaditCnotViewList> Get_cnote_register(string FromDate, string ToDate, string dockno, string frombr, string tobr)
        {
            string QueryString = "exec usp_cnote_register '" + FromDate + "','" + ToDate + "','" + dockno + "','" + frombr + "','" + tobr + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_CreaditCnotViewList> itmList = DataRowToObject.CreateListFromTable<vw_CreaditCnotViewList>(DT);
            return itmList;
        }

        public List<vw_SupplimentryViewList> Get_SupplimentryViewPrintList(string FromDate, string ToDate, string billno, string mbillno, string BillingParty, string billtype, string billstatus)
        {
            string QueryString = "exec USP_Sup_Billing_Listing'" + FromDate + "','" + ToDate + "','" + billno + "','" + mbillno + "','" + BillingParty + "','" + billtype + "','" + billstatus + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_SupplimentryViewList> itmList = DataRowToObject.CreateListFromTable<vw_SupplimentryViewList>(DT);
            return itmList;
        }

        public List<AgentBillList> Get_Agent_Octroi_Bill_ReViewPrint_Data(string FromDate, string ToDate, string party, string billno)
        {
            string QueryString = "exec USP_Agent_Octroi_Bill_ReViewPrint_Data '" + FromDate + "','" + ToDate + "','" + party + "','" + billno + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<AgentBillList> itmList = DataRowToObject.CreateListFromTable<AgentBillList>(DT);
            return itmList;
        }

        public List<AgentBillModel> GetVendor_NameList(string searchTerm)
        {
            string SQRY = "SELECT VENDORCODE as PartyCode , VENDORNAME as PartyName  from  webx_VENDOR_HDR WHERE VENDORCODE like '%" + searchTerm + "%' OR VENDORNAME LIKE '%" + searchTerm + "%'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<AgentBillModel> GetDriver_NameList_XML = DataRowToObject.CreateListFromTable<AgentBillModel>(Dt);

            return GetDriver_NameList_XML;
        }

        public List<CustomerBillList> Get_Octroi_Bill_ReViewPrint_Data(string FromDate, string ToDate, string party, string billno)
        {
            string QueryString = "exec USP_Octroi_Bill_ReViewPrint_Data '" + FromDate + "','" + ToDate + "','" + party + "','" + billno + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CustomerBillList> itmList = DataRowToObject.CreateListFromTable<CustomerBillList>(DT);
            return itmList;
        }

        public List<AgentVoucherBillList> Get_AgentPaymentVoucher_ReViewPrint_Data(string FromDate, string ToDate, string party, string billno)
        {
            string QueryString = "exec USP_AgentPaymentVoucher_ReViewPrint_Data '" + FromDate + "','" + ToDate + "','" + party + "','" + billno + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<AgentVoucherBillList> itmList = DataRowToObject.CreateListFromTable<AgentVoucherBillList>(DT);
            return itmList;
        }

        #endregion

        #region Customer Contract
        public List<webx_custcontract_hdr> GetContractdetail(string CustomerCode)
        {
            string QueryString = "Select CONVERT(varchar,Contract_From_Dt,106) as Nstartdate,CONVERT(varchar,Contract_To_Dt,106) as Nenddate,(case when Contract_Based_On='FTL' then 'FTL Type wise' else 'Vehicle wise' end) as Contract_Based_On,ISNULL(Active_Flag,'N') AS activeflag,(CASE WHEN Active_Flag = 'Y' THEN 'Active' ELSE 'InActive' END) as [Status],*from [Webx_Fleet_Secondary_Contract_Hdr] where Customer_Code = '" + CustomerCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_custcontract_hdr> ContractList = DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(dataTable);
            return ContractList;
        }
        public List<webx_custcontract_hdr> GetPeopleContactdata(string CustomerCode, string ContractType)
        {

            string QueryString = "Select * from vw_GetPMContractData where Customer_Code = '" + CustomerCode + "' And Contract_Type='" + ContractType + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_custcontract_hdr> PMContractList = DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(dataTable);
            return PMContractList;
        }
        public DataTable CustomerChnageStatus(string ContractID, string Type, string ContType)
        {
            string QueryString = "";
            QueryString = "exec sp_Contract_ChangeStatus '" + ContractID + "','" + Type + "','" + ContType + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        public DataTable GeneratedContractID(string ContractCode, string CustomerCode, string ContractType, string EntryBy)
        {
            string QueryString = "sp_Insert_Webx_Custcontract_Hdr '" + ContractCode + "','" + CustomerCode + "','" + ContractType + "','" + EntryBy + "'";

            return GF.GetDataTableFromSP(QueryString);
        }
        public List<Webx_Fleet_Contract_People> GetPepoleData(string ContractID)
        {
            string QueryString = ("Select cp.* "
               + ",(Select Type_Name from webx_Vehicle_Type Where Type_Code=cp.Vehicle_Type) as Type_Name "
               + ",(Select CodeDesc from webx_master_General where CodeType='TSCAT' And StatusCode='Y' And CodeId = cp.Category_Id) As TsCategory"
               + ",(Select CodeDesc from webx_master_General where CodeType='TSDUR' And StatusCode='Y' And CodeId = cp.Duration_Id) As Duration"
               + ",(Select CodeDesc from webx_master_General where CodeType='PKDRP' And StatusCode='Y' And CodeId = cp.PickDrop_Id) As PickDropDesc"
               + ",(Select CodeDesc from webx_master_General where CodeType='FLTCAT' And StatusCode='Y' And CodeId = cp.Veh_Category_Id) As VehCategory"
               + " From Webx_Fleet_Contract_People as cp Where Contract_Code= '" + ContractID + "'");
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_Contract_People> PepoleList = DataRowToObject.CreateListFromTable<Webx_Fleet_Contract_People>(dataTable);
            return PepoleList;
        }
        public List<Webx_Master_General> GetTripRuteLits()
        {
            string QueryString = " Select RUTCD,RUTDESC From [webx_trip_rutmas] Where ActiveFlag='Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> TripRuteLits = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return TripRuteLits;
        }
        public DataTable UpdateCustPepople(int ID, string CoustomerCode, string ContractId, string conractType, string XML, string BaseUserName, string BranchCode, string Companycode)
        {
            string sqlstr = "exec NEW_SP_INSERT_Webx_Fleet_Contract_People '" + ID + "','" + CoustomerCode + "','" + ContractId + "','" + conractType + "','" + XML + "','" + BaseUserName + "','" + BranchCode + "','" + Companycode + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);
            return Dt;
        }
        public DataTable InsertUpdateFleetSecondrt(string CoustomerCode, string ContractId, string XMLContract, string XML, string BaseUserName)
        {
            if (XML == "")
                XML = "<root></root>";
            string sqlstr = "exec NEW_SP_Insert_Webx_Fleet_Secondary_Contract_Hdr '" + CoustomerCode + "','" + ContractId + "','" + XMLContract + "','" + XML + "','" + BaseUserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            return Dt;
        }
        public DataTable InsertUpdateFleetSecondrtDetail(string CoustomerCode, string ContractId, string ContractDetCode, string XML, string BaseUserName)
        {
            string sqlstr = "exec NEW_SP_Insert_Detail_Webx_Fleet_Secondary_Contract_Hdr '" + CoustomerCode + "','" + ContractId + "','" + ContractDetCode + "','" + XML + "','" + BaseUserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            return Dt;
        }
        public DataTable InsertUpdateFleetDetailSecondrt(string CoustomerCode, string ContractId, string XMLDetail, string BaseUserName)
        {
            string sqlstr = "exec NEW_SP_Insert_Webx_Fleet_Secondary_Contract_Det '" + CoustomerCode + "','" + XMLDetail + "','" + BaseUserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            return Dt;
        }
        public List<Webx_Master_General> GetHourly_BasedList(string ContractID)
        {
            string QueryString = "Select Id,HourlyBasedSlot From [Webx_Fleet_Hourly_Based_Slot]";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> ContractList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return ContractList;
        }
        public List<Webx_Fleet_Secondary_Contract_Hdr> GetSecondryContractList(string ContractID)
        {

            string QueryString = "select LocCode,Contract_Det_Code,Vehicle_No,Vehicle_Type_Code,Category,HourlyBasedSlot_Id,From_KM,To_KM,Fixed_Cost,Variable_Cost,Fixed_Cost_KM from Webx_Fleet_Secondary_Contract_Det  Where Contract_Code= '" + ContractID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_Secondary_Contract_Hdr> SecondryContractList = DataRowToObject.CreateListFromTable<Webx_Fleet_Secondary_Contract_Hdr>(dataTable);
            return SecondryContractList;
        }
        public DataTable GetListAccountTree()
        {
            string QueryString = "sp_tree_account";
            DataTable dt = GF.GetDataTableFromSP(QueryString);
            return dt;
        }
        public List<Webx_Fleet_Secondary_Contract_Hdr> GetSingleSecondryObj(string ContractID)
        {

            string QueryString = "Select *From [Webx_Fleet_Secondary_Contract_Hdr] Where [Contract_Code] ='" + ContractID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_Secondary_Contract_Hdr> GetSingleSecondryList = DataRowToObject.CreateListFromTable<Webx_Fleet_Secondary_Contract_Hdr>(dataTable);
            return GetSingleSecondryList;

        }

        public DataTable UpdateCustomerCode(string CustId, string CoustomerCode, string CustType, string XML, string EntryBy)
        {
            string sqlstr = "exec Usp_Finance_Update_webx_custcontract_hdr '" + CustId + "','" + CoustomerCode + "','" + CustType + "','" + XML + "','" + EntryBy + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            return Dt;
        }
        public List<webx_custcontract_hdr> GetContractInfromation(string contarctId)
        {

            string sqlstr = "SELECT convert(varchar,contract_stdate,103) AS contract_stdate,convert(varchar,contract_eddate,103) AS contract_eddate,convert(varchar,contract_effectdate,103) AS contract_effectdate,* FROM webx_custcontract_hdr WHERE contractid='" + contarctId + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);
            List<webx_custcontract_hdr> GetcontarctList = DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(Dt);
            return GetcontarctList;
        }
        #endregion

        #region SeetingCustomerVendor
        public List<WEBX_FLEET_DRIVERMST> GetDriverList()
        {
            string QueryString = "select * from WEBX_FLEET_DRIVERMST";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_DRIVERMST> ContractList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(dataTable);
            return ContractList;
        }
        public List<EmpVenList> GetAccountInfoList()
        {
            string QueryString = "select  acccode,accdesc from webx_acctinfo where acccode in ('cda0001','cda0002') order by acccode";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<EmpVenList> AccountInfoList = DataRowToObject.CreateListFromTable<EmpVenList>(dataTable);
            return AccountInfoList;
        }
        public List<EmpVenList> SeetingVendorList()
        {
            string QueryString = "select a.codeid+'~'+accthead+'~'+(select accdesc from webx_acctinfo where acccode=b.accthead) as acccode,a.codedesc as accdesc from webx_master_general a ,webx_accthead b where a.codetype='VENDTY' and a.codeid=b.codeid and accthead is  not null";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<EmpVenList> SeetingVendorList = DataRowToObject.CreateListFromTable<EmpVenList>(dataTable);
            return SeetingVendorList;
        }
        public List<EmpVenList> SeetingDriverList()
        {
            string QueryString = "select  acccode,accdesc from webx_acctinfo where acccode in ('LIA0057') order by acccode";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<EmpVenList> SeetingDriverList = DataRowToObject.CreateListFromTable<EmpVenList>(dataTable);
            return SeetingDriverList;
        }
        public List<EmpVenList> SeetingEmployeeList()
        {
            string QueryString = "select  acccode,accdesc from webx_acctinfo where acccode in ('ASS0064','IMP0001','CA00016','CLO0005') order by accdesc";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<EmpVenList> SeetingEmployeeList = DataRowToObject.CreateListFromTable<EmpVenList>(dataTable);
            return SeetingEmployeeList;
        }
        public List<PartyOpeningBalance> SettingPartyDetails(string Type, string EffectAccount, string selectCode, string Desc, string location, string BaseYearVal, string tbl2, string Vtype)
        {
            DataTable dt = new DataTable();
            if (Type == "C")
            {
                string QueryString = "select   '" + EffectAccount + "'+':'+'" + Desc + "' as acctval, d.custcd +':'+ d.custnm as custval,d.custcd as custcode ,d.custnm as custname,isnull(m.opendebit,0) as opendebit,isnull(m.opencredit,0)  as opencredit from webx_custhdr d   left outer join " + tbl2 + " m  on  d.custcd=m.empcd   and     acccode='" + EffectAccount + "' and loccode='" + location + "' where      PATINDEx ('%" + location + "%',custloc)>0 and d.custcd='" + selectCode + "'";
                dt = GF.GetDataTableFromSP(QueryString);
            }
            if (Type == "V")
            {
                string QueryString = "select   '" + EffectAccount + "'+':'+'" + Desc + "' as acctval,d.vendorcode as VendorCode,d.vendorcode+':'+d.vendorName as Vendval,Cast(Cast(isnull(m.opendebit,0) As Money) as varchar) as Opendebit,Cast(Cast(isnull(m.opencredit,0) As Money) as varchar) as OpenCredit from  webx_vendor_hdr d    left outer join  " + tbl2 + " m   on d.vendorcode=m.empcd  and loccode='" + location + "' inner join  webx_vendor_det c on d.vendorcode=c.vendorcode where  1=1 and d.vendorcode= '" + selectCode + "'   and  PATINDEx ('%" + location + "%',vendorbrcd)>0  and vendor_type='" + Vtype + "'order by vendorName ";
                dt = GF.GetDataTableFromSP(QueryString);
            }
            if (Type == "D")
            {
                //DRV_account as drpeffect id ,
                string QueryString = "select   '" + EffectAccount + "'+':'+'" + Desc + "' as acctval, convert(varchar,d.driver_id,106) +':'+ d.driver_name as drvval, convert(varchar,d.driver_id,106) as driver_id , d.driver_name as driver_name,isnull(m.opendebit,0) as opendebit,isnull(m.opencredit,0)  as opencredit from WEBX_FLEET_DRIVERMST d  left outer join  " + tbl2 + " m on convert(varchar,d.driver_id,106)=m.empcd       and acccode='" + EffectAccount + "' and loccode='" + location + "' where   PATINDEx ('%" + location + "%',driver_location)>0  " + selectCode + "";
                dt = GF.GetDataTableFromSP(QueryString);
            }
            if (Type == "E")
            {
                string QueryString = "select   '" + EffectAccount + "'+':'+'" + Desc + "' as acctval, convert(varchar,d.Userid,106) +':'+ d.Name as drvval, convert(varchar,d.Userid,106) as Userid , d.name as Empname,isnull(m.opendebit,0) as opendebit,isnull(m.opencredit,0)  as opencredit from webx_master_users d  left outer join  " + tbl2 + " m on convert(varchar,d.Userid,106)=m.empcd   and acccode='" + EffectAccount + "' and loccode='" + location + "' where  1=1 " + selectCode + "";
                dt = GF.GetDataTableFromSP(QueryString);
            }
            List<PartyOpeningBalance> SeetingEmployeeList = DataRowToObject.CreateListFromTable<PartyOpeningBalance>(dt);
            return SeetingEmployeeList;
        }
        //public DataTable InsertUpdateBalance(string Type, string account, string code, string loc, string tbl, decimal debit, decimal creadit, string finyear)
        //{
        //    string Acccode = "";
        //    decimal opendebit_i = 0;
        //    decimal opencredit_i = 0;
        //    DataTable dt = new DataTable();
        //    if (Type == "C")
        //    {
        //        string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tbl + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
        //        dt = GF.GetDataTableFromSP(sql_ind);

        //        Acccode = dt.Rows[0]["acccode"].ToString();
        //        opendebit_i = Convert.ToDecimal(dt.Rows[0]["opendebit_i"]);
        //        opencredit_i = Convert.ToDecimal(dt.Rows[0]["opencredit_i"]);

        //        if (opendebit_i > opencredit_i)
        //        {
        //            opendebit_i = opendebit_i - opencredit_i;
        //            opencredit_i = 0;
        //        }
        //        else
        //        {
        //            opencredit_i = opencredit_i - opendebit_i;
        //            opendebit_i = 0;
        //        }

        //        string sqlUpdate = "delete from  " + tbl + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //        sqlUpdate = "Insert into  " + tbl + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //    }
        //    if (Type == "V")
        //    {
        //        string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tbl + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
        //        dt = GF.GetDataTableFromSP(sql_ind);

        //        Acccode = dt.Rows[0]["acccode"].ToString();
        //        opendebit_i = Convert.ToDecimal(dt.Rows[0]["opendebit_i"]);
        //        opencredit_i = Convert.ToDecimal(dt.Rows[0]["opencredit_i"]);

        //        if (opendebit_i > opencredit_i)
        //        {
        //            opendebit_i = opendebit_i - opencredit_i;
        //            opencredit_i = 0;
        //        }
        //        else
        //        {
        //            opencredit_i = opencredit_i - opendebit_i;
        //            opendebit_i = 0;
        //        }

        //        string sqlUpdate = "delete from  " + tbl + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //        sqlUpdate = "Insert into  " + tbl + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //    }
        //    if (Type == "E")
        //    {
        //        string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tbl + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
        //        dt = GF.GetDataTableFromSP(sql_ind);

        //        Acccode = dt.Rows[0]["acccode"].ToString();
        //        opendebit_i = Convert.ToDecimal(dt.Rows[0]["opendebit_i"]);
        //        opencredit_i = Convert.ToDecimal(dt.Rows[0]["opencredit_i"]);

        //        if (opendebit_i > opencredit_i)
        //        {
        //            opendebit_i = opendebit_i - opencredit_i;
        //            opencredit_i = 0;
        //        }
        //        else
        //        {
        //            opencredit_i = opencredit_i - opendebit_i;
        //            opendebit_i = 0;
        //        }

        //        string sqlUpdate = "delete from  " + tbl + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //        sqlUpdate = "Insert into  " + tbl + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //    }
        //    else
        //    {
        //        string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tbl + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
        //        dt = GF.GetDataTableFromSP(sql_ind);

        //        Acccode = dt.Rows[0]["acccode"].ToString();
        //        opendebit_i = Convert.ToDecimal(dt.Rows[0]["opendebit_i"]);
        //        opencredit_i = Convert.ToDecimal(dt.Rows[0]["opencredit_i"]);

        //        if (opendebit_i > opencredit_i)
        //        {
        //            opendebit_i = opendebit_i - opencredit_i;
        //            opencredit_i = 0;
        //        }
        //        else
        //        {
        //            opencredit_i = opencredit_i - opendebit_i;
        //            opendebit_i = 0;
        //        }

        //        string sqlUpdate = "delete from  " + tbl + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //        sqlUpdate = "Insert into  " + tbl + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
        //        dt = GF.GetDataTableFromSP(sqlUpdate);

        //    }
        //    return dt;
        //}

        public DataTable SetPartyOpeningBalance(string debit, string creadit, string Type, string account, string code, string loc, string UserName, string companycode, string finyear)
        {
            DataTable dt = new DataTable();
            string tblStr3 = "webx_acctopening_" + finyear;
            string tblStr2 = "webx_acctopening_employee_" + finyear;
            if (Type == "C")
            {
                string sql_insert = "delete from " + tblStr3 + " where Acccode='" + account + "' and Empcd='" + code + "' and Loccode='" + loc + "'";
                dt = GF.GetDataTableFromSP(sql_insert);

                sql_insert = "insert into " + tblStr3 + "(Acccode,Opendebit,Opencredit,Empcd,Loccode,COMPANY_CODE,Entryby,Entrydt) values('" + account + "'," + debit + "," + creadit + ",'" + code + "','" + loc + "','" + companycode + "','" + UserName + "',getdate())";
                dt = GF.GetDataTableFromSP(sql_insert);

                string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tblStr3 + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
                dt = GF.GetDataTableFromSP(sql_ind);

                string Acccode = Convert.ToString(dt.Rows[0]["acccode"]);
                double opendebit_i = Convert.ToDouble(dt.Rows[0]["opendebit_i"]);
                double opencredit_i = Convert.ToDouble(dt.Rows[0]["opencredit_i"]);

                if (opendebit_i > opencredit_i)
                {
                    opendebit_i = opendebit_i - opencredit_i;
                    opencredit_i = 0;
                }
                else
                {
                    opencredit_i = opencredit_i - opendebit_i;
                    opendebit_i = 0;
                }

                string sqlUpdate = "delete from  " + tblStr3 + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
                dt = GF.GetDataTableFromSP(sqlUpdate);

                sqlUpdate = "Insert into  " + tblStr3 + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
                dt = GF.GetDataTableFromSP(sqlUpdate);
            }
            else if (Type == "V")
            {
                string sql_insert = "delete from " + tblStr2 + " where Acccode='" + account + "' and Empcd='" + code + "' and Loccode='" + loc + "'";
                //dt = GF.GetDataTableFromSP(sql_insert);

                sql_insert = "insert into " + tblStr2 + "(Acccode,Opendebit,Opencredit,Empcd,Loccode,COMPANY_CODE,Entryby,Entrydt) values('" + account + "'," + debit + "," + creadit + ",'" + code + "','" + loc + "','" + companycode + "','" + UserName + "',getdate())";
                dt = GF.GetDataTableFromSP(sql_insert);

                string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tblStr2 + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
                dt = GF.GetDataTableFromSP(sql_ind);

                string Acccode = Convert.ToString(dt.Rows[0]["acccode"]);
                double opendebit_i = Convert.ToDouble(dt.Rows[0]["opendebit_i"]);
                double opencredit_i = Convert.ToDouble(dt.Rows[0]["opencredit_i"]);

                if (opendebit_i > opencredit_i)
                {
                    opendebit_i = opendebit_i - opencredit_i;
                    opencredit_i = 0;
                }
                else
                {
                    opencredit_i = opencredit_i - opendebit_i;
                    opendebit_i = 0;
                }

                string sqlUpdate = "delete from  " + sql_insert + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
                dt = GF.GetDataTableFromSP(sqlUpdate);

                sqlUpdate = "Insert into  " + sql_insert + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
                dt = GF.GetDataTableFromSP(sqlUpdate);

            }

            else if (Type == "D")
            {
                string sql_insert = "delete from " + tblStr3 + " where Acccode='" + account + "' and Empcd='" + code + "' and Loccode='" + loc + "'";
                dt = GF.GetDataTableFromSP(sql_insert);

                sql_insert = "insert into " + tblStr3 + "(Acccode,Opendebit,Opencredit,Empcd,Loccode,COMPANY_CODE,Entryby,Entrydt) values('" + account + "'," + debit + "," + creadit + ",'" + code + "','" + loc + "','" + companycode + "','" + UserName + "',getdate())";
                dt = GF.GetDataTableFromSP(sql_insert);

                string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tblStr3 + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
                dt = GF.GetDataTableFromSP(sql_ind);

                string Acccode = Convert.ToString(dt.Rows[0]["acccode"]);
                double opendebit_i = Convert.ToDouble(dt.Rows[0]["opendebit_i"]);
                double opencredit_i = Convert.ToDouble(dt.Rows[0]["opencredit_i"]);

                if (opendebit_i > opencredit_i)
                {
                    opendebit_i = opendebit_i - opencredit_i;
                    opencredit_i = 0;
                }
                else
                {
                    opencredit_i = opencredit_i - opendebit_i;
                    opendebit_i = 0;
                }

                string sqlUpdate = "delete from  " + tblStr3 + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
                dt = GF.GetDataTableFromSP(sqlUpdate);

                sqlUpdate = "Insert into  " + tblStr3 + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
                dt = GF.GetDataTableFromSP(sqlUpdate);
            }

            else
            {
                string sql_insert = "delete from " + tblStr2 + " where Acccode='" + account + "' and Empcd='" + code + "' and Loccode='" + loc + "'";
                dt = GF.GetDataTableFromSP(sql_insert);

                sql_insert = "insert into " + tblStr2 + "(Acccode,Opendebit,Opencredit,Empcd,Loccode,COMPANY_CODE,Entryby,Entrydt) values('" + account + "'," + debit + "," + creadit + ",'" + code + "','" + loc + "','" + companycode + "','" + UserName + "',getdate())";
                dt = GF.GetDataTableFromSP(sql_insert);

                string sql_ind = "select '" + account + "' as acccode, sum(a.opendebit) as opendebit_i ,sum(a.Opencredit)as opencredit_i  from " + tblStr2 + " a with (NOLOCK) ,webx_custhdr b  with (NOLOCK) where a.empcd=b.custcd  and loccode='" + loc + "' and acccode='" + account + "'";
                dt = GF.GetDataTableFromSP(sql_ind);

                string Acccode = Convert.ToString(dt.Rows[0]["acccode"]);
                double opendebit_i = Convert.ToDouble(dt.Rows[0]["opendebit_i"]);
                double opencredit_i = Convert.ToDouble(dt.Rows[0]["opencredit_i"]);

                if (opendebit_i > opencredit_i)
                {
                    opendebit_i = opendebit_i - opencredit_i;
                    opencredit_i = 0;
                }
                else
                {
                    opencredit_i = opencredit_i - opendebit_i;
                    opendebit_i = 0;
                }

                string sqlUpdate = "delete from  " + sql_insert + "  where brcd='" + loc + "' and acccode='" + Acccode + "' ";
                dt = GF.GetDataTableFromSP(sqlUpdate);

                sqlUpdate = "Insert into  " + sql_insert + "  (opendebit,opencredit,finyear,acccode,brcd,opendebit_i,opencredit_i,FinStartDate)   values(0,0,'" + finyear + "','" + Acccode + "','" + loc + "','" + opendebit_i + "','" + opencredit_i + "',NULL)  ";
                dt = GF.GetDataTableFromSP(sqlUpdate);
            }

            return dt;
        }
        #endregion

        #region Credit Note Generation

        public List<webx_BILLMST> GetCreditNoteGeneration(CNoteFilterViewModel CNFVM, string LocCode, string CmpCode)
        {
            string QueryString = "exec USP_BILLSFOR_CREDITNOTE_NewPortal '" + GF.FormateDate(CNFVM.FromDate) + "','" + GF.FormateDate(CNFVM.ToDate) + "','" + LocCode + "','" + CNFVM.BillNo + "','" + CNFVM.CUSTCD + "','" + CNFVM.Paybasis + "','" + CNFVM.TransMode + "','Y','" + (CNFVM.STax == null ? CNFVM.STax = "0" : CNFVM.STax) + "','" + CmpCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_BILLMST> itmList = DataRowToObject.CreateListFromTable<webx_BILLMST>(DT);
            return itmList;
        }

        public DataTable AddCreditNoteGeneration(string CNoteHdr, string CNoteDet)
        {
            string QueryString = "exec usp_CNote_Generation_Ver1 '" + CNoteHdr + "','" + CNoteDet + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable GetDoneTable(string Id, string TblName)
        {
            string QueryString2 = "";//, QueryString = "";
            //QueryString = "exec Usp_Show_Doc_Accounting '" + Id + "','" + TblName + "'";
            //DataTable DT = GF.GetDataTableFromSP(QueryString);
            //string Value = "";
            //try
            //{
            //    Value = DT.Rows[0][0].ToString();
            //}
            //catch (Exception)
            //{
            //    Value = "";
            //}
            //QueryString = Value;
            //DataTable DT1 = GF.GetDataTableFromSP(QueryString);
            QueryString2 = "select * from " + TblName + " where docno='" + Id + "'";
            return GF.GetDataTableFromSP(QueryString2);
        }
        #endregion

        #region Debit Note

        public List<webx_acctinfo> GetAcctInfo()
        {
            var Querystring = "SELECT * FROM webx_acctinfo";
            DataTable Dt = GF.GetDataTableFromSP(Querystring);
            List<webx_acctinfo> itmList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt);
            return itmList;
        }

        public DataTable GetDebitorName(string CustomerCode, string VENCD)
        {
            DataTable dt = new DataTable();
            string strsql = "";
            if (CustomerCode != "null")
            {
                strsql = "select top 1 Acccode,DrAccount=Acccode+' : '+Accdesc from WEBX_ACCTINFO where acccode='CDA0001'";
            }
            else
            {
                strsql = "select top 1 b.Acccode,DrAccount=b.Acccode+' : '+b.Accdesc from webx_AcctHead a inner join WEBX_ACCTINFO b on a.accthead=b.Acccode where CodeId=(select Vendor_Type from webx_VENDOR_HDR where VENDORCODE='" + VENCD + "')";
            }
            try
            {
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetName(string CustomerCode, string VENCD)
        {
            DataTable dt = new DataTable();
            string strsql = "";
            try
            {
                if (CustomerCode != "null")
                {
                    strsql = "SELECT custnm AS Name FROM webx_custhdr ";
                    strsql = strsql + " WHERE custcd='" + CustomerCode + "'";
                }
                else
                {
                    strsql = "SELECT VENDORNAME AS Name FROM webx_VENDOR_HDR ";
                    strsql = strsql + " WHERE VENDORCODE='" + VENCD + "'";
                }
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable AddDebitNoteDetail(string DetXml, string HdrXml)
        {
            var QueryString = "EXEC usp_Generate_DirectDebitNote '" + DetXml + "','" + HdrXml + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region ServiceWise Billing

        public List<webx_CUSTHDR> GetCustomerListByService(string Service)
        {
            string QueryString = "SELECT CUSTNM,CUSTCD FROM webx_custhdr Where custcd IN (SELECT CustCD FROM Webx_Cust_Service_Mapping WHERE Service_Id='" + Service + "' AND ISNULL(Active,'N')='Y') ORDER BY CUSTNM";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> ItemList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return ItemList;

        }

        #endregion

        public DataTable Insert_IDT_Payment_Data(string Xml_PAY_MST_IDT, string Xml_PAY_IDT, string BaseLocationCode, string BaseUserName, string BaseFinYear)
        {
            string QueryString = "EXEC Usp_IDTFreightMemo_Payment'" + Xml_PAY_MST_IDT + "','" + Xml_PAY_IDT + "','" + BaseLocationCode + "','" + BaseUserName + "','" + BaseFinYear + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "IDTFreightMemoPayment", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #region View / Print Crossing Challan View List

        public DataTable GetCrossingChallanViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetCrossingChallanViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion
        #region View / Print Crossing Voucher View List

        public DataTable GetCrossingVoucherViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetCrossingVoucherViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion
        #region View / Print IDT View List

        public DataTable GetIDTViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetIDTViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        public DataTable GetFreightMemoViewList(IDTFreightMemoCriteria VM, string Location)
        {
            string SQR = "exec Usp_GetFreightMemoViewList '" + VM.FMNO + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion
        #region View / Print Gatepass reprint View List

        public DataTable GetGatePassReprintViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetGatePassReprintViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        public DataTable GetGatePassReprintDKTWiseViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetGatePassReprintDKTwiaeViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion

        #region View / Print Cnote reprint View List

        public DataTable GetCnoteReprintViewList(CrossingChallanFilterViewModel VM, string Location)
        {
            string SQR = "exec Usp_GetGetCnoteReprintViewList '" + VM.DockNo + "','" + VM.FromDate.ToString("dd MMM yyyy") + "','" + VM.ToDate.ToString("dd MMM yyyy") + "','" + Location + "'";
            return GF.GetDataTableFromSP(SQR);
        }


        #endregion

        #region Buget Module On Service
        public List<webx_acctinfo> LegdetCode()
        {
            string QueryString = "EXEC sp_Ledget_work";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_acctinfo> List = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return List;
        }
        public List<webx_AcctBudget> BudgetYearList()
        {
            string QueryString = "EXEC usp_Get_Finacial_Years_New";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_AcctBudget> List = DataRowToObject.CreateListFromTable<webx_AcctBudget>(DT);
            return List;
        }
        public List<webx_AcctBudget> SerchResultsLedger(string BudgetType, string acccode, string Accountcategory, string brcd, string YearVal, string EntryType, string YearType, string RO, string LO)
        {
            string QueryString = "EXEC usp_IncUp_BudgetDetails_New_Portal " + "'" + BudgetType + "', " + "'" + acccode + "', " + "'" + Accountcategory + "', " + "'" + brcd + "', " + "'" + YearVal + "', " + "'" + EntryType + "', " + "'" + YearType + "', " + "'" + RO + "', " + "'" + LO + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_AcctBudget> List = DataRowToObject.CreateListFromTable<webx_AcctBudget>(DT);
            return List;
        }

        #endregion

        #region Crossing Challan Bill

        public List<webx_CrossingBill_DET> CrossingChallanBillList(string FromDate, string ToDate, string Vendor)
        {
            string SQL = "exec USP_CrossingBillList_NewPortal '" + FromDate + "','" + ToDate + "','" + Vendor + "'";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            List<webx_CrossingBill_DET> LST = DataRowToObject.CreateListFromTable<webx_CrossingBill_DET>(DT);
            return LST;
        }

        public DataTable CrossingChallanOpeningBalance1(string VendorCode, string FromDate, string ToDate)
        {
            string SQL = "exec Usp_GetCrossingChallanOpeningBill";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            return DT;
        }
        public DataTable CrossingChallanOpeningBalance(string FromDate, string ToDate, string Vendor, string BaseYearVal, string Accountledger, string basecomapnycode, string BaselocationName)
        {
            string SQL = "exec Usp_GetCrossingChallanOpeningBill '" + FromDate + "','" + ToDate + "','" + Vendor + "','" + BaseYearVal + "','" + Accountledger + "','" + basecomapnycode + "','" + BaselocationName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            return DT;
        }
        public DataTable Insert_CrossingChallan_Bill_Data(string Xml_BILL_Det, string Xml_BILL_Mst, string BaseLocationCode, string BaseUserName, string BaseFinYear, string BaseCompanyCode)
        {
            string QueryString = "EXEC USP_CrossingChallan_Bill_Submit_NewPortal'" + Xml_BILL_Det + "','" + Xml_BILL_Mst + "','" + BaseLocationCode + "','" + BaseUserName + "','" + BaseFinYear + "','" + BaseCompanyCode + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "Insert_CrossingChallan_Bill_Data", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable GetCrossingChallanDet(string vendorcode)
        {
            string sql = "select GSTNO as GSTNo,'' as Email,''as Mobileno,Default_Addr as Address, VENDORNAME as VendorName  from webx_VENDOR_HDR where 1=1 and isnull(active,'N')='Y' and VENDORCODE='" + vendorcode + "'  ";
            DataTable DT = GF.GetDataTableFromSP(sql);
            return DT;
        }
        #endregion
    }
}
