﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using CYGNUS.Models;
using Fleet.ViewModels;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DocumentService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DocumentService.svc or DocumentService.svc.cs at the Solution Explorer and start debugging.
    public class DocumentService : IDocumentService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
        public void DoWork()
        {
        }

        #region Scan FM Documents

        public DataTable CheckScanFMDocno(string DocNo, string DocType, string BaseLocationCode, string HeadOfficeCode)
        {
            string sqlstr = "EXEC CheckScanFMDocno '" + DocNo + "','" + DocType + "','" + BaseLocationCode + "','" + HeadOfficeCode + "'";
            return GF.GetDataTableFromSP(sqlstr);
        }

        public DataTable AddEditScanDocument(byte DocType, string DocketNo, string DocumentNo, byte ScanStatus, string fileName, string path, DateTime DocumentDate, string BaseUserName, string BaseLocationCode)
        {
            string QueryString = "";
            if (ScanStatus == 3)
            {
                QueryString = "exec Webx_SP_FM_ScanDocuments_Update_NewPortal'" + DocType + "','" + DocketNo + "','" + DocumentNo + "','" + 0 + "','" + path + "','" + BaseUserName + "','" + BaseLocationCode + "',''";
            }
            else
            {
                QueryString = "exec Webx_SP_FM_ScanDocuments_NewPortal'" + DocType + "','" + DocketNo + "','" + DocumentNo + "','" + 0 + "','" + path + "','" + BaseUserName + "','" + BaseLocationCode + "',''";
            }
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditScanDocument_" + ScanStatus, "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<Webx_FM_Scan_Documents> GetFMDetailByDocNo(string Dockno)
        {
            string QueryString = "exec USP_Get_FM_Scan_Details'" + Dockno + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_FM_Scan_Documents> ItemList = DataRowToObject.CreateListFromTable<Webx_FM_Scan_Documents>(dataTable);
            return ItemList;
        }

        #endregion

        #region Forward FM Documents
        public List<vw_Bill_For_FWD> GetDoc_FW_BillList(WebX_FM_FWD_DOC_MasterViewModel WFFDMVM)
        {
            string QueryString = "EXEC webx_DocList_for_pfm'" + WFFDMVM.FFDFM.Dockets + "','" + WFFDMVM.FFDFM.DocType + "','" + WFFDMVM.FFDFM.loccode + "','" + GF.FormateDate(WFFDMVM.FFDFM.FromDate) + "','" + GF.FormateDate(WFFDMVM.FFDFM.ToDate) + "',null,'',null";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_Bill_For_FWD> ItemList = DataRowToObject.CreateListFromTable<vw_Bill_For_FWD>(dataTable);
            return ItemList;

        }

        public List<vw_dockets_For_FWD> GetDoc_FW_COD_DOD_PODList(WebX_FM_FWD_DOC_MasterViewModel WFFDMVM)
        {
            string QueryString = "";
            if (WFFDMVM.FFDFM.DocType == "1")
            {
                QueryString = "EXEC webx_pods_for_pfm'" + WFFDMVM.FFDFM.Dockets + "','" + WFFDMVM.FFDFM.Paybas + "','" + WFFDMVM.FFDFM.loccode + "','" + GF.FormateDate(WFFDMVM.FFDFM.FromDate) + "','" + GF.FormateDate(WFFDMVM.FFDFM.ToDate) + "','" + WFFDMVM.FFDFM.DT_TYPE + "',''";
            }
            else
            {
                QueryString = "EXEC webx_DocList_for_pfm'" + WFFDMVM.FFDFM.Dockets + "','" + WFFDMVM.FFDFM.DocType + "','" + WFFDMVM.FFDFM.loccode + "','" + GF.FormateDate(WFFDMVM.FFDFM.FromDate) + "','" + GF.FormateDate(WFFDMVM.FFDFM.ToDate) + "',null,null,'" + WFFDMVM.FFDFM.DT_TYPE + "'";
            }
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_dockets_For_FWD> ItemList = DataRowToObject.CreateListFromTable<vw_dockets_For_FWD>(dataTable);
            return ItemList;

        }

        public List<WebX_FM_FWD_DOC_Detail> GetFWDFMDetailByDocNo(string Dockno)
        {
            string QueryString = "exec USP_Get_FWD_FM_Scan_Details'" + Dockno + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WebX_FM_FWD_DOC_Detail> ItemList = DataRowToObject.CreateListFromTable<WebX_FM_FWD_DOC_Detail>(dataTable);
            return ItemList;
        }

        public DataTable GetFmNo(string loccode, string year, string suffix, byte doctype)
        {
            string QueryString = "select dbo.WebX_FN_FM_FWD_Get_Next_FM_No('" + loccode + "','" + year + "','" + suffix + "','" + doctype + "')";
            return GF.GetDataTableFromSP(QueryString);

        }

        public DataTable AddEditForwardDocuments(WebX_FM_FWD_DOC_MasterViewModel WFFDMVM)
        {
            string QueryString = "exec Webx_SP_FM_ForwardDocuments_new'" + WFFDMVM.WFFDM.FM_No + "','" + WFFDMVM.WFFDM.FM_Date + "','" + WFFDMVM.WFFDM.Manual_FM_No + "','" + WFFDMVM.WFFDM.FM_Entry_Date + "','" + WFFDMVM.WFFDM.Doc_FWD_To + "','" + WFFDMVM.WFFDM.Loc_Cust_Code + "','" + WFFDMVM.WFFDM.FM_Doc_Type + "','" + WFFDMVM.WFFDM.Courier_Code + "','" + WFFDMVM.WFFDM.Courier_Way_Bill_No + "','" + WFFDMVM.WFFDM.Courier_Way_Bill_Date + "','" + WFFDMVM.WFFDM.FM_FWD_LocCode + "','" + WFFDMVM.WFFDM.FM_FWD_CurrYear + "','" + WFFDMVM.WFFDM.Total_Documents + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddEditBillDocType(string FMno, string billno, string manualbillno, DateTime bgndt, string ptmsnm, decimal billamt, string PFM_LOC, string Scaned, string loccode)
        {
            string SQL_SRNO = "select top 1 id from WebX_FM_FWD_DOC_Master where FM_NO='" + FMno + "'";
            string srno = GF.executeScalerQuery(SQL_SRNO).ToString();
            string QueryString = "exec Webx_SP_FM_InsertDetail_For_Bill'" + srno + "','" + billno + "','" + manualbillno + "','" + GF.FormateDate(bgndt) + "','" + ptmsnm + "','" + billamt + "','" + PFM_LOC + "','" + Scaned + "','" + loccode + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddEditCODDODPODDocType(string doctyp, string FMno, string docket, DateTime dktdt, string docno, DateTime docdt, decimal dktamt, string orgdest, string from_to, string delydt, string scaned, string loccode)
        {

            string SQL_SRNO = "select top 1 id from WebX_FM_FWD_DOC_Master where FM_NO='" + FMno + "'";
            string srno = GF.executeScalerQuery(SQL_SRNO).ToString();


            string QueryString = "";
            if (doctyp == "1")
            {
                QueryString = "exec Webx_SP_FM_ForwardDocuments_Docket_new'" + srno + "','" + docket + "','" + GF.FormateDate(docdt) + "','N/A',NULL,'" + dktamt + "','" + orgdest + "','" + from_to + "','" + delydt + "','" + scaned + "','" + loccode + "'";
            }
            else
            {
                QueryString = "exec Webx_SP_FM_ForwardDocuments_Docket_new'" + srno + "','" + docket + "','" + dktdt + "','" + docno + "','" + GF.FormateDate(docdt) + "','" + dktamt + "','" + orgdest + "','" + from_to + "','" + delydt + "','" + scaned + "','" + loccode + "'";
            }
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddEditDocuments(string strXMLForHeader, string strXMLForDocket, string type)
        {
            string QueryString = "EXEC Webx_SP_FM_ForwardDocuments_NewPortal'" + strXMLForHeader + "','" + strXMLForDocket + "','" + type + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "ForwardDocuments_" + type, "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Forward Ack FM Document

        public List<vw_FM_DOC_For_Ack> GetDoc_FW_AckList(vw_FM_DOC_For_AckViewModel WFFDMVM)
        {
            string QueryString = "exec webx_FM_DOC_FOR_ACk'" + GF.FormateDate(WFFDMVM.FFDAF.FromDate) + "','" + GF.FormateDate(WFFDMVM.FFDAF.ToDate) + "','" + WFFDMVM.FFDAF.FMType + "','" + WFFDMVM.FFDAF.Loccode + "','','" + WFFDMVM.FFDAF.FmNo + "','','" + WFFDMVM.FFDAF.BaseLoccode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_FM_DOC_For_Ack> ItemList = DataRowToObject.CreateListFromTable<vw_FM_DOC_For_Ack>(dataTable);
            return ItemList;

        }

        //public DataTable UpdateForwardFMAckDocumentDetail(string FMNo, string FMType, string Forwarded)
        //{
        //    string QueryString = "EXEC [Update_fm_fwd_Detail] '" + FMNo + "','" + FMType + "','" + Forwarded + "'";
        //    return GF.GetDataTableFromSP(QueryString);
        //}
        public DataSet UpdateForwardFMAckDocumentDetail(string xmlackdocklist, string BaseUserName, string BaseLocationCode)
        {
            string QueryString = "EXEC Update_fm_fwd_Detail '" + xmlackdocklist + "','" + BaseUserName + "','" + BaseLocationCode + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DocumentsAcknowledge", "", "");
            return GF.GetDataSetFromSP(QueryString);
        }
        #endregion

        #region FM View/Print
        public List<VW_PFM_ViewPrint> GetFMReportList(FMFilter FMFilter)
        {
            string QueryString = "exec webx_PFM_ViewPrint'" + GF.FormateDate(FMFilter.FromDate) + "','" + GF.FormateDate(FMFilter.ToDate) + "','" + FMFilter.RO + "','" + FMFilter.Loccode + "','" + FMFilter.FmNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VW_PFM_ViewPrint> ItemList = DataRowToObject.CreateListFromTable<VW_PFM_ViewPrint>(dataTable);
            return ItemList;
        }

        public string GetLocLevel(string loccode)
        {
            string QueryString = "select Report_Level from webx_location where LocCode='" + loccode + "'";
            return GF.executeScalerQuery(QueryString);
        }

        #endregion

        #region Document Track

        public List<VW_FM_TRACK_Report> GetDocumentTrackList(string Doctype, string DocNo)
        {
            string QueryString = "";
            if (Doctype == "2")
            {
                QueryString = "select Bill_No,fm_doc_type,fm_no,fmdt,fromloc=(select loccode +':' + locname from webx_location where loccode=FM_crrloc),toLoc=loc_cust_code+ ' -('+doc_fwd_to+')',doc_status,Ack_Date from VW_FM_TRACK_Report where Bill_No='" + DocNo + "' and fm_doc_type='" + Doctype + "'";
            }
            else
            {
                QueryString = "select dockno,fm_doc_type,fm_no,fmdt,fromloc=(select loccode +':' + locname from webx_location where loccode=FM_crrloc),toLoc=loc_cust_code+ ' -('+doc_fwd_to+')',doc_status,Ack_Date from VW_FM_TRACK_Report where dockno='" + DocNo + "' and fm_doc_type='" + Doctype + "'";
            }

            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VW_FM_TRACK_Report> ItemList = DataRowToObject.CreateListFromTable<VW_FM_TRACK_Report>(dataTable);
            return ItemList;
        }

        public string CheckDocno(string DocNo, string DocType)
        {
            string QueryString = "select COUNT(*) from WebX_Master_Docket where DOCKNO='" + DocNo + "' and PAYBAS='P0" + DocType + "'";
            return GF.executeScalerQuery(QueryString);
        }

        #endregion
    }
}
