﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using CYGNUS.Models;
using FleetDataService.ViewModels;
using Fleet.ViewModels;
using Fleet.Models;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Collections;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class APIService : IAPIService
    {
        GeneralFuncations GF = new GeneralFuncations();
        #region GSTN Verification API
        public ArrayList CreateToken()
        {
            ArrayList response = new ArrayList();
            string result = "";
            try
            {
                string username = "sg@bestroadways.com";
                string password = "Suyashh@123#";
                string client_id = "REbdjQQvBrKHjnzwXc";
                string client_secret = "DykNrsSiD44UvrDylOGgEVpe";
                string grant_type = "password";
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://commonapi.mastersindia.co/oauth/access_token");

                httpWebRequest.Method = "POST";

                //httpWebRequest.Headers.Add("aftership-api-key:********fdbfd93980b8c5***");
                httpWebRequest.ContentType = "application/json";
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    string json = "{" + "\"username\":\"" + username + "\"," + "\"password\":\"" + password + "\"," + "\"client_id\":\"" + client_id + "\"," + "\"client_secret\":\"" + client_secret + "\"," + "\"grant_type\":\"" + grant_type + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();

                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                    }
                }
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }

            var jsn = JsonConvert.DeserializeObject(result);
            string strJson = jsn.ToString();
            var JsonResp = JObject.Parse(strJson);
            string Token = "";
            DateTime ExpiryDate = DateTime.Now;
            var TokenProperty = JsonResp.Property("access_token");
            var expires = JsonResp.Property("expires_in");
            if (TokenProperty != null)
            {
                Token = TokenProperty.Value.ToString();
                if (expires != null)
                {
                    double expsec = Convert.ToDouble(expires.Value.ToString());
                    expsec = expsec - 200;
                    ExpiryDate = ExpiryDate.AddSeconds(expsec);
                }
                else
                {
                    ExpiryDate = ExpiryDate.AddMinutes(1);
                }
            }
            else
            {
                Token = "";
            }

            string QueryString = "exec USP_Update_GSTN_Token '" + Token + "','" + ExpiryDate + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            response.Add(Token);
            response.Add(ExpiryDate.ToString());
            return response;
        }
        #endregion

        
    }
}