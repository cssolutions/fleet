﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.ViewModels
{
    public class GCFilter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
      
        public string GCNO { get; set; }
        public string PAYBAS { get; set; }
        public string TRNMOD { get; set; }
        public string BUSTYPE { get; set; }
        public string DATETYPE { get; set; }

        public string BookedByType { get; set; }
        public string BookedBy { get; set; }

        public string DOCTYP { get; set; }


        public int TYP { get; set; }

        public bool isBookedby { get; set; }


        public string LoadingBy { get; set; }
        public string ChargeType { get; set; }
        public string VendorCode { get; set; }

    }
}