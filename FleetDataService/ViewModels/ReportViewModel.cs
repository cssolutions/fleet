﻿
using Microsoft.Reporting.WebForms;
using FleetDataService.Models;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Fleet.ViewModels;

namespace Fleet.ViewModel
{
    //public class ReportFilter
    //{

    //    public DateTime FromDate { get; set; }
    //    public DateTime ToDate { get; set; }
    //    public string ClientName { get; set; }
    //    public string Route { get; set; }
    //    public string Stop { get; set; }
    //    public List<ReportPortalDataService.MasterService.ReportParameterValue> RouteList { get; set; }
    //    public List<ReportPortalDataService.MasterService.ReportParameterValue> StopList { get; set; }
    //}


    //public class StandardReportFilter
    //{

    //    public string ClientName { get; set; }
    //    public string RDLName { get; set; }
    //    public int ReportParameterSetId { get; set; }
    //    public string ReportName { get; set; }
    //    public List<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
    //    public List<ReportParameter> RDLparameters { get; set; }

    //}

    public class ReportsViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Location { get; set; }
        public string LocLevel { get; set; }

        public List<ReportParameter> RDLparameters { get; set; }
    }


    public class ShowReports
    {
        public List<ReportParameter> RDLparameters { get; set; }
    }


    public class ParameterSetViewModel
    {
        public IEnumerable<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
        public Master_ParameterSet ParameterSet { get; set; }
        public IEnumerable<ParaSet> ListSetPara { get; set; }

    }

    public class ParaType
    {
        public int Id { get; set; }
        public string ParameterType { get; set; }
    }

    public class ParaSet
    {
        public int Id { get; set; }
        public string ParameterSet { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<ParaSet> ListSetPara { get; set; }
    }

    public enum ReportType
    {
        Standard = 1,
        Custom = 2,
        //Other = 3,
    }

    //public class ReportParameterValue
    //{
    //    public string Value { get; set; }
    //    public string Text { get; set; }
    //}


    public class StandardReportFilter
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ClientName { get; set; }
        public string Location { get; set; }
        public string LocationLevel { get; set; }
        public string RO { get; set; }


        public string Company { get; set; }
        public string Customer { get; set; }
        public string Vendor { get; set; }
        public string Employee { get; set; }
        public string Driver { get; set; }

        public string Paybas { get; set; }
        public string TRNMODE { get; set; }
        public string ServiceType { get; set; }
        public string BusinessType { get; set; }

        //public string ReportFields { get; set; }
        
        public int ReportId { get; set; }
        public string RDLName { get; set; }
        public int ReportParameterSetId { get; set; }

        public string BaseFinYear { get; set; }
        public string BaseUserCode { get; set; }
        public string BaseLocationCode { get; set; }
        public string BaseLocationLevel { get; set; }

        public string INC_CUM { get; set; }
        public string AccountLocation { get; set; }

        public string FinYear { get; set; }
        public CYGNUS_Master_Reports ReportDetails { get; set; }
        //public List<CYGNUS_Master_Reports_Parameters> ReportParameters { get; set; }

        public List<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
        public List<ReportParameter> RDLparameters { get; set; }

    }

    //public class Parameters
    //{
    //    public int Id { get; set; }
    //    public string ParameterName { get; set; }
    //    public string SourceTableName { get; set; }
    //    public string IdColumn { get; set; }
    //    public string NameColumn { get; set; }
    //    public bool IsMultipleChoice { get; set; }
    //    public int ParameterType { get; set; }
    //    public bool IsActive { get; set; }

    //    public IEnumerable<Master_ParameterSet_Details> ListPara { get; set; }
    //}

    //public class ParaType
    //{
    //    public int Id { get; set; }
    //    public string ParameterType { get; set; }
    //}

    //public class ParaSet
    //{
    //    public int Id { get; set; }
    //    public string ParameterSet { get; set; }
    //    public string Description { get; set; }
    //    public bool IsActive { get; set; }
    //    public IEnumerable<ParaSet> ListSetPara { get; set; }
    //}

    //public enum ReportType
    //{
    //    Standard = 1,
    //    Custom = 2,
    //    //Other = 3,
    //}



    //public class ParameterSetViewModel
    //{
    //    public IEnumerable<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
    //    public Master_ParameterSet ParameterSet { get; set; }
    //    public IEnumerable<ParaSet> ListSetPara { get; set; }

    //}

    //public class Menus
    //{
    //    public int mid { get; set; }
    //    public int pid { get; set; }
    //    public int oid { get; set; }

    //    public int id { get; set; }

    //    [JsonProperty("children")]
    //    public List<Menus> children { get; set; }
    //}


}