﻿using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{
    public class ContractViewModel
    {
    }
    public class CustomerContractViewModel
    {
        public List<webx_custcontract_hdr> ListCustContract { get; set; }
        public webx_custcontract_hdr CustomerContract { get; set; }
        public webx_custcontract_charge CustomerCharge { get; set; }
        public List<webx_custcontract_fovmatrix> ListcustcontractMatrix { get; set; }
        public List<webx_custcontract_fovmatrix> ListcustcontractMatrixOwner { get; set; }
        public webx_custcontract_fovmatrix CustContMatrix { get; set; }
        public webx_custcontract_servicecharges CustServiceChrg { get; set; }
        public List<Webx_Master_General> ListMGeneral { get; set; }
        public List<webx_master_charge> Listcharge { get; set; }
        public List<webx_custcontract_charge_constraint> ListContraint { get; set; }
        public webx_custcontract_charge_constraint chargeContraint { get; set; }

        public List<Webx_CustContract_FRTMatrix_SingleSlab> ListsingleSlab { get; set; }
        public Webx_CustContract_FRTMatrix_SingleSlab CustsingleSlab { get; set; }

        public List<webx_custcontract_frtmatrix_slabhdr> Listslabhdr { get; set; }
        public webx_custcontract_frtmatrix_slabhdr Custslabhdr { get; set; }
        public List<Webx_Fleet_Contract_People> ContractPepoleList { get; set; }
        public Webx_Fleet_Secondary_Contract_Hdr SecondryConthdr { get; set; }
        public List<Webx_Fleet_Secondary_Contract_Hdr> ListSecondryContract { get; set; }


        public List<webx_custcontract_frtmatrix_slabhdr> ListslaODAbhdr { get; set; }
        public List<webx_custcontract_frtmatrix_slabhdr> ListMatrixCharge { get; set; }


        public List<webx_HandlingCharge> ListLoading { get; set; }
        public List<webx_HandlingCharge> ListUnloading { get; set; }

        public List<webx_location> RegionList { get; set; }
        public Nullable<decimal> LocLevel { get; set; }
        public List<webx_custcontract_servicecharges> ListCustServiceChrg { get; set; }
        public string BaseLoccode { get; set; }

        public List<Cygnus_Custcontract_locaton_zone_transmode_wise_master> ListCCLZTWM { get; set; }
        public Cygnus_Custcontract_locaton_zone_transmode_wise_master ObjCCLZTWM { get; set; }

    }
    public class CustomerContractTemplateViewModel
    {
        public CYGNUS_Customer_Contract_Template CCCTM { get; set; }
        public List<CYGNUS_Charges_Customer_Template> ListCCCTM { get; set; }

        public List<CYGNUS_Contract_Terms> ListTerms { get; set; }
    }
}