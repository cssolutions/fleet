﻿using CYGNUS.Models;
using CYGNUS.ViewModel;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{

    #region Income View Print
    public class vm_InconeViewPrint
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string billno { get; set; }
        public string mbillno { get; set; }
        public string BillingParty { get; set; }
        public string billtype { get; set; }
        public string billstatus { get; set; }
        public List<webx_BILLMST> ListBillMst { get; set; }
        public List<WEBX_MR_VPLIST> ListMRhdr { get; set; }
        public List<vw_SupplimentryViewList> ListSupplimentry { get; set; }
    }
    public class WEBX_MR_VPLIST
    {
        public string mrsno { get; set; }
        public DateTime mrsdt { get; set; }
        public string mrstype { get; set; }
        public string ptname { get; set; }
        public decimal mrsamt { get; set; }
        public decimal deduction { get; set; }
        public decimal netamt { get; set; }
        public DateTime finclosedt { get; set; }
        public string mbillno { get; set; }
        public string mrsbr { get; set; }
        public string MRSTATUS { get; set; }
        public string MR_STATUS { get; set; }
        //public string MRSBR { get; set; }
        public int iSNO { get; set; }
        public bool IsCancelled { get; set; }
        public string dockno { get; set; }
        public string ptcd { get; set; }
        public string mrcollbrcd { get; set; }

    }
    public class CreaditCnotViewModel
    {
        public List<ChgangeLoc> ListLocation { get; set; }
        public List<vw_CreaditCnotViewList> ListCraditCnot { get; set; }
        public vw_CreaditCnotView CreaditCnotModel { get; set; }
    }
    public class vw_CreaditCnotView
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string From_loc { get; set; }
        public string From_br { get; set; }
        public string DockNo { get; set; }
    }
    public class vw_CreaditCnotViewList
    {
        public string CNOTENO { get; set; }
        public DateTime CNOTEDT1 { get; set; }
        public DateTime CNOTEDT { get; set; }
        public string CNBRCD { get; set; }
        public string REFNO { get; set; }
        public string CUSTCD { get; set; }
        public decimal CNAMT { get; set; }
        public string ENTRYBY { get; set; }
    }
    public class vw_SupplimentryViewList
    {
        public string BILLNO { get; set; }
        public string BBRCD { get; set; }
        public string PARTY { get; set; }
        public string BILLTYPE { get; set; }
        public decimal BILLAMT { get; set; }
        public string BGNDT { get; set; }
        public string BSBDT { get; set; }
        public string BDUEDT { get; set; }
        public string BCLDT { get; set; }
        public string BILLSTATUS { get; set; }
        public string FLAG { get; set; }
    }
    #endregion

    #region Octroi Agent Bill View & Print

    public class OctroiAgentBillViewModel
    {
        public AgentBillModel AgentBillModel { get; set; }
        public List<AgentBillList> ListAgentBill { get; set; }
        public List<CustomerBillList> ListCustBill { get; set; }
        public List<AgentVoucherBillList> ListAgntVohrBill { get; set; }

        public List<GRNViewPrintList> ListGRNViewPrint { get; set; }
        public List<webx_GENERAL_POASSET_HDR> ListPOViewPrint { get; set; }

    }

    public class AgentBillModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string BillNo { get; set; }
        public string FilterType { get; set; }
        public string MBillNo { get; set; }
    }

    public class AgentBillList
    {
        public string OCBILLNO { get; set; }
        public string BillDt { get; set; }
        public string BillBranch { get; set; }
        public string Agent { get; set; }
        public string AgentBillDt { get; set; }
        public double BillAmt { get; set; }
    }

    public class CustomerBillList
    {
        public string BillNo { get; set; }
        public string BillBranch { get; set; }
        public string BillSub { get; set; }
        public string BillCol { get; set; }
        public string Party { get; set; }
        public string BillDt { get; set; }
        public decimal BillAmt { get; set; }
    }

    public class AgentVoucherBillList
    {
        public string VOUCHERNO { get; set; }
        public string PayDt { get; set; }
        public string BillBranch { get; set; }
        public string Agent { get; set; }
        public double BillAmt { get; set; }
        public double PendAmt { get; set; }
    }

    public class GRNViewPrintList
    {
        public string GRNNO { get; set; }
        public string GRNDT { get; set; }
        public string ManualGRNNO { get; set; }
        public string VENDORCD { get; set; }
        public decimal QTY { get; set; }
        public decimal ReceivedQTY { get; set; }
        public decimal TOTALAMT { get; set; }
        public string GRNSTATUS { get; set; }
    }

    public class Driver_Attendence_ViewPrint
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DriverNo { get; set; }
        public string Location { get; set; }
    }

    #endregion

    #region Branch Accounting  View Print

    public class BR_StatusViewPrint
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BRCD { get; set; }
        public string ExpenseType { get; set; }
        public string BRStatus { get; set; }
        public string billtype { get; set; }
        public string SubExpense { get; set; }
        public string ReportType { get; set; }
        public string Vehicle { get; set; }
        public string UserID { get; set; }
        public string Route { get; set; }
        public string Sortby { get; set; }
        public string Driver { get; set; }

    }

    #endregion

    #region ODA DRS/PRS Register

    public class ODADRS_PRSRegister
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BRCD { get; set; }
        public string ODAType { get; set; }
        public string DocumentNo { get; set; }
        public string TicketNo { get; set; }
        public string Reference { get; set; }
        public string DocketNo { get; set; }
    }

    #endregion
}