﻿using CYGNUS.Models;
using Fleet.ViewModels;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.ViewModels
{

    #region BA Bill Entry

    public class StaxTDSViewModel
    {

        public bool IsStaxEnabled { get; set; }
        public decimal StaxRate { get; set; }
        public decimal StaxRateAmount { get; set; }
        public decimal CessRate { get; set; }
        public decimal CessAmount { get; set; }
        public decimal HCessRate { get; set; }
        public decimal HCessAmount { get; set; }
        public decimal SBRate { get; set; }
        public decimal SBAmount { get; set; }
        public decimal KKCRate { get; set; }
        public decimal KKCAmount { get; set; }
        public decimal StaxOnAmount { get; set; }
        public string StaxRegNo { get; set; }
        public decimal StaxAmount { get; set; }

        public bool IsTDSEnabled { get; set; }
        public bool IsTDSDisplay { get; set; }
        public bool IsGSTDisplay { get; set; }
        public string TDSAcccode { get; set; }
        public string TDSAccdesc { get; set; }
        public decimal TDSRate { get; set; }
        public decimal TDSAmount { get; set; }

        public string PANNO { get; set; }
        public decimal TDSOnAmount { get; set; }

        public bool IsVATEnabled { get; set; }
        public bool IsVATDisplay { get; set; }
        public decimal VATOnAmount { get; set; }

        public decimal VATAmount { get; set; }
        public decimal VATRate { get; set; }


        public decimal OtherAmountPlus { get; set; }
        public decimal OtherAmountLess { get; set; }
        public bool IsOtherAmountEnabled { get; set; }


        public string DiscountType { get; set; }
        public decimal Discount { get; set; }
        public bool IsDiscountEnabled { get; set; }

        public string FiledNextAmount { get; set; }
        public int StaxType { get; set; }

        public string UserId { get; set; }

        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }
        public string ChargeType { get; set; }
        public string BETYP { get; set; }
        public string TYPEOFGST { get; set; }
        public string CorporateType { get; set; }

        public decimal TotalCommAmt { get; set; }

    }

    public class BABillEntyQuery
    {

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DATETYPE { get; set; }
        public string Location { get; set; }
        public string BookingDelivery { get; set; }

        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string DocumentNo { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }

        public bool isGSTReverse { get; set; }
        public string billType { get; set; }
    }

    //public class BABillEntyQuery
    //{

    //    public DateTime FromDate { get; set; }
    //    public DateTime ToDate { get; set; }
    //    public string DATETYPE { get; set; }
    //    public string Location { get; set; }
    //    public string BookingDelivery { get; set; }

    //    public string VendorCode { get; set; }
    //    public string VendorName { get; set; }
    //    public string DocumentNo { get; set; }

    //}

    public class BABillEntyGC
    {
        public bool IsEnabled { get; set; }
        public string dockno { get; set; }
        public string dockdt { get; set; }
        public string dockdate { get; set; }
        public string orgncd { get; set; }
        public string reassign_destcd { get; set; }
        public string from_loc { get; set; }

        public string to_loc { get; set; }
        public string paybas { get; set; }
        public string Paybasedesc { get; set; }
        public string trn_mod { get; set; }
        public string billed { get; set; }

        public string Contractcd { get; set; }
        public string location { get; set; }
        public string city { get; set; }
        public string transmode { get; set; }
        public decimal subtotal { get; set; }


        public decimal pkgsno { get; set; }
        public decimal chrgwt { get; set; }
        public decimal freight { get; set; }
        public decimal chg_rate { get; set; }
        public string rate_type { get; set; }

        public decimal ContractAmt { get; set; }
        public string Contract_Rate_Type_Desc { get; set; }
        public string Contract_Rate_Type { get; set; }
        public decimal Min_charge { get; set; }
        public decimal Max_charge { get; set; }
        public string HND_Values { get; set; }
        public string PRS_DRS { get; set; }

        public decimal ODACharge { get; set; }
        public decimal FTLChargeAmount { get; set; }
        public string billType { get; set; }

        public string DocketType { get; set; }
        public string CommissionOnBookingType { get; set; }
        public string CommissionRateTypeBooking { get; set; }
        public int DocketCount { get; set; }
        public decimal WEIGHT { get; set; }
        public decimal TotalFreight { get; set; }
        public decimal ComissionRate { get; set; }
        public decimal TDSRate { get; set; }
        public decimal Commision { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
       
        public int iSNO { get; set; }
        public bool IsEnabledforCnote { get; set; }
    }

    public class FuelSlipBillEntyTripSheet
    {
        public bool IsEnabled { get; set; }
        public string FUELSLIPNO { get; set; }
        public string VehicleNo { get; set; }
        public string TRIPSHEETNO { get; set; }
        public string VSlipDt { get; set; }
        public string Entrydt { get; set; }
        public decimal QUANTITYINLITER { get; set; }
        public decimal RATE { get; set; }
        public decimal AMOUNT { get; set; }

        public decimal f_issue_startkm { get; set; }

        public decimal f_closure_closekm { get; set; }

        public string Place { get; set; }

    }

    public class OtherBillEntryLedger
    {
        public string Acccode { get; set; }
        public string AccDesc { get; set; }
        public string Narration { get; set; }
        public int Id { get; set; }
        public decimal Amount { get; set; }
    }

    public class BABillEntyTripSheet
    {
        public string TripsheetNo { get; set; }

        public string ManualTripsheetNo { get; set; }
        public string Manual_TripSheetNo { get; set; }
        public string VSlipNo { get; set; }
        public string VehicleNo { get; set; }
        public decimal f_issue_startkm { get; set; }

        public int SRNO { get; set; }
        public string FuelType { get; set; }
        public string LastKM { get; set; }
        public string CurrentKM { get; set; }

        public string SlipNo { get; set; }
        public DateTime SlipDate { get; set; }

        public decimal DisealQty { get; set; }

        public decimal DisealRate { get; set; }
        public decimal DisealAmount { get; set; }
        public decimal ApprovedDisealAmount { get; set; }
        public decimal ApprovedDiesel { get; set; }
        public decimal FilledDiesel { get; set; }

        public string Remarks { get; set; }

        public string Location { get; set; }

    }

    public class VendorDetails
    {
        public string Acccode { get; set; }
        public string Accdesc { get; set; }
        public string Pan_no { get; set; }
        public string servtaxno { get; set; }
        public string AcctHead { get; set; }


    }

    public class POBillEntry
    {
        public string pocode { get; set; }
        public bool IsEnabled { get; set; }
        public string podt { get; set; }
        public string podate { get; set; }
        public string Adv_Voucherno { get; set; }
        public string Adv_Voucherdt { get; set; }
        public decimal qty { get; set; }
        public double tax_per { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal taxableAmt { get; set; }


        public decimal Totalamt { get; set; }
        public decimal BillAMount { get; set; }
        public decimal PendAmt { get; set; }
        public decimal PaidAmt { get; set; }

        public string vendorcd { get; set; }
        //change lavnit
        public string Vendorcode { get; set; }

        public string assetcd { get; set; }
        public string narration { get; set; }
        public decimal rate { get; set; }
        public decimal OtherCharge { get; set; }
        public decimal total { get; set; }
        public string assetnm { get; set; }

        //Lavnit Assect
        public string FIXEDASSETCD { get; set; }
        public string allotcd { get; set; }
        public string loccode { get; set; }
        public string assign { get; set; }
        public string Billno { get; set; }
        public DateTime billdt { get; set; }
        public string fixedassetdt { get; set; }
        public string desc { get; set; }
        public string DpartName { get; set; }
        public string Employee { get; set; }
        public DateTime InstallDate { get; set; }
        public string AssignLoc { get; set; }
        public string RCPLSrNo { get; set; }
        public string MFGSrNo { get; set; }

        //add by jemin on  GRN Generation Assign

        public string ICode { get; set; }
        public string GRNNO { get; set; }
        public string GRNDT { get; set; }
        public string IteamName { get; set; }
        public string AssignToLoca { get; set; }
        public string Description { get; set; }
        public int id { get; set; }
        public string Issuedate { get; set; }
        public decimal srno { get; set; }
        public bool IsGSTApply { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }
    }

    public class GRNBillEntryDetails
    {
        public string POCode { get; set; }
        public string Manual_PO_No { get; set; }
        public string PODT { get; set; }
        public string VendorName { get; set; }
        public string VendorCode { get; set; }
        public decimal POAdvanceAmt { get; set; }
        public decimal totalamt { get; set; }
        public decimal pendamt { get; set; }
        public decimal PO_balamt { get; set; }
        public decimal PO_advamt { get; set; }
        public decimal POQTY { get; set; }
        public decimal GRNQTY { get; set; }
        public decimal GRNAmt { get; set; }
        public string MatCatAcctHead { get; set; }

        public bool IsChecked { get; set; }

        public string GRNNO { get; set; }
        public string ManualGRNNO { get; set; }
        public string BBRCD { get; set; }
        public string Vendor { get; set; }
        public string GRNDT { get; set; }
        public string Adv_Voucherno { get; set; }
        public string Adv_Voucherdt { get; set; }
        //public decimal Totalamt { get; set; }
        // public decimal PendAmt { get; set; }
        public decimal PaidAmt { get; set; }
        public string CreditLedger { get; set; }
        public string CreditLedgerName { get; set; }
        public decimal GSTPercentage { get; set; }

        public decimal GRNOtherCharges { get; set; }
        public decimal GRNNetAmt { get; set; }
        public decimal GSTonGRNAmt { get; set; }
    }

    public class POGRNGenerationDetails
    {
        public string pocode { get; set; }
        public string Manual_PO_No { get; set; }
        public string Podate { get; set; }
        public string Vendorname { get; set; }

        public decimal TotalQty { get; set; }
        public decimal BALANCEQTY { get; set; }
        public decimal RECEIVEDQTY { get; set; }
        public string po_loccode { get; set; }
        public decimal PO_AMOUNT { get; set; }
        public string POTYPE { get; set; }

        public decimal srno { get; set; }
        public string assetcd { get; set; }
        public string Pdate { get; set; }
        public string Item_Code { get; set; }
        public string Item_Desc { get; set; }
        public decimal OrderQTY { get; set; }
        public decimal rate { get; set; }
        public decimal Total { get; set; }
        public string Vendor { get; set; }
        //public decimal BalanceQty { get; set; }
        public string VENDORCODE { get; set; }
        public string narration { get; set; }
        //public decimal ReceivedQTY { get; set; }
        public decimal GRNQTY { get; set; }
        public string IteamName { get; set; }

        public decimal tax_per { get; set; }
        public decimal Discount_Per { get; set; }
        public decimal Other_Chg { get; set; }
        public decimal NetAmt { get; set; }
        public decimal OtherChargesGRN { get; set; }
        public decimal GSTAmount { get; set; }
        public decimal taxableAmt { get; set; }
    }

    public class JOBBillEntryDetails
    {
        public bool IsEnabled { get; set; }
        public string JOB_ORDER_NO { get; set; }
        public string JOB_ORDER_DT { get; set; }
        public string JobCardType { get; set; }


        public string VEHNO { get; set; }
        public string JOB_ORDER_CLOSEDT { get; set; }


        public string VENDOR_CODE { get; set; }
        public decimal TOT_ACT_LABOUR_COST { get; set; }
        public decimal TOT_ACT_PART_COST { get; set; }
        public decimal TOT_JOB_COST { get; set; }

        //NEW  CHANGES
        public string NO { get; set; }
        public string date { get; set; }
        public decimal Charge { get; set; }
        public int ID { get; set; }
        public decimal Rate { get; set; }
        public string LoadBY { get; set; }
        public string Name { get; set; }
        public string DocumentType { get; set; }
        public decimal PKGSNO { get; set; }
        public decimal PKGSNO_Load { get; set; }
        public decimal ACTUWT { get; set; }
        public decimal CHRGWT { get; set; }
        public string Status { get; set; }
        public decimal TOT_Part_GST_COST { get; set; }
    }

    public class ConnectivityBillEntryDetails
    {
        public bool IsEnabled { get; set; }
        public int Srno { get; set; }
        public string DocNo { get; set; }
        public string DocDt { get; set; }
        public string BranchCode { get; set; }
        public string RouteName { get; set; }
        public string VehicleNo { get; set; }
        public decimal RunKM { get; set; }
        public decimal ACTWT { get; set; }
        public decimal Rate { get; set; }
        public string RateType { get; set; }
        public decimal CLOSEKM { get; set; }
        public decimal OPENKM{  get; set; }
        public decimal ConnectivityCharge { get; set; }
    }

    public class BABillEnty
    {
        public FuelBillEntyQuery FBE { get; set; }
        public BABillEntyQuery BBEQ { get; set; }
        public WEBX_VENDORBILL_HDR WVH { get; set; }
        public List<webx_VENDOR_HDR> WVHList { get; set; }
        public List<BABillEntyGC> DocketList { get; set; }
        public List<BABillEntyTripSheet> TripsheetList { get; set; }

        public List<OtherBillEntryLedger> LedgerList { get; set; }

        public List<FuelSlipBillEntyTripSheet> FuelTripsheetList { get; set; }        

        public List<JOBBillEntryDetails> JobList { get; set; }

        public List<POBillEntry> POList { get; set; }
        public List<webx_pdchdr> PRSDRSList { get; set; }
        public List<webx_THC_SUMMARY> THCList { get; set; }
        public List<GRNBillEntryDetails> GRNList { get; set; }

        public decimal TotalDisealAmount { get; set; }
        public decimal TotalApprovedDisealAmount { get; set; }

        public decimal TotalLabourAmount { get; set; }
        public decimal TotalPartAmount { get; set; }
        public decimal TotalJobCost { get; set; }

        public string BillEntryType { get; set; }
        public string CreditLedger { get; set; }
        public string CreditLedgerName { get; set; }
        public string DebitLedger { get; set; }
        public string DebitLedgerName { get; set; }
        public string TotDoc { get; set; }
        public string TotAmt { get; set; }

        public string StaxRegNo { get; set; }
        public string PANNO { get; set; }
        public string GSTType { get; set; }
        public ExpenseBillEntryQuery EBE { get; set; }
        public List<ConnectivityBillEntryDetails> ConnectivityTHCList { get; set; }
        public List<BABillEntyGC> DocketListForCnote { get; set; }
        public BillEntryCriteria BEC { get; set; }
        public List<webx_fleet_VoucherExp_DET> StatutaryExpenseList { get; set; }
        public List<webx_fleet_VoucherExp_DET> CrewSalaryExpenseList { get; set; }
        public List<webx_fleet_VoucherExp_DET> VehicleTrackingServiceExpenseList { get; set; }
        public List<webx_fleet_VoucherExp_DET> FinancialExpenseList { get; set; }
        public List<webx_fleet_VoucherExp_DET> RepairMainteneceExpenseList { get; set; }

        public decimal TotalAmountSpent { get; set; }
        public decimal TotalAmountApproved { get; set; }
    }
 
    public class VendorContractDetails
    {
        public string ContractTypeId { get; set; }
        public string ContractId { get; set; }
        public string ContractTypeDesc { get; set; }
    }

    public class FuelEntyPaymentQuery
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DATETYPE { get; set; }
        public string Location { get; set; }

        public string PaymentType { get; set; }

        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string VendorType { get; set; }

        public string BillEntryNo { get; set; }
        public string VendorBillNo { get; set; }
        public string VehicleNo { get; set; }

    }

    #endregion



    public class FinalizeBillQuery
    {

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DATETYPE { get; set; }
        public string Location { get; set; }
        public string LocationLevel { get; set; }

        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string DocumentNo { get; set; }

    }

    public class FinalizeBillDetails
    {
        public bool IsEnabled { get; set; }
        public string BillNo { get; set; }
        public string VendorBillDate { get; set; }
        public string Vendorcode { get; set; }

        public double TotalAmount { get; set; }
        public string BranchCode { get; set; }
        public string BillType { get; set; }
    }

    public class FinalizeBillViewModel
    {
        public FinalizeBillQuery FBQ { get; set; }
        public List<FinalizeBillDetails> ListFBD { get; set; }
    }

    #region Advance Balance Payment

    public partial class AdvancePaymentFilter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string VendorCode { get; set; }
        public string SDocNo { get; set; }
        public string MDocNo { get; set; }
        public string DocType { get; set; }
        public string VendorName { get; set; }
        public string Type { get; set; }
        public string DocTypeText { get; set; }

        public string ContractSubtype { get; set; }

        public string BaseLocation { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }

        public bool isGSTReverse { get; set; }
    }

    public class AdvancePaymentViewModel
    {

        public List<VW_THC_AdvancePayment> listVWTAPM { get; set; }
        public AdvancePaymentFilter APFM { get; set; }
        public DateTime AdvanceDate { get; set; }
        public string ManualVoucherno { get; set; }
        public string BalanceLocation { get; set; }
        public decimal TotalRecods { get; set; }
        public decimal TotalVoucherAmount { get; set; }
    }

    public class BalancePaymentViewModel
    {

        public List<VW_THC_BalancePayment> listVWTAPM { get; set; }

        public List<webx_master_charge> ListCharge { get; set; }

        public AdvancePaymentFilter APFM { get; set; }

        public List<Webx_Master_General> FTLList { get; set; }
        public List<Webx_Master_General> VehicleList { get; set; }

        public DateTime BalanceDate { get; set; }
        public string ManualVoucherno { get; set; }
        public string BalanceLocation { get; set; }
        public decimal TotalRecods { get; set; }
        public string Remarks { get; set; }


        public decimal TotPaymentAmount { get; set; }
        public decimal TotPendingAmount { get; set; }
        public decimal TotVoucherAmount { get; set; }


        public decimal TotalContractAmount { get; set; }
        public decimal TotalOtherAmount { get; set; }
        public decimal TotalAdvanceAmount { get; set; }
        public decimal TotalBalanceAmount { get; set; }

        public decimal TotalNetPaybleAmount { get; set; }
        public decimal TotalPaymentAmount { get; set; }
        public decimal TotalGSTChargeAmount { get; set; }
        public string FTLType { get; set; }
        public string VehicleNo { get; set; }


        public string ContractTypeId { get; set; }
        public string ContractId { get; set; }
        public string ContractTypeDesc { get; set; }
        public string ContractAlert { get; set; }
        public string ContractSubType { get; set; }

        public string SelectedContractId { get; set; }

        public string THCContract { get; set; }
        public string PDSContract { get; set; }
        public string ADHContract { get; set; }

        public string PaymentType { get; set; }

        public string BILLNO { get; set; }
        public DateTime BILLDT { get; set; }
        public DateTime VENDORBILLDT { get; set; }
        public string VENDORBILLNO { get; set; }
        public int DueDays { get; set; }
        public DateTime DUEDT { get; set; }
        public bool isGSTReverse { get; set; }
    }

    public class FuelBillEntyQuery
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DATETYPE { get; set; }
        public string Location { get; set; }
        public string VendorService { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string VehicleNo { get; set; }
        public string MatCat { get; set; }
        public string ManualPONo { get; set; }

        public int Type { get; set; }

        /* For GEN Generation*/
        public Nullable<decimal> LocationLevel { get; set; }
        public string RegionCode { get; set; }
        public string LocationCode { get; set; }

        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }

        public string LoadType { get; set; }
        public string DocType { get; set; }
        public string SDocNo { get; set; }
        public int Id { get; set; }
        public string BillType { get; set; }
        public string Branch { get; set; }
        public string Vcode { get; set; }

        public string PoNo { get; set; }

        public List<POAdvancePayment> ListPoAdvance { get; set; }

        //Add By jemin GRN REASSIGN
        public string LocCode { get; set; }
        public string ITEMCODE { get; set; }
        public string DocumentNo { get; set; }

        public string MonthWithYear { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }
        public bool isGSTReverse { get; set; }
    }

    public class GRNBillEntryViewModel
    {
        public List<GRNBillEntryDetails> GRNList { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string VendorName { get; set; }
        public string VehicleNo { get; set; }
        public string MatCat { get; set; }
        public string ManualPONo { get; set; }
        public string VendorCode { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public bool isGSTReverse { get; set; }
        public string PoNo { get; set; }
    }
    public class POGRNTyreDetail
    {
        public int SrNo { get; set; }
        public string Tyreno { get; set; }
        public string TyreSize { get; set; }
        public string TyreSizeID { get; set; }
        public string TyreMFG { get; set; }
        public string TyreMFGID { get; set; }
        public string TyreModel { get; set; }
        public int TyreModelID { get; set; }
        public string TyreCategory { get; set; }
        public string TyreCategoryID { get; set; }
        public decimal rate { get; set; }
    }
    public class GRNGenerationViewModel
    {
        public List<POGRNGenerationDetails> POList { get; set; }
        public List<POGRNTyreDetail> POTyreList { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string VendorName { get; set; }
        public string PONo { get; set; }
        public string MatCat { get; set; }
        public string ManualPONo { get; set; }
        public string VendorCode { get; set; }

        public string GRNNo { get; set; }
        public string GRNLocation { get; set; }
        public DateTime GRNDate { get; set; }
        public string PoDate { get; set; }
        public decimal PoAmount { get; set; }
        public string ManualGRNNo { get; set; }
        public string Remarks { get; set; }
    }

    public class ExpenseBillEntryQuery
    {
        public string ExpenseType { get; set; }
        public string ExpenseSubType { get; set; }
    }

    #endregion


    #region Bill ENtry Payment

    public partial class BillEntryPaymentFilter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string VendorCode { get; set; }
        public string BillNo { get; set; }
        public string VBillNo { get; set; }
        public string LocationCode { get; set; }
        public string VendorName { get; set; }
        public string BillNumber { get; set; }
        public decimal TotalRecods { get; set; }
        public decimal TotalAmt { get; set; }
        public DateTime VoucherDate { get; set; }
        public string VoucherNo { get; set; }
        public decimal OldTotalamt { get; set; }

        public string BillTyp { get; set; }
        public string BillType { get; set; }

        public string ModuleName { get; set; }

        public List<BillEntryPaymentList> BillEntryPayList { get; set; }
        public List<BillEntryPaymentList> BillEntryPayList1 { get; set; }

        public string CreditAmt { get; set; }
        public string DebitAmt { get; set; }
        public string OutStandingAmt { get; set; }

        public string VoucherStr { get; set; }
        public string GSTType { get; set; }
        public string StateCode { get; set; }
        public bool IsAdjusteDebitNote { get; set; }
    }

    public class BillEntryPaymentViewModel
    {
        public WEBX_VENDORBILL_HDR WVBH { get; set; }
        public List<WEBX_VENDORBILL_HDR> listWVBH { get; set; }
        public BillEntryPaymentFilter BEPFM { get; set; }
        public List<BillList> listBill { get; set; }
        public PaymentControl PC { get; set; }

        public List<BillEntry> ListBillEntry { get; set; }
        public BillEntry BE { get; set; }
        public BillList BL { get; set; }
        public List<DNM_DNCancellationList> DebitListBill { get; set; }

    }

    public partial class BillList
    {
        public string Billdate { get; set; }
        public string Billdt { get; set; }
        public string Billno { get; set; }
        public string VBillno { get; set; }
        public string Vendorcode { get; set; }
        public string vendorcd { get; set; }
        public decimal Paidamt { get; set; }
        public decimal pendamt { get; set; }
        public decimal CurrAmt { get; set; }
        public decimal Totalamt { get; set; }
        public decimal PCAMT { get; set; }
        public decimal TDSRATE { get; set; }
        public decimal TDS { get; set; }

        public string Type { get; set; }
        public string VoucherNo { get; set; }

        public bool IsCheckBox { get; set; }
        public decimal Amount { get; set; }
        public string BillType { get; set; }
        public decimal OLDpendamt { get; set; }
        
    }

    public partial class PaymentControl
    {
        public string PaymentMode { get; set; }
        public decimal PayAmount { get; set; }
        public decimal CashAmount { get; set; }
        public string CashLedger { get; set; }
        public decimal ChequeAmount { get; set; }
        public string BankLedger { get; set; }
        public string ChequeNo { get; set; }
        public string CashLedgerName { get; set; }
        public string BankLedgerName { get; set; }
        public DateTime ChequeDate { get; set; }

        public string ChequeAmount_Fromcheque { get; set; }
        public string TDSType { get; set; }
        public bool ISDepositedInBank { get; set; }
        public string DepositedInBank { get; set; }
        public string Received_From_Bank { get; set; }
        public string Bank_Branch { get; set; }
        public bool IsOnAccount { get; set; }
        public decimal NET_RECEIVED { get; set; }
        public decimal Collection_Amt_From_Cheque { get; set; }
        public string acccode { get; set; }

        /* For Fuel Card*/
        public bool IsFuelCard { get; set; }
        public string FuelCardNo { get; set; }
        public string VehicleNo { get; set; }

        /* For Auto TDS*/
        public bool IsTDSApplicable { get; set; }
        public string TANNo { get; set; }
        public decimal TDSPercentage { get; set; }
        public decimal totTDSAmt { get; set; }
        public string ModuleType { get; set; }

        public bool IsAllowForChequeCollection { get; set; }
        public decimal OldTotalamt { get; set; }
    }

    public partial class BillEntry
    {
        public int Id { get; set; }
        public string Billdate { get; set; }
        public string Billno { get; set; }
        public string BillAgentDate { get; set; }
        public string OctroiAgnet { get; set; }

        public string AgentBillNo { get; set; }
        public string AgentBillDate { get; set; }
        public decimal ServiceCharge { get; set; }
        public decimal TaxServiceCharge { get; set; }
        public decimal SundryCharge { get; set; }
        public string DueDate { get; set; }

        public decimal AgentPaidBy { get; set; }
        public decimal AgentSerCharge { get; set; }
        public decimal OtherCharge { get; set; }
        public decimal FromCharge { get; set; }
        public decimal ClearCharge { get; set; }
        public decimal TotalDue { get; set; }

        public string CNoteNo { get; set; }
        public decimal OctroiAmount { get; set; }
        public string OctroiReceiptnNo { get; set; }
        public string Receiptdate { get; set; }
        public decimal Hnd_Declval { get; set; }
        public string Remark { get; set; }
        public string COMPANY_CODE { get; set; }
    }
    #endregion

    public partial class BillEntryPaymentList
    {
        public string BRCD { get; set; }
        public string BETYPE { get; set; }
        public string BILLNUMBER { get; set; }
        public string VOUCHERNO { get; set; }
        public string BILLDATE { get; set; }
        public string BILLDT { get; set; }
        public string VBILLNO { get; set; }
        public string VENDORCODE { get; set; }
        public string VENDORCD { get; set; }
        public decimal PAIDAMT { get; set; }
        public decimal PENDAMT { get; set; }
        public decimal TOTALAMT { get; set; }
        public string DocumnetPath { get; set; }
    }


    #region ADD SKU Master
    public class ADDSKUMasterViewModel
    {
        public webx_PO_SKU_Master WPM { get; set; }
        public List<webx_PO_SKU_Master> ListWPM { get; set; }
        public string Flag { get; set; }
    }
    #endregion

    #region  Bill UnSubmission

    public class BillUnSubmissionViewModel
    {
        public BillEntryPaymentFilter BEPFM { get; set; }
        public List<BillUnSubmissionModel> ListBillUn { get; set; }
    }

    public class BillUnSubmissionModel
    {
        public bool IsChecked { get; set; }
        public string billno { get; set; }
        public string bgndate { get; set; }
        public string manualbillno { get; set; }
        public string Bbrcdnm { get; set; }
        public string ptmsstr { get; set; }
        public string paybas { get; set; }
        public decimal BILLAMT { get; set; }
        public decimal pendamt { get; set; }
        public string bgndt { get; set; }
        public string bduedt { get; set; }
        public DateTime bsbdt { get; set; }

    }

    #endregion

    #region PO Advance Payment
    public partial class POAdvancePayment
    {
        public int srno { get; set; }
        public string pocode { get; set; }
        public string Podate { get; set; }
        public string Manual_PO_No { get; set; }
        public string Vendorname { get; set; }
        public decimal TotalQty { get; set; }
        public decimal BALANCEQTY { get; set; }
        public decimal RECEIVEDQTY { get; set; }
        public string po_loccode { get; set; }
        public decimal PO_AMOUNT { get; set; }
        public string POTYPE { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal PO_advamt { get; set; }
        public decimal PO_balamt { get; set; }
        public PaymentControl PC { get; set; }
    }
    #endregion

    public class AssetUTILITYViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BillType { get; set; }
        public string ITEMCODE { get; set; }
        public string RCPLSNO { get; set; }
        public string LocCode { get; set; }
        public string BaseLoccode { get; set; }
        public string ASSETTYPE { get; set; }
        public string TYPE { get; set; }
        public string GRNNo { get; set; }
        public string UitilityType { get; set; }
        public string DocumentNo { get; set; }

        public string VENCD { get; set; }

        public List<webx_location> RegionList { get; set; }
        public Nullable<decimal> LocLevel { get; set; }
        public List<webx_FAALLOT_DET> WFTDLIST { get; set; }
        public List<CYGNUS_FixAssetType> CFALIST { get; set; }
        public webx_FAALLOT_DET WFTD { get; set; }
    }


    public class BillEntryCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string FilterType { get; set; }
        public string VendorCode { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }

        public string ExpenseType { get; set; }
        public string ExpenseHeads { get; set; }

        public bool StatutoryExpens { get; set; }
        public bool CrewSalaryExpense { get; set; }
        public bool VehicleTrackingServiceExpense { get; set; }
        public bool FinancialExpense { get; set; }
        public bool RepairMaintenanceExpense { get; set; }
    }


    #region MR Approval

    public class MRApprovalViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string MRNo { get; set; }
        public string CustomerCode { get; set; }
        public bool IsApproval { get; set; }
        public bool IsRejection { get; set; }
        public List<webx_MRDET_BILL> LisMRDetBill { get; set; }
    }

    #endregion

}