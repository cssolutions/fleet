﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Reporting.WebForms;
using FleetDataService.Models;
namespace FleetDataService.ViewModels
{
    public class ReportsViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Location { get; set; }
        public string LocLevel { get; set; }

        public List<ReportParameter> RDLparameters { get; set; }
    }


    public class ShowReports
    {
        public List<ReportParameter> RDLparameters { get; set; }
    }


    public class MenuObj1
    {
        public string DisplayName { get; set; }
        public int DisplayRank { get; set; }
        public bool HasAccess { get; set; }
        public int MenuId { get; set; }
        public string NavigationURL { get; set; }
        public int ParentID { get; set; }
        public string UserId { get; set; }
        public List<MenuList> mnList { get; set; }
        public bool IsActive { get; set; }
        public bool IsQuickLink { get; set; }
        public bool IsHomeDisplay { get; set; }
        public string RDLName { get; set; }
        public int MenuType { get; set; }
        public int MenuDisplayType { get; set; }
        public int ParameterSet { get; set; }
        //public IEnumerable<Master_Menu> ListMenu { get; set; }
        //public IEnumerable<Master_Menu> ListParentMenu { get; set; }
        public IEnumerable<ParaSet> ParaSetList { get; set; }
    }

    public class MenuList
    {
        public int MenuId { get; set; }
        public string DisplayName { get; set; }
    }

    public class ReportParameterValue
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }


    //public class StandardReportFilter
    //{
    //    public DateTime StartDate { get; set; }
    //    public DateTime EndDate { get; set; }
    //    public string ClientName { get; set; }
    //    public string Location { get; set; }
    //    public string LocationLevel { get; set; }
       

    //    public string Company { get; set; }
    //    public string Paybas { get; set; }
    //    public string TRNMODE { get; set; }
    //    public string ServiceType { get; set; }
    //    public string BusinessType { get; set; }


       
       
    //    public string RDLName { get; set; }
    //    public int ReportParameterSetId { get; set; }

    //    public string BaseFinYear { get; set; }
    //    public string BaseUserCode { get; set; }
    //    public string BaseLocationCode { get; set; }
    //    public string BaseLocationLevel { get; set; }

    //    public string INC_CUM { get; set; }
    //    public string AccountLocation { get; set; }


    //    public List<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
    //    public List<ReportParameter> RDLparameters { get; set; }

    //}

    public class Parameters
    {
        public int Id { get; set; }
        public string ParameterName { get; set; }
        public string SourceTableName { get; set; }
        public string IdColumn { get; set; }
        public string NameColumn { get; set; }
        public bool IsMultipleChoice { get; set; }
        public int ParameterType { get; set; }
        public bool IsActive { get; set; }
       
        public IEnumerable<Master_ParameterSet_Details> ListPara { get; set; }
    }

    public class ParaType
    {
        public int Id { get; set; }
        public string ParameterType { get; set; }
    }

    public class ParaSet
    {
        public int Id { get; set; }
        public string ParameterSet { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<ParaSet> ListSetPara { get; set; }
    }

    public enum ReportType
    {
        Standard = 1,
        Custom = 2,
        //Other = 3,
    }



    public class ParameterSetViewModel
    {
        public IEnumerable<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
        public Master_ParameterSet ParameterSet { get; set; }
        public IEnumerable<ParaSet> ListSetPara { get; set; }

    }

    //public class Menus
    //{
    //    public int mid { get; set; }
    //    public int pid { get; set; }
    //    public int oid { get; set; }

    //    public int id { get; set; }

    //    [JsonProperty("children")]
    //    public List<Menus> children { get; set; }
    //}

}