﻿using CYGNUS.Models;
using CYGNUS.ViewModel;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{
    #region LoadingSheet

    public partial class LoadingSheetFilterViewModel
    {
        public string Location { get; set; }
        public string LsNO { get; set; }
        public string ManualLsNO { get; set; }
        public DateTime LsDate { get; set; }
        public string NextStopLocation { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TransportMode { get; set; }
        public string DestinationList { get; set; }
        public string DocketNoList { get; set; }
        public string FirlterType { get; set; }
        public string Type { get; set; }
        public string NEXTLOC { get; set; }
        public List<LoadingSheetDatatable> DocketListForMFGeneration { get; set; }

        public string MF_TransportMode { get; set; }

        //report
        public string Driver { get; set; }
        public VW_DRIVER_FINAL_INFO DRN { get; set; }
        public List<VW_DRIVER_FINAL_INFO> DRIVERList { get; set; }
        public string DRSNo { get; set; }

        public string MFNo { get; set; }

        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }
        public string VendorCode { get; set; }
        public bool IsMonthly { get; set; }
        public decimal LoadingCharge { get; set; }
        public string VendorName { get; set; }
        public string RateType { get; set; }
        public string LoadingBy { get; set; }
        public decimal hdnRate { get; set; }
        public decimal Montlycharge { get; set; }

        //Mathadi Applicable
        public bool IsMathadi { get; set; }
        public string MathadiSlipNo { get; set; }
        public DateTime MathadiDate { get; set; }
        public decimal MathadiAmt { get; set; }

        public List<CYGNUS_Internal_Movement> InternalDocumentListForMFGeneration { get; set; }
    }

    public partial class LoadingSheetDatatable
    {
        public int Id { get; set; }
        public string DocketNo { get; set; }
        public string manual_dockno { get; set; }
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
        public decimal PKGSNO { get; set; }
        public decimal ACTUWT { get; set; }
        public string TransMode { get; set; }
        public string DocketDate { get; set; }
        public string OrgCode { get; set; }
        public string Commited_DelyDate { get; set; }
        public int PackagesLB { get; set; }
        public int WeightLB { get; set; }
        public string ReDestCode { get; set; }
        //public string fromto { get; set; }
        public string FromTo { get; set; }
        public bool IsChecked { get; set; }
        public decimal HandlingCharge { get; set; } //add new

        public bool IsCP { get; set; }

        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }
        public string VendorCode { get; set; }
        public bool IsMonthly { get; set; }

        public decimal NewRate { get; set; }
        public string ratetype { get; set; }
        public int CNT { get; set; }

        public string EWayBillNo { get; set; }
        public string Message { get; set; }

    }

    #endregion

    #region DRS

    public partial class vw_DRS_Summary
    {
        public string pdcno { get; set; }
        public string PDC_Dt { get; set; }
        public string deliveryBy { get; set; }
        public string BA_Vendor_Code { get; set; }
        public string Staff { get; set; }
        public string DriverName { get; set; }
        public string VEHNO { get; set; }
        public decimal Start_KM { get; set; }
        public int Total_Dockets_In_DRS { get; set; }
        public decimal CloseKM { get; set; }//IsAvilable
        public string PDC_Updated { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DRSNoList { get; set; }
        public string DOCKNO { get; set; }
        public string DOCKDT { get; set; }
        public string DRS { get; set; }
        public string DRS_DT { get; set; }
        public int AutoNo { get; set; }

        public string LoadingBy { get; set; }
        public string RateType { get; set; }
        public decimal LoadingCharge { get; set; }

        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public bool IsMonthly { get; set; }
        public decimal hdnRate { get; set; }

        public bool IsMathadi { get; set; }
        public string MathadiSlipNo { get; set; }
        public DateTime MathadiDate { get; set; }
        public decimal MathadiAmt { get; set; }


        public decimal PKGSNO { get; set; }
        public decimal ACTUWT { get; set; }


        public List<webx_BILLDET> BillDet { get; set; }
    }

    public class UpdateDRSViewModel
    {
        public vw_DRS_Summary DRSSummary { get; set; }
        public List<vw_DRS_Summary> UpdateDRS { get; set; }
        public List<vw_dockets_for_drs_updation_New> UpdateDRSLits { get; set; }
        public List<vw_DRS_Summary> DRSDeliveryList { get; set; }
        public decimal LoadingCharge { get; set; }
    }

    #endregion

    #region Arrival - Stock Update

    public partial class THCArrivalsFilterViewModel
    {
        public string THCNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string THCDepLoc { get; set; }
        public string THCVehicleNo { get; set; }
        public string THCCnoteNo { get; set; }
        public string Type { get; set; }
        public string LoadingBy { get; set; }
        public string ChargeType { get; set; }
        public string TransportMode { get; set; }
    }

    public partial class THCArrivalsDatatable
    {
        public string THCNO { get; set; }
        public string THCDate { get; set; }
        public string PreviousBranch { get; set; }
        public string Route { get; set; }
        public string Vehicle { get; set; }
        public string ATA { get; set; }
        public string ATD { get; set; }
        public string Status { get; set; }
        public string ManualTHCNo { get; set; }
        public string NextLocation { get; set; }
        public string RouteCode { get; set; }
        public DateTime THCDT { get; set; }
        public decimal OPENKM { get; set; }
        public string THCDate_ddmmYYYY { get; set; }
        public DateTime ETA { get; set; }
        public int Stoppage_Time_Mins { get; set; }
        public string ETA__ { get; set; }
        public string ETA_ { get; set; }
        public string ATD_ { get; set; }
    }

    public class vw_Stock_For_UpdateViewModel
    {
        public vw_Stock_For_Update VSFUM { get; set; }
        public List<vw_Stock_For_Update> listVSFUM { get; set; }
    }

    #endregion

    #region THC Departures

    public class THCDeparturesViewModel
    {

        public webx_THC_SUMMARY THCDepModel { get; set; }
        public List<webx_THC_SUMMARY> THCDepList { get; set; }
    }

    #endregion

    #region DOCKET

    public class DocketViewModel
    {
        public List<CygnusChargesHeader> ListCCH { get; set; }

        public WebX_Master_Docket WMD { get; set; }
        public string OLDPayBas { get; set; }
        public Nullable<decimal> OLDDKTTOT { get; set; }
        public WebX_Master_Docket_Charges WMDC { get; set; }
        public List<WebX_Master_Docket_Invoice> ListInVoice { get; set; }
        public List<QuickDocketBalancecharges> ListCharges { get; set; }
        public List<webx_Master_DOCKET_DOCUMENT> ListDocument { get; set; }
        public string Delivered { get; set; }
        public string PRSDONE { get; set; }
        public string DRSDONE { get; set; }
        public CYGNUSPickupRequest ObjPRQ { get; set; }
        public bool IsCompletion { get; set; }

        public webx_CUSTHDR WCM { get; set; }
    }

    public partial class QuickDocketBalancecharges
    {
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public string Operator { get; set; }
        public string Acccode { get; set; }
        public decimal ChargeAmount { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsAppliedOnFreight { get; set; }
        public bool IsDiscountApplied { get; set; }
        public bool IsAddChargeApplied { get; set; }
        public bool IsLessChargeApplied { get; set; }
    }

    public partial class Step1Model
    {
        public string DATE_RULE { get; set; }
        public string FLAG_COMPUTERISED { get; set; }
        public string FLAG_ROUND { get; set; }
        public string PARTY_ENTRY_SETTING { get; set; }
        public string contractsets { get; set; }
    }

    public partial class Step2Model
    {
        public string CONTRACTID { get; set; }
        public string SEARCH { get; set; }
        public string BEHAVE { get; set; }
        public string FLAG_LOT { get; set; }
        public string FLAG_MULTI_PICKUPDEL { get; set; }

        public string FCITY { get; set; }
        public string TCITY { get; set; }
        public string FLAG_PERMIT { get; set; }
        public string STAX_EXMPT_YN { get; set; }
        public string Flag_Defer { get; set; }

        public string TransMode { get; set; }
        public string ServiceType { get; set; }
        public string PKGDelyType { get; set; }
        public string FTLType { get; set; }

        public string cft_weight_type { get; set; }


        public string Risktype { get; set; }
        public decimal CODDODCharge { get; set; }
        public decimal DACCCharge { get; set; }
        public string BillingLocation { get; set; }

        public string IsVolumentric { get; set; }
        public string IsCODDOD { get; set; }
        public string IsDACC { get; set; }
        public string CFTWeightType { get; set; }

        public string ChargeBas { get; set; }

        public string MinFreightType { get; set; }
        public string FLAG_Freight { get; set; }
        public string FLAG_Subtotal { get; set; }

        public string CODRateType { get; set; }
        public decimal Min_CODCharged { get; set; }
        public decimal CODCharged { get; set; }

        public string DACCRateType { get; set; }
        public decimal DACCCharged { get; set; }
        public decimal Min_DACCCharged { get; set; }

    }

    #region Copy From OLD Code

    public class ChargeCodeValue
    {
        string _ChargeCode;
        double _ChargeValue;

        public ChargeCodeValue()
        {
            _ChargeCode = ""; _ChargeValue = 0;
        }

        public string ChargeCode { get { return _ChargeCode; } set { _ChargeCode = value; } }
        public double ChargeValue { get { return _ChargeValue; } set { _ChargeValue = value; } }
    }
    public class DocketServiceTax
    {
        private double subtotal, servicetax, educess, heducess, staxrate, staxratestd, cessrate, hcessrate;
        private double cessratestd, hecessratestd, exceedamt;
        private string transmode, staxpayer, servicetype;
        private DateTime date;




        public DocketServiceTax()
        {
            subtotal = 0; servicetax = 0; educess = 0; heducess = 0; staxrate = 0;
            staxratestd = 0; cessrate = 0; hcessrate = 0; KKCRate = 0; KKCAmount = 0;
            transmode = ""; staxpayer = ""; servicetype = "";
            date = DateTime.MinValue;
            /* SB Cess*/
            SbcRate = 0; SBCess = 0;

            /* SB Cess*/
        }

        /* SB Cess*/
        public double SbcRate { get; set; }
        public double SBCess { get; set; }

        public double KKCRate { get; set; }
        public double KKCAmount { get; set; }


        /* SB Cess*/

        public string TransMode
        {
            get { return transmode; }
            set { transmode = value; }
        }

        public string StaxPayer
        {
            get { return staxpayer; }
            set { staxpayer = value; }
        }

        public string ServiceType
        {
            get { return servicetype; }
            set { servicetype = value; }
        }

        public double SubTotal
        {
            get { return subtotal; }
            set { subtotal = value; }
        }

        public double ExceedAmount
        {
            get { return exceedamt; }
            set { exceedamt = value; }
        }

        public double StdSTaxRate
        {
            get { return staxratestd; }
            set { staxratestd = value; }
        }

        public double StdEduCessRate
        {
            get { return cessratestd; }
            set { cessratestd = value; }
        }

        public double StdHEduCessRate
        {
            get { return hecessratestd; }
            set { hecessratestd = value; }
        }

        public double STaxRate
        {
            get { return staxrate; }
            set { staxrate = value; }
        }

        public double EduCessRate
        {
            get { return cessrate; }
            set { cessrate = value; }
        }

        public double HEduCessRate
        {
            get { return hcessrate; }
            set { hcessrate = value; }
        }

        public double ServiceTax
        {
            get { return servicetax; }
            set { servicetax = value; }
        }

        public double EduCess
        {
            get { return educess; }
            set { educess = value; }
        }

        public double HEduCess
        {
            get { return heducess; }
            set { heducess = value; }
        }

        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

    }

    public class TaxRate
    {
        public TaxRate()
        {
            staxrate = 0; educessrate = 0;
            heducessrate = 0; after_rebate_per = 0;
            date = DateTime.MinValue;

            /* SB Cess*/
            SbcRate = 0;
            KKCRate = 0;
            /* SB Cess*/
        }

        private double staxrate, educessrate, heducessrate, after_rebate_per;

        private DateTime date;

        public DateTime Date { get { return date; } set { date = value; } }
        public double ServiceTaxRate { get { return staxrate; } set { staxrate = value; } }
        public double EduCessRate { get { return educessrate; } set { educessrate = value; } }
        public double HEduCessRate { get { return heducessrate; } set { heducessrate = value; } }
        public double AfterRebatePer { get { return after_rebate_per; } set { after_rebate_per = value; } }

        /* SB Cess*/
        public double SbcRate { get; set; }
        public double KKCRate { get; set; }
        /* SB Cess*/
    }
    public class TaxRebate
    {

        public TaxRebate()
        {
            //
            // TODO: Add constructor logic here
            //

            startdate = DateTime.MinValue;
            enddate = DateTime.MinValue;
            transtype = ""; exceedamt = 0; taxrebateper = 0; cessrebateper = 0; hcessrebateper = 0;
            servicetype = "";
        }

        private string transtype, servicetype;
        private double exceedamt, taxrebateper, cessrebateper, hcessrebateper;
        private DateTime startdate, enddate;

        public DateTime StartDate { get { return startdate; } set { startdate = value; } }
        public DateTime EndDate { get { return enddate; } set { enddate = value; } }

        public string TransType { get { return transtype; } set { transtype = value; } }
        public string ServiceType { get { return servicetype; } set { servicetype = value; } }

        public double ExceedAmt { get { return ExceedAmt; } set { exceedamt = value; } }
        public double ServiceTaxRebatePer { get { return taxrebateper; } set { taxrebateper = value; } }
        public double EduCessRebatePer { get { return cessrebateper; } set { cessrebateper = value; } }
        public double HEduCessRebatePer { get { return hcessrebateper; } set { hcessrebateper = value; } }


    }

    public class ContractKeys
    {
        private string status = "", description = "", contractid = "", foundcontract = "", flagproceed = "", depth = "", paybas = "", invoiceRateApplay = "";
        private string fromcity = "", tocity = "", orgnloc = "", delloc = "";
        private string basedon1 = "", basecode1 = "", basedon2 = "", basecode2 = "";
        private string servicetype = "", ftltype = "", transmode = "", orderid;
        private double freightcharge = 0, freightrate = 0, invoiceRate = 0;
        private int trdays = 0;
        private string ratetype = "", flagprorata = "N";
        private double chargedweight = 0, noofpkgs = 0, fovcharge = 0;
        private string minfreighttype = "", minfreightbas = "";
        private double minfreightbasrate = 0;
        private double minfreightrate = 0, freightratelowerlimit = 0, freightrateupperlimit = 0;
        private double minsubtotal = 0, subtotallowerlimit = 0, subtotalupperlimit = 0, _InvAmt;
        private string flag_freight = "", flag_subtotal = "";

        public ContractKeys()
        {
            orderid = ""; _InvAmt = 0;
        }

        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        public string Description
        {
            get { return description; }
            set { description = value; }
        }
        public string ContractID
        {
            get { return contractid; }
            set { contractid = value; }
        }
        public string FoundContract
        {
            get { return foundcontract; }
            set { foundcontract = value; }
        }
        public string FlagProceed
        {
            get { return flagproceed; }
            set { flagproceed = value; }
        }
        public string Depth
        {
            get { return depth; }
            set { depth = value; }
        }
        public string PayBase
        {
            get { return paybas; }
            set { paybas = value; }
        }

        public string BasedOn1
        {
            get { return basedon1; }
            set { basedon1 = value; }
        }

        public string BaseCode1
        {
            get { return basecode1; }
            set { basecode1 = value; }
        }

        public string BasedOn2
        {
            get { return basedon2; }
            set { basedon2 = value; }
        }

        public string BaseCode2
        {
            get { return basecode2; }
            set { basecode2 = value; }
        }

        public string FromCity
        {
            get { return fromcity; }
            set { fromcity = value; }
        }
        public string ToCity
        {
            get { return tocity; }
            set { tocity = value; }
        }
        public string OrgnLoc
        {
            get { return orgnloc; }
            set { orgnloc = value; }
        }
        public string DelLoc
        {
            get { return delloc; }
            set { delloc = value; }
        }
        public string ServiceType
        {
            get { return servicetype; }
            set { servicetype = value; }
        }
        public string FTLType
        {
            get { return ftltype; }
            set { ftltype = value; }
        }
        public string TransMode
        {
            get { return transmode; }
            set { transmode = value; }
        }

        public double FreightCharge
        {
            get { return freightcharge; }
            set { freightcharge = value; }
        }
        public double FreightRate
        {
            get { return freightrate; }
            set { freightrate = value; }
        }
        public int TRDays
        {
            get { return trdays; }
            set { trdays = value; }
        }

        public string RateType
        {
            get { return ratetype; }
            set { ratetype = value; }
        }
        public string FlagProrata
        {
            get { return flagprorata; }
            set { flagprorata = value; }
        }

        public double ChargedWeight
        {
            get { return chargedweight; }
            set { chargedweight = value; }
        }
        public double NoOfPkgs
        {
            get { return noofpkgs; }
            set { noofpkgs = value; }
        }

        public string OrderID
        {
            get { return orderid; }
            set { orderid = value; }
        }

        public double FOVCharge
        {
            get { return fovcharge; }
            set { fovcharge = value; }
        }

        public string MinFreightType
        {
            get { return minfreighttype; }
            set { minfreighttype = value; }
        }
        public string MinFreightBase
        {
            get { return minfreightbas; }
            set { minfreightbas = value; }
        }
        public double MinFreightBaseRate
        {
            get { return minfreightbasrate; }
            set { minfreightbasrate = value; }
        }

        public double MinFreightRate
        {
            get { return minfreightrate; }
            set { minfreightrate = value; }
        }
        public double FreightRateLowerLimit
        {
            get { return freightratelowerlimit; }
            set { freightratelowerlimit = value; }
        }
        public double FreightRateUpperLimit
        {
            get { return freightrateupperlimit; }
            set { freightrateupperlimit = value; }
        }

        public double MinSubTotal
        {
            get { return minsubtotal; }
            set { minsubtotal = value; }
        }
        public double SubTotalLowerLimit
        {
            get { return subtotallowerlimit; }
            set { subtotallowerlimit = value; }
        }
        public double SubTotalUpperLimit
        {
            get { return subtotalupperlimit; }
            set { subtotalupperlimit = value; }
        }

        public string FlagFreight
        {
            get { return flag_freight; }
            set { flag_freight = value; }
        }
        public string FlagSutotal
        {
            get { return flag_subtotal; }
            set { flag_subtotal = value; }
        }

        public double InvAmt
        {
            get { return _InvAmt; }
            set { _InvAmt = value; }
        }

        public string InvoiceRateApplay
        {
            get { return invoiceRateApplay; }
            set { invoiceRateApplay = value; }
        }
        public double InvoiceRate
        {
            get { return invoiceRate; }
            set { invoiceRate = value; }
        }
    }

    public class FOVCharge
    {
        string contractid, flag, risktype, chargerule, basecode, fovratetype;
        double fovcharged, declaredvalue, fovrate;
        public FOVCharge()
        {
            contractid = ""; flag = ""; risktype = ""; chargerule = ""; basecode = ""; fovratetype = "";
            fovcharged = 0; declaredvalue = 0; fovrate = 0;
        }
        public string FlagFOV { get { return flag; } set { flag = value; } }
        public string ContractID { get { return contractid; } set { contractid = value; } }
        public string ChargeRule { get { return chargerule; } set { chargerule = value; } }
        public string BaseCode { get { return basecode; } set { basecode = value; } }
        public string RiskType { get { return risktype; } set { risktype = value; } }
        public string FOVRateType { get { return fovratetype; } set { fovratetype = value; } }
        public double DeclareValue { get { return declaredvalue; } set { declaredvalue = value; } }
        public double FOVCharged { get { return fovcharged; } set { fovcharged = value; } }
        public double FOVRate { get { return fovrate; } set { fovrate = value; } }
    }

    public class Docket
    {
        public Docket()
        {
            custrefdelno = ""; custrefgpno = ""; cntorderno = ""; csgnmob = ""; csgemob = "";
            sealno = ""; contid = ""; manual_dockno = ""; flaghday = ""; tamno = ""; partyname = "";
            sQuotationNo = ""; sAgendaNo = ""; companycode = ""; ContractID = "";
            _FlagCutoffApplied = ""; _FlagHolidayBooked = ""; _PrimaryCompanyDockNo = "";
            _EngineNo = ""; _ModelNo = ""; _GPSNo = ""; _ChassisNo = ""; _Industry = ""; _ContractID = "";
        }

        private string dockno = "", paybas = "", partycode = "", partyname, orgnloc = "", delloc = "", companycode, transmode = "", servicetype = "", ftltype = "", pickupdel = "", tamno;
        private string _ContractID = "", sdd_date_str = "", EmailID = "";
        DateTime dockdate = DateTime.MinValue, permitdate = DateTime.MinValue, permitvaliditydate = DateTime.MinValue, permitrecvdate = DateTime.MinValue, policydate = DateTime.MinValue, invdate = DateTime.MinValue;
        DateTime edd = DateTime.MinValue, entrydate = DateTime.MinValue, custrefdate = DateTime.MinValue, sdd_date = DateTime.MinValue;
        //START GST Changes Chirag D
        private string originstatesode = "", destdtatecode = "";
        private bool isunionteritory = false;
        private bool isewaybillparta = false;
        //END GST Changes Chirag D
        DocketInvoice[] diDocketInvoice;

        private string fromcity = "", tocity = "", packtype = "", prodtype = "", flagvolumetric = "", flagcoddod = "", flagdacc = "", flagoda = "", flaglocal = "";
        private string flagpermit = "", loadtype = "", partner = "", costid = "", dopino = "", businesstype = "", remarks = "", mrno = "";
        private string csgncd = "", csgnaddrcd = "", csgnname = "", csgnaddr = "", csgncity = "", csgnpincode = "", csgnteleno = "", csgnemail = "", csgnmob;
        private string csgecd = "", csgeaddrcd = "", csgename = "", csgeaddr = "", csgecity = "", csgepincode = "", csgeteleno = "", csgeemail = "", csgemob;
        private string obdno = "", entrysheetno = "", permitno = "", permitrecvdat = "", localstaxregno = "";
        private string cntorderno, manual_dockno, csgntinno, csgetinno, csgncstno, csgecstno, csgngstno, csgegstno, _FlagHolidayBooked, _FlagCutoffApplied;
        private string invno = "", risktype = "", custrefdelno, custrefgpno, contid, sealno, flaghday;
        private string _EngineNo, _ModelNo, _GPSNo, _ChassisNo, _Industry;
        private double coddodcharged, coddodamount, dacccharged;
        private string bacode = "", policyno = "", custrefno = "", privatemark = "", tpnumber = "", staxpaidby = "", staxregno = "", billedat = "";
        private string sQuotationNo, sAgendaNo, _PrimaryCompanyDockNo;
        private double internalcovers = 0.0, modvatecovers = 0.0, oct_amt = 0;
        private bool isCounterPickUpPRS, isCounterDelivery;

        private string csgnpanono, csgnaadharno, csgepanno, csgeaadharno;
        Int64 invamt, actualweight, chargedweight, noofpkgs, Modultype;


        private string sourcedockno = "", flagmultipickup = "", flagmultidelivery = "", flagstaxexmpt = "", partyas = "", doctype = "", entryby = "";

        public string DockNo { get { return dockno; } set { dockno = value; } }
        public string ContractID { get { return _ContractID; } set { _ContractID = value; } }
        public string MRNo { get { return mrno; } set { mrno = value; } }
        public DateTime DockDate { get { return dockdate; } set { dockdate = value; } }
        public string PayBase { get { return paybas; } set { paybas = value; } }

        public string PartyCode { get { return partycode; } set { partycode = value; } }
        public string PartyName { get { return partyname; } set { partyname = value; } }
        public string OrgnLoc { get { return orgnloc; } set { orgnloc = value; } }
        public string DelLoc { get { return delloc; } set { delloc = value; } }
        public string TransMode { get { return transmode; } set { transmode = value; } }
        public string ServiceType { get { return servicetype; } set { servicetype = value; } }
        public string FTLType { get { return ftltype; } set { ftltype = value; } }
        public string PickUpDel { get { return pickupdel; } set { pickupdel = value; } }
        public string FromCity { get { return fromcity; } set { fromcity = value; } }
        public string ToCity { get { return tocity; } set { tocity = value; } }
        public string PackType { get { return packtype; } set { packtype = value; } }
        public string ProdType { get { return prodtype; } set { prodtype = value; } }
        public string Remarks { get { return remarks; } set { remarks = value; } }
        public string TAMNo { get { return tamno; } set { tamno = value; } }

        public string QuotationNo { get { return sQuotationNo; } set { sQuotationNo = value; } }
        public string AgendaNo { get { return sAgendaNo; } set { sAgendaNo = value; } }

        public string FlagVolumetric { get { return flagvolumetric; } set { flagvolumetric = value; } }
        public string FlagCODDOD { get { return flagcoddod; } set { flagcoddod = value; } }
        public string FlagDACC { get { return flagdacc; } set { flagdacc = value; } }
        public string FlagODA { get { return flagoda; } set { flagoda = value; } }
        public string FlagLocal { get { return flaglocal; } set { flaglocal = value; } }
        public string FlagPermit { get { return flagpermit; } set { flagpermit = value; } }
        public string FlagHolidayApplied { get { return flaghday; } set { flaghday = value; } }
        public string FlagHolidayBooked { get { return _FlagHolidayBooked; } set { _FlagHolidayBooked = value; } }
        public string FlagCutoffApplied { get { return _FlagCutoffApplied; } set { _FlagCutoffApplied = value; } }

        public string LoadType { get { return loadtype; } set { loadtype = value; } }
        public string DopiNo { get { return dopino; } set { dopino = value; } }
        public string BusinessType { get { return businesstype; } set { businesstype = value; } }

        public string ConsignorCode { get { return csgncd; } set { csgncd = value; } }
        public string ConsignorAddressCode { get { return csgnaddrcd; } set { csgnaddrcd = value; } }
        public string ConsignorName { get { return csgnname; } set { csgnname = value; } }
        public string ConsignorAddress { get { return csgnaddr; } set { csgnaddr = value; } }
        public string ConsignorCity { get { return csgncity; } set { csgncity = value; } }
        public string ConsignorPinCode { get { return csgnpincode; } set { csgnpincode = value; } }
        public string ConsignorPhone { get { return csgnteleno; } set { csgnteleno = value; } }
        public string ConsignorEmail { get { return csgnemail; } set { csgnemail = value; } }
        public string ConsignorTinNo { get { return csgntinno; } set { csgntinno = value; } }
        public string ConsignorCSTNo { get { return csgncstno; } set { csgncstno = value; } }
        public string ConsignorGSTNo { get { return csgngstno; } set { csgngstno = value; } }

        public string ConsignorPanNo { get { return csgnpanono; } set { csgnpanono = value; } }
        public string ConsignorAadharNo { get { return csgnaadharno; } set { csgnaadharno = value; } }
        public string ConsigneePanNo { get { return csgepanno; } set { csgepanno = value; } }
        public string ConsigneeAadharNo { get { return csgeaadharno; } set { csgeaadharno = value; } }
        public string ConsignorMobile { get { return csgnmob; } set { csgnmob = value; } }

        public string ConsigneeCode { get { return csgecd; } set { csgecd = value; } }
        public string ConsigneeAddressCode { get { return csgeaddrcd; } set { csgeaddrcd = value; } }
        public string ConsigneeName { get { return csgename; } set { csgename = value; } }
        public string ConsigneeAddress { get { return csgeaddr; } set { csgeaddr = value; } }
        public string ConsigneeCity { get { return csgecity; } set { csgecity = value; } }
        public string ConsigneePinCode { get { return csgepincode; } set { csgepincode = value; } }
        public string ConsigneePhone { get { return csgeteleno; } set { csgeteleno = value; } }
        public string ConsigneeEmail { get { return csgeemail; } set { csgeemail = value; } }
        public string ConsigneeTinNo { get { return csgetinno; } set { csgetinno = value; } }
        public string ConsigneeCSTNo { get { return csgecstno; } set { csgecstno = value; } }
        public string ConsigneeGSTNo { get { return csgegstno; } set { csgegstno = value; } }
        public string ConsigneeMobile { get { return csgemob; } set { csgemob = value; } }

        public Int64 Moduletype { get { return Modultype; } set { Modultype = value; } }


        public DocketInvoice[] DocketInvoice { get { return diDocketInvoice; } set { diDocketInvoice = value; } }

        public string EntrySheetNo { get { return entrysheetno; } set { entrysheetno = value; } }
        public string OBDNo { get { return obdno; } set { obdno = value; } }
        public string PermitNo { get { return permitno; } set { permitno = value; } }
        public DateTime PermitDate { get { return permitdate; } set { permitdate = value; } }
        public DateTime PermitValidityDate { get { return permitvaliditydate; } set { permitvaliditydate = value; } }
        public DateTime PermitRecieveDate { get { return permitrecvdate; } set { permitrecvdate = value; } }
        public string PermitRecievedAt { get { return permitrecvdat; } set { permitrecvdat = value; } }
        public string InvoiceNo { get { return invno; } set { invno = value; } }
        public DateTime InvoiceDate { get { return invdate; } set { invdate = value; } }
        public Int64 InvoiceAmount { get { return invamt; } set { invamt = value; } }
        public string BACode { get { return bacode; } set { bacode = value; } }
        public string PolicyNo { get { return policyno; } set { policyno = value; } }
        public DateTime PolicyDate { get { return policydate; } set { policydate = value; } }
        public double InternalCovers { get { return internalcovers; } set { internalcovers = value; } }
        public double ModvatCovers { get { return modvatecovers; } set { modvatecovers = value; } }
        public string CustomerRefNo { get { return custrefno; } set { custrefno = value; } }
        public DateTime CustomerRefDate { get { return custrefdate; } set { custrefdate = value; } }

        public string CustRefGPNo { get { return custrefgpno; } set { custrefgpno = value; } }
        public string CustRefDelNo { get { return custrefdelno; } set { custrefdelno = value; } }
        public string EngineNo { get { return _EngineNo; } set { _EngineNo = value; } }
        public string ModelNo { get { return _ModelNo; } set { _ModelNo = value; } }
        public string GPSNo { get { return _GPSNo; } set { _GPSNo = value; } }
        public string ChassisNo { get { return _ChassisNo; } set { _ChassisNo = value; } }
        public string Industry { get { return _Industry; } set { _Industry = value; } }


        public string ContainerID { get { return contid; } set { contid = value; } }
        public string SealNo { get { return sealno; } set { sealno = value; } }
        public string OrderID { get { return cntorderno; } set { cntorderno = value; } }
        public string PLPartner { get { return partner; } set { partner = value; } }
        public string PLCostID { get { return costid; } set { costid = value; } }

        public string ManualDockNo { get { return manual_dockno; } set { manual_dockno = value; } }
        public string PrimaryCompanyDockNo { get { return _PrimaryCompanyDockNo; } set { _PrimaryCompanyDockNo = value; } }

        public string PrivateMark { get { return privatemark; } set { privatemark = value; } }
        public string TPNumber { get { return tpnumber; } set { tpnumber = value; } }
        public string RiskType { get { return risktype; } set { risktype = value; } }
        public Int64 NoOfPkgs { get { return noofpkgs; } set { noofpkgs = value; } }
        public Int64 ActualWeight { get { return actualweight; } set { actualweight = value; } }
        public Int64 ChargedWeight { get { return chargedweight; } set { chargedweight = value; } }
        public double CODDODCharged { get { return coddodcharged; } set { coddodcharged = value; } }
        public double CODDODAmount { get { return coddodamount; } set { coddodamount = value; } }

        public double OctroiAmount { get { return oct_amt; } set { oct_amt = value; } }

        public double DACCCharged { get { return dacccharged; } set { dacccharged = value; } }
        public DateTime EDD { get { return edd; } set { edd = value; } }
        public string STaxPaidBy { get { return staxpaidby; } set { staxpaidby = value; } }
        public string STaxRegNo { get { return staxregno; } set { staxregno = value; } }
        public string LocalSTaxRegNo { get { return localstaxregno; } set { localstaxregno = value; } }
        public string BilledAt { get { return billedat; } set { billedat = value; } }
        public string SourceDockNo { get { return sourcedockno; } set { sourcedockno = value; } }
        public string FlagMultiPickUp { get { return flagmultipickup; } set { flagmultipickup = value; } }
        public string FlagMultiDelivery { get { return flagmultidelivery; } set { flagmultidelivery = value; } }
        public string FlagStaxExmpt { get { return flagstaxexmpt; } set { flagstaxexmpt = value; } }
        public string PartyAs { get { return partyas; } set { partyas = value; } }
        public string DocType { get { return doctype; } set { doctype = value; } }
        public string EntryBy { get { return entryby; } set { entryby = value; } }

        public string CompanyCode { get { return companycode; } set { companycode = value; } }

        public DateTime EntryDate { get { return entrydate; } set { entrydate = value; } }

        public DateTime SDD_Date { get { return sdd_date; } set { sdd_date = value; } }
        public string SDD_Date_str { get { return sdd_date_str; } set { sdd_date_str = value; } }

        public string MailId { get { return EmailID; } set { EmailID = value; } }

        //START GST Changes Chirag D
        public string OriginStateCode { get { return originstatesode; } set { originstatesode = value; } }
        public string DestStateCode { get { return destdtatecode; } set { destdtatecode = value; } }
        public bool IsUnionTeritory { get { return isunionteritory; } set { isunionteritory = value; } }

        public string OriginStateName { get; set; }
        public string DestStateName { get; set; }
        public string Origin_Area { get; set; }
        public string Destination_Area { get; set; }
        public string CustGSTNo { get; set; }
        public string CSGECustGSTNo { get; set; }
        public bool IsCompletion { get; set; }
        public string BillingState { get; set; }
        public string EWayBillNo { get; set; }
        public Nullable<DateTime> EWayBillExpiredDate { get; set; }
        public decimal EWayInvoicevalue { get; set; }
        public bool ISCounterPickUpPRS { get { return isCounterPickUpPRS; } set { isCounterPickUpPRS = value; } }
        public bool ISCounterDelivery { get { return isCounterDelivery; } set { isCounterDelivery = value; } }
        public string deliveryAddress { get; set; }

        public bool IsEwayBillPartA { get { return isewaybillparta; } set { isewaybillparta = value; } }


        //END GST Changes Chirag D
    }

    public class DocketCharges
    {
        /* SB Cess */
        public DocketCharges()
        {
            SBCess = 0; SbcRate = 0;
            CODODCAmt = 0; DACCAmt = 0; FOVAmt = 0;
        }

        /* SB Cess */
        //private ChargeCodeValue[] _Charges = new ChargeCodeValue[34];
        private List<QuickDocketBalancecharges> _Charges = new List<QuickDocketBalancecharges>();
        private double _Freight, _SubTotal, _ServiceTax, _EduCess, _HEduCess, _DocketTotal, _FreightRate;
        private double _FOVRate, _ServiceTaxRate;
        string _RateType, _DockNo;

        public decimal CODODCAmt { get; set; }
        public decimal DACCAmt { get; set; }
        public decimal FOVAmt { get; set; }

        /* SB Cess */
        public double SbcRate { get; set; }
        public double SBCess { get; set; }
        /* SB Cess */

        /* KKCRate */
        public double KKCRate { get; set; }
        public double KKCAmount { get; set; }
        /* KKCRate */

        /* GreenTax */
        public double GreenTax { get; set; }

        /* GreenTax */


        public string DockNo
        {
            get { return _DockNo; }
            set { _DockNo = value; }
        }

        public List<QuickDocketBalancecharges> Charges
        {
            get { return _Charges; }
            set { _Charges = value; }
        }

        public double FreightRate
        {
            get { return _FreightRate; }
            set { _FreightRate = value; }
        }

        public double FOVRate
        {
            get { return _FOVRate; }
            set { _FOVRate = value; }
        }

        public string RateType
        {
            get { return _RateType; }
            set { _RateType = value; }
        }

        public double Freight
        {
            get { return _Freight; }
            set { _Freight = value; }
        }

        public double SubTotal
        {
            get { return _SubTotal; }
            set { _SubTotal = value; }
        }

        public double ServiceTax
        {
            get { return _ServiceTax; }
            set { _ServiceTax = value; }
        }

        public double EduCess
        {
            get { return _EduCess; }
            set { _EduCess = value; }
        }

        public double HEduCess
        {
            get { return _HEduCess; }
            set { _HEduCess = value; }
        }

        public double DocketTotal
        {
            get
            { return _DocketTotal; }
            set { _DocketTotal = value; }
        }

        public double ServiceTaxRate
        {
            get
            { return _ServiceTaxRate; }
            set { _ServiceTaxRate = value; }
        }

        /* Start GST Changes Chirag D  */
        public decimal IGSTRate { get; set; }
        public decimal IGSTAmount { get; set; }
        public decimal CGSTRate { get; set; }
        public decimal CGSTAmount { get; set; }
        public decimal SGSTRate { get; set; }
        public decimal SGSTAmount { get; set; }
        public decimal UTGSTRate { get; set; }
        public decimal UTGSTAmount { get; set; }
        public decimal IGSTCOLLECTED { get; set; }
        public decimal CGSTCOLLECTED { get; set; }
        public decimal SGSTCOLLECTED { get; set; }
        public decimal UGSTCOLLECTED { get; set; }
        public string GSTType { get; set; }
        /* End GST Changes Chirag D  */
    }

    public class DocketInvoice
    {

        private string _DockNo, _InvoiceNo, _Part_No, _ProductName, _ProductDesc, _HSNCode, _PKGSTY, _PRODCD;
        DateTime _InvoideDate;
        Int64 _DeclaredValue, _NoOfPkgs, _InvoiceAmount, _ActualWeight, _Vol_Length, _Vol_Breadth, _Vol_Height, _Qty;
        Int64 _Vol_CFT;
        Int64 _Total_CFT;

        decimal _UnitPrice;
        decimal _GSTRate;

        public string DockNo
        {
            get
            { return _DockNo; }
            set { _DockNo = value; }
        }
        public string InvoiceNo
        {
            get
            { return _InvoiceNo; }
            set { _InvoiceNo = value; }
        }

        public DateTime InvoiceDate
        {
            get { return _InvoideDate; }
            set { _InvoideDate = value; }
        }

        public Int64 DeclaredValue
        {
            get { return _DeclaredValue; }
            set { _DeclaredValue = value; }
        }
        public Int64 NoOfPkgs
        {
            get { return _NoOfPkgs; }
            set { _NoOfPkgs = value; }
        }

        public Int64 InvoiceAmount
        {
            get { return _InvoiceAmount; }
            set { _InvoiceAmount = value; }
        }

        public Int64 ActualWeight
        {
            get { return _ActualWeight; }
            set { _ActualWeight = value; }
        }

        public Int64 Vol_Length
        {
            get { return _Vol_Length; }
            set { _Vol_Length = value; }
        }

        public Int64 Vol_Breadth
        {
            get { return _Vol_Breadth; }
            set { _Vol_Breadth = value; }
        }

        public Int64 Vol_Height
        {
            get { return _Vol_Height; }
            set { _Vol_Height = value; }
        }

        public Int64 Vol_CFT
        {
            get { return _Vol_CFT; }
            set { _Vol_CFT = value; }
        }

        public Int64 Total_CFT
        {
            get { return _Total_CFT; }
            set { _Total_CFT = value; }
        }
        public string Part_No
        {
            get
            { return _Part_No; }
            set { _Part_No = value; }
        }



        public string ProductName
        {
            get
            { return _ProductName; }
            set { _ProductName = value; }
        }
        public string ProductDesc
        {
            get
            { return _ProductDesc; }
            set { _ProductDesc = value; }
        }
        public string HSNCode
        {
            get
            { return _HSNCode; }
            set { _HSNCode = value; }
        }
        public Int64 Qty
        {
            get
            { return _Qty; }
            set { _Qty = value; }
        }
        public decimal UnitPrice
        {
            get
            { return _UnitPrice; }
            set { _UnitPrice = value; }
        }
        public decimal GSTRate
        {
            get
            { return _GSTRate; }
            set { _GSTRate = value; }
        }
        public string PKGSTY
        {
            get
            { return _PKGSTY; }
            set { _PKGSTY = value; }
        }
        public string PRODCD
        {
            get
            { return _PRODCD; }
            set { _PRODCD = value; }
        }




    }

    public class DocketDocument
    {
        private string dockno, docksf, documentno;
        private double srno;
        public string DockNo
        {
            get
            { return dockno; }
            set { dockno = value; }
        }
        public string DockSF
        {
            get
            { return docksf; }
            set { docksf = value; }
        }
        public double SRNo
        {
            get
            { return srno; }
            set { srno = value; }
        }
        public string DocumentNo
        {
            get
            { return documentno; }
            set { documentno = value; }
        }

    }


    public class BarCodeSerial
    {
        string dockno, bcfrom, bcto;
        public BarCodeSerial()
        {
            dockno = ""; bcfrom = ""; bcto = "";
        }
        public string DockNo { get { return dockno; } set { dockno = value; } }
        public string BCFrom { get { return bcfrom; } set { bcfrom = value; } }
        public string BCTo { get { return bcto; } set { bcto = value; } }
    }

    #endregion

    #endregion

    #region Challan

    public partial class PRSDRSGC
    {
        public int ID { get; set; }
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
        public string Docket_Mode { get; set; }
        public string Bkg_Date { get; set; }
        public string Arrival_Date { get; set; }
        public string Commited_Dely_Date { get; set; }
        public string ORGNCD { get; set; }
        public string DEST_CD { get; set; }
        public string Curr_Loc { get; set; }
        public int PendPkgQty { get; set; }
        public decimal ArrPkgQty { get; set; }
        public decimal PKGSNO { get; set; }

        public string PayBas { get; set; }
        public string PAYBAS_Str { get; set; }
        //public DateTime DockDT { get; set; }
        public DateTime ATAD { get; set; }
        public DateTime CDELDT { get; set; }
        public string businesstype { get; set; }

        public string TRN_MOD { get; set; }
        public decimal ACTUWT { get; set; }
        public decimal ArrWeightQty { get; set; }
        public decimal CHRGWT { get; set; }
        public decimal Freight { get; set; }
        public decimal DKTTOT { get; set; }
        public decimal Handlingchrg { get; set; }

        public decimal SVCTAX { get; set; }
        public int CND { get; set; }
        public string Staff_BA { get; set; }

        public bool IsEnabled { get; set; }
        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }
        public decimal NewRate { get; set; }
        public string ratetype { get; set; }
        public int CNT { get; set; }
        public string Message { get; set; }
        public string EWayBillNo { get; set; }

        public decimal SCHG08 { get; set; } //Door Delivery Charges
        public decimal SCHG07 { get; set; } //Door Collection Charges
        public decimal UCHG07 { get; set; } //Cartage Charges
        public bool IsDoorDelChargeIncluded { get; set; }
        public bool IsDoorColChargeIncluded { get; set; }
        public bool IsCartageChargeIncluded { get; set; }
    }

    public partial class VendorType
    {
        public string Vendor_Type_Code { get; set; }
        public string Vendor_Type { get; set; }
        public int DisplayIndex { get; set; }
    }

    public partial class Vendors
    {
        public string Vendor_Code { get; set; }
        public string Vendor_Name { get; set; }

    }

    public partial class Tripsheet
    {
        public string CodeSlipNo { get; set; }
        public string VSlipNo { get; set; }
        public string Manual_TripSheetNo { get; set; }
    }

    public partial class VehicleType
    {
        public string TypeCode { get; set; }
        public string Type_Name { get; set; }
    }

    public partial class FTLType
    {
        public string codeid { get; set; }
        public string codedesc { get; set; }
    }

    public partial class ChallanCharges
    {
        public string chargecode { get; set; }
        public string chargename { get; set; }
        public string Operator { get; set; }
        public string Acccode { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal Cnt { get; set; }
    }

    public partial class RouteSchedule
    {
        public decimal srno { get; set; }
        public string rutcd { get; set; }

        public decimal Schdep_HR { get; set; }
        public decimal Schdep_min { get; set; }
        public string Is_Scheduled_rut { get; set; }
        public string Schno { get; set; }

        public string Schtype { get; set; }
        public string Day_name { get; set; }
        public string SchDT { get; set; }
        public string SchTime { get; set; }
    }

    public partial class Routes
    {
        public string MyRouteCode { get; set; }
        public string MyRouteName { get; set; }
        public int Seq { get; set; }
    }

    public partial class MFDetails
    {
        public string TCNO { get; set; }
        public string Manual { get; set; }
        public string TCBR { get; set; }
        public string TC_Date { get; set; }
        public string ToBH_CODE { get; set; }
        public decimal TOT_DKT { get; set; }
        public string Packages { get; set; }
        public string Weight { get; set; }
        public decimal TOT_LOAD_PKGS { get; set; }
        public decimal TOT_LOAD_ACTWT { get; set; }
        public string MyRouteName { get; set; }
        public string tcdt_ddmmyyyy { get; set; }
        public bool IsEnabled { get; set; }
        public int TotalInternalDocument { get; set; }
    }

    public class vw_Vehicles
    {
        public string VEHNO { get; set; }
        public string RCBookNo { get; set; }
        public decimal usedcapacity { get; set; }
        public string InsuranceValDt { get; set; }
        public string Fuel_Type { get; set; }
        public string FTLTYPE_NAME { get; set; }
        public string VEHPRMDT { get; set; }
        public string Vehicle_Type_Active { get; set; }
        public decimal Rate_Per_KM { get; set; }
        public decimal Capacity { get; set; }
        public string RegistrationDt { get; set; }
        public decimal Payload { get; set; }
        public string EngineNo { get; set; }
        public string FTLTYPe { get; set; }
        public string Vehicle_Type { get; set; }
        public string Vehicle_Active { get; set; }
        public string ChasisNo { get; set; }
        public string VendorCode { get; set; }
        public string Type_Name { get; set; }
        public string FitnessValDt { get; set; }

        public decimal StartKM { get; set; }

    }

    public class Drivers
    {
        public string VSlipNo { get; set; }
        public string Driver1 { get; set; }
        public string Licno1 { get; set; }
        public string Validity_Date1 { get; set; }
        public string RTO1 { get; set; }
        public string MOB1 { get; set; }
        public string Driver2 { get; set; }
        public string Licno2 { get; set; }
        public string Validity_Date2 { get; set; }
        public string RTO2 { get; set; }
        public string MOB2 { get; set; }
    }

    public class BookedBy
    {
        public string AllotToCode { get; set; }
        public string AllotToName { get; set; }
        public string AllotCat { get; set; }


    }

    public partial class ChallanViewModel
    {
        public CYGNUS_THC_Header CTH { get; set; }
        public CYGNUS_THC_Financial_Details CTFD { get; set; }
        public CYGNUS_THC_Vehicle_Details CTVD { get; set; }
        public List<PRSDRSGC> DocketList { get; set; }
        public List<VendorType> ListVendorType { get; set; }
        public List<ChallanCharges> ListCharges { get; set; }
        //public List<webx_VEHICLE_HDR> ListVehicle { get; set; }
        //public string DOCNO { get; set; }
        public string THCNo { get; set; }
        public bool IsMathadi { get; set; }
        public string BookedByType { get; set; }
        public decimal RatePerGram { get; set; }
        public decimal RatePerGramContractAmount { get; set; }
        public bool ISAttechedVendor { get; set; }
        public bool ISContractualVendor { get; set; }
        public CYGNUS_PDC_MaxLimit CPML { get; set; }
        public string RateType { get; set; }
    }

    #endregion

    #region PoGeneration SKU

    public class PoGenerationViewModel
    {
        public List<webx_GENERAL_POASSET_det> Listwebx_GENERAL_POASSET_det = new List<webx_GENERAL_POASSET_det>();
        public webx_GENERAL_POASSET_det webx_GENERAL_POASSET_det = new webx_GENERAL_POASSET_det();
        public webx_GENERAL_POASSET_HDR webx_GENERAL_POASSET_HDR = new webx_GENERAL_POASSET_HDR();
        public List<webx_location> Listwebx_location = new List<webx_location>();
        public List<Webx_Master_General> POFROM = new List<Webx_Master_General>();
        public string POGenerationType { get; set; }
    }

    #endregion

    public class POApprovalViewModel
    {
        public string ManualGRNNO { get; set; }
        public string GRNNO { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string VendorCode { get; set; }
        public string POCode { get; set; }
        public string ManualPO { get; set; }
        public string POType { get; set; }
        public string FilterType { get; set; }
        public decimal POApprovalLimit { get; set; }
        public string ISPOApproval { get; set; }
        public string CompanyCode { get; set; }
        public DateTime PoApproveDate { get; set; }
        public string MatCat { get; set; }
        public string Location { get; set; }
        public string RO { get; set; }
        public string ROLocation { get; set; }
        public decimal srno { get; set; }
        public string GetEntryNo { get; set; }
        public DateTime CancelDate { get; set; }
        public string STAXNo { get; set; }
        public string PANNO { get; set; }
        public string AcctHead { get; set; }
        public string TDSType { get; set; }
        public string TDSFOR { get; set; }
        public string CancelReason { get; set; }
        public List<webx_GENERAL_POASSET_HDR> POList = new List<webx_GENERAL_POASSET_HDR>();
        public webx_GENERAL_POASSET_HDR GPH = new webx_GENERAL_POASSET_HDR();
        public List<webx_GENERAL_POASSET_det> POdetList = new List<webx_GENERAL_POASSET_det>();
        public webx_GENERAL_POASSET_det GPD = new webx_GENERAL_POASSET_det();
        public webx_GENERAL_WorkASSET_HDR WAH { get; set; }
        public List<webx_GENERAL_WorkASSET_HDR> WAHList { get; set; }
    }

    public class SKUPOEditViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<webx_GENERAL_POASSET_HDR> POList = new List<webx_GENERAL_POASSET_HDR>();
        public webx_GENERAL_POASSET_HDR GPH = new webx_GENERAL_POASSET_HDR();
        public Tempwebx_GENERAL_POASSET_HDR WGPH = new Tempwebx_GENERAL_POASSET_HDR();
        public List<webx_GENERAL_POASSET_det> POdetList = new List<webx_GENERAL_POASSET_det>();
        public webx_GENERAL_POASSET_det GPD = new webx_GENERAL_POASSET_det();
        public string POType { get; set; }
        public string FilterType { get; set; }
        public string VENDORCD { get; set; }
        public string pocode { get; set; }
        public decimal POApprovalLimit { get; set; }
    }

    public class Tempwebx_GENERAL_POASSET_HDR
    {
        public string POFROM { get; set; }
        public string assetcd { get; set; }
        public string MatCat { get; set; }
        public string pocode { get; set; }
        public DateTime podate { get; set; }
        public DateTime SpotDelDT { get; set; }
        public string VENDORCD { get; set; }
        public string Manual_PO_No { get; set; }
        public string attn { get; set; }
        public string Location { get; set; }
        public string FabricSheetPO_YN { get; set; }
        public DateTime ValideDate { get; set; }
        public decimal totalamt { get; set; }
        public string terms_condition { get; set; }
        public decimal Excise_Duty { get; set; }
        public decimal Vat_S_Tax { get; set; }
        public string remarks { get; set; }
        public decimal Freight_Ins { get; set; }
        public decimal Octroi { get; set; }
        public string Specification { get; set; }
        public decimal Packing_charge { get; set; }
        public decimal PAIDAMT { get; set; }
        public decimal pendamt { get; set; }
        public string PaymentTerms { get; set; }
        public decimal totalqty { get; set; }
        public decimal PO_advamt { get; set; }
        public decimal PO_balamt { get; set; }
        public decimal tax_per { get; set; }
        public decimal TaxAmt { get; set; }
        public decimal DiscountAmt { get; set; }
        public decimal Discount_Per { get; set; }
    }

    #region Bill Submission

    public class BillingFilterViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string FilterType { get; set; }
        public string BillingParty { get; set; }
        public string Paybasis { get; set; }
        public string Transport { get; set; }
        public string DocketNo { get; set; }
        public string StaxFilter { get; set; }

        public bool IsBillSub { get; set; }
        public bool IsBillColl { get; set; }

        public bool IsMiscBill { get; set; }
        public bool IsRentBill { get; set; }
        public bool InternationalBill { get; set; }

        public string BillNo { get; set; }
        public string ManualBillNo { get; set; }
        public string BillSubNo { get; set; }
        public string BillType { get; set; }

        public DateTime BSBDT { get; set; }
        public string BillUnsubmitReason { get; set; }
        public string BILLSUBDOCNO { get; set; }
    }

    public class BillSubmissionViewModel
    {

        public List<webx_BILLMST> BillList { get; set; }

        public string BILLSUBTO { get; set; }
        public string SUBTOTEL { get; set; }
        public DateTime BillSubDate { get; set; }
        public string Phoneno { get; set; }

        public string BILLSUBDOCNO { get; set; }
        public string BillUnsubmitReason { get; set; }

        public string Party { get; set; }
        public string BillType { get; set; }
    }

    #endregion

    #region Bill Collection

    public class BillCollectionViewModel
    {

        public List<webx_BILLMST> BillList { get; set; }
        public webx_BILLMST ObjBillMst { get; set; }

        public int TOTBILL { get; set; }
        public string stringBIllNO { get; set; }
        public string PAYBAS { get; set; }
        public string MRSNO { get; set; }
        public string ManualMRNO { get; set; }
        public DateTime MRDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Remarks { get; set; }
        public string SecurityLedger { get; set; }
        public string TDSLedger { get; set; }
        public string TransactionId { get; set; }
        public string MRBranch { get; set; }
        public string PartyAddress { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }

        public CYGNUS_Collection_Payment CCP { get; set; }
        public PaymentControl PC { get; set; }
        public List<WEBX_MR_VPLIST> mrList { get; set; }

    }

    public partial class GetCashDetailList
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public partial class GetBankDetailList
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public partial class GetCONTRADetailList
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    #endregion

    public class SingleCNoteUpdatViewModel
    {
        public vw_DRS_Summary DRSSummary { get; set; }
        public vw_SingleCNoteUpdatList SingleCnote { get; set; }
        public List<vw_SingleCNoteUpdatList> CNoteUpdatList { get; set; }
        public List<vw_SingleCNoteUpdatList> ListCNoteUpdat { get; set; }
    }
    public class vw_SingleCNoteUpdatList
    {
        public string DOCKNO { get; set; }
        public string DRS { get; set; }
        public string Delivered { get; set; }
        public DateTime DRSUP_DT { get; set; }
        public string Op_Status { get; set; }
        public string DELYPERSON { get; set; }
        public int CODCollected { get; set; }
        public string DELYREASON { get; set; }
        public string CodeDesc { get; set; }
        public Nullable<decimal> PKGSNO { get; set; }
        public string mobileno { get; set; }

        public string DOCKSF { get; set; }
        public string DOCKDT { get; set; }
        public string DRS_DT { get; set; }

    }
    #region Banking Operation
    public class ChequeViewModel
    {
        public Cheque_det ChequeDetail { get; set; }
        public List<Cheque_det> ListChequeBounceDocument { get; set; }
        public List<Cheque_det> ListChequeDetail { get; set; }
        public List<Cheque_det> ListChequeDepositDetail { get; set; }
    }
    public class Cheque_det
    {
        public string ptmsptcd { get; set; }
        public string bacd { get; set; }
        public string Chq_Direct_Depo { get; set; }
        public string chqsf { get; set; }
        public string recdbrnm { get; set; }
        public string DepoFlag { get; set; }
        public string brcd { get; set; }
        public string banm { get; set; }
        public string empcd { get; set; }
        public string comments { get; set; }
        public string voucherNo { get; set; }
        public double adjustamt { get; set; }
        public string recdbrcd { get; set; }
        public string OwnCust { get; set; }
        public string chq_clear { get; set; }
        public string chq_b_r { get; set; }
        public int chqindex { get; set; }
        public string chqno { get; set; }
        public DateTime Entrydt { get; set; }
        public string acccode { get; set; }
        public DateTime chq_bounce_dt { get; set; }
        public decimal chqamt { get; set; }
        public string chq_flag { get; set; }
        public double Chq_BounceAmt { get; set; }
        public string chq_status { get; set; }
        public string Chq_Bank_Reco_O_N { get; set; }
        public string Comment { get; set; }
        public string CHQSTATUS { get; set; }
        public string staffcd { get; set; }
        public DateTime transdate { get; set; }
        public string brnm { get; set; }
        //public DateTime chqdt { get; set; }
        public string chqdt { get; set; }
        public string Onaccount_YN { get; set; }
        public string depoloccode { get; set; }
        public string chq_trf { get; set; }
        public DateTime chqcleardate { get; set; }
        public string empnm { get; set; }
        public string ptmsptnm { get; set; }
        public string Banknm { get; set; }
        public int chq_depattempt { get; set; }
        public string staffnm { get; set; }
        public DateTime chq_reoffer_dt { get; set; }

        public int Id { get; set; }
        public decimal clamt { get; set; }
        public string bankbrn { get; set; }
        //public string Voucherno { get; set; }
        public string debit { get; set; }
        public string credit { get; set; }
        public string PBOV_name { get; set; }
        public string trndate { get; set; }

        public string Mrsno { get; set; }
        public string Mrsdt { get; set; }
        public string Location { get; set; }
        public decimal Mrsamt { get; set; }
        public string Finclosedt { get; set; }
        public string Party { get; set; }
        public decimal Netamt { get; set; }

        public DateTime ChqIssueddate { get; set; }
    }

    #endregion

    #region Bank Reconcillation Module
    public class WebxAcctransViewModel
    {
        public decimal asd { get; set; }
        public webx_acctrans_Model ObjAcconttran { get; set; }
        public List<webx_acctrans_Model> ListAcconttranCreadit { get; set; }
        public List<webx_acctrans_Model> ListAcconttranDebit { get; set; }
        public string Bankacccode { get; set; }
    }
    //public class webx_acctrans_TDS_Model
    //{
    //    public int Id { get; set; }
    //    public string DOCNO { get; set; }
    //    public DateTime DOCDT { get; set; }
    //    public DateTime DOCDT1 { get; set; }
    //    public string PARTY { get; set; }
    //    public string TRANSTYPE { get; set; }
    //    public string CUSTEMAILIDS { get; set; }
    //    public string CHQNO { get; set; }
    //    public DateTime CHQDT1 { get; set; }
    //    public decimal SUBTOTAL { get; set; }
    //    public string TDS { get; set; }
    //    public string TDS_RATE { get; set; }
    //    public string PAN_NO { get; set; }
    //    public string TDSACCCODE { get; set; }
    //    public string SRNO { get; set; }
    //    public bool Chk { get; set; }

    //}
    public class webx_acctrans_Model
    {
        public int Id { get; set; }
        public int transNo { get; set; }//int
        public string Payto { get; set; }
        public string ReceivedFrom { get; set; }
        public string Deptcode { get; set; }
        public string Chqno { get; set; }
        public string Narration { get; set; }
        public string Voucherno { get; set; }
        public string servicetaxNo { get; set; }
        public string BILLNO { get; set; }
        public string voucher_status { get; set; }
        public DateTime v_approve_reject_dt { get; set; }
        public string Docsf { get; set; }
        public decimal Credit { get; set; }
        public string Acccode { get; set; }
        public decimal Debit { get; set; }
        public string Currbalance { get; set; }
        public string coastCode { get; set; }
        public string Chqdate { get; set; }

        public string Transdate { get; set; }
        public string DocType { get; set; }
        public string Brcd { get; set; }
        public DateTime Chqcleardate { get; set; }

        public decimal Srno { get; set; }
        public Int64 RowNo { get; set; }
        public string comment { get; set; }
        public string coasttype { get; set; }
        public bool Chk { get; set; }
        public string Party { get; set; }
        public string transtype { get; set; }

        public DateTime ChqIssueddate { get; set; }
        public string Flag { get; set; }

    }
    #endregion

    #region TDS
    public class TDSRecolicationViewModel
    {
        public DateTime DocumentDate { get; set; }
        public string VoucherNo { get; set; }
        public string MRNo { get; set; }
        public string CustomerNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public List<webx_acctrans_TDS_Model> ListAccountTran { get; set; }

    }
    public class webx_acctrans_TDS_Model
    {
        public int Id { get; set; }
        public string DOCNO { get; set; }
        // public DateTime DOCDT { get; set; }
        // public DateTime DOCDT1 { get; set; }

        public string BILLNO { get; set; }
        public string DOCDT { get; set; }
        public string DOCDT1 { get; set; }
        public string PARTY { get; set; }
        public string TRANSTYPE { get; set; }
        public string CUSTEMAILIDS { get; set; }
        public string CHQNO { get; set; }
        // public DateTime CHQDT1 { get; set; }
        public string CHQDT1 { get; set; }
        public decimal SUBTOTAL { get; set; }
        public decimal TDS_RATE { get; set; }
        public string PAN_NO { get; set; }
        public string TDSACCCODE { get; set; }
        public decimal SRNO { get; set; }
        public bool Chk { get; set; }
        public decimal TDS { get; set; }
        public string ENTRYBY { get; set; }
        public string ENTRYDT { get; set; }

    }
    #endregion

    #region Octroi Finalization Bill View Model

    public class OctroiFinalizationBillViewModel
    {
        public List<webx_location> WLocationList { get; set; }
        public List<webx_location> WLocationROList { get; set; }

        public string RO { get; set; }
        public string AO { get; set; }
        public string LO { get; set; }
        public string Bank { get; set; }
        public string Vendor { get; set; }
        public Nullable<decimal> LocLevel { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


        public string BillType { get; set; }
        public string OctroiBillNo { get; set; }
        public List<OctroiFinalizationBill> ListOctroiFinalizationBill { get; set; }
    }
    public class OctroiFinalizationBill
    {
        public string BillNo { get; set; }
        public string BillDate { get; set; }
        public string PartyName { get; set; }
        public decimal BillAmount { get; set; }
        public bool chk { get; set; }
        public int Id { get; set; }
        public string Billtype { get; set; }
    }

    #endregion

    #region THC Edit

    public class THCViewModel
    {
        public webx_THC_SUMMARY THCSUMRY { get; set; }
        public List<webx_master_charge> ListSUMARY { get; set; }
        public List<webx_THC_SUMMARY> ListTHCChargeCode { get; set; }
    }
    #endregion

    #region PRS ARRIVAL

    public partial class PRSArrivalsFilterViewModel
    {
        public string DocNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string UnloadBy { get; set; }
        public string RateType { get; set; }

    }

    public partial class PRSArrivalsDetailViewModel
    {
        public string DOCKNO { get; set; }
        public string ORGNCD { get; set; }
        public string DESTCD { get; set; }
        public string PAYBASCD { get; set; }
        public string PAYBAS { get; set; }
        public decimal ACTUWT { get; set; }
        public decimal PKGSNO { get; set; }
        public decimal CHRGWT { get; set; }
        public DateTime DOCKDT { get; set; }
        public string UnloadBy { get; set; }
        public string RateType { get; set; }
        public string PDCNO { get; set; }
        public string vendorcode { get; set; }
        public string vendorname { get; set; }
        public string PDCDT { get; set; }
        public DateTime ArrivalDT { get; set; }
        public decimal Rate { get; set; }
        public decimal Tot_Rate { get; set; }
        public decimal Tot_Weight { get; set; }
        public decimal Tot_PKGSNO { get; set; }
        public decimal Tot_CHRGWT { get; set; }
        public decimal Tot_CHRG { get; set; }
        public decimal CHRG { get; set; }


        public decimal MaxLimit { get; set; }
        public string VendorCode_new { get; set; }
        public string VendorName_new { get; set; }
        public bool IsMonthly { get; set; }
        public decimal LoadingCharge { get; set; }
        public decimal hdnRate { get; set; }
        public int Id { get; set; }
        public string ratetype1 { get; set; }
        public decimal NewRate { get; set; }
        public bool IsEnabled { get; set; }
    }

    public partial class PRSArrivalsViewModel
    {
        public PRSArrivalsDetailViewModel PAVM { get; set; }
        public List<PRSArrivalsDetailViewModel> ListPAVM { get; set; }
    }
    #endregion

    #region GRN Generation ReAssign
    public class GRNGenerationReAssignviewModel
    {
        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }
        public Nullable<decimal> LocationLevel { get; set; }
        public string RegionCode { get; set; }
        public string LocationCode { get; set; }
        public string LocCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ITEMCODE { get; set; }
        public string DocumentNo { get; set; }
        public string UitilityType { get; set; }

        public List<POGRNGenerationDetails> POList { get; set; }
        public List<CYGNUS_FixAssetType> GRNLIST { get; set; }
        public List<POGRNTyreDetail> POTyreList { get; set; }
    }
    #endregion

    #region AssignAssetBranchWise

    public class AssignAssetBranchWise
    {
        public string loccode { get; set; }
        public string next_loccode { get; set; }
        public string assetname { get; set; }
        public string assgnempcd { get; set; }
        public string instdate { get; set; }
        public string nextloc_dept { get; set; }
        public string MFGSrNo { get; set; }
        public string RCPLSrNo { get; set; }
    }

    #endregion

    #region PRQ

    public class PRQViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PRQNo { get; set; }
        public string CustomerCode { get; set; }
        public string Driver { get; set; }
        public string SuperVisor { get; set; }
        public string PRQ_Status { get; set; }

        public CYGNUSPickupRequest ObjPRQ { get; set; }
        public List<CYGNUSPRQHistory> ListPRQHistory { get; set; }
        public CYGNUSPickupRequest ObjPRQReq { get; set; }
        public PRQQuickDocketGeneration ObjQuickDktGen { get; set; }
    }

    #endregion

    #region Docket Ho Verification

    public class DocketHoVerificationViewModel
    {
        public List<WebX_Master_Docket> DocketList { get; set; }
        public string DATEFROM { get; set; }
        public string DATETO { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
    }

    #endregion        

    #region Internal Movement

    public class InternalMovementViewModel
    {
        public List<CYGNUS_Internal_Movement> IntMovementList { get; set; }
        public CYGNUS_Internal_Movement objIntMovement { get; set; }
        public List<CYGNUS_TCTRN_IM> TCTRNList { get; set; }
        public CYGNUS_TCTRN_IM objTCTRN { get; set; }
    }

    #endregion

    #region Delivery MR

    public partial class vw_DeliveryMR
    {
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
        public DateTime dockdate { get; set; }
        public DateTime MRDate { get; set; }
        public string mrstype { get; set; }
        public string paymentbas { get; set; }
        public string service_class { get; set; }
        public string ftl_types { get; set; }
        public string businesstype { get; set; }
        public string orgncd { get; set; }
        public string delloc { get; set; }
        public string from_loc { get; set; }
        public string to_loc { get; set; }
        public string paybas { get; set; }
        public string party_code { get; set; }
        public string BillingPartyAS { get; set; }
        public string csgecd { get; set; }
        public string csgenm { get; set; }
        public string oldptcd { get; set; }
        public string oldptname { get; set; }
        public string receivercode { get; set; }
        public string receivername { get; set; }
        public Int32 pkgsno { get; set; }
        public string trn_mod { get; set; }
        public string prodcd { get; set; }
        public string diplomat { get; set; }
        public string stax_exmpt_yn { get; set; }
        public string stax_paidby { get; set; }
        public string dacc_yn { get; set; }
        public string cod_dod { get; set; }
        public string insuyn { get; set; }
        public string packtype { get; set; }
        public DateTime edd { get; set; }
        public decimal chrgwt { get; set; }
        public Int32 totpkgs { get; set; }
        public string curr_loc { get; set; }
        public decimal invamt { get; set; }
        public string godown { get; set; }
        public Int32 arrpkgqty { get; set; }
        public decimal arrweightqty { get; set; }
        public string doctype { get; set; }
        public bool IsAllowPartPayment { get; set; }
        public bool AllowPartialPayment { get; set; }

        public bool IsAllowPartDelivery { get; set; }
        public Int32 PartPkgQty { get; set; }
        public string DeliveryVehicle { get; set; }
        public string Remarks { get; set; }

        public string ChargeRule { get; set; }
        public string ChargeSubRule { get; set; }
        public string BaseCode1 { get; set; }
        public string BaseCode2 { get; set; }
        public string flagroundoff { get; set; }

        public decimal SubTotal { get; set; }
        public decimal BillpendingAmount { get; set; }
        public decimal MRAmount { get; set; }
        public decimal BkgSubTotal { get; set; }
        public decimal DelSubTotal { get; set; }
        public decimal FreightAmount { get; set; }
        public decimal DiffSubTotal { get; set; }
        public decimal CollectionAmount { get; set; }
        public string RATE_TYPE { get; set; }
        public string GSTType { get; set; }
        public string BillingState { get; set; }
        public string CSGEmobileno { get; set; }

        public string CustGSTNo { get; set; }
        public string CSGECustGSTNo { get; set; }
        public string CSGNTeleNo { get; set; }
        public string CSGETeleNo { get; set; }
        public string Moduletype { get; set; }
    }

    public class DeliveryMR_Criteria
    {
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
    }

    public partial class BookingCharges
    {
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public string Operator { get; set; }
        public string Acccode { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal DocketCharge { get; set; }
        public decimal ContractCharge { get; set; }
        public decimal CollectedCharge { get; set; }
        public bool IsDisabled { get; set; }
        public string paybas { get; set; }
    }

    public partial class DeliveryCharges
    {
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public string Operator { get; set; }
        public string Acccode { get; set; }
        public decimal ChargeAmount { get; set; }
        public decimal DefaultChargeAmount { get; set; }
        public bool IsDisabled { get; set; }
    }

    public partial class DeliveryMR_GSTCharges
    {
        public string DOCKNO { get; set; }
        public string RATE_TYPE { get; set; }
        public Nullable<decimal> FRT_RATE { get; set; }
        public Nullable<decimal> FREIGHT_CALC { get; set; }
        public Nullable<decimal> FREIGHT { get; set; }
        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> SVCTAX { get; set; }
        public Nullable<decimal> CESS { get; set; }
        public decimal DKTTOT { get; set; }
        public Nullable<decimal> hedu_cess { get; set; }
        public decimal SVCTAX_Rate { get; set; }
        public Nullable<decimal> BalanceAmount { get; set; }
        public Nullable<decimal> SbcRate { get; set; }
        public Nullable<decimal> SBCess { get; set; }
        public Nullable<decimal> KKCRate { get; set; }
        public Nullable<decimal> KKCAmount { get; set; }
        public Nullable<decimal> GreenTax { get; set; }
        public string stax_paidby { get; set; }
        public string GSTType { get; set; }
        public bool IsGSTApplied { get; set; }
        public decimal IGSTRate { get; set; }
        public decimal IGSTAmount { get; set; }
        public decimal IGSTCOLLECTED { get; set; }
        public decimal CGSTRate { get; set; }
        public decimal CGSTAmount { get; set; }
        public decimal CGSTCOLLECTED { get; set; }
        public decimal SGSTRate { get; set; }
        public decimal SGSTAmount { get; set; }
        public decimal SGSTCOLLECTED { get; set; }
        public decimal UTGSTRate { get; set; }
        public decimal UTGSTAmount { get; set; }
        public decimal UTGSTCOLLECTED { get; set; }
    }

    public class DeliveryViewModel
    {
        public vw_DeliveryMR Dtl { get; set; }
        public List<BookingCharges> ListBookingCharges { get; set; }
        public List<DeliveryCharges> ListDeliveryCharges { get; set; }
        //public webx_CUSTHDR WCM { get; set; }
        public List<CygnusChargesHeader> ListCCH { get; set; }
        public CYGNUS_Collection_Payment CCP { get; set; }
    }
    public class vw_DeliveryMRList
    {
        public string DOCKNO { get; set; }
        public string DRS { get; set; }
        public string Delivered { get; set; }
        public DateTime DRSUP_DT { get; set; }
        public string Op_Status { get; set; }
        public string DELYPERSON { get; set; }
        public int CODCollected { get; set; }
        public string DELYREASON { get; set; }
        public string CodeDesc { get; set; }
        public Nullable<decimal> PKGSNO { get; set; }
        public string mobileno { get; set; }

        public string DOCKSF { get; set; }
        public string DOCKDT { get; set; }
        public string DRS_DT { get; set; }

    }

    #endregion

    #region CrossingChallan
    public partial class CrossingChallanFilterViewModel
    {
        public string THCNo { get; set; }
        public string DockNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Orgncd { get; set; }
        public string DeliveryMethod { get; set; }
        public string VendorType { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string DateType { get; set; }
        public string DocType { get; set; }

        public List<webx_CrossingBill_DET> WCDB { get; set; }
        public webx_CrossingBill_DET WCD { get; set; }
        public List<webx_CrossingBill_MST> WCMB { get; set; }
        public webx_CrossingBill_MST WCM { get; set; }

    }

    public partial class CrossingChallanViewModel
    {
        public string THCNo { get; set; }
        public string DockNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Orgncd { get; set; }
        public string DeliveryMethod { get; set; }
        public string VendorType { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string DateType { get; set; }

        public Webx_Crossing_Docket_Master WCDM { get; set; }
        public List<Webx_Crossing_Docket_Detail> WCDD { get; set; }
        public Webx_Account_Details WAD { get; set; }
        public List<Webx_Account_Details> WADListing { get; set; }


    }

    public class CrossingChallanPouchViewModel
    {
        public CrossingChallanFilterViewModel CCF { get; set; }
        public List<Webx_Crossing_Docket_Master> WCDM { get; set; }
    }

    //public partial class CrossingDocketDetail
    //{
    //    public bool IsChecked { get; set; }
    //    public string DockNo { get; set; }
    //    public DateTime DockDate { get; set; }
    //    public string Origin { get; set; }
    //    public string Destination { get; set; }
    //    public string Paybas { get; set; }
    //    public double Pkgs { get; set; }
    //    public double ActuWeight { get; set; }
    //    public double ChargedWeight { get; set; }
    //    public double DKTTOT { get; set; }
    //    public string Ratetype { get; set; }
    //    public double VendorRate { get; set; }
    //    public double CrossingChrg { get; set; }
    //    public string DDRatetype { get; set; }
    //    public double DDVendorRate { get; set; }
    //    public double DoorDelyChrg { get; set; }
    //    public double BulkyChrg { get; set; }
    //    public string Bulky { get; set; }
    //    public string Remarks { get; set; }

    //}
    #endregion

    #region AutounloadingViewPrint
    public partial class THCAutounloadingViewPrintViewModel
    {
        public string THCNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string UnLoadingNo { get; set; }

    }
    #endregion

    #region IDTFreightMemo
    public partial class IDTFreightMemoCriteria
    {
        public string FMNO { get; set; }
        public string IDTNO { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Brcd { get; set; }

    }

    public partial class IDTFreightMemoViewModel
    {
        public List<IDTDetail> IDTDET { get; set; }
        public string FMNO { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string FMDATE { get; set; }
        public string Brcd { get; set; }
        public decimal TotalToPay { get; set; }
        public decimal TotalCrossing { get; set; }
        public decimal TotalOther { get; set; }
        public decimal NetPayable { get; set; }
        public decimal Deductions { get; set; }
        public decimal FinalNetPayable { get; set; }
        public string EntryBy { get; set; }
        public bool IsChecked { get; set; }
    }

    #endregion

    #region IDTFreightMemoPayment
    public partial class IDTFreightMemoPaymentViewModel
    {
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string Brcd { get; set; }
        public string VoucherNo { get; set; }
        public DateTime VoucherDate { get; set; }
        public decimal TotalAmtPayable { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal TotalNetPayable { get; set; }
        public string EntryBy { get; set; }
        public PaymentControl CCP { get; set; }
        public List<IDTFreightMemoViewModel> FMDET { get; set; }
    }
    #endregion

    #region CHQ_Register_Details
    public class CHQ_Register_Details
    {
        public string ChequeNo { get; set; }
        public string Chequedate { get; set; }

        public string Issuelocation { get; set; }
        public string Issuedate { get; set; }
        public string Depositlocation { get; set; }
        public string Depositdate { get; set; }
        public string Depositvoucherno { get; set; }
        public string Depositedfrom { get; set; }
        public decimal ChequeAmount { get; set; }
        public string RecivedFrom { get; set; }
        public decimal AccountedAmount { get; set; }
        public decimal UnAccountedAmount { get; set; }
        public List<CHQ_Register_Details_DET> ListDCD { get; set; }
    }

    public class CHQ_Register_Details_DET
    {
        public string docno { get; set; }
        public string chqdt { get; set; }
        public double chqamt { get; set; }
        public double clamt { get; set; }
        public string MRSTYPE { get; set; }
    }
    #endregion

    #region Accident/break Down
    public class THCCriteariaSelectionViewModel
    {
        public List<THCCriteariaSelection> ListTHCSelection { get; set; }
        public THCCriteariaSelection THCSelection { get; set; }
        public string EditFlag { get; set; }
    }
    #endregion
}
