﻿using CYGNUS.Models;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace FleetDataService.ViewModels
{
    public class BillingQueryModel
    {
        public string Paybas { get; set; }
        public string TransportMode { get; set; }
        public string BusinessType { get; set; }
        public string DocketNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BillingParty { get; set; }
        //public string DATETYPE { get; set; }
        public string StaxYN { get; set; }
        public decimal StaxRate { get; set; }
        public string CurLocation { get; set; }

        public bool IsOctroiBill { get; set; }
        public bool IsDAMBill { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string PartyType { get; set; }

        public string Bill_POD_Approval_YN { get; set; }
        public string Trans_Type { get; set; }
       public string DateType { get; set; }
        public GSTCriteria GC { get; set; }

        public string manual_dockno { get; set; }
        public string Service_Class { get; set; } 
    }


    /* START GST Changes By Chirag D */
    public class GSTCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string FilterType { get; set; }
        public string VendorCode { get; set; }
        public string CustomerCode { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool IsState { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsVendor { get; set; }
        public bool IsGSTType { get; set; }
        public bool IsGSTApply { get; set; }

        public string TransitMode { get; set; }
        public bool IsTransitMode { get; set; }
        public string DateType { get; set; }
        public string PAYBAS { get; set; }
        public string PARTY_CODE { get; set; }
        public string manual_dockno { get; set; }
        public string Service_Class { get; set; } 
    }

    /* END GST Changes By Chirag D */


    public class BillCustDetails
    {
        public string Custcd { get; set; }
        public string custnm { get; set; }
        public string custaddress { get; set; }
        public string telno { get; set; }
        public string emailids { get; set; }
        public string billsub_loccode { get; set; }
        public string billcol_loccode { get; set; }
        public decimal credit_limit { get; set; }
        public decimal credit_day { get; set; }
        public decimal OutStdAmt { get; set; }
        public decimal Generated { get; set; }
    }

    public class BillGeneration
    {
        public GSTCriteria GC { get; set; }
        public BillingQueryModel BQM { get; set; }
        public BillCustDetails CUSTHDR { get; set; }
        public webx_BILLMST BILLMST { get; set; }
        public List<BillDocket> DocketList { get; set; }

        public List<webx_BILLDET> BILLDET { get; set; }
        public Webx_Account_Details objWAD { get; set; }


        public bool IsMultipleDoc { get; set; }
        public bool IsApplyStax { get; set; }
        public bool IsAbatement { get; set; }
        public bool IsRoundOff { get; set; }

        public bool IsDAMBill { get; set; }
        public bool IsOctroiBill { get; set; }
        public bool IsOctroiStax { get; set; }
        public DateTime DeliveryDate { get; set; }

        public string BTWiseYN { get; set; }
        public string BIllPODApprovalYN { get; set; }
        public string Mindate { get; set; }
        public string Maxdate { get; set; }
        public string BackdaysYN { get; set; }
        public string BackDays { get; set; }
        public string BillLocation { get; set; }

        public string OutStdLimitAllow { get; set; }
        public string CRLimitAllow { get; set; }
        public string ManualBillnoMandatory { get; set; }
        public string DuplicateCheck { get; set; }
        public string BillCollLocNM { get; set; }
    }


    public class BillDocket
    {
        public bool IsEnabled { get; set; }
        public decimal DACC_CHRG { get; set; }
        public decimal DDCharge { get; set; }
        public decimal FOV { get; set; }
        public string diplomat { get; set; }
        public string modedesc { get; set; }
        public decimal FODCharge { get; set; }
        public decimal hld_chrg { get; set; }
        public string dockno { get; set; }
        public string CSGECD { get; set; }
        public decimal frt_rate { get; set; }
        public string fromto { get; set; }
        public string docknosf { get; set; }
        public string CSGNCD { get; set; }
        public string dock_cdeldt { get; set; }
        public string COD_DOD { get; set; }
        public decimal nform_chrg { get; set; }
        public string REASSIGN_DESTCD { get; set; }
        public decimal PODHOCLEARDATE { get; set; }
        public decimal DIPLOMATCHG { get; set; }
        public decimal CONTRACT_PARTY { get; set; }
        public decimal billstatus { get; set; }
        public decimal DKTCHG { get; set; }
        public decimal DCCharge { get; set; }
        public string CSGNNM { get; set; }
        public decimal CHRGWT { get; set; }
        public string paybasdesc { get; set; }
        public string CSGENM { get; set; }
        public int diesel_chrg { get; set; }
        public string FRT_chrg_RATE { get; set; }
        public decimal CODCHG { get; set; }
        public string dockdt { get; set; }
        public decimal hld_dlychrg { get; set; }
        public string DLY_LOC { get; set; }
        public decimal dkttot { get; set; }
        public decimal SVCTAX { get; set; }
        public decimal state_chrg { get; set; }
        public string trn_mod { get; set; }
        public decimal demchgs { get; set; }
        public decimal OTCHG { get; set; }
        public decimal DemAmtStax { get; set; }
        public decimal Total { get; set; }
        public decimal DemAmt { get; set; }
        public int Total_Storage_Day { get; set; }
        public decimal Total_Free_Days { get; set; }
        public decimal Actual { get; set; }
        public string ContractID { get; set; }

        public string PARTY_CODE { get; set; }
        public int AutoBillGenerated { get; set; }
        public string GSTType { get; set; }
        public string fincmplbr { get; set; }
        public string fincmplbrState { get; set; }
    }

    #region CNote Finalization

    public class FinalizationModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string RegionCode { get; set; }
        public string LocationCode { get; set; }
        public string PartyCode { get; set; }
        public string DocumentNo { get; set; }
        public string Type { get; set; }
        public string BaseFinYear { get; set; }
        public string SelectionType { get; set; }
        public Nullable<decimal> LocationLevel { get; set; }


        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }

        public List<CNoteFinalizationViewModel> ListCNFVM { get; set; }
        public List<BillFinalizationViewModel> ListBillFVM { get; set; }

        public List<MRFinalizationViewModel> ListMrFM { get; set; }
    }

    public class CNoteFinalizationViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string LRNo { get; set; }
        public string Origin { get; set; }
        public string PartyName { get; set; }
        public string LRDate { get; set; }
        public decimal Freight { get; set; }
        public decimal SubTotal { get; set; }
        public decimal LRTotal { get; set; }
        public bool IsChecked { get; set; }
        public DateTime Finalized_Date { get; set; }
    }

    public class CNoteBillFinalizationDoneViewModel
    {
        public string CnotNo { get; set; }
        public string BillNo { get; set; } 
        public string Status { get; set; }
    }

    public class BillFinalizationViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BillNo { get; set; }
        public string ManualBillNo { get; set; }
        public string PartyName { get; set; }
        public string BillDate { get; set; }
        public decimal BillAmount { get; set; }
        //public decimal SubTotal { get; set; }
        //public decimal LRTotal { get; set; }
        public bool IsChecked { get; set; }
        public int iSNO { get; set; }
    }

    public class MRFinalizationViewModel
    {   public string MRSNO { get; set; }
        public string BillNo { get; set; }
        public string PartyName { get; set; }
        public string MRSDT { get; set; }
        public string BillDate { get; set; }
        public decimal BillAmount{ get; set; }
        public decimal CollectedAmount { get; set; }
        public decimal CollectionMode { get; set; }
        public decimal ChqNo { get; set; }
        public decimal ChqDate { get; set; }
        public bool IsChecked { get; set; }
        public int iSNO { get; set; }
        public string paymode { get; set; }
        public string BankAcccode { get; set; }

    }

    #endregion

    #region Bill/BillEntry Adjustment

    public class BillEntryAdjustmentViewModel
    {
        public string AdjustmentBillNo { get; set; }
        public DateTime AdjustmentDate  { get; set; }
        public decimal AdjustmentPaymentAmount { get; set; }
        public decimal AdjustmentIncomeAmount { get; set; }
        public string AdjustmentLocation  { get; set; }
        public string BillType { get; set; }
        public string PARTY_CODE { get; set; }
        public DateTime CancellationDate { get; set; }
        public string CancellationReason { get; set; }

        public List<BillEntryModel> PaymentBillEntry { get; set; }
        public List<BillEntryModel> IncomeBillEntry { get; set; }
    }

    public class BillEntryModel
    {
        public int SrNo { get; set; }
        public string BIllno { get; set; }
        public string BillDate { get; set; }
        public string Bgndt { get; set; }
        public decimal billamt { get; set; }
        public decimal PendAmt { get; set; }
        public decimal AdjstAmt { get; set; }
        public decimal BillAdjAmt { get; set; }
        public string Party { get; set; }
        public string billstatus { get; set; }
        public string bill_cancel { get; set; }
        public string Acccode { get; set; }
        public string billcolbrcd { get; set; }
        public bool IsChecked { get; set; }
    }

    #endregion

    #region SeetingCustomerVendor
    public class CustomerVendrosetting
    {
        public string CustomerType { get; set; }
        public string Customereffect { get; set; }
        public string Customerlocation { get; set; }
        public string Customer { get; set; }

        public string Vendoreffect { get; set; }
        public string Vendorlocation { get; set; }
        public string Vendor { get; set; }

        public string Driveeffect { get; set; }
        public string Drivelocation { get; set; }
        public string Drive { get; set; }

        public string Employeeeffect { get; set; }
        public string Employeelocation { get; set; }
        public string Employee { get; set; }

        public List<EmpVenList> ListAccount { get; set; }
        public List<EmpVenList> ListEmp { get; set; }
        public List<EmpVenList> ListDriver { get; set; }
        public List<EmpVenList> ListVendor { get; set; }
        public List<EmpVenList> ListEmployee { get; set; }
        public List<PartyOpeningBalance> ListPartyOpeing { get; set; }
    }
    public class EmpVenList
    {
        public string acccode { get; set; }
        public string accdesc { get; set; }
    }
    public class PartyOpeningBalance
    {
        public int Id { get; set; }
        public bool chk { get; set; }
        public string Vendval { get; set; }
        public string vendorcode { get; set; }
        public string acctval { get; set; }
        public string Opendebit { get; set; }
        public string OpenCredit { get; set; }

    }

    #endregion

    #region Credit Note Generation

    public class CNoteFilterViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BillNo { get; set; }
        public string CUSTCD { get; set; }
        public string CUSTNM { get; set; }
        public string Paybasis { get; set; }
        public string TransMode { get; set; }
        public bool STax_YN { get; set; }
        public string STax { get; set; }
    }

    public class CNoteSubmissionViewModel
    {

        public List<webx_BILLMST> BillList { get; set; }
        public webx_BILLMST WBM { get; set; }
        public CNoteFilterViewModel CNFVM { get; set; }

        public string CnoteNo { get; set; }
        public DateTime CNoteDt { get; set; }
        public string Ref_No { get; set; }
        public bool STax_YN { get; set; }
        public string STax { get; set; }
        public decimal TotAmt { get; set; }
    }

    #endregion

    #region Debit Note Generation

    public class DNote
    {
        public string DNoteNo { get; set; }
        public DateTime DNoteDt { get; set; }
        public string Ref_No { get; set; }
        public string pbov_code { get; set; }
        public string pbov_name { get; set; }
        public string pbov_typ { get; set; }
        public string DebitAccount { get; set; }
        public string CUSTCD { get; set; }
        public string VENCD { get; set; }
        public string preparedfor { get; set; }
        public string preparedby { get; set; }
        public decimal DebitAmount { get; set; }
    }

    public class DCNoteSubmissionViewModel
    {

        public List<webx_BILLMST> BillList { get; set; }
        public webx_BILLMST WBM { get; set; }
        public string DNoteNo { get; set; }
        public DateTime DNoteDt { get; set; }
        public string Ref_No { get; set; }
        public string pbov_code { get; set; }
        public string pbov_name { get; set; }
        public string pbov_typ { get; set; }
        public string DebitAccount { get; set; }
        public string DebitAccountDisplay { get; set; }
        public string CUSTCD { get; set; }
        public string VENCD { get; set; }
        public string preparedfor { get; set; }
        public string preparedby { get; set; }
        public decimal DebitAmount { get; set; }
     //   public string DebitAccountDisplay { get; set; }
    }

    #endregion

    #region ServiceWise Billing

    public partial class ServiceWiseBilling
    {
        public string Service { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CustCD { get; set; }
        public string BillTo { get; set; }
        public string custtype { get; set; }
    }

    #endregion

    #region Budgest Final

    public class BudgetFinalYear
    {
        public List<vw_Get_Finacial_Years> ListYears { get; set; }
        public List<webx_location> Loc { get; set; }
        public BudgetSetting budgetsetting { get; set; }
        public int submitType { get; set; }
    }

    public partial class BudgetSetting
    {
        public string LocCode { get; set; }
        public string FinYear { get; set; }
        public string CompanyCode { get; set; }
    }

    public partial class BudgetData1
    {
        //string BudgetType,string Acccode, string main_category, string brcd, string YearVal,string Entrytype,string YearType,string RO,string ORGNCD
        public string BudgetType { get; set; }
        public string Acccode { get; set; }
        public string main_category { get; set; }
        public string brcd { get; set; }
        public string YearVal { get; set; }
        public string Entrytype { get; set; }
        public string YearType { get; set; }
        public string RO { get; set; }
        public string ORGNCD { get; set; }
    }

    public partial class BudgetData
    {
        //string BudgetType,string Acccode, string main_category, string brcd, string YearVal,string Entrytype,string YearType,string RO,string ORGNCD
        public string brcd { get; set; }
        public string Budget_Month_Year { get; set; }
        public string finyear { get; set; }
        public string Budget_Type { get; set; }
        public string Acccategory { get; set; }
        public string Acccode { get; set; }
        public string Accdesc { get; set; }
        public string report_loc { get; set; }
        public string loccode { get; set; }
        public string EntryType { get; set; }
    }

    public partial class webx_groups
    {
        public string Groupcode { get; set; }
        public string Groupdesc { get; set; }
        public string main_category { get; set; }
    }

    public class SetOpeningBalanceViewModel1
    {

        public List<BudgetData> GetBudget { get; set; }
        public BudgetData GetBudgetobj { get; set; }

        public webx_groups groupobj { get; set; }

        public List<webx_groups> groupDataobj { get; set; }


        public webx_acctinfo Balance { get; set; }

        public List<webx_acctinfo> Balancelist { get; set; }

        public webx_AcctBudget Account1 { get; set; }

        public List<webx_AcctBudget> Accountlist1 { get; set; }

        public string DocType { get; set; }
        public string RO { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        // public string Loccode { get; set; }
        public string FmNo { get; set; }
        public string BaseLoccode { get; set; }
        public Nullable<decimal> LocLevel { get; set; }

        public List<webx_location> RegionList { get; set; }

        public webx_location Location { get; set; }
        public List<webx_location> LocationList { get; set; }


        public string brcd { get; set; }
        public string Budget_Month_Year { get; set; }
        public string finyear { get; set; }
        public string Budget_Type { get; set; }

        public string YearType { get; set; }
        public string Acccategory { get; set; }
        public string Acccode { get; set; }
        public string Accdesc { get; set; }
        public string report_loc { get; set; }
        public string loccode { get; set; }
        public string EntryType { get; set; }
        public string BudgetAmount { get; set; }
        public List<SetOpeningBalanceViewModel1> Listofresult { get; set; }


    }
    #endregion
}