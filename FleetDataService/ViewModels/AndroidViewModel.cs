﻿using Fleet.Classes;
using FleetDataService.Models;
using CYGNUSDataService.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace Fleet.ViewModels
{
    public class AndroidViewModel
    {
    }

    public class Detail_Track_ViewModel
    {
        public string DOCKNO { get; set; }
        public string sourcehb { get; set; }
        public string ASDTDate { get; set; }
        public string ASDTTime { get; set; }
        public string tobh_code { get; set; }
        public int TRACK_CODE { get; set; }
        public string ACTIVITY { get; set; }
        public string PODLink { get; set; }
    }


    public class TrackingViewModel
    {
        public string DOCKNO { get; set; }
        public string PICKUP_DATE { get; set; }
        public string ORDER_NO { get; set; }
        public string CURRENT_STATUS { get; set; }
        public string CURRENT_STATUS_CODE { get; set; }
        public string CURRENT_LOCATION { get; set; }
        public string DELIVERED_ON { get; set; }
        public string DELIVERED_TIME { get; set; }
        public string DELIVERED_TO { get; set; }
        public string REMARKS { get; set; }
        public string CSGE_NAME { get; set; }
        public string CONTACT_NO { get; set; }
        public string CSGE_ADDR { get; set; }
        public string CITY { get; set; }
        public string PINCODE { get; set; }
        public string PAYMENT { get; set; }
        public string VALUE_OF_SHIPMENT { get; set; }
        public string SAID_TO_CONTAIN { get; set; }
        public string ORIGIN { get; set; }
        public string DESTINATION { get; set; }
        public string NO_OF_PIECES { get; set; }
        public string WEIGHT { get; set; }
        public string RTO_ON { get; set; }
        public string RTO_REASON { get; set; }
        public string NDR_REASON { get; set; }
        public string NDR_DATE { get; set; }
        public string RELATION { get; set; }
        public string CLIENTCODE { get; set; }
        public string CLIENTNAME { get; set; }
    }

    public class UnDiliverdGC
    {
        public string CSGNCD { get; set; }
        public string PARTY_CODE { get; set; }
        public string DOCKNO { get; set; }
        public System.DateTime DOCKDT { get; set; }
        public string ORGNCD { get; set; }
        public string DESTCD { get; set; }
        public string Delivered { get; set; }
        public string APIURL { get; set; }
        public string DKT_STATUS { get; set; }
        public string Message { get; set; }
        public int Statuss { get; set; }
        public string INVNO { get; set; }
        public string vehno { get; set; }
        public string GPSNotFoundMessage { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public bool IsShowTrackingButton { get; set; }
    }

    public class PRQ
    {
        public string VehicleType_Code { get; set; }
        public string Consignor_Pincode { get; set; }
        public string Consignor_State { get; set; }
        public string Consignor_LandMark { get; set; }
        public string Consignor_Address { get; set; }
        public string Consignor_City { get; set; }
        public string Consignee_State { get; set; }
        public string Consignee_LandMark { get; set; }
        public string Consignee_City { get; set; }
        public string Contact_Person_No { get; set; }
        public string Contact_Person_Name { get; set; }
        public string Approximate { get; set; }
        public string No_Of_Packet { get; set; }
        public string Traveling_Mode { get; set; }
        public string IMEI_NO { get; set; }
        public string EntryBy { get; set; }
        public string Origin_Lat { get; set; }
        public string Origin_Lon { get; set; }
        public string Destination_Lat { get; set; }
        public string Destination_Lon { get; set; }
    }
}