﻿using CYGNUS.Models;
using CYGNUS.ViewModel;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{
    #region Fix Assets Master

    public class FixAssetsMasterViewModel
    {
        public decimal srno { get; set; }
        public string ASSETCD { get; set; }
        public string DEPRATE { get; set; }
        public string UNITS { get; set; }
        public string CATEGORY { get; set; }
        public string prefix { get; set; }
        public string assetname { get; set; }
        public string DEPMETHOD { get; set; }
        public string AccountCode { get; set; }
        public List<WebxAssetmaster> ListWebxAssetmaster { get; set; }
    }
    public class WebxAssetmaster
    {
        public int Id { get; set; }
        public decimal srno { get; set; }
        public string assetcd { get; set; }
        public string DEPRATE { get; set; }
        public string units { get; set; }
        public string category { get; set; }
        public string prefix { get; set; }
        public string assetname { get; set; }
        public string Groupcd { get; set; }
        public string DEPMETHOD { get; set; }
        public string GRPASSTCD { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
    #endregion

    #region Purchase Order
    public class POOrderViewModel
    {
        public string pocode { get; set; }
        public DateTime podate { get; set; }
        public string assetcd { get; set; }
        public decimal qty { get; set; }
        public decimal rate { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal TotalQty { get; set; }
        public decimal balanceqty { get; set; }
        public string location { get; set; }
        public string activeflag { get; set; }
        public string narration { get; set; }
        public string voucherNo { get; set; }
        public DateTime PAYDT { get; set; }
        public double tax_per { get; set; }
        public string tax_type { get; set; }
        public string VENDORCD { get; set; }
        public string qutno { get; set; }
        public DateTime qutdate { get; set; }
        public string attn { get; set; }//txtkind


        public string POSTATUS { get; set; }
        public decimal Pendamt { get; set; }
        public string po_loccode { get; set; }
        public string chqdate { get; set; }
        public string PayMode { get; set; }
        public string acccode { get; set; }
        public string accdesc { get; set; }
        public string Chqno { get; set; }
        public decimal paidamt { get; set; }
        public string terms_condition { get; set; }
        public decimal chqamt { get; set; }
        public decimal ashamt { get; set; }
        public decimal CashAmt { get; set; }
        public decimal PO_advamt { get; set; }
        public decimal PO_balamt { get; set; }
        public string Bank_Accode { get; set; }
        public string Cash_Accode { get; set; }
        public string reqno { get; set; }
        public DateTime reqdt { get; set; }
        public decimal GSTPercentage { get; set; }
        public List<WebxPOASSET_det> ListPO { get; set; }
        public PaymentControl PC { get; set; }
        public string Image { get; set; }
        public string Documentpath { get; set; }
    }

    public class WebxPOASSET_det
    {
        public int Id { get; set; }
        public decimal srno { get; set; }
        public string pocode { get; set; }
        public DateTime podate { get; set; }
        public string assetcd { get; set; }
        public decimal qty { get; set; }
        public decimal rate { get; set; }
        public double OtherCharge { get; set; }
        public decimal total { get; set; }
        public decimal balanceqty { get; set; }
        public string location { get; set; }
        public string activeflag { get; set; }
        public string narration { get; set; }
        public string voucherNo { get; set; }
        public DateTime PAYDT { get; set; }
        public double tax_per { get; set; }
        public string tax_type { get; set; }
        public bool Chk { get; set; }

        public string HSN_SAC { get; set; }
        public decimal taxableAmt { get; set; }
        public decimal TaxAmount { get; set; }
    }
    #endregion

    #region  FixAssets Exception & View & Print & Reports

    public class FixAssetPrintViewModel
    {
        public vm_FixAssetModel FixAssetModel { get; set; }
        public List<vm_FixAssetList> ListFixAsset { get; set; }
        public List<vm_PoCancellationList> ListPoCancellation { get; set; }

    }

    public class vm_FixAssetModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string BillNo { get; set; }
        public string FilterType { get; set; }
        public string selectDate { get; set; }
        public string CancelReason { get; set; }
    }

    public class vm_FixAssetList
    {
        public string pocode { get; set; }
        public string podate { get; set; }
        public string VENDORCD { get; set; }
        public decimal TotalQty { get; set; }
        public decimal BalanceQty { get; set; }
        public string POSTATUS { get; set; }
        public decimal totalamt { get; set; }
        public decimal pendamt { get; set; }
        public string Documentpath { get; set; }
    }

    public class vm_PoCancellationList
    {
        public string pocode { get; set; }
        public string podate { get; set; }
        public string podt { get; set; }
        public string Vendorcode { get; set; }
        public string vendorcd { get; set; }
        public decimal Paidamt { get; set; }
        public decimal pendamt { get; set; }
        public decimal Totalamt { get; set; }
        public string Active { get; set; }
    }



    #endregion
}