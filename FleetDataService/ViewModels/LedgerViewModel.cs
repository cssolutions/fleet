﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.ViewModel
{
    public partial class CYGNUS_Collection_Payment
    {
        public Nullable<System.DateTime> TransactionDate { get; set; }

        public string CashAccountCode { get; set; }
        public string BankAccountCode { get; set; }
        public string DisealAccountCode { get; set; }
        public int TranasctionNo { get; set; }
        public string TransactionType { get; set; }
        public string AccountingLocation { get; set; }
        public string ChequeNo { get; set; }
        public System.DateTime ChequeDate { get; set; }
        public decimal Amount { get; set; }
        public decimal CashAmount { get; set; }
        public decimal DisealAmount { get; set; }
        public decimal ChequeAmount { get; set; }
        public decimal ChequeAdjustedAmount { get; set; }
        public decimal OnAccountBalance { get; set; }
        public string ChequeBankName { get; set; }
        public string ChequeBankBranchName { get; set; }
        public string ReceivedBankName { get; set; }
        public string PrintName { get; set; }
        public string ChequeLocation { get; set; }
        public string ChequeDepositLocation { get; set; }
        public bool IsChequeDeposited { get; set; }
        public bool IsChequeDirectDeposited { get; set; }
        public bool IsChequeOnAccount { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }


        /* Add Field For Non Claim Bounus Voucher */
        public decimal Amount_Applicable { get; set; }
        public decimal NetPayabale { get; set; }

        public string TDSType { get; set; }
        public bool IsTDSApplicable { get; set; }
        public string PANNO { get; set; }
        public string TANNo { get; set; }
        public decimal TDSPercentage { get; set; }
        public decimal totTDSAmt { get; set; }

        public decimal NET_RECEIVED { get; set; }
        public string Bank_Branch { get; set; }
    }
}