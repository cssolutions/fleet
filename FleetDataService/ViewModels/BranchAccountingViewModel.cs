﻿using CYGNUS.Models;
using Fleet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetDataService.ViewModels;

namespace Fleet.ViewModels
{
    public class BranchAccountingViewModel
    {
    }

    public class ElectricMaster
    {
        public CYGNUS_Elect_Office_Godown_Details CEOGD { get; set; }
        public List<CYGNUS_Elect_Office_Godown_Details> ListCEOGD { get; set; }
        public string BRCD { get; set; }
    }

    public class BranchAccountingMaster
    {
        public CYGNUS_Branch_Accounting_Master CBAM { get; set; }
        public List<CYGNUS_Branch_Accounting_Master> ListCBAM { get; set; }
        public CYGNUS_Branch_Expance BRS { get; set; }
        public List<CYGNUS_Branch_Expance> List_BRS { get; set; }
        public int ExpenceType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BranchCode { get; set; }
        public string DocumentNo { get; set; }
        public int Type { get; set; }
        public int SubType { get; set; }
        public string BRCD { get; set; }
        public int Id { get; set; }
        public int ApprovePaidType { get; set; }
        public string Staff { get; set; }
        public string Vendor { get; set; }
        public string ConveyanceExpense { get; set; }
        public string SubmitType { get; set; }
        public int Month { get; set; }
        public string Bill_Status { get; set; }
    }

    public class ExpensesMasters
    {
        public List<CYGNUS_Porter_Expense> CPEListing { get; set; }
        public CYGNUS_Porter_Expense CPE { get; set; }
    }

    public class ExpensesEntryModule
    {
        public CYGNUS_Branch_Electricity_Expenses_HDR Elec_Expense_HDR { get; set; }
        public List<CYGNUS_Branch_Electricity_Expenses_DET> List_Elec_Expense_DET { get; set; }

        public CYGNUS_Branch_Office_Expenses_Other_HDR Office_Other_Expense_HDR { get; set; }
        public List<CYGNUS_Branch_Office_Expenses_Other_DET> List_Office_Other_Expense_DET { get; set; }


        public CYGNUS_Branch_Office_Expenses_Fixed_HDR Office_Fixed_Expense_HDR { get; set; }
        public List<CYGNUS_Branch_Office_Expenses_Fixed_DET> List_Office_Fixed_Expense_DET { get; set; }

        public CYGNUS_Branch_Travelling_Tour_HDR Trave_Tour_Expense_HDR { get; set; }
        public CYGNUS_Branch_Travelling_Tour_DET Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Fare_Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Conveyance_Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Bo_Lo_Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Food_Trave_Tour_Expense_DET { get; set; }
        public List<CYGNUS_Branch_Travelling_Tour_DET> List_Other_Trave_Tour_Expense_DET { get; set; }

        public CYGNUS_Detention_Expense_HDR Detension_Expense_HDR { get; set; }
        public List<CYGNUS_Detention_Expense_DET> List_Detension_Expense_DET { get; set; }

        public CYGNUS_MarketVehicleCharges_Expence_HDR MarketVehicleCharges_Expence_HDR { get; set; }
        public List<CYGNUS_MarketVehicleCharges_Expence_DET> List_MarketVehicle_Expence_DET { get; set; }
        public List<CYGNUS_Elect_Office_Godown_Details> List_Elect_AddressDetail { get; set; }

        public CYGNUS_Branch_Expenses_EntryHDR CPEE_HDR { get; set; }
        public List<CYGNUS_Branch_Expenses_EntryHDR> ListCPEE_HDR { get; set; }
        public List<CYGNUS_Branch_Expenses_EntryDET> ListCPEE_DET { get; set; }
        public List<Webx_Account_Details> WADListing { get; set; }
        public Webx_Account_Details WAD { get; set; }
        public decimal HdnServiceTaxRate { get; set; }
        public decimal HdnEduCessRate { get; set; }
        public decimal HdnHEduCessRate { get; set; }
        //public int Type { get; set; }
        public int ID { get; set; }
        public int Type2 { get; set; }
        public int SubType { get; set; }
        public int Approve_Account_Type { get; set; }
        public DateTime ApprovedDate { get; set; }
        public DateTime BR_Date { get; set; }
        public int ExpanceType { get; set; }
        public decimal DriverID { get; set; }
        public string NameofDriver { get; set; }

        public CYGNUS_DriverIncentiveTSP_Expense_HDR Driver_TSP_Expense_HDR { get; set; }
        public List<CYGNUS_DriverIncentiveTSP_Expense_DET> List_Driver_TSP_Expense_DET { get; set; }

        public CYGNUS_Conveyance_Expense_HDR Conveyance_Expense_HDR { get; set; }
        public List<CYGNUS_Conveyance_Expense_DET> List_Conveyance_DET { get; set; }

        public CYGNUS_Food_Expense_HDR Food_Expense_HDR { get; set; }
        public List<CYGNUS_Food_Expense_DET> List_Food_DET { get; set; }

        public CYGNUS_HandlingLoading_Unloading_Expence_HDR Handling_Expence_HDR { get; set; }
        public List<CYGNUS_HandlingLoading_Unloading_Expence_DET> List_Handling_Expence_DET { get; set; }

        public CYGNUS_Tripsheet_Disputed_Amount_HDR Tripsheet_HDR { get; set; }
        public List<CYGNUS_Tripsheet_Disputed_Amount_DET> List_Tripsheet_DET { get; set; }

        public List<CYGNUS_BR_Advance_Entry> List_BR_Adv { get; set; }


        public CYGNUS_DriverIncentiveTSP_Expense_Master CDITEM { get; set; }
        public List<CYGNUS_DriverIncentiveTSP_Expense_Master> List_CDITEM { get; set; }
    }

    public class BillStatusDetailsViewModel
    {
        public string BillNo { get; set; }
        public string BillStatus { get; set; }
    }

    public class BR_StatusSummary
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BRCD { get; set; }
        public decimal Netamt { get; set; }
        public int ExpenseType { get; set; }
        public int type { get; set; }
        public List<CYGNUS_Branch_Expance> List_BRS { get; set; }
        public CYGNUS_Branch_Expance BRS { get; set; }

    }

    public partial class BR_ViewPrint_Filter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BillNo { get; set; }
        public string Type { get; set; }
        public List<CYGNUS_Branch_Accounting_Master> ListCBAM { get; set; }
        public List<CYGNUS_Branch_Expance> ListBR_Expance { get; set; }
    }

    public class Expenses_Advance_EntryModule
    {
        public CYGNUS_BR_Advance_Entry BR_ADV { get; set; }
        public CYGNUS_Branch_Travelling_Tour_HDR BR_Travel_Hdr { get; set; }
        public PaymentControl PC { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string DocumentNo { get; set; }
        public string Type { get; set; }
        public string BRCD { get; set; }

        public List<CYGNUS_Branch_Travelling_Tour_HDR> ListBR_Travel_Hdr { get; set; }

        public string BillNo { get; set; }
        public string Staff { get; set; }
    }

    public class TeaExpBranchUserWise
    {
        public List<CYGNUS_Tea_Expense_Master> ListCPEE_HDR { get; set; }
        public CYGNUS_Tea_Expense_Master CPEE_HDR { get; set; }
        public string Mapped { get; set; }
        public string BRCD { get; set; }
    }
}