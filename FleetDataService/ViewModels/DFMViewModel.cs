﻿using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{
    #region Scan Documents
    public class Webx_FM_Scan_DocumentsViewModel
    {
        public Webx_FM_Scan_Documents WFSDM { get; set; }
        public List<Webx_FM_Scan_Documents> ListWFSDM { get; set; }
    }
    #endregion

    #region Forward Scan Documents
    public partial class FM_FWD_DOCFilter
    {
        public string DocType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Paybas { get; set; }
        public string Dockets { get; set; }
        public string DT_TYPE { get; set; }
        public string loccode { get; set; }
    }

    public partial class FM_FWD_DOCBill
    {
        public string DocType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Paybas { get; set; }
        public string Dockets { get; set; }
        public string DT_TYPE { get; set; }
    }

    public class WebX_FM_FWD_DOC_MasterViewModel
    {
        public WebX_FM_FWD_DOC_Master WFFDM { get; set; }
        public List<WebX_FM_FWD_DOC_Master> ListWFFDM { get; set; }
        public FM_FWD_DOCFilter FFDFM { get; set; }
        public List<FM_FWD_DOCFilter> ListFFDFM { get; set; }
        public vw_Bill_For_FWD VWBFF { get; set; }
        public List<vw_Bill_For_FWD> ListVWBFF { get; set; }
        public vw_dockets_For_FWD VWDFFM { get; set; }
        public List<vw_dockets_For_FWD> ListVWDFFM { get; set; }
        public List<WebX_FM_FWD_DOC_Detail> ListWFFDDM { get; set; }
    }
    #endregion

    #region Acknowledge Documents
    public partial class FM_FWD_DOC_AckFilter
    {
        public string FMType { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Loccode { get; set; }
        public string FmNo { get; set; }
        public string BaseLoccode { get; set; }
    }

    public class vw_FM_DOC_For_AckViewModel
    {
        public vw_FM_DOC_For_Ack VWFDFAM { get; set; }
        public List<vw_FM_DOC_For_Ack> ListVWFDFAM { get; set; }
        public FM_FWD_DOC_AckFilter FFDAF { get; set; }
        public List<FM_FWD_DOC_AckFilter> ListFFDAF { get; set; }
    }
    #endregion

    public partial class FMFilter
    {
        public string DocType { get; set; }
        public string RO { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Loccode { get; set; }
        public string FmNo { get; set; }
        public string BaseLoccode { get; set; }
        public Nullable<decimal> LocLevel { get; set; }

        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }
    }

    #region View/Print
    public class ViewPrintViewModel
    {
        public VW_PFM_ViewPrint VWPFMVP { get; set; }
        public List<VW_PFM_ViewPrint> ListVWPFMVP { get; set; }
        public FMFilter FMF { get; set; }
        public List<FMFilter> ListFMF { get; set; }
    }
    #endregion

    #region Document Track
    public partial class DocTrackFilter
    {
        public string DocType { get; set; }
        public string DocNo { get; set; }
    }
    public class DocumentTrackViewModel
    {
        public VW_FM_TRACK_Report VWFTR { get; set; }
        public List<VW_FM_TRACK_Report> ListVWFTR { get; set; }
        public DocTrackFilter DTF { get; set; }
        public List<VW_FM_View> ListFMView { get; set; }
        public List<WebX_FM_FWD_DOC_Master> ListFMDetail { get; set; }
    }
    #endregion

    #region View / Print POD PFM

    public class PODPFMViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string fromloc { get; set; }
    }

    #endregion

    #region Vendor Debit Note

    public class DebitNoteModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string BillNo { get; set; }
        public string FilterType { get; set; }
        public string Cust_State { get; set; }
        public bool isGSTReverse { get; set; }
        public string GSTType { get; set; }
        public string Type { get; set; }
        public decimal GSTPercentage { get; set; }
        public List<DNM_DNCancellationList> DebitNoteList { get; set; }

    }


    public class DNM_DNCancellationList
    {
        public string DocumentNo { get; set; }
        public string DocumentDate { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public string Branch { get; set; }
        public decimal NetAmount { get; set; }
        public string Active { get; set; }
        public string InvoiceNo { get; set; }
        public bool IsCheckBox { get; set; }
        public bool ISAdjusted { get; set; }
        public string BillNo { get; set; }
        public decimal PendAmount { get; set; }

    }

    #endregion    
}