﻿using CYGNUS.Models;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Fleet.ViewModels
{
    public partial class vw_MFCancellation
    {
        public DateTime FromDate { get; set; } 
        public DateTime ToDate { get; set; }
        public string THCNO { get; set; }
        public string ManualTHCNo { get; set; }
        public string THCDT { get; set; }
        public string SourceHB { get; set; }
        public string ToBH_Code { get; set; }
        public string tcno { get; set; }
        public bool IsChecked { get; set; }
    }

    public partial class vw_PDCCancellation
    {
        public string pdcno { get; set; }
        public string pdcdt { get; set; }
        public string Manualpdcno { get; set; }
        public string PDCBR { get; set; }
        public string PDCTY { get; set; }
        public bool IsChecked { get; set; }
    }

    public partial class MFCancellationViewModel
    {
        public webx_THC_SUMMARY MFCnlModel { get; set; }
        public List<webx_THC_SUMMARY> THCList { get; set; }
        public vw_MFCancellation THCCnlModel { get; set; }
        public List<vw_MFCancellation> THCCnlList { get; set; }
        public vw_PDCCancellation PDCCnlModel { get; set; }
        public List<vw_PDCCancellation> PDCCnlList { get; set; }
    }

    #region Thc Financial Tracker
    public partial class ThcFinancialTrackerCriteria
    {
        public string DocType { get; set; }
        public string NumberType { get; set; }
        public string DocNo { get; set; }
    }
    #endregion

    #region PO Cancellation
    public partial class POCancellationCriteria
    {
        public string PONo { get; set; }
        public string GRNNo { get; set; }
        public DateTime POCANCELDATE { get; set; }
        public string ManualPONo { get; set; }
        public string Reason { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    #region Doket Booking Exception

    public partial class DoketBookingExceptionViewModel
    {
        public string CnoteNo { get; set; }
        // public List<WebX_Master_Docket> DktList { get; set; }
        public List<CalenderEvent> DktCal { get; set; }
        public WebX_Master_Docket Dkt { get; set; }
    }

    #endregion


    public partial class POCancellation
    {
        public string PONO { get; set; }
        public string ManualPoNo { get; set; }
        public string PODATE { get; set; }
        public DateTime podate1 { get; set; }
        public string VENDORNAME { get; set; }
        public string LOCATION { get; set; }
        public string POSTATUS { get; set; }
        public decimal TOTALQTR { get; set; }
        public decimal TOTALAMT { get; set; }
        public decimal PENDAMT { get; set; }

        public string Reason { get; set; }
        public DateTime POCANCELDATE { get; set; }
        public decimal TotalRecods { get; set; }
    }

    public class POCancellationViewModel
    {
        public POCancellation POC { get; set; }
        public List<POCancellation> ListPOC { get; set; }
    }

    #endregion

    #region Edit DFM
    public partial class DFMLIST
    {
        public string DockNo { get; set; }
        public string DockDT { get; set; }
        public string Orgn_Dest { get; set; }
        public string From_To { get; set; }
        public string Dely_Date { get; set; }
        public decimal Amount { get; set; }
    }

    public class DFMLISTViewModel
    {
        public List<DFMLIST> ListDLM { get; set; }
        public vw_Edit_ForwardDocuments_HeaderDetails VWFDHD { get; set; }
        public FMFilter FMF { get; set; }
    }
    #endregion

    public partial class BillEntryCancellation
    {
        public webx_BILLMST BillNo { get; set; }

        public List<webx_BILLMST> BillNOList { get; set; }
        public string HdnAcctReversal { get; set; }
        public string HdnFinalizedRule { get; set; }
    }

    public partial class VoucherCancellation
    {
        public webx_acctinfo WAIM { get; set; }
        public List<webx_acctinfo> ListWAIM { get; set; }
    }

    public partial class ReAssnBillSubmColln
    {
        public webx_BILLMST WBMM { get; set; }
        public List<webx_BILLMST> ListWBMM { get; set; }
    }

    public partial class CNoteDetentionViewModel
    {
        public vw_MFCancellation CnotDetModel { get; set; }
        public List<CNoteDetentionList> CnotCnlList { get; set; }

    }

    public partial class CNoteDetentionList
    {
        public string dockno { get; set; }
        public string docksf { get; set; }
        public string DocketNo { get; set; }
        public string bkg_dt { get; set; }
        public DateTime dockdt { get; set; }
        public string docdt { get; set; }
        public string Origin_Dest { get; set; }
        public string orgncd { get; set; }
        public string Arr_DT { get; set; }
        public DateTime Arrived_DT { get; set; }
        public bool IsChecked { get; set; }
        public string DetentionReason { get; set; }
        public decimal Detained_On_days { get; set; }
        public DateTime DetainedSysdate { get; set; }
        public string DetainDt { get; set; }
        public string detainedby { get; set; }
        public string RemovalReason { get; set; }
    }

    #region Docket cancellation

    public partial class Cnotecancellation
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CNote { get; set; }
        public string Reason { get; set; }
        public List<VWNET_WebX_Docket_status> DocketList { get; set; }
        public VWNET_WebX_Docket_status vw_DocketCancellation { get; set; }
    }

    #endregion

    public class VendorCodeChange
    {
        public webx_VENDOR_HDR WVHM { get; set; }
        public List<webx_VENDOR_HDR> WVHL { get; set; }
        public string BaseLoccode { get; set; }

    }

    public class BalanceLocationChange
    {
        public webx_VENDOR_HDR WVHM { get; set; }
        public List<webx_VENDOR_HDR> WVHL { get; set; }
        public string BaseLoccode { get; set; }

    }

    public partial class MaterialHeldUpReason
    {
        public VWNET_WebX_Docket_status VWDS { get; set; }
        public List<VWNET_WebX_Docket_status> VWDSL { get; set; }
    }

    #region Shortage and Damage Declaration

    public partial class ShortageViewModel
    {
        public ShortageCriteria WVHM { get; set; }
        public ShortageModel SM { get; set; }
        public List<ShortageModel> ListSM { get; set; }
        public List<DocketMisRouteModel> ListMR { get; set; }
        public string BaseLoccode { get; set; }
    }

    public partial class ShortageCriteria
    {
        public string RO { get; set; }
        public string location { get; set; }
        public DateTime DATEFROM { get; set; }
        public DateTime DATETO { get; set; }
        public string DOC_NO { get; set; }
        public string FilterType { get; set; }
    }

    public partial class ShortageModel
    {
        public string DocketNo { get; set; }
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
        public decimal PKGSNO { get; set; }
        public decimal ACTUWT { get; set; }
        public string TransMode { get; set; }
        public string DocketDate { get; set; }
        public string OrgCode { get; set; }
        public string Commited_DelyDate { get; set; }
        public Int32 PackagesLB { get; set; }
        public Int32 WeightLB { get; set; }
        public string ReDestCode { get; set; }
        public string fromto { get; set; }
        public bool IsChecked { get; set; }
        public string SReason { get; set; }
        public string DReason { get; set; }
        public Int32 SPackagesLB { get; set; }
        public Int32 SWeightLB { get; set; }
        public Int32 DPackagesLB { get; set; }
        public Int32 DWeightLB { get; set; }
    }

    public partial class DocketMisRouteModel
    {
        public string Dockno { get; set; }
        public string DockSF { get; set; }
        public decimal PkgSNo { get; set; }
        public decimal ACTUWT { get; set; }
        public Int32 ArrPkgQty { get; set; }
        public decimal ArrWeightQty { get; set; }
        public decimal ShortageQty { get; set; }
        public decimal ShortageWeight { get; set; }

        public decimal ArrivedPkgs { get; set; }
        public decimal ArrivedWt { get; set; }

        public string OriginDestination { get; set; }
        public string FromTOCity { get; set; }
        public string PayBas { get; set; }
        public string TRN_MOD { get; set; }
        public Int32 MisRouteCount { get; set; }
        public bool IsChecked { get; set; }
    }




    #endregion 

    #region CNote Reassign Location

    public partial class CNoteReassignFilter
    {
        public string ChangeLoc { get; set; }
        public string RO { get; set; }
        public string Loccode { get; set; }
        public string DT_TYPE { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string CnoteNo { get; set; }
        public string BaseLoccode { get; set; }
        public Nullable<decimal> LocLevel { get; set; }

        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }

        public string RO_To { get; set; }
        public string Loccode_To { get; set; }
        public string Status_TYPE { get; set; }
        public Nullable<decimal> LocLevel_To { get; set; }
        public List<webx_location> RegionList_To { get; set; }
        public List<webx_location> LocationList_To { get; set; }

    }

    public class CNoteReassignViewModel
    {
        public VWNET_WebX_Docket_status VWWDS { get; set; }
        public List<VWNET_WebX_Docket_status> ListVWWDS { get; set; }
    }

    #endregion

    #region Balance Location Change
    public class BalanceLocationChangeViewModel
    {
        public webx_VENDOR_HDR WVHM { get; set; }
        public List<webx_VENDOR_HDR> WVHList { get; set; }
        public string BaseLoccode { get; set; }

        public string VENDORCODE { get; set; }
        public string VENDORCODENAME { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
        public string THCNo { get; set; }
        public string DocType { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string RO { get; set; }
        public string Loccode { get; set; }
        public string DOC_NO { get; set; }
        public string THCDate { get; set; }
        public string VehicleNo { get; set; }
        public decimal THCAmount { get; set; }
        public string NBLocation { get; set; }
        public string BalanceLocation { get; set; }
        public string SelectionType { get; set; }
        public bool IsChecked { get; set; }

        public Nullable<decimal> LocLevel { get; set; }
        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }

    }
    #endregion

    #region Tripsheet

    public partial class TSAdvanceEditCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TripSheetNo { get; set; }
        public string Flag { get; set; }
    }

    public partial class TSAdvanceEditDates
    {
        public string TripSheetNo { get; set; }
        public string Vslipdt1 { get; set; }
        public string VCloseDt { get; set; }
        public string ServerDt { get; set; }
    }

    public class TSAdvanceEditViewModel
    {
        public TSAdvanceEditDates TSAEDM { get; set; }
        public List<TSAdvanceEditDates> TSAEDMList { get; set; }

        public WEBX_TRIPSHEET_ADVEXP WTSASelect { get; set; }
        public List<WEBX_TRIPSHEET_ADVEXP> WTSAList { get; set; }
        public PaymentControl PC { get; set; }

    }
    #endregion

    #region Financial Edit

    public partial class TSFinancialEditCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TripSheetNo { get; set; }
        public string Flag { get; set; }
        public string UpdClose { get; set; }
        public List<WEBX_FLEET_VEHICLE_ISSUE> WFVIList { get; set; }
    }


    public class TSFinancialEditViewModel
    {
        public string Operational_Close { get; set; }
        public vw_TripSheetClose TSClose { get; set; }
        public List<vw_TripSheetClose> TSCloseList { get; set; }
        public WEBX_TRIPSHEET_ADVEXP WTSASelect { get; set; }
        public List<WEBX_TRIPSHEET_ADVEXP> WTSAList { get; set; }
        public WEBX_FLEET_VEHICLE_ISSUE WFVI { get; set; }
        public WEBX_FLEET_ENROUTE_EXP WFEE { get; set; }

        public List<WEBX_TRIPSHEET_OILEXPDET> LIST_WTRSOP = new List<WEBX_TRIPSHEET_OILEXPDET>();

        public List<WEBX_FLEET_ENROUTE_EXP> EnrtExpList = new List<WEBX_FLEET_ENROUTE_EXP>();
        public PaymentControl PC { get; set; }
        public Webx_Fleet_Triprule Obj_WFT { get; set; }

    }

    #endregion

    #region Cancle Trip Advance Voucher
    public partial class AdvanceTripVoucherCancellation
    {
        public webx_acctrans WAAT { get; set; }
        public List<webx_acctrans> ListWAAT { get; set; }
    }

    public partial class AdnavceTripVoucherDoneList
    {
        public string VoucherNo { get; set; }
        public DateTime voucherDate { get; set; }
        public string Tripsheetno { get; set; }
        public string amt { get; set; }
        public DateTime Canceldt { get; set; }
        public string Status { get; set; }
    }
    #endregion

    #region ADVICE Edit/Cancellation

    public class AdviceCancelViewModel
    {
        public WEBX_Advice_Hdr Advice { get; set; }
        public List<WEBX_Advice_Hdr> ListAdvice { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string raisedon { get; set; }
        public string transtable { get; set; }
        public string Adviceno { get; set; }
        public string ORGNCD { get; set; }
        public string LOCTYP { get; set; }
        public string STATUS { get; set; }
        public string advicestatus { get; set; }
        public bool IsCheck { get; set; }
    }
    #endregion

    #region CNote Non Delivery Reason Update

    public partial class CNoteNonDeliveryCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int NoofDays { get; set; }
        public string CNoteNo { get; set; }
    }

    public partial class SubReasons
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public partial class CNoteNonDeliveryreasonUpdateViewModel
    {
        public CNoteNonDeliveryCriteria Criteria { get; set; }
        public List<WebX_Master_Docket> DocketList { get; set; }
    }


    #endregion

    #region Service Tax Removal

    public class ServiceTaxRemove
    {
        public string CnoteNo { get; set; }
    }

    #endregion

    #region OctroiBill_VendorChange

    public partial class OctroiBill_VendorChange
    {
        public WEBX_VENDORBILL_HDR ObjWVBill { get; set; }
        public List<WEBX_VENDORBILL_HDR> ListWVBill { get; set; }
    }

    #endregion
        
    #region  Customer Generate Mail

    public partial class CustomergeneratemailviewModel
    {
        public string CnoteNo { get; set; }
        public string COD { get; set; }
        public string DACC { get; set; }
        // public List<WebX_Master_Docket> DktList { get; set; }
        public List<CalenderEvent> DktCal { get; set; }
        public WebX_Master_Docket Dkt { get; set; }
        public string Reason { get; set; }
        public string DOCKET_DELY_REASON { get; set; }
    }

    #endregion

    public partial class ReAssnVoucherDate
    {
        public webx_acctrans WVMM { get; set; }
        public List<webx_acctrans> ListWVMM { get; set; }
    }
}