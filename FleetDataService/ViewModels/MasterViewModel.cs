﻿using CYGNUS.Models;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.ViewModels
{
    #region Menu Master

    public class MenuObj1
    {
        public string DisplayName { get; set; }
        public int DisplayRank { get; set; }
        public bool HasAccess { get; set; }
        public int MenuId { get; set; }
        public string NavigationURL { get; set; }
        public int ParentID { get; set; }
        public string UserId { get; set; }
    }

    #endregion

    #region General Master

    public class Webx_Master_GeneralViewModel
    {
        public WebX_Master_CodeTypes WMCT { get; set; }
        public List<Webx_Master_General> listWMG { get; set; }
        public List<WebX_Master_CodeTypes> VPart { get; set; }
        public Webx_Master_General WMG { get; set; }

        //lavnit
        public List<webx_location> WLocationList { get; set; }
        public List<webx_location> WLocationROList { get; set; }
        public List<webx_location> WLocationAOList { get; set; }
        public List<webx_location> WLocationBankList { get; set; }

        public string RO { get; set; }
        public string AO { get; set; }
        public string LO { get; set; }
        public string Bank { get; set; }
        public string rdio { get; set; }
        public Nullable<decimal> LocLevel { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ChequeNo { get; set; }
        public bool rdioshow { get; set; }
        
    }

    #endregion

    #region State Master

    public class Webx_StateViewModel
    {
        public List<Webx_State> listWS { get; set; }
        public Webx_State WST { get; set; }
        public List<Webx_State_Document> listWSD { get; set; }
        public Webx_State_Document WSD { get; set; }
    }

    #endregion

    #region City Master

    public class webx_citymasterViewModel
    {
        public List<webx_citymaster> listWCM { get; set; }
        public webx_citymaster WCM { get; set; }
    }

    #endregion

    #region Location Master

    public class LocationViewModel
    {
        public List<webx_location> listWS { get; set; }
        public webx_location WL { get; set; }
        public List<Webx_State> ListLocState { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region pincode Master

    public class webx_pincodemasterViewModel
    {
        public List<webx_pincode_master> listWPM { get; set; }
        public webx_pincode_master WPM { get; set; }
    }

    #endregion

    #region User Master

    public class WebX_Master_UsersViewModel
    {
        public WebX_Master_Users WMU { get; set; }
        public string UserId { get; set; }
        public bool EditFlag { get; set; }
    }

    #endregion

    #region Group Master

    public class webx_GRPMSTViewModel
    {
        public List<webx_GRPMST> listWGM { get; set; }
        public webx_GRPMST WGM { get; set; }
    }

    #endregion

    #region Vendor Master

    public class VendorViewModel
    {
        public List<webx_VENDOR_HDR> listVendor { get; set; }
        public webx_VENDOR_HDR VendorHDR { get; set; }
        public webx_VENDOR_DET VendorDET { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }
        public List<webx_location> locationList { get; set; }
        public string EditFlag { get; set; }
        public List<CygnusVendorGSTDetails> stateGSTList { get; set; }
        public Cygnus_Vendor_Document VendorDoc { get; set; }
        public List<Cygnus_Vendor_Document> ListVendorDoc { get; set; }
    }
    #endregion

    #region Optimum Route & Location Connection Master

    public class OptimumRouteViewModel
    {
        public Webx_master_OptimumRouteMaster OR { get; set; }
        public List<webx_location> locationList { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }
        public string EditFlag { get; set; }
    }

    public class LocationConnectionViewModel
    {
        public Webx_master_LocationConnection LC { get; set; }
        public List<webx_location> locationList { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Customer Master

    public class webx_CUSTHDRViewModel
    {
        public List<webx_CUSTHDR> listWCH { get; set; }
        public webx_CUSTHDR WCH { get; set; }
    }

    #endregion

    public class objDate
    {
        public string ControlId { get; set; }
        public string ControlValue { get; set; }
        public string ModuleCode { get; set; }
        public bool isControlRemove { get; set; }
    }

    #region Vehicle Type Master

    public class webx_Vehicle_TypeViewModel
    {
        public webx_Vehicle_Type WVT { get; set; }
        public bool EditFlag { get; set; }
    }

    #endregion

    #region Delivery Zone Master

    public class webx_Delivery_ZoneViewModel
    {
        public List<webx_Delivery_Zone> listWDZM { get; set; }
        public webx_Delivery_Zone WDZM { get; set; }
    }

    #endregion

    #region Flight Schedule Master

    public class webx_Master_Flight_Schedule_DetViewModel
    {
        public webx_Master_Flight_Schedule_Det WMFSDT { get; set; }
        public List<webx_Master_Flight_Schedule_Det> listWMFSDT { get; set; }
        public webx_Master_Flight_Schedule_Hdr WMFSHT { get; set; }
        public List<webx_Master_Flight_Schedule_Hdr> listWMFSHT { get; set; }
    }

    #endregion

    #region Airport Master

    public class webx_Master_AirportViewModel
    {
        public List<webx_Master_Airport> listWMAM { get; set; }
        public webx_Master_Airport WMAM { get; set; }
    }

    #endregion

    #region City Location Mapping Master

    public class Webx_master_CityLocationMappingViewModel
    {
        public List<Webx_master_CityLocationMapping> listWMCLMM { get; set; }
        public Webx_master_CityLocationMapping WMCLMM { get; set; }
    }

    #endregion

    #region Route Master

    public class webx_rutMST_TRAN_CityViewModel
    {
        public List<webx_rutmas_City> listWRMCT { get; set; }
        public webx_rutmas_City WRMCT { get; set; }
        public List<webx_ruttran_City> listWRTCT { get; set; }
        public webx_ruttran_City WRTCT { get; set; }
    }

    public class webx_rutMST_TRANViewModel
    {
        public List<webx_rutmas> listWRMT { get; set; }
        public webx_rutmas WRMT { get; set; }
        public List<webx_ruttran> listWRTT { get; set; }
        public webx_ruttran WRTT { get; set; }
    }

    #endregion

    #region Vehicle Master

    public class VehicleViewModel
    {
        public List<webx_VEHICLE_HDR> listVehicle { get; set; }
        public List<webx_Vehicle_Type> listVehicleType { get; set; }
        public webx_VEHICLE_HDR VehicleHDR { get; set; }
        //public List<Webx_Master_General> ListGnMST { get; set; }
        public List<webx_location> locationList { get; set; }
        public List<Webx_State> StatePermitList { get; set; }
        public string EditFlag { get; set; }

        public List<WEBX_FLEET_DOCUMENT_MST> listFLEETDOCUMENT { get; set; }
        public List<vw_VT_tripsheet> listVTrip { get; set; }
        public List<VW_VEH_FUEL_FILL_HISTORY> listFFH { get; set; }
        public List<VW_ODOMETER_HISTORY> listodometerHtr { get; set; }
        public List<WEBX_FLEET_TYREMST> listTyreDetails { get; set; }

        public List<CYGNUS_Vehicle_GPS_Current_Details> CVGPCDL { get; set; }
        public CYGNUS_Vehicle_GPS_Current_Details CVGPCD { get; set; }
    }

    #endregion

    #region Customer Addresses & Address Mapping Master

    public class webx_master_CustAddressesViewModel
    {
        public List<webx_master_CustAddresses> listCustAddress { get; set; }
        public webx_master_CustAddresses CustAddress { get; set; }
        public string EditFlag { get; set; }
    }

    public class webx_master_CustAddr_MappingViewModel
    {
        public List<webx_master_CustAddr_Mapping> listWMCAMM { get; set; }
        public webx_master_CustAddr_Mapping WMCAMM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Receiver Master

    public class WebX_Master_ReceiverViewModel
    {
        public List<WebX_Master_Receiver> listWMR { get; set; }
        public WebX_Master_Receiver WMR { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Warehouse Master

    public class webx_GODOWNMSTViewModel
    {
        public List<webx_GODOWNMST> listWGMM { get; set; }
        public webx_GODOWNMST WGMM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Tyre

    public class TyreViewModel
    {
        public WEBX_FLEET_TYREMST tyreMST { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }
        public List<WEBX_FLEET_TYREMFG> ListMFG { get; set; }
        public List<WEBX_FLEET_TYREMODELMST> ListTyreModel { get; set; }
        public List<WEBX_FLEET_TYRESIZEMST> ListTyreSize { get; set; }
        public List<WEBX_FLEET_TYREPATTERN> ListTyrePattern { get; set; }
        public List<webx_VEHICLE_HDR> ListVehicle { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Company Employee & Company Location Mapping

    public class WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel
    {
        public List<WEBX_EMPLOYEE_COMPANY_MAPPING> listWECMM { get; set; }
        public WEBX_EMPLOYEE_COMPANY_MAPPING WECMM { get; set; }
        public List<Webx_Emp_Company> listWECM { get; set; }
        public string EditFlag { get; set; }
    }

    public class WEBX_LOCATION_COMPANY_MAPPINGViewModel
    {
        public List<WEBX_LOCATION_COMPANY_MAPPING> listWLCMM { get; set; }
        public WEBX_LOCATION_COMPANY_MAPPING WLCMM { get; set; }
        public List<Webx_Loc_Company> listWLCM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Vendor Contract

    public class Webx_Vendor_Contract_Summary_Ver1ViewModel
    {
        public Webx_Vendor_Contract_Summary_Ver1 WVCSV1 { get; set; }
        public List<Webx_Vendor_Contract_RouteBased> listWVCRM { get; set; }
        public List<Webx_Vendor_Contract_CityBased> listWVCCM { get; set; }
        public List<Webx_Vendor_Contract_DistanceBased> listWVCDM { get; set; }
        public List<Webx_Vendor_Contract_DocketBased> listWVCDoBCM { get; set; }
        public List<Webx_Vendor_Contract_DocketBased> listWVCDoDCM { get; set; }
        public string ContractID { get; set; }

    }

    #endregion

    #region DCR

    public class webx_DCR_HeaderViewModel
    {
        public webx_DCR_Header WDHM { get; set; }
        public List<webx_DCR_Header> listWDHM { get; set; }
        public Nullable<decimal> LocLevel { get; set; }
    }

    public partial class AlloteLocation
    {
        public string AllotToCode { get; set; }
        public string AllotToName { get; set; }
        public string AllotCat { get; set; }
    }

    public class vw_Series_ToBe_Alloted2ViewModel
    {
        public vw_Series_ToBe_Alloted2 VWSTBA2M { get; set; }
        public List<vw_Series_ToBe_Alloted2> listVWSTBA2M { get; set; }
        public string series { get; set; }
        public string AlloteTo { get; set; }
        public string Message { get; set; }
    }

    public class vw_DCR_RegisterViewModel
    {
        public vw_DCR_Register VWDRM { get; set; }
        public List<vw_DCR_Register> listVWDRM { get; set; }
        public vw_DCR_Management_History VWDMH { get; set; }
        public List<vw_DCR_Management_History> listVWDMH { get; set; }
    }

    public partial class AllottedSeries
    {
        public string DocumentType { get; set; }
        public string Contain { get; set; }
        public string ReportType { get; set; }
        public string RO { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Loccode { get; set; }
        public string BaseLoccode { get; set; }
        public Nullable<decimal> LocLevel { get; set; }

        public List<webx_location> RegionList { get; set; }
        public List<webx_location> LocationList { get; set; }
    }

    #endregion

    #region Work Group Master

    public class WEBX_FLEET_WORKGROUPMSTViewModel
    {
        public List<WEBX_FLEET_WORKGROUPMST> listWFWGM { get; set; }
        public WEBX_FLEET_WORKGROUPMST WFWGM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Task Type Master

    public class Webx_Fleet_TaskTypeMstViewModel
    {
        public List<Webx_Fleet_TaskTypeMst> listWFTTMM { get; set; }
        public Webx_Fleet_TaskTypeMst WFTTMM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Update Vehicle Km
    public class UpdateVehicleKmViewModel
    {
        public List<VehicleKm> listVehicleKm { get; set; }
        public VehicleKm VehicleKm { get; set; }
        public string EditFlag { get; set; }
    }
    public partial class VehicleKm
    {
        public int VEH_INTERNAL_NO { get; set; }
        public string VEHNO { get; set; }
        public string VehicleNo { get; set; }
        public string VSlipNo { get; set; }
        public string KMChgTyp { get; set; }
        public decimal Current_KM_Read { get; set; }
        public decimal Tmp_Current_KM_Read { get; set; }
        public decimal PRS_DRS_THC_KM { get; set; }
        public decimal f_issue_startkm { get; set; }
    }
    #endregion

    #region Mail Setting
    public class MailSettingViewModel
    {
        public List<Webx_Fleet_JobApprovalMailSettings> listWFJMSM { get; set; }
        public Webx_Fleet_JobApprovalMailSettings WFJMSM { get; set; }
        public string EditFlag { get; set; }
    }
    #endregion

    #region Jobsheet Approved Amount
    public class JSApproval_AmtViewModel
    {
        public List<Webx_Fleet_JS_Approval_Hrchy_Amt> listWFJSAHAM { get; set; }
        public Webx_Fleet_JS_Approval_Hrchy_Amt WFJSAHAM { get; set; }
        public string EditFlag { get; set; }
    }
    #endregion

    #region GENERAL TASK
    public class WEBX_FLEET_GENERALTASKMSTViewModel
    {
        public List<WEBX_FLEET_GENERALTASKMST> listWFGTM { get; set; }
        public WEBX_FLEET_GENERALTASKMST WFGTM { get; set; }
        public string EditFlag { get; set; }
    }
    #endregion

    #region Spare Part
    public class SparePartViewModel
    {
        public List<Webx_Fleet_spareParthdr> listWFSHM { get; set; }
        public Webx_Fleet_spareParthdr WFSHM { get; set; }
        public List<WEBX_FLEET_SPAREPARTDET> listWFSDM { get; set; }
        public WEBX_FLEET_SPAREPARTDET WFSDM { get; set; }
        public SparePartFilter SFM { get; set; }
        public string EditFlag { get; set; }
    }

    public partial class SparePartFilter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PartID { get; set; }
    }
    #endregion

    #region Jobsheet Approved Amount Matrix
    public class JSApproval_Amt_MatrixViewModel
    {
        public List<VW_FLEET_JS_APPROVAL_AMT_MATRIX> listWFJSAHAM { get; set; }
        public VW_FLEET_JS_APPROVAL_AMT_MATRIX WFJSAHAM { get; set; }
        public string EditFlag { get; set; }
    }
    #endregion

    #region SM Task

    public class SMTaskViewModel
    {
        public List<Webx_Fleet_SM_Task_Hdr> listWFSHM { get; set; }
        public Webx_Fleet_SM_Task_Hdr WFSHM { get; set; }
        public List<Webx_Fleet_SM_Task_Det> listWFSDM { get; set; }
        public Webx_Fleet_SM_Task_Det WFSDM { get; set; }
        public SMTask SFM { get; set; }
        public List<SMTask> listSFM { get; set; }
        public List<SMTaskList> listSMTASK { get; set; }
        public SMTaskList SMTASK { get; set; }
        // public List<TaskTypeList> listTasktype{ get; set; }
        public string EditFlag { get; set; }
    }
    public partial class SMTask
    {
        public string TYPE_NAME { get; set; }
        public int TYPE_CODE { get; set; }
        public int id { get; set; }
    }

    public partial class SMTaskList
    {
        public int SMTask_Id { get; set; }
        public string VEH_TYPE_CODE { get; set; }
        public string VEH_TYPE_NAME { get; set; }
    }
    public partial class TaskTypeList
    {
        public int TaskTypeID { get; set; }
        public string TaskType { get; set; }
    }
    #endregion

    #region SetOpeningBalance
    public class SetOpeningBalanceViewModel
    {
        public webx_acctinfo Balance { get; set; }
        public List<webx_acctinfo> Balancelist { get; set; }

    }

    #endregion

    //#region ChqDepositVoucher

    //public class ChqDepositVoucherViewModel
    //{
    //    public WEBX_chq_det ChqNO { get; set; }
    //    public List<WEBX_chq_det> ChqNOList { get; set; }

    //}
    //   #endregion

    #region Customer Group - Business Type Mapping
    public class WebX_master_grpbus_mappingViewModel
    {
        public WebX_master_grpbus_mapping WMGBM { get; set; }
        public List<WebX_master_grpbus_mapping> ListWMGBM { get; set; }
    }
    #endregion

    #region Vendor Service Mapping Master
    public class VendorServiceMappingViewModel
    {
        public List<webx_VENDOR_HDR> ListVendor { get; set; }
        public List<Webx_Master_General> Listservice { get; set; }
        public Webx_Master_General WMGM { get; set; }
        public Webx_Vendor_Service_Mapping_master WVSMM { get; set; }
        public List<Webx_Vendor_Service_Mapping_master> ListWVSMM { get; set; }
    }
    #endregion

    #region Mapping Master

    public partial class MappingService
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public string Mode { get; set; }
        public string OppCode { get; set; }
        public string StringName { get; set; }
        public string ModeType { get; set; }
        public string Scode { get; set; }
    }

    public class MappingViewModel
    {
        public MappingService MS { get; set; }
        public List<MappingService> ListMappingService { get; set; }

        public string CSMappingMode { get; set; }
        public string CSService { get; set; }
        public string CSCustomer { get; set; }
        public List<Webx_Cust_Service_Mapping> ListCustServiceMap { get; set; }

        public string ClMappingMode { get; set; }
        public string ClLocation { get; set; }
        public string ClCustomer { get; set; }
        public List<Webx_Cust_Location_Mapping> ListCustLocationMap { get; set; }

        public string SlMappingMode { get; set; }
        public string SlLocation { get; set; }
        public string SlService { get; set; }
        public List<Webx_Service_Location_Mapping> ListServiceLocationMap { get; set; }

        public string USMappingMode { get; set; }
        public string USUser { get; set; }
        public string USService { get; set; }
        public List<Webx_User_Service_Mapping> ListUserServiceMap { get; set; }

        public string UMMappingMode { get; set; }
        public string UMUser { get; set; }
        public string UMModule { get; set; }
        public string UMService { get; set; }
        public List<WebX_Master_Module> ListWMM { get; set; }
        public List<Webx_User_Module_Mapping> ListUserModuleMap { get; set; }

    }
    #endregion

    public class Webx_View_TrackViewModel
    {
        public Webx_View_Track WVTVM { get; set; }
        public List<Webx_View_Track> listWVTVM { get; set; }
        public ModuleAcessModel WVTVM1 { get; set; }
        public List<ModuleAcessModel> listWVTVM1 { get; set; }

    }

    public class CustomerServiceMappingViewModel
    {
        public webx_CUSTHDR WCM { get; set; }
        public List<webx_CUSTHDR> Custlist { get; set; }
        public List<webx_CUSTHDR> Custlist1 { get; set; }

    }

    #region HandlingCharge

    public class HandlingCharge
    {
        public List<webx_HandlingCharge> objChargesLoad { get; set; }
        public List<webx_HandlingCharge> objChargesUnload { get; set; }
        public List<ChgangeLoc> ListLocation { get; set; }
        public List<Webx_Master_General> ListLodingBy { get; set; }
        public List<CYGNUS_LoadingUnLoading_VendorType_Mapping> Listvndt { get; set; }
        public string Type { get; set; }
        public string LoadingBy { get; set; }
        public string Branch { get; set; }
    }

    public class LoadingUnloadingDataChange
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Type { get; set; }
        public string Branch { get; set; }
        public string DocumentNo { get; set; }
        public List<CYGNUS_LoadingUnLoading_Charges> dataList { get; set; }
    }

    #endregion

    public class DebtorsModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ASDate { get; set; }
        public string BillStatus { get; set; }
        public bool IsOSBill { get; set; }
        public string OSBill { get; set; }
    }

    public class BalanceTransferForLedgerSubLedgerViewModel
    {
        public string Ledger { get; set; }
        public string SubLedger { get; set; }
        public string PndLLedger { get; set; }
    }

    #region Add Account

    public partial class AddAccountModel
    {
        public string AccountCode { get; set; }
        public string AccountCategory { get; set; }
        public string Category { get; set; }
        public string AccGroup { get; set; }
        public string ManualAccountCode { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string IsAllBranch { get; set; }
        public string LocationCode { get; set; }
        public string BankLocationCode { get; set; }
        public string AccountDescription { get; set; }
        public string AccountNo { get; set; }
        public string Type { get; set; }
    }

    public partial class WEBX_GROUPS
    {
        public string Groupcode { get; set; }
        public string Groupdesc { get; set; }
        public string Main_Category { get; set; }
    }

    #endregion

    #region Add/Edit Account Groups

    public class AddEditAccountGroupViewModel
    {
        public List<webx_Groups> GroupsList { get; set; }
        public webx_Groups Group { get; set; }
        public webx_Groups EditGroup { get; set; }
    }

    #endregion

    #region Setting Opening Balance

    public class SettingOpeningBalanceViewModel
    {
        public List<OpeningBalanceCriteriaList> SearchList { get; set; }
        public SettingOpeningBalanceCriteria CriteriaMd { get; set; }
        public webx_acctinfo Balance { get; set; }
        public List<webx_acctinfo> Balancelist { get; set; }
        public List<Rslt> Result { get; set; }
    }

    public class Rslt
    {
        public string Id { get; set; }
        public int Status { get; set; }
        public string Msg { get; set; }
    }

    #endregion

    #region Customer Employee Mapping

    public class CustomerEmployeeMappingViewModel
    {
        public string MappingType { get; set; }
        public string EMP_CODE { get; set; }
        public string EMP_NAME { get; set; }
        public string CUST_CODE { get; set; }
        public string CUST_NAME { get; set; }
        public List<CustomerEmployeeMappingList> CusEmpList { get; set; }

        public string IsAllCust { get; set; }
        public string EmpId { get; set; }
        public string CustCode { get; set; }
        public string EMPCust { get; set; }

    }

    public class CustomerEmployeeMappingList
    {
        public string Display { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; }
    }

    #endregion
    
    #region PRS DRS Max Limit Master

    public class PRSDRSMaxViewModel 
    {
        public CYGNUS_PDC_MaxLimit ObjCPDCML { get; set; }
        public List<CYGNUS_PDC_MaxLimit> ListCPDCML { get; set; }
        public string PDCType { get; set; }
    }

    #endregion

    #region PDC Attached Vendor master

    public class PDCAttachedVendorViewModel
    {
        public PDC_attached_vendor_master objPAVM { get; set; }
        public List<PDC_attached_vendor_master> listobjPAVM { get; set; }
        public List<PDC_attached_vendor_master> listobjList { get; set; }
        public string PDCType { get; set; }
        public int type { get; set; }
    }

    #endregion


    #region  Customer Customer Mapping

    public class CCMappingVW
    {
        public CYGNUS_CUST_To_CUST_Mapping CCTCM { get; set; }
        public List<CYGNUS_CUST_To_CUST_Mapping> ListCCTCM { get; set; }
    }

    #endregion


    #region  Customer Customer Mapping
    public class AssignToCustVW
    {
        public webx_DCR_Header ObjDCRH { get; set; }

        public string BookCode { get; set; }
        public string SrFrom { get; set; }
    }
    #endregion

    #region  SubCustomer Assign Docket Series
    public class SubCustAssignDKTseriesViewModel
    {
        public CYGNUS_Docket_CustSeries_DET ObjCDCD { get; set; }
        public List<CYGNUS_Docket_CustSeries_DET> ListCDCD { get; set; }
    }
    #endregion

    #region  Customer to Customer Mapping
    public class CustomertoCustomerMappingViewmodel
    {
        public CYGNUS_CUST_To_CUST_Mapping ObjCCMV { get; set; }
        public List<CYGNUS_CUST_To_CUST_Mapping> ListCCMVL { get; set; }
    }

    #endregion

    #region PRQ Location change

    public class PRQLocationchangeViewModel
    {
        public CYGNUSPickupRequest ObjCPR { get; set; }
        public List<CYGNUSPickupRequest> ListCPDCML { get; set; }

    }

    #endregion

    #region Leave Master

    public class CYGNUS_Leave_MasterViewModel
    {
        public string Type { get; set; }
        public CYGNUS_Leave_Master LeaveMst { get; set; }
        public List<CYGNUS_Leave_Master> listLS { get; set; }

    }

    #endregion

    #region CrossingVendorMaster
    public class webx_vendor_Crossing_Contract_DetViewModel
    {
        public List<webx_vendor_Crossing_Contract_Det> listWVCCD { get; set; }
        public webx_vendor_Crossing_Contract_Det WVCCD { get; set; }

    }
    #endregion

    #region Role master
    public class Webx_Master_RoleViewModel
    {
        public string flag { get; set; }
        public Webx_Master_Role role { get; set; }
        public List<Webx_Master_Role> listrole { get; set; }

    }
    #endregion

    #region Industry - Product Mapping Master
 
    public class Webx_Industry_Product_MappingViewModel
    {
        public List<Webx_Industry_Product_Mapping> listWIPM { get; set; }
        public Webx_Industry_Product_Mapping WIPM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region Role - Employee Intigration Master

    public class Webx_Role_Employee_IntigrationViewModel
    {
        public List<Webx_Role_Employee_Intigration> listWREIVM { get; set; }
        public Webx_Role_Employee_Intigration WREIM { get; set; }
        public string EditFlag { get; set; }
    }

    #endregion

    #region  miscellaneous

    public class webx_master_Holiday_daywiseViewModel
    {
        public List<webx_master_Holiday_daywise> listWmHdwVM { get; set; }
        public webx_master_Holiday_daywise WMHDW { get; set; }

    }
    public class WebX_Master_Holiday_DatewiseViewModel
    {
        public List<WebX_Master_Holiday_Datewise> listWMHDWVM { get; set; }
        public WebX_Master_Holiday_Datewise WMHDW { get; set; }
        public string EditFlag { get; set; }

    }
    public class webx_News_UpdatesViewModel
    {
        public webx_News_Updates updates { get; set; }
        public List<webx_News_Updates> listupdates { get; set; }

    }
    public class webx_Customer_UpdatesViewModel
    {
        public webx_Customer_News_updates Custupdates { get; set; }
        public List<webx_Customer_News_updates> listCustupdates { get; set; }

    }
    public class DashboardModal
    {
        public DashboardDataModal JustInData { get; set; }
        public List<DashboardDataModal> NewsDataList { get; set; }
    }
    public class DashboardDataModal
    {
        public string Heading { get; set; }
        public string Updates { get; set; }
        public string Color { get; set; }
    }

    #endregion

    


}