﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using CYGNUS.Models;

namespace Fleet.ViewModels
{
    public class TripOpen
    {
        public WEBX_FLEET_VEHICLE_ISSUE WFVI { get; set; }
        public WEBX_TRIPSHEET_ADVEXP WTSA { get; set; }
        public List<VW_Fleet_CheckList_Module> ListFCM { get; set; }
        public List<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO> ListFuelSlipNo { get; set; }
        public Webx_Fleet_Triprule WFTP { get; set; }
        public Webx_Fleet_CheckList_hdr WFCLH { get; set; }
        public List<Webx_Fleet_CheckList_Detail> ListCheckListDetail { get; set; }
        public PaymentControl PC { get; set; }
    }

    public class CloseTripSheetViewModel
    {
        public WEBX_FLEET_DRIVERMST WFD { get; set; }
        public WEBX_TRIPSHEET_ADVEXP WTSA { get; set; }
        public WEBX_FLEET_VEHICLE_ISSUE WFVI { get; set; }
        public List<WEBX_FLEET_VEHICLE_ISSUE> POList = new List<WEBX_FLEET_VEHICLE_ISSUE>();
        public WEBX_FLEET_ENROUTE_EXP WFEE { get; set; }
        public List<WEBX_FLEET_ENROUTE_EXP> EnrtExpList = new List<WEBX_FLEET_ENROUTE_EXP>();
        public List<WEBX_FLEET_VEHICLE_ISSUE> List = new List<WEBX_FLEET_VEHICLE_ISSUE>();
        public List<WEBX_TRIPSHEET_ADVEXP> LISTAdvanceExp = new List<WEBX_TRIPSHEET_ADVEXP>();
        public List<WEBX_TRIPSHEET_ADVEXP> LIST_WTSADV = new List<WEBX_TRIPSHEET_ADVEXP>();
        public List<WEBX_TRIPSHEET_OILEXPDET> LIST_WTRSOP = new List<WEBX_TRIPSHEET_OILEXPDET>();
        public Webx_Fleet_Triprule Obj_WFT { get; set; }
        public List<vw_FleetFuelList> ListTSFF { get; set; }
        public List<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO> ListFuelSlip { get; set; }
        public PaymentControl PC { get; set; }

        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public string SlipId { get; set; }
        public string TripsheetFlag { get; set; }
        public string IssueID { get; set; }
        public string VehNo { get; set; }
        public string rutdesc { get; set; }
        public string rut_code { get; set; }
        public string driver_name { get; set; }
        public string License_no { get; set; }
        public string Telno { get; set; }
        public int SRNO { get; set; }
        public string Operational_Close { get; set; }
        public string Operational_Financial_Close { get; set; }

        //lavnit chages
        //string Amt = "", ExeAmt = "";
        //string sqlstr = "";

        public string TotAdvancePaid { get; set; }
        public string TotAdvancePaidAppr { get; set; }

        public string TotDieselCashExp { get; set; }
        public string TotDieselCashExpAppr { get; set; }
        public string TotDieselCardExp { get; set; }
        public string TotDieselCardExpAppr { get; set; }
        public string TotDieselCreditExp { get; set; }
        public string TotDieselCreditExpAppr { get; set; }

        public string TotRepairExp { get; set; }
        public string TotRepairExpAppr { get; set; }

        public string TotSpareExpAppr { get; set; }

        public string TotIncDedExp { get; set; }
        public string TotIncDedExpAppr { get; set; }

        public string TotClaimExp { get; set; }
        public string TotClaimExpAppr { get; set; }
        public string TotEnrouteExp { get; set; }
        public string TotEnrouteExpAppr { get; set; }


        public double TotAdvAmt { get; set; }
        public double TotAdvAmtAppr { get; set; }
        public double TotExpAmt { get; set; }
        public double TotExpAmtAppr { get; set; }
        public double TotNetExpAmt { get; set; }
        public double TotNetExpAmtAppr { get; set; }

        public string TotalFuelFilled { get; set; }
        public string txtFuelRate { get; set; }
        public double mApprovedFuel { get; set; }
        public double txtApprovedFuel { get; set; }

        public string TotSpareExp { get; set; }
        public string TotExpense { get; set; }
        public string TotExpenseAppr { get; set; }

        public string NetExpAmt { get; set; }
        public string NetExpAmtAppr { get; set; }
        public double AmtRecvdFromDriver { get; set; }
        public string AmtPaidToDriver { get; set; }
        public string remark { get; set; }

        public string BalToDriverLedger { get; set; }
        public double OpeningFuel { get; set; }
        public double txtExtraDiesel { get; set; }
        public string txtFuelCarryFwd { get; set; }

        public string Diesel_CF { get; set; }
        public string hdnDiesel_CF { get; set; }
        public string ddlPlusMinus { get; set; }


        public string IsApprove { get; set; }
    }

    public partial class vw_FleetFuelMgt
    {
        public string Mode { get; set; }

        public string FuelFilterType { get; set; }
        public string TripFTType { get; set; }
        public string TripSheetNo { get; set; }
        public string TripActionType { get; set; }

        public string THCFTType { get; set; }
        public string THCNo { get; set; }
        public string THCActionType { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public List<vw_FleetFuelList> ListTSFF { get; set; }

    }

    public partial class vw_FleetFuelList
    {
        public string VSlipNo { get; set; }
        public string Manual_TripsheetNo { get; set; }
        public string VSlipDt { get; set; }
        public string Category { get; set; }
        public string Trip_route_type { get; set; }
        public string VehicleNo { get; set; }
        public string DriverName { get; set; }
        public string License_No { get; set; }
        public string Valdity_dt { get; set; }
        public string Oper_Close_Dt { get; set; }
        public decimal Start_km { get; set; }
        public decimal f_issue_fill { get; set; }
        public string Tripsheet_StartLoc { get; set; }
        public string Tripsheet_EndLoc { get; set; }
        public string Market_Own { get; set; }
        public string driver1 { get; set; }
        public List<vw_FleetFuelRowList> FFRList { get; set; }
        public vw_FleetFuelRowList FFRModel { get; set; }
        public string Mode { get; set; }
    }

    public partial class vw_FleetFuelRowList
    {
        public int SLIPTYPE { get; set; }
        public int SRNO { get; set; }
        public string FUELSLIPNO { get; set; }
        public string FUELVENDOR { get; set; }
        public decimal QUANTITYINLITER { get; set; }
        public decimal RATE { get; set; }
        public decimal AMOUNT { get; set; }
    }

    public class JobOrderViewModel
    {
        public WEBX_FLEET_PM_JOBORDER_HDR ObjWFPJH { get; set; }
        public List<Webx_Master_General> ListAssetType { get; set; }
        public List<Webx_Master_General> ListJOBCARDTYP { get; set; }
        public List<webx_VENDOR_HDR> ListVendor_List { get; set; }
        public List<webx_location> ListLocation { get; set; }
        public List<WEBX_FLEET_PM_JOBORDER_DET> ListWFPJOD { get; set; }
        public List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET> ListWFPJOSED { get; set; }
        public List<Webx_Fleet_Vehicle_SMTask_Mst> ListWFVSMM { get; set; }
        public List<VW_JOBSHEET_SUMMARY> ListBindSummary { get; set; }
        public Webx_Fleet_Triprule ObjFleet_Triprule { get; set; }
        public string ISClose { get; set; }
        public string IsOperatinllyClose { get; set; }
        public string IsFinanciallyClose { get; set; }

        public decimal TOT_EST_LABOUR_HRS_Total { get; set; }
        public decimal TOT_EST_LABOUR_COST_Total { get; set; }

        /*Advance Payment Details*/
        public bool IsAdvancePaid { get; set; }
        public string Staff_Vendor { get; set; }
        public string Vendor_Employee { get; set; }
        public PaymentControl PC { get; set; }
        public string Staff_Vendor_Name { get; set; }
        public List<CYGNUS_Job_Order_AdvanceEntry> ListCJOAdv { get; set; }
    }

    public class VW_JOBSHEET_SUMMARY
    {
        public string TaskType { get; set; }
        public string ACCDESC { get; set; }
        public decimal TOT_LABOUR_COST { get; set; }
        public decimal TOT_SPARE_COST { get; set; }
        public int SrNo { get; set; }
    }

    public class JobApprovalViewModel
    {
        public string Type { get; set; }
        public string JobNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string JobCardType { get; set; }
        public List<Webx_Master_General> ListJobCardType { get; set; }
        public List<JobApprovalGenerationViewModel> List_JAGVW { get; set; }
    }

    public class JobApprovalGenerationViewModel
    {
        public string JOB_ORDER_NO { get; set; }
        public string JOB_ORDER_DT { get; set; }
        public string VEHNO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string SERVICE_CENTER_TYPE { get; set; }
        public string JOB_ORDER_CLOSEDT { get; set; }
        public string ORDER_STATUS { get; set; }
        public decimal Tot_Estimated_Cost { get; set; }
        public decimal TOT_ACTUAL_COST { get; set; }
        public string JS_BRCD { get; set; }
        public string JS_Approval_Close_Remarks { get; set; }
        public string Email_To { get; set; }
        public string Emaol_CC { get; set; }
        public string Company_Code { get; set; }
        public string JS_Approval_Remarks { get; set; }
        public bool Approve { get; set; }
        public bool Reject { get; set; }
        public bool IsCheck { get; set; }

        public string JS_Approve { get; set; }
        public string JS_Approve_By { get; set; }
        public string JS_Reject { get; set; }
        public string JS_Reject_By { get; set; }
        public string Cancel_Dt { get; set; }
        public int SrNo { get; set; }
    }

    public class JobOrderCancelViewModel
    {
        public List<WEBX_FLEET_PM_JOBORDER_HDR> ListWFPJH { get; set; }
    }

    public class AdvanceEntryViewModel
    {
        public WEBX_FLEET_VEHICLE_ISSUE ObjWFVI { get; set; }
        public Webx_Fleet_Triprule ObjWFT { get; set; }
        public List<WEBX_TRIPSHEET_ADVEXP> ListWFVI { get; set; }
        public WEBX_TRIPSHEET_ADVEXP ObjWTADV { get; set; }
        public DriverSetllementViewModel ObjDSDV { get; set; }
        public PaymentControl PC { get; set; }
        public decimal Amount { get; set; }
        public decimal NET_PAYABLE { get; set; }
    }

    public class DriverSetllementViewModel
    {
        public decimal Total_Advance_Paid { get; set; }
        public decimal Total_Advance_Paid_Appr { get; set; }
        public decimal Total_Expenses { get; set; }
        public decimal Total_Expenses_Appr { get; set; }
        public decimal Diesel_expense_against_cash { get; set; }
        public decimal Diesel_expense_against_cash_Appr { get; set; }
        public decimal Repair_Expense { get; set; }
        public decimal Repair_Expense_Appr { get; set; }
        public decimal Spare_Expense { get; set; }
        public decimal Spare_Expense_Appr { get; set; }
        public decimal Incidental_Deduction_Expense { get; set; }
        public decimal Incidental_Deduction_Expense_Appr { get; set; }
        public decimal Claims_Expense { get; set; }
        public decimal Claims_Expense_Appr { get; set; }
        public decimal Enroute_Expense { get; set; }
        public decimal Enroute_Expense_Appr { get; set; }
        public decimal Diesel_expense_against_card_Expense { get; set; }
        public decimal Diesel_expense_against_card_Expense_Appr { get; set; }
        public decimal Diesel_expense_against_credit_Expense { get; set; }
        public decimal Diesel_expense_against_credit_Expense_Appr { get; set; }
        public decimal Net_amount_Expense { get; set; }
        public decimal Net_amount_Expense_Appr { get; set; }
        public decimal Net_Amount_to_Driver_Payable { get; set; }
        public decimal Net_Amount_to_Driver_Receivable { get; set; }
        public decimal Trip_Sheet_balance_to_driver_ledger { get; set; }
        public decimal Opening_Fuel { get; set; }
        public decimal Total_Fuel_Filled { get; set; }
        public decimal Fuel_Rate { get; set; }
        public decimal Approved_Fuel { get; set; }
        public decimal Extra_Diesel { get; set; }
        public decimal Net_Fuel_Carry_Forward { get; set; }
        public decimal Trip_Sheet_net_balance { get; set; }
        public string Remarks { get; set; }
        public string Carry_Forward { get; set; }
        public string Card_Cash { get; set; }
        public decimal Diesel_Expense { get; set; }
        public decimal Diesel_Expense_Appr { get; set; }
    }

    public class VehicleIssueSlipQueryViewModel
    {
        public List<List_Type> ListType { get; set; }
        public List<IssueSlipQueryViewModel> ListIsuueSlipVW { get; set; }
        public string Flag { get; set; }
        public string Type { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string TripType { get; set; }
        public string TripNo { get; set; }
        public string Passed_Type { get; set; }
    }

    public class IssueSlipQueryViewModel
    {
        public string Manual_TripsheetNo { get; set; }
        public string VSlipNo { get; set; }
        public string VSlipDt { get; set; }
        public string VehicleNo { get; set; }
        public string TripSheet_EndLoc { get; set; }
        public string Trip_route_type { get; set; }
        public string Category { get; set; }
        public string DriverName { get; set; }
        public string License_No { get; set; }
        public string Valdity_dt { get; set; }
        public int SrNo { get; set; }
        public string Type { get; set; }
    }

    public class List_Type
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    #region  Job Order & Trip Sheet View Print

    public partial class JobTripViewPrintViewModel
    {
        public string TripFTType { get; set; }
        public string TripSheetNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string BRCD { get; set; }
        public string Status { get; set; }
        public string Driver { get; set; }
        public string Vehicle { get; set; }
        public List<vm_JobTripViewPrint> listJobSheet { get; set; }
    }
    public partial class vm_JobTripViewPrint
    {
        public string JOB_ORDER_NO { get; set; }
        public string JOB_ORDER_DT { get; set; }
        public string JOB_ORDER_CLOSEDT { get; set; }
        public string VEHNO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string SERVICE_CENTER_TYPE { get; set; }
        public string ORDER_STATUS { get; set; }
    }

    #endregion

    #region Trip Route
    public class TripRouteViewModel
    {
        public List<webx_trip_rutmas> listWRMT { get; set; }
        public webx_trip_rutmas WRMT { get; set; }
        public List<webx_trip_ruttran> listWRTT { get; set; }
        public webx_trip_ruttran WRTT { get; set; }
    }
    #endregion

    #region Tyre Odometer Search
    public class TyreOdometerSearch
    {
        public List<webx_VEHICLE_HDR> WVHL { get; set; }
        public webx_VEHICLE_HDR WVH { get; set; }
    }
    #endregion

    #region  Fuel Brand Master

    public partial class FuelBrandViewModel
    {
        public List<FuelBrandModel> ListFuelBrand { get; set; }
        public FuelBrandModel FBM { get; set; }
        public string EditFlag { get; set; }
    }

    public partial class FuelBrandModel
    {
        public int Fuel_Brand_ID { get; set; }
        public string Fuel_Brand_Name { get; set; }
        public string VendorCode { get; set; }
        public string Fuel_Type_ID { get; set; }
        public string Active_Flag { get; set; }
    }

    #endregion

    #region  Fuel Card Master

    public partial class FUELCARDViewModel
    {
        public List<WEBX_FLEET_FUELCARD> ListFuelCard { get; set; }
        public WEBX_FLEET_FUELCARD FCM { get; set; }
        public string EditFlag { get; set; }
    }

    public partial class WEBX_FLEET_FUELCARD
    {
        public int Fuel_Card_ID { get; set; }
        public string Manual_Fuel_Card_No { get; set; }
        public string VendorCode { get; set; }
        public DateTime Issue_Date { get; set; }
        public DateTime Expiry_Date { get; set; }
        public string Vehicle_No { get; set; }
        public string Fuel_Type_ID { get; set; }
        public int Fuel_Brand_ID { get; set; }
        public string Active_Flag { get; set; }
        public string acccode { get; set; }
        public string parentAcccode { get; set; }
        public string Fuel_Brand_Name { get; set; }
    }

    #endregion

    #region  CheckList

    public class CheckListViewModel
    {
        public List<WEBX_FLEET_CHECKLIST_MST> listCheckList { get; set; }
        public WEBX_FLEET_CHECKLIST_MST CheckListModel { get; set; }
    }

    public class WEBX_FLEET_CHECKLIST_MST
    {
        public decimal chk_id { get; set; }
        public string Chk_Cat_name { get; set; }
        public string Chk_Document_Shown_name { get; set; }
        public string Chk_Desc { get; set; }
        public string chk_cat { get; set; }
        public string Chk_Document_Shown { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDt { get; set; }
        public string Company_Code { get; set; }
    }



    #endregion

    #region  Document Type Master

    public partial class DOCU_TYPEViewModel
    {
        public List<WEBX_FLEET_DOCU_TYPE_MST> ListDocType { get; set; }
        public WEBX_FLEET_DOCU_TYPE_MST ModelDocType { get; set; }
        public string EditFlag { get; set; }
    }

    public partial class WEBX_FLEET_DOCU_TYPE_MST
    {
        public int DOCU_TYPE_ID { get; set; }
        public string DOCU_TYPE { get; set; }
        public string DECS { get; set; }
        public string APPLICABLE_STATE { get; set; }
        public int RENEW_AUTH_ID { get; set; }
        public string COST_CAPTURED { get; set; }
        public string ACTIVE_FLAG { get; set; }
    }

    #endregion

    #region Driver
    public class DriverViewModel
    {
        public List<WEBX_FLEET_DRIVERMST> listWFDM { get; set; }
        public WEBX_FLEET_DRIVERMST WFDM { get; set; }
        public List<WEBX_FLEET_DRIVER_DOCDET> listWFDDD { get; set; }
        public WEBX_FLEET_DRIVER_DOCDET WFDDD { get; set; }
    }
    #endregion

    #region Trip Route Expence

    public class TripRouteExpenceViewModel
    {
        public List<vw_TRIPROUTEEXP_VIEW> listVWTRE { get; set; }
        public vw_TRIPROUTEEXP_VIEW VWTRE { get; set; }
    }

    #endregion

    #region Trip Route Fuel Expence

    public class TripRouteFuelExpenceViewModel
    {
        public List<vw_TRIPROUTEFuelEXP_VIEW> listVWTRFE { get; set; }
        public vw_TRIPROUTEFuelEXP_VIEW VWTRFE { get; set; }
    }

    #endregion

    #region Document Entry
    public class DocumentEntryViewModel
    {
        public List<WEBX_FLEET_DOCUMENT_DET> WFDL { get; set; }
        public WEBX_FLEET_DOCUMENT_DET WFDD { get; set; }
    }
    #endregion

    #region Tyre Removal

    public class TyreRemovalViewModel
    {
        public List<VW_TyreDetails_New> listVWTDNM { get; set; }
        public VW_TyreDetails_New VWTDNM { get; set; }
        public string TrucNo { get; set; }
        public DateTime RemovalDt { get; set; }
        public decimal Tyre_Pr_Alex { get; set; }
        public decimal Actual_Tyre { get; set; }
        public decimal Pending_Tyre { get; set; }
    }

    #endregion

    #region Tyre Interchange

    public partial class TyreDetail
    {
        public string TYRE_VEHNO { get; set; }
        public string TYRE_ID { get; set; }
        public string TYRE_NO { get; set; }
        public int TYRE_MODEL_ID { get; set; }
        public string MODEL_DESC { get; set; }
    }

    public class TyreInterchangeViewModel
    {
        public List<TyreDetail> listVWTDNM { get; set; }
        public TyreDetail VWTDNM { get; set; }
        public string TruckNo1 { get; set; }
        public string Read_ODO1 { get; set; }
        //public IList<SelectListItem> GCNO { get; set; }
        public string Truck1Tyres { get; set; }
        public string Truck2Tyres { get; set; }
        public string TruckNo2 { get; set; }
        public string Read_ODO2 { get; set; }
    }

    #endregion

    #region Tyre IssueMaster
 
    public class TyreIssueMasterViewModel
    {
        public string CheckVhNo { get; set; }
        public DateTime ChkDate { get; set; }
        public int RemovalKM { get; set; }
        public string Type { get; set; }

        public List<VW_TyreDetails_New> VWTDNL { get; set; }
        public List<VW_TyreDetails_New> VWTDNL1 { get; set; }
        public VW_TyreDetails_New VWTDN { get; set; }
    }
    #endregion

    #region OldTyre Stock

    public class OldTyreStockViewModel
    {
        public string Action { get; set; }
        public List<WEBX_FLEET_TYREMST_HISTORY> WFTHL { get; set; }
        public WEBX_FLEET_TYREMST_HISTORY WFTH { get; set; }
    } 
        #endregion

    #region Update Remold Tyre

    public class UpdateRemoldTyreViewModel
    {
        public vm_UpdateRemoldTyre RemoldModel { get; set; }
        public List<UpdateRemoldTyreList> ListRemolTyre { get; set; }
        public List<RemoldTyreList_DET> RemolTyreDet { get; set; }
        public List<RemoldTyreBillPayment> RemolBillPayment { get; set; }
        public RemoldTyreList_DET RemoldModelDet { get; set; }
        public RemoldTyreBillPayment BillPay { get; set; }
    }

    public class vm_UpdateRemoldTyre
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string RemoldNo { get; set; }


        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }
    }

    public class UpdateRemoldTyreList
    {
        public int NO_OF_TYRE { get; set; }
        public string CLAIM_REMOLD_SALE_ID { get; set; }
        public string VendorCode { get; set; }
        public string ACTION_DT { get; set; }
    }

    public class RemoldTyreList_DET
    {
        public int SR_NO { get; set; }
        public string CLAIM_REMOLD_SALE_ID { get; set; }
        public string TYRE_ID { get; set; }
        public string TYRE_NO { get; set; }
        public string MODEL { get; set; }
        public string PATTERN { get; set; }
        public string SIZE { get; set; }
        public string MFG { get; set; }
        public string Fittment_DT { get; set; }
        public string TYRE_REMOVE_DT { get; set; }
        public decimal Dist_Covered { get; set; }
        public string Remark { get; set; }
        public string VendorCode { get; set; }
        public string ACTION_DT { get; set; }
        public string Active { get; set; }
        public string Rejection { get; set; }
        public string REMOLD_YN { get; set; }
        public decimal NetPay { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public string Claim_DT { get; set; }
        public string Claim_YN { get; set; }

        public string GSTType { get; set; }
        public double tax_per { get; set; }
        public decimal GSTAmount { get; set; }
        public decimal TotalAmount { get; set; }

        public decimal GSTPercentage { get; set; }
        public string StateCode { get; set; }
        public decimal NETAMT { get; set; }
    }

    public class RemoldTyreBillPayment
    {
        public string VENDORCODE { get; set; }
        public string VENDORNAME { get; set; }
        public string PAN_NO { get; set; }
        public string BILLNO { get; set; }
        public string VendorBill_DT { get; set; }
        public double NETAMT { get; set; }
        public double Advpaid { get; set; }
        public double pendamt { get; set; }
        public string acccode { get; set; }
        public decimal CurrentAmt { get; set; }
        public decimal TotalAmt { get; set; }
        public string Active { get; set; }
        public string selectDate { get; set; }
    }

    #endregion

    #region View / Print Job Order

    public class JobOrderCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string JobOrderNo { get; set; }
        public string OrderStatus { get; set; }
    }

    #endregion 

    #region View / Print Tyre Register

    public class TyreRegisterViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string fromloc { get; set; }
        public string TyreNo { get; set; }
        public string Make { get; set; }
        public string VehicleNo { get; set; }
        public string PoNo { get; set; }

    }

    #endregion

    #region View / Print Tyre life cycle ViewModel

    public class TyrelifecycleViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string fromloc { get; set; }
        public string TyreNo { get; set; }
        public string TyreModelNo { get; set; }
        public string GRNNo { get; set; }

    }

    #endregion

    #region View / Print Tyre Traking ViewModel

    public class TyreTrakingViewModel
    {
        public string TyreNo { get; set; }
        public string VehicleNo { get; set; }
    }

    #endregion

    public class TripAdvanceXlsModel
    {
        public List<TripAdvanceEntryXls> AdvanceVoucherList { get; set; }
    }

    public partial class AdvanceEntryXLSDone
    {
        public string VoucherNo { get; set; }
        public double Amount { get; set; }
    }

}