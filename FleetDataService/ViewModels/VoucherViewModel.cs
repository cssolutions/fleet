﻿using CYGNUS.Models;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.ViewModels
{
    public class VoucherViewModel
    {
    }

    public class DebitCreditVoucherViewModel
    {
        public Webx_Account_Details WAD { get; set; }
        public List<Webx_Account_Details> WADListing { get; set; }
        public Webx_Acc WA { get; set; }
        public List<Webx_Acc> WADList { get; set; }
        public decimal HdnServiceTaxRate { get; set; }
        public decimal HdnEduCessRate { get; set; }
        public decimal HdnHEduCessRate { get; set; }
        /*-- For Voucher BackDate Access -- */
        public Cygnus_Master_Voucher_BackDate_Access BackDateAccess { get; set; }

        //Bharat
        public string PartyCode { get; set; }
        public string PartyName { get; set; }
        public string Vendor_Type { get; set; }
        public string InvoiceNo { get; set; }
        public string HSNCode { get; set; }
        public string SACNO { get; set; }
        public string GSTIN { get; set; }
        public DateTime Date { get; set; }
        public bool IsPercentageShow { get; set; }
        public string Nature_Of_Transaction { get; set; }
        public bool isGSTReverse { get; set; }
        public string LocAddress { get; set; }
        public string Remarks { get; set; }
        public List<Webx_Master_General> ListGnMST { get; set; }

        public StaxTDSViewModel OBJstaxtds { get; set; }
        public string AccountCode { get; set; }
    }

    //public partial class GetCashDetailList
    //{
    //    public string Text { get; set; }
    //    public string Value { get; set; }
    //}
    //public partial class GetBankDetailList
    //{
    //    public string Text { get; set; }
    //    public string Value { get; set; }
    //}
    public partial class GetCostTypeFromAccountCode
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
    public partial class GetLedgerAccountFromPaymode
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class MJVViewModel
    {
        public Webx_Account_Details WAD { get; set; }
        public List<Webx_Account_Details> WADListing { get; set; }
        public Webx_Acc WA { get; set; }
        public List<Webx_Acc> WADList { get; set; }
    }

    public class AdviceAcknowledgementViewModel
    {
        public WEBX_Advice_Hdr Advice { get; set; }
        public List<WEBX_Advice_Hdr> ListAdvice { get; set; }
        public DateTime datefrom { get; set; }
        public DateTime dateto { get; set; }
        public string raisedon { get; set; }

        public string transtable { get; set; }
        public string Adviceno { get; set; }
        public string ORGNCD { get; set; }
        public string LOCTYP { get; set; }
        public string STATUS { get; set; }

        public string advicestatus { get; set; }
        public bool IsCheck { get; set; }
        public Webx_Account_Details WAD { get; set; }
        public List<Webx_Account_Details> WADListing { get; set; }
    }

    public class AdviceCancellationViewModel
    {
        public WEBX_Advice_Hdr Advice { get; set; }
        public List<WEBX_Advice_Hdr> ListAdvice { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string raisedon { get; set; }

        public string transtable { get; set; }
        public string Adviceno { get; set; }
        public string ORGNCD { get; set; }
        public string LOCTYP { get; set; }
        public string STATUS { get; set; }

        public string advicestatus { get; set; }
        public bool IsCheck { get; set; }
    }

    public class ChqDepositVoucherCriteriaViewModel
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Chqno { get; set; }
        public string transdate { get; set; }
    }

    public class ChqDepositVoucherViewModel
    {
        public WEBX_chq_det chqdet { get; set; }
        public List<WEBX_chq_det> ChqdetList { get; set; }
        public string transdate { get; set; }
        public string Voucherno { get; set; }
    }

    public partial class GetDeposited_InList
    {
        public string acccode { get; set; }
        public string accdesc { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Cygnus_Master_Voucher_BackDate_Access
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public int AllowedDays { get; set; }
        public string ModuleName { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string DaysType { get; set; }
    }
}