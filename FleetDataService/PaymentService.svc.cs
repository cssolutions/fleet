﻿using CYGNUS.Models;
using Fleet.Classes;
using Fleet.ViewModels;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PaymentService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PaymentService.svc or PaymentService.svc.cs at the Solution Explorer and start debugging.
    public class PaymentService : IPaymentService
    {
        GeneralFuncations GF = new GeneralFuncations();
        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region BA Bill Entry

        public VendorContractDetails GetVendorContract(string VendorCode)
        {
            string QueryString = "EXEC [Usp_GetVendorContractDetail] '" + VendorCode + "','',''";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VendorContractDetails> VendorTypeList = DataRowToObject.CreateListFromTable<VendorContractDetails>(DT);
            return VendorTypeList.FirstOrDefault();
        }

        public List<BABillEntyGC> GetDocketList(BABillEntyQuery BBQ)
        {
            //string QueryString = "EXEC [Usp_BA_Docket_ListingGST] '" + BBQ.VendorCode + "','" + BBQ.Location + "','" + BBQ.BookingDelivery + "','" + BBQ.FromDate.ToString("dd MMM yyyy") + "','" + BBQ.ToDate.ToString("dd MMM yyyy") + "','" + BBQ.DocumentNo + "','" + BBQ.DATETYPE + "','" + BBQ.StateCode + "','" + BBQ.GSTType + "'";
            string QueryString = "";
            //if (BBQ.billType == "B")
            //{
            //    QueryString = "EXEC [Usp_BA_Docket_Listing] '" + BBQ.VendorCode + "','" + BBQ.Location + "','" + BBQ.BookingDelivery + "','" + BBQ.FromDate.ToString("dd MMM yyyy") + "','" + BBQ.ToDate.ToString("dd MMM yyyy") + "','" + BBQ.DocumentNo + "','" + BBQ.DATETYPE + "'";


            //}
            //else if (BBQ.billType == "F")
            //{
            //    QueryString = "EXEC [Usp_Franchise_Docket_Listing] '" + BBQ.VendorCode + "','" + BBQ.Location + "','" + BBQ.BookingDelivery + "','" + BBQ.FromDate.ToString("dd MMM yyyy") + "','" + BBQ.ToDate.ToString("dd MMM yyyy") + "','" + BBQ.DocumentNo + "','" + BBQ.DATETYPE + "'";
            //}
            QueryString = "EXEC [Usp_Bind_Docket_detail_for_BABILL_New] '" + BBQ.Location + "','" + BBQ.BookingDelivery + "','" + BBQ.DATETYPE + "','" + BBQ.FromDate.ToString("dd MMM yyyy") + "','" + BBQ.ToDate.ToString("dd MMM yyyy") + "','" + BBQ.VendorCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BABillEntyGC> ItemList = DataRowToObject.CreateListFromTable<BABillEntyGC>(DT);
            return ItemList;
        }

        public List<BABillEntyGC> GetDocketListForBACnotedetail(BABillEntyQuery BBQ)
        {
            string QueryString = "";

            QueryString = "EXEC [Usp_GetDocketListForBACnotedetail] '" + BBQ.BookingDelivery + "','" + BBQ.Location + "','" + BBQ.FromDate.ToString("dd MMM yyyy") + "','" + BBQ.ToDate.ToString("dd MMM yyyy") + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BABillEntyGC> ItemList = DataRowToObject.CreateListFromTable<BABillEntyGC>(DT);
            return ItemList;
        }
        public List<CYGNUS_BA_ServiceTax> GetBAStax(string DocumentDate)
        {
            string QueryString = "SELECT TOP 1 * FROM CYGNUS_BA_ServiceTax  WHERE '" + DocumentDate + "' BETWEEN startdate AND EndDate";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_BA_ServiceTax> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_BA_ServiceTax>(DT);
            return ItemList;
        }

        public List<CYGNUS_BA_ServiceTax> GetBAStaxList(int id)
        {
            string QueryString = "SELECT * FROM CYGNUS_BA_ServiceTax";
            if (id > 0)
                QueryString = QueryString + " WHERE SrNo = " + id;
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_BA_ServiceTax> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_BA_ServiceTax>(DT);
            return ItemList;
        }

        public DataTable GetVendorServiceNoAndPanNo(string VendorCode)
        {
            string QueryString = "EXEC [Usp_GetVendorServiceNoAndPanNo] '" + VendorCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public List<CYGNUS_BA_ServiceTax> GetBAStaxList(int? id)
        {
            string QueryString = "SELECT * FROM CYGNUS_BA_ServiceTax";
            if (id > 0)
                QueryString = QueryString + " WHERE SrNo = " + id;
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_BA_ServiceTax> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_BA_ServiceTax>(DT);
            return ItemList;
        }

        public List<Webx_Master_General> GetTDSLedger()
        {
            string QueryString = "select CodeId=Acccode,CodeDesc=Accdesc from vw_tds_payable";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> ItemList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);

            return ItemList;
        }

        //public DataTable Insert_BA_BillEntry_Data(string Xml_PAY_Mst, string Xml_PAY_Det, string Xml_GSTCharges, string EntryType, string Location, string FinYear, string CurrentEmployeeID)
        //{
        //    //string Voucherno = "";
        //    string Sql = "";
        //    if (EntryType == "BA_Bill")
        //        Sql = "EXEC [Usp_Doc_BA_BillEntry_NewPortalGST] '" + Location + "','" + Xml_PAY_Det + "','" + Xml_PAY_Mst + "','" + Xml_GSTCharges + "','" + FinYear + "','" + CurrentEmployeeID + "'";
        //    int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "Insert_BillEntry_BA_Bill", "", "");
        //    DataTable DT = GF.GetDataTableFromSP(Sql);
        //    //Voucherno = DT.Rows[0][0].ToString() ;
        //    return DT;
        //}

        public DataTable Insert_BA_BillEntry_Data(string Xml_PAY_Mst, string Xml_PAY_Det, string Xml_Docket_Details, string FromDate, string ToDate, string Location)
        {

            string Sql = "";
            Sql = "EXEC [usp_Generate_VendorBABill_New] '" + Xml_PAY_Mst + "','" + Xml_PAY_Det + "','" + Xml_Docket_Details + "','" + FromDate + "','" + ToDate + "','" + Location + "'";
            int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "Insert_BillEntry_BA_Bill", "", "");
            DataTable DT = GF.GetDataTableFromSP(Sql);
            return DT;
        }

        public DataTable Insert_BillEntry(string Xml_PAY_Mst, string Xml_PAY_Det, string Xml_GST_Det, string EntryType, string LOcation, string BaseYearVal, string FinYear, string CurrentEmployeeID)
        {
            string SQRY = "";
            SQRY = "EXEC [Usp_BillEntryGST] '" + LOcation + "','" + Xml_PAY_Det + "','" + Xml_PAY_Mst + "','" + Xml_GST_Det + "','" + FinYear + "','" + BaseYearVal + "','" + CurrentEmployeeID.ToString() + "','" + EntryType + "' ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_BillEntry_" + EntryType, "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Insert_FixedVariableBillEntry(string xml_PAY_Mst, string xml_PAY_Det, string xml_GST_Det, string location, string finYear, string baseYearVal, string userId, string entryType)
        {
            string SQRY = "";
            SQRY = "EXEC USP_FixedExpenseBillEntryGST '" + location + "','" + xml_PAY_Det + "','" + xml_PAY_Mst + "','" + xml_GST_Det + "','" + finYear + "','" + baseYearVal + "','" + userId + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_BillEntry_" + entryType, "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }
        #endregion

        #region Advance Balance Payment

        public List<VW_THC_AdvancePayment> PaymentList(AdvancePaymentFilter APF,string Username)
        {
            string QueryString = "EXEC WEBX_Vendor_Payment_Listing '" + APF.DocType + "','" + APF.Type + "','" + GF.FormateDate(APF.FromDate) + "','" + GF.FormateDate(APF.ToDate) + "','" + APF.VendorCode + "','" + APF.SDocNo + "','" + APF.MDocNo + "','','" + APF.BaseLocation + "','" + APF.DocType + "','','','" + Username + "'";
            return DataRowToObject.CreateListFromTable<VW_THC_AdvancePayment>(GF.GetDataTableFromSP(QueryString));
        }

        public List<webx_master_charge> ListCharge(string ChargeType)
        {
            string QueryString = "SELECT *,operator1=operator FROM webx_master_charge where isnull(activeflag,'Y')='Y' and chargetype='" + ChargeType + "'";
            return DataRowToObject.CreateListFromTable<webx_master_charge>(GF.GetDataTableFromSP(QueryString));
        }

        public List<VW_THC_BalancePayment> BalancePaymentList(AdvancePaymentFilter APF,string UserName)
        {
            //string QueryString = "EXEC WEBX_Vendor_Payment_Listing '" + APF.DocType + "','" + APF.Type + "','" + GF.FormateDate(APF.FromDate) + "','" + GF.FormateDate(APF.ToDate) + "','" + APF.VendorCode + "','" + APF.SDocNo + "','" + APF.MDocNo + "','','" + APF.BaseLocation + "','" + APF.DocType + "','" + APF.ContractSubtype + "','','" + APF.GSTType + "','" + APF.StateCode + "'";
            string QueryString = "EXEC WEBX_Vendor_Payment_Listing '" + APF.DocType + "','" + APF.Type + "','" + GF.FormateDate(APF.FromDate) + "','" + GF.FormateDate(APF.ToDate) + "','" + APF.VendorCode + "','" + APF.SDocNo + "','" + APF.MDocNo + "','','" + APF.BaseLocation + "','" + APF.DocType + "','" + APF.ContractSubtype + "','','" + UserName + "'";

            return DataRowToObject.CreateListFromTable<VW_THC_BalancePayment>(GF.GetDataTableFromSP(QueryString));
        }

        public List<Webx_Master_General> GetFTLVehiclefromContract(string VendorCode, string ContractId, string ContractType)
        {
            string QueryString = "EXEC Usp_GetVendorContractDetail '" + VendorCode + "','" + ContractId + "','" + ContractType + "'";
            return DataRowToObject.CreateListFromTable<Webx_Master_General>(GF.GetDataTableFromSP(QueryString));
        }

        public List<Webx_Master_General> LedgerListFromCategory(string Category, string Location, string VehicleNo, string ModuleType)
        {
            string QueryString = "EXEC USP_GetLedgerListFromCategory '" + Category + "','" + Location + "','" + VehicleNo + "','" + ModuleType + "'";
            //string QueryString = "";
            //if (Category == "Cash")
            //    QueryString = "select acccode  as CodeId,CodeDesc=accdesc from webx_acctinfo where  acccategory='CASH'";
            //else if (Category == "Bank")
            //    QueryString = "select acccode  as CodeId,CodeDesc=accdesc from webx_acctinfo where  ( ((bkloccode like 'All' or PATINDEx ('%" + Location + "%',bkloccode)>0) AND acccategory='BANK')) order by accdesc asc";


            return DataRowToObject.CreateListFromTable<Webx_Master_General>(GF.GetDataTableFromSP(QueryString));
        }

        public string Duplicate_ChqNO(string Chqno, string ChqDate)
        {
            string Cnt = "";
            string Sql = "EXEC WEBX_Chq_Duplicate_Check '" + Chqno + "','" + ChqDate + "'";

            object Obj = GF.executeScalerQuery(Sql);
            if (Obj != null)
                Cnt = Obj.ToString();
            return Cnt;
        }

        public string DR_VR_Trn_Halt(double Netamt, string Trn_date, string FinYear, string Location)
        {
            string Flag = "";

            string FIN_Start = "", Financial_Year = "", fin_year = "", Curr_Year = "", Finyear = "";
            Financial_Year = FinYear.ToString().Substring(2, 2);
            fin_year = FinYear.ToString();
            double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
            fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
            Curr_Year = DateTime.Now.ToString("yyyy");
            Finyear = FinYear.ToString();
            if (Finyear == Curr_Year)
                FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
            else
                FIN_Start = "01 Apr " + Financial_Year;
            string Sql = "exec WEBX_DR_VR_Trn_Halt '" + Location + "','" + Trn_date + "','" + FIN_Start + "','" + fin_year + "','" + Netamt + "'";
            object Obj = GF.executeScalerQuery(Sql);
            if (Obj != null)
                Flag = Obj.ToString();
            return Flag;
        }

        public bool IsValidDocument(string DocumentNo)
        {
            bool Flag = false;
            string Sql = "SELECT Count(*) FROM WEBX_THC_PDC_FIN_DET WHERE DOCNO='" + DocumentNo + "' and TransactionType='Bill Entry'";
            double Cnt = Convert.ToDouble(GF.executeScalerQuery(Sql));
            if (Cnt > 0)
                Flag = false;
            else
                Flag = true;

            return Flag;
        }

        public string Insert_Advance_Balance_BillEntry_Data(string Xml_PAY_Det, string EntryType, string xmlDocGSTCha, string CurrentBranchCode, string FinYear, string CurrentEmployeeID)
        { 
            string Voucherno = "";
            string Sql = "";
            if (EntryType == "ADV")
                Sql = "EXEC [Usp_Doc_Advance_Payment] '" + CurrentBranchCode + "','" + Xml_PAY_Det.ReplaceSpecialCharacters() + "','" + FinYear.ToString() + "','" + CurrentEmployeeID.ToString() + "'";
            else if (EntryType == "BAL")
                Sql = "EXEC [Usp_Doc_Balance_Payment] '" + CurrentBranchCode + "','" + Xml_PAY_Det.ReplaceSpecialCharacters() + "','" + FinYear.ToString() + "','" + CurrentEmployeeID.ToString() + "'";
            else if (EntryType == "BE")
                Sql = "EXEC [Usp_Doc_BillEntry] '" + CurrentBranchCode + "','" + Xml_PAY_Det.ReplaceSpecialCharacters() + "','" + FinYear.ToString() + "','" + CurrentEmployeeID.ToString() + "'";
            else if (EntryType == "BAL_KM")
                Sql = "EXEC [Usp_Doc_Balance_Payment_KM_Based] '" + CurrentBranchCode + "','" + Xml_PAY_Det.ReplaceSpecialCharacters() + "','" + FinYear.ToString() + "','" + CurrentEmployeeID.ToString() + "'";
            else if (EntryType == "BE_KM")
                Sql = "EXEC  [Usp_Doc_BillEntry_KM_Based]  '" + CurrentBranchCode + "','" + Xml_PAY_Det.ReplaceSpecialCharacters() + "','" + FinYear.ToString() + "','" + CurrentEmployeeID.ToString() + "'";

            int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "Insert_Advance_Balance_BillEntry_Data", "", "");

            DataTable DT = GF.GetDataTableFromSP(Sql);

            if (EntryType == "BE")
            {
                var GeneratedVoucher = "";
                GeneratedVoucher = DT.Rows[0]["GeneratedVoucher"].ToString();
                if (GeneratedVoucher == "NA")
                {
                    Voucherno = DT.Rows[0][0].ToString().Split('~')[0].ToString() + "~" + DT.Rows[0]["TranXaction"].ToString().ToUpper() + "~" + EntryType.ToUpper() + "~" + GeneratedVoucher.ToUpper();
                }
                else
                {
                    //Voucherno = GeneratedVoucher + "~" + DT.Rows[0]["TranXaction"].ToString().ToUpper() + "~" + EntryType.ToUpper() + "~" + GeneratedVoucher.ToUpper();
                    Voucherno = DT.Rows[0][0].ToString().Split('~')[0].ToString() + "~" + DT.Rows[0]["TranXaction"].ToString().ToUpper() + "~" + EntryType.ToUpper() + "~" + GeneratedVoucher.ToUpper();

                }
            }
            else
            {
                Voucherno = DT.Rows[0][0].ToString() + "~" + DT.Rows[0]["TranXaction"].ToString().ToUpper() + "~" + EntryType.ToUpper() + "~" + DT.Rows[0][0].ToString();
            }

            return Voucherno;
        }
        //public string DR_VR_Trn_Halt(double Netamt, string Trn_date, string Brcd, string FinYear)
        //{
        //    string Flag = "";

        //    string FIN_Start = "", Financial_Year = "", fin_year = "", Curr_Year = "", Finyear = "";
        //    Financial_Year = FinYear.ToString().Substring(2, 2);
        //    fin_year = FinYear.ToString();
        //    double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
        //    fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
        //    Curr_Year = DateTime.Now.ToString("yyyy");
        //    Finyear = FinYear.ToString();
        //    if (Finyear == Curr_Year)
        //        FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
        //    else
        //        FIN_Start = "01 Apr " + Financial_Year;
        //    string Sql = "exec WEBX_DR_VR_Trn_Halt '" + Brcd + "','" + Trn_date + "','" + FIN_Start + "','" + fin_year + "','" + Netamt + "'";
        //    object Obj = GF.executeScalerQuery(Sql);
        //    if (Obj != null)
        //        Flag = Obj.ToString();
        //    return Flag;
        //}
        #endregion

        #region Fuel Bill Entry

        public List<BABillEntyTripSheet> GetTripsheetBySearch(string SearchTerm)
        {
            string QueryString = "exec USP_AUTO_ManualTSNumber '" + SearchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BABillEntyTripSheet> ItemList = DataRowToObject.CreateListFromTable<BABillEntyTripSheet>(DT);

            return ItemList;
        }

        public List<Webx_Master_General> GetFuelSlipVehicle()
        {
            string QueryString = "exec USP_FLEET_TRIP_FUEL_SLIP_VEHICLENO";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> ItemList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);

            return ItemList;
        }

        public List<VendorDetails> GetVendorTypeLedger(string Vendor)
        {
            string QueryString = "select Acccode,Accdesc,Pan_no,servtaxno,AcctHead  from webx_vendor_hdr A With(NOLOCK) Inner join webx_AcctHead  B With(NOLOCK) on B.Codeid=A.vendor_type   Inner join webx_acctinfo at  WITH(NOLOCK) on b.accthead=at.acccode where Vendorcode='" + Vendor + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VendorDetails> ItemList = DataRowToObject.CreateListFromTable<VendorDetails>(DT);

            return ItemList;
        }

        public List<FuelSlipBillEntyTripSheet> GetFuelSlipTripSheet(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC USP_FLEET_FUELSLIP_BILLENTRY '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.VendorCode + "','" + FBE.VendorService + "','" + FBE.VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<FuelSlipBillEntyTripSheet> ItemList = DataRowToObject.CreateListFromTable<FuelSlipBillEntyTripSheet>(DT);

            return ItemList;
        }

        public List<POBillEntry> GetPOForBillEntry(FuelBillEntyQuery FBE)
        {
            //string QueryString = "EXEC usp_PO_Listing_BillEntryGST_bkp '" + FBE.PoNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "','" + FBE.GSTType + "','" + FBE.isGSTReverse + "'";
            string QueryString = "EXEC usp_PO_Listing_BillEntryGST_Cygnus '" + FBE.PoNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);

            return ItemList;
        }

        public List<webx_acctinfo> GetLedgerBySerach(string SerachTerm)
        {
            string SQRY = " exec USP_GetLedgerList '" + SerachTerm + "','1' ";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<webx_acctinfo> ListGetDSTimeSloteDetail = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return ListGetDSTimeSloteDetail;
        }

        public List<JOBBillEntryDetails> GetJOBForBillEntry(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC usp_JobOrderListForBillGenGST '" + FBE.VehicleNo + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "', '" + FBE.VendorCode + "','" + FBE.StateCode + "','" + FBE.GSTType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<JOBBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<JOBBillEntryDetails>(DT);
            return ItemList;
        }

        public List<GRNBillEntryDetails> GetGRNForBillEntry(FuelBillEntyQuery FBE, string BRCD, string CompanyCode)
        {
            string QueryString = "EXEC USP_GetPO_BillEntryListGST '" + FBE.PoNo + "','" + FBE.ManualPONo + "','" + FBE.VendorCode + "','" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.MatCat + "','" + BRCD.ToString() + "','" + FBE.StateCode + "','" + CompanyCode.ToString() + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<GRNBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<GRNBillEntryDetails>(DT);
            return ItemList;
        }

        public List<GRNBillEntryDetails> GetPOGRNForBillEntryDetails(string PONo)
        {
            string QueryString = "EXEC usp_GRN_Listing_BillEntry_Ver1 '" + PONo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<GRNBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<GRNBillEntryDetails>(DT);
            return ItemList;
        }

        public List<GRNBillEntryDetails> GetPOGRNForBillEntry(string PONo)
        {
            string QueryString = "EXEC usp_GET_POGRN_BillEntry_ListingGST '" + PONo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<GRNBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<GRNBillEntryDetails>(DT);

            return ItemList;
        }

        public List<CygnusChargesHeader> GetGSTDynamicCharges(string DocumentType, string GSTType, decimal Percentage)
        {
            string ChargesStr = "EXEC USP_GetDynamicChargesNew '" + DocumentType + "','" + GSTType + "','" + Percentage + "'";
            return DataRowToObject.CreateListFromTable<CygnusChargesHeader>(GF.GetDataTableFromSP(ChargesStr));
        }

        #endregion

        #region Finalize Bill

        public List<FinalizeBillDetails> FinalizeBillList(FinalizeBillQuery FBQ, string FinYear)
        {
            string QueryString = "EXEC [USP_THC_PDC_FOR_FINALIZATION_NewPortal] 'BILLING', '" + GF.FormateDate(FBQ.FromDate) + "','" + GF.FormateDate(FBQ.ToDate) + "','" + FBQ.VendorCode + "','" + FBQ.DocumentNo + "','" + FinYear + "','0','" + FBQ.Location + "','" + FBQ.LocationLevel + "','1'";
            return DataRowToObject.CreateListFromTable<FinalizeBillDetails>(GF.GetDataTableFromSP(QueryString));
        }

        #endregion

        #region Vendor Bill Entry Payment  -Loading Charge

        public List<JOBBillEntryDetails> GetLoadBillEntry(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC usp_LoadUnloadBill_Listing '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.VendorCode + "',  '" + FBE.BillType + "',  '" + FBE.Branch + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<JOBBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<JOBBillEntryDetails>(DT);
            return ItemList;
        }

        public List<JOBBillEntryDetails> GetMonthlyLoadBillEntry(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC usp_MonthlyLoadUnloadBill_Listing '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.VendorCode + "',  '" + FBE.BillType + "',  '" + FBE.Branch + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<JOBBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<JOBBillEntryDetails>(DT);
            return ItemList;
        }

        public List<webx_acctrans_Model> GetCBSReport()
        {
            string QueryString = "exec USP_Get_CBSDetails";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_acctrans_Model> ItemList = DataRowToObject.CreateListFromTable<webx_acctrans_Model>(DT);
            return ItemList;
        }

        #endregion

        #region Connectivity Charge Bill Entry for Franchies THC

        public List<ConnectivityBillEntryDetails> GetVendorWiseTHCforConnectivityBillEntry(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC USP_GetVendorWiseTHCListforConnectivityBill '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.VendorCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<ConnectivityBillEntryDetails> ItemList = DataRowToObject.CreateListFromTable<ConnectivityBillEntryDetails>(DT);
            return ItemList;
        }

        #endregion

        #region Bill Entry Payment

        public List<BillList> BillEntryList(BillEntryPaymentFilter BEPF, string Fromdate, string Todate, string BillType,string BaseUser)
        {
            string QueryString = "EXEC usp_Bill_Listing '" + BEPF.BillNo + "','" + BEPF.VendorCode + "','" + Fromdate + "','" + Todate + "','" + BEPF.LocationCode + "','" + BEPF.VBillNo + "','" + BillType + "','" + BaseUser + "'";
            return DataRowToObject.CreateListFromTable<BillList>(GF.GetDataTableFromSP(QueryString));
        }
        public List<DNM_DNCancellationList> DebitNoteEntryList(BillEntryPaymentFilter BEPF)
        {
            string QueryString = "EXEC Usp_DebitNote_Listing '" + BEPF.VendorCode + "'";
            return DataRowToObject.CreateListFromTable<DNM_DNCancellationList>(GF.GetDataTableFromSP(QueryString));
        }

        public DataTable GetVendorOutStanding(string VendorCode, string FinYear)
        {
            string QueryString = "EXEC USP_GetVendorOutStanding '" + VendorCode + "','" + FinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        //public DataTable Insert_BillEntry_Payment_Data(string Xml_PAY_MST, string Xml_PAY_Det, string EntryType, string BaseLocationCode, string BaseUserName, string BaseFinYear, string Xml_DebitNote)
        //{
        //    string QueryString = "EXEC Usp_BillEntry_PaymentGST '" + BaseLocationCode + "','" + Xml_PAY_Det + "','" + Xml_PAY_MST + "','" + BaseFinYear + "','" + BaseUserName + "','" + Xml_DebitNote + "'";
        //    int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "Insert_BillEntry_Payment_Data", "", "");
        //    DataTable DT = GF.GetDataTableFromSP(QueryString);
        //    return DT;
        //}

        public DataTable Insert_BillEntry_Payment_Data(string Xml_PAY_MST, string Xml_PAY_Det, string EntryType, string BaseLocationCode, string BaseUserName, string BaseFinYear)
        {
            string QueryString = "EXEC [Usp_BillEntry_Payment_NewPortal]'" + BaseLocationCode + "','" + Xml_PAY_Det + "','" + Xml_PAY_MST + "','" + BaseFinYear + "','" + BaseUserName + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "Insert_BillEntry_Payment_Data", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region Bill Entry

        public DataTable Get_BillNo(string Brcd, string date)
        {
            string QueryString = "usp_next_vendorbillentry_code  '" + Brcd + "','" + date + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_Agent_BillEntry(string userName, string brcd, string XML)
        {
            string QueryString = "exec sp_Insert_BillEntry  '" + userName + "','" + brcd + "','" + XML + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_0(string DocNo)
        {
            string QueryString = "select count(*) as count from webx_oct_det where dockno='" + DocNo + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_1(string BillNo)
        {
            string QueryString = "select isnull(ocbillno,'') as ocbillno  from webx_oct_hdr where ocbillno='" + BillNo + "' and cancel_yn='N'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_2(string OBillNo)
        {
            string QueryString = "select isnull(ocbillno,'') as ocbillno , isnull(voucherno,'') as voucherno  from webx_oct_det where dockno='" + OBillNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_3(string BillNo, string DocNo, decimal OCtAmount, string ReciptNo, string Reciptdt, decimal Hnd_Declval, string DrpAgent, string BillDate)
        {
            string QueryString = "insert into webx_oct_det(ocbillno,dockno,octamt,recptno,recptdt,DOCKSF, DECLVAL,oct_vendorcd,oct_vendornm,vendor_paiddt) values " +
                            "('" + BillNo + "','" + DocNo + "'," + OCtAmount + ",'" + ReciptNo + "','" + Reciptdt + "','.','" + Hnd_Declval + "','" + DrpAgent + "','" + DrpAgent + "','" + BillDate + "')";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_4(string BillNo, string DocNo, decimal OCtAmount, string ReciptNo, string Reciptdt, string DrpAgent, string BillDate)
        {
            string QueryString = "Update webx_OCT_DET set ocbillno='" + BillNo + "' ,OCTAMT=" + OCtAmount + ",RECPTNO='" + ReciptNo + "',RECPTDT='" + Reciptdt + "' ,oct_vendorcd='" + DrpAgent + "',oct_vendornm='" + DrpAgent + "',vendor_paiddt='" + BillDate + "'  where DOCKNO='" + DocNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_5(string DocNo)
        {
            string QueryString = "Update webx_trans_docket_status set OCTROI='Y' where DOCKNO='" + DocNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable Insert_webx_oct_det_6(string BillNo, string FinYear)
        {
            string QueryString = "exec usp_Octroi_Agentbill_Transaction 1,'" + BillNo + "','" + FinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public string checkDocketNo(string DockNo, string Finear, string FirstFinear)
        {
            string Message = "";
            string code2 = DockNo;
            string strValidDocket = "exec USP_Oct_Docket_Validation '" + code2 + "','Vend','',''";
            DataTable dtValidDocket = GF.GetDataTableFromSP(strValidDocket);
            if (dtValidDocket.Rows.Count > 0)
            {
                if (dtValidDocket.Rows[0][0].ToString() == "F")
                {
                    Message = ("InValidDocket|" + dtValidDocket.Rows[0][1].ToString() + "|");
                }
                else
                {
                    string strDocket = "exec USP_Get_Oct_Docket_FinYear '" + code2 + "'";
                    DataTable dtDocket = GF.GetDataTableFromSP(strDocket);
                    if (dtDocket.Rows[0]["Finyear"].ToString() == FirstFinear)
                    {
                        string str = "exec usp_get_AgentBill_GridDetails_NewPortal '" + code2 + "'";
                        DataTable dt = GF.GetDataTableFromSP(str);
                        if (dt.Rows.Count > 0)
                        {
                            Message = "true|" + dt.Rows[0]["OCTAMT"].ToString() + "|" + dt.Rows[0]["RECPTNO"].ToString() + "|" + dt.Rows[0]["RECPTDT"].ToString() + "|" + dt.Rows[0]["DOCKDT"].ToString() + "|" + dt.Rows[0]["STATUS"].ToString() + "|";
                        }
                        else
                        {
                            string StrDate = "select DOCKDT = convert(varchar(50),DOCKDT,103) from WebX_Master_Docket where DOCKNO = '" + code2 + "'";
                            DataTable dtDate = GF.GetDataTableFromSP(StrDate);
                            if (dtDate.Rows.Count > 0)
                            {
                                Message = "false|" + dtDate.Rows[0]["DOCKDT"].ToString() + "|" + "N|";
                            }
                            else
                            {
                                Message = "InValid|";
                            }
                        }
                    }
                    else
                    {
                        Message = "InValidFinYear|";
                    }
                }
            }
            return Message;
        }

        public List<Webx_Master_General> GeTOctroiVendor(string Prefix)
        {
            string QueryString = "exec USP_GETOctroi_Vendor'" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> itmList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return itmList;
        }

        public DataTable InsertOctroiBillPayment(string BaseLocationCode, string Xml_OctBillHeader, string Xml_OctBillDetail, string BaseFinYear, string Businesstype)
        {
            string SQRY = "exec usp_Prepare_Agent_Bill_Entry '" + BaseLocationCode + "','" + Xml_OctBillHeader + "','" + Xml_OctBillDetail + "','" + BaseFinYear + "','" + "N" + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertOctroiBillPayment", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //DataTable Dt = new DataTable();
            return Dt;
        }

        #endregion

        #region Bill Entry Payment View Print

        public List<BillEntryPaymentList> Get_VendorBillPaymentListView(string BillNo, string VendorCode, string FromDate, string ToDate, string BaseLocationCode, string VBillNo, string BillType, string BillTyp, string VoucherNo)
        {
            string QueryString = "exec usp_VendorBillPaymentListView '" + BillNo + "','" + VendorCode + "','" + FromDate + "','" + ToDate + "','" + BaseLocationCode + "','" + VBillNo + "','" + BillType + "','" + BillTyp + "','" + VoucherNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BillEntryPaymentList> ItemList = DataRowToObject.CreateListFromTable<BillEntryPaymentList>(DT);
            return ItemList;
        }

        #endregion

        #region GRN View Print

        public List<GRNViewPrintList> Get_GRNViewList(string Type, string VendorCode, string FromDate, string ToDate, string GRNCode, string ManualGRNCode)
        {
            string QueryString = "";
            if (Type == "0")
            {
                QueryString = "exec USP_GRN_Listing_For_View_Print '" + Type + "','" + VendorCode + "','" + FromDate + "','" + ToDate + "','NA','NA'";
            }
            if (Type == "1")
            {
                QueryString = "exec USP_GRN_Listing_For_View_Print '" + Type + "','NA','NA','NA','" + GRNCode + "','NA'";
            }
            if (Type == "2")
            {
                QueryString = "exec USP_GRN_Listing_For_View_Print '" + Type + "','NA','NA','NA','NA','" + ManualGRNCode + "'";
            }
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<GRNViewPrintList> ItemList = DataRowToObject.CreateListFromTable<GRNViewPrintList>(DT);
            return ItemList;
        }

        #endregion

        #region PO View Print

        public List<webx_GENERAL_POASSET_HDR> Get_POViewList(string Type, string VendorCode, string FromDate, string ToDate, string GRNCode, string BaseLocationCode)
        {
            string QueryString = "";
            if (BaseLocationCode == "HQTR")
            {
                BaseLocationCode = "All";
            }
            if (Type == "0")
            {
                QueryString = "exec USP_GPO_Listing_For_View_Print '" + Type + "','" + VendorCode + "','" + FromDate + "','" + ToDate + "','NA','" + BaseLocationCode + "'";
            }
            if (Type == "1")
            {
                QueryString = "exec USP_GPO_Listing_For_View_Print '" + Type + "','NA','NA','NA','" + GRNCode + "','" + BaseLocationCode + "'";
            }

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_GENERAL_POASSET_HDR> ItemList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(DT);
            return ItemList;
        }

        #endregion

        #region SKU Master

        public DataTable GetSKUItemDetails(string SKUID)
        {
            string SQRY = "select SKU_ID,MatCat_Id,SKU_Desc,Mfg_Desc,SKU_Type,SKU_Size,UOM_ID,SKU_Remark,ActiveFlag,SKU_UDF1,SKU_UDF2,SKU_UDF3,SKU_UDF4,SKU_UDF5 from Webx_PO_SKU_Master where SKU_ID='" + SKUID + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetSKUID()
        {
            string SQRY = "exec Usp_GetSKU_ID_Newportal ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable InsertSKUMASTERADD(string XML, string EntryBy, string SKUID)
        {
            string SQRY = "exec Usp_Insert_SKUMASTERADD_NewPortal '" + XML + "','" + EntryBy + "','" + SKUID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertSKUMASTERADD", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable UpdateSKUMASTERADD(string XML, string EntryBy)
        {
            string SQRY = "exec Usp_Update_SKUMASTERADD_NewPortal '" + XML + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UpdateSKUMASTERADD", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<webx_PO_SKU_Master> GetSKUItemListFromSearch(string Serach)
        {
            string SQR = "exec USP_GetSKUItemListFromChar '" + Serach + "',''";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_PO_SKU_Master> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_PO_SKU_Master>(Dt);
            return GetCustomerVendorList;
        }

        #endregion

        #region POApproval
        public List<webx_GENERAL_POASSET_HDR> GetPOData(string FilterType, string VendorCode, string FirstDate1, string LastDate1, string POCode, string ManualPO, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_PO_Listing_For_Approval_NewPortal '" + FilterType + "','" + VendorCode + "','" + FirstDate1 + "','" + LastDate1 + "','" + POCode + "','" + ManualPO + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);

            return POList;
        }
        public List<webx_GENERAL_POASSET_HDR> GetWOData(string FilterType, string VendorCode, string FirstDate1, string LastDate1, string POCode, string ManualPO, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_WO_Listing_For_Approval_NewPortal '" + FilterType + "','" + VendorCode + "','" + FirstDate1 + "','" + LastDate1 + "','" + POCode + "','" + ManualPO + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);

            return POList;
        }
        public DataTable GetPOAprovalLimit(string UserName)
        {
            string SQRY = "";
            SQRY = "EXEC Usp_CheckPOLimit '" + UserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable ApprovePO(string POcodeList, string UserName, string approveDate, string POorWo)
        {
            string SQRY = "";
            SQRY = "EXEC usp_POApprove_NewPortal '" + POcodeList + "','" + UserName + "','" + approveDate + "','" + POorWo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //DataTable Dt = new DataTable();
            return Dt;
        }
        public List<webx_GENERAL_POASSET_HDR> GETPOForGRNGENERATION(string FromDate, string ToDate, string POCode, string ManualPOCode, string VendCd, string POType, string Approval_YN, string BranchCode, string CompanyCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_GETPO_For_GRN_GENERATION_SRNO '','" + FromDate + "','" + ToDate + "','" + POCode + "','" + ManualPOCode + "','" + VendCd + "','" + POType + "','" + Approval_YN + "','" + BranchCode + "','" + CompanyCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt);
            return POList;
        }
        public List<webx_GENERAL_POASSET_HDR> GETPOForGRNGENERATIONForPH(string FromDate, string ToDate, string POCode, string ManualPOCode, string VendCd, string POType, string Approval_YN, string BranchCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_GETPO_For_Portable_Home_GRN_GENERATION_NewPortal '" + FromDate + "','" + ToDate + "','" + POCode + "','" + ManualPOCode + "','" + VendCd + "','" + POType + "','" + Approval_YN + "','" + BranchCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt);
            return POList;
        }
        public List<webx_GENERAL_POASSET_det> GetPODetData(string pocode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_GENERAL_PO_LIST_FOR_GRN_NewPortal '" + pocode + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_det> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_det>(Dt1);

            return POList;
        }
        public List<webx_GENERAL_POASSET_HDR> GetPODetailFromPocode(string pocode)
        {
            string SQRY = "";
            SQRY = "EXEC GetPODetailFromPocode '" + pocode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);
            return POList;
        }
        public DataTable InsertGRN(string Xml_ReqMST, string Xml_ReqDET, string Xml_StkDET, string LocationCode, string finYear, string type)
        {
            string SQRY = "";
            SQRY = "EXEC Usp_InsertSrNoGRN '" + Xml_ReqMST.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim() + "','" + Xml_ReqDET.Replace("&", "&amp;").Replace("–", "-").Replace("'", " ").Replace("'", "").Trim() + "','" + Xml_StkDET.Replace("&", "&amp;").Replace("–", "-").Replace("'", " ").Replace("'", "").Trim() + "','" + LocationCode + "','" + finYear + "','" + type + "'";
            //  SaveRequestServices(SQRY.Replace("'", "''"), "InsertGRN", "", "");
            DataTable dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }
        public DataTable InsertSTNGRNData(string Xml_ReqMST, string Xml_ReqDET, string Xml_StockDET, string BaseLocationCode, string BaseFinYear, string type)
        {
            string SQRY = "";
            SQRY = "EXEC Usp_InsertSrNoGRN'" + Xml_ReqMST.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim() + "','" + Xml_ReqDET.Replace("&", "&amp;").Replace("–", "-").Replace("'", " ").Replace("'", "").Trim() + "','" + Xml_StockDET.Replace("&", "&amp;").Replace("–", "-").Replace("'", " ").Replace("'", "").Trim() + "','" + BaseLocationCode + "','" + BaseFinYear + "','" + type + "'";
            //  SaveRequestServices(SQRY.Replace("'", "''"), "InsertSTNGRNData", "", "");
            DataTable dt = GF.GetDataTableFromSP(SQRY);

            return dt;
        }
        public DataTable CheckExistanceBox(string SKUID, string BaseCompanyCode, int NO)
        {
            string SQRY = "";
            SQRY = "exec usp_check_SrNo_ONGRN '" + SKUID + "','" + NO + "','" + BaseCompanyCode + "'";
            //SQRY = "Select COUNT(*) FROM WEBX_GRN_DET D WITH(NOLOCK) INNER JOIN WEBX_GRN_HDR H WITH(NOLOCK) ON H.GRNNO=D.GRNNO Where ISNULL(H.Cancelled,'N')='N' AND ICode='" + SKUID + "' AND H.COMPANY_CODE='" + BaseCompanyCode + "' AND '" + NO + "' between D.FromSrNo AND D.ToSrNo";
            DataTable dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public string GetPrefix(string ItemCode)
        {
            string sql = "SELECT isnull(Sku_Prefix,'')+'~'+isnull(SKU_Desc,'') FROM Webx_PO_SKU_Master where SKU_ID= '" + ItemCode + "'";
            DataTable dt = GF.GetDataTableFromSP(sql);
            return dt.Rows[0][0].ToString();
        }

        public string GetVenderCodeFromSTN(string STNNO)
        {
            string SQRY = "", vndrcd = "";
            SQRY = "SELECT Vendorname+'~'+Vendorcode FROM webx_GENERAL_POASSET_HDR WHERE POCode='" + STNNO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            if (Dt.Rows.Count > 0)
            {
                vndrcd = Dt.Rows[0][0].ToString();
            }
            return vndrcd;
        }

        #endregion

        #region   Bill UnSubmission

        public List<BillUnSubmissionModel> Get_BillUnSubmissionList(string BillNo, string VendorName, string BillType, string FromDate, string ToDate, string BaseLocationCode, string VBillNo)
        {
            string sql = "exec webx_BillUnSubmission '" + BillNo + "','" + VendorName + "','" + BillType + "','" + FromDate + "','" + ToDate + "','" + BaseLocationCode + "','" + VBillNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(sql);
            List<BillUnSubmissionModel> MRList_Data = DataRowToObject.CreateListFromTable<BillUnSubmissionModel>(Dt1);
            return MRList_Data;
        }

        public DataTable BillUnSubmissionSubmit(string billno, string BaseUserName, string VoucherDate)
        {
            string Str = "UPDATE DBO.webx_BILLMST WITH(ROWLOCK) SET BSBDT = NULL,BillStatus = 'Bill Generated',";
            Str = Str + "BILLSUBTO = NULL, SUBTOTEL=NULL,Bill_UnSubmit_By='" + BaseUserName + "',Bill_UnSubmit_On='" + VoucherDate + "'";
            Str = Str + " WHERE BillNO='" + billno + "'";

            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }


        #endregion

        #region GRN Bill Entry View Print

        public List<webx_GENERAL_POASSET_HDR> Get_GRNBillEntryList(string FromDate, string ToDate, string GRNCode)
        {
            string QueryString = "exec USP_GRNBillEntry_Listing_For_View_Print '" + FromDate + "','" + ToDate + "','" + GRNCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_GENERAL_POASSET_HDR> ItemList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(DT);
            return ItemList;
        }

        #endregion

        #region MR Approval

        public DataTable GetMRApprovalDT(string FromDate, string ToDate, string MRNo, string FinYear, string BRCD, string CustomeCode)
        {
            string QueryString = "exec [USP_MRApprovalList] '" + FromDate + "','" + ToDate + "','" + MRNo + "','" + FinYear + "','" + BRCD + "','" + CustomeCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable MRApproval(string XML, string BRCD,  string UserName, string FinYear)
        {
            string QueryString = "exec [USP_MRApproval] '" + XML + "','" + BRCD + "','" + UserName + "','" + FinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }


        public DataTable MRRejection(string XML, string BRCD, string UserName, string FinYear)
        {
            string QueryString = "exec [USP_MRRejection] '" + XML + "','" + BRCD + "','" + UserName + "','" + FinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region Crossing Challan Payment

        public List<Webx_Crossing_Docket_Detail> CrossingChallanPaymentList(string DockNo, string FromDate, string ToDate, string Brcd, string Vendor)
        {
            string QueryString = "exec usp_Crossing_for_Payment '" + DockNo + "','" + FromDate + "','" + ToDate + "','" + Brcd + "','" + Vendor + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Crossing_Docket_Detail> itmList = DataRowToObject.CreateListFromTable<Webx_Crossing_Docket_Detail>(DT);
            return itmList;
        }
        public DataTable Insert_CrossingChallan_Payment_Data(string Xml_PAY_MST, string Xml_PAY_Det, string BaseLocationCode, string BaseUserName, string BaseFinYear, string BaseCompanyCode)
        {
            string QueryString = "EXEC USP_CrossingChallan_PAYMENT_Submit'" + Xml_PAY_MST + "','" + Xml_PAY_Det + "','" + BaseLocationCode + "','" + BaseUserName + "','" + BaseFinYear + "','" + BaseCompanyCode + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "Insert_CrossingChallan_Payment_Data", "", "");
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        #endregion

        public List<webx_pdchdr> GetPRSDRSAttachedVendorBillEntry(FuelBillEntyQuery FBE,string Id)
        {
            string QueryString = "EXEC usp_PRSDRS_AttechedContraVendorbill_Listing '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.VendorCode + "','" + FBE.DocType + "','" + FBE.BillType + "','" + FBE.Branch + "','" + Id + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_pdchdr> ItemList = DataRowToObject.CreateListFromTable<webx_pdchdr>(DT);
            return ItemList;
        }
    }
}
