﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Expance
    {
        public string ExpanceTypeName { get; set; }
        public int CountBR { get; set; }
        public decimal SumAmt { get; set; }
        public int CountPendingApp { get; set; }
        public decimal SumPendingApp { get; set; }
        public int ExpanceType { get; set; }
        public int CountPendingPaid { get; set; }
        public decimal SumPendingPaid { get; set; }
        public decimal NetAmt { get; set; }

        public int type { get; set; }
        public string BillNo { get; set; }
        public string Bill_Status { get; set; }
        public string VoucherNo { get; set; }
        public int ID { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
    }
}