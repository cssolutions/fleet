﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Fleet_Triprule
    {
        public int ID { get; set; }
        public string Vehicle_Selection { get; set; }
        public string Driver1_Manual { get; set; }
        public string Driver2_Manual { get; set; }
        public string Trip_Route_Selection { get; set; }
        public string THC_Attached { get; set; }
        public DateTime Entry_Date { get; set; }
        public string Enter_By { get; set; }
        public string Vehicle_Filter { get; set; }
        public string UFL_Filter { get; set; }
        public string Auto_Vslip_Filter { get; set; }
        public string ExternalUsage_Category { get; set; }
        public string Fuel_Bill_Entry_TSWise { get; set; }
        public string VEHICLE_MODE { get; set; }
        public string DOCKET_WITH_TRIPSHEET { get; set; }
        public string LOCAL_CITY { get; set; }
        public string DOCKET_NO_LABEL { get; set; }
        public string TSWithVehicle_Valid { get; set; }
        public string Driver_WO_Vehicle { get; set; }
        public string THC_WO_Tripsheet { get; set; }
        public string Date_Operationally_Equal_Financial { get; set; }
        public string Checklist { get; set; }
        public string Checklist_Mandatory { get; set; }
        public string Driver_Photo_Rule { get; set; }
        public string Driver_License { get; set; }
        public string Settlement_Pending { get; set; }
        public string Tot_Tripsheet { get; set; }
        public string Retrieved_from_last_Trip { get; set; }
        public string Not_To_Be_Changed { get; set; }
        public string Editable_Can_Be_Change { get; set; }
        public string Diff_Bet_CloseKM_StartKM { get; set; }
        public string CloseKM_Greater_Than_StartKM { get; set; }
        public string Trip_Closure_Not_Zero { get; set; }
        public string Trip_Closure_Zero { get; set; }
        public string Fuel_Bill_Entry_Trip_Date { get; set; }
        public string Fuel_Bill_Entry_Trip_Close_Date { get; set; }
        public string Fuel_Bill_Entry_Trip_settlement_Date { get; set; }
        public string Advance_Entry_Settlement_Not_Done { get; set; }
        public string Advance_Entry_Tripsheet_Not_Closed { get; set; }
        public string TripSheet_Name { get; set; }
        public string JobSheet_Name { get; set; }
        public string DriverSettlmt_DateRule_2Days { get; set; }
        public string DriverSettlmt_DateRule_Equals_CloseDate { get; set; }
        public string DriverSettlmt_DateRule_Equals_LastAdvanceDate { get; set; }
        public string LINK_PO_WITH_JOB { get; set; }
        public string TripSheet_Cancel { get; set; }
        public string TripSheet_Row { get; set; }
        public string Driver2_Photo_Rule { get; set; }
        public string Driver2_License { get; set; }
        public string Driver2_Mandetory { get; set; }
        public string Cleaner_Mandetory { get; set; }
        public string Driver2_With_LocationFilter { get; set; }
        public string Diesel_CF { get; set; }
        public string IOCL_YN { get; set; }
        public string Driver1NTFM { get; set; }
        public string Driver2NTFM { get; set; }
        public string CleanerNTFM { get; set; }
        public string FUEL_SLIP_PROVIDED { get; set; }
        public string Trip_Billing { get; set; }
        public string Trip_Billing_wise { get; set; }
        public string TripLogFromMaster { get; set; }
        public string FuelDetailsFromMaster { get; set; }
        public string FUEL_SLIP_ALLOWED_TRIP_GENERATION { get; set; }
        public string FUEL_SLIP_ALLOWED_TILL_CLOSURE { get; set; }
        public string NTSvehicle_Type { get; set; }
        public string NTSvehicle_Type_Show { get; set; }
        public string TripWithOut_Driver2 { get; set; }
        public string TripWithOut_Cleaner { get; set; }
        public string DriverAutoComplete_Criteria { get; set; }
        public string AttachDocketInTripsheetUpdate { get; set; }
        public string ContainernoYN { get; set; }
        public string SNdContainernoYN { get; set; }
        public string DocumentnoYN { get; set; }
        public string Veh_Doc_Trip_Gen { get; set; }
        public string GPS_DB_NAME { get; set; }
    }
}