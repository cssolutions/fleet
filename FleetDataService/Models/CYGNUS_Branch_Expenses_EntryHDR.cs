﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Expenses_EntryHDR
    {
        public int ID { get; set; }
        public int Bill_Status_ID { get; set; }
        public string Bill_Status { get; set; }
        public bool IsChecked { get; set; }
        public string DocumentNo { get; set; }
        public int ExpenceType { get; set; }
        public string BillNo { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string BankName { get; set; }
        public string BankAccNumber { get; set; }
        public string IFSCCode { get; set; }
        public string NameOFPayee { get; set; }
        public string NameOfVendor { get; set; }
        public string Display_Name_Of_Vendor { get; set; }
        public string VendorAccNo { get; set; }
        public int NoOfDocket { get; set; }
        public decimal ActualWeight { get; set; }
        public int NoOfPackage { get; set; }
        public bool IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Brcd { get; set; }
        public DateTime BR_Date { get; set; }
        /* Type :- 6 */
        public decimal Total_Weight { get; set; }
        public int   Total_Number_Of_Pkts { get; set; }
        public decimal Total_Amount { get; set; }
        public decimal Total_Amount_Passed { get; set; }
        public decimal Unit_Cost_Charged_Amount { get; set; }
        public decimal Unit_Cost_Pased_Amount { get; set; }

        /* Type :- 4 */
        public int   NoOfManifest { get; set; }
        public decimal Weight { get; set; }
        public int ReferenceNo { get; set; }
        public string Approved_By { get; set; }

        /* Type :- 2 */
        public string Branch_Name { get; set; }
        public decimal Milometer_Reading { get; set; }
        public string Opening { get; set; }
        public string Destination { get; set; }
        public string Closing { get; set; }
        public string Department { get; set; }
        public string Mobile_Number { get; set; }
        public string HOD_Name { get; set; }
        public string Total_Business { get; set; }
        public string Claim_For_Month { get; set; }
        public string Bank_Name { get; set; }

        /* Type :- 10 */
        public string For_The_Month_Of { get; set; }

        /* Type :- 3 */
        public string Grade { get; set; }
        public DateTime Date { get; set; }
        public string From_Location { get; set; }
        public string To_Location { get; set; }
        public string Period_From { get; set; }
        public string Period_To { get; set; }

        /* Type :- 7/8 */
        public int No_Of_Staff { get; set; }
        public int No_of_Laborers { get; set; }
        public int No_of_Drivers { get; set; }

        /**/
        public bool CheckBox { get; set; }
        public int Approve_Account_Type { get; set; }
        public string Labour { get; set; }
        public string Driver { get; set; }

        /* Type :- 1 Electricity Board */
        public string PaymentMode { get; set; }
        public string Electricity_Board { get; set; }
        public string Electricity_Board_Details { get; set; }
        public string Staff_Details { get; set; }
    }
}