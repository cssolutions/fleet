﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Vendor_Service_Mapping_master
    {
        public string Vendorcode { get; set; }
        public string Vendor_Service_Code { get; set; }

        public string mapmode { get; set; }
    }
}