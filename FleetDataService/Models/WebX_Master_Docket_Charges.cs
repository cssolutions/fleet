//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CYGNUS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class WebX_Master_Docket_Charges
    {
        public string DOCKNO { get; set; }
        public string RATE_TYPE { get; set; }
        public Nullable<decimal> FRT_RATE { get; set; }
        public Nullable<decimal> FREIGHT_CALC { get; set; }
        public Nullable<decimal> FREIGHT { get; set; }
        public Nullable<decimal> FOV { get; set; }
        public Nullable<decimal> SCHG01 { get; set; }
        public Nullable<decimal> SCHG02 { get; set; }
        public Nullable<decimal> SCHG03 { get; set; }
        public Nullable<decimal> SCHG04 { get; set; }
        public Nullable<decimal> SCHG05 { get; set; }
        public Nullable<decimal> SCHG06 { get; set; }
        public Nullable<decimal> SCHG07 { get; set; }
        public Nullable<decimal> SCHG08 { get; set; }
        public Nullable<decimal> SCHG09 { get; set; }
        public Nullable<decimal> SCHG10 { get; set; }
        public Nullable<decimal> SCHG11 { get; set; }
        public Nullable<decimal> SCHG12 { get; set; }
        public Nullable<decimal> SCHG13 { get; set; }
        public Nullable<decimal> SCHG14 { get; set; }
        public Nullable<decimal> SCHG15 { get; set; }
        public Nullable<decimal> SCHG16 { get; set; }
        public Nullable<decimal> SCHG17 { get; set; }
        public Nullable<decimal> SCHG18 { get; set; }
        public Nullable<decimal> SCHG19 { get; set; }
        public Nullable<decimal> SCHG20 { get; set; }
        public Nullable<decimal> SCHG21 { get; set; }
        public Nullable<decimal> SCHG22 { get; set; }
        public Nullable<decimal> SCHG23 { get; set; }
        public Nullable<decimal> UCHG01 { get; set; }
        public Nullable<decimal> UCHG02 { get; set; }
        public Nullable<decimal> UCHG03 { get; set; }
        public Nullable<decimal> UCHG04 { get; set; }
        public Nullable<decimal> UCHG05 { get; set; }
        public Nullable<decimal> UCHG06 { get; set; }
        public Nullable<decimal> UCHG07 { get; set; }
        public Nullable<decimal> UCHG08 { get; set; }
        public Nullable<decimal> UCHG09 { get; set; }
        public Nullable<decimal> UCHG10 { get; set; }
        public Nullable<decimal> SubTotal { get; set; }
        public Nullable<decimal> SVCTAX { get; set; }
        public Nullable<decimal> CESS { get; set; }
        public decimal DKTTOT { get; set; }
        public Nullable<decimal> hedu_cess { get; set; }
        public decimal P01Amount { get; set; }
        public decimal P03Amount { get; set; }
        public decimal SVCTAX_Rate { get; set; }
        public Nullable<decimal> BalanceAmount { get; set; }
        public Nullable<System.DateTime> LASTEDITDATE { get; set; }
        public string Comments { get; set; }
        public string savedby { get; set; }
        public Nullable<System.DateTime> savingdate { get; set; }
        public string EditModule { get; set; }
        public Nullable<decimal> Discount { get; set; }
        public Nullable<bool> SurChgExcludefrom27dec14 { get; set; }
        public Nullable<bool> IsAdditionalAmountAdd { get; set; }
        public Nullable<decimal> AddtionalCharge { get; set; }
        public Nullable<decimal> LessCharge { get; set; }
        public Nullable<decimal> FovRate { get; set; }
        public Nullable<decimal> SurChargeRate { get; set; }
        public string CFRWAYBILLNO { get; set; }
        public Nullable<decimal> CFRWAYBILLAMT { get; set; }
        public Nullable<decimal> CFRTOTALWAYBILLAMT { get; set; }
        public Nullable<decimal> CFRWAYBILLSeriviceTax { get; set; }
        public Nullable<decimal> TollTaxPer { get; set; }
        public Nullable<decimal> TollTaxCharge { get; set; }
        public Nullable<decimal> OtherServiceTaxPer { get; set; }
        public Nullable<decimal> OtherServiceTaxAmt { get; set; }
        public Nullable<decimal> OtherServiceSubTotal { get; set; }
        public Nullable<decimal> TransportationTotalAmt { get; set; }
        public Nullable<decimal> OtherServiceTotalAmt { get; set; }
        public Nullable<decimal> OtherServiceDiscount { get; set; }
        public Nullable<decimal> TransportationDiscount { get; set; }
        public string WriteOffComment { get; set; }
        public Nullable<decimal> ExtraGoodsValue { get; set; }
        public Nullable<decimal> ExtraFovAmt { get; set; }
        public Nullable<decimal> ExtraFovPer { get; set; }
        public Nullable<bool> IsServiceTaxExempted { get; set; }
        public Nullable<bool> IsSurChargesExempted { get; set; }
        public string ManualInvoiceNo { get; set; }
        public Nullable<System.DateTime> ManualInvoiceDate { get; set; }
        public Nullable<bool> IsFromPRQ { get; set; }
        public string Rate { get; set; }
        public string Volume { get; set; }
        public Nullable<System.DateTime> WayBillDate { get; set; }
        public Nullable<bool> IsFromTab { get; set; }
        public Nullable<decimal> FOVRate2 { get; set; }

        public Nullable<decimal> SbcRate { get; set; }
        public Nullable<decimal> SBCess { get; set; }
        public Nullable<decimal> FOVCalculated { get; set; }


        public Nullable<decimal> KKCRate { get; set; }
        public Nullable<decimal> KKCAmount { get; set; } public Nullable<decimal> GreenTax { get; set; }

        public string stax_paidby { get; set; }

         //START GST Changes On Chirag D  //
        public string GSTType { get; set; }
        public bool IsGSTApplied { get; set; }
        public decimal IGSTRate { get; set; }
        public decimal IGSTAmount { get; set; }
        public decimal CGSTRate { get; set; }
        public decimal CGSTAmount { get; set; }
        public decimal SGSTRate { get; set; }
        public decimal SGSTAmount { get; set; }
        public decimal UTGSTRate { get; set; }
        public decimal UTGSTAmount { get; set; }

        // End GST Changes On Chirag D  //
    }
}
