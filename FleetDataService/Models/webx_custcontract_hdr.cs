﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_custcontract_hdr
    {
        public string remarks { get; set; }
        public string Comp_Empname { get; set; }
        public string acc_rep { get; set; }
        public Decimal fovchg { get; set; }
        public DateTime Contract_Effectdate { get; set; }
        public string manual_contractid { get; set; }
        public string Bill_pin { get; set; }
        public string contract_sign_loccode { get; set; }
        public string cseemail { get; set; }
        public string CustDesig { get; set; }
        public string stax_paidby { get; set; }
        public string accperemail { get; set; }
        public string contractmode { get; set; }
        public string paytype { get; set; }
        public string csephone { get; set; }
        public string Entryby { get; set; }
        public string multiple_slab_yn { get; set; }
        public string Custcode { get; set; }
        public string lasteditby { get; set; }
        public string custphone { get; set; }
        public string SKU_YN { get; set; }
        public string activeflag { get; set; }
        public string ContractId { get; set; }
        public string Bill_city { get; set; }
        public string custcat { get; set; }
        public string Comp_Witness { get; set; }
        public string Contract_Type { get; set; }
        public Decimal dktchg { get; set; }
        public string csename { get; set; }
        public string compemail { get; set; }
        public string Cust_represent { get; set; }
        public string Comp_Desig { get; set; }
        public string scan_copy_name { get; set; }
        public Decimal commitedbusiness { get; set; }
        public string compphone { get; set; }
        public string accperphone { get; set; }
        public DateTime Contract_Eddate { get; set; }
        public DateTime Contract_Stdate { get; set; }
        public string contractname { get; set; }
        public string add_contract_loccode { get; set; }
        public string accmanager { get; set; }
        public DateTime lasteditdate { get; set; }
        public string custaccperson { get; set; }
        public string stax_yn { get; set; }
        public string single_slab_yn { get; set; }
        public string custemail { get; set; }
        public string Bill_address { get; set; }
        public string contractcat { get; set; }
        public Decimal Srno { get; set; }
        public string CustWitness { get; set; }
        public string Custname { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        /*Extra Column*/
        public bool stax_yn_chek { get; set; }
        public string slabType { get; set; }
        public string Status { get; set; }

        public string Name { get; set; }
        public string Nstartdate { get; set; }
        public string Nenddate { get; set; }

        //Lavnit
        public string Contract_Based_On { get; set; }

        public string Customer_Code { get; set; }
        public string Contract_Code { get; set; }
        public string Image_Path { get; set; }
        
    }
}