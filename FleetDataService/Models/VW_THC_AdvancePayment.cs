﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class VW_THC_AdvancePayment
    {
        public DateTime Docdt { get; set; }
        public decimal HEDU_CESS { get; set; }
        public string DOCTYPE { get; set; }
        public decimal Advamt { get; set; }
        public string Docno { get; set; }
        public string FTLTYPE { get; set; }
        public decimal tdsded { get; set; }
        public string Doc_Close_mode { get; set; }
        public string Company_Code { get; set; }
        public string VendorCode { get; set; }
        public string Doc_Status { get; set; }
        public string Doc_Cancelled { get; set; }
        public decimal Svctax_cess { get; set; }
        public decimal svrc_rate { get; set; }
        public string cancelled { get; set; }
        public decimal Contract_Amt { get; set; }
        public string Advance_paid_flag { get; set; }
        public string Voucherno { get; set; }
        public decimal CESSAMT { get; set; }
        public string Rut_cat { get; set; }
        public string DocBR { get; set; }
        public string DocDesc { get; set; }
        public string Docdate { get; set; }
        public string VendorName { get; set; }
        public string Docn_Mode { get; set; }
        public string BACODE { get; set; }
        public string Docsf { get; set; }
        public decimal NET { get; set; }
        public string Dockdate { get; set; }
        public string ManualDocno { get; set; }
        public string Dot_Type { get; set; }
        public string VendorBENo { get; set; }
        public decimal TdsRate { get; set; }
        public string Finalized { get; set; }
        public string Bal_Location { get; set; }
        public string vehno { get; set; }
        public string DOC_STR { get; set; }
        public string FinStatus { get; set; }
        public decimal SVCTAX { get; set; }
        public decimal OTHCHRG { get; set; }
        public string RouteCD { get; set; }
        public decimal netbalamt { get; set; }
        public string Adv_Location { get; set; }
        public string FtlTypeDesc { get; set; }
        public string DocumentType { get; set; }
    }
}