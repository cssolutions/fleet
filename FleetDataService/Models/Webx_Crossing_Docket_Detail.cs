﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Crossing_Docket_Detail
    {
        public bool IsChecked { get; set; }
        public string DockNo { get; set; }
        public DateTime DockDate { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public string from_loc { get; set; }
        public string to_Loc { get; set; }
        public string Paybas { get; set; }
        public string paybasCode { get; set; }
        public string Ratetype { get; set; }
        public decimal Pkgs { get; set; }
        public decimal ActuWeight { get; set; }
        public decimal ChargedWeight { get; set; }
        public decimal DKTTOT { get; set; }
        public decimal NetPayable { get; set; }
        public decimal VendorRate { get; set; }
        public decimal CrossingChrg { get; set; }
        public decimal DoorDelyChrg { get; set; }
        public string EntryBY { get; set; }
        public string Bulky { get; set; }
        public DateTime EntryDate { get; set; }
        public string Cancel { get; set; }
        public string CrossingChallan { get; set; }
        public string Commets { get; set; }
        public decimal BulkyChrg { get; set; }
        public string DDRatetype { get; set; }
        public decimal DDVendorRate { get; set; }
        public string Remarks { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public decimal Deductions { get; set; }
        public decimal NetReceivable { get; set; }
    }
}