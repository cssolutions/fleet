﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class webx_rules_docket
    {
        public string code { get; set; }
        public decimal maxvalue { get; set; }
        public decimal minvalue { get; set; }
        public decimal srno { get; set; }
        public string paybas { get; set; }
        public string valuedescription { get; set; }
        public string description { get; set; }
        public string defaultvalue { get; set; }
        public string activeflag { get; set; }
        public string enabled { get; set; }

    }
}