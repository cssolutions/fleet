﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_chq_det
    {
        public string ptmsptcd { get; set; }
        public string bacd { get; set; }
        public string Chq_Direct_Depo { get; set; }
        public string chqsf { get; set; }
        public string recdbrnm { get; set; }
        public string DepoFlag { get; set; }
        public string brcd { get; set; }
        public string banm { get; set; }
        public string empcd { get; set; }
        public string comments { get; set; }
        public string voucherNo { get; set; }
        public double adjustamt { get; set; }
        public string recdbrcd { get; set; }
        public string BANKBRN { get; set; }
        public string OwnCust { get; set; }
        public string chq_clear { get; set; }
        public string chq_b_r { get; set; }
        public string chqno { get; set; }
        public DateTime Entrydt { get; set; }
        public string acccode { get; set; }
        public DateTime chq_bounce_dt { get; set; }
        public decimal chqamt { get; set; }
        public string chq_flag { get; set; }
        public double Chq_BounceAmt { get; set; }
        public string chq_status { get; set; }
        public string Chq_Bank_Reco_O_N { get; set; }
        public string Comment { get; set; }
        public string CHQSTATUS { get; set; }
        public string staffcd { get; set; }
        public DateTime transdate { get; set; }
        public string brnm { get; set; }
        public string chqdt { get; set; }
        public string Onaccount_YN { get; set; }
        public string depoloccode { get; set; }
        public string chq_trf { get; set; }
        public DateTime chqcleardate { get; set; }
        public string empnm { get; set; }
        public string ptmsptnm { get; set; }
        public string banknm { get; set; }
        public int chq_depattempt { get; set; }
        public string staffnm { get; set; }
        public DateTime chq_reoffer_dt { get; set; }
        public DateTime DATEFROM { get; set; }
        public DateTime DATETO { get; set; }


        public bool IsChecked { get; set; }
        public int Srno { get; set; }
        public DateTime VoucherDate { get; set; }
        public string Narration { get; set; }
        public DateTime VoucherDateTotalAmt { get; set; }
        public string TotalAmt { get; set; }
        public string Bankaccode { get; set; }
        public decimal Chqindex { get; set; }
        public string BankCode { get; set; }
        public string Ledger { get; set; }
        public decimal Amount { get; set; }
        public string ChqPrintName { get; set; }
        public string party { get; set; }
        public string Accdesc { get; set; }
    }
}