﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_Customer_News_updates
    {
        public int srno { get; set; }
        public string Headline { get; set; }
        public string News { get; set; }
        public string Updates { get; set; }
        public string ID { get; set; }
        public string Color { get; set; }
    }
}