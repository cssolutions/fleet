﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_Master_Airport
    {
        public int SrNo { get; set; }
        public string ActiveFlag { get; set; }
        public string AirportName { get; set; }
        public string EntryBy { get; set; }
        public string CountryCode { get; set; }
        public DateTime UpdateDate { get; set; }
        public DateTime EntryDate { get; set; }
        public string AirportCode { get; set; }
        public string City { get; set; }
        public string UpdateBy { get; set; }
        public string AlternateCity { get; set; }

    }
}