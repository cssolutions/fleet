﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_TRIPROUTEFuelEXP_VIEW
    {
        public string active_flag { get; set; }
        public int ID { get; set; }
        public string route_code { get; set; }
        public string rutdesc { get; set; }
        public string Vehicle_Type_Desc { get; set; }
        public decimal KMPL { get; set; }
        public decimal Vehicle_Type_Code { get; set; }

        public int Srno { get; set; }
        public decimal RutCharge { get; set; }
        public int RateId { get; set; }
        public string Route_Fuel_Exp_Code { get; set; }
        public string VEHICLENO { get; set; }

        public decimal LTRS { get; set; }
    }
}