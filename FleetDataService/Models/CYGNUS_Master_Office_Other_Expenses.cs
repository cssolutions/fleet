﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Master_Office_Other_Expenses
    {
        public int ID { get; set; }
        public string Type { get; set; }
        public string BRCD { get; set; }
        public decimal Printing_Stationary { get; set; }
        public decimal Data_Entry_Computer { get; set; }
        public decimal RPF_GRP_Police { get; set; }
        public decimal Sales_Promotion { get; set; }
        public decimal Festival { get; set; }
        public decimal Donation { get; set; }
        public decimal Generator_Running_Diesel { get; set; }
        public decimal Any_Purchase { get; set; }
        public string EntryBy { get; set; }
    }
}