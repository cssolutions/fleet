using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class WEBX_FLEET_TYREMODELMST
    {
        public int TYRE_MODEL_ID { get; set; }
        public string MFG_ID { get; set; }
        public string MODEL_NO { get; set; }
        public string MODEL_DESC { get; set; }
        public string TREAD_DEPTH { get; set; }
        public Nullable<int> TYRE_PATTERNID { get; set; }
        public string TYRE_SIZEID { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public Nullable<System.DateTime> ENTRY_DT { get; set; }
        public string COMPANY_CODE { get; set; }

        public string MFG_NAME { get; set; }
        public string TYREPAT_CODE { get; set; }
        public string TYRE_SIZENAME { get; set; }
    }
}
