﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Fleet_Vehicle_SMTask_Mst
    {
        public int ID { get; set; }
        public string Job_Order_No { get; set; }
        public string Vehicle_No { get; set; }
        public int Task_Id { get; set; }
        public DateTime JS_Dt { get; set; }
        public decimal JS_KM { get; set; }
        public DateTime Entry_Dt { get; set; }
        public string Entry_By { get; set; }
    }
}