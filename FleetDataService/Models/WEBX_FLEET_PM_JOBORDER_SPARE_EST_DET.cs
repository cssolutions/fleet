﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET
    {
        public string JOB_ORDER_NO { get; set; }
        public string PART_CODE { get; set; }
        public decimal PART_QTY { get; set; }
        public decimal COST_UNIT { get; set; }
        public decimal COST { get; set; }
        public string S_Remarks { get; set; }
        public int W_GRPCD { get; set; }
        public decimal ACT_PART_QTY { get; set; }
        public decimal ACT_COST_UNIT { get; set; }
        public decimal ACT_COST { get; set; }
        public int TaskTypeID { get; set; }
        public string SKU_GRPCD { get; set; }
        public int SRNO { get; set; }

        public string SMTask { get; set; }
        public string PM_SCH_CODE { get; set; }
        public string ISClose { get; set; }

        public decimal GSTPer { get; set; }

    }
}