﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;   

namespace FleetDataService.Models
{
    public class CygnusCustomerGSTDetails
    {
        public int id { get; set; }
        public string custcode { get; set; }
        public string statecode { get; set; }
        public string statename { get; set; }
        public string gst_registration_no { get; set; }
        public bool isActive { get; set; }
        public string entryby { get; set; }
        public DateTime entrydate { get; set; }
        public string updateby { get; set; }
        public DateTime updatedate { get; set; }

        //bharat
        public string statePrefix { get; set; }

        public string CSGEAddress { get; set; }
        public int iSNO { get; set; }
    }
}