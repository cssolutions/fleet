﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{

    //public class objDate
    //{
    //    public string ControlId { get; set; }
    //    public string ControlValue { get; set; }
    //    public string ModuleCode { get; set; }
    //    public bool isControlRemove { get; set; }
    //}
    public class claims
    {
        public decimal SNO { get; set; }
        public decimal COFAmt { get; set; }
        public decimal LOCEMPCLMAMT { get; set; }
        public string BR_PREPBY { get; set; }
        public DateTime AO_RECDDT { get; set; }
        public string HO_APVREM { get; set; }
        public string BR_PREPNM { get; set; }
        public DateTime CIR_HORECDDT { get; set; }
        public string BR_PREPDT { get; set; }
        public string clmpty { get; set; }
        public string ChallanNo { get; set; }
        public string CUSTSRYRNM { get; set; }
        public decimal CUSTSRYRAMT { get; set; }
        public string AO_APVBY { get; set; }
        public decimal BKGBUS { get; set; }
        public string DMGMKT_VEH { get; set; }
        public decimal DMG_THCAMT { get; set; }
        public string Damg { get; set; }
        public string clmptyadd { get; set; }
        public DateTime COF_CUSTISSDT { get; set; }
        public string DMG_TCBR { get; set; }
        public string NONDELY { get; set; }
        public string natbus { get; set; }
        public string COFBR { get; set; }
        public DateTime DMG_THCDT { get; set; }
        public decimal CLMAMT { get; set; }
        public string DMG_THCNO { get; set; }
        public decimal DELYBUS { get; set; }
        public string ACTDMGLOC { get; set; }
        public string AO_FLAG { get; set; }
        public DateTime COF_BRRECDDT { get; set; }
        public DateTime CIR_BRSENTDT { get; set; }
        public string Short { get; set; }
        public DateTime DelvOnDt { get; set; }
        public string HO_APVBY { get; set; }
        public string CIRBR { get; set; }
        public decimal AmtLoss { get; set; }
        public DateTime AO_APVDT { get; set; }
        public string COFNO { get; set; }
        public string PKGS_COND { get; set; }
        public DateTime BR_RECDDT { get; set; }
        public string DOCKNO { get; set; }
        public string LOCEMPNM { get; set; }
        public string COF_Inspty { get; set; }
        public string INSUCONM { get; set; }
        public string COFREM { get; set; }
        public string DMG_ROUTENM { get; set; }
        public string HO_FLAG { get; set; }
        public DateTime HO_RECDDT { get; set; }
        public decimal mnthbus { get; set; }
        public string LOCSRYRNM { get; set; }
        public string clmptytel { get; set; }
        public string DOCKSF { get; set; }
        public string INSUPL { get; set; }
        public string LOCEMPCD { get; set; }
        public DateTime CIR_ENTRYDT { get; set; }
        public string AO_APVNAME { get; set; }
        public decimal LOCSRYRAMT { get; set; }
        public string DMG_TCNO { get; set; }
        public DateTime COF_AOSENT { get; set; }
        public string clmptynm { get; set; }
        public string Leakg { get; set; }
        public string DMG_VENDBR { get; set; }
        public string BR_FLAG { get; set; }
        public string AO_APVREM { get; set; }
        public string DMG_THCBR { get; set; }
        public DateTime COFDT { get; set; }
        public decimal DECVAL { get; set; }
        public string CLMPTCAT { get; set; }
        public string DMG_VENDNM { get; set; }
        public DateTime CIR_AOSENTDT { get; set; }
        public string DMG_ROUTETY { get; set; }
        public string COF { get; set; }
        public string Other { get; set; }
        public string CIRNo { get; set; }
        public string HO_APVNAME { get; set; }
        public string clmptypin { get; set; }
        public DateTime HO_APVDT { get; set; }
        public string DMG_VEHNO { get; set; }
        public DateTime COF_HOSENT { get; set; }
        public string pilfer { get; set; }
        public decimal InvVal { get; set; }
        //Create new
        public string ENTRYBY { get; set; }
        public string DOCKDT { get; set; }
        public DateTime ctr_date { get; set; }
        public string ORGNCD { get; set; }
        public string REASSIGN_DESTCD { get; set; }

        public string CSGNCD { get; set; }
        public string CSGNNM { get; set; }
        public string CSGNADDR { get; set; }
        public string CSGNCity { get; set; }
        public string CSGNPinCode { get; set; }
        public string CSGNTeleNo { get; set; }


        public string CSGECD { get; set; }
        public string CSGENM { get; set; }
        public string CSGEADDR { get; set; }
        public string CSGECity { get; set; }
        public string CSGEPinCode { get; set; }
        public string CSGETeleNo { get; set; }
        //public string VehicleNo { get; set; }
        public string proddesc { get; set; }
        public string insuyn { get; set; }
        public string paydesc { get; set; }
        public decimal tot_frtamt { get; set; }
        //   public string ClaimAmt { get; set; }

        public string ClaimParty { get; set; }
        public string Temp { get; set; }
        public string IncomingFrom { get; set; }
        public string mf { get; set; }
        public string thc { get; set; }
        public string thcdate { get; set; }
        public string vehno { get; set; }
        public string Temp1 { get; set; }
        public string route { get; set; }
        public string empnm { get; set; }
        public string Claimstr { get; set; }
        public string insuyn1 { get; set; }
        public decimal tot_frtamt1 { get; set; }
        public string CIRNo1 { get; set; }
        public string CIR_PREPBY { get; set; }
        //Insert 
        public string CName { get; set; }
        public string CValue { get; set; }
        public string SName { get; set; }
        public string SValue { get; set; }
        public string SEName { get; set; }
        public string SEValue { get; set; }
        public string IncCoName { get; set; }
        public string IncPNo { get; set; }
        public Int16 Challan { get; set; }
        public string MBusiness { get; set; }
        public string Booking { get; set; }
        public string Delivery { get; set; }
        public string NBusiness { get; set; }
        public string ApprLoc { get; set; }
        public string BRATTPCL { get; set; }
        public string BRATTPCL_FILENAME { get; set; }
        public string BRATTPIC { get; set; }
        public string BRATTPIC_FILENAME { get; set; }
        public string BRATTPOD { get; set; }
        public string BRATTPOD_FILENAME { get; set; }
        public string BRATTTCTHC { get; set; }
        public string BRATTTCTHC_FILENAME { get; set; }
        public string BRATTFIR { get; set; }
        public string BRATTFIR_FILENAME { get; set; }
        public string BRATTSRPT { get; set; }
        public string BRATTSRPT_FILENAME { get; set; }
        public string BRATTPHOTO { get; set; }
        public string BRATTPHOTO_FILENAME { get; set; }
        public string invno { get; set; }
        public decimal invamt { get; set; }
        public string PKGSTY { get; set; }
        public decimal ACTUWT { get; set; }
        public string thcrem { get; set; }
        public string CIRDT { get; set; }
        public DateTime TempDt { get; set; }
        public string ClmPrtyGrd { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }


    }

    public class claims_Details
    {
        public string BRATTSRPT { get; set; }
        public string HORECDPCL { get; set; }
        public string AORECDSRPT { get; set; }
        public string BRATTPOD { get; set; }
        public string HORECDPCL_FILENAME { get; set; }
        public string HORECDTCTHC { get; set; }
        public string AORECDFIR { get; set; }
        public string HORECDPOD { get; set; }
        public string BRATTTCTHC_FILENAME { get; set; }
        public string AORECDSRPT_FILENAME { get; set; }
        public string BRATTPIC { get; set; }
        public string HORECDFIR { get; set; }
        public string HORECDSRPT_FILENAME { get; set; }
        public string BRATTFIR_FILENAME { get; set; }
        public string HORECDPOD_FILENAME { get; set; }
        public string HORECDSRPT { get; set; }
        public string BRATTSRPT_FILENAME { get; set; }
        public string AORECDPHOTO { get; set; }
        public string AORECDFIR_FILENAME { get; set; }
        public string AORECDPIC { get; set; }
        public string AORECDPIC_FILENAME { get; set; }
        public int SRNO { get; set; }
        public string AORECDTCTHC { get; set; }
        public string HORECDPIC { get; set; }
        public string HORECDPHOTO { get; set; }
        public string BRATTPHOTO_FILENAME { get; set; }
        public string HORECDFIR_FILENAME { get; set; }
        public string HORECDPIC_FILENAME { get; set; }
        public string AORECDPCL_FILENAME { get; set; }
        public string BRATTPOD_FILENAME { get; set; }
        public string BRATTPHOTO { get; set; }
        public string BRATTPCL { get; set; }
        public string AORECDPCL { get; set; }
        public string BRATTFIR { get; set; }
        public string BRATTPIC_FILENAME { get; set; }
        public string AORECDPOD_FILENAME { get; set; }
        public string AORECDPHOTO_FILENAME { get; set; }
        public string HORECDPHOTO_FILENAME { get; set; }
        public string HORECDTCTHC_FILENAME { get; set; }
        public string AORECDTCTHC_FILENAME { get; set; }
        public string BRATTPCL_FILENAME { get; set; }
        public string CIRNO { get; set; }
        public string BRATTTCTHC { get; set; }
        public string AORECDPOD { get; set; }
    }

}


