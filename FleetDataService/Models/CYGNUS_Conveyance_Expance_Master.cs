﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Conveyance_Expance_Master
    {
        public int ID { get; set; }
        public string Travel_Mode { get; set; }
        public decimal Rate_Per_KM { get; set; }
        public string Location { get; set; }
    }
}