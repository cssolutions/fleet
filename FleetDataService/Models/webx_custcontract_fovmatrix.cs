﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_custcontract_fovmatrix
    {
        public int Id { get; set; }
        public decimal srno { get; set; }
        public string custcode { get; set; }
        public string risktype { get; set; }
        public string contractid { get; set; }
        public decimal slabfrom { get; set; }
        public decimal slabto { get; set; }
        public decimal fovrate { get; set; }
        public string ratetype { get; set; }   
    }
}