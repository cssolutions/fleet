﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Detention_Expense_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BillNo { get; set; }
        public string Vehicle_Number { get; set; }
        public DateTime Date_From { get; set; }
        public DateTime Date_To { get; set; }
        public TimeSpan Time_From { get; set; }
        public TimeSpan Time_To { get; set; }
        //public string Time_From { get; set; }
        //public string Time_To { get; set; }
        //public string Concerning_Docket_PRS_DRS_THC_No { get; set; }
        public string Party_Location_Name { get; set; }
        public decimal GPS_KM { get; set; }
        public int Number_Of_Days { get; set; }
        public int Rate_Per_Day { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }

        public bool CheckBox { get; set; }


        public string TripSheetNo { get; set; }
        public int Detension_Day { get; set; }
    }
}