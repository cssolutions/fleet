﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Notification_Token
    {
        public int ID { get; set; }
        public string IMEINo { get; set; }
        public string UserId { get; set; }
        public string TokenNo { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
    }

    public partial class CYGNUS_Notification_Token_Response
    {
        public int ID { get; set; }
        public string MESSAGE { get; set; }
        public int STATUS { get; set; }
    }
}