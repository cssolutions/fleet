﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class Cygnus_VendorEmployeeMapping
    {
        public int ID { get; set; }
        public string VendorCode { get; set; }
        public string UserId { get; set; }
        public bool IsActive { get; set; }
        public DateTime EntrtyDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public int Srno { get; set; }
    }
}