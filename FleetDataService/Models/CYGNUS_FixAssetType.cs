﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_FixAssetType
    {
        public bool CheckBox { get; set; }
        public string Employee { get; set; }
        public string DumpType { get; set; }
        public string Branch { get; set; }
        public DateTime EntryDate { get; set; }
        public string DocumentNo { get; set; }
        public string SaleToWhom { get; set; }
        public int Type { get; set; }
        public int ID { get; set; }
        public string Description { get; set; }
        public string ItemCode { get; set; }
        public string RCPLSrNo { get; set; }
        public decimal Amount { get; set; }
        public string EntryBy { get; set; }
        public decimal Qty { get; set; }
        public string ReassignToLoc { get; set; }
        public string DumpToLocation { get; set; }
        public string Brcd { get; set; }
        public string MFGSrNo { get; set; }

        //New Add
        public string ItemName { get; set; }
        public double Rate { get; set; }
        public string AssignLocation { get; set; }
        public string FIXEDASSETCD { get; set; }
        public DateTime AssignDate { get; set; }
        public DateTime DumpToDate { get; set; }
        //public string FIXEDASSETCD { get; set; }
        //public DateTime AssignDate { get; set; }
        //public DateTime DumpToDate { get; set; }
        public DateTime Reassign_Dt { get; set; }
        public DateTime Sales_Dt { get; set; }
        public DateTime fixedassetdt { get; set; }
        public decimal qty { get; set; }
        public decimal rate { get; set; }
        public decimal billamt { get; set; }
        public decimal balanceqty { get; set; }
        public string location { get; set; }
        public string assign { get; set; }

        //add jemin for GRN ASSIGN REASSIGN BILL ENTRY PAYMENT VIEW PRINT
        public string AssignLoc { get; set; }
        public string GRNNO { get; set; }
        public string GRNDT { get; set; }
        public string IteamName { get; set; }
        public string AssignToLoca { get; set; }
        //public string Description { get; set; }
        public int id { get; set; }
        public string Issuedate { get; set; }
        public string DpartName { get; set; }
        public string BILLNO { get; set; }
        public string VendorName { get; set; }
        public string VendorBillDT { get; set; }
        public string VENDORBILLNO { get; set; }
        public string VOUCHERNO { get; set; }
        public string BRCD { get; set; }
        public string PaymentDT { get; set; }
        public string IssueDate { get; set; }

    }
}