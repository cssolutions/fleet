﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Vendor_Contract_DocketBased
    {
        public decimal Min_Charge { get; set; }
        public string TransMode { get; set; }
        public DateTime EntryDt { get; set; }
        public string CONTRACTCD { get; set; }
        public decimal Chg_Rate { get; set; }
        public string EntryBy { get; set; }
        public string VendorCode { get; set; }
        public string PayBas { get; set; }
        public Int64 ID { get; set; }
        public decimal Max_Charge { get; set; }
        public string Location { get; set; }
        public string UpdateBy { get; set; }
        public string City { get; set; }
        public string ChargeType { get; set; }
        public DateTime UpdateDt { get; set; }
        public string Rate_Type { get; set; }
        public int RID { get; set; }
    }
}