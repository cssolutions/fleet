﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Porter_Expense
    {
        public int ID { get; set; }
        public bool Marker { get; set; }
        public string Rate_Charged { get; set; }
        public string Porter_Type { get; set; }
        public string BRCD { get; set; }
        public bool Handling { get; set; }
        public bool Other { get; set; }
        public string NameOfStation { get; set; }
        public string Location { get; set; }

        //public DateTime EntryDate { get; set; }
        //public DateTime UpdateDate { get; set; }
        //public string UpdateBy { get; set; }
        //public string EntryBy { get; set; }
    }
}