﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class VW_TyreDetails_New
    {
        public string TYRE_SIZEID { get; set; }
        public decimal TYRE_COST { get; set; }
        public string TYRE_NO { get; set; }
        public string TYRE_ID { get; set; }
        public decimal MANUAL_MOUNT_KM_RUN { get; set; }
        public string MOUNT_DT { get; set; }
        public string TYRE_TYPEID { get; set; }
        public string SIZE { get; set; }
        public string GRNNO { get; set; }
        public string MFG_ID { get; set; }
        public string MODEL { get; set; }
        public string TYRE_STATUS { get; set; }
        public string TYRE_PATTERN_CODE { get; set; }
        public string TYRE_TYPE { get; set; }
        public string MFG { get; set; }
        public string GRNDT { get; set; }
        public string TYRE_VEHNO { get; set; }
        public string TYREID_N { get; set; }
        public string TYRE_LOCATION { get; set; }
        public string PATTERN { get; set; }
        public int TYRE_MODEL_ID { get; set; }

        public decimal Fitment_KM { get; set; }
        public decimal Removal_KM { get; set; }
        public decimal Distance { get; set; }
        public decimal Per_KM_Cost { get; set; }
        public bool active { get; set; }

        //jemin
        public decimal DistanceCovered { get; set; }
        public decimal PerKMCost { get; set; }
        public string CheckVhNo { get; set; }
        public DateTime ChkDate { get; set; }
        public bool IsChecked { get; set; }
        public string Axel { get; set; }
        public int ActualTyre { get; set; }
        public int PendingTyre { get; set; }
        public decimal RemovalKM { get; set; }

        public decimal LifeOfTyre { get; set; }

        public DateTime? TYRE_REMOVE_DT { get; set; }

        public DateTime RemoveDT { get; set; }

        public string IssueDate { get; set; }
    }
}