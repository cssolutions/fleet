﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_VENDORBILL_HDR
    {
        public string ENTRYBY { get; set; }
        public double TDS { get; set; }
        public double OTHERCHRG { get; set; }
        public string BRCD { get; set; }
        public double OTHERDED { get; set; }
        public string TRN_MODE { get; set; }
        public int TotalKM { get; set; }
        public string DISCOUNT_TYPE { get; set; }
        public string cancel_by { get; set; }
        public string REMARK { get; set; }
        public string REFNO { get; set; }
        public string tdsgrpcode { get; set; }
        public double SVCTAXDED { get; set; }
        public DateTime Finalized_Date { get; set; }
        public string BILLNO { get; set; }
        public string company_code { get; set; }
        public int SBCRATE { get; set; }
        public string PANNO { get; set; }
        public int OTHAMT { get; set; }
        public int VATRate { get; set; }
        public string ContractType { get; set; }
        public string BILLSF { get; set; }
        public DateTime VENDORBILLDT { get; set; }
        public DateTime PAYDT { get; set; }
        public string tdsfor { get; set; }
        public DateTime DUEDT { get; set; }
        public string STaxRegNo { get; set; }
        public string acccode { get; set; }
        public double servicetaxrate { get; set; }
        public DateTime finclosedt { get; set; }
        public int hdu_cessrate { get; set; }
        public string tdsgrpdesc { get; set; }
        public string Finalized_By { get; set; }
        public int SBCESS { get; set; }
        public double pendamt { get; set; }
        public DateTime ENTRYDT { get; set; }
        public int Hedu_cess { get; set; }
        public string VENDORCODE { get; set; }
        public int PER_KM_Rate { get; set; }
        public double discount { get; set; }
        public int cessamt { get; set; }
        public DateTime cancel_dt { get; set; }
        public int TDSRATE { get; set; }
        public double othertaxrate { get; set; }
        public string cancel_reason { get; set; }
        public DateTime BILLDT { get; set; }
        public double DEDUCTION_CHRG { get; set; }
        public double othertax { get; set; }
        public string accdesc { get; set; }
        public string VOUCHERNO { get; set; }
        public string tdsaccdesc { get; set; }
        public string VENDORBILLNO { get; set; }
        public int DIS_PERC_VAL { get; set; }
        public int OtherCharge { get; set; }
        public int SVCTAX { get; set; }
        public int ADVAMT { get; set; }
        public string tdsacccode { get; set; }
        public string finalized { get; set; }
        public int PCAMT { get; set; }
        public string betype { get; set; }
        public double NETAMT { get; set; }
        public string bill_cancel { get; set; }
        public string VENDORNAME { get; set; }
        public string VENDORTYPE { get; set; }
        public int VATAmt { get; set; }

        public int DueDays { get; set; }


        public decimal ContactCommission1 { get; set; }
        public decimal ContactCommission2 { get; set; }

        public bool IsStaxEnabled { get; set; }
        public bool IsTDSEnabled { get; set; }

        public decimal CessRate { get; set; }
        public decimal SBRate { get; set; }
        public decimal StaxOnAmount { get; set; }
        public decimal TDSOnAmount { get; set; }

        //add lavnit
        public string PoNo { get; set; }
        public decimal Totalamt { get; set; }
        public string NVENDORNAME { get; set; }
        public string FROMDATE { get; set; }
        public string TODATE { get; set; }
        public int ID { get; set; }

        public string BillGENMonth { get; set; }

        /*Regarding GST Changes*/
        public bool IsGSTApplied { get; set; }
        public string StateCode { get; set; }
        public string GSTType { get; set; }
        public decimal CGSTRate { get; set; }
        public decimal CGSTAmount { get; set; }
        public decimal SGSTRate { get; set; }
        public decimal SGSTAmount { get; set; }
        public decimal UTGSTRate { get; set; }
        public decimal UTGSTAmount { get; set; }
        public decimal IGSTRate { get; set; }
        public decimal IGSTAmount { get; set; }
        public decimal GSTPercentage { get; set; }
        public bool isGSTReverse { get; set; }

        //Start New Part GST Change Chirag D
        public decimal TotPartGST { get; set; }
        public decimal PIGSTAmount { get; set; }
        public decimal PCGSTAmount { get; set; }
        public decimal PSGSTAmount { get; set; }
        public decimal PUTGSTAmount { get; set; }

        //End New Part GST Change Chirag D

        public decimal OtherDedudction { get; set; }
        public decimal DiscRecvd { get; set; }

        public string Image { get; set; }
        public string BillPayType { get; set; }
    }
}