﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_TYREMST_HISTORY
    {
        public string TYRE_REMOVE_DT { get; set; }
        public string TYRE_ENTRYBY { get; set; }
        public string NEW_VEHNO { get; set; }
        public int VEH_INTERNAL_NO { get; set; }
        public string TYRE_REMOVE_BY { get; set; }
        public string TYRE_LOCATION { get; set; }
        public string PendingExcess { get; set; }
        public string TYRE_VEHNO { get; set; }
        public string TYRE_ID { get; set; }
        public DateTime TYRE_ENTRYDT { get; set; }
        public string TYRE_STATUS { get; set; }
        public string INTERCHANGE_DT { get; set; }
        public int MANUAL_DISMOUNT_KM_RUN { get; set; }
        public string TYRE_ACTIVEFLAG { get; set; }
        public decimal MANUAL_MOUNT_KM_RUN { get; set; }
        public DateTime ExceptionDT { get; set; }
        public decimal CostPerKM { get; set; }
        public string GRNNO { get; set; }
        public decimal Dist_Covered { get; set; }
        public DateTime TYRE_ISSUE_DT { get; set; }
        public string TYRE_NO { get; set; }
        public string TYRE_ISSUE_BY { get; set; }
        public int SrNo { get; set; }
        public string MODEL { get; set; }
        public string SIZE { get; set; }
        public string PATTERN { get; set; }
        public string TYRE_TYPE { get; set; }
        public string Remark { get; set; }
        public string MFG { get; set; }
        public bool IsChecked { get; set; }
        public string Text { get; set; }
        public DateTime ClaimDT { get; set; }
        public string Fittment_DT { get; set; }
        public string VENDORCODE { get; set; }
        public string ACTIONS { get; set; }
        public decimal CLAIM_AMT { get; set; }
        public string Suffix { get; set; }
        public decimal SALE_AMT { get; set; }
        public string MobileNo { get; set; }
        public string Vendor { get; set; }


        public decimal LifeofTyreRemaining { get; set; }


        
        
        

        
    }
}