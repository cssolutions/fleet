﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class ContractTemplateRates
    {
        public string fromloc { get; set; }
        public string toloc { get; set; }
        public string ratetype { get; set; }
        public string loc_reg { get; set; }
        public decimal Air_rate { get; set; }
        public decimal Air_trdays { get; set; }
        public decimal Express_rate { get; set; }
        public decimal Express_trdays { get; set; }
        public decimal Train_rate { get; set; }
        public decimal Train_trdays { get; set; }
    }
}