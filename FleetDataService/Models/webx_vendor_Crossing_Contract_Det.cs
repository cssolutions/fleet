﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_vendor_Crossing_Contract_Det
    {
        public int Id { get; set; }
        public string ContractID { get; set; }
        public string VendorCode { get; set; }
        public string Origin { get; set; }
        public string DestCity { get; set; }
        public string ratetype { get; set; }
        public Decimal rate { get; set; }
        public Decimal doordeliveryCharge { get; set; }
        public string DDRatetype { get; set; }

        public Decimal otherCharge { get; set; }
        public string ActiveFlag { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdatedBy { get; set; }

        public DateTime UpdatedDate { get; set; }
        public string EntryBy { get; set; }
        public bool Check { get; set; }
        public bool IsUpdate { get; set; }
        public bool Deleted { get; set; }
        public List<webx_vendor_Crossing_Contract_Det> ListWVCCD { get; set; }
        public decimal SRNo { get; set; }
    }
}