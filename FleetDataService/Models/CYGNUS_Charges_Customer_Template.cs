﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class CYGNUS_Charges_Customer_Template
    {
        public int ContractID { get; set; }
        public string CustCD { get; set; }
        public string chargecode { get; set; }
        public string chargename { get; set; }
        public decimal Air { get; set; }
        public decimal Train { get; set; }
        public decimal Express { get; set; }
        public Int64 RNo { get; set; }

        public string EntryBy  { get; set; }
        public DateTime EntryDt  { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDt { get; set; }


    }
}