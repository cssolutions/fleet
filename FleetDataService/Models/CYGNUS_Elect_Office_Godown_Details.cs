﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Elect_Office_Godown_Details
    {
        public string Address { get; set; }
        public int ID { get; set; }
        public decimal Total_Area_in_sq_ft { get; set; }
        public int Number_of_Rooms { get; set; }
        public decimal RateAsPer_Agreement { get; set; }
        public int Others { get; set; }
        public int Computers { get; set; }
        public int Bulbs { get; set; }
        public int CFLs { get; set; }
        public int TubeLights { get; set; }
        public int Printers { get; set; }
        public string Text { get; set; }
        public int Value { get; set; }

        public DateTime EntyDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string BRCD { get; set; }
        public string EntryBy { get; set; }

        public decimal PreviousReading { get; set; }
        public decimal Previous_Claimed_Amount { get; set; }

        public string Name_Of_Land_Lord { get; set; }
        public string Bank_Account_No { get; set; }
        public string IFSC_Code { get; set; }

        public decimal Reading { get; set; }
    }
}