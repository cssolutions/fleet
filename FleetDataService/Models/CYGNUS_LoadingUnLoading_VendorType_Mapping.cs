﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_LoadingUnLoading_VendorType_Mapping
    {
        public int Srno { get; set; }
        public string Location { get; set; }
        public string DocumentType { get; set; }
        public string Loading_VendorType { get; set; }
        public string UnLoading_VendorType { get; set; }
        public bool Active { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }

        public string CodeId { get; set; }
        public string CodeDesc { get; set; }
    }
}