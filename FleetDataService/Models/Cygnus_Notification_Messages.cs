﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Cygnus_Notification_Messages
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Message { get; set; }
        public int LeaveId { get; set; }
        public bool IsRead { get; set; }
        public string Entryby { get; set; }
        public DateTime Entrydate { get; set; }
    }
}