﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Office_Expenses_Other_HDR
    {
        public int ID { get; set; }
        public int ExpenceType { get; set; }
        public string Branch_Name { get; set; }
        public string Type_Of_Staff { get; set; }
        public string EmpName { get; set; }
        public string BillNo { get; set; }
        public int No_Of_Staff { get; set; }
        public string EmpCode { get; set; }
        public int No_of_Labore { get; set; }
        public string BankName { get; set; }
        public int No_of_Drivers { get; set; }
        public string BankAccNumber { get; set; }
        public string NameOFPayee { get; set; }
        public string IFSCCode { get; set; }
        public string BRCD { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        //public DateTime BR_Date { get; set; }
        public decimal MaxLimit { get; set; }
    }
}