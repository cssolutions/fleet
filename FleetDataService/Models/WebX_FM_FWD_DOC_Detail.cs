﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WebX_FM_FWD_DOC_Detail
    {
        public string Manual_Bill_No { get; set; }
        public string Orgn_Dest { get; set; }
        public DateTime Bill_Date { get; set; }
        public DateTime DocumentDate { get; set; }
        public string FM_Ack_Status { get; set; }
        public string doc_status { get; set; }
        public string DocumentNo { get; set; }
        public DateTime Dely_Date { get; set; }
        public decimal Bill_Amount { get; set; }
        public string Submission_Location { get; set; }
        public decimal ID { get; set; }
        public string From_To { get; set; }
        public string Scan_Status_New { get; set; }
        public string Billing_Party { get; set; }
        public string DockNo { get; set; }
        public string CurrLoc { get; set; }
        public string Bill_no { get; set; }
        public decimal Amount { get; set; }
        public DateTime Updt { get; set; }
        public string RE_FWD { get; set; }
        public decimal HDR_ID { get; set; }
        public DateTime DockDt { get; set; }
        public int Scan_Status { get; set; }
    }
}