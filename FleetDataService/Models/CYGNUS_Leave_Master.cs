﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Leave_Master
    {
        public int ID { get; set; }
        public string UserID { get; set; }
        public string FinancialYear { get; set; }
        public string LeaveReason { get; set; }
        public string LeaveType { get; set; }
        public decimal Days { get; set; }
        public DateTime LeaveStartDate { get; set; }
        public DateTime LeaveEndDate { get; set; }
        public string EntryBy { get; set; }
        public string LocationCode { get; set; }
        public string status { get; set; }
        public DateTime EntryDate { get; set; }
        public string Approv_Reject_By { get; set; }
        public DateTime Approv_Reject_Date { get; set; }
        public string BranchCode { get; set; }
    }
}