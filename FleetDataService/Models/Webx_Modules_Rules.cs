﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Modules_Rules
    {
        public string Module_Title_desc { get; set; }
        public string RULEID { get; set; }
        public decimal Srno { get; set; }
        public string Module_Name { get; set; }
        public string RULE_Y_N { get; set; }
        public string ModuleID { get; set; }
        public string RULE_VALUE { get; set; }
        public string RULE_DESC { get; set; }

    }
}