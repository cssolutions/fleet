﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_TripSheetClose
    {
        public string VSlipNo { get; set; }
        public string Manual_TripSheetNo { get; set; }
        public string mVSlipDt { get; set; }
        public string VSlipDt { get; set; }
        public string Financial_date22 { get; set; }
        public string Financial_date { get; set; }
        public string Hour_Time { get; set; }
        public string Minute_Time { get; set; }
        public string AM_PM_Time { get; set; }
        public string OperCloseDtTime { get; set; }
        public string vehno { get; set; }
        public decimal Start_km { get; set; }
        public decimal f_closure_closekm { get; set; }
        public decimal f_issue_fill { get; set; }
        public decimal f_closure_fill { get; set; }
        public decimal Approved_KMPL { get; set; }
        public decimal Actual_KMPL { get; set; }
        public decimal current_KM_Read_AfterTHCCLose { get; set; }
        public decimal current_KM_Read { get; set; }
        public string THC_Attached_YN { get; set; }
        public string driver_name { get; set; }
        public string License_No { get; set; }
        public string Valdity_dt { get; set; }
        public string PreparedBy { get; set; }
        public string CheckedBy { get; set; }
        public string ApprovedBy { get; set; }
        public string AuditedBy { get; set; }
        public string Driver1 { get; set; }
        public string Custcode { get; set; }
        public string Category { get; set; }
        public string Market_Own { get; set; }
        public string TripSheet_StartLoc { get; set; }
        public string TripSheet_EndLoc { get; set; }
        public string rut_code { get; set; }
        public string rutdesc { get; set; }
        public string VehicleType { get; set; }
        public decimal KMPL { get; set; }
        public decimal AvgDieselRate { get; set; }
        public string From_City { get; set; }
        public string To_City { get; set; }
        public string Oper_Close_Dt { get; set; }
        public string TS_Close_Dt { get; set; }
        public string Externalusage_Category { get; set; }
        public string Vehicle_Mode { get; set; }
        public string DOCKNO { get; set; }
        public decimal DIESEL_AMT_CF { get; set; }
        public decimal DIESEL_QTY_CF { get; set; }



    }
}