﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_ServiceList_Mapping
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string UserCode { get; set; }
    }
}