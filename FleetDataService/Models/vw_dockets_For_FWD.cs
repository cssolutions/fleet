﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_dockets_For_FWD
    {
        public string dockno { get; set; }
        public string doc_fwd_to { get; set; }
        public string fm_ack_status { get; set; }
        public byte fm_doc_type { get; set; }
        public string orgncd { get; set; }
        public string PFM_LOC { get; set; }
        public string dely_date { get; set; }
        public string loc_cust_code { get; set; }
        public string View_st { get; set; }
        public string curr_loc { get; set; }
        public string Scaned { get; set; }
        public string DocumentName { get; set; }
        public string Doc_ack_status { get; set; }
        public string loc { get; set; }
        public DateTime dockdt { get; set; }
        public string from_to { get; set; }
        public decimal dkttot { get; set; }
        public string delivered { get; set; }
        public string dkt { get; set; }
        public string paybas { get; set; }
        public string Fur_FWD_loc { get; set; }

        public bool isChecked { get; set; }
    }
}