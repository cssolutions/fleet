﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class VW_DRIVER_FINAL_INFO
    {
        public string Driver_Status { get; set; }
        public string P_Address { get; set; }
        public string Rationcard_File { get; set; }
        public string C_City_Pincode { get; set; }
        public string Guarantor_Name { get; set; }
        public string Thumb_Impression_YN { get; set; }
        public string Electricity_Bill_File { get; set; }
        public string Thumb_Impression_File { get; set; }
        public string TelNo { get; set; }
        public string Telephone_Bill_File { get; set; }
        public string P_City_Pincode { get; set; }
        public string Manual_Driver_Code { get; set; }
        public string Rationcard_YN { get; set; }
        public string DFather_Name { get; set; }
        public string Vehno { get; set; }
        public decimal Driver_Id { get; set; }
        public string C_Address { get; set; }
        public string Passport_YN { get; set; }
        public string VoterId_File { get; set; }
        public string MobileNo { get; set; }
        public string Driving_Lic_YN { get; set; }
        public string License_No { get; set; }
        public string BankAcc_File { get; set; }
        public string VoterId_YN { get; set; }
        public string ID_Passport_YN { get; set; }
        public string ID_Passport_File { get; set; }
        public string Valdity_dt { get; set; }
        public string PAN_File { get; set; }
        public DateTime EntryDt { get; set; }
        public string ActiveFlag { get; set; }
        public string Driver_Registration_Form_YN { get; set; }
        public string Pan_YN { get; set; }
        public string Driving_Lic_File { get; set; }
        public string BankAcc_YN { get; set; }
        public string Driver1_op_bal { get; set; }
        public string Driver_Name { get; set; }
        public string Driver_Location { get; set; }
        public string Issue_By_RTO { get; set; }
        public string Passport_File { get; set; }
        public string Driver_Registration_Form_File { get; set; }
        public string Telephone_Bill_YN { get; set; }
        public string Driver_Photo { get; set; }
        public string Electricity_Bill_YN { get; set; }
        public string Currentbls { get; set; }

    }
}