﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Master_Office_Fixed_Expenses
    {
        public int ID { get; set; }
        //public string Type { get; set; }
        public string BRCD { get; set; }
        public decimal TEA { get; set; }
        public decimal Water { get; set; }
        public decimal Pooja { get; set; }
        public decimal Sweeper { get; set; }
        public decimal NewsPaper { get; set; }
        public decimal Watchmen { get; set; }
        //public string EntryBy { get; set; }
        //public string UpdateBy { get; set; }
    }
}