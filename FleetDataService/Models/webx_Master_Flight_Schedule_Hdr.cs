﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_Master_Flight_Schedule_Hdr
    {
        public DateTime EntryDate { get; set; }
        public string AirlineCode { get; set; }
        public string ActiveFlag { get; set; }
        public int SrNo { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public string FlightCode { get; set; }
        public string FlightNumber { get; set; }

    }
}