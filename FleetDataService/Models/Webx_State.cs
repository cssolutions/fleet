using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
	[XmlRoot("DocumentElement"), XmlType("DocumentElement")]
	public partial class Webx_State
	{
        //public string activeflag {get;set;}
        //public DateTime entrydt {get;set;}
        //public decimal frt_rate {get;set;}
        //public decimal srno {get;set;}
        //public string lasteditby {get;set;}
        //public string stax_exmpt_yn {get;set;}
        //public string rate_type {get;set;}
        //public string stnm {get;set;}
        //public string StateCode {get;set;}
        //public string Category {get;set;}
        //public DateTime lasteditdate {get;set;}
        //public string entryby {get;set;}
        //public string stcd {get;set;}
        public string stcd { get; set; }
        public string stnm { get; set; }
        public Nullable<decimal> frt_rate { get; set; }
        public string rate_type { get; set; }
        public string entryby { get; set; }
        public Nullable<System.DateTime> entrydt { get; set; }
        public string lasteditby { get; set; }
        public Nullable<System.DateTime> lasteditdate { get; set; }
        public string activeflag { get; set; }
        public string stax_exmpt_yn { get; set; }
        public decimal srno { get; set; }
        public string Category { get; set; }
        public string StateCode { get; set; }

        public bool activeflag2 { get; set; }
        public bool stax_exmpt_yn2 { get; set; }

        public bool IsUnionTerritory { get; set; }
        public string GSTRegNo { get; set; }
        public string gstStateCode { get; set; }
        public string statePrefix { get; set; }

        public string AccountNo { get; set; }
        public string IFSCCode { get; set; }
        public string BankName { get; set; }
		}
	}

