﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_Master_Flight_Schedule_Det
    {
        public DateTime Arr_Time { get; set; }
        public string ToAirportCode { get; set; }
        public int SrNo { get; set; }
        public DateTime Dep_Time { get; set; }
        public int Stoppage_Min { get; set; }
        public string FromAirportCode { get; set; }
        public string AirportCode { get; set; }
        public string FlightCode { get; set; }
        public int Days { get; set; }

    }
}