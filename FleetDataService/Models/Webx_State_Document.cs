using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
	public class Webx_State_Document
	{
		public string STFORM {get;set;}
		public int srno {get;set;}
		public string STCD {get;set;}
		public string STCSTREQ {get;set;}
		public string inoutbound {get;set;}
		public string STREM {get;set;}
		public string STPERMIT {get;set;}
		}
	}

