﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Office_Expenses_Fixed_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BillNo { get; set; }
        public string ExpenseType { get; set; }
        public string LegderCode { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        //public string MFrom { get; set; }
        //public string MTo { get; set; }
        public int For_the_Period { get; set; }
        public int Amount { get; set; }
        public string Particular { get; set; }

        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }

        public decimal MaxLimit { get; set; }

        
        public string Brcd { get; set; }

        public bool CheckBox { get; set; }

        public string OfficeExpenseFixedImage { get; set; }

    }
}