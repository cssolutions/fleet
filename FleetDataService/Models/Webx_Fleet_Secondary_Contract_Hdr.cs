﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Fleet_Secondary_Contract_Hdr
    {
        public DateTime Contract_To_Dt { get; set; }
        public string Customer_Code { get; set; }
        public string Entry_By { get; set; }
        public DateTime Contract_From_Dt { get; set; }
        public string Rate_Based_On { get; set; }
        public string AC { get; set; }
        public string Empty { get; set; }
        public string Active_Flag { get; set; }
        public string Non_AC { get; set; }
        public string Fixed_Cost_Rate { get; set; }
        public string Variable_Cost_Rate { get; set; }
        public DateTime Update_Dt { get; set; }
        public string Contract_Based_On { get; set; }
        public string Variable_Cost_Rate_Based_On { get; set; }
        public string Fixed_Cost_Rate_Based_On { get; set; }
        public string Rate_Type { get; set; }
        public string Contract_Code { get; set; }
        public string Standard_Charge_Applicable { get; set; }
        public DateTime Entry_Dt { get; set; }
        public string Update_By { get; set; }
        public bool AC1 { get; set; }
        public bool Empty1 { get; set; }
        public bool Non_AC1 { get; set; }
        public bool FREEZER1 { get; set; }
        public bool Standard_Charge_Applicable1 { get; set; }
        public bool Fixed_Cost_Rate_Based_On1 { get; set; }

        public decimal HourlyBasedSlot { get; set; }
        public decimal Extra_Charge_Per_Hour { get; set; }
        public int Id { get; set; }

        //LAVNIT
        public string Vehicle_No { get; set; }
        public int Vehicle_Type_Code { get; set; }
        public string Category { get; set; }
        public int HourlyBasedSlot_Id { get; set; }
        public decimal From_KM { get; set; }
        public decimal To_KM { get; set; }
        public decimal Fixed_Cost { get; set; }
        public decimal Variable_Cost { get; set; }
        public string Fixed_Cost_KM { get; set; }
        public string LocCode { get; set; }

        public string Contract_Det_Code { get; set; }
    }
}