using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class WEBX_FLEET_TYREPATTERN
    {
        public int TYRE_PATTERNID { get; set; }
        public string TYRE_PATTERN_DESC { get; set; }
        public string POS_ALLOWED { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public Nullable<System.DateTime> ENTRYDT { get; set; }
        public string TYREPAT_CODE { get; set; }
        public string COMPANY_CODE { get; set; }


        public string TyrePosition { get; set; }
    }
}
