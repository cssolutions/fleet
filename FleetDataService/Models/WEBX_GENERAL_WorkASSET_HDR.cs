﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    /// <summary>
    /// Generated Class for Table : webx_GENERAL_WorkASSET_HDR.
    /// </summary>
    public class webx_GENERAL_WorkASSET_HDR
    {
        public string Payment_terms { get; set; }
        public string PayMode { get; set; }
        public DateTime qutdate { get; set; }
        public decimal totalqty { get; set; }
        public string tax_type { get; set; }
        public string POSTATUS { get; set; }
        public string Subject { get; set; }
        public string CloseReason { get; set; }
        public string reqno { get; set; }
        public string Company_Code { get; set; }
        public string Billno { get; set; }
        public string pocode { get; set; }
        public string Adv_Voucherno { get; set; }
        public decimal chqamt { get; set; }
        public DateTime reqdt { get; set; }
        public string qutno { get; set; }
        public string Chqno { get; set; }
        public string acccode { get; set; }
        public string Bank_Accode { get; set; }
        public string comment { get; set; }
        public string po_loccode { get; set; }
        public string FinalizedBy { get; set; }
        public DateTime chqdate { get; set; }
        public DateTime cancelled_dt { get; set; }
        public string GRNNO { get; set; }
        public string ScopeOfWork { get; set; }
        public string Finalized { get; set; }
        public decimal srno { get; set; }
        public string VendorBIllNo { get; set; }
        public DateTime finclosedt { get; set; }
        public DateTime UPDTON { get; set; }
        public DateTime ENTRYON { get; set; }
        public string accdesc { get; set; }
        public DateTime Bal_VoucherDT { get; set; }
        public DateTime Adv_VoucherDT { get; set; }
        public DateTime PAYDT { get; set; }
        public decimal PO_balamt { get; set; }
        public decimal cashamt { get; set; }
        public string cancelled_by { get; set; }
        public string FabricSheetPO_YN { get; set; }
        public string voucherNo { get; set; }
        public DateTime SpotDelDT { get; set; }
        public string cancelled { get; set; }
        public DateTime POCloseDt { get; set; }
        public decimal PAIDAMT { get; set; }
        public string Reference { get; set; }
        public string PerfectBoxPO_YN { get; set; }
        public DateTime Duedt { get; set; }
        public string UPDTBY { get; set; }
        public string terms_condition { get; set; }
        public DateTime podate { get; set; }
        public string MatCat { get; set; }
        public string attn { get; set; }
        public string Time { get; set; }
        public int PO_advamt { get; set; }
        public DateTime FinalizedDT { get; set; }
        public string Manual_PO_No { get; set; }
        public decimal pendamt { get; set; }
        public string ENTRYBY { get; set; }
        public string Cash_Accode { get; set; }
        public string VENDORCD { get; set; }
        public string Bal_Voucherno { get; set; }
        public decimal totalamt { get; set; }
        public string POCloseBy { get; set; }
        public string vendorname { get; set; }
        public bool IsCheck { get; set; }

        // add by ashok
        public string PO_Approval { get; set; }
        public decimal BalanceQty { get; set; }
    }
}

