﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Accounting_Master
    {
        public int ID { get; set; }
        public int SrNo { get; set; }
        public bool IsAvailable { get; set; }
        public string ChargeName { get; set; }
        public decimal MaxAmt { get; set; }
        public decimal NetAmt { get; set; }
        public string Accode { get; set; }
        public string Narration { get; set; }
        public string BRCD { get; set; }
        public bool IsActive { get; set; }

        public string CodeDesc { get; set; }
        public string LegderCode { get; set; }
        public string ExpenseType { get; set; }
    }
}