﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_FUELBRAND
    {
        public int Fuel_Brand_ID { get; set; }
        public string Fuel_Brand_Name { get; set; }
        public string VendorCode { get; set; }
        public string Fuel_Type_ID { get; set; }
        public string Active_Flag { get; set; }
    }
}