﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_custcontract_frtmatrix_slabdet
    {
        public string to_loccode { get; set; }
        public string basedon1 { get; set; }
        public string ContractId { get; set; }
        public string from_loccode { get; set; }
        public string trans_type { get; set; }
        public int trdays { get; set; }
        public string slab_code { get; set; }
        public string LOC_REG { get; set; }
        public string basecode1 { get; set; }
        public string basecode2 { get; set; }
        public int rate { get; set; }
        public int srno { get; set; }
        public string basedon2 { get; set; }
    }
}