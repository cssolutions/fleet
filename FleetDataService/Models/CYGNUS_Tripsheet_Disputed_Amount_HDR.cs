﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Tripsheet_Disputed_Amount_HDR
    {
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string BillNo { get; set; }
        public string BankAccNumber { get; set; }
        public string BankName { get; set; }
        public string IFSCCode { get; set; }
        public string TRIPSHEETNO { get; set; }
        public string VEHICLENO { get; set; }
        public string Brcd { get; set; }
        public int ExpenceType { get; set; }
        public DateTime TRIPSHEETSTARTDATE { get; set; }
        public DateTime OPERATIONLLYCLOSUREDATE { get; set; }
        public DateTime FINANCIALLYCLOSUREDATE { get; set; }
        public DateTime DRIVERSETTLEMENTDATE { get; set; }
        public decimal TOTALEXPENSE { get; set; }
        public decimal DISTANCECOVERED { get; set; }
        public decimal TOTALFUEL { get; set; }
        public string FUELTYPE { get; set; }
        public decimal Total_Toll { get; set; }
        public DateTime BR_Date { get; set; }

        public string Vehicle_Mode { get; set; }
        public string RUTCAT { get; set; }
        public string RUTCD { get; set; }

        //public string NAMEOFDRIVER { get; set; }
        //public string BillNo { get; set; }
        //public string ACCOUNTSNO { get; set; }
        //public string TRIPSHEETNO { get; set; }
        //public string VEHICLENO { get; set; }
        //public string Brcd { get; set; }
        //public int ExpenceType { get; set; }
        //public DateTime TRIPSHEETSTARTDATE { get; set; }
        //public DateTime OPERATIONLLYCLOSUREDATE { get; set; }
        //public DateTime FINANCIALLYCLOSUREDATE { get; set; }
        //public DateTime DRIVERSETTLEMENTDATE { get; set; }
        //public decimal TOTALEXPENSE { get; set; }
        //public decimal DISTANCECOVERED { get; set; }
        //public decimal TOTALFUEL { get; set; }
        //public string FUELTYPE { get; set; }
    }
}