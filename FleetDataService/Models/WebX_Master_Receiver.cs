﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class WebX_Master_Receiver
    {
        public string ReceiverCode { get; set; }
        public DateTime EntryDate { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string LocCode { get; set; }
        public string UpdateBy { get; set; }
        public string ReceiverName { get; set; }
    }
}