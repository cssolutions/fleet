﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_HandlingLoading_Unloading_Expence_DET
    {
        public bool CheckBox { get; set; }
        public string Reason_For_Rejection { get; set; }
        public int ID { get; set; }
        public DateTime ExpensIncurredDate { get; set; }
        public string RejectReason { get; set; }
        public DateTime RejectDate { get; set; }
        public bool IsReject { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
        public int NoOfPKGS { get; set; }
        public string DocketNo { get; set; }
        public bool IsApprove { get; set; }
        public decimal AmountPaid_Per_Branch { get; set; }
        public bool PerKGS { get; set; }
        public string RejectBy { get; set; }
        public decimal PerKG_AmountPaid_ActualWeight { get; set; }
        public decimal StdRate { get; set; }
        public decimal RateCharged { get; set; }
        public decimal ActualWeight { get; set; }
        public int Amount_Passed { get; set; }
        public string ExpenseType { get; set; }
        public string PorterType { get; set; }
        public int Amount_Rejected { get; set; }
        public string BillNo { get; set; }
        public int ReferenceNo { get; set; }
        public bool PerPackage { get; set; }
        public string LodingUnlodingType { get; set; }
        public string ManifestNumber { get; set; }

    }
}