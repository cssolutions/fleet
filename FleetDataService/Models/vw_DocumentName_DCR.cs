﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_DocumentName_DCR
    {
        public string DOC_Called_AS { get; set; }
        public string CharacterRequired { get; set; }
        public int SrNo { get; set; }
        public string ManualNoRequired { get; set; }
        public string DefaultValue { get; set; }
        public string Sql_Pattern { get; set; }
        public string Regex_Pattern { get; set; }
        public int NoOfCharacter { get; set; }
        public int NoofDigit { get; set; }
        public string DocumentID { get; set; }
        public string CompanyWiseRequired { get; set; }
        public string DCRRequired { get; set; }
        public int DCRLength { get; set; }
        public string BusinessWiseRequired { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string ManualNoMandatory { get; set; }
        public string MessageToShow { get; set; }
        public string ManualNoDuplicate { get; set; }

        public string CodeID { get; set; }
        public string CodeDesc { get; set; }

    }
}