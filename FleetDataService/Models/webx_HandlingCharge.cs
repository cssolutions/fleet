﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_HandlingCharge
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public string ContractId { get; set; }
        public string ContractType { get; set; }
        public string LoadingBy { get; set; }
        public string RateType { get; set; }
        public decimal Rate { get; set; }
        public decimal Wt_Pkg { get; set; }
        public decimal MaxAmount { get; set; }
        public string EntryBy { get; set; }
        public decimal TotalCharge { get; set; }
        public string BranchCode { get; set; }
        public string VendorCode { get; set; }
        public decimal MonthlyAmount { get; set; }
        public bool IsMonthlyFixed { get; set; }
        public bool CheckCharge { get; set; }
        public string Customer { get; set; }
        public decimal Amount { get; set; }
        public bool IsCustomer { get; set; }
        public string CustomerCode { get; set; }
        public decimal CustomerCharges { get; set; }
    }
}