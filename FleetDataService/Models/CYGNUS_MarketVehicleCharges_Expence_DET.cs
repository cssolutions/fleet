﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_MarketVehicleCharges_Expence_DET
    {
        public bool CheckBox { get; set; }
        public string Vendor_Place_Name { get; set; }
        public string DocumentNo { get; set; }
        public decimal Rate_Total_Amount_Weight { get; set; }
        public bool IsApprove { get; set; }
        public decimal Amount { get; set; }
        public string ApproveBy { get; set; }
        public decimal Actual_Weight { get; set; }
        public DateTime ApproveDate { get; set; }
        public string Contract_Number_of_Vehicle_Owner { get; set; }
        public string Vehicle_Number { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string RejectBy { get; set; }
        public string Reason_For_Rejection { get; set; }
        public DateTime RejectDate { get; set; }
        public string MTo { get; set; }
        public DateTime Date { get; set; }
        public string Docket_PRS_DRS_THC_No { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Number_Of_Packages { get; set; }
        public string BillNo { get; set; }
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public bool IsReject { get; set; }
        public decimal Distance { get; set; }
        public string MFrom { get; set; }
        public string Hours { get; set; }
    }
}