using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
	[XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class Webx_master_LocationConnection
	{
        
        public int Code { get; set; }

        public string ManifestLocation { get; set; }
        public string Locations { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
		}
	}

