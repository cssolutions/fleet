﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
namespace FleetDataService.Models
{    
    public class WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO
    {
        public decimal QUANTITYINLITER { get; set; }
        public int SRNO { get; set; }
        public string BILLNO { get; set; }
        public string FUELVENDOR { get; set; }
        public string BRCD { get; set; }
        public string CANCELBY { get; set; }
        public DateTime UPDATEDT { get; set; }
        public int SLIPTYPE { get; set; }
        public decimal AMOUNT { get; set; }
        public DateTime CANCELDT { get; set; }
        public string UPDATEBY { get; set; }
        public string TRIPSHEET_THC { get; set; }
        public DateTime ENTRYDT { get; set; }
        public string TRIPSHEETNO { get; set; }
        public decimal RATE { get; set; }
        public string VehicleNo { get; set; }
        public string CANCEL_REASON { get; set; }
        public string FUELSLIPNO { get; set; }
        public string ENTRYBY { get; set; }
        public string BrcdCode { get; set; }
        public int Id { get; set; }

    }
}