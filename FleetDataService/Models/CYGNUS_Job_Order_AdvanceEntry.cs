﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CYGNUS_Job_Order_AdvanceEntry
    {
        public int ID { get; set; }
        public string JobOrderNo { get; set; }
        public string PayMode { get; set; }
        public decimal Advance_Amount { get; set; }
        public string Selected_Ledger { get; set; }
        public string Against_Ledger { get; set; }
        public string ChqNo { get; set; }
        public DateTime ChqDate { get; set; }
        public string BRCD { get; set; }
        public string Advance_Paid_Type { get; set; }
        public string Advance_Paid_Code { get; set; }
        public string Advance_Paid_Name { get; set; }
        public string VoucherNo { get; set; }
        public decimal Total_JobAmount_GenerationTime { get; set; }
        public decimal Total_JobAmount_CloseTime { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ISCancel { get; set; }
    }
}