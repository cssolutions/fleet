﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_FM_Scan_Documents
    {
        public decimal ID { get; set; }
        public string DocumentNo { get; set; }
        public byte ScanStatus { get; set; }
        public string DocketNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DocumentName { get; set; }
        public string DocumentHref { get; set; }
        public bool Doc_Fwd { get; set; }
        public byte DocType { get; set; }

        public int Srno { get; set; }
        public string Status { get; set; }
    }

    public class Webx_FM_Scan_Documents_Android
    {
        public decimal ID { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentNo2 { get; set; }
        public int ScanStatus { get; set; }
        public bool Scan_Status { get; set; }
        public string DocketNo { get; set; }
        public DateTime DocumentDate { get; set; }
        public string DocumentName { get; set; }
        public string DocumentHref { get; set; }
        public bool Doc_Fwd { get; set; }
        public byte DocType { get; set; }

        public int Srno { get; set; }
        public string Status { get; set; }
        public string ImagePath { get; set; }
        public string ImagePath1 { get; set; }
    }

    public partial class Webx_FM_Scan_DocumentsDone
    {
        public string DocketNo { get; set; }
        public string Status { get; set; }
    }

    public partial class Webx_DocketNo
    {
        public string DOCKNO { get; set; }
        public string DRS { get; set; }
        public string DOCKSF { get; set; }
    }
    public partial class Webx_DocketNo_Del_Status
    {
        public string DOCKNO { get; set; }
        public string Delivered { get; set; }
    }

    public partial class Webx_DRS_DocketNo_Delivered
    {
        public string DOCKNO { get; set; }
        public string DOCKSF { get; set; }
        public string DRS { get; set; }
        public string DELYDATE { get; set; }
        public string DELYTIME { get; set; }
        public string DELYPERSON { get; set; }
        public string IMEINumber { get; set; }
        public string BRCD { get; set; }
    }
    public partial class Webx_DRS_DocketNo_Delivered_Response
    {
        public string MESSAGE { get; set; }
        public string STATUS { get; set; }
    }

    public partial class Webx_DocketNo_Criteria
    {
        public string DRSNO { get; set; }
        public string BRCD { get; set; }
        public string UserName { get; set; }
        public string CompanyCode { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string IMEINo { get; set; }
    }

    public class PRQ_VW
    {
        public CYGNUSPickupRequest ObjCPR { get; set; }
        public List<CYGNUSPickupRequest> List_CPR { get; set; }
    }

}