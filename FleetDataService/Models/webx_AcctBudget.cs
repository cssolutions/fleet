﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_AcctBudget
    {
        public int Srno { get; set; }
        public DateTime FinyearStart { get; set; }

        public string brcd { get; set; }
        public string LedgerCode { get; set; }
        public string Budget_Type { get; set; }
        public string Budget_Month_Year { get; set; }
        public int BudgetAmount { get; set; }
        public string COMPANY_CODE { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime Entrydate { get; set; }

        public string YearVal { get; set; }
        public string FinYear { get; set; }
        public string YearType { get; set; }

        public string LocCode { get; set; }
        public string Location { get; set; }
        public string ACCCODE { get; set; }

        public string ACCDESC { get; set; }
        public decimal txtAmt { get; set; }
        public decimal JanAmt { get; set; }
        public decimal FebAmt { get; set; }
        public decimal MarAmt { get; set; }
        public decimal AprAmt { get; set; }
        public decimal MayAmt { get; set; }
        public decimal JunAmt { get; set; }
        public decimal JulAmt { get; set; }
        public decimal AugAmt { get; set; }
        public decimal SepAmt { get; set; }
        public decimal OctAmt { get; set; }
        public decimal NovAmt { get; set; }
        public decimal DecAmt { get; set; }
        public bool IsCheck { get; set; }


    }
}