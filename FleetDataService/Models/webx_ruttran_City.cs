﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_ruttran_City
    {
        public string STTIME_HR { get; set; }
        public string TRTIME_MIN { get; set; }
        public string RUTCD { get; set; }
        public string STTIME_MIN { get; set; }
        public decimal DIST_KM { get; set; }
        public int RUTNO { get; set; }
        public string LOCCD { get; set; }
        public string TRTIME_HR { get; set; }
    }
}