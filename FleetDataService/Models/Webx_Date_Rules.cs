﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Date_Rules
    {
        public string Err_Msg { get; set; }
        public string RuleDesc { get; set; }
        public string Rule_Y_N { get; set; }
        public string RuleValue { get; set; }
        public decimal Srno { get; set; }
        public DateTime Min_Date { get; set; }
        public string Module_Name { get; set; }
        public string RuleId { get; set; }
        public decimal BackDate_Days { get; set; }
        public string Module_Code { get; set; }
        public string Empcd { get; set; }

    }
}