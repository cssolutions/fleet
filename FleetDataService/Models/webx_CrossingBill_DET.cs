using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_CrossingBill_DET
    {
        public string BILLNO { get; set; }
        public string CrossingChallanNo { get; set; }
        public DateTime CrossingChallanDT { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public decimal DKTTOTCount { get; set; }
        public decimal Pkgs { get; set; }
        public decimal ActuWeight { get; set; }
        public decimal TotalTopay { get; set; }
        public decimal CrossingChrg { get; set; }
        public decimal BulkyChrg { get; set; }
        public decimal NetPayable { get; set; }
        public string EntryBy { get; set; }
        public DateTime entrydt { get; set; }
        public int iSNO { get; set; }
    }
}