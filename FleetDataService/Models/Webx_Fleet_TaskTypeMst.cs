﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Fleet_TaskTypeMst
    {
        public string TaskType { get; set; }
        public int TaskTypeID { get; set; }
        public string AccCode { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }

        public string AccDesc { get; set; }
    }
}