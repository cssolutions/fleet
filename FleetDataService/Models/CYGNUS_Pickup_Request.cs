﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUSPickupRequest
    {
        public int ID { get; set; }
        public string PRQNo { get; set; }
        public string PreferdAddress { get; set; }
        public string Address { get; set; }
        public string LandMark { get; set; }
        public string City { get; set; }
        public string Pincode { get; set; }
        public string State { get; set; }
        public string Name { get; set; }
        public string MobileNo { get; set; }
        public string VehicleType { get; set; }
        public decimal ApproxWeight { get; set; }
        public int PKGS { get; set; }
        public string ImeiNo { get; set; }
        public string OriginLat { get; set; }
        public string OriginLong { get; set; }
        public string BRCD { get; set; }
        public string ChangeBRCD { get; set; }
        public int Status { get; set; }
        public string Remarks { get; set; }
        public string VehicleNo { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public bool IsAssign { get; set; }
        public string AssignBy { get; set; }
        public string AssignTo { get; set; }
        public DateTime AssignDate { get; set; }
        public bool IsReject { get; set; }
        public string RejectBy { get; set; }
        public DateTime RejectDate { get; set; }
        public bool IsCancel { get; set; }
        public string CancelBy { get; set; }
        public DateTime CancelDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsFromAndroid { get; set; }
        public DateTime ExcepctedEndDate { get; set; }
        public string ExcepctedEndDateStr { get; set; }
        public string Type_Name { get; set; }
        public string SelectedGoogleAddress { get; set; }

        public decimal ActualWeight { get; set; }
        public int ActualPKGS { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustName { get; set; }
        public DateTime PRQDate { get; set; }
        public string DockNo { get; set; }
        public string Destcd { get; set; }
        public bool IsDocketCompleted { get; set; }
        public DateTime DocketGeneratedDate { get; set; }
        public string DocketGenerateBy { get; set; }
        public string DocketCompletedBy { get; set; }
        public DateTime DocketCompletedDate { get; set; }
        public bool IsDocketGenerated { get; set; }

        public string FinYear { get; set; }

    }

    public partial class CYGNUSPickupRequest_Response
    {
        public int ID { get; set; }
        public string PRQNO { get; set; }
        public string MESSAGE { get; set; }
        public int STATUS { get; set; }
    }

    public partial class CYGNUSPickupRequest_Address
    {
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string LandMark { get; set; }
        public string Pincode { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string MobileNo { get; set; }
        public string OriginLat { get; set; }
        public string OriginLong { get; set; }
        public string PreferdAddress { get; set; }
    }

    public partial class CYGNUSPRQHistory
    {
        public string PRQNo { get; set; }
        public int Status { get; set; }
        public string Status_Name { get; set; }
        public bool IsFromTab { get; set; }
        public string Remarks { get; set; }
        public string BRCD { get; set; }
        public string    DriverID { get; set; }
        public string DriverName { get; set; }
        public string AssignBy { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string DocketNo { get; set; }

        public bool IsDocketGenerated { get; set; }
        public bool IsDocketCompleted { get; set; }

    }

    public partial class PRQQuickDocketGeneration
    {
        public string DockNo { get; set; }
        public DateTime DockDate { get; set; }
        public DateTime PRQDate { get; set; }
        public string Origin { get; set; }
        public string Destcd { get; set; }
        public string CustomerCode { get; set; }
        public string BRCD { get; set; }
        public decimal ActualWeight { get; set; }
        public int NoOfPKGS { get; set; }
    }
}