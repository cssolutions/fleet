﻿namespace FleetDataService.Models
{
    public class webx_MRDET_BILL
    {
        public string MRSNO { get; set; }
        public string MRSSF { get; set; }
        public decimal TOTBILL { get; set; }
        public decimal TDSDED { get; set; }
        public float ONAC { get; set; }
        public float TOTBDED { get; set; }
        public float OTHERDED { get; set; }

        public decimal NETAMT { get; set; }
        public float FRTDED { get; set; }
        public float CLMDED { get; set; }
        public decimal UNEXPDED { get; set; }
        public string DOCNO { get; set; }
        public string DOCSF { get; set; }


        public float OCT_AMT { get; set; }
        public float OCT_SVCCHG { get; set; }
        public float DECLVAL { get; set; }
        public float FRTDED_frt { get; set; }
        public float FRTDED_svctax { get; set; }
        public float FRTDED_cess { get; set; }
        public decimal SCHG01 { get; set; }
        public decimal SCHG02 { get; set; }
        public decimal SCHG03 { get; set; }
        public decimal SCHG04 { get; set; }
        public decimal SCHG05 { get; set; }
        public decimal SCHG06 { get; set; }
        public decimal SCHG07 { get; set; }
        public decimal SCHG08 { get; set; }
        public decimal SCHG09 { get; set; }
        public decimal SCHG10 { get; set; }
        public decimal STax_Rate { get; set; }
        public decimal Cess_Rate { get; set; }
        public decimal HCess_Rate { get; set; }
        public decimal Col_Amt { get; set; }
        public decimal STax_Amt { get; set; }
        public decimal Cess_Amt { get; set; }
        public decimal HCess_Amt { get; set; }
        public int FinYear { get; set; }
        public bool IsMrApproval { get; set; }
        public string ENTRYBY { get; set; }
        public string MRSBR { get; set; }
        public string PayBasis { get; set; }
        public string paymode { get; set; }
    }
}