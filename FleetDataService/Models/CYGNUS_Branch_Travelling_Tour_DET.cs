﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Travelling_Tour_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BillNo { get; set; }
        public DateTime Date { get; set; }
        public string LocationFrom { get; set; }
        public string LocationTo { get; set; }
        public string TravellingMode { get; set; }
        public string Class { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount_Passed { get; set; }
        public string Name_Of_City { get; set; }
        public string Grade_Of_City { get; set; }
        public string Name_Of_Hotel { get; set; }
        public DateTime DateOfCheckIn { get; set; }
        public DateTime DateOfCheckOut { get; set; }
        public int DaysOfStay { get; set; }
        public decimal Distance { get; set; }
        public string Particular { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }

        public decimal Fare_SubTotal_Amount { get; set; }
        public decimal Fare_SubTotal_Amount_Passed { get; set; }
        public decimal Fare_SubTotal_Amount_Rejected { get; set; }

        public decimal Conveyance_SubTotal_Amount { get; set; }
        public decimal Conveyance_SubTotal_Amount_Passed { get; set; }
        public decimal Conveyance_SubTotal_Amount_Rejected { get; set; }

        public decimal Bo_Lo_SubTotal_Amount { get; set; }
        public decimal Bo_Lo_SubTotal_Amount_Passed { get; set; }
        public decimal Bo_Lo_SubTotal_Amount_Rejected { get; set; }

        public decimal Food_SubTotal_Amount { get; set; }
        public decimal Food_SubTotal_Amount_Passed { get; set; }
        public decimal Food_SubTotal_Amount_Rejected { get; set; }

        public decimal Other_SubTotal_Amount { get; set; }
        public decimal Other_SubTotal_Amount_Passed { get; set; }
        public decimal Other_SubTotal_Amount_Rejected { get; set; }

        public decimal GrandTotal_SubTotal_Amount { get; set; }
        public decimal GrandTotal_SubTotal_Amount_Passed { get; set; }
        public decimal GrandTotal_SubTotal_Amount_Rejected { get; set; }

        public string SubExpense_Type { get; set; }
        public int SubType { get; set; }

       
        public string Image { get; set; }

    }
}