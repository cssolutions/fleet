﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_Series_ToBe_Alloted2
    {
        public int Used { get; set; }
        public int SrNo { get; set; }
        public string Alloted_By { get; set; }
        public string DOC_TYPE { get; set; }
        public int DOC_KEY { get; set; }
        public string Entry_By { get; set; }
        public DateTime Entry_Date_Sys { get; set; }
        public string Alloted_To { get; set; }
        public string Alloted_On { get; set; }
        public string Alloted_To_Name { get; set; }
        public string DOC_SR_TO { get; set; }
        public int Cancelled { get; set; }
        public int BUSINESS_TYPE_ID { get; set; }
        public string BUSINESS_TYPE { get; set; }
        public string DOC_SR_FROM { get; set; }
        public string ConNo { get; set; }
        public string BookCode { get; set; }
        public int TOT_LEAF { get; set; }
        public bool rdioFromSeries { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}