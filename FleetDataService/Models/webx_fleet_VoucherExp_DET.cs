﻿namespace FleetDataService.Models
{
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class webx_fleet_VoucherExp_DET
    {
        public int SRNO { get; set; }
        public string VoucherNo { get; set; }
        public string ExpenseId { get; set; }
        public decimal Amt { get; set; }
        public decimal ExeAmt { get; set; }
        public string BillNo { get; set; }
        public string Remarks { get; set; }
        public string VehicleNo { get; set; }
        public string DriverId { get; set; }

        public List<Webx_Master_General> ListNatureOfExpense { get; set; }
    }
}