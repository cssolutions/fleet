﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Master_Reports
    {

        public int ID { get; set; }
        public string ReprotName { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public string ReportType { get; set; }
        public string ReportSubType { get; set; }
        public string RDLName { get; set; }


        public int ParameterSetId { get; set; }
        public bool IsActive { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }

        public bool ShowParameterPrompts { get; set; }
        public bool ShowFindControls { get; set; }
        public bool ShowToolBar { get; set; }


        public bool HasAccess { get; set; }



        public string UserName { get; set; }
    }
}