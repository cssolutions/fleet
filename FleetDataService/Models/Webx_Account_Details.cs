﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class Webx_Account_Details
    {
        public int SRNo { get; set; }
        public string AccountingLocation { get; set; }
        public string BusinessDivision { get; set; }
        public string CustCode { get; set; }
        public string CustName { get; set; }
        public string ManualNo { get; set; }
        public string Narration { get; set; }
        public string PrepardBy { get; set; }
        public string Preparedatlocation { get; set; }
        public string PreparedFor { get; set; }
        public string ReferenceNo { get; set; }
        public DateTime? VoucherDate { get; set; }
        public string VoucherNo { get; set; }
        public string CustomerName { get; set; }
        public decimal SumAmount { get; set; }
        public decimal ServiceTaxRate { get; set; }
        public decimal AmountApplicable { get; set; }
        public decimal ServiceTax { get; set; }
        public decimal EducationCess { get; set; }
        public decimal HEducationCess { get; set; }
        public string ServiceTaxRegNo { get; set; }
        public decimal TDSRate { get; set; }
        public decimal TDSAmount { get; set; }
        public string PANNumber { get; set; }
        public decimal CashAmount { get; set; }
        public decimal ChequeAmount { get; set; }
        public string ChequeNo { get; set; }
        public DateTime Chequedate { get; set; }
        public bool Deposited { get; set; }
        public string DepositedInBank { get; set; }
        public string ReceivedFromBank { get; set; }
        public string TDSSection { get; set; }
        public decimal NETAMOUNT { get; set; }
        public string ContraAccount { get; set; }
        public decimal TransactionAmount { get; set; }
        public decimal AmountApplicableForTDS { get; set; }

        public string TransactionMode { get; set; }
        public string PaymentMode { get; set; }
        public string CashAccount { get; set; }

        public bool IsServicetaxapply { get; set; }
        public bool IsTDStaxapply { get; set; }
        public bool IsTDSWithoutSvcTax { get; set; }
        public bool IsonAccount { get; set; }
        public string CustTyp { get; set; }
        public decimal DebitTotal { get; set; }
        public decimal CreditTotal { get; set; }

        public string Opertitle { get; set; }
        public string Ptype { get; set; }
        public string Vouchertype { get; set; }
        public int ExpesneType { get; set; }

        public bool OKPOD { get; set; }

        public string FuelCardNo { get; set; }
        public bool IsFuelCard { get; set; }

        public string SelectVoucherType { get; set; }

        /*GST Details*/
        public string GSTType { get; set; }
        public decimal GSTPercentage { get; set; }
        public decimal GSTBillAmount { get; set; }
        public string StateCode { get; set; }
        public string Type { get; set; }
        public string Image { get; set; }
        public string TypeOfGst { get; set; }
    }

    public class Webx_Acc
    {
        public int Srno { get; set; }
        public string AccountCode { get; set; }
        public string Amount { get; set; }
        public string TotalAmount { get; set; }
        public string CostType { get; set; }
        public string CostCode { get; set; }
        public string NarrationAccountinfo { get; set; }
        public string DocType { get; set; }
        public string DocNo { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public string PartyName { get; set; }
        public string PartyCode { get; set; }
        public string BRCD { get; set; }


        public string PaidTo { get; set; }
        public string AccDesc { get; set; }
        public bool Particular { get; set; }

        /*Voucher cancellation*/
        public string VoucherNo { get; set; }
        public bool IsCheck { get; set; }
        public DateTime VoucherDate { get; set; }
        public string voucher_cancel { get; set; }
        public string Transtype { get; set; }
        public string comment { get; set; }

        public string opertitle { get; set; }
        public string VoucherDateStr { get; set; }

        //Bharat

        public int PCS { get; set; }
        public decimal VendorWt { get; set; }
        public decimal RCPLWt { get; set; }
        public decimal DiffWt { get; set; }
        public decimal VendorRate { get; set; }
        public decimal RCPLRate { get; set; }
        public decimal DiffRate { get; set; }
        public decimal WtDiffAmt { get; set; }
        public decimal RateDiffAmt { get; set; }
    }

}