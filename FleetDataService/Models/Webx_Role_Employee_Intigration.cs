﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Role_Employee_Intigration
    {
        public string SetRole { get; set; }
        public int Id { get; set; }
        public int RoleId { get; set; }
        public string Roledesc { get; set; }
        public bool MultiPle { get; set; }
        public decimal SRNO { get; set; }
        public bool ACTIVEFLAG { get; set; }

        public string LocCode { get; set; }
        public string LocName { get; set; }

        public string UserId { get; set; }
        public string Name { get; set; }
        public string BranchCode { get; set; }
        public string EmailId { get; set; }
        public string Checked { get; set; }
        public bool Checked1 { get; set; }
    }
}