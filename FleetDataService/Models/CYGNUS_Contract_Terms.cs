﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class CYGNUS_Contract_Terms
    {
        public int ContractID { get; set; }
        public string CustCD { get; set; }
        public string Description { get; set; }
        public Int64 RNo { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDt { get; set; }
    }
}