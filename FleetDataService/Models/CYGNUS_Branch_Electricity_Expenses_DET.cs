﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Electricity_Expenses_DET
    {
        public int RowId { get; set; }
        public bool CheckBox { get; set; }
        public int ID { get; set; }
        public string BillNo { get; set; }
        public int ReferenceNo { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public decimal Total_Area_sqft { get; set; }
        public int NoOf_Rooms { get; set; }
        public int Computers { get; set; }
        public int TubeLights { get; set; }
        public int Bulbs { get; set; }
        public int Printers { get; set; }
        public int CFLs { get; set; }
        public string Others_Elec_Equip_Gadgets { get; set; }
        public decimal Previous_Claimed_Amount { get; set; }
        public string Previous_Month_When_Claimed { get; set; }
        public int Month_For_Which_Claim_BeingMade { get; set; }
        public decimal PreviousReading { get; set; }
        public decimal CurrentReading { get; set; }
        public decimal UnitConsumption { get; set; }
        public decimal RateAsPer_Agreement { get; set; }
        public decimal Others { get; set; }
        public decimal Total_Amount { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }
        public string RefBillNo { get; set; }

        public string Name_Of_Land_Lord { get; set; }
        public string Land_Lord_Bank_Account_No { get; set; }
        public string Land_Lord_IFSC_Code { get; set; }
        public int Bill_Year { get; set; }
        public string Month_Year { get; set; }

        public string ElectriCity_BillImage { get; set; }


        

    }
}