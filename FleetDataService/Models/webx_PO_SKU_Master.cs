﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_PO_SKU_Master
    {
        public int Srno { get; set; }
        public string SKU_ID { get; set; }
        public string SKU_Desc { get; set; }
        public string MatCat_Id { get; set; }
        public string Mfg_Desc { get; set; }
        public string SKU_Type { get; set; }
        public string SKU_Size { get; set; }
        public string SKU_UOM { get; set; }
        public string SKU_UDF1 { get; set; }
        public string SKU_UDF2 { get; set; }
        public string SKU_UDF3 { get; set; }
        public string SKU_UDF4 { get; set; }
        public string SKU_UDF5 { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDt { get; set; }
        public string SKU_Remark { get; set; }
        public string UOM_ID { get; set; }

        public string W_GRPCD { get; set; }
        public string W_GRPDESC { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }

        public bool IsCheck { get; set; }
        public bool IsActive { get; set; }
    }
}