﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Fleet_spareParthdr
    {
        public string Part_Id { get; set; }
        public string Part_Type { get; set; }
        public string Part_Category { get; set; }
        public string Description { get; set; }
        public string Unit_Of_Inventory { get; set; }
        public decimal Avg_Cost { get; set; }
        public decimal Selling_Price { get; set; }
        public string Reorder_Point { get; set; }
        public decimal Reorder_Qty { get; set; }
        public DateTime EntryDate { get; set; }
        public string ActiveFlag { get; set; }
        public DateTime ExpDt { get; set; }
        public decimal ExpKms { get; set; }
        public int W_GRPDC { get; set; }
        public int TaskTypeId { get; set; }
        public string SKU_GRPDC { get; set; }

        public string Value { get; set; }
        public string Text { get; set; }
        public string W_GrpDesc { get; set; }
        public string TaskType { get; set; }
    }
}