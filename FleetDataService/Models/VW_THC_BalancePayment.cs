﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class VW_THC_BalancePayment
    {
        public int ID { get; set; }
        public int RNO { get; set; }
        public DateTime Docdt { get; set; }
        public decimal HEDU_CESS { get; set; }
        public string DOCTYPE { get; set; }
        public decimal Advamt { get; set; }
        public string Docno { get; set; }
        public decimal tdsded { get; set; }
        public string Doc_Close_mode { get; set; }
        public string Company_Code { get; set; }
        public string VendorCode { get; set; }
        public string Doc_Cancelled { get; set; }
        public string Bal_Location { get; set; }
        public decimal OPENKM { get; set; }
        public decimal Svctax_cess { get; set; }
        public decimal svrc_rate { get; set; }
        public string cancelled { get; set; }
        public decimal Contract_Amt { get; set; }
        public decimal CLOSEKM { get; set; }
        public string Voucherno { get; set; }
        public string FtlType { get; set; }
        public decimal CESSAMT { get; set; }
        public string Rut_cat { get; set; }
        public string DocBR { get; set; }
        public string DocDesc { get; set; }
        public string Docdate { get; set; }
        public string Doc_Status { get; set; }
        public string Docn_Mode { get; set; }
        public string BACODE { get; set; }
        public string Docsf { get; set; }
        public string conttyp { get; set; }
        public decimal NET { get; set; }
        public string Dockdate { get; set; }
        public string ManualDocno { get; set; }
        public string Dot_Type { get; set; }
        public decimal TOTALKM { get; set; }
        public string VendorBENo { get; set; }
        public decimal TdsRate { get; set; }
        public string Finalized { get; set; }
        public string VendorName { get; set; }
        public string vehno { get; set; }
        public string DOC_STR { get; set; }
        public string FinStatus { get; set; }
        public decimal SVCTAX { get; set; }
        public decimal OTHCHRG { get; set; }
        public string RouteCD { get; set; }
        public decimal netbalamt { get; set; }
        public string Adv_Location { get; set; }
        public decimal PaymentAmount { get; set; }

        public decimal SCHG01 { get; set; }
        public decimal SCHG02 { get; set; }
        public decimal SCHG03 { get; set; }
        public decimal SCHG04 { get; set; }
        public decimal SCHG05 { get; set; }
        public decimal SCHG06 { get; set; }
        public decimal SCHG07 { get; set; }
        public decimal SCHG08 { get; set; }
        public decimal SCHG09 { get; set; }
        public decimal SCHG10 { get; set; }

        public string Operator { get; set; }
        public string ChargeName { get; set; }
        public string ChargeAcccode { get; set; }
        public string Narration { get; set; }

        public decimal OTherChrg { get; set; }

        public List<webx_master_charge> ListCharge { get; set; }

        public string FtlTypeDesc { get; set; }
        public string DocumentType { get; set; }

        public decimal PendingAmount { get; set; }

        public decimal GSTChgAmt { get; set; }
        public decimal TotalWt { get; set; }
        public decimal CostPerKG { get; set; }
    }
}