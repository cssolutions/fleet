﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Master_General
    {
        public string CodeType { get; set; }
        public string CodeId { get; set; }
        public string CodeDesc { get; set; }
        public string CodeAccess { get; set; }
        public string StatusCode { get; set; }
        public System.DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public Nullable<System.DateTime> LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public Nullable<decimal> noofdigits { get; set; }
        public Nullable<decimal> noofchar { get; set; }
        public string codefor { get; set; }
        public string locaname { get; set; }
        public string loccode { get; set; }

        public string RUTCD { get; set; }
        public string RUTDESC { get; set; }

        //Lavnit 
        public decimal HourlyBasedSlot { get; set; }
        public int Id { get; set; }

        public bool activeservice { get; set; }
        public string Name { get; set; }
        public string Vendor_Type { get; set; }
        public string Code { get; set; }

        /* Added Fro Octroi Bill */
        public string OCT_AMT { get; set; }
        public string oct_percentage { get; set; }
        public string DKTTOT { get; set; }
        public string OCT_RECEIPTNO { get; set; }
        public string recptdt { get; set; }
    }

    public class Webx_Master_General_Tab
    {
        public string CodeType { get; set; }
        public string CodeId { get; set; }
        public string CodeDesc { get; set; }
        public string Version { get; set; }
        public string Link { get; set; }
    }

    public class vw_Master_General
    {
        public string CodeID { get; set; }
        public string Code { get; set; }
        public string Loccode { get; set; }
    }

    public class VW_Hierarchy
    {
        public int CodeId { get; set; }
        public string CodeDesc { get; set; }
    }

        public class Webx_Industry_Product_Mapping
    {
        public string MappingType { get; set; }
        public string CustCode { get; set; }
        public string Display { get; set; }
        public string Value { get; set; }
        public bool Checked { get; set; }
    }

    public partial class THCCriteariaSelection
    {
        public int SrNo { get; set; }
        public string Manual_TCNO { get; set; }
        public string TCBR { get; set; }
        public string TCDT { get; set; }
        public string TC_Tobh_Code { get; set; }
        public decimal TOT_DKT { get; set; }
        public decimal TOT_PKGS { get; set; }
        public decimal TOT_ACTUWT { get; set; }
        public string THCBR { get; set; }
        public string THCNO { get; set; }
        public string THC_Date { get; set; }
        public string VehNo { get; set; }
        public string SourceHB { get; set; }
        public string Tobh_Code { get; set; }
        public string Departure_Date { get; set; }
        public string routecd { get; set; }
        public string routename { get; set; }
        public DateTime THCDT { get; set; }
        public DateTime ActDept_Dt { get; set; }
        public string TCNO { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string selectTHCNO { get; set; }


    }
}