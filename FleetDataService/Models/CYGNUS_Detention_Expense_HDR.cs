﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Detention_Expense_HDR
    {
        public int ID { get; set; }
        public int ExpenceType { get; set; }
        public string BillNo { get; set; }
        public string EmpCode { get; set; }
        public string EmpName { get; set; }
        public string BankName { get; set; }
        public string BankAccNumber { get; set; }
        public string IFSCCode { get; set; }
        public string NameOFPayee { get; set; }
        public string Branch_Name { get; set; }
        public string For_The_Month_Of { get; set; }

        public string BRCD { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string Driver_Name { get; set; }
        public DateTime BR_Date { get; set; }

    }
}