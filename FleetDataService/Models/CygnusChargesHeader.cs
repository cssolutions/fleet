﻿using System;

namespace FleetDataService.Models
{
    public class CygnusChargesHeader
    {
        public int Id { get; set; }
        public int TempId { get; set; }
        public string ChargeCode { get; set; }
        public string ChargeName { get; set; }
        public string ChargeType { get; set; }
        public string Operator { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime? EffectiveEndDate { get; set; }
        public decimal Percentage { get; set; }
        public decimal ChargeAmount { get; set; }
        public bool IsDDL { get; set; }
        public string FromTabel { get; set; }
        public string WhereClause { get; set; }
        public bool IsActive { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string Acccode { get; set; }
        public string AccountPayable { get; set; }
        public string AccountReceivable { get; set; }
        public bool IsApplicable { get; set; }
        public bool IsPercentageShow { get; set; }
    }
}