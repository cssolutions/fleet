﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_CustContract_FRTMatrix_SingleSlab
    {
        public int Id { get; set; }
        public string ratetype { get; set; }
        public string chargetype { get; set; }
        public string basedon1 { get; set; }
        public string BillLoc { get; set; }
        public decimal trdays { get; set; }
        public string basecode1 { get; set; }
        public string basecode2 { get; set; }
        public string contractid { get; set; }
        public string trans_type { get; set; }
        public string chargecode { get; set; }
        public string FromLoc { get; set; }
        public string toloc { get; set; }

        public string loc_reg { get; set; }
        public decimal rate { get; set; }
        public string custcode { get; set; }
        public int srno { get; set; }
        public string basedon2 { get; set; }
        public string Flag { get; set; }

        public decimal Slab1 { get; set; }
        public decimal Slab2 { get; set; }
        public decimal Slab3 { get; set; }
        public decimal Slab4 { get; set; }
        public decimal Slab5 { get; set; }
        public decimal Slab6 { get; set; }

        public string FilterFromLoc { get; set; }
        public string FilterToLoc { get; set; }
        public string FilterTrnsMode { get; set; }
        public string FilterMatrixtype { get; set; }
        public string Matrixtype { get; set; }
        public string TransMode { get; set; }
        public string OldFTLType { get; set; }
        public string FTL { get; set; }

        public string SlabMS { get; set; }
        public string InsertType { get; set; }

        public string OldFromLoc { get; set; }
        public string OldToLoc { get; set; }
        public string OldTransType { get; set; }
        public string SlabTransMode { get; set; }

        // Chnage Lavnit

        // Type
        public string Type { get; set; }
        public decimal ODACharge { get; set; }
        public string Slabtype { get; set; }

        public bool isNewZoneContractApplied { get; set; }


        public string FromRange { get; set; }
        public string ToRange { get; set; }
        public string RateUnits { get; set; }
        public string ContractType { get; set; }
        public string LoopTransMode { get; set; }

    }
}