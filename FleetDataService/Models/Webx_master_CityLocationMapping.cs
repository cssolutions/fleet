﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_master_CityLocationMapping
    {
        public string City { get; set; }
        public int Code { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string ActiveFlag { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Locations { get; set; }
        public string UpdateBy { get; set; }
        public decimal city_code { get; set; }
    }
}