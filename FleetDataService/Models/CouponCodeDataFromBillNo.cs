﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CouponCodeDataFromBillNo
    {
        public Int64 ID { get; set; }
        public string BILLNO { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherDate { get; set; }
        public string VoucherAmount { get; set; }
    }
}