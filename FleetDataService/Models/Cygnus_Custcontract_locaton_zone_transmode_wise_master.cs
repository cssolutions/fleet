﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Cygnus_Custcontract_locaton_zone_transmode_wise_master
    {
        public int Id { get; set; }
        public string ZoneId { get; set; }
        public string Loccode { get; set; }
        public string Mode { get; set; }
        public string ReportZone { get; set; }
    }
}