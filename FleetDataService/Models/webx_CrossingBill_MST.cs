using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_CrossingBill_MST
    {
        public string BILLNO { get; set; }
        public string Manualbillno { get; set; }
        public DateTime BGNDT { get; set; }
        public string BBRCD { get; set; }
        public string BBRNM { get; set; }
        public string PTMSCD { get; set; }
        public string PTMSNM { get; set; }
        public string PTMSTEL { get; set; }
        public string PTMSEMAIL { get; set; }
        public string PTMSADDR { get; set; }
        public string BilltypeCD { get; set; }
        public string BilltypeNM { get; set; }
        public string REMARK { get; set; }
        public decimal BILLAMT { get; set; }
        public decimal SUBTOTEL { get; set; }
        public string BGENEMPCD { get; set; }
        public string BILL_CANCEL { get; set; }
        public DateTime BCANDT { get; set; }
        public string CANCOMMENT { get; set; }
        public string CancelBy { get; set; }
        public DateTime entrydt { get; set; }
        public DateTime FromDate { get; set; }
        public string COMPANY_CODE { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateComment { get; set; }

        public string GSTNo { get; set; }
        public string Email { get; set; }
        public string Mobileno { get; set; }
        public string Address { get; set; }
        public string Duration { get; set; }
        public decimal OpeningBalance { get; set; }
        public string VendNameStr { get; set; }

        public decimal TotalDkt { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalPackages { get; set; }
        public decimal TotalBulkyCharges { get; set; }

        public decimal TotalToPayCharges { get; set; }
        public decimal TotalCrossingCharges { get; set; }
        public decimal TotalNetpayableCharges { get; set; }
    }
}