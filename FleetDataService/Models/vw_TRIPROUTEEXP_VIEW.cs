﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_TRIPROUTEEXP_VIEW
    {
        public string vt { get; set; }
        public string fec { get; set; }
        public int ID { get; set; }
        public string vehicle_Type { get; set; }
        public string EXP_RATE_CODE { get; set; }
        public string route_segment { get; set; }
        public decimal standard_rate { get; set; }
        public string route_code { get; set; }
        public string fulexp_code { get; set; }
        public string erc { get; set; }
        public string route_exp_code { get; set; }
        public string active_flag { get; set; }
        public string rutdesc { get; set; }

        public int Srno { get; set; }

        public string VEHICLENO { get; set; }
    }
}