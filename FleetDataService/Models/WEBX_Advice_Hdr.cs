﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class WEBX_Advice_Hdr
    {
        public string AdvRecvByNm { get; set; }

        public string PayMode { get; set; }
        public decimal PaymentAmount { get; set; }
        public decimal CashAmount { get; set; }
        public string CashAccount { get; set; }
        public string BankAccount { get; set; }
        public decimal ChequeAmount { get; set; }
        public string ChequeNo { get; set; }
        public DateTime ChequeDate { get; set; }

        public string adviceStatus { get; set; }
        public decimal advice_amount { get; set; }
        public string Advice_Cancel { get; set; }
        public string AdvGenByCd { get; set; }
        public DateTime AdvRecvDt { get; set; }
        public string RaisedBy { get; set; }
        public string enclosed { get; set; }
        public string RecvLocCd { get; set; }
        public string Adviceno { get; set; }
        public string Advicetype { get; set; }
        public string AdvApprByNm { get; set; }
        public string raiseacccode { get; set; }
        public string reason { get; set; }
        public string Toaccdesc { get; set; }
        public string AdvAccByCd { get; set; }
        public string RaisedOn { get; set; }
        public string entryby { get; set; }
        public DateTime AdviceDt { get; set; }
        public DateTime accdate1 { get; set; }
        public string AdvAccByNm { get; set; }
        public DateTime cancel_date { get; set; }
        public string finyear { get; set; }
        public string fromaccdesc { get; set; }
        public string Toacccode { get; set; }
        public string Cancel_Reason { get; set; }
        public string AdvApprByCd { get; set; }
        public string COMPANY_CODE { get; set; }
        public string AdvAccBrCd { get; set; }
        public string credit_account { get; set; }
        public string raiseaccdesc { get; set; }
        public string AdvGenByNm { get; set; }
        public string cancelby { get; set; }
        public DateTime AdvApprDt { get; set; }
        public DateTime AdvAccDt { get; set; }
        public string fromAcccode { get; set; }
        public string AdvApprBrCd { get; set; }
        public DateTime entrydt { get; set; }
        public string againstadviceno { get; set; }
        public decimal adjusted_amount { get; set; }
        public string AdvRecvByCd { get; set; }
        public bool IsCheck { get; set; }

        public string Accdesc { get; set; }
        public string ChequeDatestr { get; set; }
        public string adviceDt1 { get; set; }
        /*-- Advic Cancellation -- */
        public int SrNo { get; set; }
        public string UpdatedRaisedCode { get; set; }
        public string AdviceAMT { get; set; }
       
    }
}