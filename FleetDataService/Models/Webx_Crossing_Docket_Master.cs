﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Crossing_Docket_Master
    {
        public Int64 SrNo { get; set; }
        public bool IsCheck { get; set; }
        public string CrossingChallanNo { get; set; }
        public DateTime ChallanDate { get; set; }
        public string ChallanLocCode { get; set; }
        public string VehicleNo { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public decimal TotalToPay { get; set; }
        public decimal TotalCrossing { get; set; }
        public decimal TotalDoorDelyChrg { get; set; }
        public decimal NetPayable { get; set; }
        public string Cancel { get; set; }
        public string PaymentFlag { get; set; }
        public string CancelledBy { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string TotalBulky { get; set; }
        public string BILLENTRY_Y_N { get; set; }
        public string BILLNO { get; set; }
        public string Remarks { get; set; }
        public decimal Totaldeductions { get; set; }
        public decimal TotalNetPayables { get; set; }
        public string ChallanDate_str { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
    }
}