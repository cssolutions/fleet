﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Cust_Service_Mapping
    {
        public string Updateby { get; set; }
        public int SrNo { get; set; }
        public string Service_Code { get; set; }
        public string Service_Name { get; set; }
        public string Entryby { get; set; }
        public string Active { get; set; }
        public DateTime Entrydt { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CUSTCD { get; set; }
    }
}