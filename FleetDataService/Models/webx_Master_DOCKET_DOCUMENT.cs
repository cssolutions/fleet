﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace CYGNUS.Models
{
   
    public class webx_Master_DOCKET_DOCUMENT
    {
        public string DOCKNO { get; set; }
        public string comments { get; set; }
        public string DOCKSF { get; set; }
        public int SRNO { get; set; }
        public string DOCUMENTNO { get; set; }
    }
}

