﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_Incoming_THC
    {
        public string RouteCode { get; set; }
        public string Status { get; set; }
        public string THCNO { get; set; }
        public string Route { get; set; }
        public string ManualTHCNo { get; set; }
        public DateTime ETA { get; set; }
        public string THCDate_ddmmYYYY { get; set; }
        public string ATA { get; set; }
        public decimal OPENKM { get; set; }
        public DateTime THCDT { get; set; }
        public string ATD { get; set; }
        public string PreviousBranch { get; set; }
        public string THCDate { get; set; }
        public int Stoppage_Time_Mins { get; set; }
        public string Vehicle { get; set; }
        public string NextLocation { get; set; }
        public string Unloder{ get; set; }
        public decimal CLOSEKM { get; set; }
        public DateTime AD { get; set; }
        public string AT { get; set; }
        public string LAR { get; set; }
        public string ISN { get; set; }
        public string IR { get; set; }
        public string Seal_Reason { get; set; }

        public decimal LoadingCharge { get; set; }
        public string RateType { get; set; }
        public string LoadingBy { get; set; }
        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }
        public string VendorCode { get; set; }
        public decimal CHRGWT { get; set; }
        public decimal PKGSNO { get; set; }
        public string VendorName { get; set; }
        public decimal hdnRate { get; set; }
        public bool IsMonthly { get; set; }
        public string MonthlyRate { get; set; }

        public bool IsMathadi { get; set; }
        public string MathadiSlipNo { get; set; }
        public DateTime MathadiDate { get; set; }
        public decimal MathadiAmt { get; set; }

        public decimal LastCloseKM { get; set; }
    }
}