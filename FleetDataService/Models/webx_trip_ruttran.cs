﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_trip_ruttran
    {
        public string STTIME_HR { get; set; }
        public string TRTIME_MIN { get; set; }
        public string RUTCD { get; set; }
        public string ONW_RET { get; set; }
        public string STTIME_MIN { get; set; }
        public decimal DIST_KM { get; set; }
        public int RUTNO { get; set; }
        public string LOCCD { get; set; }
        public string TRTIME_HR { get; set; }
    }
}