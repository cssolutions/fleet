﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class PDC_attached_vendor_master
    {
        public int Id { get; set; }
        public string Transmode { get; set; }
        public string Origin { get; set; }
        public string DestCode { get; set; }
        public string VehicleSize { get; set; }
        public string Ratetype { get; set; }
        public string RateID { get; set; }
        public string BRCD { get; set; }
        public decimal Maxlimite { get; set; }
        public decimal Rate { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        public decimal ConRate { get; set; }
        public string Route { get; set; }
        public string type { get; set; }
        public bool IsActive { get; set; }
    }
}