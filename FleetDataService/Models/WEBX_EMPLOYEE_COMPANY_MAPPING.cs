﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class WEBX_EMPLOYEE_COMPANY_MAPPING
    {
        public string EMP_CODE { get; set; }
        public string COMPANY_LIST { get; set; }
        public string ACTIVEFLAG { get; set; }
        public decimal SRNO { get; set; }
        public DateTime ENTRYDT { get; set; }
        public string DEFAULT_COMPANY { get; set; }
        public string ENTRYBY { get; set; }
    }
}