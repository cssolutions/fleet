﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_FAALLOT_DET
    {
        public bool CheckBox { get; set; }
        public string UPDTBY { get; set; }
        public string ENTRYBY { get; set; }
        public string sale_flag { get; set; }
        public string assgnempcd { get; set; }
        public string loccode { get; set; }
        public int trans_cnt { get; set; }
        public string next_loccaddress { get; set; }
        public string RCPLSrNo { get; set; }
        public DateTime trans_fromdt { get; set; }
        public string ENTRYON { get; set; }
        public DateTime sale_date { get; set; }
        public string DEPRATE { get; set; }
        public string assetcd { get; set; }
        public string nextloc_dept { get; set; }
        public DateTime UPDTON { get; set; }
        public DateTime trans_todt { get; set; }
        public string trans_brcd { get; set; }
        public string grpcd { get; set; }
        public string allotcd { get; set; }
        public string loc_add { get; set; }
        public string MFGSrNo { get; set; }
        public string Assign { get; set; }
        public DateTime instdate { get; set; }
        public string assetname { get; set; }
        public double sale_value { get; set; }
        public string activeflag { get; set; }
        public double billamt { get; set; }
        public string description_detail { get; set; }
        public string FIXEDASSETCD { get; set; }
        public string DEPmethod { get; set; }
        public string next_loccode { get; set; }
        //new  Add
        public string Branch { get; set; }
        public string IteamCode { get; set; }
        public string Qty { get; set; }
        public string  DumpToLocation { get; set; }
        public string DumpType { get; set; }
        public string SaleToWhom { get; set; }
        public decimal Amount { get; set; }
        public string ReassignToLoc { get; set; }
        public string Employee { get; set; }
        public string Description { get; set; }
        public DateTime fixedassetdt { get; set; }
        public string DpartName { get; set; }
        public DateTime InstallDate { get; set; }



        public string allotedcd { get; set; }

    }
}