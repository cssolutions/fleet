﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_DriverIncentiveTSP_Expense_HDR
    {
        public string EmpName { get; set; }
        public string BankAccNumber { get; set; }
        public string IFSCCode { get; set; }
        public string EmpCode { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Brcd { get; set; }
        public string BillNo { get; set; }
        public DateTime EntryDate { get; set; }
        public string UpdateBy { get; set; }
        public string NameOFPayee { get; set; }
        public string DocumentNo { get; set; }
        public int ExpenceType { get; set; }
        public int NoOfManifest { get; set; }
        public string BankName { get; set; }
        public string Approved_By { get; set; }
        public string EntryBy { get; set; }
        public int ID { get; set; }
        public string ReferenceNo { get; set; }
        public string SecondEmpolyeeName { get; set; }
        public decimal Weight { get; set; }
        //public DateTime BR_Date { get; set; }
        public string SecondEmpolyeeCode { get; set; }
        public string Type_Of_Staff { get; set; }
        public string DriverName { get; set; }

        public string DriverMobileNo { get; set; }
        //public int ExpenceType { get; set; }

    }
}