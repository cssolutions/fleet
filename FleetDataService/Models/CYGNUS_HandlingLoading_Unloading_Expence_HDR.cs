﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_HandlingLoading_Unloading_Expence_HDR
    {
        public string EmpName { get; set; }
        public int ID { get; set; }
        public int NoOfPackage { get; set; }
        public bool IsApprove { get; set; }
        public string VendorAccNo { get; set; }
        public DateTime EntryDate { get; set; }
        public string EmpCode { get; set; }
        public int ExpenceType { get; set; }
        public int NoOfDocket { get; set; }
        public string UpdateBy { get; set; }
        public string NameOfVendor { get; set; }
        public string DocumentNo { get; set; }
        public string BankName { get; set; }
        public string Approved_By { get; set; }
        public decimal ActualWeight { get; set; }
        public DateTime UpdateDate { get; set; }
        public string BankAccNumber { get; set; }
        public string NameOFPayee { get; set; }
        public string IFSCCode { get; set; }
        public string BillNo { get; set; }
        public string EntryBy { get; set; }
        public DateTime BR_Date { get; set; }
        public string Brcd { get; set; }


    }
}