using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class webx_THC_SUMMARY
    {
        public string thcnoumber { get; set; } 
        public string MFNo { get; set; } 
        public string DockNo { get; set; }
        public DateTime FromDate { get; set; } 
        public DateTime ToDate { get; set; }

        public string THCNO { get; set; } 
        public string THCBR { get; set; } 
        public string THC_Date { get; set; }
        public string Arrival_Date { get; set; }
        public DateTime ETD { get; set; }

        public DateTime THCDT { get; set; } 
        public string Route { get; set; }
        public string Vehno { get; set; }
        public DateTime ATA { get; set; }
        public string SourceHB { get; set; }
        public string ToBH_Code { get; set; }
        public int VFS { get; set; }
        public string sealno_in { get; set; }
        public DateTime ATD { get; set; }
        public string deptime_flight { get; set; }
        public string EmpCode { get; set; }
        public string OutRemarks { get; set; }

        public string tcno { get; set; }
        public string manual_tcno { get; set; }
        public string tcbr { get; set; }
        public DateTime MF_Date { get; set; }
        public Nullable<decimal> tot_dkt { get; set; }
        public string Pkgs { get; set; }
        public string Wts { get; set; }
        public bool IsChecked { get; set; }

        public DateTime TCDT { get; set; }
        public string ManualTHCNo { get; set; }
        public string FilterType { get; set; }
        public string LS { get; set; }
        public string FINAL_TC { get; set; }

        /* Add New Lavnit*/
        public string Docno { get; set; }
        public string doc_dt { get; set; }
        public string Origin_dest { get; set; }
        public string Curr_Next { get; set; }
        public string Deldt { get; set; }
        public string Party { get; set; }
        public int Id { get; set; }
        public decimal pcamt { get; set; }
        public decimal Hdnpcamt { get; set; }
        public decimal ADVAMT { get; set; }
        public decimal HdnADVAMT { get; set; }
        public string fincmplbr { get; set; }
        public string Hdnfincmplbr { get; set; }
        public string balamtbrcd { get; set; }
        public string Hdnbalamtbrcd { get; set; }
        public string FinStatus { get; set; }
        public string routecd { get; set; }
        public string Financial_Edit { get; set; }
        public string CONTTYP { get; set; }
        public string CONTTYP_Name { get; set; }
        public string ContractAmt { get; set; }
        public decimal SCHG01 { get; set; }
        public decimal SCHG02 { get; set; }
        public decimal SCHG03 { get; set; }
        public string chargename { get; set; }
        public string ChargeCode { get; set; }
        public string Type { get; set; }

        public string vendorcode { get; set; }
        public bool IsEnabled { get; set; }
        public string DocType { get; set; }
        public decimal contract_amt { get; set; }




    }
}



