﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class WebX_master_grpbus_mapping
    {
        public string grpcd { get; set; }
        public string entryby { get; set; }
        public DateTime entrydt { get; set; }
        public string businesstype { get; set; }
        public string activeflag { get; set; }
        public int srno { get; set; }
    }
}