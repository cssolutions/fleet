﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_pdchdr
    {
        public string PDCNO { get; set; }
        public string PRSDate { get; set; }
        public string PDCTY { get; set; }
        public string Manualpdcno { get; set; }
        public bool IsEnabled { get; set; }
        public string PDCBR { get; set; }
        public string vendorcode { get; set; }
        public decimal contract_amt { get; set; }
        public string DocType { get; set; }
        public string NO { get; set; }
        public string Typ { get; set; }
        public string Branch { get; set; }
        public string PDCUpdated { get; set; }
    }
}