﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class VW_PFM_ViewPrint
    {
        public string fmdt { get; set; }
        public string Ack_Status { get; set; }
        public string FM_Close { get; set; }
        public string FM_Status { get; set; }
        public string FM_Ack_Status { get; set; }
        public string doc_fwd_to { get; set; }
        public string Ack_Date { get; set; }
        public string Manual_fm_no { get; set; }
        public string locname { get; set; }
        public string loccode { get; set; }
        public string Fur_FWD_loc { get; set; }
        public decimal Total_Documents { get; set; }
        public string loc_cust_code { get; set; }
        public byte fm_doc_type { get; set; }
        public string report_loc { get; set; }
        public string fm_no { get; set; }
        public DateTime fm_date { get; set; }
        public DateTime FM_Rec_Dt { get; set; }
    }
}