﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_custcontract_servicecharges  
    {
        public decimal lowlim_subtot { get; set; }
        public string fuelsurchrgbas { get; set; }
        public decimal lowlim_frt { get; set; }
        public string trans_type { get; set; }
        public decimal cutoff_trdays { get; set; }
        public decimal cutoff_hrs { get; set; }
        public decimal min_frtbasrate { get; set; }
        public string stax_paidby_opts { get; set; }
        public string contractid { get; set; }
        public string stax_paidby_enabled { get; set; }
        public decimal min_subtot_per { get; set; }
        public decimal upplim_frt { get; set; }
        public string frt_disc_ratetype { get; set; }
        public string cft_measure { get; set; }
        public decimal fuelsurchrg { get; set; }
        public string custcode { get; set; }
        public decimal upplim_subtot { get; set; }
        public decimal cft_ratio { get; set; }
        public string stax_paidby { get; set; }
        public decimal max_fuelsurchrg { get; set; }
        public decimal min_frtrate_per { get; set; }
        public decimal frt_disc_rate { get; set; }
        public string min_frtbas { get; set; }
        public decimal min_fuelsurchrg { get; set; }
        public decimal cutoff_min { get; set; }
        public bool STaxPaidByEnabled { get; set; }//stax_paidby_enabled

        /* START GST Changes Chirag D */
        public string gstpaidby { get; set; }

        /* END GST Changes Chirag D */
        public bool InvoiceRateApplay { get; set; }
        public decimal InvoiceRate { get; set; }
    }
}