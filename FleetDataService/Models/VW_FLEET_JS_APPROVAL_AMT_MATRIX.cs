﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class VW_FLEET_JS_APPROVAL_AMT_MATRIX
    {
        public int ID { get; set; }
        public decimal LOC_APPROVE_AMT { get; set; }
        public string CC_UserId { get; set; }
        public decimal Job_ID { get; set; }
        public decimal Approved_Id { get; set; }
        public string HRCHYDESC { get; set; }
        public string Approver_UserId { get; set; }
        public string JS_TYPE { get; set; }
    }
}