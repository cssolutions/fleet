﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class WEBX_LOCATION_COMPANY_MAPPING
    {
        public string COMPANY_LIST { get; set; }
        public string LOCCODE { get; set; }
        public string ACTIVEFLAG { get; set; }
        public int SRNO { get; set; }
        public DateTime ENTRYDT { get; set; }
        public string DEFAULT_COMPANY { get; set; }
        public string ENTRYBY { get; set; }

    }
}