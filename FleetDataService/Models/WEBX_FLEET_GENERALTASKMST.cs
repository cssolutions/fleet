﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_GENERALTASKMST
    {
        public string TASKTYP { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public int W_GRPCD { get; set; }
        public string ENTRY_DT { get; set; }
        public string ENTRY_BY { get; set; }
        public string TASKDESC { get; set; }
        public decimal LABOUR_HRS { get; set; }
        public decimal TASKCD { get; set; }

        public string Group { get; set; }
        public string Text { get; set; }
        public int GRPCD { get; set; }
        public string W_GRPDESC { get; set; }
        public string TASKTYPDesc { get; set; }
    }
}