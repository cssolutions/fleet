﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Travelling_Tour_HDR
    {
        public int ID { get; set; }
        public int ExpenceType { get; set; }
        public string BillNo { get; set; }
        public string EmpName { get; set; }
        public string Grade { get; set; }
        public string EmpCode { get; set; }
        public DateTime Date { get; set; }
        public string BankAccNumber { get; set; }
        public string IFSCCode { get; set; }
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public string PeriodFrom { get; set; }
        public string PeriodTo { get; set; }
        public string BRCD { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }
        public DateTime BR_Date { get; set; }

        public string Hotel_Bill_Image { get; set; }
        public string Fare_Bill_Image { get; set; }

        public bool IsOpen { get; set; }
        public string BankName { get; set; }

        public decimal AmountPaidToEmployee { get; set; }
        public decimal AmountPaidFromEmployee { get; set; }
    }
}