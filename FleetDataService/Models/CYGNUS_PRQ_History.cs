﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_PRQ_History
    {
        public int ID { get; set; }
        public string PRQNo { get; set; }
        public int Status { get; set; }
        public string Status_Name { get; set; }
        public string Remarks { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public bool IsFromTab { get; set; }

    }

    public partial class CYGNUS_PRQ_History_Response
    {
        public string MESSAGE { get; set; }
        public int STATUS { get; set; }
    }

}