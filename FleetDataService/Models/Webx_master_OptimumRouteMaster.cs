//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FleetDataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class Webx_master_OptimumRouteMaster
    {
        public int SrNo { get; set; }
        public string OptimumRouteCode { get; set; }
        public string OriginLocation { get; set; }
        public string DeliveryLocation { get; set; }
        public string EnrouteLocations { get; set; }
        public string TransportMode { get; set; }
        public string ActiveFlag { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateOn { get; set; }
    }
}
