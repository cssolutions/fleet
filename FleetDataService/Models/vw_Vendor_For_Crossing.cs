﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_Vendor_For_Crossing
    {
        public string vendorcode { get; set; }
        public string vendorname { get; set; }
        public string contract_YN { get; set; }
    }
}