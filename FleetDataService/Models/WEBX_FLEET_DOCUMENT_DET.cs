﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_DOCUMENT_DET
    {
        public string DOCUMENT_NO { get; set; }
        public DateTime ENTRY_DT { get; set; }
        public DateTime LAST_MODIFIED { get; set; }
        public string APPLICABLE_STATE { get; set; }
        public int DOCU_ID { get; set; }
        public string RENEWAL_AUTU_NAME { get; set; }
        public string REMARKS { get; set; }
        public string DOCU_COST { get; set; }
        public string ENTER_BY { get; set; }
        public string FILENAME { get; set; }
        public DateTime EXPRITY_DT { get; set; }
        public decimal REMINDER_IN_DAYS { get; set; }
        public DateTime START_DT { get; set; }
        public int DOCU_DET_ID { get; set; }
        public string ENTRY_BY { get; set; }
      
        public string DOCU_TYPE { get; set; }
        public int DOCU_TYPE_ID { get; set; }
        public string VEHNO { get; set; }
        public string DocumentType { get; set; }
        public string STNM { get; set; }
        public string Flag { get; set; }
        public int STCD { get; set; }
        public int VEH_INTERNAL_NO { get; set; }
        public int VEHNOActive { get; set; }
        public string VEHNOINActive { get; set; }
        public string DOC_DESC { get; set; }
        public int VEH_NO { get; set; }

        



    }
}