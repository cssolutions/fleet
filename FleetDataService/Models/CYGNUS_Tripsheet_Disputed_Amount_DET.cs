﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Tripsheet_Disputed_Amount_DET
    {
        public string Particular { get; set; }
        public string KMASPERGPS { get; set; }
        public string KMASDRIVERCLAIMED { get; set; }
        public string AMOUNTPAIDAPPROVED { get; set; }
        public decimal AMOUNTCLAIMEDBYDRIVER { get; set; }
        public string DIFFEXCES { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public decimal Amount { get; set; }
        public string REMARK { get; set; }
        public string Reason_For_Rejection { get; set; }
        public int ID { get; set; }
    }
}