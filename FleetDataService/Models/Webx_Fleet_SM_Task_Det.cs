﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Fleet_SM_Task_Det
    {
        public decimal SERVICE_INTERVALS_KMS { get; set; }
        public int Task_Id { get; set; }
        public string TASKTYP { get; set; }
        public string T_UOM { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public decimal ADV_NOTIFICATION_DAY { get; set; }
        public string W_GRPCD { get; set; }
        public decimal ADV_NOTIFACATION_KMS { get; set; }
        public decimal Estimated_Hrs { get; set; }
        public string TASKDESC { get; set; }
        public decimal SERVICE_INTERVALS_DAY { get; set; }
        public DateTime ENTRY_DT { get; set; }
        public int SMTask_Id { get; set; }
        public string ENTRY_BY { get; set; }

        public int Srno { get; set; }
    }
}