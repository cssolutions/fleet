﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CYGNUS_Docket_CustSeries_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BookCode { get; set; }
        public string SrFrom { get; set; }
        public string SrTo { get; set; }
        public string SubCustomer { get; set; }
        public string CustCode { get; set; }
        public string Location { get; set; }
        public string CustomerCode { get; set; }

    }
}