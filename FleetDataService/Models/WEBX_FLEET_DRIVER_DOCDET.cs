﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_DRIVER_DOCDET
    {
        public string PAN_File { get; set; }
        public string Thumb_Impression_YN { get; set; }
        public string Electricity_Bill_File { get; set; }
        public string Thumb_Impression_File { get; set; }
        public string Telephone_Bill_File { get; set; }
        public string Rationcard_YN { get; set; }
        public string VoterId_YN { get; set; }
        public decimal Driver_Id { get; set; }
        public string Address_Verified { get; set; }
        public string Passport_YN { get; set; }
        public string License_Verified { get; set; }
        public string VoterId_File { get; set; }
        public string ID_Passport_File { get; set; }
        public string Driving_lic_File { get; set; }
        public string BankAcc_File { get; set; }
        public DateTime License_Verified_Dt { get; set; }
        public string ID_Passport_YN { get; set; }
        public string Driver_Registration_Form_YN { get; set; }
        public string Driving_lic_YN { get; set; }
        public string BankAcc_YN { get; set; }
        public string PAN_YN { get; set; }
        public string Rationcard_File { get; set; }
        public string Passport_File { get; set; }
        public string Driver_Registration_Form_File { get; set; }
        public string Telephone_Bill_YN { get; set; }
        public decimal Srno { get; set; }
        public string Electricity_Bill_YN { get; set; }
      


    }
}