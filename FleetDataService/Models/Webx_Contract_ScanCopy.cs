﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Contract_ScanCopy
    {
        public int Srno { get; set; }
        public string ContractID { get; set; }
        public DateTime UploadDate { get; set; }
        public string Filename { get; set; }
    }
}