﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_TCTRN_IM
    {
        public int Id { get; set; }
        public string MFNo { get; set; }
        public string IMNo { get; set; }
        public decimal Weight { get; set; }
        public decimal Packages { get; set; }
        public bool IsArrived { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ArrivedBy { get; set; }
        public bool IsStockUpdated { get; set; }
        public DateTime StockUpdatationDate { get; set; }
        public string StockUpdateBy { get; set; }
        public bool IsDelivered { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string DeliveredBy { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
    }
}