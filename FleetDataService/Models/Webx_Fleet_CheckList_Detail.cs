﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{

    public class Webx_Fleet_CheckList_Detail
    {
        public string TripsheetNo { get; set; }
        public string Chk_Cat { get; set; }
        public decimal Chk_ID { get; set; }
        public string Chk_Comments { get; set; }
        public string Category { get; set; }
        public bool chkDone { get; set; }
        public string CheckList { get; set; }

    }
}