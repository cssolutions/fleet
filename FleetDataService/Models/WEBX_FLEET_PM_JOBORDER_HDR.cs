﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class WEBX_FLEET_PM_JOBORDER_HDR
    {
        public string JOB_ORDER_NO { get; set; }
        public DateTime JOB_ORDER_DT { get; set; }
        public string VEHNO { get; set; }
        public string ORDER_TYPE { get; set; }
        public string ORDER_STATUS { get; set; }
        public string SERVICE_CENTER_TYPE { get; set; }
        public string VENDOR_CODE { get; set; }
        public string WS_LOCCODE { get; set; }
        public string PM_SCH_CODE { get; set; }
        public DateTime JOB_ORDER_ACKDT { get; set; }
        public DateTime JOB_ORDER_CLOSEDT { get; set; }
        public decimal KM_READING { get; set; }
        public string VEH_ARRV_STATUS { get; set; }
        public DateTime VEH_ARRV_DT { get; set; }
        public decimal TOT_EST_LABOUR_HRS { get; set; }
        public decimal TOT_EST_LABOUR_COST { get; set; }
        public decimal TOT_EST_PART_COST { get; set; }
        public decimal TOT_ACT_LABOUR_HRS { get; set; }
        public decimal TOT_ACT_LABOUR_COST { get; set; }
        public decimal TOT_ACT_PART_COST { get; set; }
        public DateTime SENDDT_WORKSHOP { get; set; }
        public DateTime RETURNDT_WORKSHOP { get; set; }
        public DateTime ACTRETURNDT_WORKSHOP { get; set; }
        public decimal CLOSE_KM_READING { get; set; }
        public string billed_Yn { get; set; }
        public string billno { get; set; }
        public decimal Estimated_Labour_Hrs { get; set; }
        public decimal TOT_ESTIMATED_COST { get; set; }
        public string JS_BRCD { get; set; }
        public bool JS_Approve { get; set; }
        public string JS_Approve_By { get; set; }
        public DateTime JS_Approve_Dt { get; set; }
        public bool JS_Reject { get; set; }
        public string JS_Reject_By { get; set; }
        public DateTime JS_Reject_Dt { get; set; }
        public decimal TOT_ACTUAL_COST { get; set; }
        public bool JS_Approve_Close { get; set; }
        public string JS_Approve_By_Close { get; set; }
        public DateTime JS_Approve_Dt_Close { get; set; }
        public bool JS_Reject_Close { get; set; }
        public string JS_Reject_By_Close { get; set; }
        public DateTime JS_Reject_Dt_Close { get; set; }
        public string JS_Approved_LocCode { get; set; }
        public string JS_Approved_LocCode_Close { get; set; }
        public string JS_Approval_Remarks { get; set; }
        public string Job_Order_Close_Type { get; set; }
        public string Cancel_Status { get; set; }
        public DateTime Cancel_Actual_Dt { get; set; }
        public DateTime Cancel_Dt { get; set; }
        public string Cancel_Remarks { get; set; }
        public string JS_Approval_Close_Remarks { get; set; }
        public string COMPANY_CODE { get; set; }
        public string EntryBy { get; set; }
        public string Customer_Code { get; set; }
        public string Asset_Type { get; set; }
        public string Unit_Type { get; set; }
        public string Unit_SerialNo { get; set; }

        /* Manually Added Field */
        public string Vehicle_Category { get; set; }
        public string Manufacturer { get; set; }
        public string Model { get; set; }
        public string Vehicle_Type_Category { get; set; }
        public string Height { get; set; }
        public string Current_KM_Reading { get; set; }
        public string Labour_Cost_Per_Hour { get; set; }
        public string Asset_Temp_Min { get; set; }
        public string Asset_Temp_Max { get; set; }
        public string Length { get; set; }
        public string Breadth { get; set; }
        public int SrNo { get; set; }
        public decimal Total_Actual_Labour_Cost { get; set; }
        public decimal Total_Actual_Part_Quantity { get; set; }
        public decimal Total_Actual_Part_Unit { get; set; }
        public decimal Total_Actual_Part_Cost { get; set; }
        public decimal Total_Actual_Job_Cost { get; set; }
        public decimal Total_Estimated_Part_Quantity { get; set; }
        public decimal Total_Estimated_Part_Unit { get; set; }
        public decimal Total_Estimated_Part_Cost { get; set; }
        public string Actual_Date_of_return { get; set; }
        public decimal TOT_EST_LABOUR_HRS_IsClose_Total { get; set; }
        public decimal TOT_EST_LABOUR_COST_IsClose_Total { get; set; }
       
    }
}