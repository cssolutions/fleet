﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class WebX_FM_FWD_DOC_Master
    {
        public string FM_No { get; set; }
        public decimal ID { get; set; }
        public string FM_Close { get; set; }
        public string FM_Status { get; set; }
        public string FM_FWD_LocCode { get; set; }
        public string FM_Ack_Status { get; set; }
        public string Courier_Way_Bill_No { get; set; }
        public string FM_Edit_by { get; set; }
        public string Loc_Cust_Code { get; set; }
        public DateTime FM_Entry_Date { get; set; }
        public DateTime Courier_Way_Bill_Date { get; set; }
        public DateTime FM_Rec_dt { get; set; }
        public decimal Total_Documents { get; set; }
        public DateTime FM_Edit_Date { get; set; }
        public string Manual_FM_No { get; set; }
        public string FM_FWD_CurrYear { get; set; }
        public DateTime FM_Date { get; set; }
        public string Doc_FWD_To { get; set; }
        public byte FM_Doc_Type { get; set; }
        public string Courier_Code { get; set; }
    }
}