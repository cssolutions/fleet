﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_DCR_Register
    {
        public string Alloted_To { get; set; }
        public decimal TOT_LEAF { get; set; }
        public string Alloted_By { get; set; }
        public DateTime Entry_Date_Sys { get; set; }
        public string FROM_TO { get; set; }
        public string Alloted_To_Name { get; set; }
        public string DOC_TYPE { get; set; }
        public string Entry_By { get; set; }
        public string Business_Type { get; set; }
        public DateTime Alloted_On { get; set; }
        public string Suffix { get; set; }
        public string DOC_SR_FROM { get; set; }
        public decimal Used { get; set; }
        public string Status { get; set; }
        public string Alloted_By_Name { get; set; }
        public string Alloted_Type_ID { get; set; }
        public decimal Cancelled { get; set; }
        public int DOC_KEY { get; set; }
        public string Suffix_Path { get; set; }
        public string BookCode { get; set; }
        public int Business_Type_ID { get; set; }
        public string Alloted_Type { get; set; }
        public string Alloted_By_User { get; set; }
        public string Book_Number { get; set; }
        public string DOC_SR_TO { get; set; }
        public string LocationHierarchy { get; set; }
        
        
        public string DOC_Number { get; set; }
        public string Action { get; set; }
        public string AlloteTo { get; set; }
        public string AlloteToName { get; set; }
        public string DOCTYPE { get; set; }
        public decimal DifLEAF { get; set; }
        public int  UsedCount{ get; set; }
        public string MaxSuffix { get; set; }
        public string MaxUsed { get; set; }
        public string NextCode { get; set; }
        public string DOC_NEW_SR_FROM { get; set; }
        public string LastDocNo { get; set; }
        public string AllotedBy { get; set; }
        public string NewSrTo { get; set; }
        public string Alloted_TypeID { get; set; }
        
    }
}