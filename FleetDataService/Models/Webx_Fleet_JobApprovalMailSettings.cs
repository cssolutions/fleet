﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Fleet_JobApprovalMailSettings
    {
        public string From_Address { get; set; }
        public int ID { get; set; }
        public DateTime Update_Dt { get; set; }
        public string From_To { get; set; }
        public string Update_By { get; set; }
        public string SMTP_Server { get; set; }
    }
}