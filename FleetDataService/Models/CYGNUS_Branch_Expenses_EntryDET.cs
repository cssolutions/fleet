﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Branch_Expenses_EntryDET
    {
        public bool CheckBox { get; set; }
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string ManifestNumber { get; set; }
        public string DocketNo { get; set; }
        public string DocumentNo { get; set; }
        public string BillNo { get; set; }
        public decimal ActualWeight { get; set; }
        public int NoOfPKGS { get; set; }
      
        public DateTime ExpensIncurredDate { get; set; }
        public bool PerKGS { get; set; }
        public bool PerPackage { get; set; }
        public decimal StdRate { get; set; }
        public decimal RateCharged { get; set; }
        public decimal PortelPaidPerBranch { get; set; }
        public decimal OtherTSPEtc { get; set; }
        public decimal Marker { get; set; }
        public decimal HandlingInsideThePlatForm { get; set; }
        public decimal TotalClaimMade_ExlHandling { get; set; }
        public decimal TotalClaim_IncHandling { get; set; }
        public decimal PerKGCost_PKG { get; set; }
        public decimal PerKGCost_Weight { get; set; }
        public bool IsApprove { get; set; }
        public string ApproveBy { get; set; }
        public DateTime ApproveDate { get; set; }
        public bool IsReject { get; set; }
        public string RejectBy { get; set; }
        public DateTime RejectDate { get; set; }
        public string RejectReason { get; set; }

        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }


        public string NameOfStation { get; set; }
        public string PorterType { get; set; }

        public string SubExpense_Type { get; set; }

        public int DKTCount { get; set; }
        public string Brcd { get; set; }

        public string Image { get; set; }

    }
}