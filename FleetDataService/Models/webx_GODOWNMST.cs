﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_GODOWNMST
    {
        public string ActiveFlag { get; set; }
        public string godown_name { get; set; }
        public DateTime UPDTON { get; set; }
        public string SPDBRCD { get; set; }
        public decimal godown_srno { get; set; }
        public string UPDTBY { get; set; }
        public string godown_desc { get; set; }
    }
}