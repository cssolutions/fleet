﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_BR_Advance_Entry
    {
        public decimal PENDAMT { get; set; }
        public int ID { get; set; }
        public string Against_Ledger { get; set; }
        public string Advance_Paid_Name { get; set; }
        public string UpdateBy { get; set; }
        public decimal Advance_Amount { get; set; }
        public string BRCD { get; set; }
        public string DocumentNo { get; set; }
        public string ISCancel { get; set; }
        public string Advance_Paid_Type { get; set; }
        public string VENDOR_CODE { get; set; }
        public string PayMode { get; set; }
        public DateTime EntryDate { get; set; }
        public string Selected_Ledger { get; set; }
        public string Payment_Voucher { get; set; }
        public string Reverse_Accode { get; set; }
        public string VoucherNo { get; set; }
        public string Advance_Paid_Code { get; set; }
        public DateTime ChqDate { get; set; }
        public DateTime UpdateDate { get; set; }
        public string ChqNo { get; set; }
        public string Reverse_Acc_Voucher { get; set; }
        public string EntryBy { get; set; }
        public bool IsAdjustedAdvanceVoucher { get; set; }
        public string VEHNO { get; set; }
        public decimal AdvanceVoucherPendingAmount { get; set; }

        public string Advance_Paid_By { get; set; }

    }
}