﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
namespace FleetDataService.Models
{
    public class WEBX_FLEET_SPAREPARTDET
    {
        public string Vendor_Part_No { get; set; }
        public decimal Cost { get; set; }
        public string Manufacturer { get; set; }
        public string Vendorcode { get; set; }
        public string Part_ID { get; set; }
        public string Leadtime { get; set; }
        public int Srno { get; set; }
    }
}