﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_DCR_Header
    {
        public string doc_type { get; set; }
        public int DOC_KEY { get; set; }
        public string Entry_By { get; set; }
        public string Suffix { get; set; }
        public DateTime Entry_Date_Sys { get; set; }
        public string Suffix_Path { get; set; }
        public string Active { get; set; }
        public DateTime Entry_Date_Manual { get; set; }
        public string DOC_SR_TO { get; set; }
        public int Cancelled { get; set; }
        public int BUSINESS_TYPE_ID { get; set; }
        public string DOC_SR_FROM { get; set; }        
        public int TOT_LEAF { get; set; }
        public int TotalLeaf { get; set; }
        public string Locatoin { get; set; }
        public string Page { get; set; }
       
        public string series_from { get; set; }       
        public int leafs { get; set; }
        public string trans_status { get; set; }
        public string trans_description { get; set; }
        public string allote_to { get; set; }
        public string bookcode { get; set; }

        public string CustCode { get; set; }
        public bool IsCustomerAssign { get; set; }

    }
}