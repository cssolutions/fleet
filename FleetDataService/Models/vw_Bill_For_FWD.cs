﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_Bill_For_FWD
    {
        public string manualbillno { get; set; }
        public string Doc_ack_status { get; set; }
        public string doc_fwd_to { get; set; }
        public string billsubbrcd { get; set; }
        public string DocumentNo { get; set; }
        public string fm_ack_status { get; set; }
        public DateTime bgndt { get; set; }
        public string PFM_LOC { get; set; }
        public DateTime bsbdt { get; set; }
        public string ptmsnm { get; set; }
        public string loc_cust_code { get; set; }
        public string View_st { get; set; }
        public string billno { get; set; }
        public string FWD_Cust_Rest { get; set; }
        public string ptmscd { get; set; }
        public string Scaned { get; set; }
        public string DocumentName { get; set; }
        public string loc { get; set; }
        public byte fm_doc_type { get; set; }
        public string dkt { get; set; }
        public string FWD_LOC_Rest { get; set; }
        public decimal billamt { get; set; }
        public string Fur_FWD_loc { get; set; }

        public bool isChecked { get; set; }
    }
}