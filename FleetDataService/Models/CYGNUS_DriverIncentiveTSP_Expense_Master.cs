﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_DriverIncentiveTSP_Expense_Master
    {
        public int Id { get; set; }
        public string Route { get; set; }
        public string DistanceKm { get; set; }
        public string VehicleType { get; set; }
        public string Runninghoursinclhalting1stprize { get; set; }
        public string IncentiveINCLNormalIncentiveSpecial { get; set; }
        public string Runninghoursinclhalting2secondprize { get; set; }
        public decimal IncentiveAMT { get; set; }
        public string PenaltyPerHRDeductionFromthc { get; set; }
        public bool IsChecked { get; set; }
    }
}