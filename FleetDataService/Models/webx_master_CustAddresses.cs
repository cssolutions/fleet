﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_master_CustAddresses
    {
        public string Address { get; set; }
        public string City { get; set; }
        public string AddID { get; set; }
        public string ActiveFlag { get; set; }
        public string Email { get; set; }
        public DateTime UpdateDate { get; set; }
        public decimal srno { get; set; }
        public string Pincode { get; set; }
        public string UpdateBy { get; set; }
        public string PhoneNo { get; set; }

    }
}