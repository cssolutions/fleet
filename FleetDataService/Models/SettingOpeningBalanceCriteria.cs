﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class SettingOpeningBalanceCriteria
    {
        //public string RdSelect { get; set; }
        public string Selectedtype { get; set; }

        public string CustEffectiveAccount { get; set; }

        public string CustLocationName { get; set; }

        public string CustomerName { get; set; }

        public string EffectiveVendorType { get; set; }

        public string VendorLocationName { get; set; }

        public string VendorName { get; set; }

        public string EmployeeEffectiveAccount { get; set; }
        public string EmployeeLocationName { get; set; }
        public string EmployeeName { get; set; }


        public string DriverEffectiveAccount { get; set; }
        public string DriverLocationName { get; set; }
        public string DriverName { get; set; }
    }

    public class OpeningBalanceCriteriaList
    {
        public int SrNo { get; set; }
        
        public string acctval { get; set; }

        public string custval { get; set; }

        public string custcode { get; set; }

        public string custname { get; set; }

        public decimal opendebit { get; set; }

        public decimal opencredit { get; set; }

        public bool IsChecked { get; set; }
        public string AccountCode { get; set; }

        public string AccountName { get; set; }

    }
}