﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Tea_Expense_Master
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string BRCD { get; set; }
        public string Mapped { get; set; }
        public decimal Amount { get; set; }
        public int ID { get; set; }
    }
}