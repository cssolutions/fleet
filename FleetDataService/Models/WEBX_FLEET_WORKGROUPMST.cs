﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_WORKGROUPMST
    {
        public int W_GRPCD { get; set; }
        public string BW_GRPDESCCD { get; set; }
        public string W_GRPDESC { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public string ENTRY_BY { get; set; }
        public string ENTRY_DT { get; set; }
        public string Asset_Type { get; set; }
        public string Asset_Type_desc { get; set; }
        public string Value { get; set; }
        public string Text { get; set; }
    }
}