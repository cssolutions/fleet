﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_BA_ServiceTax
    {
        public DateTime EndDate { get; set; }
        public decimal CessRate { get; set; }
        public int SrNo { get; set; }
        public decimal SBRate { get; set; }
        public decimal KKCRate { get; set; }
        public decimal StaxRate { get; set; }
        public DateTime StartDate { get; set; }
        public decimal HCessRate { get; set; }

    }
}