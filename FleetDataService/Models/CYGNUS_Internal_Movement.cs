﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Internal_Movement
    {
        public int Id { get; set; }
        public string IMNo { get; set; }
        public DateTime IMDate { get; set; }
        public string ManualNo { get; set; }
        public string ORGNCD { get; set; }
        public string DESTCD { get; set; }
        public string Curr_Loc { get; set; }
        public string Next_Loc { get; set; }
        public decimal Weight { get; set; }
        public decimal Packages { get; set; }
        public string MFNo { get; set; }
        public DateTime MFDate { get; set; }
        public string THCNo { get; set; }
        public DateTime THCDate { get; set; }
        public string Status { get; set; }
        public bool IsArrived { get; set; }
        public DateTime ArrivalDate { get; set; }
        public string ArrivedBy { get; set; }
        public bool IsStockUpdated { get; set; }
        public DateTime StockUpdatationDate { get; set; }
        public string StockUpdateBy { get; set; }
        public bool IsDelivered { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string DeliveredBy { get; set; }
        public bool IsCancelled { get; set; }
        public DateTime CancellationDate { get; set; }
        public string CancelledBy { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string Remarks { get; set; }
        public string Contents { get; set; }
        public bool IsChecked { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
}