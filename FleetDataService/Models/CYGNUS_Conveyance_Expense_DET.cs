﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Conveyance_Expense_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BillNo { get; set; }
        public string Employee { get; set; }
        public DateTime Date { get; set; }
        public string Client_Party_Name_Visiting_Place { get; set; }
        public string Contract_Person { get; set; }
        public string Contract_Num_Concerned { get; set; }
        public string DocumentNo { get; set; }
        public string Travel_Mode { get; set; }
        public string MFrom { get; set; }
        public string MTo { get; set; }
        public string Distance_Covered { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }
        public bool CheckBox { get; set; }
        public string Reason_Purpose_Visting { get; set; }
        public string OppNonOpp_Type { get; set; }
        public int Subtype { get; set; }

        public string Opening { get; set; }
        public string Closing { get; set; }
        public decimal Travel_Mode_Rate { get; set; }
        public string ConveyanceExp_Image { get; set; }
    }
}