﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_ENROUTE_EXP
    {
        public string TripsheetNo { get; set; }
        public string id { get; set; }
        public decimal Amount_Spent { get; set; }
        public string BillNo { get; set; }
        public string Dt { get; set; }
        public decimal Exe_Appr_Amt { get; set; }
        public string Remarks { get; set; }
        public string Polarity { get; set; }
        public int SRNO { get; set; }
        public decimal Total { get; set; }
    }
}