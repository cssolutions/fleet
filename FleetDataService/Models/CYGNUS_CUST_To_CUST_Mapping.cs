﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CYGNUS_CUST_To_CUST_Mapping
    {
        public int ID { get; set; }
        public string CustCode { get; set; }
        public string SubCustCode { get; set; }
    }
}