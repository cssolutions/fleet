﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace CYGNUS.Models
{
    /// <summary>
    /// Generated Class for Table : webx_GENERAL_POASSET_det.
    /// </summary>
    public class webx_GENERAL_POASSET_det
    {
        public DateTime podate { get; set; }
        public string ENTRYBY { get; set; }
        public string narration { get; set; }
        public decimal balanceqty { get; set; }
        public string ExAccDesc { get; set; }
        public string UnitType { get; set; }
        public DateTime ENTRYdt { get; set; }
        public decimal total { get; set; }
        public decimal DiscountAmt { get; set; }
        public string activeflag { get; set; }
        public decimal TaxAmt { get; set; }
        public Int64 srno { get; set; }
        public decimal Discount_Per { get; set; }
        public string voucherNo { get; set; }
        public string assetcd { get; set; }
        public string comment { get; set; }
        public DateTime PAYDT { get; set; }
        public decimal qty { get; set; }
        public double tax_per { get; set; }
        public decimal rate { get; set; }
        public string location { get; set; }
        public decimal LastRate { get; set; }
        public string TyreTypeCode { get; set; }
        public string TyreMFG_Code { get; set; }
        public string UPDTBY { get; set; }
        public string pocode { get; set; }
        public string TyreSizeCode { get; set; }
        public string ExAccCode { get; set; }
        public DateTime UPDTON { get; set; }
        public string tax_type { get; set; }
        public decimal ReceivedQTY { get; set; }
        public string FromSrNo { get; set; }
        public string ToSrno { get; set; }
        public decimal totqty { get; set; }
        public decimal WarrantyInYear { get; set; }
        public decimal Other_Chg { get; set; }
        public decimal GSTPer { get; set; }
        public decimal GSTAmt { get; set; }

        public string HSN_SAC { get; set; }
        public decimal taxableAmt { get; set; }
    }
}