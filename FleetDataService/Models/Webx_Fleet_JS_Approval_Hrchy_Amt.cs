﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Fleet_JS_Approval_Hrchy_Amt
    {
        public int ID { get; set; }
        public decimal LOC_APPROVE_AMT { get; set; }
        public decimal LOC_APPROVE_AMT_BANK { get; set; }
        public string LOC_HRCHY_CODE { get; set; }
        public string UPDATE_BY { get; set; }
        public DateTime UPDATE_DT { get; set; }
        public DateTime ENTRY_DT { get; set; }
        public string JS_TYPE { get; set; }
        public string ENTRY_BY { get; set; }
        public string CODEDESC { get; set; }
    }
}