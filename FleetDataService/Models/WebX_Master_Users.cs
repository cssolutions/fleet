﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class WebX_Master_Users
    {
        //public string UserId { get; set; }
        //public string User_Type { get; set; }
        //public string UserPwd { get; set; }
        //public string BranchCode { get; set; }
        //public string Name { get; set; }
        //public string PasswordQues { get; set; }
        //public string PasswordAns { get; set; }
        //public string EmpId { get; set; }
        //public string ManagerId { get; set; }
        //public string EmailId { get; set; }
        //public string PhoneNo { get; set; }
        //public Nullable<System.DateTime> ActiveTillDate { get; set; }
        //public Nullable<System.DateTime> PwdLastChangeOn { get; set; }
        //public string LastPwd { get; set; }
        //public string Status { get; set; }
        //public System.DateTime EntryDate { get; set; }
        //public string EntryBy { get; set; }
        //public System.DateTime LastUpdatedDate { get; set; }
        //public string LastUpdatedBy { get; set; }
        //public string mobileno { get; set; }
        //public string gender { get; set; }
        //public string resi_addr { get; set; }
        //public System.DateTime DOB { get; set; }
        //public Nullable<System.DateTime> DOJ_ORG { get; set; }
        //public string ROLEID { get; set; }
        //public string IsPassEncrypted { get; set; }
        //public string emptype { get; set; }
        //public string ispassreset { get; set; }
        //public string USERGROUP { get; set; }
        //public string WebrefNo { get; set; }
        //public string IsFieldOfficer { get; set; }
        //public Nullable<bool> IsExpert { get; set; }
        //public bool IsDistancesurveyor { get; set; }
        //public string IsPOApproval { get; set; }
        //public Nullable<decimal> POApprovalAmt { get; set; }
        //public string IsWithoutPMPOApproval { get; set; }
        //public Nullable<decimal> IsWithoutPMPOApprovalAmt { get; set; }
        //public string PersonalNo { get; set; }
        //public Nullable<bool> IsPackingSupervisor { get; set; }
        //public Nullable<bool> IsPackers { get; set; }
        //public Nullable<bool> IsDelivers { get; set; }
        //public Nullable<bool> IsLabours { get; set; }
        //public Nullable<bool> IsCommercial { get; set; }


        public string UserId { get; set; }
        public string User_Type { get; set; }
        public string UserPwd { get; set; }
        public string BranchCode { get; set; }
        public string Name { get; set; }
        public string PasswordQues { get; set; }
        public string PasswordAns { get; set; }
        public string EmpId { get; set; }
        public string ManagerId { get; set; }
        public string EmailId { get; set; }
        public string PhoneNo { get; set; }
        public Nullable<System.DateTime> ActiveTillDate { get; set; }
        public Nullable<System.DateTime> PwdLastChangeOn { get; set; }
        public string LastPwd { get; set; }
        public string Status { get; set; }
        public System.DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public System.DateTime LastUpdatedDate { get; set; }
        public string LastUpdatedBy { get; set; }
        public string mobileno { get; set; }
        public string gender { get; set; }
        public string resi_addr { get; set; }
        public System.DateTime DOB { get; set; }
        public Nullable<System.DateTime> DOJ_ORG { get; set; }
        public string ROLEID { get; set; }
        public string IsPassEncrypted { get; set; }
        public string IsPassReset { get; set; }
        public string emptype { get; set; }

        public string confirmPassword { get; set; }
        public string Name_Of_bank { get; set; }
        public string Bank_AC_Number { get; set; }
        public string IFSC_Code { get; set; }
        public string Designation { get; set; }
        public string User_Image { get; set; }
        public string Department { get; set; }
        public string HOD { get; set; }
        public string Grade { get; set; }
        public decimal ConveyanceExpance { get; set; }
        public string Message { get; set; }

        public string BrachName { get; set; }
        public string Menu_1 { get; set; }
        public string Menu_2 { get; set; }
        public string Menu_3 { get; set; }

        public string Category { get; set; }

        public string Read_Witre { get; set; }
        public string Change_Menu_Rights { get; set; }
        public string Change_Reports_Rights { get; set; }
        public string Reset_Password { get; set; }
        public string Block_User { get; set; }
        public string Action_Url { get; set; }


    }

    public partial class Webx_View_Track
    {
        public string UserId { get; set; }
        public string ReportId { get; set; }
        public string chacked { get; set; }
        public string Level0Text { get; set; }
        public string Level1Text { get; set; }
        public string DOC_Called_AS { get; set; }
        public string L0_App_Module { get; set; }
        public decimal L1_App_Module { get; set; }
        public string App_Module { get; set; }
        public bool New_chacked { get; set; }
    }

    public partial class ModuleAcessModel
    {
        public string UserID { get; set; }
        public string Chk { get; set; }
        public string Name { get; set; }
        public string BranchCode { get; set; }
        public string HasAccess { get; set; }
        public bool New_chacked { get; set; }
        public string Text { get; set; }
        public string App_Module { get; set; }
        public string Level1Text { get; set; }
        public string Level2Text { get; set; }
        public string Level3Text { get; set; }
    }
}