﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_Pincode_Loc_Master
    {
        public string Area { get; set; }
        public int SrNo { get; set; }
        public string Service_Type { get; set; }
        public string EntryBy { get; set; }
        public string ActiveFlag { get; set; }
        public string LocCode { get; set; }
        public int Pincode { get; set; }
        public DateTime Entrydt { get; set; }

    }
}