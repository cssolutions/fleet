﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_BILLMST
    {
        public string PTMSBRCD { get; set; }
        public string commitbill { get; set; }
        public string Bill_UnSubmit_By { get; set; }
        public string BILLSUBTYPE { get; set; }
        public double SUPBILLAMT { get; set; }
        public string BCBSEMPCD { get; set; }
        public string BBRNM { get; set; }
        public string bankacccode { get; set; }
        public DateTime BSBDT { get; set; }
        public double rentalcharg { get; set; }
        public int H_cess_rate { get; set; }
        public decimal RoundOffAmt { get; set; }
        public double vehdetchrg { get; set; }
        public DateTime BDATETO { get; set; }
        public string PTMSEMAIL { get; set; }
        public double chqamt { get; set; }
        public string PTMSADDR { get; set; }
        public string PTMSNM_OLD { get; set; }
        public double othchrg { get; set; }
        public string BADDEBTS { get; set; }
        public DateTime BDATEFROM { get; set; }
        public decimal SBCRATE { get; set; }
        public string UTY_NAME { get; set; }
        public int PENDAMT { get; set; }
        public double BILLTDS { get; set; }
        public string PTMSNM { get; set; }
        public string BILLAO { get; set; }
        public string COMPANY_CODE { get; set; }
        public double cess_rate { get; set; }
        public string Finalized { get; set; }
        public DateTime LastEditDate { get; set; }
        public string paytype { get; set; }
        public DateTime finclosedt { get; set; }
        public string REMARK { get; set; }
        public string spe_inst { get; set; }
        public double CESSAMT { get; set; }
        public DateTime BCBSDT { get; set; }
        public string WRITEOFFCOMMENT { get; set; }
        public DateTime ProposalDelyDt { get; set; }
        public string UTY_CD { get; set; }
        public double warechrg { get; set; }
        public double clamt { get; set; }
        public string SEQNo { get; set; }
        public DateTime BGNDT { get; set; }
        public decimal SBCAMT { get; set; }
        public DateTime BCANDT { get; set; }
        public string SUBTOTEL { get; set; }
        public DateTime BPRINTDTF { get; set; }
        public DateTime Aggrementdt { get; set; }
        public DateTime BPRINTDTL { get; set; }
        public string Businesstype { get; set; }
        public double svctax_rate { get; set; }
        public string BILLCOLBRCD { get; set; }
        public double SVRCAMT { get; set; }
        public string BILLNO { get; set; }
        public DateTime chqdt { get; set; }
        public string CANCOMMENT { get; set; }
        public string STax_Y_N { get; set; }
        public string BILLSTATUS { get; set; }
        public string BGENEMPCD { get; set; }
        public string AggrementNo { get; set; }
        public double hedu_cess { get; set; }
        public string bankaccdesc { get; set; }
        public DateTime BDUEDT { get; set; }
        public string BILLSUBBRCD { get; set; }
        public double dischrg { get; set; }
        public string BCOLEMPCD { get; set; }
        public DateTime entrydt { get; set; }
        public string chqno { get; set; }
        public string AcctGNEffect { get; set; }
        public string BSUBEMPCD { get; set; }
        public string OLD_BILL { get; set; }
        public string PTMSCD_OLD { get; set; }
        public string BCANEMPCD { get; set; }
        public string BBRCD { get; set; }
        public string AutoBillGenerate { get; set; }
        public string banknm { get; set; }
        public DateTime BCLDT { get; set; }
        public string SUPBILLNOFOR { get; set; }
        public int Commission_Charges { get; set; }
        public string BILLSUBTO { get; set; }
        public string closebill { get; set; }
        public double billded { get; set; }
        public string PTMSCD { get; set; }
        public string Narration { get; set; }
        public double handchrg { get; set; }
        public decimal BILLAMT { get; set; }
        public string BILLCOLPTCD { get; set; }
        public DateTime Bill_UnSubmit_On { get; set; }
        public string TRN_MOD { get; set; }
        public string PTMSTEL { get; set; }
        public string PAYBAS { get; set; }
        public string manualbillno { get; set; }
        public string BILL_CANCEL { get; set; }
        public string BGNDT1 { get; set; }
        public string BRANCH { get; set; }
        public string PARTY { get; set; }


        /* Add By Chirag D */
        public string ptmsstr { get; set; }
        public bool IsBillSubmitted { get; set; }
        public string Dockno { get; set; }
        public string Bbrcdnm { get; set; }
        public decimal Loading { get; set; }
        public decimal UnLoading { get; set; }
        public decimal Detention { get; set; }
        public decimal Other { get; set; }
        public decimal Penalty { get; set; }
        public bool IsMRGenerated { get; set; }

        public decimal BILAMT { get; set; }

        /* Add By jemin  */
        public string BILLTYPE { get; set; }
        public DateTime DATEFROM { get; set; }
        public DateTime DATETO { get; set; }
        public string CancelRession { get; set; }
        public string BILLDT { get; set; }
        public string location { get; set; }
        public Decimal Netamt { get; set; }
        public string betype { get; set; }
        public string vendname { get; set; }
        public bool IsChecked { get; set; }
        public int SrNo { get; set; }
        public string SubLocation { get; set; }
        public string ExiBilLocation { get; set; }
        public string NewLocation { get; set; }
        public string LocCode { get; set; }
        public string LocName { get; set; }

        public decimal Col_Amt { get; set; }
        public decimal UNEXPDED { get; set; }
        public decimal TDSDED { get; set; }
        public decimal Freight_Deduction { get; set; }
        public decimal Claim_Deduction { get; set; }
        public decimal Other_Amount { get; set; }
        public string BGNDTStr { get; set; }
        public string BillSubDate { get; set; }
        public string CHGCODE { get; set; }
        public string Custcd { get; set; }

        /* Add By keyur  */
        public string POC { get; set; }
        public decimal CNAMT { get; set; }
        public decimal AccAmt { get; set; }

        public decimal KKCRATE { get; set; }
        public decimal KKCAMT { get; set; }

        public bool IsTDSApplicable { get; set; }
        public int iSNO { get; set; }

        public decimal maxTDSAmt { get; set; }

        public string DocImage { get; set; }
    }
}