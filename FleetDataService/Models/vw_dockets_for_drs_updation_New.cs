﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class vw_dockets_for_drs_updation_New
    {
        public int AutoNo { get; set; }
        public string dockno { get; set; }//IsAvilable
        public string docksf { get; set; }
        public string Booking_Date { get; set; }
        public string orgncd { get; set; }
        public string destcd { get; set; }
        public string PayBasis { get; set; }
        public string csgncd { get; set; }
        public string csgnnm { get; set; }
        public string csgecd { get; set; }
        public string csgenm { get; set; }
        public int Pkgs_Arrived { get; set; }
        public decimal Pkgs_Booked { get; set; }
        public decimal Pkgs_Pending { get; set; }
        public decimal Booked_Wt { get; set; }
        public decimal Wt_Arrived { get; set; }
        public string Comm_Dely_Dt { get; set; }
        public decimal Freight { get; set; }
        public decimal Docket_Total { get; set; }
        public decimal Service_Tax { get; set; }
        public string DelyLocation { get; set; }
        public string curr_loc { get; set; }
        public string PayBasCode { get; set; }
        public DateTime DockDt { get; set; }
        public string COD_DOD { get; set; }
        public decimal CODDODAmount { get; set; }
        public string CDELDT_ddmmyyyy { get; set; }
        public string DockDt_ddmmyyyy { get; set; }
        public string dlypdcno { get; set; }
        public bool CODDOD { get; set; }
        public int? PKGSDELIVERED { get; set; }
        public string remark { get; set; }
        public DateTime DELYDATE { get; set; }
        public DateTime DELYTIME { get; set; }
        public string DELYPERSON { get; set; }
        public string cboReason { get; set; }
        public int CODDODCOLLECTED { get; set; }
        public int CODDODNO { get; set; }
        public string cboLateReason { get; set; }        
        public string HDcboReason { get; set; }
        public bool IsChecked { get; set; }


        public decimal PkgQty { get; set; }
        public decimal ActQty { get; set; }

        public decimal Rate { get; set; }
        public decimal MaxLimit { get; set; }

        public decimal NewRate { get; set; }
        public string ratetype { get; set; }
        public bool IsEnabled { get; set; }
    }
}