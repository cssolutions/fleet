﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_PM_JOBORDER_DET
    {
        public string JOB_ORDER_NO { get; set; }
        public string PM_SCH_CODE { get; set; }
        public string W_GRPCD { get; set; }
        public decimal W_EST_LABOUR_HRS { get; set; }
        public decimal W_EST_LABOUR_COST { get; set; }
        public decimal W_ACT_LABOUR_HRS { get; set; }
        public decimal W_ACT_LABOUR_COST { get; set; }
        public decimal Taskcd { get; set; }
        public string T_Remarks { get; set; }
        public DateTime Task_Copmletion_dt { get; set; }
        public string tasktype_code { get; set; }
        public string TASK_ACTIONDESC { get; set; }
        public string Task_Completed { get; set; }
        public string AMC { get; set; }
        public string SMTask { get; set; }
        public int TaskTypeId { get; set; }
        public int SRNo { get; set; }

        public string ISClose { get; set; }
    }
}