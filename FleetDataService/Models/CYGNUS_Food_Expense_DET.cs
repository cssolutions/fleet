﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Food_Expense_DET
    {
        public int ID { get; set; }
        public int ReferenceNo { get; set; }
        public string BillNo { get; set; }
        public DateTime Date { get; set; }
        public int Number_Of_Staff { get; set; }
        public string Reason { get; set; }
        public string Staff_Member_Name { get; set; }
        public string DocketNo { get; set; }
        public string Party_Location_Name { get; set; }
        public string Concerning_Docket_PRS_DRS_THC_No { get; set; }
        public decimal Rate_Per_Day { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }
        public int Subtype { get; set; }
        public string FoodImageName { get; set; }
        
    }
}