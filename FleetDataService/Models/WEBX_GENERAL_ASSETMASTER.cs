﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_GENERAL_ASSETMASTER
    {
        public string PREFIX { get; set; }
        public string ACCDESC { get; set; }
        public DateTime ENTRYON { get; set; }
        public string ENTRYBY { get; set; }
        public string ACTIVE { get; set; }
        public string UPDTBY { get; set; }
        public string ASSETNAME { get; set; }
        public DateTime UPDTON { get; set; }
        public decimal srno { get; set; }
        public string ASSETCD { get; set; }
        public string ACCCODE { get; set; }

    }
}