﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Cust_Location_Mapping
    {
        public string Updateby { get; set; }
        public decimal SrNo { get; set; }
        public string Entryby { get; set; }
        public string LocCode { get; set; }
        public string Active { get; set; }
        public DateTime Entrydt { get; set; }
        public DateTime UpdateDate { get; set; }
        public string CUSTCD { get; set; }
    }
}