﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class webx_RateMatrix
    {
        public string Rate { get; set; }
        public string FromZone { get; set; }
        public string Type { get; set; }
        public string ToZone { get; set; }
        public string TransitDays { get; set; }

        public string Origin { get; set; }
        public string Destination { get; set; }
        public string Weight { get; set; }
    }
}