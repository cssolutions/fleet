﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_Stock_For_Update
    {
        public string CDELYDT { get; set; }
        public string sourcehb { get; set; }
        public string THCBR { get; set; }
        public string Octroi_IO { get; set; }
        public string vehno { get; set; }
        public string THCNO { get; set; }
        public decimal PKGSNO { get; set; }
        public string Service_Class { get; set; }
        public string BusinessType { get; set; }
        public DateTime THCDT { get; set; }
        public string MF_ToHub { get; set; }
        public string routecd { get; set; }
        public string DelyProcessCode { get; set; }
        public string BizType { get; set; }
        public string routename { get; set; }
        public string DockSF { get; set; }
        public decimal BKG_PKGSNO { get; set; }
        public string CODDOD { get; set; }
        public decimal CODDODAmount { get; set; }
        public string ORGNCD { get; set; }
        public string TCBR { get; set; }
        public string IsBCProcess { get; set; }
        public int ManifestKount { get; set; }
        public string DEST_CD { get; set; }
        public decimal ACTUWT { get; set; }
        public decimal BKG_ACTUWT { get; set; }
        public int DocketKount { get; set; }
        public string TCNO { get; set; }
        public DateTime actarrv_dt { get; set; }
        public string DockNo { get; set; }
        public string THC_NextLoc { get; set; }
        public string DOCKDT { get; set; }

        public string AC { get; set; }
        public string WI { get; set; }
        public string DP { get; set; }
        public DateTime UpdateDate { get; set; }

        public bool DelyOnStockUpdate { get; set; }
        public string DelyOnStockUpdateChar { get; set; }
        public string Pickup_Dely { get; set; }
        public bool Octroi_ProcChg_Applicable { get; set; }
        public string Octroi_ProcChg_ApplicableChar { get; set; }
        public bool IsCODDOD { get; set; }
        public string IsCODDODChar { get; set; }
        public string DELYREASON { get; set; }
        public string DELYPERSON { get; set; }
        public int CODDODCOLLECTED { get; set; }

        public bool IsShort { get; set; }
        public bool IsDamage { get; set; }
        public bool IsPilferage { get; set; }
        public bool IsExcess { get; set; }
        public int ArrPkgQty { get; set; }
        public int DelPkgQty { get; set; }

        public decimal ShortageQty { get; set; }
        public decimal ShortageWeight { get; set; }
        public string ShortageReason { get; set; }
        public string ShortageRemarks { get; set; }

        public decimal PilferageQty { get; set; }
        public decimal PilferageWeight { get; set; }
        public string PilferageReason { get; set; }
        public string PilferageRemarks { get; set; }

        public decimal DamageQry { get; set; }
        public decimal DamageWeight { get; set; }
        public string DamageReason { get; set; }
        public string DamageRemarks { get; set; }

        public decimal ExcessQty { get; set; }
        public decimal ExcessWeight { get; set; }
        public string ExcessReason { get; set; }
        public string ExcessRemarks { get; set; }

        public bool ISCounterDelivery { get; set; }
    }
}