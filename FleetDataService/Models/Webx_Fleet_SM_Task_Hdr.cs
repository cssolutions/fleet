﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Fleet_SM_Task_Hdr
    {
        public DateTime Entry_Date { get; set; }
        public string Entry_By { get; set; }
        public string VEH_TYPE_CODE { get; set; }
        public int SMTask_Id { get; set; }
        public string VEH_TYPE_CODEValue { get; set; }
    }
}