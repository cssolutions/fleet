using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    /// <summary>
    /// Generated Class for Table : WEBX_FLEET_VEHICLE_ISSUE.
    /// </summary>
    public class WEBX_FLEET_VEHICLE_ISSUE
    {
        public decimal f_closure_fill { get; set; }
        public string EntryByLoc { get; set; }
        public decimal SP_VrAmt { get; set; }
        public decimal Tonnage { get; set; }
        public decimal c_dedamt { get; set; }
        public string VehicleNo { get; set; }
        public string Cancel_Remarks { get; set; }
        public decimal TotAdvExp { get; set; }
        public string TRIP_TYPE { get; set; }
        public string Oper_dt_Updated { get; set; }
        public string c_TDStype { get; set; }
        public decimal f_issue_rate { get; set; }
        public string Orgn_Phone { get; set; }
        public string SP_CrVrno { get; set; }
        public decimal c_Advamt_paid { get; set; }
        public string Cleaner_Roster_YN { get; set; }
        public string From_City { get; set; }
        public decimal SP_CrVrAmt { get; set; }
        public decimal c_net_amt { get; set; }
        public string DriverSettleDt { get; set; }
        public string driver1_Op_Bal { get; set; }
        public decimal f_issue_amt { get; set; }
        public decimal DIESEL_QTY_CF { get; set; }
        public string Destn_Email { get; set; }
        public string Transit_Time { get; set; }
        public DateTime Entrydt { get; set; }
        public decimal e_issue_totamt { get; set; }
        public string Destn_Addr { get; set; }
        public decimal e_closure_balamt { get; set; }
        public string ReqId { get; set; }
        public string rut_code { get; set; }
        public decimal Contractamt { get; set; }
        public string Billed_YN { get; set; }
        public string VehiclePath { get; set; }
        public string Market_Own { get; set; }
        public decimal e_issue_fuel { get; set; }
        public string ReqBy { get; set; }
        public decimal c_Othercharg { get; set; }
        public string CheckedBy { get; set; }
        public string Return_Load { get; set; }
        public decimal e_closure_totamt { get; set; }
        public decimal Approved_KMPL { get; set; }
        public DateTime VSlipDt { get; set; }
        public String Vslipdate { get; set; }
        public string Request_Type { get; set; }
        public decimal f_issue_startkm { get; set; }
        public string AuditedBy { get; set; }
        public string Last_Arrived_LOC { get; set; }
        public decimal Orgn_Pin { get; set; }
        public string Driver1_Roster_YN { get; set; }
        public decimal No_Passenger { get; set; }
        public string SP_Vrno { get; set; }
        public DateTime Close_Entrydt { get; set; }
        public DateTime Cancel_Entry_Dt { get; set; }
        public string RTO2 { get; set; }
        public decimal VehSize { get; set; }
        public string Driver2_Name { get; set; }
        public decimal Revenue_Capture { get; set; }
        public string expacc_entry { get; set; }
        public string Custcode { get; set; }
        public decimal f_closure_amt { get; set; }
        public string Manual_TripsheetNo { get; set; }
        public string Cancel_Status { get; set; }
        public decimal c_issue_tot { get; set; }
        public string billno { get; set; }
        public DateTime Oper_Close_Dt { get; set; }
        public decimal AFL_Weight { get; set; }
        public string Driver_Assistant_Id { get; set; }
        public decimal e_net_amt { get; set; }
        public string END_DT_TM { get; set; }
        public decimal Total_Weight { get; set; }
        public string Destn_City { get; set; }
        public decimal c_TDSrate { get; set; }
        public string COMPANY_CODE { get; set; }
        public string VSlipNo { get; set; }
        public decimal c_Balamt_paid { get; set; }
        public string Orgn_Addr { get; set; }
        public decimal TotClaimsAmt { get; set; }
        public string Orgn_LocCode { get; set; }
        public DateTime ReqDt { get; set; }
        public decimal e_issue_spare { get; set; }
        public decimal e_closure_deposit { get; set; }
        public string Driver_Assistant_Name { get; set; }
        public string Externalusage_Category { get; set; }
        public decimal TotSpareExp { get; set; }
        public string TripSheet_StartLoc { get; set; }
        public string Orgn_Email { get; set; }
        public decimal TotOilExp { get; set; }
        public string FUEL_SLIP_FLAG { get; set; }
        public decimal VehType { get; set; }
        public decimal TotIncDedExp { get; set; }
        public string ApprovedBy { get; set; }
        public string Entryby { get; set; }
        public decimal e_issue_advamt { get; set; }
        public string RTO1 { get; set; }
        public string Category { get; set; }
        public string PreparedBy { get; set; }
        public DateTime finupdt_dt { get; set; }
        public decimal f_issue_fill { get; set; }
        public string finupdt_by { get; set; }
        public string Licno2 { get; set; }
        public decimal Destn_Pin { get; set; }
        public decimal e_closure_dedamt { get; set; }
        public decimal e_closure_incentive { get; set; }
        public decimal Actual_KMPL { get; set; }
        public string THC_Attached_YN { get; set; }
        public string cur_loc { get; set; }
        public decimal f_closure_closekm { get; set; }
        public string Licno1 { get; set; }
        public string DOCKNO { get; set; }
        public string TripSheet_EndLoc { get; set; }
        public string TripSheet_EndLocName { get; set; }
        public string Trip_Route_Type { get; set; }
        public decimal c_TDSamt { get; set; }
        public decimal f_closure_rate { get; set; }
        public string Trip_Adv_Det_Flag { get; set; }
        public string Close_Entryby { get; set; }
        public decimal TotEnrouteExp { get; set; }
        public string Destn_Phone { get; set; }
        public string Route { get; set; }
        public decimal UFL_Weight { get; set; }
        public string TripBillingWise { get; set; }
        public decimal e_issue_comm { get; set; }
        public decimal DIESEL_AMT_CF { get; set; }
        public decimal TotRepairExp { get; set; }
        public decimal c_closure_tot { get; set; }
        public string Destn_LocCode { get; set; }
        public decimal ReqCatId { get; set; }
        public string To_City { get; set; }
        public DateTime Validdt1 { get; set; }
        public string Set_UnSet { get; set; }
        public string Vehicle_Mode { get; set; }
        public string Driver1_Name { get; set; }
        public string DriverName { get; set; }
        public string DRIVERNAMENew { get; set; }
        
        public string TripBillingType { get; set; }
        public string Driver2 { get; set; }
        public string Orgn_City { get; set; }
        public string Driver1 { get; set; }
        public DateTime Validdt2 { get; set; }
        public DateTime Cancel_Dt { get; set; }
        public DateTime Start_dt_tm { get; set; }
        public string Driver2_Roster_YN { get; set; }
        public string STATUS { get; set; }

        /* Add Field */
        public string License_No { get; set; }
        public DateTime Valdity_dt { get; set; }
        //public decimal Start_km { get; set; }
        //public decimal Closing_Km { get; set; }
        public decimal Fuel_Filled_enroute { get; set; }
        public decimal Total_Kms { get; set; }
        public decimal ApprovedDiesel { get; set; }
        public decimal Average_Diesel_Rate { get; set; }
        public decimal Approved_Fuel_Amount { get; set; }
       // public string driver_name { get; set; }
        public string TS_Close_Dt { get; set; }
        public string vehno { get; set; }
        //public string driver1 { get; set; }
        public string rutdesc { get; set; }
        public string VehicleType { get; set; }
        public decimal KMPL { get; set; }
        public decimal AvgDieselRate { get; set; }

        public decimal current_KM_Read { get; set; }


        //LAVNIT
        public string NextDocumentCode { get; set; }
        public bool ChkslipProvide { get; set; }
        public string In_driver_Name1 { get; set; }
        public string In_driver_Name2 { get; set; }
        public string In_driver_Name { get; set; }
        public string Valditydt { get; set; }

        public string settelDate { get; set; }
        public string VSlipDtstr { get; set; }
        public string Valdity_dtstr { get; set; }


        public string finYear { get; set; }
        public string yearSuffix { get; set; }
        public string CompanyCode { get; set; }

        public string Oper_Close_Dt_S { get; set; }

        //jemin
        public string Value { get; set; }
        public string Text { get; set; }
        //public string VEHICLENO { get; set; }
        //public string VSLIPNO { get; set; }
        //public string MANUAL_TRIPSHEETNO { get; set; }
        public string ISSUESTATUS { get; set; }

        public string Driver1_MobileNo { get; set; }
        public string Driver2_MobileNo { get; set; }

        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public string Mode { get; set; }


        public string ReassignDriver { get; set; }

        public string RouteList { get; set; }
        public string Routbased { get; set; }

        public string Transit_HH { get; set; }

        public string Transit_MM { get; set; }
        public decimal LossDiesel { get; set; }


    }
}