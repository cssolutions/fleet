//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FleetDataService.Models
{
    using System;
    using System.Collections.Generic;
    using System.Xml.Serialization;

    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class WEBX_FLEET_TYREPOS
    {
        public int TYREPOS_ID { get; set; }
        public string TYREPOS_CODE { get; set; }
        public string TYREPOS_DESC { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public string VEHICLE_TYPE { get; set; }
        public Nullable<System.DateTime> TYREPOS_ENTRYDT { get; set; }
        public string POS_ALLOWED { get; set; }
        public string TRUCK_TRAILER { get; set; }
        public string COMPANY_CODE { get; set; }
    }
}
