﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_Delivery_Zone
    {
        public string ZoneDescription { get; set; }
        public DateTime EntryDate { get; set; }
        public string ActiveFlag { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string Location { get; set; }
        public string UpdateBy { get; set; }
        public int ZoneID { get; set; }
        public string ZoneName { get; set; }
        public string Pincodes { get; set; }

    }
}