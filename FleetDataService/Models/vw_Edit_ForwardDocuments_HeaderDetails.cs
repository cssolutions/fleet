﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_Edit_ForwardDocuments_HeaderDetails
    {
        public string Courier_Way_Bill_No { get; set; }
        public string FM_Date { get; set; }
        public string Courier_Way_Bill_Date { get; set; }
        public string Manual_FM_No { get; set; }
        public string FM_Entry_Date { get; set; }
        public string FM_No { get; set; }
        public string Loc_Cust_Code { get; set; }
        public string Courier_Code { get; set; }
        public string Doc_Fwd_To { get; set; }
    }
}