﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_Conveyance_Expense_HDR
    {
        public int ID { get; set; }
        public string BillNo { get; set; }
        public int ExpenceType { get; set; }
        public string Branch_Name { get; set; }
        public int Milometer_Reading { get; set; }
        public string EmpName { get; set; }
        public string Opening { get; set; }
        public string Destination { get; set; }
        public string Closing { get; set; }
        public string Department { get; set; }
        public string Mobile_Number { get; set; }
        public string HOD_Name { get; set; }
        public string Total_Business { get; set; }
        public string Claim_For_Month { get; set; }
        public string Bank_Name { get; set; }
        public string EmpCode { get; set; }
        public string BankAccNumber { get; set; }
        public string IFSCCode { get; set; }
        public string Labour_Name { get; set; }
        public string BRCD { get; set; }
        public DateTime EntryDate { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

        public string ConveyanceExp_Image { get; set; }
        public string OppNonOpp_Type { get; set; }
        public string TripSheetNo { get; set; }
        public DateTime BR_Date { get; set; }
        public decimal Expence_Max_Limit { get; set; }
        public decimal Total_Expence { get; set; }
        public decimal MaxLimit { get; set; }
        public string MSGValidate { get; set; }
    }
}