﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WebX_Master_Service
    {
        public string ServiceManager { get; set; }
        public int SrNo { get; set; }
        public string Service_Code { get; set; }
        public string EntryBy { get; set; }
        public DateTime UpdateOn { get; set; }
        public string Active { get; set; }
        public string UpdateBy { get; set; }
        public string ServiceName { get; set; }
        public DateTime EntryOn { get; set; }
    }
}