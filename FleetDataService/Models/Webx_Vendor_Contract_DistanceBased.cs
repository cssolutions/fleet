﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Vendor_Contract_DistanceBased
    {
        public int Trips_PM { get; set; }
        public Int64 ID { get; set; }
        public string CONTRACTCD { get; set; }
        public DateTime UpdateDt { get; set; }
        public string Vehicle_Number { get; set; }
        public string VendorCode { get; set; }
        public string EntryBy { get; set; }
        public decimal Min_Amt_Committed { get; set; }
        public string Vehicle_Type { get; set; }
        public string FTL_Type { get; set; }
        public decimal Chg_Per_Add_Km { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateBy { get; set; }
        public decimal Committed_Km { get; set; }
        public decimal Max_Amt_Committed { get; set; }

    }
}