﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WebX_Master_Holiday_Datewise
    {
        public DateTime hday_date { get; set; }
        public string hday_note { get; set; }
        public string hday_loc { get; set; }
        public string RadiohdayType { get; set; }
        public string FlagType { get; set; }
        public bool activeflag { get; set; }
        public string actflag { get; set; }
        public string updateby { get; set; }
        public DateTime updatedate { get; set; }
               
    }
}