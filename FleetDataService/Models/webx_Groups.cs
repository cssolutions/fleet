﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class webx_Groups
    {
        public string Groupcode { get; set; }
        public string Groupdesc { get; set; }
        public string Parentcode { get; set; }
        public decimal Grouplevel { get; set; }
        public string Acct_prefix { get; set; }
        public string entryby { get; set; }
        public DateTime entrydt { get; set; }
        public string Company_Groupcode { get; set; }
        public decimal orderno { get; set; }
        public string main_category { get; set; }
        public string bstype { get; set; }
        public string bstypeno { get; set; }
       // public string GroupcodeType { get; set; }
    }
}
