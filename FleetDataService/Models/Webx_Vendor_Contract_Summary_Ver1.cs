﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Vendor_Contract_Summary_Ver1
    {
        public string Contract_loccode { get; set; }
        public DateTime Start_Dt { get; set; }
        public string CompWitness { get; set; }
        public DateTime ContractDt { get; set; }
        public string CONTRACTCD { get; set; }
        public string VendorPerDesg { get; set; }
        public string Payment_loc { get; set; }
        public string VendorContractCat { get; set; }
        public string VendorCode { get; set; }
        public string TDSAppl_YN { get; set; }
        public Nullable<DateTime> Security_deposit_date { get; set; }
        public DateTime UpdateDt { get; set; }
        public string VendorWitness { get; set; }
        public string Vendor_Address { get; set; }
        public string CompEmpDesg { get; set; }
        public string VendorPin { get; set; }
        public string Security_deposit_chq { get; set; }
        public string VendorPerName { get; set; }
        public string EntryBy { get; set; }
        public decimal TDS_Rate { get; set; }
        public string Vendor_Type { get; set; }
        public string Payment_interval { get; set; }
        public string Status { get; set; }
        public string VendorCity { get; set; }
        public DateTime EntryDt { get; set; }
        public decimal Security_deposit_Amt { get; set; }
        public string Payment_Basis { get; set; }
        public decimal Default_Charge { get; set; }
        public string CompEmpName { get; set; }
        public string VendorName { get; set; }
        public string VendorCategory { get; set; }
        public string UpdateBy { get; set; }
        public decimal Monthly_Phone_Charges { get; set; }
        public DateTime Valid_uptodt { get; set; }
        public string MetrixType { get; set; }
        public string ContractType { get; set; }
        public string Flag { get; set; }

        public string contract_YN { get; set; }
        //public string vendor_type { get; set; }
        public string VendorTypeName { get; set; }

        /*Added for Franchies*/
        public decimal FTLFixAmount { get; set; }
        public string Local_Feeder_Rate_Type { get; set; }
        public decimal Local_Feeder_Rate { get; set; }
        //public decimal CommissionUptoKM { get; set; }
        //public decimal ExtraKMCharegeStart { get; set; }
        //public decimal Rate_ExtraKM { get; set; }
        public bool ODAPickupApply { get; set; }
        public decimal ODAPickupStartKM { get; set; }
        public string ODAPickupRateType { get; set; }
        public decimal ODAPickupRate { get; set; }

        public bool ODADeliveryApply { get; set; }
        public decimal ODADeliveryStartKM { get; set; }
        public string ODADeliveryRateType { get; set; }
        public decimal ODADeliveryRate { get; set; }
        public string BookingService { get; set; }
        public string DeliveryService { get; set; }
        public string BookingRateType { get; set; }
        public string DeliveryRateType { get; set; }
        public decimal BookingRate { get; set; }
        public decimal DeliveryRate { get; set; }
    }
}