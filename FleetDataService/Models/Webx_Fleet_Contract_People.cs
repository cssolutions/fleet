﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class Webx_Fleet_Contract_People
    {
        public string From_Area { get; set; }
        public decimal RUTKM { get; set; }
        public int ID { get; set; }
        public int SrNo { get; set; }
        public string Deployment_St_Time { get; set; }
        public decimal Variable_KM { get; set; }
        public string Contract_Code { get; set; }
        public decimal Outstanding_Charge { get; set; }
        public string PickDrop_Id { get; set; }
        public string Entry_By { get; set; }
        public string Customer_Code { get; set; }
        public string Category_Id { get; set; }
        public DateTime Update_Date { get; set; }
        public decimal Minimum_Head { get; set; }
        public string Veh_Category_Id { get; set; }
        public string Deployment_End_Time { get; set; }
        public string Company_Code { get; set; }
        public string To_Area { get; set; }
        public string Contract_Type_Id { get; set; }
        public decimal Fixed_KM { get; set; }
        public decimal Rate { get; set; }
        public decimal Fixed_Hour { get; set; }
        public decimal No_of_Days { get; set; }
        public decimal No_of_Vehicle { get; set; }
        public string Duration_Id { get; set; }
        public string Active_Flag { get; set; }
        public decimal Variable_Hour { get; set; }
        public DateTime Entry_Date { get; set; }
        public string RUTCD { get; set; }
        public int Vehicle_Type { get; set; }
        public string Update_By { get; set; }
        public string Branch_Code { get; set; }
        public decimal Non_Working_Charge { get; set; }

        
    }
}