﻿using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class webx_Acccode_Details
    {
        public string Accdesc { get; set; }
        public string Entryby { get; set; }
        public string Narration { get; set; }
        public string Voucherno { get; set; }
        public DateTime Entrydt { get; set; }
        public string opertitle { get; set; }
        public string COMPANY_CODE { get; set; }
        public int Credit { get; set; }
        public string ManualNo { get; set;}
        public string ReferenceNo { get; set; }
        public string ChequeNo { get; set; }
        public DateTime Chequedate { get; set; }
        public int Credit1 { get; set; }
        public int finyear { get; set; }
        public string Acccode { get; set; }
        public string Acccode1 { get; set; }
        public int Debit { get; set; }
        public string Debit1 { get; set; }
        public string Voucher_Cancel { get; set; }
        public DateTime Transdate { get; set; }
        public string Brcd { get; set; }
        public int Srno { get; set; }
        public string comment { get; set; }

        public bool chkRefno { get; set; }
        public string PayMode { get; set; }
        public string PayMode1 { get; set; }
        public string ChequeNo1 { get; set; }
        public DateTime Chequedate1 { get; set; }

        /*-- For Voucher BackDate Access -- */
        public Cygnus_Master_Voucher_BackDate_Access BackDateAccess { get; set; }

    }
}