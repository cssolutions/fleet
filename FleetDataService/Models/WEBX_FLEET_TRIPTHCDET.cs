﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_FLEET_TRIPTHCDET
    {
        public int SRNO { get; set; }
        public string TripSheetNo { get; set; }
        public string Custcode { get; set; }
        public string FromCity { get; set; }
        public string ToCity { get; set; }
        public string THCNo { get; set; }
        public string THCDt { get; set; }
        public decimal FreightAmt { get; set; }
        public decimal LabourAmt { get; set; }
        public decimal OtherAmt { get; set; }
        public decimal TotalAmt { get; set; }
    }
}