﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet.Models
{
    public class CYGNUS_DriverIncentiveTSP_Expense_DET
    {
        public bool CheckBox { get; set; }
        public string Second_ST_Prize_Amount_Hours { get; set; }
        public string MTo { get; set; }
        public string Name_Of_Driver { get; set; }
        public string MFrom { get; set; }
        public DateTime ApproveDate { get; set; }
        public string BillNo { get; set; }
        public string ApproveBy { get; set; }
        public string Hours_AsPer_Erp { get; set; }
        public string GoogleHours { get; set; }
        public int Second_ST_Prize { get; set; }
        public string ExpenseType { get; set; }
        public string ManifestNumber { get; set; }
        public string RejectReason { get; set; }
        public decimal AmountPaid_Per_Branch { get; set; }
        public string RejectBy { get; set; }
        public int One_ST_Prize { get; set; }
        public int Third_ST_Prize { get; set; }
        public bool IsReject { get; set; }
        public string Driver_Contract_Number { get; set; }
        public DateTime RejectDate { get; set; }
        public decimal Distance { get; set; }
        public decimal GoogleDistance { get; set; }
        public string Third_ST_Prize_Amount_Hours { get; set; }
        public string One_ST_Prize_Amount_Hours { get; set; }
        public int ID { get; set; }
        public int ReferenceNo { get; set; }

        public decimal Amount_Passed { get; set; }
        public decimal Amount_Rejected { get; set; }
        public string Reason_For_Rejection { get; set; }
        public string DocketNo { get; set; }
        public decimal Weight { get; set; }
        public decimal Driver_Id { get; set; }
        public int Hours_Per_Dest_BRCD { get; set; }
        public string Route { get; set; }
    }
}