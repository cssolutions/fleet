﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class VW_Fleet_CheckList_Module
    {
        public string CheckList { get; set; }
        public decimal Chk_ID { get; set; }
        public string Chk_Cat { get; set; }
        public string Category { get; set; }
    }
}