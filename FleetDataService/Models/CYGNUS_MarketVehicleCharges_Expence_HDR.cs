﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Fleet .Models
{
    public class CYGNUS_MarketVehicleCharges_Expence_HDR
    {
        public decimal Unit_Cost_Pased_Amount { get; set; }
        public string EmpName { get; set; }
        public int ID { get; set; }
        public decimal Unit_Cost_Charged_Amount { get; set; }
        public decimal Total_Number_Of_Pkts { get; set; }
        public DateTime EntryDate { get; set; }
        public string EmpCode { get; set; }
        public string DocumentNo { get; set; }
        public int ExpenceType { get; set; }
        public decimal Total_Amount_Passed { get; set; }
        public decimal Total_Amount { get; set; }
        public string UpdateBy { get; set; }
        public string BankName { get; set; }
        public DateTime UpdateDate { get; set; }
        public string BankAccNumber { get; set; }
        public string NameOFPayee { get; set; }
        public string IFSCCode { get; set; }
        public string BillNo { get; set; }
        public string Brcd { get; set; }
        public string EntryBy { get; set; }
        public decimal Total_Weight { get; set; }
        public DateTime BR_Date { get; set; }

    }
}