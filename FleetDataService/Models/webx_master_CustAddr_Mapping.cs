﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class webx_master_CustAddr_Mapping
    {
        public decimal SrNo { get; set; }
        public string AddID { get; set; }
        public string ActiveFlag { get; set; }
        public string CustCD { get; set; }
        public DateTime UpdateDate { get; set; }
        public string UpdateBy { get; set; }

    }
}