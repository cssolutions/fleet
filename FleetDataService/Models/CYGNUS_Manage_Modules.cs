﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Manage_Modules
    {
        public string Caption { get; set; }
        public string Header { get; set; }
        public int SrNo { get; set; }
        public bool IsRequired { get; set; }
        public int ModuleCode { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public string FieldCode { get; set; }
        public bool IsUsed { get; set; }

    }
}