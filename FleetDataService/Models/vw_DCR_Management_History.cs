﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_DCR_Management_History
    {
        public decimal SRNO { get; set; }
        public string FROM_TO { get; set; }
        public string Alloted_To_Name { get; set; }
        public string DOC_TYPE { get; set; }
        public string Action_Date { get; set; }
        public string Suffix { get; set; }
        public string DOC_SR_FROM { get; set; }
        public DateTime EntryDate { get; set; }
        public string Alloted_To { get; set; }
        public int Split_To_Key { get; set; }
        public int DOC_KEY { get; set; }
        public string Alloted_Loc { get; set; }
        public string Alloted_Loc_Name { get; set; }
        public string EntryBy { get; set; }
        public string BookCode { get; set; }
        public string Action { get; set; }
        public string Alloted_Type { get; set; }
        public string Book_Number { get; set; }
        public string DOC_SR_TO { get; set; }
        public string Alloted_Type_Desc { get; set; }
        public string Action_Desc { get; set; }
        public string EntryByName { get; set; }

    }
}