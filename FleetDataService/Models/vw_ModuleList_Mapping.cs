﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_ModuleList_Mapping
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string Service_Code { get; set; }
    }
}