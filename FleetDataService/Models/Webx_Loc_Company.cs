﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class Webx_Loc_Company
    {
        public string COMPANY_CODE { get; set; }
        public string COMPANY_NAME { get; set; }
        public string CHECKYN { get; set; }
        public string DEFAULTYN { get; set; }
    }
}