﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WebX_Master_Module
    {
        public int SrNo { get; set; }
        public DateTime EntryOn { get; set; }
        public string ModuleManager { get; set; }
        public string Service_Code { get; set; }
        public string EntryBy { get; set; }
        public string ModuleName { get; set; }
        public DateTime UpdateOn { get; set; }
        public string Active { get; set; }
        public string UpdateBy { get; set; }
        public string Module_Code { get; set; }
    }
}