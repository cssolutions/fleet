﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class WEBX_TRIPSHEET_OILEXPDET
    {
        public string TripSheetNo { get; set; }
        public string Place { get; set; }
        public string KM_Reading { get; set; }
        public string BillNo { get; set; }
        public DateTime BillDt { get; set; }
        public string Diesel_Ltr { get; set; }
        public string Oil_Ltr { get; set; }
        public string Amount { get; set; }
        public string THCno { get; set; }
        public string Brand { get; set; }
        public string Category { get; set; }
        public string Last_Km_Read { get; set; }
        public string Card_Cash { get; set; }
        public decimal Diesel_Rate { get; set; }
        public decimal Oil_Rate { get; set; }
        public string PetrolPName { get; set; }
        public string FuelType { get; set; }
        public string Remark { get; set; }
        public string EXE_AMT { get; set; }
        public DateTime EntryDate { get; set; }
        public string Currloc { get; set; }
        public string vendor_billno { get; set; }
        public string COMPANY_CODE { get; set; }
        public int IOCL_ID { get; set; }
        public string Diesel_Upload_YN { get; set; }

        public int SRNO { get; set; }
    }
}