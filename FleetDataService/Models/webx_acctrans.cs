﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_acctrans
    {
        public int transNo { get; set; }
        public string Payto { get; set; }
        public string servicetaxNo { get; set; }
        public string UpdateBy { get; set; }
        public DateTime Updateon { get; set; }
        public string DocNo { get; set; }
      
        public string COSTCENTER { get; set; }
        public string Bookcode { get; set; }
        public string autoentry { get; set; }
        public DateTime Chqdate { get; set; }
        public string coastName { get; set; }
        public string Deptcode { get; set; }
        public string Chqno { get; set; }
        public string Transtype { get; set; }
        public string Entryby { get; set; }
        public DateTime lasttransDate { get; set; }
        public string Narration { get; set; }
        public string PBOV_TYP { get; set; }
        public string entryfor { get; set; }
        public string Oppaccount { get; set; }
        public DateTime billduedt { get; set; }
        public DateTime billdt { get; set; }
        public DateTime v_closed_dt { get; set; }
        public string Voucherno { get; set; }
        public DateTime Entrydt { get; set; }
        public string PBOV_CODE { get; set; }
        public string opertitle { get; set; }
        public string Old_Acccode { get; set; }
        public string billno { get; set; }
        public string docksf { get; set; }
        public string Update_Comment10Nov14 { get; set; }
        public string panno { get; set; }
        public string voucher_status { get; set; }
        public DateTime v_approve_reject_dt { get; set; }
        public string Docsf { get; set; }
        public string COMPANY_CODE { get; set; }
        public decimal Credit { get; set; }
        public decimal finyear { get; set; }
        public string v_approve_reject_by { get; set; }
        public string oppacccode { get; set; }
        public string Update_Comment_17Sep14 { get; set; }
        public string v_approve_reject_branch { get; set; }
        public string Acccode { get; set; }
        public string Update_Comment { get; set; }
        public string prepareByLoc { get; set; }
        public string PBOV_NAME { get; set; }
        public decimal Debit { get; set; }
        public string Currbalance { get; set; }
        public string coastCode { get; set; }
        public string Voucher_Cancel { get; set; }
        public string v_closed_branch { get; set; }
        public string ReceivedFrom { get; set; }
        public DateTime Transdate { get; set; }
        //public DateTime transdate { get; set; }
        public string DocType { get; set; }
        public string Brcd { get; set; }
        public DateTime Chqcleardate { get; set; }
        public string v_closed_by { get; set; }
        public decimal Srno { get; set; }
        public string comment { get; set; }
        public string coasttype { get; set; }
        public bool IsChecked { get; set; }
        public DateTime CDate { get; set; }
        public DateTime VoucherEditDate { get; set; }
        public string TripShitNo { get; set; }
       public string AdvancedAmount { get; set; }
       public DateTime canceldt { get; set; }
       public DateTime VoucherDate { get; set; }

    }
}