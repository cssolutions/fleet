﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class Webx_Master_Role
    {
        public decimal srno { get; set; }
        public string Roleid { get; set; }
        public string ROledesc { get; set; }
        public string HYC { get; set; }
        public string Multiple { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string Id { get; set; }
        public bool ACTIVEFLAG1 { get; set; }
        public bool Multiple1 { get; set; }
    }
}