﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{

    public class Webx_Fleet_CheckList_hdr
    {
        public string TripsheetNo { get; set; }
        public string Checked_By { get; set; }
        public DateTime Checked_Dt { get; set; }
        public string Approved_By { get; set; }
        public DateTime Approved_Dt { get; set; }
        public string Company_Code { get; set; }
    }
}