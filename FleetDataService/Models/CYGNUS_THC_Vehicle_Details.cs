//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CYGNUS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CYGNUS_THC_Vehicle_Details
    {
        public string THCNO { get; set; }
        public string THCSF { get; set; }
        public string VehicleNO { get; set; }
        public string VehicleType { get; set; }
        public string FTLType { get; set; }
        public string VehicleCapacity { get; set; }
        public string VehicleSize { get; set; }
        public string Driver1Name { get; set; }
        public string Driver1MobileNo { get; set; }
        public string Driver1RTONo { get; set; }
        public string Driver1Licence { get; set; }
        public Nullable<System.DateTime> Driver1LicenceValDate { get; set; }
        public string Driver2Name { get; set; }
        public string Driver2MobileNo { get; set; }
        public string Driver2RTONo { get; set; }
        public string Driver2Licence { get; set; }
        public Nullable<System.DateTime> Driver2LicenceValDate { get; set; }
        public string DriverPhotoPath { get; set; }
        public Nullable<int> Make { get; set; }
        public Nullable<int> Model { get; set; }
        public Nullable<decimal> VehicleVolume { get; set; }
        public string VehicleColor { get; set; }
        public string CHASISNO { get; set; }
        public string ENGINENO { get; set; }
        public string MODELNo { get; set; }
        public string RCBOOKNO { get; set; }
        public string CertificateNo { get; set; }
        public string InsuranceNo { get; set; }
        public string RTONo { get; set; }
        public Nullable<System.DateTime> RegistrationDate { get; set; }
        public Nullable<System.DateTime> FitnessDate { get; set; }
        public Nullable<System.DateTime> PermitDate { get; set; }
        public Nullable<System.DateTime> InsuranceDate { get; set; }
        public Nullable<decimal> CAPACITY { get; set; }
        public string MarketVehImage { get; set; }
        public string VehicleFileName { get; set; }

        public string tabletNumber { get; set; }
        public string StaffName { get; set; }
        public string StaffMobileNo { get; set; }
        public string VehicleTypeSize { get; set; }
    }
}
