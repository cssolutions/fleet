﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public class CYGNUS_Customer_Contract_Template
    {
        public int ContractID { get; set; }
        public string CustCD { get; set; }
        public string CustNM { get; set; }
        public string CustLOC { get; set; }
        public string BillingADD { get; set; }
        public DateTime ContractDT { get; set; }
        public DateTime ValidTillDT { get; set; }
        public DateTime EffectivefromDT { get; set; }
        public int SegamentType { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string Email { get; set; }
        public string TransitMode { get; set; }
        public string Matrix { get; set; }
        public string RateType { get; set; }
        public string Bill_Gen_Loc { get; set; }
        public string Bill_sub_Loc { get; set; }
        public string Bill_Col_Loc { get; set; }
        public bool Codecharge { get; set; }
        public bool DACCCharge { get; set; }
        public bool ODAPickupchatrge { get; set; }
        public bool ODADeliverychatrge { get; set; }
        public decimal CFT  { get; set; }
        public decimal FOV  { get; set; }

        public decimal CODDOCRate { get; set; }
        public decimal DACCRate { get; set; }
        public decimal ODAPickupRate { get; set; }
        public decimal ODADeliveryRate { get; set; }
        public decimal CreditPeriod { get; set; }
        public decimal CreditLimit { get; set; }
        public string bill_schedule { get; set; }
        public DateTime BillingDt { get; set; }
        public string BillNoType { get; set; }
        public string MonthlyPotential { get; set; }
        public string TANNo { get; set; }
        public decimal BaseFuelPrice { get; set; }
        public bool ZeroRoundoff { get; set; }
        public decimal MinChargablbeWt { get; set; }
        public decimal Volume_Wt_PerCFT { get; set; }
        public decimal GreenSurcharge { get; set; }
        public decimal MinimumFreight { get; set; }
        public bool PenaltyClause { get; set; }
        public string BasefuelIndex { get; set; }
        public string ContractType { get; set; }
        public decimal SlabProtection { get; set; }
        public decimal AnnualIncreamentClause { get; set; }
        public decimal AnnualIncreamentType { get; set; }
        public string EntryBy { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateBy { get; set; }
        public DateTime UpdateDt { get; set; }


    }
}