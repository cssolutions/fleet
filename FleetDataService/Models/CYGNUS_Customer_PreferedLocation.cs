﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_Customer_PreferedLocation
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string PreferedLocation { get; set; }
        public bool IsFromAndroid { get; set; }
        public bool IsActive { get; set; }
        public string EntryBy { get; set; }
        public string BRCD { get; set; }
    }

    public partial class CYGNUS_Customer_PreferedLocationResponse
    {
        public int ID { get; set; }
        public string MESSAGE { get; set; }
        public int STATUS { get; set; }
    }
}