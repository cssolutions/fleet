﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    [XmlRoot("DocumentElement"), XmlType("DocumentElement")]
    public partial class WEBX_COMPANY_MASTER
    {
        public decimal SRNO { get; set; }
        public string COMPANY_CODE { get; set; }
        public string COMPANY_NAME { get; set; }
        public string BRCD { get; set; }
        public string ADDRESS { get; set; }
        public string CONTACT_PERSON { get; set; }
        public decimal CONTACTNO { get; set; }
        public string ACTIVEFLAG { get; set; }
        public string ENTRYBY { get; set; }
        public System.DateTime ENTRYDT { get; set; }
        //public byte[] COMPANY_LOGO { get; set; }
        public string PAN { get; set; }
        public string TAN { get; set; }
        public string ServiceTaxNo { get; set; }
        public string PunchLine { get; set; }
        public string RegistrationNo { get; set; }
        public string TelePhoneNo { get; set; }
        public string FAXNo { get; set; }
        public string HelpLineNo { get; set; }
    }
}