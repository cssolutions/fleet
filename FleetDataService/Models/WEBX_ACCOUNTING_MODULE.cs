﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class WEBX_ACCOUNTING_MODULE
    {
        public int srno { get; set; }
        public string Module_Code { get; set; }
        public string Module_Name { get; set; }
    }
}