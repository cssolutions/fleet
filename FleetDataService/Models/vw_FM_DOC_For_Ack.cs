﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FleetDataService.Models
{
    public class vw_FM_DOC_For_Ack
    {
        public DateTime fm_date { get; set; }
        public decimal Total_Documents { get; set; }
        public string pfmdt { get; set; }
        public string doc_fwd_to { get; set; }
        public Int32 fm_doc_type { get; set; }
        public string fm_no { get; set; }
        public string fm_fwd_loccode { get; set; }
        public string loc_cust_code { get; set; }
        public string Fur_FWD_loc { get; set; }
        public string fm_ack_status { get; set; }
        public string FM_Status { get; set; }
        public string from_to { get; set; }
        public string FM_Close { get; set; }

        public bool active { get; set; }
        public int ID { get; set; }
    }
}