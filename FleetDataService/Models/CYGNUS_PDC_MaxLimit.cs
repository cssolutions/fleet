﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CYGNUS_PDC_MaxLimit
    { 
        public int Id { get; set; }
        public string BRCD { get; set; }
        public decimal RatePerGM { get; set; }
        public string VehicleSize { get; set; }
        //public decimal MaximumLimit { get; set; }
    }
}