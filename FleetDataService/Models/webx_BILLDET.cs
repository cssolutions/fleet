﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class webx_BILLDET
    {
        public int Id { get; set; }
        public double TDS { get; set; }
        public DateTime CDELDT { get; set; }
        public string ORGNCD { get; set; }
        public double oct_cess { get; set; }
        public double FRTDED { get; set; }
        public string Accdesc { get; set; }
        public string DOCKNO { get; set; }
        public string COMMENTS { get; set; }
        public string BILLNO { get; set; }
        public string Narration { get; set; }
        public double CHRGWT { get; set; }
        public double oct_svctax { get; set; }
        public int Total_DAMAMT { get; set; }
        public string REASSIGN_DESTCD { get; set; }
        public double STGCLM { get; set; }
        public double declval { get; set; }
        public int SBCAMT { get; set; }
        public double SVRCAMT { get; set; }
        public double ONAC { get; set; }
        public int CESSAMT { get; set; }
        public double OCT_SVCCHG { get; set; }
        public double DKTCHG { get; set; }
        public double OTHDED { get; set; }
        public double OCT_AMT { get; set; }
        public double LATEDEL { get; set; }
        public int NET_DamAmt { get; set; }
        public int Free_Storage_Days { get; set; }
        public int process_chrg { get; set; }
        public int Total_Storage_Days { get; set; }
        public string DOCKSF { get; set; }
        public double TOTDED { get; set; }
        public double Oct_High_cess { get; set; }
        public int clearance_chrg { get; set; }
        public string Acccode { get; set; }
        public double DMGCLM { get; set; }
        public string DocType { get; set; }
        public double DKTTOT { get; set; }
        public double FOVCHG { get; set; }
        public int Hedu_Cess { get; set; }
        public string OCT_RECEIPTNO { get; set; }
        public decimal oct_percentage { get; set; }
        public DateTime DELYDT { get; set; }
        public int processing_per { get; set; }
        public int Actual_Storage_Days { get; set; }
        public DateTime DOCKDT { get; set; }
        public double totamt { get; set; }
        public DateTime recptdt { get; set; }
        public double demchgs { get; set; }
        public double OTCHG { get; set; }
        public double DKTAMT { get; set; }

        public string TripsheetNo { get; set; }
        public string VehicleNo { get; set; }
        public string THCRoute { get; set; }
        public string THCNo { get; set; }
        public DateTime THCDate { get; set; }
    }
}