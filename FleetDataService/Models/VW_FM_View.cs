﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class VW_FM_View
    {
        public string FM_last_loc { get; set; }
        public string poddt { get; set; }
        public string Orgn_Dest { get; set; }
        public string waybilldate { get; set; }
        public DateTime dockdt { get; set; }
        public string FM_status { get; set; }
        public string dockno { get; set; }
        public string fm_no { get; set; }
        public string courier_way_bill_no { get; set; }
        public DateTime fm_date { get; set; }
        public string FM_close { get; set; }
        public string doc_fwd_to { get; set; }
        public string loccode { get; set; }
        public string fmdt { get; set; }
        public string dlydt { get; set; }
        public string loc_cust_code { get; set; }
        public string Doc_Recieved { get; set; }
        public string doc_status { get; set; }
        public int fm_doc_type { get; set; }
        public string report_loc { get; set; }
        public decimal Total_Documents { get; set; }
        public string fm_fwd_loccode { get; set; }
        public string courier_code { get; set; }
        public string FM_Next_loc { get; set; }
        public string FM_Crrloc { get; set; }
        public string FM_ack_status { get; set; }
    }
}