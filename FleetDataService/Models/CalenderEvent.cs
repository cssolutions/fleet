﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CYGNUS.Models
{
    public class CalenderEvent
    {
        public string Id { get; set; }
        public DateTime StartDt { get; set; }
        public DateTime EndDt { get; set; }
        public string Title { get; set; }
        public string backgroundColor { get; set; }
        public string url { get; set; }
        public string DOCKNO { get; set; }

        public string SuspectID { get; set; }

        public int SDD_Date { get; set; }
        public string ShedualDate { get; set; }
    }
}