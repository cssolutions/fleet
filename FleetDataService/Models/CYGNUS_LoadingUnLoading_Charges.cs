﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class CYGNUS_LoadingUnLoading_Charges
    {
        public bool IsEnabled { get; set; }
        public int ID { get; set; }
        public string DocumentNo { get; set; }
        public string DocumentDate { get; set; }
        public string LoadBY { get; set; }
        public string DocumentType { get; set; }
        public string ChargesType { get; set; }
        public string Status { get; set; }
        /*
        public string BillNo { get; set; }//no
        public string VoucherNo { get; set; }//no
        */
        public string ChargedBy { get; set; }
        public string ChargedByDesc { get; set; }
        public string RateType { get; set; }
        public string RateTypeDesc { get; set; }
        public decimal ChargeRate { get; set; }
        public decimal ChargeAmount { get; set; }
        public string VendorCode { get; set; }
        public string VendorName { get; set; }
        /*
        public DateTime EntryDate { get; set; }//no
        public string EntryBy { get; set; }//no
        */
        public bool IsMathadi { get; set; }
        public decimal ACTUPKGS { get; set; }
        public decimal ACTUPKGS_Load { get; set; }
        public decimal ACTUWT { get; set; }
        public decimal ACTUWT_Load { get; set; }
        public string Branch { get; set; }

        public decimal Rate { get; set; }
        public decimal MaxAmount { get; set; }
        public string VendorType { get; set; }
        /*//no
        public string MathadiSlipNo { get; set; }
        public DateTime MathadiDate { get; set; }
        public decimal MathadiAmt { get; set; }
        public string PayMode { get; set; }
        public string ChqNo { get; set; }
        public DateTime ChqDate { get; set; }
        public decimal ChqAmt { get; set; }
        public DateTime BillDate { get; set; }
        public decimal NETAmount { get; set; }
        public DateTime VoucherDate { get; set; }
        public decimal VoucherAmount { get; set; }
        public string Comment { get; set; }
         * */
    }
}