﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FleetDataService.Models
{
    public class vw_UserList_Mapping
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public string CodeName { get; set; }
    }
}