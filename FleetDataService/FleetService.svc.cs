﻿using CYGNUS.Models;
using Fleet.Classes;
using Fleet.ViewModels;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "FleetService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select FleetService.svc or FleetService.svc.cs at the Solution Explorer and start debugging.
    public class FleetService : IFleetService
    {
        public void DoWork()
        {
        }

        GeneralFuncations GF = new GeneralFuncations();
        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region Trip OPEN

        #region Get Vechical

        public List<Webx_Master_General> GetSelectedVechical()
        {
            //string QueryString = "select  CodeId,CodeDesc from webx_master_General where codetype = 'VENDTY' AND CODEID IN ('01','05','07')";
            string QueryString = "SELECT CodeId,CodeDesc FROM webx_master_General WHERE codetype = 'VENDTY' AND CODEID ='05'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList;
        }

        #endregion

        #region Get Valid Driver List

        public List<WEBX_FLEET_DRIVERMST> GetValidDriverList(string driverName, string BranCode, string Flag, string VechicalNo)
        {
            string QueryString = "exec USP_VALID_Driver '" + driverName + "','" + BranCode + "','" + Flag + "','" + VechicalNo + "'";

            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_DRIVERMST> GetValidDriverListList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(dataTable);
            return GetValidDriverListList;
        }

        #endregion

        #region Get Valid Vehical List

        public List<webx_VEHICLE_HDR> GetValidDriverList(string driverName, string BranCode, string VendorType)
        {
            string QueryString = "exec USP_VALID_VEHICLE '" + driverName + "','" + BranCode + "','" + VendorType + "'";

            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_VEHICLE_HDR> GetValidDriverListList = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(dataTable);
            return GetValidDriverListList;
        }

        #endregion

        #region Get DriverDetails_New List
        public DataTable GetDriverDetails_New(string Driver_Id)
        {
            string SQR = "exec Usp_GetDriverDetails_New_TS '" + Driver_Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }
        #endregion

        #region Get Vechical KM
        public DataTable GetVechicalKM(string VechicalNo)
        {
            string SQR1 = "Select ISNULL(current_KM_Read,0) as current_KM_Read  from webx_vehicle_hdr  where vehno='" + VechicalNo.Trim() + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQR1);
            return Dt1;
        }
        #endregion

        #region Get Fleet CheckList Module
        public List<Webx_Fleet_CheckList_Detail> GetFleetCheckList()
        {
            string SQR1 = "Select * from VW_Fleet_CheckList_Module";
            DataTable DtFleet = GF.GetDataTableFromSP(SQR1);
            List<Webx_Fleet_CheckList_Detail> GetDtFleetList = DataRowToObject.CreateListFromTable<Webx_Fleet_CheckList_Detail>(DtFleet);
            return GetDtFleetList;
        }
        #endregion

        #region Get Fuel Vendor List
        public List<Webx_Master_General> GetFuelVendor(string BranchCode)
        {
            string SQR1 = "select A.VENDORCODE as CodeId,(A.VENDORCODE +' : '+ A.VENDORNAME) as CodeDesc from webx_VENDOR_HDR as A inner join webx_VENDOR_det as B on A.VENDORCODE=b.VENDORCODE where VENDOR_TYPE='12' and B.VENDORBRCD like '%" + BranchCode + "%'";
            DataTable DtFuelVendor = GF.GetDataTableFromSP(SQR1);
            List<Webx_Master_General> GetFuelVendorList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DtFuelVendor);
            return GetFuelVendorList;
        }


        #endregion

        #region New Treep Sheet Insert Process

        public DataTable WebX_SP_GetNextDocumentCode(string LocCode, string FinYear, string Document)
        {
            string SQR = "exec WebX_SP_GetNextDocumentCode '" + LocCode + "','" + FinYear + "','" + Document + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;

        }

        public DataTable Insert_Webx_Fleet_Drivermst_Attached(int Driver_Id, string Driver_Name, string License_No, DateTime Valdity_dt, string Manual_Driver_Code, string ActiveFlag, string EntryBy)
        {
            string SQR = "exec Usp_Insert_Webx_Fleet_Drivermst_Attached '" + Driver_Id + "','" + Driver_Name + "','" + License_No + "','" + Valdity_dt + "','" + Manual_Driver_Code + "','" + ActiveFlag + "','" + EntryBy + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public List<Webx_Fleet_Triprule> Get_CheckTripRule()
        {
            string SQR = "Select * From Webx_Fleet_Triprule";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<Webx_Fleet_Triprule> GetDtFleetList = DataRowToObject.CreateListFromTable<Webx_Fleet_Triprule>(Dt);
            return GetDtFleetList;
        }

        public DataTable Insert_TripOpen(string username, string XML, string XML1, string XML2, string XML3, string XML4, string XML5, string BaseLocationCode)
        {
            string SQR = "exec Usp_Operationally_TripOpen_NewPortal '" + username + "','" + XML + "','" + XML1 + "','" + XML2 + "','" + XML3 + "','" + XML4 + "','" + XML5 + "','" + BaseLocationCode + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "TripOpen", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        public DataTable CheckDocumentNoDetails(string DocumentNo)
        {
            string SQRY = "EXEC USP_CheckDocumentNoManullTripDetails '" + DocumentNo + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<webx_rutmas> GetRoutemodewiseDetails()
        {
            string SQR = "SELECT RUTCD,RUTCD +':'+RUTNM as RUTNM  FROM webx_rutmas (NOLOCK)";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_rutmas> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_rutmas>(Dt);

            return GetCustomerVendorList;
        }

        #endregion

        #region Tripsheetclose

        public List<WEBX_FLEET_VEHICLE_ISSUE> GetTripSheetData(string IssueID, string FirstDate1, string LastDate1, string BaseLocationCode, string TripsheetFlag, string VehNo, string BaseCompanyCode, string CloseType)
        {
            string SQRY = "";
            //SQRY = "EXEC /*usp_VehicleIssueList*/ usp_VehicleIssueList_NewPortal '" + IssueID + "','" + FirstDate1 + "','" + LastDate1 + "','Close','" + BaseLocationCode + "','" + TripsheetFlag + "','" + BaseCompanyCode + "'/*,'" + VehNo + "','2'*/";
            SQRY = "EXEC usp_VehicleIssueList_NewPortal '" + IssueID + "','" + FirstDate1 + "','" + LastDate1 + "','Close','" + BaseLocationCode + "','" + TripsheetFlag + "','" + BaseCompanyCode + "','" + CloseType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_VEHICLE_ISSUE> POList = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(Dt1);
            return POList;
        }

        public List<WEBX_TRIPSHEET_ADVEXP> Get_WTRSADV(string TripSheetNo)
        {
            string SQRY = "";
            SQRY = "SELECT convert(varchar,AdvDate,103) AS ADate, * FROM WEBX_TRIPSHEET_ADVEXP WHERE isnull(VoucherRefNo,'')<>'' and TripSheetNo ='" + TripSheetNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_TRIPSHEET_ADVEXP> List = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_ADVEXP>(Dt1);
            return List;
        }

        public WEBX_FLEET_VEHICLE_ISSUE DetailsFromId(string id)
        {
            string SQR = "SELECT * from vw_TripSheetClose_Newportal where VSlipNo='" + id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(Dt).FirstOrDefault();
        }

        public DataTable TripOperationally_Close(string XML, string XML1, string XML2, string XML3, string XML4, string TripsheetNo, string Location, string Company)
        {
            string SQR = "exec Usp_Operationally_TripClose '" + XML + "','" + XML1 + "','" + XML2 + "','" + XML3 + "','" + XML4 + "','" + TripsheetNo + "','" + Location + "','" + Company + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "TripOperationally_Close", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable Operationally_Financial_Close(string XML, string XML1, string XML2, string XML3, string TripsheetNo)
        {
            string SQR = "exec Usp_Operationally_Financial_Close_TripClose '" + XML + "','" + XML1 + "','" + XML2 + "','" + XML3 + "','" + TripsheetNo + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "Operationally_Financial_Close", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public List<WEBX_TRIPSHEET_ADVEXP> DetailsForTrpAdvExp(string id)
        {
            string SQR = "exec Usp_Bind_Advance_against_TripSheet '" + id + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_ADVEXP>(Dt);
        }

        public List<Webx_Trip_Expense_Master> GetNatureofexp()
        {
            //string SQRY = "exec usp_fulExp";
            string SQRY = "exec usp_fulExp_New_Portal";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Trip_Expense_Master> GeneralList = DataRowToObject.CreateListFromTable<Webx_Trip_Expense_Master>(Dt1);
            return GeneralList;
        }

        public List<webx_VENDOR_HDR> Get_VendHDR_List(string Brcd)
        {
            string SQRY = "";
            //if (Brcd == "FLLET")
            //{
                SQRY = "SELECT DISTINCT VENDORNAME,VENDORCODE FROM webx_VENDOR_HDR WHERE VENDOR_TYPE='12' AND ACTIVE='Y' ORDER BY VENDORNAME";
            //}
            //else
            //{
            //    SQRY = "SELECT DISTINCT VENDORNAME,VENDORCODE FROM webx_VENDOR_HDR WHERE VENDOR_TYPE='12' and VENDORCODE in ('V0477') ORDER BY VENDORNAME";
            //}
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> List = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_FUELBRAND> Get_Brand_List()
        {
            string SQRY = "SELECT DISTINCT Fuel_Brand_ID,Fuel_Brand_Name FROM WEBX_FLEET_FUELBRAND WHERE Active_Flag='Y' ORDER BY Fuel_Brand_Name";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_FUELBRAND> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_FUELBRAND>(Dt1);
            return List;
        }

        public List<Webx_Master_General> Get_FuelType_List()
        {
            string SQRY = "SELECT DISTINCT CodeId,CodeDesc FROM Webx_Master_General WHERE CodeType='FUELTY' ORDER BY CodeDesc";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> List = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_ENROUTE_EXP> Get_TripExp_List(string VSlipNo)
        {
            string SQRY = "exec  Usp_Get_Trip_Enroute_Exp '" + VSlipNo + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_ENROUTE_EXP> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_ENROUTE_EXP>(Dt1);
            return List;
        }

        public List<Webx_Fleet_Triprule> Get_Webx_Fleet_Trip()
        {
            string SQRY = "Select *  From Webx_Fleet_Triprule";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Fleet_Triprule> List = DataRowToObject.CreateListFromTable<Webx_Fleet_Triprule>(Dt1);
            return List;
        }
        public List<WEBX_TRIPSHEET_OILEXPDET> ListTripSheer_OilExpdet(string TripSheetNumber)
        {
            string QueryString = "select convert(varchar, BillDt, 103) as BillDt1,(select top 1 vendorname from webx_vendor_hdr where vendorcode = PetrolPName or VendorName = PetrolPName) as PPName,(select  top 1 Fuel_Brand_Name from WEBX_FLEET_FUELBRAND where convert(varchar(3), Fuel_Brand_ID) = Brand or Fuel_Brand_Name = Brand) BrandName,(select CodeDesc from Webx_Master_General where CodeType = 'FUELTY' and(CodeId = FuelType or CodeDesc = FuelType)) FuelTypeName, *from WEBX_TRIPSHEET_OILEXPDET where TripSheetNo ='" + TripSheetNumber + "'";
            DataTable Results = GF.getdatetablefromQuery(QueryString);
            return DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_OILEXPDET>(Results);

        }

        public DataTable Get_FuelSlipData(string NO)
        {
            string SQRY = "USP_Get_FuelSlipData '" + NO + "','','' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Get_Oil_SlipData(string NO)
        {
            string SQRY = "USP_Get_Oil_SlipData '" + NO + "','','' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Amount Approval

        public List<WEBX_FLEET_ENROUTE_EXP> Get_Other_EnrouteExp_Details(string id)
        {
            string SQR = "exec USP_Get_Other_EnrouteExp_Details '" + id + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return DataRowToObject.CreateListFromTable<WEBX_FLEET_ENROUTE_EXP>(Dt);
        }

        public DataTable USP_Trip_Other_Approve_Reject(string TripsheetNo, string IsApprove, string IsReject)
        {
            string SQR = "exec USP_Trip_Other_Approve_Reject '" + TripsheetNo + "','" + IsApprove + "','" + IsReject + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "USP_Trip_Other_Approve_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable Check_Is_DriverSetllementDone(string TripsheetNo)
        {
            string SQR = "exec USP_Is_DriverSetllementDone '" + TripsheetNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        #region Fleet Fuel Managment

        public DataTable Get_TripFuelListForEntry(string TripSheetNo, string FromDate, string ToDate, string TripActionType, string BaseLocationCode, string TripFTType, string BaseCompanyCode)
        {
            string SQRY = "exec usp_New_Trip_VehicleIssueList_ver2 '" + TripSheetNo + "','" + FromDate + "','" + ToDate + "','" + TripActionType + "','" + BaseLocationCode + "','" + TripFTType + "','" + BaseCompanyCode + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Get_FuelSlip_By_Id(string Id)
        {
            string sql = "select V.VSlipNo,Convert(varchar,VSlipDt,106) VSlipDt,V.Vehicleno as VehicleNo,f_issue_startkm as Start_km,f_issue_fill "
                     + " ,(Select Driver_Name from webx_fleet_drivermst where Driver_Id=v.driver1) DriverName "
                     + " ,(Select License_no from webx_fleet_drivermst where Driver_Id=v.driver1) License_No "
                     + " ,(Select convert(varchar,valdity_dt,106) valdity_dt from webx_fleet_drivermst where Driver_Id=v.driver1) Valdity_dt,v.driver1,v.CustCode,v.Category,v.Market_Own ,Tripsheet_StartLoc,Tripsheet_EndLoc ,(Select custnm  from webx_CUSTHDR  where custcd=v.CustCode) as Custname "
                     + " from WEBX_FLEET_VEHICLE_ISSUE v LEFT OUTER JOIN webx_fleet_vehicle_request r ON V.VSlipNo=r.VSlipId"
                     + " where 1=1 "
                     + " and V.VSlipNo='" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(sql);
            return Dt;
        }

        public DataTable Get_FuelSlip_Row_By_ID(string Id, string brcd)
        {
            string sql = "SELECT SLIPTYPE,SRNO,FUELSLIPNO,FUELVENDOR,QUANTITYINLITER,RATE,AMOUNT FROM WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO WHERE CANCELDT IS NULL  AND BILLNO IS NULL AND TRIPSHEETNO ='" + Id + "' and BRCD='" + brcd + "'";
            DataTable Dt = GF.GetDataTableFromSP(sql);
            return Dt;
        }

        public DataTable USP_FLEET_INSERT_FUELSLIP_DETAILS(string VSlipNo, string FUELSLIPNO, string FUELVENDOR, decimal QUANTITYINLITER, decimal RATE, decimal AMOUNT, string BaseUserName, string BaseLocationCode, string VehicleNo, int SLIPTYPE, string BaseCompanyCode, string BaseFinYear, string yearSuffix)
        {
            string Sql = "EXEC [USP_FLEET_INSERT_FUELSLIP_DETAILS] '" + VSlipNo + "','" + FUELSLIPNO + "','" + FUELVENDOR + "','" + QUANTITYINLITER + "','" + RATE + "','" + AMOUNT + "','" + BaseUserName + "','" + BaseLocationCode + "','" + VehicleNo + "','" + SLIPTYPE + "','" + BaseCompanyCode + "','" + BaseFinYear + "','" + yearSuffix + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        public DataTable Usp_Get_Fuel_Edit_Record(int SrNo)
        {
            string Sql = "select * from WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO with (nolock) where SRNO='" + SrNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        public DataTable USP_FLEET_UPDATE_FUELSLIP_DETAILS(string VSlipNo, string FUELSLIPNO, string FUELVENDOR, decimal QUANTITYINLITER, decimal RATE, decimal AMOUNT, string BaseUserName, string BaseLocationCode, string VehicleNo, int SRNO)
        {
            string Sql = "EXEC [USP_FLEET_UPDATE_FUELSLIP_DETAILS] '" + VSlipNo + "','" + FUELSLIPNO + "','" + FUELVENDOR + "','" + QUANTITYINLITER + "','" + RATE + "','" + AMOUNT + "','" + BaseUserName + "','" + BaseLocationCode + "','" + VehicleNo + "','" + SRNO + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        public DataTable USP_FLEET_THC_FUEL_QUERY(string THCNO, string FromDate, string ToDate, string BaseLocationCode, string BaseCompanyCode)
        {
            string SQRY = "exec USP_FLEET_THC_FUEL_QUERY '" + THCNO + "','" + FromDate + "','" + ToDate + "','" + BaseLocationCode + "','" + BaseCompanyCode + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Job Order

        public List<WEBX_FLEET_WORKGROUPMST> Get_AllWorkGroupList(string Type)
        {
            string SQRY = "";
            SQRY = "exec usp_Job_AllWorkGroupList_NewPortal '" + Type + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_WORKGROUPMST> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_WORKGROUPMST>(Dt1);
            return List;
        }

        public List<Webx_Fleet_TaskTypeMst> Get_TaskTypeList()
        {
            string SQRY = "";
            SQRY = "exec USP_Get_TaskTypeList";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Fleet_TaskTypeMst> List = DataRowToObject.CreateListFromTable<Webx_Fleet_TaskTypeMst>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_GENERALTASKMST> Get_TaskDescriptionList(string GRPCD)
        {
            string SQRY = "";
            SQRY = "exec usp_Job_TaskList_NewPortal '" + GRPCD + "','0'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_GENERALTASKMST> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_GENERALTASKMST>(Dt1);
            return List;
        }

        public List<webx_PO_SKU_Master> Get_SKU_MasterList()
        {
            string SQRY = "";
            SQRY = "[usp_Job_AllWorkGroupList_SKU]";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_PO_SKU_Master> List = DataRowToObject.CreateListFromTable<webx_PO_SKU_Master>(Dt1);
            return List;
        }

        public List<Webx_Fleet_spareParthdr> Get_SpareParthdrList(string W_GRPDC)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_SpareParthdrList '" + W_GRPDC + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Fleet_spareParthdr> List = DataRowToObject.CreateListFromTable<Webx_Fleet_spareParthdr>(Dt1);
            return List;
        }

        public List<webx_VENDOR_HDR> Get_Vendor_List(string BaseLocation)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_VendorList '" + BaseLocation + "' ";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> List = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt1);
            return List;
        }

        public List<webx_VEHICLE_HDR> Get_Vehicles_List(string searchTerm,string BaseLocationCode)
        {
            string SQRY = "";

            //SQRY = "select V.VEHNO,V.Vehicle_Status from webx_vehicle_hdr V where 1=1  and VENDORTYPE='05' and vehno like '%" + searchTerm + "%'  order by VEHNO";
            SQRY = "exec USP_Get_VehicleWithStatus '" + searchTerm + "','" + BaseLocationCode + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VEHICLE_HDR> List = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(Dt1);
            return List;
        }

        public DataTable AddJobOrder(string Job_OrderHdrInsertUpdate, string Job_OrderDetInsertUpdate, string Job_OrderDetInsertUpdate1, string SMJob_OrderDetInsert, string Job_EstSparePart, string BRCD, string FIN_YEAR, string XML, string CompanyCode, string YearValFirst)
        {
            string SQRY = "";
            SQRY = "exec Usp_JobOrder_New '" + Job_OrderHdrInsertUpdate + "','" + Job_OrderDetInsertUpdate + "','" + Job_OrderDetInsertUpdate1 + "','" + SMJob_OrderDetInsert + "','" + Job_EstSparePart + "','" + BRCD + "','" + FIN_YEAR + "','" + XML + "','" + CompanyCode + "','" + YearValFirst + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "JobOrder", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable CloseJobOrder(string JOB_ORDER_NO, string UJHdrCXML, string UJTIUXML, string UJActSPXML, string UserName, string BRCD, string FIN_YEAR)
        {
            string SQRY = "";
            SQRY = "exec Usp_JobOrder_Close '" + JOB_ORDER_NO + "','" + UJHdrCXML + "','" + UJTIUXML + "','" + UJActSPXML + "','" + UserName + "','" + BRCD + "','" + FIN_YEAR + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "CloseJobOrder", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable GetValid_Vehicle_NO(string Vehicle_NO)
        {
            string SQRY = "exec USP_Get_VehicleDetails '" + Vehicle_NO + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetVW_JOBSHEET_SUMMARY(string NO)
        {
            //string SQRY = "SELECT TaskType,ACCDESC,SUM(TOT_LABOUR_COST) AS TOT_LABOUR_COST,SUM(TOT_SPARE_COST)AS TOT_SPARE_COST FROM VW_JOBSHEET_SUMMARY WHERE JOB_ORDER_NO ='" + NO + "' GROUP BY TASKTYPE,ACCDESC";
            string SQRY = "exec USP_JOBSHEET_SUMMARY '" + NO + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<WEBX_FLEET_PM_JOBORDER_HDR> Get_JOBORDER_HDRList(string No)
        {
            string SQRY = "";
            //SQRY = "SELECT  cast (RETURNDT_WORKSHOP as varchar) as Actual_Date_of_return,* FROM WEBX_FLEET_PM_JOBORDER_HDR WHERE JOB_ORDER_NO= '" + No + "'";
            SQRY = "exec USP_JOBORDER_HDRList '" + No + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_PM_JOBORDER_HDR> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_PM_JOBORDER_HDR>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_PM_JOBORDER_DET> Get_JOBORDER_DETList(string No)
        {
            string SQRY = "";
            //SQRY = "SELECT * FROM WEBX_FLEET_PM_JOBORDER_DET WHERE JOB_ORDER_NO= '" + No + "'";
            SQRY = "exec USP_JOBORDER_DETList '" + No + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_PM_JOBORDER_DET> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_PM_JOBORDER_DET>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET> Get_SPARE_EST_DETList(string No)
        {
            string SQRY = "";
            SQRY = "exec USP_SPARE_EST_DETList '" + No + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET>(Dt1);
            return List;
        }

        public List<CYGNUS_Job_Order_AdvanceEntry> Get_JOBORDER_Advance(string No)
        {
            string SQRY = "exec USP_JOBORDER_AdvanceList '" + No + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Job_Order_AdvanceEntry> List = DataRowToObject.CreateListFromTable<CYGNUS_Job_Order_AdvanceEntry>(Dt1);
            return List;
        }

        public string Get_ValidJobOrderNo(string NO)
        {
            string SQRY = "exec USP_ValidJobOrderNo'" + NO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt.Rows[0][0].ToString();
        }

        public DataTable GetSparePart_NO(string Type)
        {
            string SQRY = "exec USP_FLEET_Job_SparePart_NewPortal '" + Type + "','0' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetValid_JobOrder_NO(string No)
        {
            string SQRY = "exec USP_Get_JobOrder_NO '" + No + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable ServiceCentreType_Changes(string No, string SERVICE_CENTER_TYPE, string VENDOR_CODE, string WS_LOCCODE)
        {
            string SQRY = "exec USP_Update_ServiceType '" + No + "','" + SERVICE_CENTER_TYPE + "','" + VENDOR_CODE + "','" + WS_LOCCODE + "' ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "ServiceCentreType_Changes", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Job Order Approval

        public DataTable Get_ApproveList(string JobNo, string FromDate, string ToDate, string JobCardType, string BaseLocationCode, string BaseCompanyCode)
        {
            string SQRY = "";
            SQRY = "exec usp_JobOrderCloseApproveList_NewPortal '" + JobNo + "','" + FromDate + "','" + ToDate + "','" + JobCardType + "','" + BaseLocationCode + "','" + BaseCompanyCode + "' ";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Get_GenerateApproveList(string JobNo, string FromDate, string ToDate, string JobCardType, string BaseLocationCode, string BaseCompanyCode)
        {
            string SQRY = "";
            SQRY = "exec usp_JobOrderGenerateApproveList_NewPortal '" + JobNo + "','" + FromDate + "','" + ToDate + "','" + JobCardType + "','" + BaseLocationCode + "','" + BaseCompanyCode + "' ";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Get_dtCheckAmt(string JobNo, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_dtCheckAmt '" + JobNo + "','" + UserName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Get_dtLoginUsesDet(string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_dtLoginUsesDet'" + UserName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Get_dtEmails(string JobNo, double vLoc_Level)
        {
            string SQRY = "";
            SQRY = "exec USP_dtEmails '" + JobNo + "','" + vLoc_Level + "','1'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable Get_dtEmails_1(string JobNo, double vLoc_Level)
        {
            string SQRY = "";
            SQRY = "exec USP_dtEmails '" + JobNo + "','" + vLoc_Level + "','0'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Job Order Cancel

        public DataTable GetValid_JOB_ORDER_NO(string JOB_ORDER_NO, string Company_Code)
        {
            string SQRY = "exec USP_GET_JOB_ORDER_NO_NewPortal '" + JOB_ORDER_NO + "','" + Company_Code + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable JobOrder_Cancel(string XML, string UserName, string CompanyCode, string BaseLocationCode)
        {
            string SQRY = "exec Usp_JobOrder_Cancle '" + XML + "','" + UserName + "','" + CompanyCode + "','" + BaseLocationCode + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Job Order Close

        public DataTable Get_JobOrderClose(string JobOrderNo, string FromDt, string ToDt, string CardType, string BRCD, string CompanyCode)
        {
            string SQRY = "exec usp_JobOrderCloseList_NewPortal '" + JobOrderNo + "','" + FromDt + "','" + ToDt + "','" + CardType + "','" + BRCD + "','" + CompanyCode + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Vehicle Issue Slip Query

        public DataTable Get_VehicleIssueSlipQuery(string VSlipId, string FromDt, string ToDt, string UpdClose, string BRCD, string TripsheetFlag, string CompanyCode)
        {
            string SQRY = "exec usp_New_Trip_VehicleIssueList_ver2 '" + VSlipId + "','" + FromDt + "','" + ToDt + "','" + UpdClose + "','" + BRCD + "','" + TripsheetFlag + "','" + CompanyCode + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<WEBX_FLEET_VEHICLE_ISSUE> Get_IssueslipData(string TripSheetNo)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_IssueslipData '" + TripSheetNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_VEHICLE_ISSUE> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(Dt1);
            return List;
        }

        public List<WEBX_TRIPSHEET_ADVEXP> Get_VehicleAdvanceData(string TripSheetNo)
        {
            string SQRY = "";
            SQRY = "exec USP_VehicleAdvance '" + TripSheetNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_TRIPSHEET_ADVEXP> List = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_ADVEXP>(Dt1);
            return List;
        }

        public string FineYear(string Year)
        {
            string SQRY = "";
            SQRY = "SELECT DBO.FINANCIAL_YEAR('" + Year + "') AS FINANCIAL_YEAR";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1.Rows[0][0].ToString();
        }

        public List<Account_Cash_Bank_Code> Get_adv_acccodeList(string Type, string Location)
        {
            string SQRY = "exec USP_Get_Cash_Bank_Account '" + Type + "','" + Location + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Account_Cash_Bank_Code> List = DataRowToObject.CreateListFromTable<Account_Cash_Bank_Code>(Dt1);
            return List;
        }

        #endregion

        #region View Print / Job Order & Trip Sheet

        public List<ChgangeLoc> TopBranch()
        {
            string commandText = "select loccode as LocCode,locname+' : '+loccode as Location from webx_location where activeFlag='Y' order by locName";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<ChgangeLoc> ListLocation = DataRowToObject.CreateListFromTable<ChgangeLoc>(dataTable);

            return ListLocation;
        }

        public List<Webx_Master_General> Get_DriverList(string Search)
        {
            string commandText = "Select Manual_Driver_Code as CodeId,Driver_Name as CodeDesc from WEBX_FLEET_DRIVERMST where ActiveFlag='Y' and Driver_Name like '%" + Search + "%' order by Driver_Name ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<Webx_Master_General> ListLocation = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);

            return ListLocation;
        }

        public List<webx_VEHICLE_HDR> Get_VehicleList(string Search, string baselocation)
        {
            string commandText = "USP_VALID_VEHICLE '" + Search + "','" + baselocation + "','05'";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<webx_VEHICLE_HDR> ListLocation = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(dataTable);

            return ListLocation;
        }

        public List<vm_JobTripViewPrint> Get_JobOrderViewPrintList(string TripSheetNo, string FromDate, string ToDate, string Status, string BaseCompanyCode)
        {
            string QueryString = "exec usp_JobOrderViewPrintList '" + TripSheetNo + "','" + FromDate + "','" + ToDate + "','" + Status + "','" + BaseCompanyCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vm_JobTripViewPrint> itmList = DataRowToObject.CreateListFromTable<vm_JobTripViewPrint>(DT);
            return itmList;
        }


        #endregion

        #region Trip Route Master

        public List<webx_trip_rutmas> GetTripRutMstDetails()
        {
            string curFile = folderPath + "webx_trip_rutmas.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "select * from webx_trip_rutmas";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_trip_rutmas> TripRutMstList_XML = DataRowToObject.CreateListFromTable<webx_trip_rutmas>(Dt);
                GF.SerializeParams<webx_trip_rutmas>(TripRutMstList_XML, folderPath + "webx_trip_rutmas.xml");
            }
            List<webx_trip_rutmas> TripRutMstList = GF.DeserializeParams<webx_trip_rutmas>(folderPath + "webx_trip_rutmas.xml");

            return TripRutMstList;
        }

        public List<webx_trip_ruttran> GetTripRutTranDetails()
        {
            string curFile = folderPath + "webx_trip_ruttran.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "select * from webx_trip_ruttran";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_trip_ruttran> TripRutTranList_XML = DataRowToObject.CreateListFromTable<webx_trip_ruttran>(Dt);
                GF.SerializeParams<webx_trip_ruttran>(TripRutTranList_XML, folderPath + "webx_trip_ruttran.xml");
            }
            List<webx_trip_ruttran> TripRutTranList = GF.DeserializeParams<webx_trip_ruttran>(folderPath + "webx_trip_ruttran.xml");

            return TripRutTranList;
        }

        public DataTable AddEditTranRut(string HdrXML, string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryStringAdd = "exec [usp_InsertUpdate_webx_Trip_Rut] '" + HdrXML + "','" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryStringAdd);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_trip_rutmas> webx_trip_rutmasList = DataRowToObject.CreateListFromTable<webx_trip_rutmas>(DS.Tables[1]);
                GF.SerializeParams<webx_trip_rutmas>(webx_trip_rutmasList, folderPath + "webx_trip_rutmas.xml");
                List<webx_trip_ruttran> webx_trip_ruttranList = DataRowToObject.CreateListFromTable<webx_trip_ruttran>(DS.Tables[2]);
                GF.SerializeParams<webx_trip_ruttran>(webx_trip_ruttranList, folderPath + "webx_trip_ruttran.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Tyre Odometer Search

        public List<webx_VEHICLE_HDR> GetVEHICLEList()
        {
            string SQR = "exec USP_GetVEHICLEL";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_VEHICLE_HDR> VEHICLE = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(Dt);
            return VEHICLE;
        }

        public List<webx_VEHICLE_HDR> GetVEHICLE(string VehNo)
        {
            string SQR = "exec usp_Tyre_Odometer_List '" + VehNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_VEHICLE_HDR> listVEHICLE = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(Dt);
            return listVEHICLE;
        }

        public List<webx_VEHICLE_HDR> GetOdometerResion()
        {
            string SQR = "exec USP_TYRE_ODOMETER_REASON";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_VEHICLE_HDR> listVEHICLE = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(Dt);
            return listVEHICLE;
        }

        public DataTable GetVEHNOCUKM(string VehNo)
        {
            string SQRY = "exec USP_GetVEHOKM '" + VehNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable TyreOdometersubmit(decimal Tran_No, int VEH_INTERNAL_NO, decimal Last_Km_Reading, decimal Odometer_Reading, string Odometer_Reading_Dt, string Odometer_Reason)
        {

            string SQRY = "exec USP_INS_UP_TYREODOMETER '" + Tran_No + "', '" + VEH_INTERNAL_NO + "','" + Last_Km_Reading + "','" + Odometer_Reading + "','" + Odometer_Reading_Dt + "','" + Odometer_Reason + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Vehicle Part

        public List<WebX_Master_CodeTypes> GetVehiclePartList()
        {
            string SQRY = "select PartId as Id, PartName as HeaderCode,ActiveFlag as Activeflag_YN from WEBX_FLEET_PARTMST";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_CodeTypes> VendorTypeList = DataRowToObject.CreateListFromTable<WebX_Master_CodeTypes>(Dt);
            return VendorTypeList;
        }

        public string CheckDuplicatePartName(string CodeDesc)
        {
            string QueryString = "SELECT COUNT(*) FROM WEBX_FLEET_PARTMST WITH(NOLOCK) WHERE PartName=LTRIM(RTRIM('" + CodeDesc + "'))";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        #region FueBrand

        public List<FuelBrandModel> GetFuelBrandList()
        {
            string SQRY = "exec sp_Fleet_Fuel_Get_FuelBrand";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<FuelBrandModel> VendorTypeList = DataRowToObject.CreateListFromTable<FuelBrandModel>(Dt);
            return VendorTypeList;
        }

        public List<Webx_Master_General> GetFuelType()
        {
            string SQRY = "exec sp_Fleet_Fuel_Get_Fuel_Types";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> VendorTypeList = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return VendorTypeList;
        }

        public bool Fleet_Fuel_Insert_WEBX_FLEET_FUELBRAND(int Fuel_Brand_ID, string Fuel_Brand_Name, string VendorCode, string Fuel_Type_ID, string Active_Flag)
        {
            bool Status = false;
            string Sql = "EXEC [sp_Fleet_Fuel_Insert_WEBX_FLEET_FUELBRAND] '" + Fuel_Brand_ID + "','" + Fuel_Brand_Name + "','" + VendorCode + "','" + Fuel_Type_ID + "','" + Active_Flag + "'";
            DataSet DS = GF.GetDataSetFromSP(Sql);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        public bool Fleet_Fuel_Update_WEBX_FLEET_FUELBRAND(int Fuel_Brand_ID, string Fuel_Brand_Name, string VendorCode, string Fuel_Type_ID, string Active_Flag)
        {
            bool Status = false;
            string Sql = "EXEC [sp_Fleet_Fuel_Update_WEBX_FLEET_FUELBRAND] '" + Fuel_Brand_ID + "','" + Fuel_Brand_Name + "','" + VendorCode + "','" + Fuel_Type_ID + "','" + Active_Flag + "'";
            DataSet DS = GF.GetDataSetFromSP(Sql);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion

        #region Fuel Card Master

        public List<WEBX_FLEET_FUELCARD> GetFuelCardList()
        {
            string SQRY = "exec sp_Fleet_Fuel_Get_FuelCard";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_FUELCARD> VendorTypeList = DataRowToObject.CreateListFromTable<WEBX_FLEET_FUELCARD>(Dt);
            return VendorTypeList;
        }

        public List<Webx_Master_General> GetFuelBrandFromType(string Serach)
        {
            string SQRY = "Select Fuel_Brand_ID as Id, Fuel_Brand_Name as CodeDesc From WEBX_FLEET_FUELBRAND Where Fuel_Type_ID = '" + Serach + "' and Active_Flag='Y' Order By Fuel_Brand_Name";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> ListName = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1);
            ListName = ListName.ToList();
            return ListName;
        }

        public List<webx_acctinfo> GetAccountCode()
        {
            string SQRY = "exec sp_Fleet_Fuel_Get_Fuel_Ledger";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> VendorTypeList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt);
            return VendorTypeList;
        }

        public List<webx_acctinfo> GetGroupCode()
        {
            string SQRY = "SELECT Groupcode AS Acccode , Groupdesc AS Accdesc FROM Webx_Groups WITH(NOLOCK) WHERE Groupcode='ASS029'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> GroupList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt);
            return GroupList;
        }

        public bool Fleet_Fuel_Insert_WEBX_FLEET_FUELCARD(int Fuel_Card_ID, string Manual_Fuel_Card_No, string VendorCode, string Issue_Date, string Expiry_Date, string Vehicle_No, string Fuel_Type_ID, int Fuel_Brand_ID, string Active_Flag, string acccode, string BaseComapnyCode)
        {
            bool Status = false;
            //string Sql = "EXEC [sp_Fleet_Fuel_Insert_WEBX_FLEET_FUELCARD] '" + Fuel_Card_ID + "','" + Manual_Fuel_Card_No + "','" + VendorCode + "','" + Issue_Date + "','" + Expiry_Date + "','" + Vehicle_No + "','" + Fuel_Type_ID + "','" + Fuel_Brand_ID + "','" + Active_Flag + "','" + acccode + "'";
            string Sql = "EXEC [sp_Fleet_Fuel_Insert_WEBX_FLEET_FUELCARD_NewPortal] '" + Fuel_Card_ID + "','" + Manual_Fuel_Card_No + "','" + VendorCode + "','" + Issue_Date + "','" + Expiry_Date + "','" + Vehicle_No + "','" + Fuel_Type_ID + "','" + Fuel_Brand_ID + "','" + Active_Flag + "','" + acccode + "','" + BaseComapnyCode + "'";
            int id = GF.SaveRequestServices(Sql.Replace("'", "''"), "Fleet_Fuel_Insert_WEBX_FLEET_FUELCARD_NewPortal", "", "");
            DataSet DS = GF.GetDataSetFromSP(Sql);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            File.Delete(folderPath + "webx_VEHICLE_HDR.xml");
            return Status;
        }

        public bool Fleet_Fuel_Update_WEBX_FLEET_FUELCARD(int Fuel_Card_ID, string Manual_Fuel_Card_No, string VendorCode, string Issue_Date, string Expiry_Date, string Vehicle_No, string Fuel_Type_ID, int Fuel_Brand_ID, string Active_Flag, string acccode )
        {
            bool Status = false;
            string Sql = "EXEC [sp_Fleet_Fuel_Update_WEBX_FLEET_FUELCARD] '" + Fuel_Card_ID + "','" + Manual_Fuel_Card_No + "','" + VendorCode + "','" + Issue_Date + "','" + Expiry_Date + "','" + Vehicle_No + "','" + Fuel_Type_ID + "','" + Fuel_Brand_ID + "','" + Active_Flag + "','" + acccode + "'";
            int id = GF.SaveRequestServices(Sql.Replace("'", "''"), "Fleet_Fuel_Update_WEBX_FLEET_FUELCARD", "", "");
            DataSet DS = GF.GetDataSetFromSP(Sql);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            File.Delete(folderPath + "webx_VEHICLE_HDR.xml");
            return Status;
        }

        #endregion

        #region CHECKLIST

        public List<WEBX_FLEET_CHECKLIST_MST> GetListOfCheckList()
        {
            string SQRY = "exec Usp_Fleet_CheckList_Master ''";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_CHECKLIST_MST> CHECKLIST = DataRowToObject.CreateListFromTable<WEBX_FLEET_CHECKLIST_MST>(Dt);
            return CHECKLIST;
        }

        public List<Webx_Master_General> GetCheckList_Category()
        {
            string SQRY = "exec usp_CheckList_Category";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> CHECKLIST = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return CHECKLIST;
        }

        public List<Webx_Master_General> GetCheckList_Document()
        {
            string SQRY = "exec usp_DocumentNameCode";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> CHECKLIST = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return CHECKLIST;
        }

        public DataTable AddCheckList(string chk_cat, string Chk_Desc, string Chk_Document_Shown, string ActiveFlag, string BaseUserName, string BaseLocationCode)
        {
            string QueryString = "exec USP_FLEET_INSERT_CHECKLIST_MST '" + chk_cat + "','" + Chk_Desc + "','" + Chk_Document_Shown + "','" + ActiveFlag + "','" + BaseUserName + "','" + BaseLocationCode + "' ";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        public DataTable EditCheckList(decimal chk_id, string chk_cat, string Chk_Desc, string Chk_Document_Shown, string ActiveFlag, string BaseUserName, string BaseCompanyCode)
        {
            string QueryString = "exec USP_FLEET_EDIT_CHECKLIST_MST '" + chk_id + "','" + chk_cat + "','" + Chk_Desc + "','" + Chk_Document_Shown + "','" + ActiveFlag + "','" + BaseUserName + "','" + System.DateTime.Now + "','" + BaseCompanyCode + "' ";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        #endregion

        #region Document Type Master

        public List<WEBX_FLEET_DOCU_TYPE_MST> GetDocumentTypeList(string DOCU_TYPE_ID, string DOCU_TYPE)
        {
            string SQRY = "exec usp_document_type_list '" + DOCU_TYPE_ID + "','" + DOCU_TYPE + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DOCU_TYPE_MST> DockTypeList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCU_TYPE_MST>(Dt);
            return DockTypeList;
        }

        public List<Webx_Master_General> GetRENEW_AUTH_ID()
        {
            string SQRY = "exec USP_RENEWAUCODE";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> CHECKLIST = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return CHECKLIST;
        }

        public DataTable Insert_Docu_Type(string DOCU_TYPE, string DECS, string APPLICABLE_STATE, int RENEW_AUTH_ID, string COST_CAPTURED, string ACTIVE_FLAG)
        {
            string QueryString = "exec usp_Insert_Docu_Type '','" + DOCU_TYPE + "','" + DECS + "','" + APPLICABLE_STATE + "','" + RENEW_AUTH_ID + "','" + COST_CAPTURED + "','" + ACTIVE_FLAG + "' ";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        public DataTable Update_Docu_Type(int DOCU_TYPE_ID, string DOCU_TYPE, string DECS, string APPLICABLE_STATE, int RENEW_AUTH_ID, string COST_CAPTURED, string ACTIVE_FLAG)
        {
            string QueryString = "UPDATE WEBX_FLEET_DOCU_TYPE_MST SET DOCU_TYPE='" + DOCU_TYPE + "',DECS='" + DECS + "',APPLICABLE_STATE='" + APPLICABLE_STATE + "',RENEW_AUTH_ID='" + RENEW_AUTH_ID + "',COST_CAPTURED='" + COST_CAPTURED + "',ACTIVE_FLAG='" + ACTIVE_FLAG + "' WHERE DOCU_TYPE_ID='" + DOCU_TYPE_ID + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }
        #endregion

        #region Driver SelltelMent

        public List<vw_FleetFuelList> GetTripFuelListForEntryList(string TripSheetNo, string FromDate, string ToDate, string TripActionType, string BaseLocationCode, string TripFTType, string BaseCompanyCode)
        {
            string SQRY = "exec usp_New_Trip_VehicleIssueList_ver2 '" + TripSheetNo + "','" + FromDate + "','" + ToDate + "','" + TripActionType + "','" + BaseLocationCode + "','" + TripFTType + "','" + BaseCompanyCode + "' ";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<vw_FleetFuelList> POList = DataRowToObject.CreateListFromTable<vw_FleetFuelList>(Dt1);

            return POList;
        }

        public List<WEBX_FLEET_VEHICLE_ISSUE> Get_DriverDetail(string TripSheetNo)
        {

            string sql = "SELECT v.VSlipNo,v.DIESEL_QTY_CF,v.DIESEL_AMT_CF,CONVERT(varchar, v.VSlipDt, 106) AS Vslipdate, CONVERT(varchar, v.END_DT_TM, 106) AS END_DT_TM, v.VehicleNo AS vehno, v.f_issue_startkm AS f_issue_startkm, v.f_closure_closekm as f_closure_closekm, v.f_issue_fill,"
                       + " (SELECT Driver_Name FROM WEBX_FLEET_DRIVERMST WHERE (Driver_Id = v.Driver1)) AS DriverName,"
                       + " (SELECT License_No FROM WEBX_FLEET_DRIVERMST WHERE (Driver_Id = v.Driver1)) AS License_No,"
                       + " (SELECT CONVERT(varchar, Valdity_dt, 106) AS Valdity_dt FROM WEBX_FLEET_DRIVERMST WHERE (Driver_Id = v.Driver1)) AS Valditydt, v.Driver1, v.Custcode, v.Category, v.Market_Own, v.TripSheet_StartLoc, v.TripSheet_EndLoc,"
                       + " (SELECT CUSTNM FROM webx_CUSTHDR WHERE (CUSTCD = v.Custcode)) AS Custname,"
                       + " (Select Top 1 Convert(VarChar,AdvDate,106) from WEBX_TRIPSHEET_ADVEXP Where TripSheetNo = '" + TripSheetNo + "' Order By EntryDt Desc) As AdvDate "
                       + " FROM WEBX_FLEET_VEHICLE_ISSUE AS v LEFT OUTER JOIN	WEBX_FLEET_VEHICLE_REQUEST AS r ON v.VSlipNo = r.VSlipId"
                       + " left outer join WEBX_FLEET_VEHICLE_CARRY_FORWARD as CF on CF.Tripsheetno = V.VSlipNo"
                       + " WHERE (1 = 1) AND (v.VSlipNo = '" + TripSheetNo + "')";
            DataTable Dt1 = GF.GetDataTableFromSP(sql);
            List<WEBX_FLEET_VEHICLE_ISSUE> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(Dt1);
            return List;
        }

        #endregion

        #region Driver

        public List<WEBX_FLEET_DRIVERMST> GetDriverMstDetails()
        {
            string SQRY = "SELECT * FROM WEBX_FLEET_DRIVERMST";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DRIVERMST> DriverMstList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(Dt);
            return DriverMstList;
        }

        public List<WEBX_FLEET_DRIVER_DOCDET> GetDriverDetDetails()
        {
            string SQRY = "SELECT * FROM WEBX_FLEET_DRIVER_DOCDET";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DRIVER_DOCDET> DriverDetList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVER_DOCDET>(Dt);
            return DriverDetList;
        }

        public string GetMaxDriverCode()
        {
            string QueryString = "select max(Driver_Id) from WEBX_FLEET_DRIVERMST";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        public DataTable GetFileName(int Id, string FileName)
        {
            if (FileName == null)
            {
                FileName = "";
            }
            string QueryString = "EXEC usp_Get_FileName '" + Id + "','" + FileName + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddEditDriver(DriverViewModel DVM, string[] array)
        {
            var Mode = "I";
            if (DVM.WFDM.Driver_Id != 0)
            {
                Mode = "U";
            }
            string String = "'" + DVM.WFDM.Driver_Id + "','" + Mode + "','" + DVM.WFDM.Driver_Name + "','" + DVM.WFDM.Manual_Driver_Code + "','" + DVM.WFDM.DFather_Name + "','" + DVM.WFDM.VEHNO + "','" + DVM.WFDM.License_No + "','" + GF.FormateDate(DVM.WFDM.Valdity_dt) + "','" + DVM.WFDM.ActiveFlag + "','" + DVM.WFDM.Issue_By_RTO + "','" + DVM.WFDM.Name_Of_bank + "','" + DVM.WFDM.Bank_AC_Number + "','" + DVM.WFDM.IFSC_Code + "',";
            string String1 = "'" + DVM.WFDM.Driver_Location + "','" + DVM.WFDM.Mobileno + "','" + DVM.WFDM.P_Address + "','" + DVM.WFDM.P_City + "','" + DVM.WFDM.P_Pincode + "','" + DVM.WFDM.C_Address + "','" + DVM.WFDM.C_City + "','" + DVM.WFDM.C_Pincode + "','" + DVM.WFDM.D_category + "','" + GF.FormateDate(DVM.WFDM.D_DOB) + "',";
            string String2 = "'" + DVM.WFDM.D_Ethnicity_Id + "','" + GF.FormateDate(DVM.WFDM.D_Lic_Initial_Issuance_Date) + "','" + GF.FormateDate(DVM.WFDM.D_Lic_Current_Issuance_Date) + "','" + DVM.WFDDD.License_Verified + "','" + DVM.WFDDD.Address_Verified + "','" + GF.FormateDate(DVM.WFDDD.License_Verified_Dt) + "','" + array[10] + "','" + DVM.WFDDD.Electricity_Bill_YN + "','" + array[0] + "','" + DVM.WFDDD.Telephone_Bill_YN + "',";
            string String3 = "'" + array[1] + "','" + DVM.WFDDD.BankAcc_YN + "','" + array[2] + "','" + DVM.WFDDD.Passport_YN + "','" + array[3] + "','" + DVM.WFDDD.Rationcard_YN + "','" + array[4] + "','" + DVM.WFDDD.ID_Passport_YN + "','" + array[6] + "','" + DVM.WFDDD.Driving_lic_YN + "',";
            string String4 = "'" + array[7] + "','" + DVM.WFDDD.VoterId_YN + "','" + array[8] + "','" + DVM.WFDDD.PAN_YN + "','" + array[9] + "','" + DVM.WFDM.Guarantor_Name + "','" + DVM.WFDDD.Thumb_Impression_YN + "','" + array[11] + "','" + DVM.WFDDD.Driver_Registration_Form_YN + "','" + array[5] + "','" + DVM.WFDM.EntryBy + "','" + DVM.WFDM.UpdatedBy + "',''";
            string QueryString = "EXEC usp_Driver_InsertUpdate_NewPortal" + String + String1 + String2 + String3 + String4;
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditDriver", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Trip Route Expence
        public List<vw_TRIPROUTEEXP_VIEW> GetTripRutExpenceDetails(string Id, string RutCD)
        {
            string SQRY = "Exec usp_TRIPROUTEEXP_VIEW'" + Id + "','" + RutCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<vw_TRIPROUTEEXP_VIEW> TripRutExpenceList = DataRowToObject.CreateListFromTable<vw_TRIPROUTEEXP_VIEW>(Dt);
            return TripRutExpenceList;
        }

        public List<Webx_Trip_Expense_Master> GetTripRutExpence()
        {
            string SQRY = "EXEC USP_FUELEXP";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Trip_Expense_Master> TripRutExpenceList = DataRowToObject.CreateListFromTable<Webx_Trip_Expense_Master>(Dt);
            return TripRutExpenceList;
        }

        public DataTable AddEditTripRutExpence(int ID, string RutExpCD, string RutCD, string VT, string FEC, decimal STDRate, string Flag, string Username,string vehicleNo)
        {
            string QueryStringAdd = "exec USP_TripRouteExp_InsUp '" + ID + "','" + RutExpCD + "','" + RutCD + "','','" + VT + "','" + FEC + "','" + STDRate + "','" + Flag + "','" + Username + "','','"+ vehicleNo + "'";
            return GF.GetDataTableFromSP(QueryStringAdd);
        }

        public string CheckDuplicateFuelTypeCode(string RutCD, string VT, string FEC)
        {
            string QueryString = "exec usp_Check_FuelType_Code '','" + RutCD + "',' " + VT + " ','" + FEC + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        public DataTable AddInsert_EnroutExpance(string XML, string mBranchCode, string mTripSheetNo, string mFinYear, string mYearVal, string BaseCompanyCode, string Username)
        {
            string SQRY = "exec USP_Insert_EnroutExpance '" + XML + "','" + mBranchCode + "','" + mTripSheetNo + "','" + mFinYear + "','" + mYearVal + "','" + BaseCompanyCode + "','" + Username + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "AddInsert_EnroutExpance", "", "");
            return GF.GetDataTableFromSP(SQRY);
        }

        #endregion

        #region Trip Route Expence Fuel

        public DataTable GetTripRutFuelDetails(string Id)
        {
            string SQRY = "SELECT AvgDieselRateId,AvgDieselRate FROM Webx_Fleet_AvgDieselRate WHERE ROUTE_CODE='" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable AddEditDieselRate(int RateId, string route_code, decimal RutCharge, string UserName)
        {
            string SQRY = "";
            if (RateId == 0)
            {
                SQRY = "Usp_Insert_Webx_Fleet_AvgDieselRate'" + RutCharge + "','" + UserName + "','" + route_code + "'";

            }
            else
            {
                SQRY = "Usp_Update_Webx_Fleet_AvgDieselRate'" + RateId + "','" + RutCharge + "','" + UserName + "'";
            }
            return GF.GetDataTableFromSP(SQRY);
        }

        public DataTable AddEditTripRouteFuelExp(int ID, string RutExpCD, string RutCD, string VT, decimal KMPL, string Flag, string Username,string vehicleNo, decimal ltrs)
        {
            string SQRY = "USP_TripRouteFuelExp_InsUp'" + ID + "','" + RutExpCD + "','" + RutCD + "','" + VT + "','" + KMPL + "','" + Flag + "','" + Username + "','"+ vehicleNo + "','"+ ltrs + "'";
            return GF.GetDataTableFromSP(SQRY);
        }

        public List<vw_TRIPROUTEFuelEXP_VIEW> GetTripRutFuelExpenceDetails(string Id, string RutCD)
        {
            string SQRY = "usp_TRIPROUTEFuelEXP_VIEW'" + Id + "','" + RutCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<vw_TRIPROUTEFuelEXP_VIEW> TripRutExpenceList = DataRowToObject.CreateListFromTable<vw_TRIPROUTEFuelEXP_VIEW>(Dt);
            return TripRutExpenceList;
        }

        public string CheckDuplicateVehicleNo(string Id, string RutCD, string VehicleNo)
        {
            string QueryString = "exec [usp_Check_VehicleNo_Fuel_Exp] '" + Id + "','" + RutCD + " ','" + VehicleNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }
        #endregion

        #region Vehicle Document

        public List<WEBX_FLEET_DOCUMENT_DET> GetDOCUTYPE_LIS()
        {
            string SQR = "exec USP_DOCUTYPE_LIST";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> DOCUTYPE = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt);
            return DOCUTYPE;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> GetDOCMGT_STATE()
        {
            string SQR = "exec USP_DOCMGT_STATE";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> DOCUTYPE = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt);
            return DOCUTYPE;
        }

        public DataTable DocumentEntrysubmit(string RENEWAL_AUTU_NAME, string DOCUMENT_NO, DateTime START_DT, DateTime EXPRITY_DT, decimal REMINDER_IN_DAYS, string APPLICABLE_STATE, string DOCU_COST, string REMARKS, string FILENAME, string BaseUserName, string ENTER_BY, int vehicleno, string Flag, int DOCU_TYPE_ID, int DOCU_ID)
        {

            string SQRY = "exec usp_WEBX_FLEET_DOCUMENT_DET_Insert_NewPortal '" + RENEWAL_AUTU_NAME + "', '" + DOCUMENT_NO + "','" + START_DT + "','" + EXPRITY_DT + "','" + REMINDER_IN_DAYS + "','" + APPLICABLE_STATE + "', '" + DOCU_COST + "','" + REMARKS + "','" + FILENAME + "','" + BaseUserName + "','" + ENTER_BY + "','" + vehicleno + "', '" + Flag + "','" + DOCU_TYPE_ID + "','" + DOCU_ID + "'";
            /*usp_WEBX_FLEET_DOCUMENT_DET_Insert_New changes Proces*/
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> GetVehicle_Active()
        {
            string SQR = "exec USP_Vehicle_Active";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> DOCUTYPE = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt);
            return DOCUTYPE;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> GetVehicle_InActive()
        {
            string SQR = "exec USP_Vehicle_InActive";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> DOCUTYPE = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt);
            return DOCUTYPE;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> GetEditDocumentEntryList(int VEHNO, int TypeId)
        {
            string SQR = "exec USP_VEHICLE_WISE_DOC_bkp'" + VEHNO + "', '" + TypeId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> DOCUTYPE = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt);
            return DOCUTYPE;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> DocumentDetailLIST(int VEHNOActive, int DOC_TYPE_ID)
        {
            string SQR = "exec USP_VEHICLE_WISE_DOC_bkp'" + VEHNOActive + "', '" + "" + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt1);
            return MRList_Data;
        }

        public List<WEBX_FLEET_DOCUMENT_DET> DocumentDetailLISTAll()
        {

            string SQR = "exec USP_GetVEHNOALL_LIST";
            DataTable Dt1 = GF.GetDataTableFromSP(SQR);
            List<WEBX_FLEET_DOCUMENT_DET> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_DET>(Dt1);
            return MRList_Data;
        }

        #endregion

        #region Tyre Removal

        public DataTable GetTyreRemovalDetail(string Id)
        {
            string SQRY = "select VEHNO ,Tyre_Attached=(case  when Tyre_Attached is null then '0'  else Tyre_Attached end)  from webx_VEHICLE_HDR where VEHNO='" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<VW_TyreDetails_New> GetTyreRemovalList(string Id)
        {
            //string SQRY = "EXEC USP_Get_TyreDetail_VehicleWIse_New'" + Id + "'";
            string SQRY = "EXEC USP_Get_TyreDetail_VehicleWIse_New_bkp'" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<VW_TyreDetails_New> ItemList = DataRowToObject.CreateListFromTable<VW_TyreDetails_New>(Dt);
            return ItemList;
        }

        public List<VW_TyreDetails_New> GetTyreRemovalListForHome(string Id)
        {
            //string SQRY = "EXEC USP_Get_TyreDetail_VehicleWIse_New'" + Id + "'";
            string SQRY = "EXEC USP_Get_TyreDetail_VehicleWIse_New_bkp_ForHome'" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<VW_TyreDetails_New> ItemList = DataRowToObject.CreateListFromTable<VW_TyreDetails_New>(Dt);
            return ItemList;
        }

        public DataTable EditTyreRemoval(string TrucNo, decimal Removal_KM, string TYRE_ID, string UserName, decimal Distance, decimal Per_KM_Cost, string LocCode, DateTime RemovalDt, decimal LifeOfTyre)
        {
            /* Comment For Live */
            //string SQRY = "EXEC USP_UpdateDismountTyreDtl_New'" + TrucNo + "','" + Removal_KM + "','" + TYRE_ID + "','" + UserName + "','" + Distance + "','" + Per_KM_Cost + "','" + LocCode + "','" + RemovalDt + "','" + LifeOfTyre + "'";
            string SQRY = "EXEC USP_UpdateDismountTyreDtl_New_NewPortal'" + TrucNo + "','" + Removal_KM + "','" + TYRE_ID + "','" + UserName + "','" + Distance + "','" + Per_KM_Cost + "','" + LocCode + "','" + RemovalDt + "','" + LifeOfTyre + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "EditTyreRemoval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region  Tyre Interchange

        public List<TyreDetail> GetTyreDetailByTruck(string Id)
        {
            /* Comment For Live */
            //string SQRY = "EXEC USP_GetTyreModel_VH_Wise'" + Id + "'";
            string SQRY = "EXEC USP_GetTyreModel_VH_Wise_NewPortal'" + Id + "'";

            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<TyreDetail> ItemList = DataRowToObject.CreateListFromTable<TyreDetail>(Dt);
            return ItemList;
        }

        public DataTable EditTyreInterchange(string TyreId, string VEHNO, string NEWVEHNO, decimal ODO, string CmpCode, string UserName)
        {
            /* Comment For Live */
            //string QueryString = "EXEC USP_INTERCHANGE_TYRE'" + TyreId + "','" + VEHNO + "','" + NEWVEHNO + "','" + ODO + "','" + CmpCode + "','" + UserName + "'";
            string QueryString = "EXEC USP_INTERCHANGE_TYRE_NewPortal'" + TyreId + "','" + VEHNO + "','" + NEWVEHNO + "','" + ODO + "','" + CmpCode + "','" + UserName + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "EditTyreInterchange", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<webx_VEHICLE_HDR> GetVehicleBasedonTyreSize(string VehicleNo)
        {
            string QueryString = "SELECT * FROM webx_VEHICLE_HDR A WHERE A.TyreSize = (SELECT B.TyreSize FROM webx_VEHICLE_HDR B WHERE B.VEHNO = '" + VehicleNo + "') and A.VEHNO NOT IN ('" + VehicleNo + "') AND A. ACTIVEFLAG='Y'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_VEHICLE_HDR> VendorTypeList = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(DT);
            return VendorTypeList;
        }

        #endregion

        #region Tyre Issue Master

        public List<VW_TyreDetails_New> GetTyreDetailVehicleWIse(string CheckVhNo)
        {
            string SQRY = "exec USP_Get_TyreDetail_VehicleWIse_New'" + CheckVhNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<VW_TyreDetails_New> MRList_Data = DataRowToObject.CreateListFromTable<VW_TyreDetails_New>(Dt1);
            return MRList_Data;
        }

        public List<VW_TyreDetails_New> GetTyreDetailBranchWIse(string BaseLocation)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_Get_TyreDetail_BranchWIse_NEW'" + BaseLocation + "'";
            string SQRY = "exec USP_Get_TyreDetail_BranchWIse_NEW_NewPortal'" + BaseLocation + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<VW_TyreDetails_New> MRList_Data = DataRowToObject.CreateListFromTable<VW_TyreDetails_New>(Dt1);
            return MRList_Data;
        }

        public List<VW_TyreDetails_New> GetTyreDetailBranchWIseBasedonVehicle(string BaseLocation, string VehicelNo)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_Get_TyreDetail_BranchWIse_BasedonVehicle_NEW'" + BaseLocation + "','" + VehicelNo + "'";
            string SQRY = "exec USP_Get_TyreDetail_BranchWIse_BasedonVehicle_NEW_NewPortal'" + BaseLocation + "','" + VehicelNo + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<VW_TyreDetails_New> MRList_Data = DataRowToObject.CreateListFromTable<VW_TyreDetails_New>(Dt1);
            return MRList_Data;
        }

        public DataTable UpdateDismountTyre(string CheckVhNo, decimal RemovalKM, string TYRE_ID, string BaseUserName, decimal DistanceCovered, decimal PerKMCost, string BaseLocationCode, string ChkDate, decimal LifeOfTyre)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_UpdateDismountTyreDtl_New '" + CheckVhNo + "','" + RemovalKM + "','" + TYRE_ID + "','" + BaseUserName + "','" + DistanceCovered + "','" + PerKMCost + "','" + BaseLocationCode + "','" + ChkDate + "','" + LifeOfTyre + "'";
            string SQRY = "exec USP_UpdateDismountTyreDtl_New_NewPortal '" + CheckVhNo + "','" + RemovalKM + "','" + TYRE_ID + "','" + BaseUserName + "','" + DistanceCovered + "','" + PerKMCost + "','" + BaseLocationCode + "','" + ChkDate + "','" + LifeOfTyre + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UpdateDismountTyre", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable UpdateTyre(string CheckVhNo, string ChkDate, decimal RemovalKM, string TYRE_ID, string BaseUserName, string BaseLocationCode, decimal LifeOfTyre)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_UpdateTyreDtl '" + CheckVhNo + "','" + ChkDate + "','" + RemovalKM + "','" + TYRE_ID + "','" + BaseUserName + "','" + BaseLocationCode + "','" + LifeOfTyre + "'";
            string SQRY = "exec USP_UpdateTyreDtl_NewPortal '" + CheckVhNo + "','" + ChkDate + "','" + RemovalKM + "','" + TYRE_ID + "','" + BaseUserName + "','" + BaseLocationCode + "','" + LifeOfTyre + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UpdateTyre", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetValid_Fitment(string TruckNo)
        {
            string SQRY = "exec USP_CHEAKFITMENTTIME '" + TruckNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Old Stoak Master

        public List<WEBX_FLEET_TYREMST_HISTORY> GetOldTyreList()
        {
            /* Comment For Live */
            //string SQRY = "exec  USP_OldTyre_List_new";
            string SQRY = "exec  USP_OldTyre_List_NEW_NewPortal";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_TYREMST_HISTORY> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST_HISTORY>(Dt1);
            return MRList_Data;
        }

        public DataTable Internalsubmit(string TYRE_ID, string Suffix, string Actions, string UserName, string BaseLocation, decimal LifeofTyreRemaining)
        {
            //string SQRY = "exec  USP_UpdateOldTyreDtl'" + TYRE_ID + "','" + "I" + "','" + "INTERNAL" + "'";
            /* Comment For Live */
            //string SQRY = "exec  USP_UpdateOldTyreDtl_New'" + TYRE_ID + "','" + "I" + "','" + "INTERNAL" + "','" + UserName + "','" + BaseLocation + "','" + LifeofTyreRemaining + "'";
            string SQRY = "exec  USP_UpdateOldTyreDtl_New_NewPortal'" + TYRE_ID + "','" + "I" + "','" + "INTERNAL" + "','" + UserName + "','" + BaseLocation + "','" + LifeofTyreRemaining + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Internalsubmit", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public List<WEBX_FLEET_TYREMST_HISTORY> GetTyresListClaim(string TYRE_ID)
        {
            /* Comment For Live */
            //string SQRY = "exec  USP_TyresList_Claim '" + TYRE_ID + "'";
            string SQRY = "exec  USP_TyresList_Claim_NewPortal '" + TYRE_ID + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_TYREMST_HISTORY> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST_HISTORY>(Dt1);
            return MRList_Data;
        }

        public List<WEBX_FLEET_TYREMST_HISTORY> GetTyresListRemold(string TYRE_ID)
        {
            /* Comment For Live */
            //string SQRY = "exec  USP_TyresList_Remold '" + TYRE_ID + "'";
            string SQRY = "exec  USP_TyresList_Remold_NewPortal '" + TYRE_ID + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_TYREMST_HISTORY> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST_HISTORY>(Dt1);
            return MRList_Data;
        }
        public List<WEBX_FLEET_TYREMST_HISTORY> GetTyresListSale(string TYRE_ID)
        {
            /* Comment For Live */
            //string SQRY = "exec  USP_TyresList_Sale '" + TYRE_ID + "'";
            string SQRY = "exec  USP_TyresList_Sale_NewPortal '" + TYRE_ID + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_TYREMST_HISTORY> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST_HISTORY>(Dt1);
            return MRList_Data;
        }

        public DataTable Claimsubmit(string TYRE_ID, string CLAIM_REMOLD_SALE_ID, string ClaimDT, string BaseLocationCode, string VENDORCODE, string TYRE_STATUS, string ACTIONS, decimal CLAIM_AMT, string Remark, string TYRE_ACTIVEFLAG, string Suffix, decimal Dist_Covered, string BaseUserName)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_TYRE_CLAIM_UPDATE '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + ClaimDT + "','" + BaseLocationCode + "','" + VENDORCODE + "','" + "Pending" + "','" + "CLAIM" + "','" + CLAIM_AMT + "', '" + Remark + "','" + "N" + "','" + "c" + "','" + Dist_Covered + "','" + BaseUserName + "'";
            string SQRY = "exec USP_TYRE_CLAIM_UPDATE_NewPortal '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + ClaimDT + "','" + BaseLocationCode + "','" + VENDORCODE + "','" + "Pending" + "','" + "CLAIM" + "','" + CLAIM_AMT + "', '" + Remark + "','" + "N" + "','" + "c" + "','" + Dist_Covered + "','" + BaseUserName + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Claimsubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable TyreRemoldsubmit(string TYRE_ID, string CLAIM_REMOLD_SALE_ID, string ClaimDT, string BaseLocationCode, string VENDORCODE, string TYRE_STATUS, string ACTIONS, string Suffix, decimal Dist_Covered, string BaseUserName)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_TYRE_REMOLD_UPDATE '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + ClaimDT + "','" + BaseLocationCode + "','" + VENDORCODE + "','" + "Pending" + "','" + "REMOLD" + "','" + "R" + "','" + Dist_Covered + "','" + BaseUserName + "'";
            string SQRY = "exec USP_TYRE_REMOLD_UPDATE_NewPortal '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + ClaimDT + "','" + BaseLocationCode + "','" + VENDORCODE + "','" + "Pending" + "','" + "REMOLD" + "','" + "R" + "','" + Dist_Covered + "','" + BaseUserName + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "TyreRemoldsubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable TyreSalesubmit(string TYRE_ID, string CLAIM_REMOLD_SALE_ID, string VENDORCODE, string TYRE_STATUS, decimal SALE_AMT, string MobileNo, string BaseLocationCode, string PaymentMode, string CashLedger, string ChequeNo, DateTime ChequeDate, decimal ChequeAmount, decimal CashAmount, string Accode, string BankLedger, decimal PayAmount, string BaseUserName)
        {
            /* Comment For Live */
            //string SQRY = "exec USP_TyreSale_Payment '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + VENDORCODE + "','" + "SALE" + "','" + SALE_AMT + "','" + MobileNo + "','" + BaseLocationCode + "','" + PaymentMode + "','" + CashLedger + "','" + ChequeNo + "','" + ChequeDate + "','" + ChequeAmount + "','" + CashAmount + "','" + Accode + "','" + BankLedger + "','" + PayAmount + "','" + BaseUserName + "'";
            string SQRY = "exec USP_TyreSale_Payment_NewPortal '" + TYRE_ID + "', '" + CLAIM_REMOLD_SALE_ID + "','" + VENDORCODE + "','" + "SALE" + "','" + SALE_AMT + "','" + MobileNo + "','" + BaseLocationCode + "','" + PaymentMode + "','" + CashLedger + "','" + ChequeNo + "','" + ChequeDate + "','" + ChequeAmount + "','" + CashAmount + "','" + Accode + "','" + BankLedger + "','" + PayAmount + "','" + BaseUserName + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "TyreSalesubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }


        #endregion

        #region Update Remold Tyre

        public List<UpdateRemoldTyreList> Get_RemoldList(string FromDate, string ToDate, string VendorCode, string BillNo)
        {
            /* Comment For Live */
            //string QueryString = "exec USP_RemoldList '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";
            string QueryString = "exec USP_RemoldList_NewPortal '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<UpdateRemoldTyreList> ItemList = DataRowToObject.CreateListFromTable<UpdateRemoldTyreList>(DT);
            return ItemList;
        }

        public List<RemoldTyreList_DET> Get_RemoldListDet(string id)
        {
            /* Comment For Live */
            //string QueryString = "exec USP_RemoldList_Dtls '" + id + "'";
            string QueryString = "exec USP_RemoldList_Dtls_NewPortal '" + id + "'";

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<RemoldTyreList_DET> ItemList = DataRowToObject.CreateListFromTable<RemoldTyreList_DET>(DT);
            return ItemList;
        }

        public DataTable GENERATE_BILLNO(string Brcd, string Finyear)
        {
            /* Comment For Live */
            //string Str = "EXEC USP_GEN_REMOLD_BILLNO '" + Brcd + "','" + Finyear + "'";
            string Str = "EXEC USP_GEN_REMOLD_BILLNO_NewPortal '" + Brcd + "','" + Finyear + "'";

            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        public DataTable UpdateRemoldTyreSubmit(int SR_NO, string TYRE_ID, string BILL_NO, string CLAIM_REMOLD_SALE_ID, string STATUS, string VendorCode, double REMOLD_AMT, string Rejection, string REMOLD_YN, string BaseLocationCode, decimal StaxOnAmount, string GSTType, decimal GSTPercentage, decimal NETAMT, string StateCode, decimal StaxRate, decimal TDSRate, decimal TDSAmount, string TDSAcccode, string TDSAccdesc, string PANNO, string BaseUserName, string BaseCompanyCode, string BaseFinYear)
        {
            string Str = "exec USP_TyreRemold_Payment_NewPortal '" + SR_NO + "','" + TYRE_ID + "','" + BILL_NO + "','" + CLAIM_REMOLD_SALE_ID + "','" + STATUS + "','" + VendorCode + "','" + REMOLD_AMT + "','" + Rejection + "','" + REMOLD_YN + "','" + BaseLocationCode + "','" + StaxOnAmount + "','" + GSTType + "','" + GSTPercentage + "','" + NETAMT + "','" + StateCode + "','" + StaxRate + "','" + TDSRate + "','" + TDSAmount + "','" + TDSAcccode + "','" + TDSAccdesc + "','" + PANNO + "','" + BaseUserName + "','" + BaseCompanyCode + "','" + BaseFinYear + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "UpdateRemoldTyreSubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        #endregion

        #region Update Claim of Tyre

        public List<RemoldTyreList_DET> Get_ClaimTyreList(string FromDate, string ToDate, string VendorCode, string BillNo)
        {
            /* Comment For Live */
            //string QueryString = "exec USP_ClaimList '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";
            string QueryString = "exec USP_ClaimList_NewPortal '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<RemoldTyreList_DET> ItemList = DataRowToObject.CreateListFromTable<RemoldTyreList_DET>(DT);
            return ItemList;
        }

        public DataTable UpdateClimeTyreSubmit(int SR_NO, string TYRE_ID, string CLAIM_REMOLD_SALE_ID, string VendorCode, string STATUS, double REMOLD_AMT, string Rejection, string REMOLD_YN, string BaseLocationCode, string PaymentMode, string ChequeNo, string ChequeDate, decimal ChequeAmount, decimal CashAmount, string CashLedger, string BankLedgerName, decimal NetPay, string BaseUserName)
        {
            /* Comment For Live */
            //string Str = "exec USP_TyreClaim_Payment '" + SR_NO + "','" + TYRE_ID + "','" + CLAIM_REMOLD_SALE_ID + "','" + VendorCode + "','" + STATUS + "','" + REMOLD_AMT + "','" + Rejection + "','" + REMOLD_YN + "','" + BaseLocationCode + "' ,'" + PaymentMode + "','','" + ChequeNo + "','" + ChequeDate + "','" + ChequeAmount + "','" + CashAmount + "','" + CashLedger + "','" + BankLedgerName + "','" + NetPay + "','" + BaseUserName + "'";
            string Str = "exec USP_TyreClaim_Payment_NewPortal '" + SR_NO + "','" + TYRE_ID + "','" + CLAIM_REMOLD_SALE_ID + "','" + VendorCode + "','" + STATUS + "','" + REMOLD_AMT + "','" + Rejection + "','" + REMOLD_YN + "','" + BaseLocationCode + "' ,'" + PaymentMode + "','','" + ChequeNo + "','" + ChequeDate + "','" + ChequeAmount + "','" + CashAmount + "','" + CashLedger + "','" + BankLedgerName + "','" + NetPay + "','" + BaseUserName + "'";

            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "UpdateClimeTyreSubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        #endregion

        #region Remold Tyre Bill Payment

        public List<RemoldTyreBillPayment> Get_RemoldTyreBillPaymentList(string FromDate, string ToDate, string VendorCode, string BillNo)
        {
            /* Comment For Live */
            //string QueryString = "exec USP_RemoldBillList '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";
            string QueryString = "exec USP_RemoldBillList_NewPortal '" + FromDate + "','" + ToDate + "','" + VendorCode + "','" + BillNo + "'";

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<RemoldTyreBillPayment> ItemList = DataRowToObject.CreateListFromTable<RemoldTyreBillPayment>(DT);
            return ItemList;
        }

        public DataTable NextVoucherno(string Brcd, string Finyear)
        {
            /* Comment For Live */
            //String Str = "EXEC usp_next_VoucherNo_Wo_Output '" + Brcd + "','" + Finyear + "',''";
            string Str = "EXEC usp_next_VoucherNo_Wo_Output_NewPortal '" + Brcd + "','" + Finyear + "',''";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        public DataTable InsertInToWEBX_VENDORBILL_PAYDET(string voucherNo, decimal CurrentAmt, double pendamt, string BILLNO)
        {
            String Str = "INSERT INTO [WEBX_VENDORBILL_PAYDET] SELECT [BILLNO],[BILLSF],[BRCD],[BILLDT],[VENDORCODE],[VENDORNAME],[VENDORBILLDT],[VENDORBILLNO],[DUEDT],[PAYDT],'" + voucherNo + "',[NETAMT]," + CurrentAmt + "," + pendamt + "  FROM  WEBX_vendorbill_hdr WHERE BILLNO='" + BILLNO + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "InsertInToWEBX_VENDORBILL_PAYDET", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        public DataTable UpdateVendorbill_det(decimal CurrentAmt, string selectDate, string voucherNo, string BILLNO)
        {
            String Str = "update WEBX_vendorbill_hdr set pendamt=isnull(pendamt,0)-" + CurrentAmt + ",finclosedt='" + selectDate + "',voucherNo='" + voucherNo + "',PAYDT='" + selectDate + "' where BillNo='" + BILLNO + "'";

            string sql_detail = "update WEBX_vendorbill_det set voucherNo='" + voucherNo + "',PAYDT='" + selectDate + "' where BillNo='" + BILLNO + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''") + " - " + sql_detail.Replace("'", "''"), "UpdateVendorbill_det", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Str);
            DataTable Dt1 = GF.GetDataTableFromSP(sql_detail);
            return Dt;
        }

        public DataTable UpdateVendorbill_hdr(decimal CurrentAmt, string selectDate, string voucherNo, string BILLNO)
        {
            String Str = "update WEBX_vendorbill_hdr set pendamt=isnull(pendamt,0)-" + CurrentAmt + ",finclosedt='" + selectDate + "',voucherNo='" + voucherNo + "',PAYDT='" + selectDate + "' where BillNo='" + BILLNO + "'";
            int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "UpdateVendorbill_hdr", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        public string Remold_Tyre_Bill_payments(string Xml_Other_Details)
        {
            string SQRY = "exec USP_RemoldTyreBill_payments '" + Xml_Other_Details.Replace("&", "&amp;").Trim() + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Remold_Tyre_Bill_payments", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            string GenVoucherno = "";
            try
            {
                GenVoucherno = DT.Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }

        #endregion

        #region TripSheet View and print

        public List<WEBX_FLEET_VEHICLE_ISSUE> Get_TripSheetView_List_By_Id(string TreepId, string Type, string FromDate, string ToDate, string TripSheetStatus, string Branch, string Driverwise, string Vehiclewise)
        {
            string SQRY = "USP_VEHICLEISSUEVIEWPRINT_Query_NewPortal'" + TreepId + "','" + FromDate + "','" + ToDate + "','" + TripSheetStatus + "','" + Type + "','" + Branch + "','" + Driverwise + "','" + Vehiclewise + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_VEHICLE_ISSUE> ItemList = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(DT);
            return ItemList;
        }

        public List<Webx_Master_General> GetBranchFrom(string Serach)
        {
            string SQRY = "select top 1 CodeId='All',CodeDesc='All' from webx_location union select loccode,LOCNAME=LocCode+' : '+LocName  from webx_location where activeFlag='Y'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> ListName = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1);

            ListName = ListName.ToList();

            return ListName;
        }
        #endregion

        #region View / Print Job Order

        public DataTable GetJobOrderViewList(JobOrderCriteria VM, string CompanyCode)
        {
            string SQR = "exec Usp_GetJobOrderViewList '" + VM.JobOrderNo + "','" + VM.FromDate + "','" + VM.ToDate + "','" + VM.OrderStatus + "','" + CompanyCode + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion


        public List<WEBX_FLEET_DRIVERMST> GetFreeDriverForTrip(string BRCD, string UserName)
        {
            string SQRY = "EXEC USP_GetFreeDriverForTrip '" + BRCD + "','" + UserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DRIVERMST> DriverMstList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(Dt);
            return DriverMstList;
        }

        public DataSet UploadTripAdvanceEntryXls(string XML, string BaseCompanyCode, string Username)
        {
            string SQRY = "exec USP_Insert_EnroutExpanceFromXlsUpload '" + XML + "','" + BaseCompanyCode + "','" + Username + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UploadTripAdvanceEntryXls", "", "");
            return GF.getdatasetfromQuery(SQRY);
        }
    }
}
