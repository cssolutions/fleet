﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.ServiceModel.Web;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Fleet.Classes
{
    public class GeneralFuncations
    {
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        public string GetConnstr()
        {
            return Connstr;
        }

        public string executeScalerQuery(string Squery)
        {
            string Scaler = "";
            var conn = new SqlConnection(Connstr);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = Squery;
                cmd.CommandType = CommandType.Text;
                Scaler =Convert.ToString(cmd.ExecuteScalar());
                conn.Close();
                conn.Dispose();
            }
            return Scaler;
        }

        public DataTable GetDataTableFromObjects(object[] objects)
        {
            if (objects != null && objects.Length > 0)
            {
                Type t = objects[0].GetType();
                DataTable dt = new DataTable(t.Name);
                foreach (PropertyInfo pi in t.GetProperties())
                {
                    dt.Columns.Add(new DataColumn(pi.Name));
                }
                foreach (var o in objects)
                {
                    DataRow dr = dt.NewRow();
                    foreach (DataColumn dc in dt.Columns)
                    {
                        dr[dc.ColumnName] = o.GetType().GetProperty(dc.ColumnName).GetValue(o, null);
                    }
                    dt.Rows.Add(dr);
                }
                return dt;
            }
            return null;
        }

        public void executeNonQuery(string Squery)
        {
            var conn = new SqlConnection(Connstr);
            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = Squery;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                conn.Close();
                conn.Dispose();
            }
        }

        public DataTable getdatetablefromQuery(string Squery)
        {
            DataTable dataTable = new DataTable();
            dataTable.TableName = "datatable1";
            var conn = new SqlConnection(Connstr);

            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = Squery;//
                cmd.CommandTimeout = 6000;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                // this will query your database and return the result to your datatable

                  da.Fill(dataTable);
                da.Dispose();
                conn.Close();
            }
            return dataTable;
        }

        public DataTable GetDataTableFromSP(string Squery)
        {
            GeneralFuncations GF = new GeneralFuncations();
            DataTable dataTable = GF.getdatetablefromQuery(Squery);
            return dataTable;
        }

        public DataSet GetDataSetFromSP(string Squery)
        {
            GeneralFuncations GF = new GeneralFuncations();
            DataSet DS = GF.getdatasetfromQuery(Squery);
            return DS;
        }

        public DataSet getdatasetfromQuery(string Squery)
        {
            DataSet dataTable = new DataSet();
            // dataTable.TableName = "datatable1";
            var conn = new SqlConnection(Connstr);
            using (var cmd = conn.CreateCommand())
            {
                cmd.CommandText = Squery;//
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                // this will query your database and return the result to your datatable
                da.Fill(dataTable);
                da.Dispose();
                conn.Close();
            }
            return dataTable;
        }

        public string StreamToJsonString(Stream jsonStream)
        {
            Encoding encoding = Encoding.UTF8;

            // Read the stream into a byte array
            byte[] data = ToByteArray(jsonStream);

            // Copy to a string for header parsing
            string content = encoding.GetString(data);

            // The first line should contain the delimiter
            int delimiterEndIndex = content.IndexOf("\r\n");

            if (delimiterEndIndex > -1)
            {
                string delimiter = content.Substring(0, content.IndexOf("\r\n"));
                content = content.Replace(delimiter, "");
                delimiter = content.Substring(0, content.IndexOf("\r\n\r\n"));
                content = content.Replace(delimiter, "").Replace("\r\n\r\n", "");
                delimiterEndIndex = content.IndexOf("------");
                content = content.Replace("--", "");
            }
            return content;
        }

        private byte[] ToByteArray(Stream stream)
        {
            byte[] buffer = new byte[32768];
            using (MemoryStream ms = new MemoryStream())
            {
                while (true)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return ms.ToArray();
                    ms.Write(buffer, 0, read);
                }
            }
        }

        public List<T> DictionaryToObjectList<T>(object[] objArr) where T : new()
        {
            List<T> t = new List<T>();
            foreach (var itm in objArr)
            {
                T CMQ = DictionaryToObject<T>((Dictionary<string, object>)itm);
                t.Add(CMQ);
            }
            return t;
        }

        public T DictionaryToObject<T>(IDictionary<string, object> dict) where T : new()
        {
            T t = new T();
            PropertyInfo[] properties = t.GetType().GetProperties();
            int i = 0;
            foreach (PropertyInfo property in properties)
            {
                int j = i;
                if (!dict.Any(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase)))
                    continue;
                KeyValuePair<string, object> item = dict.First(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase));
                Type tPropertyType = t.GetType().GetProperty(property.Name).PropertyType;
                Type newT = Nullable.GetUnderlyingType(tPropertyType) ?? tPropertyType;
                if (item.Value != null)
                {
                    object newA = Convert.ChangeType(item.Value, newT);
                    t.GetType().GetProperty(property.Name).SetValue(t, newA, null);
                }
                i++;
            }
            return t;
        }

        public Stream ReturnJSONStream<T>(T t)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string returnValue = jSearializer.Serialize(t);
            var jsonObj = new
            {
                Success = 1,
                Message = "",
                Response = t
            };
            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            returnValue = jScriptSerializer.Serialize(jsonObj);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        public string GetTypeInt(int Id)
        {
            string Type = "";

            if (Id == 0)
            {
                Type = "Add";
            }
            else
            {
                Type = "Edit";
            }

            return Type;
        }

        public string FormateDate(DateTime date)
        {
            return ((date != null && date != DateTime.MinValue) ? date.ToString("dd MMM yy") : "");
        }

        public string FormateDateWithFullYear(DateTime date)
        {
            return ((date != null && date != DateTime.MinValue) ? date.ToString("dd MMM yyyy") : "");
        }
        public string FormateString(string str)
        {
            return ((str != null && str != "") ? str.Trim() : "");
        }

        public string NextKeyCode(string KeyCode)
        {
            byte[] ASCIIValues = ASCIIEncoding.ASCII.GetBytes(KeyCode);
            int StringLength = ASCIIValues.Length;
            bool isAllZed = true;
            bool isAllNine = true;
            //Check if all has ZZZ.... then do nothing just return empty string.

            for (int i = 0; i < StringLength - 1; i++)
            {
                if (ASCIIValues[i] != 90)
                {
                    isAllZed = false;
                    break;
                }
            }
            if (isAllZed && ASCIIValues[StringLength - 1] == 57)
            {
                ASCIIValues[StringLength - 1] = 64;
            }

            // Check if all has 999... then mak/e it A0
            for (int i = 0; i < StringLength; i++)
            {
                if (ASCIIValues[i] != 57)
                {
                    isAllNine = false;
                    break;
                }
            }
            if (isAllNine)
            {
                ASCIIValues[StringLength - 1] = 47;
                ASCIIValues[0] = 65;
                for (int i = 1; i < StringLength - 1; i++)
                {
                    ASCIIValues[i] = 48;
                }
            }


            for (int i = StringLength; i > 0; i--)
            {
                if (i - StringLength == 0)
                {
                    ASCIIValues[i - 1] += 1;
                }
                if (ASCIIValues[i - 1] == 58)
                {
                    ASCIIValues[i - 1] = 48;
                    if (i - 2 == -1)
                    {
                        break;
                    }
                    ASCIIValues[i - 2] += 1;
                }
                else if (ASCIIValues[i - 1] == 91)
                {
                    ASCIIValues[i - 1] = 65;
                    if (i - 2 == -1)
                    {
                        break;
                    }
                    ASCIIValues[i - 2] += 1;

                }
                else
                {
                    break;
                }

            }
            KeyCode = ASCIIEncoding.ASCII.GetString(ASCIIValues, 0, ASCIIValues.Length);
            return KeyCode;
        }

        public string NextKeyCode(string KeyCode, int IncreaseBy)
        {
            string strRet = KeyCode;
            string newCode = KeyCode;
            for (int i = 0; i < IncreaseBy; i++)
            {
                newCode = NextKeyCode(newCode);
            }
            strRet = newCode;
            return strRet;
        }

        public string getNextSuffix(string suffix)
        {
            string str = suffix;

            if (str == ".")
                str = "A";
            else
                str = NextKeyCode(suffix, 1);
            return str;
        }

        #region Serialize Deserialize

        public void SerializeParams<T>(List<T> paramList, string folderPath)
        {
            XDocument doc = new XDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(paramList.GetType());
            System.Xml.XmlWriter writer = doc.CreateWriter();
            serializer.Serialize(writer, paramList);
            writer.Close();
           doc.Save(folderPath);
        }

        public List<T> DeserializeParams<T>(string folderPath)
        {
            XDocument doc = XDocument.Load(folderPath);
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));
            System.Xml.XmlReader reader = doc.CreateReader();
            List<T> result = (List<T>)serializer.Deserialize(reader);
            reader.Close();
            return result;
        }

        public Stream ReturnJSONStreamError<T>(T t, string ErrorMessage)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jSearializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            string returnValue = jSearializer.Serialize(t);

            if (ErrorMessage == null)
                ErrorMessage = "Some Error in Submission";

            var jsonObj = new
            {
                Success = 0,
                Message = ErrorMessage,
                Response = t
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

            return memoryStream;
        }

        #endregion

        #region Save SQL to Table

        public int SaveRequestServices(string SaveXMLString, string ModuleName, string Succes, string ErrorMessage)
        {
            int id = 0;
            DataTable Dt = new DataTable();
            string sql = "";

            try
            {
                sql = "exec Usp_CalledServicesAndriod '" + SaveXMLString + "','" + ModuleName + "','" + Succes + "','" + ErrorMessage + "'";

                Dt = GetDataTableFromSP(sql);
                id = Convert.ToInt32(Dt.Rows[0][0].ToString());

            }
            catch (Exception)
            {
                //  throw;
            }

            return id;
        }

        #endregion

        public string Encrypt(string strToEncrypt, string strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
                return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        public string Decrypt(string strEncrypted, string strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = Convert.FromBase64String(strEncrypted);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objDESCrypto = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        public void deleteAllFile()
        {
            try
            {
                if (Directory.Exists(folderPath))
                {
                    System.IO.DirectoryInfo directory = new System.IO.DirectoryInfo(folderPath);
                    foreach (FileInfo file in directory.GetFiles())
                    {
                        file.IsReadOnly = false;
                        file.Delete();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public T StreamToObject<T>(Stream jsonStream)
        {
            Encoding encoding = Encoding.UTF8;
            // Copy to a string for header parsing


            // Read the stream into a byte array
            byte[] data = ToByteArray(jsonStream);

            // Copy to a string for header parsing
            string content = encoding.GetString(data);

            // The first line should contain the delimiter
            int delimiterEndIndex = content.IndexOf("\r\n");

            if (delimiterEndIndex > -1)
            {
                string delimiter = content.Substring(0, content.IndexOf("\r\n"));


                content = content.Replace(delimiter, "");

                delimiter = content.Substring(0, content.IndexOf("\r\n\r\n"));

                content = content.Replace(delimiter, "").Replace("\r\n\r\n", "");


                delimiterEndIndex = content.IndexOf("------");
                content = content.Replace("--", "");

                // delimiter = content.Substring(delimiterEndIndex, content.Length);

                //  content = content.Replace(delimiter, "");

            }


            T obj = Activator.CreateInstance<T>();

            obj = JsonConvert.DeserializeObject<T>(content);

            //MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(content));
            //DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            //obj = (T)serializer.ReadObject(ms);
            //ms.Close();
            return obj;

        }

        public Stream ReturnJSONStream_DT<T>(T t, DataTable DT)
        {
            System.Web.Script.Serialization.JavaScriptSerializer jSearializer =
            new System.Web.Script.Serialization.JavaScriptSerializer();
            string returnValue = jSearializer.Serialize(t);

            var jsonObj = new
            {
                Success = DT.Rows[0]["Statuss"].ToString(),
                Message = DT.Rows[0]["Message"].ToString(),
                Response = t
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

            return memoryStream;
        }

        public DataSet getdatasetFromParams(string Squery, IDictionary<string, string> parameters)
        {
            DataSet dataTable = new DataSet();
            // dataTable.TableName = "datatable1";
            var conn = new SqlConnection(Connstr);

            using (var cmd = conn.CreateCommand())
            {

                cmd.CommandText = Squery;//
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 100000;
                foreach (var itm in parameters)
                {
                    cmd.Parameters.AddWithValue("@" + itm.Key, itm.Value);
                }
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                conn.Close();
                // this will query your database and return the result to your datatable
                da.Fill(dataTable);

                da.Dispose();
                conn.Close();
            }
            return dataTable;
        }

        //public DataTable getdatetablefromQuery_Async(string Squery)
        //{
        //    DataTable dataTable = new DataTable();
        //    dataTable.TableName = "datatable1";
        //    var conn = new SqlConnection(Connstr);
            
        //    using (var cmd = conn.CreateCommand())
        //    {
        //        cmd.CommandTimeout = 120;
        //        cmd.CommandText = Squery;//
        //        SqlDataAdapter da = new SqlDataAdapter(cmd);
        //        conn.Close();
        //        // this will query your database and return the result to your datatable

        //        da.Fill(dataTable);
        //        da.Dispose();
        //        conn.Close();
        //    }
        //    return dataTable;
        //}

        Random random = new Random();
        public const string Alphabet = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


        public string GenerateString(int size)
        {
            char[] chars = new char[size];
            for (int i = 0; i < size; i++)
            {
                chars[i] = Alphabet[random.Next(Alphabet.Length)];
            }
            return new string(chars);
        }
    }

    public static class ExtensionMethods
    {
        public static string ReplaceSpecialCharacters(this string value)
        {
            // Replace the Special Characters from String.
            //int[] intarry = { 1, 5, 23 };
            //foreach (int a in intarry)
            //    value = value.Replace((char)a, '');
            return value.Replace("&", "&amp;").Replace("'", "&#39;").Replace("’", "&#39;").Replace("”", "").Replace("–", "-").Replace("�", " ").Replace(System.Environment.NewLine, " ").Trim();
        }
    }
}