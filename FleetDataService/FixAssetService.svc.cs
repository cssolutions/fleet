﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using Fleet.ViewModels;
using CYGNUS.ViewModels;
using System.Xml;
using System.Data.SqlClient;
using CYGNUS.Controllers;
using Microsoft.ApplicationBlocks.Data;
using CYGNUS.Models;
using FleetDataService.ViewModels;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OperationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select OperationService.svc or OperationService.svc.cs at the Solution Explorer and start debugging.
    public class FixAssetService : IFixAssetService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region Fix Asset Master

        public List<webx_acctinfo> GetAccountInfoList()
        {
            string SQRY = "SELECT Acccode ,Accdesc+'~'+Company_Acccode AS Accdesc FROM webx_acctinfo WITH(NOLOCK) WHERE Acccategory='Fixed Assets'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> AccountInfoList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt1);
            return AccountInfoList;
        }

        public List<WebxAssetmaster> GetAccountWiseRecord(string AccountCode, string AccountGroup)
        {
            string SQRY = "Exec USP_Asset_Listing '" + AccountCode + "','" + AccountGroup + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WebxAssetmaster> AccountInfoList = DataRowToObject.CreateListFromTable<WebxAssetmaster>(Dt1);
            return AccountInfoList;
        }

        public string getnewassetcd()
        {
            string sql = "Select max(convert(int,substring(assetcd,3,6)))+1 as maxOCAGCD from webx_assetmaster";
            DataTable Dt = GF.GetDataTableFromSP(sql);
            string cd = Dt.Rows[0]["maxOCAGCD"].ToString();

            if (cd.Length == 1)
            {
                cd = "FA" + "0000" + cd;
            }
            else if (cd.Length == 2)
            {
                cd = "FA" + "000" + cd;
            }
            else if (cd.Length == 3)
            {
                cd = "FA" + "00" + cd;
            }
            else if (cd.Length == 4)
            {
                cd = "FA" + "0" + cd;
            }
            else if (cd.Length == 5)
            {
                cd = "FA" + cd;
            }
            return cd;
        }

        public DataTable InsertUpdateAccount(decimal Srno, string DEPMETHOD, string DEPRATE, string dec, string GRPASSTCD, string unit, string category, string username, string prefix, string assetcd, string ExpiryDate)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (Srno > 0)
                {
                    sql = "Update webx_assetmaster set DEPMETHOD ='" + DEPMETHOD + "',DEPRATE ='" + DEPRATE + "', assetname='" + dec + "',GRPASSTCD='" + GRPASSTCD + "',UNITS='" + unit + "',CATEGORY='" + category + "',updtBY='" + username + "',updtON=getdate(),prefix='" + prefix + "',ExpiryDate='" + ExpiryDate + "' where assetcd='" + assetcd + "' ";
                    DataTable Dt = GF.GetDataTableFromSP(sql);
                }
                else
                {
                    string acd = getnewassetcd();
                    sql = "Insert into webx_assetmaster(assetcd,DEPMETHOD,DEPRATE,assetname,GRPASSTCD,UNITS,CATEGORY,ENTRYBY,ENTRYON,prefix,ExpiryDate) values('" + acd + "','" + DEPMETHOD + "','" + DEPRATE + "','" + dec + "','" + GRPASSTCD + "','" + unit + "','" + category + "','" + username + "',getdate(),'" + prefix + "','" + ExpiryDate + "')";
                    DataTable Dt = GF.GetDataTableFromSP(sql);
                }
            }
            catch (Exception)
            {

            }
            return dt;
        }

        #endregion

        #region PO

        public DataTable getnewcd(string brcd, string finyear)
        {
            string sql = "exec WebX_SP_GetNextDocumentCode_FA   '" + brcd + "','" + finyear + "','PO'";
            DataTable Dt = GF.GetDataTableFromSP(sql);
            return Dt;
        }

        public DataTable sp_Insert_PurchseOrderDetail(string Brcd, string XML, string UserName)
        {
            DataTable dt = new DataTable();

            string sql = "exec sp_Insert_PurchseOrderDetail   '" + Brcd + "','" + XML + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(sql.Replace("'", "''"), "FixassetPOInsertProcess", "", "");
            DataTable Dt = GF.GetDataTableFromSP(sql);
            return dt;
        }

        public List<WebxAssetmaster> ListAsserMaster()
        {
            DataTable dt = new DataTable();
            string sql = "select assetcd,assetname from webx_assetmaster WHERE ExpiryDate > GETDATE() OR ExpiryDate is NULL";
            dt = GF.GetDataTableFromSP(sql);
            List<WebxAssetmaster> ListAsserMaster = DataRowToObject.CreateListFromTable<WebxAssetmaster>(dt);
            return ListAsserMaster;
        } 

        public DataTable InsertPoList(string XML_Det, string BaseUserName, string BaseLocationCode)
        {
            string SQRY = "exec USP_InsertPoList '" + XML_Det + "','" + BaseUserName + "','" + BaseLocationCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertPoList", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable InsertFAPoList(string XML_HDR,string XML_Det, string BaseUserName, string BaseLocationCode, string BaseFinYear)
        {
            string SQRY = "exec USP_InsertPoListGST_Cygnus '" + XML_HDR + "','" + XML_Det + "','" + BaseUserName + "','" + BaseLocationCode + "','" + BaseFinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertPoListGST", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Fix Asset Exception View Print Report

        public List<vm_FixAssetList> Get_PO_Listing_For_View_Print(string Type, string VendorCode, string FromDate, string ToDate, string GRNCode)
        {
            string QueryString = "";

            if (Type == "0")
            {
                QueryString = "exec USP_PO_Listing_For_View_Print '" + Type + "','" + VendorCode + "','" + FromDate + "','" + ToDate + "','NA'";
            }
            if (Type == "1")
            {
                QueryString = "exec USP_PO_Listing_For_View_Print '" + Type + "','NA','NA','NA','" + GRNCode + "'";
            }

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vm_FixAssetList> ItemList = DataRowToObject.CreateListFromTable<vm_FixAssetList>(DT);
            return ItemList;
        }

        public List<vm_PoCancellationList> Get_PO_Listing_For_Cancellation(string BillNo, string VendorCode, string FromDate, string ToDate, string location)
        {
            string QueryString = "";
            QueryString = "exec usp_PO_Listing_For_Cancellation '" + BillNo + "','" + VendorCode + "','" + FromDate + "','" + ToDate + "','" + location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vm_PoCancellationList> ItemList = DataRowToObject.CreateListFromTable<vm_PoCancellationList>(DT);
            return ItemList;
        }

        public DataTable POCancellationSubmit(string selectDate, string pocode, string Reason, string BaseUserName, string BaseComapny)
        {
            //String Str;
            //Str = "UPDATE DBO.webx_POASSET_HDR WITH(ROWLOCK) SET POSTATUS='PO Cancelled',cancelled ='Y',cancelled_by='" + BaseUserName;
            //Str = Str + "',cancelled_dt= '" + selectDate + "' WHERE pocode='" + pocode + "';";
            //Str = Str + " Exec usp_PO_Transaction 3,'" + pocode + "','" + BaseFinYear + "','" + BaseCompanyCode + "'";
            //DataTable Dt = GF.GetDataTableFromSP(Str);
            //return Dt;
            string QueryString = "exec USP_POEntry_Cancellation_Cygnus '" + pocode + "','" + selectDate + "','" + Reason + "','" + BaseUserName + "','" + BaseComapny + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Goods Reciept Note Cretira

        public List<POBillEntry> GetPOForBillEntry(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC usp_PO_ListingGST_Cygnus '" + FBE.VehicleNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);
            return ItemList;
        }

        public List<POBillEntry> GetPOPendingList(string PoCode)
        {
            string QueryString = "EXEC usp_PO_Asset_Listing '" + PoCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);
            return ItemList;
        }

        public DataTable InsertUpdateBill(string GRNNO, string pocd, string actcd, string narration, string POCode, decimal qt, decimal rt, decimal OtherCharge, decimal tot, string usernmae, string brcd)
        {
            string sql = ""; double Total_Amt = 0, Total_Qty = 0;
            DataTable dt = new DataTable();

            Total_Amt = Total_Amt + Convert.ToDouble(tot);
            Total_Qty = Total_Qty + Convert.ToDouble(qt);

            sql = "INSERT INTO webx_fixedassets_det(FIXEDASSETCD,fixedassetdt,grpcd,qty,rate,OtherCharge,billamt,ENTRYBY,ENTRYdt,balanceqty,location,assign)" +
                "VALUES('" + GRNNO + "',GETDATE(),'" + actcd + "'," + qt + "," + rt + "," + OtherCharge + "," + tot + ",'" + usernmae + "',GETDATE()," + qt + ",'" + brcd + "','N')";

            dt = GF.GetDataTableFromSP(sql);

            sql = "UPDATE webx_POASSET_HDR SET POSTATUS='PO GRN GENERATED',GRNNO='" + GRNNO + "' WHERE pocode='" + pocd + "' ";
            dt = GF.GetDataTableFromSP(sql);

            sql = "UPDATE webx_POASSET_det SET activeflag='Y' WHERE pocode='" + pocd + "' AND assetcd='" + actcd + "'";
            dt = GF.GetDataTableFromSP(sql);

            //prefix
            string prefix = "SELECT prefix FROM webx_ASSETMASTER WITH(NOLOCK) WHERE ASSETCD ='" + actcd + "'";
            dt = GF.GetDataTableFromSP(prefix);
            prefix = dt.Rows[0]["prefix"].ToString();

            //grpasstcd
            string grpasstcd = "SELECT GRPASSTCD FROM webx_ASSETMASTER WITH(NOLOCK) WHERE ASSETCD ='" + actcd + "'";
            dt = GF.GetDataTableFromSP(grpasstcd);
            grpasstcd = dt.Rows[0]["GRPASSTCD"].ToString();

            //deprate
            string deprate = "SELECT ISNULL(DEPRATE,0) AS deprate FROM webx_ASSETMASTER WITH(NOLOCK) WHERE ASSETCD ='" + actcd + "'";
            dt = GF.GetDataTableFromSP(deprate);
            deprate = dt.Rows[0]["deprate"].ToString();

            //depmethod
            string depmethod = "SELECT ISNULL(DEPMETHOD,'') AS depmethod FROM webx_ASSETMASTER WITH(NOLOCK) WHERE ASSETCD ='" + actcd + "'";
            dt = GF.GetDataTableFromSP(depmethod);
            depmethod = dt.Rows[0]["depmethod"].ToString();

            double finalamt = Convert.ToDouble(tot) / Convert.ToDouble(qt);

            for (int i = 0; i < Convert.ToDouble(qt); i++)
            {
                string pre = prefix;
                int l = pre.Length;
                sql = "SELECT ISNULL(MAX(RIGHT(allotcd,5)+1),0) AS Maxcode FROM webx_FAALLOT_DET WITH(NOLOCK) WHERE SUBSTRING(allotcd,1," + l + ")='" + pre + "'";
                dt = GF.GetDataTableFromSP(sql);
                string Maxcode = dt.Rows[0]["Maxcode"].ToString();
                double inc = 00001;
                string Postfix = "";
                if (Maxcode == "")
                {
                    Postfix = "00001";
                }
                else
                {
                    if (Maxcode.Length == 1)
                    {
                        Postfix = "0000";
                    }
                    else if (Maxcode.Length == 2)
                    {
                        Postfix = "000";
                    }
                    else if (Maxcode.Length == 3)
                    {
                        Postfix = "00";
                    }
                    else if (Maxcode.Length == 4)
                    {
                        Postfix = "0";
                    }
                    else if (Maxcode.Length >= 5)
                    {
                        Postfix = "";
                    }
                    inc = Convert.ToDouble(Maxcode) + 1;
                }
                Maxcode = pre + Postfix + Maxcode;
                string sql5 = "INSERT INTO webx_faallot_det(allotcd,assetcd,loccode,ENTRYBY,ENTRYON,FIXEDASSETCD,activeflag,billamt,description_detail,grpcd,DEPRATE,DEPmethod) VALUES('" + Maxcode.ToUpper() + "','" + actcd + "','" + brcd + "','" + usernmae + "',GETDATE(),'" + GRNNO + "','N'," + finalamt + ",'" + narration + "','" + grpasstcd + "'," + deprate + ",'" + depmethod + "')";
                dt = GF.GetDataTableFromSP(sql5);
            }
            return dt;
        }

        #endregion	

        #region Assign Asset Cretira

        public List<POBillEntry> GetAssignAssect(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC USP_Asset_Listing_For_Assign '" + FBE.VehicleNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);

            return ItemList;
        }

        public string InsertUpdateAssets(string AssignLoc, string GRCD, string dpartname, string empcode, string instdate, string allocd, string Employee, string desc, string MFGSrNo, string RCPLSrNo)
        {
            string sql = "", Grns = "";
            sql = "exec [usp_InsertUpdateAssets_Cygnus]   '" + AssignLoc + "','" + dpartname + "','" + Employee + "','" + instdate + "','" + desc + "','" + MFGSrNo + "','" + allocd + "','" + GRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(sql);

            //DataTable dt = new DataTable();
            //sql = "update webx_faallot_det set  assign='Y',IsDump ='Y',activeflag='Y',next_loccode='" + AssignLoc + "', next_loccaddress='" + AssignLoc + "',nextloc_dept='" + dpartname + "',assgnempcd='" + Employee + "',instdate='" + instdate + "',description_detail='" + desc + "',MFGSrNo='" + MFGSrNo + "',RCPLSrNo='" + RCPLSrNo + "' where allotcd='" + allocd + "'";
            //dt = GF.GetDataTableFromSP(sql);

            //sql = "update webx_fixedassets_det set assign='Y' where fixedassetcd in('" + GRCD + "') and grpcd='" + allocd + "'";
            //dt = GF.GetDataTableFromSP(sql);

            //sql = "update webx_fixedassets_hdr set postatus='PO Assigned' where fixedassetcd='" + GRCD + "'";
            //dt = GF.GetDataTableFromSP(sql);

            if (Grns == "")
            {
                Grns = "'" + GRCD + "'";
            }
            else
            {
                Grns = Grns + "," + "'" + GRCD + "'";
            }
            return Grns;
        }








        #endregion

        public List<webx_FAALLOT_DET> GetDumpDetail(string FromDate, string ToDate,string ITEMCODE, string LocCode)
        {
            string QueryString = "EXEC Usp_GetDumpDetail_Cygnus '" + FromDate + "','" + ToDate + "','" + ITEMCODE + "','" + LocCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_FAALLOT_DET> ItemList = DataRowToObject.CreateListFromTable<webx_FAALLOT_DET>(DT);
            return ItemList;
        }

        public DataTable InsertFixAssetType(string XML1, string BaseLocationCode, string UserName, string BaseYearVal, string assetcd, string FIXEDASSETCD, string Dump, string IsDump, string ReassignToLocation ,string Type)
        {
            string SQRY = "exec Usp_Insert_FixAssetType_Cygnus '" + XML1 + "','" + BaseLocationCode + "','" + UserName + "','" + BaseYearVal + "','" + FIXEDASSETCD + "','" + assetcd + "','" + Dump + "','" + IsDump + "','" + ReassignToLocation + "','" + Type + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Usp_Insert_FixAssetType", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<WEBX_GENERAL_ASSETMASTER> GetAssetCode()
        {
            string commandText = "Exec USP_GetASSETCODE_Cygnus ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<WEBX_GENERAL_ASSETMASTER> ListLocation = DataRowToObject.CreateListFromTable<WEBX_GENERAL_ASSETMASTER>(dataTable);
            return ListLocation;
        }

        public List<CYGNUS_FixAssetType> GetFixAssetDetail(AssetUTILITYViewModel objViewModel)
        {
            string QueryString = "EXEC Usp_GetFixAssetDetail '" + GF.FormateDate(objViewModel.FromDate) + "','" + GF.FormateDate(objViewModel.ToDate) + "','" + objViewModel.LocCode + "','" + objViewModel.TYPE + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_FixAssetType> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_FixAssetType>(DT);
            return ItemList;
        }

        public List<webx_location> GetReassignLocations(string Prefix)
        {
            string QueryString = "exec USP_Get_Location_Cygnus'" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_location> itmList = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return itmList;
        }

        public List<POBillEntry> GetAssignAssectViewPrint(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC USP_Asset_Listing_For_Assign_ViewPrint '" + FBE.VehicleNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);
            return ItemList;
        }

        public List<CYGNUS_FixAssetType> Get_FixAssetViewPrintListView(string DocumentNo, string GRNNo, string FromDate, string ToDate, string TYPE)
        {
            string QueryString = "exec USP_GetFIXASSETVIEWPRINTDETAILS '" + DocumentNo + "','" + GRNNo + "','" + FromDate + "','" + ToDate + "','" + TYPE + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_FixAssetType> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_FixAssetType>(DT);
            return ItemList;
        }

        public List<CYGNUS_FixAssetType> Get_GRNViewPrintListView(string GRNNo, string FromDate, string ToDate, string VENCD)
        {
            string QueryString = "exec USP_Get_GrnViewPrintList '" + GRNNo + "','" + FromDate + "','" + ToDate + "','" + VENCD + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_FixAssetType> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_FixAssetType>(DT);
            return ItemList;
        }

        #region Wave Off of Fix Assets

        public List<webx_FAALLOT_DET> GetWaveList(string ITEMCODE, string LocCode, string empcode)
        {
            string QueryString = "EXEC Usp_GetWaveOffDetail '" + ITEMCODE + "','" + LocCode + "','" + empcode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_FAALLOT_DET> ItemList = DataRowToObject.CreateListFromTable<webx_FAALLOT_DET>(DT);
            return ItemList;
        }

        public DataTable usp_XML_Wave_Submmit(string xml)
        {
            string Sql = "EXEC [usp_Update_Wava_Change] '" + xml + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }
        #endregion

        public DataTable CheckMFGSrNoDuplicate(string DocNo)
        {
            string sqlstr = "EXEC [CheckMFGSrNoDuplicate_Cygnus] '" + DocNo + "'";
            return GF.GetDataTableFromSP(sqlstr);
        }
    }   
}
