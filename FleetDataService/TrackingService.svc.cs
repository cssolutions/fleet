﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using Fleet.ViewModels;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TrackingService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select TrackingService.svc or TrackingService.svc.cs at the Solution Explorer and start debugging.
    public class TrackingService : ITrackingService
    {
        GeneralFuncations GF = new GeneralFuncations();
        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
        public void DoWork() { }

        #region Operation Track

        public List<Webx_Master_General> GetGeneralMasterObject()
        {
            //string QueryString = "select DOCNO_ID as CodeId ,DOC_Called_AS as CodeDesc  from [Webx_View_Track] where  title='O'  and activeflag='Y' order by title";
            string QueryString = "exec USP_Get_View_Track_Document_Type";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public List<Webx_Master_General> GetSubBranch()
        {
            string QueryString = "select top 1 CodeId='All',CodeDesc='All' from webx_location union select loccode,LOCNAME=LocCode+' : '+LocName  from webx_location ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public List<Webx_Master_General> GetSubBranchFromBranch(string Serach)
        {
            string SQRY = "select top 1 CodeId='All',CodeDesc='All' from webx_location union select loccode,LOCNAME=LocCode+' : '+LocName  from webx_location where Report_loc='" + Serach + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> ListName = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1);

            ListName = ListName.ToList();

            return ListName;
        }

        public List<vw_OperationTrack> Get_ListOfOperationTracking(string FromDate, string ToDate, string From_loc, string MaxLocVal, string To_loc, string MinLocVal, string DOCKNO, string ManualNo, string VehicleNo, string FreeText, string DockType, int iPage, int iDisplayLength)
        {
            string QueryString = "exec webxNet_Tracking_Ver1 '" + FromDate + "','" + ToDate + "','" + From_loc + "','" + MaxLocVal + "','" + To_loc + "','" + MinLocVal + "','" + DOCKNO + "','" + ManualNo + "','" + VehicleNo + "','" + FreeText + "','" + DockType + "','" + iPage + "','" + iDisplayLength + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_OperationTrack> itmList = DataRowToObject.CreateListFromTable<vw_OperationTrack>(DT);
            return itmList;
        }

        #endregion

        #region  Trip Sheet tracking

        public DataTable Get_TripSheet_List_By_Id(string TreepId, string Type)
        {
            string SQRY = "";
            if (Type == "T")
            {
                SQRY = "SELECT * from VW_FLEET_TRIPSHEET_TRACKER where VSlipNo= '" + TreepId + "'";
            }
            if (Type == "M")
            {
                SQRY = " SELECT * from VW_FLEET_TRIPSHEET_TRACKER where Manual_TripsheetNo= '" + TreepId + "'";
            }

            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region  Driver tracking

        public List<VW_DRIVER_FINAL_INFO> GetDriver_NameList()
        {
            string curFile = folderPath + "VW_DRIVER_FINAL_INFO.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_DriverName";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<VW_DRIVER_FINAL_INFO> GetDriver_NameList_XML = DataRowToObject.CreateListFromTable<VW_DRIVER_FINAL_INFO>(Dt);
                GF.SerializeParams<VW_DRIVER_FINAL_INFO>(GetDriver_NameList_XML, folderPath + "VW_DRIVER_FINAL_INFO.xml");
            }
            List<VW_DRIVER_FINAL_INFO> LocationList = GF.DeserializeParams<VW_DRIVER_FINAL_INFO>(folderPath + "VW_DRIVER_FINAL_INFO.xml");

            return LocationList;
        }

        public List<VW_DRIVER_FINAL_INFO> GetDriverList(string DriverId)
        {
            string SQR = "exec USP_GetDriverList '" + DriverId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<VW_DRIVER_FINAL_INFO> listDriver = DataRowToObject.CreateListFromTable<VW_DRIVER_FINAL_INFO>(Dt);
            return listDriver;
        }

        #endregion

        #region  Vehical tracking

        public DataTable GetVehicleList(string vno)
        {
            string SQR = "Select * From VW_Vehicle_Tracking_NewPortal WITH(NOLOCK) Where Vehno= '" + vno + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public List<WEBX_FLEET_DOCUMENT_MST> Get_ListOfDocumentDetails(string vno)
        {
            string QueryString = "SELECT * FROM vw_Document_Register WITH(NOLOCK) WHERE vehno= '" + vno + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_DOCUMENT_MST> itmList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DOCUMENT_MST>(DT);
            return itmList;
        }

        public List<vw_VT_tripsheet> Get_ListOfVehivleTripDetails(string vno)
        {
            string QueryString = "SELECT Convert(VarChar,Vslipdt,106) as Vslipdt,* FROM vw_VT_tripsheet_NewPortal WITH(NOLOCK) WHERE VehicleNo= '" + vno + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_VT_tripsheet> itmList = DataRowToObject.CreateListFromTable<vw_VT_tripsheet>(DT);
            return itmList;
        }

        public List<VW_VEH_FUEL_FILL_HISTORY> Get_ListOfFFHDetails(string vno)
        {
            string QueryString = "SELECT VSlipNo,CONVERT(VARCHAR,BILLDT,106) AS BILLFROMDT,* FROM VW_VEH_FUEL_FILL_HISTORY WITH(NOLOCK) WHERE VEHNO ='" + vno + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VW_VEH_FUEL_FILL_HISTORY> itmList = DataRowToObject.CreateListFromTable<VW_VEH_FUEL_FILL_HISTORY>(DT);
            return itmList;
        }

        public List<VW_ODOMETER_HISTORY> Get_ListOf_ODOMETER_HISTORY(string vno)
        {
            string QueryString = "SELECT *FROM VW_ODOMETER_HISTORY WITH(NOLOCK) WHERE VEHNO ='" + vno + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VW_ODOMETER_HISTORY> itmList = DataRowToObject.CreateListFromTable<VW_ODOMETER_HISTORY>(DT);
            return itmList;
        }

        public List<WEBX_FLEET_TYREMST> Get_ListOf_TyreMst(string vno)
        {
            string QueryString = "SELECT * FROM vw_tyre_register_NewPortal WITH(NOLOCK) WHERE VEHNO ='" + vno + "' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_TYREMST> itmList = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST>(DT);
            return itmList;
        }

        #endregion

        #region  Voucher tracking

        public DataTable GetVoucherExistorNot(string code, string BaseYearVal)
        {
            string SQR = "";
            DataTable Dt = new DataTable();
            SQR = "SELECT COUNT(*) FROM webx_acctrans_" + BaseYearVal + " WHERE  Voucherno='" + code + "'";
            Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }
        public DataTable GetVoucherType(string code, string BaseYearVal)
        {
            string SQR = "";
            DataTable Dt = new DataTable();
            SQR = "Select top 1 opertitle=UPPER(opertitle) from webx_acctrans_" + BaseYearVal + " where Voucherno='" + code + "'";
            Dt = GF.GetDataTableFromSP(SQR);
            string opertitle = Dt.Rows[0][0].ToString();
            string str = "";
            if (opertitle == "MANUAL CREDIT VOUCHER" || opertitle == "CROSS LOCATION VOUCHER" || opertitle == "MANUAL DEBIT VOUCHER" || opertitle == "MANUAL JOURNAL VOUCHER" || opertitle == "SPECIAL COST VOUCHER" || opertitle == "MANUAL CONTRA VOUCHER")
            {
                str = "Select * from VWNET_Voucher_Tracker where Voucherno='" + code + "'";
                Dt = GF.GetDataTableFromSP(str);
            }
            else
            {
                str = "EXEC USP_Voucher_Tracker '" + code + "','" + BaseYearVal + "'";
                Dt = GF.GetDataTableFromSP(str);
            }
            return Dt;
        }

        #endregion

        #region GPS Tracking
        public List<CYGNUS_Vehicle_GPS_Current_Details> GetVehicleGPSTrackingList()
        {
            string SQRY = "select * from CYGNUS_Vehicle_GPS_Current_Details with(Nolock)";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Vehicle_GPS_Current_Details> MRList_Data = DataRowToObject.CreateListFromTable<CYGNUS_Vehicle_GPS_Current_Details>(Dt1);
            return MRList_Data;
        }

        #endregion

        #region THC DRS PRS Tracking

        public List<vw_OperationTrack> GetDocumentType()
        {
            string Squery = "EXEC Get_THS_PRS_DRS_DocumentType";
            DataTable Dt = GF.GetDataTableFromSP(Squery);
            List<vw_OperationTrack> objDocumentType = DataRowToObject.CreateListFromTable<vw_OperationTrack>(Dt);
            return objDocumentType;
        }
        #endregion
    }
}
