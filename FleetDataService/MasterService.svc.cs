﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using CYGNUS.Models;
using Fleet.ViewModels;
using FleetDataService.ViewModels;
using Fleet.Models;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class MasterService : IMasterService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");

        #region Excel Data Export

        public DataTable GetExcelData(string MethodName, string BaseUserName, string BaseCompanyCode, string BaseLocationCode, string BaseYearVal)
        {
            string SQRY = "exec Usp_GetExcelData '" + MethodName + "','" + BaseUserName + "','" + BaseCompanyCode + "','" + BaseLocationCode + "','" + BaseYearVal + "'";
            return GF.GetDataTableFromSP(SQRY);
        }

        #endregion

        #region Menu Function

        public List<VW_GetUserMenuRights> GetMenuListWithRights(string Userid, bool IsLogin, string Type, string BaseUserName)
        {
            //Type = "2";
            string curFile = folderPath + "UserMenuRights_" + Userid + ".xml";
            if (!File.Exists(curFile) || IsLogin)
            {
                string SQRY = "exec Usp_GetUserMenuRights '" + Userid + "','" + Type + "','" + BaseUserName + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<VW_GetUserMenuRights> MenuList_XML = DataRowToObject.CreateListFromTable<VW_GetUserMenuRights>(Dt);
                GF.SerializeParams<VW_GetUserMenuRights>(MenuList_XML, folderPath + "UserMenuRights_" + Userid + ".xml");
            }
            List<VW_GetUserMenuRights> MenuList = GF.DeserializeParams<VW_GetUserMenuRights>(folderPath + "UserMenuRights_" + Userid + ".xml");
            return MenuList.Where(c => c.IsNewPortal == true).ToList();
        }
        

        //public List<CYGNUS_Master_Menu> GetMenusList---Stopped(bool IsLogin)
        //{
        //    //string curFile = folderPath + "Cygnus_Master_Menu.xml";
        //    //if (!File.Exists(curFile) || IsLogin)
        //    //{
        //    string SQRY = "exec USP_GetMenus_NewPortal";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    List<CYGNUS_Master_Menu> MenuList_XML = DataRowToObject.CreateListFromTable<CYGNUS_Master_Menu>(Dt);
        //    //GF.SerializeParams<CYGNUS_Master_Menu>(MenuList_XML, folderPath + "Cygnus_Master_Menu.xml");
        //    //}
        //    //List<CYGNUS_Master_Menu> MenuList = GF.DeserializeParams<CYGNUS_Master_Menu>(folderPath + "Cygnus_Master_Menu.xml");

        //    return MenuList_XML.Where(c => c.IsNewPortal == true).ToList();
        //}
        public List<CYGNUS_Master_Menu> GetMenusList(bool IsLogin, string UserId)
        {
            //string curFile = folderPath + "Cygnus_Master_Menu.xml";
            //if (!File.Exists(curFile) || IsLogin)
            //{
            string SQRY = "exec USP_GetMenus_NewPortal '" + UserId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Master_Menu> MenuList_XML = DataRowToObject.CreateListFromTable<CYGNUS_Master_Menu>(Dt);
            //GF.SerializeParams<CYGNUS_Master_Menu>(MenuList_XML, folderPath + "Cygnus_Master_Menu.xml");
            //}
            //List<CYGNUS_Master_Menu> MenuList = GF.DeserializeParams<CYGNUS_Master_Menu>(folderPath + "Cygnus_Master_Menu.xml");

            return MenuList_XML.Where(c => c.IsNewPortal == true).ToList();
        }

        #endregion

        #region User Menu Rights

        public DataTable InsertMenuRights(string XML, string UserId, string isNewPortal, string UserName)
        {
            string SQR = "exec Usp_Insert_MenuRights '" + XML + "','" + UserId + "','" + isNewPortal + "','" + UserName + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "InsertMenuRights", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            GetMenuListWithRights(UserId, true, "0", UserId);
            return Dt;
        }

        #endregion

        #region User Master

        public List<WebX_Master_Users> GetUserDetails()
        {
            string SQRY = "exec USP_GetUserDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Users> UserList = DataRowToObject.CreateListFromTable<WebX_Master_Users>(Dt);
            return UserList;
        }
        public bool CheckUserDetailsExistInUserprofile(string UserId)
        {
            string SQRY = "exec USP_CheckUserDetailsExistInUserprofile '" + UserId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            bool Valid = Convert.ToBoolean(Dt.Rows[0][0].ToString());
            return Valid;
        }
        public List<WebX_Master_Users> GetUserDetailsForUserMasterList(string userId)
        {
            string SQRY = "exec USP_GetUserDetailsForUserMasterList '" + userId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Users> UserList = DataRowToObject.CreateListFromTable<WebX_Master_Users>(Dt);
            return UserList;
        }

        public DataTable InsertUser(string XML, string EditFlag)
        {
            string SQRY = "exec Usp_InsertUpdate_UserMaster_NewPortal '" + XML + "','" + EditFlag + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUser", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            if (DT.Rows[0][0].ToString() == "Done")
            {
                GetUserDetails();
            }
            return DT;
        }

        public string GetLoginUserType(string VendorType, string Username) // XX5 = Franchise , 08 = BA
        {
            try
            {
                string SQRY = "EXEC USP_GetLoginUserType '" + VendorType + "','" + Username + "'";
                DataTable DT = GF.GetDataTableFromSP(SQRY);
                return DT.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                return "0";
            }
        }

        public string CheckUserPasswordExpired(string Username)
        {
            try
            {
                string SQRY = "EXEC USP_CheckUserPasswordExpired '" + Username + "'";
                DataTable DT = GF.GetDataTableFromSP(SQRY);
                return DT.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                return "0-0";
            }
        }

        #endregion

        #region Finacial Years

        public List<vw_Get_Finacial_Years> GetFinacialYearDetails()
        {
            string SQRY = "Select * from vw_Get_Finacial_Years";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<vw_Get_Finacial_Years> FinacialYearList = DataRowToObject.CreateListFromTable<vw_Get_Finacial_Years>(Dt);
            return FinacialYearList;
        }

        #endregion

        #region Company Detials

        public List<WEBX_COMPANY_MASTER> GetComapanyDetails()
        {
            string SQRY = "exec USP_GetComapanyDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_COMPANY_MASTER> ComapanyMasterList = DataRowToObject.CreateListFromTable<WEBX_COMPANY_MASTER>(Dt);
            return ComapanyMasterList;
        }

        #endregion

        #region GeneralMaster Master

        public List<Webx_Master_General> GetGeneralMasterObject()
        {
            string curFile = folderPath + "Webx_Master_General.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_GeneralMaster_List";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
                GF.SerializeParams<Webx_Master_General>(Webx_Master_GeneralList_XML, folderPath + "Webx_Master_General.xml");
            }

            List<Webx_Master_General> Webx_Master_GeneralList = GF.DeserializeParams<Webx_Master_General>(folderPath + "Webx_Master_General.xml");
            return Webx_Master_GeneralList;
        }

        public List<VW_Hierarchy> GetGeneralMasterHRCHY()
        {

            string QueryString = "select * from VW_Hierarchy";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VW_Hierarchy> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<VW_Hierarchy>(dataTable);


            return Webx_Master_GeneralList_XML;
        }

        public DataTable AddEditGeneralMaster(string XML, string flag, string Finyear)
        {
            string QueryString = "exec Usp_InsertMstData_NewPortal '" + XML + "','" + flag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                if (DS.Tables[1].Rows.Count > 0)
                {
                    List<Webx_Master_General> SSI_Master_GeneralList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DS.Tables[1]);
                    GF.SerializeParams<Webx_Master_General>(SSI_Master_GeneralList, folderPath + "Webx_Master_General.xml");
                }
                else
                {
                    File.Delete(folderPath + "Webx_Master_General.xml");
                }
            }
            return DS.Tables[0];
        }

        public string CheckDuplicateGeneralMaster(string CodeType, string CodeDesc)
        {
            string QueryString = "exec CheckDuplicateGeneralMaster '" + CodeDesc + "','" + CodeType + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        public List<WebX_Master_CodeTypes> GetCodetypesMasterList()
        {
            string QueryString = "exec Usp_GetCodeType_list_NewPortal";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WebX_Master_CodeTypes> WebX_Master_CodeTypesList = DataRowToObject.CreateListFromTable<WebX_Master_CodeTypes>(dataTable);

            return WebX_Master_CodeTypesList;
        }

        public List<Webx_Master_General> GetColumnSplitMasterObject(string Type)
        {
            string QueryString = "exec Cygnus_Master_CommonSplitColumn'" + Type + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_ColumnSplitMaster = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);

            return Webx_ColumnSplitMaster;
        }

        public List<Webx_Master_General> GetGSTTypeFromStateandVendor(string statePrefix, string VendorCode)
        {
            string QueryString = "exec USP_GetGSTTypeFromStateandVendor '" + statePrefix + "','" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> GSTTypeList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return GSTTypeList;
        }

        public List<WebX_Master_Users> GetManagerFromDesignationandLocation(string Location)
        {
            string QueryString = "exec USP_GetManagerFromDesignationandLocation '" + Location + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WebX_Master_Users> ManagerList = DataRowToObject.CreateListFromTable<WebX_Master_Users>(dataTable);
            return ManagerList;
        }

        public List<Webx_Master_General> GetDesignationFromCategory(string Category)
        {
            string QueryString = "exec Usp_Get_DesignationFromCategory '" + Category + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> DesignationList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return DesignationList;
        }

        #endregion

        #region  Change Settings

        public List<ChgangeLoc> GetWorkingLocations(string BaseLocationCode, string MainLocCode)
        {
            string commandText = "Exec usp_WorkingLocations '" + BaseLocationCode + "','" + MainLocCode + "'  ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<ChgangeLoc> ListLocation = DataRowToObject.CreateListFromTable<ChgangeLoc>(dataTable);

            return ListLocation;
        }

        public List<ChgangeLoc> GetWorkingLocationsNewPortal(string BaseLocationCode, string MainLocCode, string UserName)
        {
            //string commandText = "Exec usp_WorkingLocations '" + BaseLocationCode + "','" + MainLocCode + "'  ";
            string commandText = "Exec usp_WorkingLocations_NewPortal '" + BaseLocationCode + "','" + MainLocCode + "' ,'" + UserName + "' ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<ChgangeLoc> ListLocation = DataRowToObject.CreateListFromTable<ChgangeLoc>(dataTable);

            return ListLocation;
        }

        public List<ChgangeCompany> GetCompanyMappedToEmployee(string UserId)
        {
            string commandText = "Exec CompanyMappedToEmployee '" + UserId + "'  ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<ChgangeCompany> ListCompany = DataRowToObject.CreateListFromTable<ChgangeCompany>(dataTable);

            return ListCompany;
        }

        #endregion

        #region State Master Function

        public List<Webx_State> GetStateListFromGSTType(string LocLevel, string LocCode)
        {
            string QueryString = "exec USP_GetStateListFromGSTType '" + LocLevel + "','" + LocCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_State> Webx_StateList = DataRowToObject.CreateListFromTable<Webx_State>(dataTable);
            return Webx_StateList;
        }

        public List<Webx_State> GetStateMasterObject()
        {
            string QueryString = "exec USP_StateMaster_List";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_State> Webx_StateList = DataRowToObject.CreateListFromTable<Webx_State>(dataTable);
            return Webx_StateList;
        }

        public List<Webx_State_Document> GetStateMasterDocumentObject()
        {
            string QueryString = "exec USP_StateMasterDocument_List";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_State_Document> Webx_State_DocumentList = DataRowToObject.CreateListFromTable<Webx_State_Document>(dataTable);
            return Webx_State_DocumentList;
        }

        public DataTable AddEditStateMaster(string XML_Main, string XML_Detail)
        {
            string QueryString = "exec usp_webx_STATE_Insert_NewPortal '" + XML_Main + "','" + XML_Detail + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<Webx_State> Webx_StateList = DataRowToObject.CreateListFromTable<Webx_State>(DS.Tables[1]);
                List<Webx_State_Document> Webx_StateDocumentList = DataRowToObject.CreateListFromTable<Webx_State_Document>(DS.Tables[2]);
                GF.SerializeParams<Webx_State>(Webx_StateList, folderPath + "Webx_State.xml");
                GF.SerializeParams<Webx_State_Document>(Webx_StateDocumentList, folderPath + "Webx_State_Document.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region City Master

        public List<webx_citymaster> GetCityMasterObject()
        {
            string curFile = folderPath + "webx_citymaster.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_CityMaster_List";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_citymaster> webx_citymasterList_XML = DataRowToObject.CreateListFromTable<webx_citymaster>(dataTable);
                GF.SerializeParams<webx_citymaster>(webx_citymasterList_XML, folderPath + "webx_citymaster.xml");
            }

            List<webx_citymaster> webx_citymasterList = GF.DeserializeParams<webx_citymaster>(folderPath + "webx_citymaster.xml");
            return webx_citymasterList;
        }

        public DataTable AddEditCityMaster(string XML)
        {
            string QueryString = "exec Usp_InsertCityMaster_NewPortal '" + XML + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_citymaster> SSI_Master_GeneralList = DataRowToObject.CreateListFromTable<webx_citymaster>(DS.Tables[1]);
                if (File.Exists(folderPath + "webx_citymaster.xml"))
                {
                    File.Delete(folderPath + "webx_citymaster.xml");
                }
                GF.SerializeParams<webx_citymaster>(SSI_Master_GeneralList, folderPath + "webx_citymaster.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Location

        public List<webx_location> GetLocationDetails()
        {
            string SQRY = "exec USP_GetLocationDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_location> List = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            return List;

            //string curFile = folderPath + "webx_location.xml";
            //if (!File.Exists(curFile))
            //{
            //    string SQRY = "exec USP_GetLocationDetails";
            //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //    List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            //    GF.SerializeParams<webx_location>(LocationList_XML, folderPath + "webx_location.xml");
            //}
            //List<webx_location> LocationList = GF.DeserializeParams<webx_location>(folderPath + "webx_location.xml");

            //return LocationList;
        }

        public List<vw_Series_ToBe_Alloted2> GetAllotToListDetails(string BRCD)
        {
            string SQRY = "exec usp_AllotToList_New_Portal '"+ BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<vw_Series_ToBe_Alloted2> List = DataRowToObject.CreateListFromTable<vw_Series_ToBe_Alloted2>(Dt);
            return List;

            //string curFile = folderPath + "webx_location.xml";
            //if (!File.Exists(curFile))
            //{
            //    string SQRY = "exec USP_GetLocationDetails";
            //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //    List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            //    GF.SerializeParams<webx_location>(LocationList_XML, folderPath + "webx_location.xml");
            //}
            //List<webx_location> LocationList = GF.DeserializeParams<webx_location>(folderPath + "webx_location.xml");

            //return LocationList;
        }

        public List<webx_location> GetLocationAsPerZoneDetails()
        {
            string SQRY = "select * from VW_LocationAsPerZone";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_location> List = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            return List;

            //string curFile = folderPath + "webx_location.xml";
            //if (!File.Exists(curFile))
            //{
            //    string SQRY = "exec USP_GetLocationDetails";
            //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //    List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            //    GF.SerializeParams<webx_location>(LocationList_XML, folderPath + "webx_location.xml");
            //}
            //List<webx_location> LocationList = GF.DeserializeParams<webx_location>(folderPath + "webx_location.xml");

            //return LocationList;
        }

        public DataTable GetLocationListByState(string StateCode, string searchTerm)
        {
            string SQRY = "exec usp_GetLocationByState '" + StateCode + "','" + searchTerm + "'";
            return GF.GetDataTableFromSP(SQRY);
        }

        public List<webx_location> GetLocCodeLocationDetails()
        {
            string SQRY = "exec USP_GetLocCodeLocationDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            return LocationList_XML;
        }

        public List<webx_location> GetLocationListFromState(string StateCode, string searchTerm)
        {
            string SQRY = "exec usp_GetLocationByState '" + StateCode + "','" + searchTerm + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            return LocationList_XML;
        }

        public bool InsertLocation(string XML, string EditFlag, string EntryBy)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertLocationMaster_NewPortal '" + XML + "','" + EditFlag + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertLocation", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 1 && DS.Tables[1].Rows.Count > 0)
                {
                    List<webx_location> LocationList_XML = DataRowToObject.CreateListFromTable<webx_location>(DS.Tables[1]);
                    GF.SerializeParams<webx_location>(LocationList_XML, folderPath + "webx_location.xml");
                }
                else
                {
                    File.Delete(folderPath + "webx_location.xml");
                }
            }
            return Status;
        }

        public List<webx_location> GetRegionList(string LocCode, string Type)
        {
            string SQRY = "EXEC USP_GetLocationFromRO '" + LocCode + "','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_location> LocationList = DataRowToObject.CreateListFromTable<webx_location>(Dt);
            return LocationList;
        }

        #endregion

        #region Pincode Master Function

        public List<webx_pincode_master> GetPincodeMasterObject()
        {

            string curFile = folderPath + "webx_pincode_master.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_PincodeMaster_List";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_pincode_master> webx_pincode_masterList_XML = DataRowToObject.CreateListFromTable<webx_pincode_master>(dataTable);
                GF.SerializeParams<webx_pincode_master>(webx_pincode_masterList_XML, folderPath + "webx_pincode_master.xml");
            }

            List<webx_pincode_master> webx_pincode_masterList = GF.DeserializeParams<webx_pincode_master>(folderPath + "webx_pincode_master.xml");

            return webx_pincode_masterList;

        }

        public DataTable AddEditPincodeMaster(string XML)
        {
            string QueryString = "exec usp_webx_Pincode_Master_Insert_NewPortal '" + XML + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_pincode_master> webx_pincode_masterList = DataRowToObject.CreateListFromTable<webx_pincode_master>(DS.Tables[1]);
                if (File.Exists(folderPath + "webx_pincode_master.xml"))
                {
                    File.Delete(folderPath + "webx_pincode_master.xml");
                }
                GF.SerializeParams<webx_pincode_master>(webx_pincode_masterList, folderPath + "webx_pincode_master.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Customer Group Master

        public List<webx_GRPMST> GetCustomerGroupMasterObject()
        {

            string curFile = folderPath + "webx_GRPMST.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_CustomerGroupMaster_List";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_GRPMST> webx_GRPMST_masterList_XML = DataRowToObject.CreateListFromTable<webx_GRPMST>(dataTable);
                GF.SerializeParams<webx_GRPMST>(webx_GRPMST_masterList_XML, folderPath + "webx_GRPMST.xml");
            }

            List<webx_GRPMST> webx_GRPMST_masterList = GF.DeserializeParams<webx_GRPMST>(folderPath + "webx_GRPMST.xml");

            return webx_GRPMST_masterList;

        }

        public DataTable AddEditCustomerGroupMaster(string XML)
        {
            string QueryString = "exec usp_webx_Customer_Group_Master_Insert_NewPortal '" + XML + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_GRPMST> webx_GRPMST_masterList = DataRowToObject.CreateListFromTable<webx_GRPMST>(DS.Tables[1]);
                GF.SerializeParams<webx_GRPMST>(webx_GRPMST_masterList, folderPath + "webx_GRPMST.xml");
            }
            return DS.Tables[0];
        }
        public List<webx_CUSTHDR> GetCustomerList(string Type, string Name)
        {

            string QueryString = "exec sp_GetLike_GetCustomer '" + Name + "','" + Type + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> itmCustList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(DT);
            return itmCustList;
        }

        #endregion

        #region Customer Master

        public List<webx_CUSTHDR> GetCustomerMasterObject()
        {
            //string curFile = folderPath + "webx_CUSTHDR.xml";
            //if (!File.Exists(curFile))
            //{
            string QueryString = "exec USP_CustomerMaster_List";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList_XML = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            //GF.SerializeParams<webx_CUSTHDR>(webx_CUSTHDRList_XML, folderPath + "webx_CUSTHDR.xml");
            //}

            //List<webx_CUSTHDR> webx_CUSTHDRList = GF.DeserializeParams<webx_CUSTHDR>(folderPath + "webx_CUSTHDR.xml");

            return webx_CUSTHDRList_XML;
        }

        public List<webx_CUSTHDR> GetCustomerMasterObjectDocketSubmit(string searchTerm)
        {
            //string curFile = folderPath + "webx_CUSTHDR.xml";
            //if (!File.Exists(curFile))
            //{
            string QueryString = "exec USP_CustomerMaster_List '"+ searchTerm + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList_XML = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            //GF.SerializeParams<webx_CUSTHDR>(webx_CUSTHDRList_XML, folderPath + "webx_CUSTHDR.xml");
            //}

            //List<webx_CUSTHDR> webx_CUSTHDRList = GF.DeserializeParams<webx_CUSTHDR>(folderPath + "webx_CUSTHDR.xml");

            return webx_CUSTHDRList_XML;
        }

        public List<webx_CUSTHDR> GetPrimaryCustomerObject(string searchTerm)
        {

            string QueryString = "exec USP_PrimaryCustomerMaster_List '" + searchTerm + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList_XML = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);

            return webx_CUSTHDRList_XML;
        }

        public List<webx_CUSTHDR> GetCustomerMasterObject(string CustCode)
        {
            string QueryString = "exec USP_CustomerMaster_List '" + CustCode +"'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList_XML = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList_XML;
        }
        
        public List<webx_CUSTHDR> GetCustomerListingNew(string searchTerm, string GRPCD, string state = null)
        {
            string QueryString = "exec USP_CustomerMaster_List_New '" + searchTerm + "','" + GRPCD + "','" + state + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public List<webx_CUSTHDR> GetCustomerMasterObject1(string searchTerm, string Location, string Paybas)
        {
            string QueryString = "exec [USP_GetBillingParty] '" + searchTerm + "','" + Paybas + "','" + Location + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public List<webx_CUSTHDR> GetCustomerMaster_OctroiBill(string searchTerm, string Location, string Paybas)
        {
            string QueryString = "exec USP_GetBillingParty_OctroiBill '" + searchTerm + "','" + Paybas + "','" + Location + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public DataTable AddEditCustomerMaster(string XML, string GSTDetailsXML, string AutoBillGenerationDetailsXML, string Username)
        {
            string QueryString = "exec usp_Webx_CUSTHDR_Insert_NewPortal '" + XML + "','" + GSTDetailsXML + "','" + AutoBillGenerationDetailsXML + "','" + Username + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "CustomerMasterAddEdit", "", "");
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                //List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(DS.Tables[1]);
                //GF.SerializeParams<webx_CUSTHDR>(webx_CUSTHDRList, folderPath + "webx_CUSTHDR.xml");
                File.Delete(folderPath + "webx_CUSTHDR.xml");
                //GetCustomerMasterObject();
            }
            return DS.Tables[0];
        }

        public List<Cygnus_CustomerBillCycleSetting> GetDetailsFromCycleType(string id, string CustomerCode)
        {
            string QueryString = "EXEC USP_GetDetailsFromCycleType '" + id + "','" + CustomerCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Cygnus_CustomerBillCycleSetting> Cygnus_CustomerBillCycleSetting = DataRowToObject.CreateListFromTable<Cygnus_CustomerBillCycleSetting>(dataTable);
            return Cygnus_CustomerBillCycleSetting;
        }

        #endregion

        #region Address Master

        public List<webx_master_CustAddresses> GetAddressObject()
        {
            string curFile = folderPath + "webx_master_CustAddresses.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec Get_webx_master_CustAddresses";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_master_CustAddresses> webx_master_CustAddressesList_XML = DataRowToObject.CreateListFromTable<webx_master_CustAddresses>(dataTable);
                GF.SerializeParams<webx_master_CustAddresses>(webx_master_CustAddressesList_XML, folderPath + "webx_master_CustAddresses.xml");
            }

            List<webx_master_CustAddresses> webx_master_CustAddressesList = GF.DeserializeParams<webx_master_CustAddresses>(folderPath + "webx_master_CustAddresses.xml");
            return webx_master_CustAddressesList;
        }

        public DataTable AddEditAddress(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_webx_master_CustAddresses'" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_master_CustAddresses> webx_master_CustAddressesList = DataRowToObject.CreateListFromTable<webx_master_CustAddresses>(DS.Tables[1]);
                GF.SerializeParams<webx_master_CustAddresses>(webx_master_CustAddressesList, folderPath + "webx_master_CustAddresses.xml");
            }
            return DS.Tables[0];
        }

        public string CheckDuplicateAddressCode(string AddID)
        {
            string QueryString = "exec CheckDuplicateAddress '" + AddID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        #region Customer Address Mapping

        public List<webx_master_CustAddr_Mapping> GetCustAddrressMappingObject()
        {
            string curFile = folderPath + "webx_master_CustAddr_Mapping.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec Get_webx_master_CustAddr_Mapping";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_master_CustAddr_Mapping> webx_master_CustAddr_MappingList_XML = DataRowToObject.CreateListFromTable<webx_master_CustAddr_Mapping>(dataTable);
                GF.SerializeParams<webx_master_CustAddr_Mapping>(webx_master_CustAddr_MappingList_XML, folderPath + "webx_master_CustAddr_Mapping.xml");
            }

            List<webx_master_CustAddr_Mapping> webx_master_CustAddr_MappingList = GF.DeserializeParams<webx_master_CustAddr_Mapping>(folderPath + "webx_master_CustAddr_Mapping.xml");
            return webx_master_CustAddr_MappingList;
        }

        public DataTable AddEditCustAddressMapping(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_webx_master_CustAddresses_Mapping'" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_master_CustAddr_Mapping> webx_master_CustAddr_MappingList = DataRowToObject.CreateListFromTable<webx_master_CustAddr_Mapping>(DS.Tables[1]);
                GF.SerializeParams<webx_master_CustAddr_Mapping>(webx_master_CustAddr_MappingList, folderPath + "webx_master_CustAddr_Mapping.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Vendor Master

        public List<webx_VENDOR_HDR> GetVendorObject()
        {
            string curFile = folderPath + "webx_VENDOR_HDR.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetVendorObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_VENDOR_HDR> VendorList_XML = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
                GF.SerializeParams<webx_VENDOR_HDR>(VendorList_XML, folderPath + "webx_VENDOR_HDR.xml");
            }
            List<webx_VENDOR_HDR> VendorList = GF.DeserializeParams<webx_VENDOR_HDR>(folderPath + "webx_VENDOR_HDR.xml");
            return VendorList;
        }

        public List<WebX_Master_Users> GetEmployeeObject(string Prefix)
        {
            string SQRY = "";
            SQRY = "exec USP_GET_Employee_LIST '" + Prefix + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Users> List = DataRowToObject.CreateListFromTable<WebX_Master_Users>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_DRIVERMST> GetDriverObject(string Prefix)
        {
            string SQRY = "";
            SQRY = "exec USP_GET_DRIVER_LIST '" + Prefix + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DRIVERMST> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(Dt1);
            return List;
        }
        public List<webx_VENDOR_HDR> GetVendorObjectWithoutBlacklisted(string BaseLocationCode)
        {
            string SQRY = "exec Sp_GetVendorObjectWithoutBlacklisted'" + BaseLocationCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> VendorList = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
            return VendorList;
        }

        public DataTable GetVendorByState(string searchTerm, string State)
        {
            string SQRY = "exec usp_GetVendorByState '" + searchTerm + "','" + State + "'";
            return GF.GetDataTableFromSP(SQRY);
        }



        public List<webx_VENDOR_DET> GetVendorDetObject()
        {
            string curFile = folderPath + "webx_VENDOR_DET.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetVendorDetObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_VENDOR_DET> VendorList_XML = DataRowToObject.CreateListFromTable<webx_VENDOR_DET>(Dt);
                GF.SerializeParams<webx_VENDOR_DET>(VendorList_XML, folderPath + "webx_VENDOR_DET.xml");
            }
            List<webx_VENDOR_DET> VendorList = GF.DeserializeParams<webx_VENDOR_DET>(folderPath + "webx_VENDOR_DET.xml");
            return VendorList;
        }

        public List<Cygnus_Vendor_Document> GetVendorDoc(string VendorId)
        {
            string SQRY = "exec Sp_GetVendorDocument '" + VendorId + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Cygnus_Vendor_Document> VendorList = DataRowToObject.CreateListFromTable<Cygnus_Vendor_Document>(Dt);
            return VendorList;
        }
        //public bool InsertVendor(string XML, string DetXML, string EditFlag, string EntryBy)
        //{
        //    bool Status = false;
        //    string SQRY = "exec Usp_InsertVendor_NewPortal '" + XML + "','" + DetXML + "','" + EditFlag + "','" + EntryBy + "'";
        //    DataSet DS = GF.GetDataSetFromSP(SQRY);
        //    if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
        //    {
        //        Status = true;
        //        if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
        //        {
        //            List<webx_VENDOR_HDR> VendorList_XML = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(DS.Tables[1]);
        //            GF.SerializeParams<webx_VENDOR_HDR>(VendorList_XML, folderPath + "webx_VENDOR_HDR.xml");
        //        }
        //        else
        //        {
        //            File.Delete(folderPath + "webx_VENDOR_HDR.xml");
        //            File.Delete(folderPath + "webx_VENDOR_DET.xml");
        //        }
        //    }
        //    return Status;
        //}

        public DataTable InsertVendor(string XML, string DetXML, string GSTXML, string VendorDocXML, string EditFlag, string EntryBy)
        {
            DataTable DT = new DataTable();
            //bool Status = false;
            string SQRY = "exec Usp_InsertVendor_NewPortal '" + XML + "','" + DetXML + "','" + GSTXML + "','" + VendorDocXML + "','" + EditFlag + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertVendor", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                DT = DS.Tables[0];
                //Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<webx_VENDOR_HDR> VendorList_XML = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(DS.Tables[1]);
                    GF.SerializeParams<webx_VENDOR_HDR>(VendorList_XML, folderPath + "webx_VENDOR_HDR.xml");
                }
                else
                {
                    File.Delete(folderPath + "webx_VENDOR_HDR.xml");
                    File.Delete(folderPath + "webx_VENDOR_DET.xml");
                }
            }
            return DT;
        }

        public DataTable GetStateWiseGSTDetails(string flag, string Code)
        {
            string SQRY = "EXEC usp_GetStateWiseGSTDetails '" + flag + "','" + Code + "'";
            return GF.GetDataTableFromSP(SQRY);
        }

        public List<CygnusCustomerGSTDetails> GetGstNovendorcode(string flag, string Code)
        {
            string SQRY = "EXEC usp_GetStateWiseGSTDetails '" + flag + "','" + Code + "'";
            List<CygnusCustomerGSTDetails> VendorList = DataRowToObject.CreateListFromTable<CygnusCustomerGSTDetails>(GF.GetDataTableFromSP(SQRY));
            return VendorList;
        }
        #endregion

        #region Optimum Route

        public List<Webx_master_OptimumRouteMaster> GetOptimumRouteObject()
        {
            string curFile = folderPath + "Webx_master_OptimumRouteMaster.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetOptimumRouteObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<Webx_master_OptimumRouteMaster> OptimumRouteList_XML = DataRowToObject.CreateListFromTable<Webx_master_OptimumRouteMaster>(Dt);
                GF.SerializeParams<Webx_master_OptimumRouteMaster>(OptimumRouteList_XML, folderPath + "Webx_master_OptimumRouteMaster.xml");
            }
            List<Webx_master_OptimumRouteMaster> OptimumRouteList = GF.DeserializeParams<Webx_master_OptimumRouteMaster>(folderPath + "Webx_master_OptimumRouteMaster.xml");
            return OptimumRouteList;
        }

        public bool InsertOptimalRoute(string XML, string EditFlag, string EntryBy)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertOptimumRoute_NewPortal '" + XML + "','" + EditFlag + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertOptimalRoute", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<Webx_master_OptimumRouteMaster> OptimumList_XML = DataRowToObject.CreateListFromTable<Webx_master_OptimumRouteMaster>(DS.Tables[1]);
                    GF.SerializeParams<Webx_master_OptimumRouteMaster>(OptimumList_XML, folderPath + "Webx_master_OptimumRouteMaster.xml");
                }
                else
                {
                    File.Delete(folderPath + "Webx_master_OptimumRouteMaster.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Location Connection

        public List<Webx_master_LocationConnection> GetLocationConnectionObject()
        {
            string curFile = folderPath + "Webx_master_LocationConnection.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetLocationConnectionObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<Webx_master_LocationConnection> LocationConnectionList_XML = DataRowToObject.CreateListFromTable<Webx_master_LocationConnection>(Dt);
                GF.SerializeParams<Webx_master_LocationConnection>(LocationConnectionList_XML, folderPath + "Webx_master_LocationConnection.xml");
            }
            List<Webx_master_LocationConnection> LocationConnectionList = GF.DeserializeParams<Webx_master_LocationConnection>(folderPath + "Webx_master_LocationConnection.xml");
            return LocationConnectionList;
        }

        public bool InsertLocationConnection(string XML, string EditFlag, string EntryBy)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertLocationConnection_NewPortal '" + XML + "','" + EditFlag + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertLocationConnection", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<Webx_master_LocationConnection> OptimumList_XML = DataRowToObject.CreateListFromTable<Webx_master_LocationConnection>(DS.Tables[1]);
                    GF.SerializeParams<Webx_master_LocationConnection>(OptimumList_XML, folderPath + "Webx_master_LocationConnection.xml");
                }
                else
                {
                    File.Delete(folderPath + "Webx_master_LocationConnection.xml");
                }
            }
            return Status;
        }

        public List<webx_location> GetDestinationLocationsWithHQTR(string Prefix)
        {
            string QueryString = "exec [USP_Get_GCDestination_With_HQTR] '" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_location> itmList = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return itmList;
        }

        #endregion

        #region Vehicle Type

        public List<webx_Vehicle_Type> GetVehicleTypeDetails()
        {
            string curFile = folderPath + "webx_Vehicle_Type.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Usp_GetVehicleTypeList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_Vehicle_Type> VehicleTypeList_XML = DataRowToObject.CreateListFromTable<webx_Vehicle_Type>(Dt);
                GF.SerializeParams<webx_Vehicle_Type>(VehicleTypeList_XML, folderPath + "webx_Vehicle_Type.xml");
            }
            List<webx_Vehicle_Type> VehicleTypeList = GF.DeserializeParams<webx_Vehicle_Type>(folderPath + "webx_Vehicle_Type.xml");

            return VehicleTypeList;
        }

        public DataTable AddEditVehicleTypeMaster(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryStringAdd = "exec usp_InsertUpdate_webx_Vehicle_Type '" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
            string QueryStringGet = "exec Usp_GetVehicleTypeList";
            DataTable dataTableGet = GF.GetDataTableFromSP(QueryStringGet);
            List<webx_Vehicle_Type> VehicleTypeList = DataRowToObject.CreateListFromTable<webx_Vehicle_Type>(dataTableGet);

            GF.SerializeParams<webx_Vehicle_Type>(VehicleTypeList, folderPath + "webx_Vehicle_Type.xml");

            return dataTableAdd;
        }

        #endregion

        #region Delivery Zone Master

        public List<webx_Delivery_Zone> GetDeliveryZoneObject()
        {
            string curFile = folderPath + "webx_Delivery_Zone.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_Delivery_Zone_List";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_Delivery_Zone> webx_Delivery_ZoneList_XML = DataRowToObject.CreateListFromTable<webx_Delivery_Zone>(dataTable);
                GF.SerializeParams<webx_Delivery_Zone>(webx_Delivery_ZoneList_XML, folderPath + "webx_Delivery_Zone.xml");
            }

            List<webx_Delivery_Zone> webx_Delivery_ZoneList = GF.DeserializeParams<webx_Delivery_Zone>(folderPath + "webx_Delivery_Zone.xml");
            return webx_Delivery_ZoneList;
        }

        public DataTable AddEditDeliveryZone(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_webx_Delivery_Zone '" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            string QueryStringGet = "exec USP_Delivery_Zone_List";
            DataTable dataTableGet = GF.GetDataTableFromSP(QueryStringGet);
            List<webx_Delivery_Zone> webx_Delivery_ZoneList = DataRowToObject.CreateListFromTable<webx_Delivery_Zone>(dataTableGet);

            GF.SerializeParams<webx_Delivery_Zone>(webx_Delivery_ZoneList, folderPath + "webx_Delivery_Zone.xml");

            return dataTableAdd;
        }

        public string CheckDuplicateDeliveryZone(string ZoneName, string Location)
        {
            string QueryString = "SELECT count(*) FROM webx_Delivery_Zone WHERE ZoneName='" + ZoneName.Trim() + "' AND Location='" + Location.Trim() + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        #region Airport Master

        public List<webx_Master_Airport> GetAirportMasterObject()
        {
            string curFile = folderPath + "webx_Master_Airport.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec Usp_Get_webx_Master_Airport_Detail";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_Master_Airport> webx_Master_AirportList_XML = DataRowToObject.CreateListFromTable<webx_Master_Airport>(dataTable);
                GF.SerializeParams<webx_Master_Airport>(webx_Master_AirportList_XML, folderPath + "webx_Master_Airport.xml");
            }

            List<webx_Master_Airport> webx_Master_AirportList = GF.DeserializeParams<webx_Master_Airport>(folderPath + "webx_Master_Airport.xml");
            return webx_Master_AirportList;
        }

        public DataTable AddEditAirportMaster(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_webx_Master_Airport '" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            string QueryStringGet = "exec Usp_Get_webx_Master_Airport_Detail";
            DataTable dataTableGet = GF.GetDataTableFromSP(QueryStringGet);
            List<webx_Master_Airport> webx_Delivery_ZoneList = DataRowToObject.CreateListFromTable<webx_Master_Airport>(dataTableGet);

            GF.SerializeParams<webx_Master_Airport>(webx_Delivery_ZoneList, folderPath + "webx_Master_Airport.xml");

            return dataTableAdd;
        }

        #endregion

        #region Flight_Schedule Master

        public List<webx_Master_Flight_Schedule_Det> GetFlight_ScheduleDetails()
        {
            string curFile = folderPath + "webx_Master_Flight_Schedule_Det.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_Flight_Schedule_List_Det";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_Master_Flight_Schedule_Det> Flight_ScheduleList_XML = DataRowToObject.CreateListFromTable<webx_Master_Flight_Schedule_Det>(Dt);
                GF.SerializeParams<webx_Master_Flight_Schedule_Det>(Flight_ScheduleList_XML, folderPath + "webx_Master_Flight_Schedule_Det.xml");
            }
            List<webx_Master_Flight_Schedule_Det> Flight_ScheduleList = GF.DeserializeParams<webx_Master_Flight_Schedule_Det>(folderPath + "webx_Master_Flight_Schedule_Det.xml");

            return Flight_ScheduleList;
        }

        public List<webx_Master_Flight_Schedule_Hdr> GetFlightDetails()
        {
            string curFile = folderPath + "webx_Master_Flight_Schedule_Hdr.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_Flight_Schedule_Hdr_List";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_Master_Flight_Schedule_Hdr> FlightList_XML = DataRowToObject.CreateListFromTable<webx_Master_Flight_Schedule_Hdr>(Dt);
                GF.SerializeParams<webx_Master_Flight_Schedule_Hdr>(FlightList_XML, folderPath + "webx_Master_Flight_Schedule_Hdr.xml");
            }
            List<webx_Master_Flight_Schedule_Hdr> FlightList = GF.DeserializeParams<webx_Master_Flight_Schedule_Hdr>(folderPath + "webx_Master_Flight_Schedule_Hdr.xml");

            return FlightList;
        }

        public bool AddEditFlight_Schedule(string HdrXML, string XML, string Entry_EditFlag, string Finyear)
        {
            bool Status = false;
            string QueryStringAdd = "exec usp_InsertUpdate_webx_Master_Flight_Schedule_Det '" + HdrXML + "','" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            int Id = GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditFlight_Schedule", "", "");

            DataSet DS = GF.GetDataSetFromSP(QueryStringAdd);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<webx_Master_Flight_Schedule_Det> webx_Master_Flight_Schedule_DetList = DataRowToObject.CreateListFromTable<webx_Master_Flight_Schedule_Det>(DS.Tables[1]);
                    GF.SerializeParams<webx_Master_Flight_Schedule_Det>(webx_Master_Flight_Schedule_DetList, folderPath + "webx_Master_Flight_Schedule_Det.xml");
                    List<webx_Master_Flight_Schedule_Hdr> webx_Master_Flight_Schedule_HdrList = DataRowToObject.CreateListFromTable<webx_Master_Flight_Schedule_Hdr>(DS.Tables[2]);
                    GF.SerializeParams<webx_Master_Flight_Schedule_Hdr>(webx_Master_Flight_Schedule_HdrList, folderPath + "webx_Master_Flight_Schedule_Hdr.xml");
                }
                else
                {
                    File.Delete(folderPath + "webx_Master_Flight_Schedule_Det.xml");
                    File.Delete(folderPath + "webx_Master_Flight_Schedule_Hdr.xml");
                }
            }
            return Status;
        }

        #endregion

        #region City Location Mapping

        public List<Webx_master_CityLocationMapping> GetCityMappingLocationObject()
        {
            string curFile = folderPath + "Webx_master_CityLocationMapping.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec GetCityLocationMapping";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<Webx_master_CityLocationMapping> webx_cityLocationMappingList_XML = DataRowToObject.CreateListFromTable<Webx_master_CityLocationMapping>(dataTable);
                GF.SerializeParams<Webx_master_CityLocationMapping>(webx_cityLocationMappingList_XML, folderPath + "Webx_master_CityLocationMapping.xml");
            }

            List<Webx_master_CityLocationMapping> webx_cityLocationMappingList = GF.DeserializeParams<Webx_master_CityLocationMapping>(folderPath + "Webx_master_CityLocationMapping.xml");
            return webx_cityLocationMappingList;
        }

        public List<Webx_master_CityLocationMapping> GetCityFromLocation(string id)
        {
            string QueryString = "exec GetCityLocationMapping '" + id + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_master_CityLocationMapping> webx_cityLocationMappingList = DataRowToObject.CreateListFromTable<Webx_master_CityLocationMapping>(dataTable);
            return webx_cityLocationMappingList;
        }

        public DataTable AddEditCityLocationMapping(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_Webx_master_CityLocationMapping '" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<Webx_master_CityLocationMapping> webx_cityLocationMappingList = DataRowToObject.CreateListFromTable<Webx_master_CityLocationMapping>(DS.Tables[1]);
                GF.SerializeParams<Webx_master_CityLocationMapping>(webx_cityLocationMappingList, folderPath + "Webx_master_CityLocationMapping.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Route Master City

        public List<webx_rutmas_City> GetRutMstCityDetails()
        {
            string curFile = folderPath + "webx_rutmas_City.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec GetRutMstCity";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_rutmas_City> RutMstCityList_XML = DataRowToObject.CreateListFromTable<webx_rutmas_City>(Dt);
                GF.SerializeParams<webx_rutmas_City>(RutMstCityList_XML, folderPath + "webx_rutmas_City.xml");
            }
            List<webx_rutmas_City> RutMstCityList = GF.DeserializeParams<webx_rutmas_City>(folderPath + "webx_rutmas_City.xml");

            return RutMstCityList;
        }

        public List<webx_ruttran_City> GetRutTranCityDetails()
        {
            string curFile = folderPath + "webx_ruttran_City.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec GetRutTranCity";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_ruttran_City> RutTranCityList_XML = DataRowToObject.CreateListFromTable<webx_ruttran_City>(Dt);
                GF.SerializeParams<webx_ruttran_City>(RutTranCityList_XML, folderPath + "webx_ruttran_City.xml");
            }
            List<webx_ruttran_City> RutTranCityList = GF.DeserializeParams<webx_ruttran_City>(folderPath + "webx_ruttran_City.xml");

            return RutTranCityList;
        }

        public DataTable AddEditRutCity(string HdrXML, string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryStringAdd = "exec [usp_InsertUpdate_webx_Rut_City] '" + HdrXML + "','" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditRutCity", "", "");
            DataSet DS = GF.GetDataSetFromSP(QueryStringAdd);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() != " ")
            {
                List<webx_rutmas_City> webx_rutmas_CityList = DataRowToObject.CreateListFromTable<webx_rutmas_City>(DS.Tables[1]);
                GF.SerializeParams<webx_rutmas_City>(webx_rutmas_CityList, folderPath + "webx_rutmas_City.xml");
                List<webx_ruttran_City> webx_ruttran_CityList = DataRowToObject.CreateListFromTable<webx_ruttran_City>(DS.Tables[2]);
                GF.SerializeParams<webx_ruttran_City>(webx_ruttran_CityList, folderPath + "webx_ruttran_City.xml");
           
            }
            return DS.Tables[0];
        }

        #endregion

        #region Route Master location

        public List<webx_rutmas> GetRutMstDetails()
        {
            string curFile = folderPath + "webx_rutmas.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec GetRutMst";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_rutmas> RutMstList_XML = DataRowToObject.CreateListFromTable<webx_rutmas>(Dt);
                GF.SerializeParams<webx_rutmas>(RutMstList_XML, folderPath + "webx_rutmas.xml");
            }
            List<webx_rutmas> RutMstList = GF.DeserializeParams<webx_rutmas>(folderPath + "webx_rutmas.xml");

            return RutMstList;
        }

        public List<webx_ruttran> GetRutTranDetails()
        {
            string curFile = folderPath + "webx_ruttran.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec GetRutTran";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_ruttran> RutTranList_XML = DataRowToObject.CreateListFromTable<webx_ruttran>(Dt);
                GF.SerializeParams<webx_ruttran>(RutTranList_XML, folderPath + "webx_ruttran.xml");
            }
            List<webx_ruttran> RutTranList = GF.DeserializeParams<webx_ruttran>(folderPath + "webx_ruttran.xml");

            return RutTranList;
        }

        public DataTable AddEditRut(string HdrXML, string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryStringAdd = "exec [usp_InsertUpdate_webx_Rut_Loc] '" + HdrXML + "','" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            int Id = GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditRut", "", "");
            DataSet DS = GF.GetDataSetFromSP(QueryStringAdd);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_rutmas> webx_rutmasList = DataRowToObject.CreateListFromTable<webx_rutmas>(DS.Tables[1]);
                GF.SerializeParams<webx_rutmas>(webx_rutmasList, folderPath + "webx_rutmas.xml");
                List<webx_ruttran> webx_ruttranList = DataRowToObject.CreateListFromTable<webx_ruttran>(DS.Tables[2]);
                GF.SerializeParams<webx_ruttran>(webx_ruttranList, folderPath + "webx_ruttran.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Vehicle

        public List<webx_VEHICLE_HDR> GetVehicleObject()
        {
            string curFile = folderPath + "webx_VEHICLE_HDR.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetVehicleObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_VEHICLE_HDR> VehicleList_XML = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(Dt);
                GF.SerializeParams<webx_VEHICLE_HDR>(VehicleList_XML, folderPath + "webx_VEHICLE_HDR.xml");
            }
            List<webx_VEHICLE_HDR> VehicleList = GF.DeserializeParams<webx_VEHICLE_HDR>(folderPath + "webx_VEHICLE_HDR.xml");
            return VehicleList;
        }

        public List<webx_Vehicle_Type> GetVehicleTypeObject()
        {
            string curFile = folderPath + "webx_Vehicle_Type.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec Sp_GetVehicleTypeObject";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<webx_Vehicle_Type> VehicleList_XML = DataRowToObject.CreateListFromTable<webx_Vehicle_Type>(Dt);
                GF.SerializeParams<webx_Vehicle_Type>(VehicleList_XML, folderPath + "webx_Vehicle_Type.xml");
            }
            List<webx_Vehicle_Type> VehicleList = GF.DeserializeParams<webx_Vehicle_Type>(folderPath + "webx_Vehicle_Type.xml");
            return VehicleList;
        }

        public bool InsertVehicle(string XML, string EditFlag, string EntryBy)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertVehicle_NewPortal '" + XML + "','" + EditFlag + "','" + EntryBy + "'";
            int id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertVehicle", "", "");
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<webx_VEHICLE_HDR> VehicleList_XML = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(DS.Tables[1]);
                    GF.SerializeParams<webx_VEHICLE_HDR>(VehicleList_XML, folderPath + "webx_VEHICLE_HDR.xml");
                }
                else
                {
                    File.Delete(folderPath + "webx_VEHICLE_HDR.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Masters

        #region Tyre Pattern

        public List<WEBX_FLEET_TYREPATTERN> GetTyrePatternList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYREPATTERN.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTyrePatternList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYREPATTERN> TyrePatternList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREPATTERN>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYREPATTERN>(TyrePatternList_XML, folderPath + "WEBX_FLEET_TYREPATTERN.xml");
            }
            List<WEBX_FLEET_TYREPATTERN> TyrePatternList = GF.DeserializeParams<WEBX_FLEET_TYREPATTERN>(folderPath + "WEBX_FLEET_TYREPATTERN.xml");
            return TyrePatternList;
        }

        public bool InsertUpdateTyrePattern(string XML, int TYRE_PATTERNID)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertUpdateTyrePattern_NewPortal '" + XML + "','" + TYRE_PATTERNID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateTyrePattern", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYREPATTERN> TyrePatternList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREPATTERN>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYREPATTERN>(TyrePatternList_XML, folderPath + "WEBX_FLEET_TYREPATTERN.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYREPATTERN.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Size

        public List<WEBX_FLEET_TYRESIZEMST> GetTyreSizeList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYRESIZEMST.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTyreSizeList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYRESIZEMST> TyreSizeList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYRESIZEMST>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYRESIZEMST>(TyreSizeList_XML, folderPath + "WEBX_FLEET_TYRESIZEMST.xml");
            }
            List<WEBX_FLEET_TYRESIZEMST> TyreSizeList = GF.DeserializeParams<WEBX_FLEET_TYRESIZEMST>(folderPath + "WEBX_FLEET_TYRESIZEMST.xml");
            return TyreSizeList;
        }

        public bool InsertUpdateTyreSize(string XML, string TYRE_SIZEID)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertUpdateTyreSize_NewPortal '" + XML + "','" + TYRE_SIZEID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateTyreSize", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYRESIZEMST> TyreSizeList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYRESIZEMST>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYRESIZEMST>(TyreSizeList_XML, folderPath + "WEBX_FLEET_TYRESIZEMST.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYRESIZEMST.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Manufacture

        public List<WEBX_FLEET_TYREMFG> GetTYREMFGList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYREMFG.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTYREMFGList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYREMFG> TYREMFGList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMFG>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYREMFG>(TYREMFGList_XML, folderPath + "WEBX_FLEET_TYREMFG.xml");
            }
            List<WEBX_FLEET_TYREMFG> TYREMFGList = GF.DeserializeParams<WEBX_FLEET_TYREMFG>(folderPath + "WEBX_FLEET_TYREMFG.xml");
            return TYREMFGList;
        }

        public bool InsertUpdateTYREMFG(string XML)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertUpdateTYREMFG_NewPortal '" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateTYREMFG", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYREMFG> VehicleList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMFG>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYREMFG>(VehicleList_XML, folderPath + "WEBX_FLEET_TYREMFG.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYREMFG.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Model

        public List<WEBX_FLEET_TYREMODELMST> GetTyreModelList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYREMODELMST.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTyreModelList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYREMODELMST> TyreModelList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMODELMST>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYREMODELMST>(TyreModelList_XML, folderPath + "WEBX_FLEET_TYREMODELMST.xml");
            }
            List<WEBX_FLEET_TYREMODELMST> TyreModelList = GF.DeserializeParams<WEBX_FLEET_TYREMODELMST>(folderPath + "WEBX_FLEET_TYREMODELMST.xml");
            return TyreModelList;
        }

        public bool InsertUpdateTyreModel(string XML, int TYRE_ModelID)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertUpdateTyreModel_NewPortal '" + XML + "','" + TYRE_ModelID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateTyreModel", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYREMODELMST> TyreModelList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMODELMST>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYREMODELMST>(TyreModelList_XML, folderPath + "WEBX_FLEET_TYREMODELMST.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYREMODELMST.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Position

        public List<WEBX_FLEET_TYREPOS> GetTYREPOSList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYREPOS.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTYREPOSList";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYREPOS> TYREPOSList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREPOS>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYREPOS>(TYREPOSList_XML, folderPath + "WEBX_FLEET_TYREPOS.xml");
            }
            List<WEBX_FLEET_TYREPOS> TYREPOSList = GF.DeserializeParams<WEBX_FLEET_TYREPOS>(folderPath + "WEBX_FLEET_TYREPOS.xml");
            return TYREPOSList;
        }

        public bool InsertUpdateTYREPOS(string XML)
        {
            bool Status = false;
            string SQRY = "exec Usp_InsertUpdateTYREPOS_NewPortal '" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateTYREPOS", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYREPOS> VehicleList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREPOS>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYREPOS>(VehicleList_XML, folderPath + "WEBX_FLEET_TYREPOS.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYREPOS.xml");
                }
            }
            return Status;
        }

        #endregion

        #region Tyre Msater

        public List<WEBX_FLEET_TYREMST> GetTyreMSTList()
        {
            string curFile = folderPath + "WEBX_FLEET_TYREMST.xml";
            if (!File.Exists(curFile))
            {
                string SQRY = "exec USP_GetTyreobjectList_newPortal";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<WEBX_FLEET_TYREMST> TyreList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST>(Dt);
                GF.SerializeParams<WEBX_FLEET_TYREMST>(TyreList_XML, folderPath + "WEBX_FLEET_TYREMST.xml");
            }
            List<WEBX_FLEET_TYREMST> TyreList = GF.DeserializeParams<WEBX_FLEET_TYREMST>(folderPath + "WEBX_FLEET_TYREMST.xml");
            return TyreList;
        }

        public bool InsertTyreMst(string XML, string EditFlag, string EntryBy)
        {
            bool Status = false;
            string SQRY = "exec usp_Insert_TyreMaster_NewPortal '" + XML + "','" + EditFlag + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertTyreMst", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
                if (DS.Tables.Count > 2 && DS.Tables[1].Rows.Count > 0 && DS.Tables[2].Rows.Count > 0)
                {
                    List<WEBX_FLEET_TYREMST> VendorList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_TYREMST>(DS.Tables[1]);
                    GF.SerializeParams<WEBX_FLEET_TYREMST>(VendorList_XML, folderPath + "WEBX_FLEET_TYREMST.xml");
                }
                else
                {
                    File.Delete(folderPath + "WEBX_FLEET_TYREMST.xml");
                }
            }
            File.Delete(folderPath + "WEBX_FLEET_TYREMST.xml");
            return Status;
        }

        #endregion

        #endregion

        #region Receiver Master

        public List<WebX_Master_Receiver> GetReceiverObject()
        {
            string curFile = folderPath + "WebX_Master_Receiver.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec Get_WebX_Master_Receiver";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<WebX_Master_Receiver> WebX_Master_ReceiverList_XML = DataRowToObject.CreateListFromTable<WebX_Master_Receiver>(dataTable);
                GF.SerializeParams<WebX_Master_Receiver>(WebX_Master_ReceiverList_XML, folderPath + "WebX_Master_Receiver.xml");
            }

            List<WebX_Master_Receiver> WebX_Master_ReceiverList = GF.DeserializeParams<WebX_Master_Receiver>(folderPath + "WebX_Master_Receiver.xml");
            return WebX_Master_ReceiverList;
        }

        public DataTable AddEditReceiver(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_WebX_Master_Receiver'" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<WebX_Master_Receiver> WebX_Master_ReceiverList = DataRowToObject.CreateListFromTable<WebX_Master_Receiver>(DS.Tables[1]);
                GF.SerializeParams<WebX_Master_Receiver>(WebX_Master_ReceiverList, folderPath + "WebX_Master_Receiver.xml");
            }
            return DS.Tables[0];
        }

        #endregion

        #region Warehouse Master

        public List<webx_GODOWNMST> GetWarehouseObject()
        {
            string curFile = folderPath + "webx_GODOWNMST.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec Get_webx_GODOWNMST";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_GODOWNMST> webx_GODOWNMSTList_XML = DataRowToObject.CreateListFromTable<webx_GODOWNMST>(dataTable);
                GF.SerializeParams<webx_GODOWNMST>(webx_GODOWNMSTList_XML, folderPath + "webx_GODOWNMST.xml");
            }

            List<webx_GODOWNMST> webx_GODOWNMSTList = GF.DeserializeParams<webx_GODOWNMST>(folderPath + "webx_GODOWNMST.xml");
            return webx_GODOWNMSTList;
        }

        public DataTable AddEditWarehouse(string XML, string Entry_EditFlag, string Finyear)
        {
            string QueryString = "exec usp_InsertUpdate_webx_GODOWNMST'" + XML + "','" + Entry_EditFlag + "','" + Finyear + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<webx_GODOWNMST> webx_GODOWNMSTList = DataRowToObject.CreateListFromTable<webx_GODOWNMST>(DS.Tables[1]);
                GF.SerializeParams<webx_GODOWNMST>(webx_GODOWNMSTList, folderPath + "webx_GODOWNMST.xml");
            }
            return DS.Tables[0];
        }

        public string CheckDuplicateWarehouse(string loccode, string Name)
        {
            string QueryString = "exec CheckDuplicateWarehouse ' " + loccode + " ','" + Name + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        public DataSet GetData(string Method, string param1, string param2, string param3, string param4)
        {
            string Qry = "";
            Qry = (param1 != null && param1 != "" ? '"' + param1 + '"' : "");
            Qry = Qry + (param2 != null && param2 != "" ? "," + '"' + param2 + '"' : "") + (param3 != null && param3 != "" ? "," + '"' + param3 + '"' : "") + (param4 != null && param4 != "" ? "," + '"' + param4 + '"' : "");
            return GF.GetDataSetFromSP("EXEC USP_" + Method + Qry);
        }

        #region Pincode Locaion Mapping

        public List<webx_Pincode_Loc_Master> GetPincodeLocationObject()
        {

            string curFile = folderPath + "webx_Pincode_Loc_Master.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "exec USP_PincodeLocationMaster";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_Pincode_Loc_Master> webx_Pincode_Loc_MasterList_XML = DataRowToObject.CreateListFromTable<webx_Pincode_Loc_Master>(dataTable);
                GF.SerializeParams<webx_Pincode_Loc_Master>(webx_Pincode_Loc_MasterList_XML, folderPath + "webx_Pincode_Loc_Master.xml");
            }

            List<webx_Pincode_Loc_Master> webx_Pincode_Loc_MasterList = GF.DeserializeParams<webx_Pincode_Loc_Master>(folderPath + "webx_Pincode_Loc_Master.xml");

            return webx_Pincode_Loc_MasterList;

        }

        #endregion

        #region Employee Company Master

        public List<Webx_Emp_Company> GetEmpCompanyObject(string EmployeeCode)
        {
            string curFile = folderPath + "Webx_Emp_Company.xml";
            string QueryString = "exec USP_GetEmployeeCompany_List '" + EmployeeCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Emp_Company> Webx_Emp_CompanyList_XML = DataRowToObject.CreateListFromTable<Webx_Emp_Company>(dataTable);
            GF.SerializeParams<Webx_Emp_Company>(Webx_Emp_CompanyList_XML, folderPath + "Webx_Emp_Company.xml");
            List<Webx_Emp_Company> Webx_Emp_CompanyList = GF.DeserializeParams<Webx_Emp_Company>(folderPath + "Webx_Emp_Company.xml");
            return Webx_Emp_CompanyList;
        }

        public DataTable AddEditEmployeeCompany(string XML)
        {
            string QueryString = "exec usp_Insert_Update_Emp_Company_Mapping_NewPortal'" + XML + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        #endregion

        #region Location Company Master

        public List<Webx_Loc_Company> GetLocCompanyObject(string LocCode)
        {
            string curFile = folderPath + "Webx_Loc_Company.xml";
            string QueryString = "exec USP_GetLocationCompany_List '" + LocCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Loc_Company> Webx_Loc_CompanyList_XML = DataRowToObject.CreateListFromTable<Webx_Loc_Company>(dataTable);
            GF.SerializeParams<Webx_Loc_Company>(Webx_Loc_CompanyList_XML, folderPath + "Webx_Loc_Company.xml");
            List<Webx_Loc_Company> Webx_Loc_CompanyList = GF.DeserializeParams<Webx_Loc_Company>(folderPath + "Webx_Loc_Company.xml");
            return Webx_Loc_CompanyList;
        }

        public DataSet GetLocCompanyObject(string Code, string Type)
        {
            DataSet DS = new DataSet();
            return DS;
        }

        public DataTable EmpLocCheckExists(string Code, string Type)
        {
            string QueryString = "EXEC USP_EmpLocCheckExists '" + Code + "','" + Type + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        public DataTable AddEditLocationCompany(string XML)
        {
            string QueryString = "exec usp_Insert_Update_Location_Company_Mapping_NewPortal'" + XML + "'";
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        #endregion

        #region Docket Rule

        public List<webx_rules_docket> GetRule()
        {

            string curFile = folderPath + "webx_rules_docket.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "select * from webx_rules_docket";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_rules_docket> List_XML = DataRowToObject.CreateListFromTable<webx_rules_docket>(dataTable);
                GF.SerializeParams<webx_rules_docket>(List_XML, folderPath + "webx_rules_docket.xml");
            }

            List<webx_rules_docket> ItemList = GF.DeserializeParams<webx_rules_docket>(folderPath + "webx_rules_docket.xml");

            return ItemList;

        }

        public List<Webx_Modules_Rules> GetModuleRule()
        {

            string curFile = folderPath + "Webx_Modules_Rules.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "select * from Webx_Modules_Rules";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<Webx_Modules_Rules> List_XML = DataRowToObject.CreateListFromTable<Webx_Modules_Rules>(dataTable);
                GF.SerializeParams<Webx_Modules_Rules>(List_XML, folderPath + "Webx_Modules_Rules.xml");
            }

            List<Webx_Modules_Rules> ItemList = GF.DeserializeParams<Webx_Modules_Rules>(folderPath + "Webx_Modules_Rules.xml");

            return ItemList;

        }

        public List<Webx_Date_Rules> GetDateRule(string ModuleCode)
        {
            string QueryString = "select * from Webx_Date_Rules where Module_Code='" + ModuleCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Date_Rules> List_XML = DataRowToObject.CreateListFromTable<Webx_Date_Rules>(dataTable);
            return List_XML;
        }

        public webx_rules_docket GetRuleObject(string Key)
        {
            Key = Key.ToUpper();

            webx_rules_docket objRule = new webx_rules_docket();

            var RuleList = GetRule().Where(c => c.code.ToUpper() == Key);

            if (RuleList.Count() > 0)
                objRule = RuleList.FirstOrDefault();

            return objRule;

        }

        public webx_rules_docket GetRuleObjectPaybas(string Key, string Paybas)
        {
            Key = Key.ToUpper();

            webx_rules_docket objRule = new webx_rules_docket();

            var RuleList = GetRule().Where(c => c.code.ToUpper() == Key && c.paybas == Paybas);

            if (RuleList.Count() > 0)
                objRule = RuleList.FirstOrDefault();

            return objRule;

        }


        #endregion

        #region Docket Field

        public List<CYGNUS_Manage_Modules> GetField()
        {

            string curFile = folderPath + "CYGNUS_Manage_Modules.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "select * from CYGNUS_Manage_Modules";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<CYGNUS_Manage_Modules> List_XML = DataRowToObject.CreateListFromTable<CYGNUS_Manage_Modules>(dataTable);
                GF.SerializeParams<CYGNUS_Manage_Modules>(List_XML, folderPath + "CYGNUS_Manage_Modules.xml");
            }

            List<CYGNUS_Manage_Modules> ItemList = GF.DeserializeParams<CYGNUS_Manage_Modules>(folderPath + "CYGNUS_Manage_Modules.xml");

            return ItemList;

        }

        public CYGNUS_Manage_Modules GetFieldObject(string Key)
        {
            Key = Key.ToUpper();

            CYGNUS_Manage_Modules objRule = new CYGNUS_Manage_Modules();

            var FieldList = GetField().Where(c => c.FieldCode.ToUpper() == Key);

            if (FieldList.Count() > 0)
                objRule = FieldList.FirstOrDefault();

            return objRule;

        }

        #endregion

        #region Vendor Contract

        //public List<Webx_Vendor_Contract_Summary_Ver1> GetVendorNameInVendorContract(string SelectionType)
        //{
        //    string commandText = "Exec sp_Get_VendorContract_Selection '" + SelectionType + "'  ";
        //    DataTable dataTable = GF.getdatetablefromQuery(commandText);
        //    List<Webx_Vendor_Contract_Summary_Ver1> ListVendorName = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_Summary_Ver1>(dataTable);
        //    return ListVendorName;
        //}

        public List<Webx_Vendor_Contract_Summary_Ver1> GetVendorNameInVendorContract(string Flag, string VendorType)
        {
            string commandText = "Exec USP_GetVendors '" + "" + "','" + Flag + "','" + "Y" + "','" + VendorType + "'  ";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<Webx_Vendor_Contract_Summary_Ver1> ListVendorName = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_Summary_Ver1>(dataTable);
            return ListVendorName;
        }

        public List<Webx_Vendor_Contract_Summary_Ver1> GetVendorContractObject(string VendorCode)
        {
            string QueryString = "exec usp_Vendor_Contract_Summary_Ver1_GetListByVendorCode '" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_Summary_Ver1> Webx_Vendor_Contract_Summary_Ver1List = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_Summary_Ver1>(dataTable);
            return Webx_Vendor_Contract_Summary_Ver1List;
        }

        public List<Webx_Vendor_Contract_DocketBased> GetVendorContractDocketObject(string VendorCode)
        {
            //string curFile = folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml";
            string QueryString = "exec usp_Vendor_Contract_DocketBased_GetByVendorCode '" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_DocketBased> DocketbasedList = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_DocketBased>(dataTable);
            return DocketbasedList;
        }

        public List<Webx_Vendor_Contract_DocketBased> GetVendorContractDocketChargesObject(string ContractID, string ChargeType)
        {
            //string curFile = folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml";
            string QueryString = "exec usp_Vendor_Contract_DocketBased_GetByChgType '" + ContractID + "','" + ChargeType + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_DocketBased> DocketbasedList = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_DocketBased>(dataTable);
            return DocketbasedList;
        }

        public List<Webx_Vendor_Contract_CityBased> GetVendorContractCityObject(string VendorCode)
        {
            //string curFile = folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml";
            string QueryString = "exec usp_Vendor_Contract_CityBased_GetByVendorCode '" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_CityBased> CitybasedList = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_CityBased>(dataTable);
            return CitybasedList;
        }

        public List<Webx_Vendor_Contract_DistanceBased> GetVendorContractDistanceObject(string VendorCode)
        {
            //string curFile = folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml";
            string QueryString = "exec usp_Vendor_Contract_DistanceBased_GetByVendorCode '" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_DistanceBased> DistancebasedList = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_DistanceBased>(dataTable);
            return DistancebasedList;
        }

        public List<Webx_Vendor_Contract_RouteBased> GetVendorContractRouteObject(string VendorCode)
        {
            //string curFile = folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml";
            string QueryString = "exec usp_Vendor_Contract_RouteBased_GetByVendorCode '" + VendorCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Contract_RouteBased> RoutebasedList = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_RouteBased>(dataTable);
            return RoutebasedList;
        }

        public DataTable AddEditVendorContract(string XML, string Flag)
        {
            string QueryString = "exec Webx_InsertUpdate_Vendor_Contract_Summary_Ver1 '" + XML + "','" + Flag + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditVendorContract", "", "");
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);

            //string QueryStringGet = "exec usp_Vendor_Contract_Summary_Ver1_List";
            //DataTable dataTableGet = GF.GetDataTableFromSP(QueryStringGet);
            //List<Webx_Vendor_Contract_Summary_Ver1> VendorContractList_XML = DataRowToObject.CreateListFromTable<Webx_Vendor_Contract_Summary_Ver1>(dataTableGet);
            //GF.SerializeParams<Webx_Vendor_Contract_Summary_Ver1>(VendorContractList_XML, folderPath + "Webx_Vendor_Contract_Summary_Ver1.xml");
            return dataTableAdd;
        }


        public DataTable AddEditVendorContractBased(string XML, string Flag, string Type, string XmlDC, string VendorCode)
        {
            string QueryString = "";
            if (Type == "RB")
            {
                QueryString = "exec Webx_InsertUpdate_Vendor_Contract_RouteBased '" + XML + "','" + Flag + "','" + VendorCode + "'";
            }
            if (Type == "CB")
            {
                QueryString = "exec Webx_InsertUpdate_Vendor_Contract_CityBased '" + XML + "','" + Flag + "','" + VendorCode + "'";
            }
            if (Type == "DB")
            {
                //QueryString = "exec Webx_InsertUpdate_Vendor_Contract_DistanceBased'" + XML + "','" + Flag + "','" + VendorCode + "'";
                QueryString = "exec Webx_InsertUpdate_Vendor_Contract_DistanceBased '" + XML + "','" + Flag + "'";
            }
            if (Type == "08" || Type == "XX5")
            {
                QueryString = "exec Webx_InsertUpdate_Vendor_Contract_DocketBased '" + XML + "','" + XmlDC + "','" + Flag + "','" + VendorCode + "'";
            }
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditVendorContractBased", "", "");
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }

        #endregion

        #region DCR

        #region DCR
        public List<vw_DocumentName_DCR> GetDCRNameObject()
        {
            string curFile = folderPath + "vw_DocumentName_DCR.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "SELECT CodeID=DocumentID,CodeDesc=DOC_Called_AS FROM vw_DocumentName_DCR WHERE ISNULL(ACTIVEFLAG,'N')='Y' Order By DOC_Called_AS";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<vw_DocumentName_DCR> vw_DocumentName_DCRList_XML = DataRowToObject.CreateListFromTable<vw_DocumentName_DCR>(dataTable);
                GF.SerializeParams<vw_DocumentName_DCR>(vw_DocumentName_DCRList_XML, folderPath + "vw_DocumentName_DCR.xml");
            }

            List<vw_DocumentName_DCR> vw_DocumentName_DCRList = GF.DeserializeParams<vw_DocumentName_DCR>(folderPath + "vw_DocumentName_DCR.xml");
            return vw_DocumentName_DCRList;
        }

        public string CheckBookCode(string DocType, string BookCode)
        {
            string sqlstr = "EXEC IsValiedDCRBookCode '" + DocType + "','" + BookCode + "'";
            return GF.executeScalerQuery(sqlstr);
        }

        public string CheckValidDCRSeries(string DocType, string DRSStartFrom, string TotalLeaf)
        {
            string sqlstr = "EXEC IsValiedDCRSeries '" + DocType + "','" + DRSStartFrom + "','" + TotalLeaf + "'";
            return GF.executeScalerQuery(sqlstr);
        }

        public DataTable AddUpdateDCR(string doc_array, string UserName)
        {
            string QueryString = "";

            QueryString = "exec usp_DCR_Insert_Multiple2'" + doc_array + "','" + UserName + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddUpdateDCR", "", "");
            DataTable dt = GF.GetDataTableFromSP(QueryString);
            return dt;
        }

        #endregion

        #region Allote

        public List<AlloteLocation> GetAlloteLocationDetails(string Location)
        {
            string SQRY = "exec usp_AllotToList'" + Location + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<AlloteLocation> Itemlist = DataRowToObject.CreateListFromTable<AlloteLocation>(Dt);
            return Itemlist;
        }

        public DataTable DCRAlloteList(string dcr_date_from, string dcr_date_to, string alloted_by, string doc_type)
        {
            string QueryString = "exec usp_Series_ToBe_Alloted '" + dcr_date_from + "','" + dcr_date_to + "','" + alloted_by + "',null,'" + doc_type + "'";

            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable DCRAllote(int doc_key, string alloted_by, string alloted_to, string empcd)
        {
            string QueryString = "exec usp_DCR_Allocate'" + doc_key + "','" + alloted_by + "','" + alloted_to + "','" + empcd + "'";

            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Allote-DeAllote

        public List<vw_Series_ToBe_Alloted2> AlloteDeAllote(string from_dt, string to_dt, string from_sr, string to_sr, string sr_list, string bookcode, int bus_type_id, string doc_type)
        {
            string SQL = "exec [usp_DCR_Series_ForReDeAllocate_newportal]'" + from_dt + " 00:00:00','" + to_dt + " 23:59:59','" + from_sr + "','" + to_sr + "','" + sr_list + "','" + bookcode + "','" + bus_type_id + "','" + doc_type + "'";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            List<vw_Series_ToBe_Alloted2> DCRLIST = DataRowToObject.CreateListFromTable<vw_Series_ToBe_Alloted2>(DT);
            return DCRLIST;
        }

        public List<vw_Series_ToBe_Alloted2> GetDCR(int Doc_Key)
        {
            string SQL = "select * from vw_DCR_Series_ForReDeAllocate2 where DOC_KEY='" + Doc_Key + "'";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            List<vw_Series_ToBe_Alloted2> ListDCR = DataRowToObject.CreateListFromTable<vw_Series_ToBe_Alloted2>(DT);
            return ListDCR;
        }

        public DataTable DeleteAlloteDeallote(int Doc_Key)
        {
            string SQL = "exec usp_Series_DeAllocate '" + Doc_Key + "'";
            return GF.GetDataTableFromSP(SQL);
        }


        public List<vw_Series_ToBe_Alloted2> UpdateAlloteDeallote(int Doc_Key, string reallot_to, string empcd)
        {
            string SQL = "exec usp_Series_ReAllocate '" + Doc_Key + "','" + reallot_to + "','" + empcd + "'";
            DataTable DT = GF.GetDataTableFromSP(SQL);
            List<vw_Series_ToBe_Alloted2> DCRLIST = DataRowToObject.CreateListFromTable<vw_Series_ToBe_Alloted2>(DT);
            return DCRLIST;
        }

        #endregion




        #region DCR Manage
        public List<vw_DCR_Register> GetDCRNameObject(string DOC_TYPE, string SR)
        {
            string QueryString = "SELECT * FROM vw_DCR_Register WHERE DOC_TYPE='" + DOC_TYPE + "' AND '" + SR + "' BETWEEN DOC_SR_FROM AND DOC_SR_TO AND LEN('" + SR + "')=LEN(DOC_SR_FROM)";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_DCR_Register> vw_DCR_RegisterList = DataRowToObject.CreateListFromTable<vw_DCR_Register>(dataTable);
            return vw_DCR_RegisterList;
        }

        public List<vw_DCR_Register> GetNewPersonAssignedObject(string Loccode, string Category)
        {
            string QueryString = "";
            QueryString = "EXEC USP_AllotToList_NewPortal '" + Loccode + "','" + Category + "'";
            //if (Category == "B")
            //{
            //    QueryString = "Select AlloteTo=H.VENDORCODE,AlloteToName=H.VENDORNAME From webx_vendor_hdr H INNER JOIN webx_vendor_det D on H.VENDORCODE=D.VENDORCODE where (H.vendor_type='8' or H.vendor_type='08' ) AND '" + Loccode + "' in (SELECT Items FROM dbo.split(D.vendorbrcd,',')) and IsNull(Active,'Y')='Y' and (Select Loc_Level From webx_location where loccode=ltrim(rtrim('" + Loccode + "')) )>1 Order By H.VENDORNAME";
            //}
            //if (Category == "E")
            //{
            //    QueryString = "SELECT AlloteTo=UserId,AlloteToName=Name FROM Webx_Master_Users WHERE BranchCode='" + Loccode + "' AND Status='100' ORDER BY [Name]";
            //}
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_DCR_Register> NewPersonAssignedList = DataRowToObject.CreateListFromTable<vw_DCR_Register>(dataTable);
            return NewPersonAssignedList;
        }

        //public List<vw_DCR_Register> GetNewsplitPersonAssignedObject(string Loccode, string Category)
        //{
        //    string QueryString = "";
        //    if (Category == "B")
        //    {
        //        QueryString = "Select AlloteTo=H.VENDORCODE,AlloteToName=H.VENDORCODE+':'+H.VENDORNAME From webx_vendor_hdr H INNER JOIN webx_vendor_det D on H.VENDORCODE=D.VENDORCODE where (H.vendor_type='8' or H.vendor_type='08' ) AND '" + Loccode + "' in (SELECT Items FROM dbo.split(D.vendorbrcd,',')) and IsNull(Active,'Y')='Y' and (Select Loc_Level From webx_location where loccode=ltrim(rtrim('" + Loccode + "')) )>1 Order By H.VENDORNAME";
        //    }
        //    if (Category == "E")
        //    {
        //        QueryString = "SELECT AlloteTo=UserId,AlloteToName=UserId+':'+[Name] FROM Webx_Master_Users WHERE BranchCode='" + Loccode + "' AND [Status]='100' ORDER BY [Name]";
        //    }
        //    DataTable dataTable = GF.GetDataTableFromSP(QueryString);
        //    List<vw_DCR_Register> NewPersonAssignedList = DataRowToObject.CreateListFromTable<vw_DCR_Register>(dataTable);
        //    return NewPersonAssignedList;
        //}

        public List<vw_DCR_Register> GetDCRSplitObject(string DocKey)
        {
            string QueryString = "usp_get_DCR_Status'','','" + DocKey + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_DCR_Register> DCRSplitList = DataRowToObject.CreateListFromTable<vw_DCR_Register>(dataTable);
            return DCRSplitList;
        }

        public List<vw_DCR_Management_History> GetDCRHistoryObject(string DocKey)
        {
            string QueryString = "SELECT Action_Desc,Action_Date,Book_Number,FROM_TO,Alloted_Type_Desc, Alloted_Loc=(CASE WHEN ISNULL(Alloted_Loc,'')='' THEN '' ELSE Alloted_Loc+' : '+Alloted_Loc_Name END),Alloted_To=(CASE WHEN ISNULL(Alloted_To,'')='' THEN '' ELSE Alloted_To+' : '+Alloted_To_Name END) FROM vw_DCR_Management_History WHERE DOC_KEY='" + DocKey + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_DCR_Management_History> DCRHistoryList = DataRowToObject.CreateListFromTable<vw_DCR_Management_History>(dataTable);
            return DCRHistoryList;
        }
        public DataTable DCRSplitDetail(int DOC_KEY, string BookCode, string LastDocNo, int diffQty, string Suffix, string DOC_NEW_SR_FROM, decimal TOT_LEAF, string AllotedBy, string Alloted_To, string Alloted_Type, int Business_Type_ID, string BaseUserName)
        {
            string QueryString = "exec usp_DCR_Split '" + DOC_KEY + "','" + BookCode + "','" + LastDocNo + "','" + diffQty + "','" + BookCode + "','" + Suffix + "','" + DOC_NEW_SR_FROM + "','" + Convert.ToInt32(TOT_LEAF) + "','" + AllotedBy + "','" + Alloted_Type + "','" + Alloted_To + "','" + Business_Type_ID + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DCRSplitDetail", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }
        public DataTable DCRReallocationDetail(string DOC_KEY, string Alloted_By, string Alloted_Type_ID, string Alloted_To, string BaseUserName)
        {
            string QueryString = "exec usp_Series_ReAllocate_new '" + DOC_KEY + "','" + Alloted_By + "','" + Alloted_Type_ID + "','" + Alloted_To + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DCRReallocationDetail", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #endregion

        #region Rate

        public List<webx_RateMatrix> GetRateMatrixObject(string FromCity, string ToCity, string Kg)
        {
            string QueryString = "exec USP_RateMatrix_List1 '" + FromCity + "','" + ToCity + "','" + Kg + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_RateMatrix> RateMatrixList = DataRowToObject.CreateListFromTable<webx_RateMatrix>(dataTable);
            return RateMatrixList;
        }

        #endregion

        #region Job Order

        #region Work Group Master

        public List<WEBX_FLEET_WORKGROUPMST> GetWorkGroupObject()
        {
            string curFile = folderPath + "WEBX_FLEET_WORKGROUPMST.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "SELECT W_GRPCD,W_GRPDESC,ENTRY_BY,CONVERT(VARCHAR,ENTRY_DT,106),RTRIM(LTRIM(Asset_Type)) as Asset_Type,(select CodeDesc from Webx_Master_General where CodeType='VEHASSTYP' and CodeId='01' )as Asset_Type_desc,ACTIVE_FLAG FROM [WEBX_FLEET_WORKGROUPMST] order by convert(datetime,ENTRY_DT,106)";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<WEBX_FLEET_WORKGROUPMST> WEBX_FLEET_WORKGROUPMSTList_XML = DataRowToObject.CreateListFromTable<WEBX_FLEET_WORKGROUPMST>(dataTable);
                GF.SerializeParams<WEBX_FLEET_WORKGROUPMST>(WEBX_FLEET_WORKGROUPMSTList_XML, folderPath + "WEBX_FLEET_WORKGROUPMST.xml");
            }

            List<WEBX_FLEET_WORKGROUPMST> WEBX_FLEET_WORKGROUPMSTList = GF.DeserializeParams<WEBX_FLEET_WORKGROUPMST>(folderPath + "WEBX_FLEET_WORKGROUPMST.xml");
            return WEBX_FLEET_WORKGROUPMSTList;
        }

        public DataTable AddEditWorkGroup(int GROUPCODE, string GROUPDESC, bool FLAG, string ENTRY_BY, string Asset_Type)
        {
            var ActiveFLAG = "";
            if (FLAG == true)
            {
                ActiveFLAG = "Y";
            }
            else
            {
                ActiveFLAG = "N";
            }
            string QueryString = "exec USP_INSERT_WORKGROUP'" + GROUPCODE + "','" + GROUPDESC + "','" + ActiveFLAG + "','" + ENTRY_BY + "','" + Asset_Type + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<WEBX_FLEET_WORKGROUPMST> WEBX_FLEET_WORKGROUPMSTList = DataRowToObject.CreateListFromTable<WEBX_FLEET_WORKGROUPMST>(DS.Tables[1]);
                GF.SerializeParams<WEBX_FLEET_WORKGROUPMST>(WEBX_FLEET_WORKGROUPMSTList, folderPath + "WEBX_FLEET_WORKGROUPMST.xml");
            }
            return DS.Tables[0];
        }

        public string CheckDuplicateWorkGroup(string GRPDESC)
        {
            string QueryString = "exec CheckDuplicateWorkGroup ' " + GRPDESC + " '";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        #region Task Type Master

        public List<Webx_Fleet_TaskTypeMst> GetTaskTypeObject()
        {
            string curFile = folderPath + "Webx_Fleet_TaskTypeMst.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "SELECT TaskTypeID,TaskType,AccCode,(SELECT AccCode + ' :' +AccDesc FROM WEBX_ACCTINFO WHERE ACCCATEGORY='EXPENSE' and Company_Acccode=WEBX_FLEET_TASKTYPEMST.AccCode) as AccDesc,ActiveFlag,EntryBy,Convert(VarChar,EntryDt,106) as EntryDt FROM [WEBX_FLEET_TASKTYPEMST]";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<Webx_Fleet_TaskTypeMst> Webx_Fleet_TaskTypeMstList_XML = DataRowToObject.CreateListFromTable<Webx_Fleet_TaskTypeMst>(dataTable);
                GF.SerializeParams<Webx_Fleet_TaskTypeMst>(Webx_Fleet_TaskTypeMstList_XML, folderPath + "Webx_Fleet_TaskTypeMst.xml");
            }

            List<Webx_Fleet_TaskTypeMst> Webx_Fleet_TaskTypeMstList = GF.DeserializeParams<Webx_Fleet_TaskTypeMst>(folderPath + "Webx_Fleet_TaskTypeMst.xml");
            return Webx_Fleet_TaskTypeMstList;
        }

        public List<webx_acctinfo> GetAcctTaskTypeObject()
        {
            string curFile = folderPath + "webx_acctinfoTask.xml";
            if (!File.Exists(curFile))
            {
                string QueryString = "select * from webx_acctinfo  WITH(NOLOCK) where ACCCATEGORY='EXPENSE' and ActiveFlag='Y'";
                DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                List<webx_acctinfo> webx_acctinfoTaskList_XML = DataRowToObject.CreateListFromTable<webx_acctinfo>(dataTable);
                GF.SerializeParams<webx_acctinfo>(webx_acctinfoTaskList_XML, folderPath + "webx_acctinfoTask.xml");
            }

            List<webx_acctinfo> webx_acctinfoTaskList = GF.DeserializeParams<webx_acctinfo>(folderPath + "webx_acctinfoTask.xml");
            return webx_acctinfoTaskList;
        }

        public DataTable AddEditTaskType(int TaskTypeId, string TaskType, string AccCode, bool ActiveFlag, string EntryBy)
        {
            var Active = "";
            if (ActiveFlag == true)
            {
                Active = "Y";
            }
            else
            {
                Active = "N";
            }
            string QueryString = "exec USP_INSERTUPDATE_WEBX_FLEET_TASKTYPEMST'" + TaskTypeId + "','" + TaskType + "','" + AccCode + "','" + Active + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditTaskType", "", "");

            DataSet DS = GF.GetDataSetFromSP(QueryString);
            if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            {
                List<Webx_Fleet_TaskTypeMst> Webx_Fleet_TaskTypeMstList = DataRowToObject.CreateListFromTable<Webx_Fleet_TaskTypeMst>(DS.Tables[1]);
                GF.SerializeParams<Webx_Fleet_TaskTypeMst>(Webx_Fleet_TaskTypeMstList, folderPath + "Webx_Fleet_TaskTypeMst.xml");
            }
            return DS.Tables[0];
        }

        //public string CheckDuplicateWorkGroup(string GRPDESC)
        //{
        //    string QueryString = "exec CheckDuplicateWorkGroup ' " + GRPDESC + " '";
        //    DataTable dataTable = GF.GetDataTableFromSP(QueryString);
        //    return dataTable.Rows[0][0].ToString();
        //}

        #endregion

        #region Update Vehicle Km Master

        public List<VehicleKm> GetVehicleKmObject()
        {
            //string curFile = folderPath + "VehicleKm.xml";
            //if (!File.Exists(curFile))
            //{
            string QueryString = "SELECT [VEH_INTERNAL_NO], [VEHNO], [Current_KM_Read], [Tmp_Current_KM_Read] FROM [webx_vehicle_hdr] WHERE ACTIVEFLAG='Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VehicleKm> VehicleKmList_XML = DataRowToObject.CreateListFromTable<VehicleKm>(dataTable);
            //    GF.SerializeParams<VehicleKm>(VehicleKmList_XML, folderPath + "VehicleKm.xml");
            //}

            //List<VehicleKm> VehicleKmList = GF.DeserializeParams<VehicleKm>(folderPath + "VehicleKm.xml");
            return VehicleKmList_XML;
        }

        public DataTable AddEditVehicleKmMaster(decimal TMP_CURRENT_KM_READ, int Veh_Internal_No)
        {
            string QueryString = "UPDATE WEBX_VEHICLE_HDR SET Tmp_Current_KM_Read=" + TMP_CURRENT_KM_READ + " WHERE  VEH_INTERNAL_NO=" + Veh_Internal_No + " select VEH_INTERNAL_NO,Tmp_Current_KM_Read,'Done' as TranXaction from WEBX_VEHICLE_HDR where VEH_INTERNAL_NO =" + Veh_Internal_No + "SELECT [VEH_INTERNAL_NO], [VEHNO], [Current_KM_Read], [Tmp_Current_KM_Read] FROM [webx_vehicle_hdr] WHERE ACTIVEFLAG='Y'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            //if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            //{
            //    if (DS.Tables[1].Rows.Count > 0)
            //    {
            //        List<VehicleKm> VehicleKmList = DataRowToObject.CreateListFromTable<VehicleKm>(DS.Tables[1]);
            //        GF.SerializeParams<VehicleKm>(VehicleKmList, folderPath + "VehicleKm.xml");
            //    }
            //    else
            //    {
            //        File.Delete(folderPath + "VehicleKm.xml");
            //    }
            //}
            return DS.Tables[0];
        }

        #endregion

        #region Mail Setting

        public List<Webx_Fleet_JobApprovalMailSettings> GetMailSettingObject()
        {
            string QueryString = "select * from Webx_Fleet_JobApprovalMailSettings";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_JobApprovalMailSettings> MailSettingList = DataRowToObject.CreateListFromTable<Webx_Fleet_JobApprovalMailSettings>(dataTable);
            return MailSettingList;
        }

        public DataTable AddEditMailSettingMaster(string SMTP_Server, string From_Address, string From_To, string Update_By)
        {
            string QueryString = "exec Usp_Update_Webx_Fleet_JobApprovalMailSettings '" + SMTP_Server + "','" + From_Address + "','" + From_To + "','" + Update_By + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            return DS.Tables[0];
        }

        #endregion

        #region Jobsheet Approved Amount Master

        public List<Webx_Fleet_JS_Approval_Hrchy_Amt> GetJobsheetApprovedAmountObject()
        {
            string QueryString = "Select ID,(SELECT CODEDESC FROM WEBX_MASTER_GENERAL WHERE CODETYPE='HRCHY' AND CODEID=LOC_HRCHY_CODE) AS CODEDESC,LOC_HRCHY_CODE,JS_TYPE,LOC_APPROVE_AMT,UPDATE_BY,LOC_APPROVE_AMT_BANK FROM Webx_Fleet_JS_Approval_Hrchy_Amt";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_JS_Approval_Hrchy_Amt> Webx_Fleet_JS_Approval_Hrchy_AmtList_XML = DataRowToObject.CreateListFromTable<Webx_Fleet_JS_Approval_Hrchy_Amt>(dataTable);
            return Webx_Fleet_JS_Approval_Hrchy_AmtList_XML;
        }

        public DataTable AddEditJobsheetApprovedAmountMaster(int Id, string LOC_HRCHY_CODE, decimal LOC_APPROVE_AMT, string JS_TYPE, string UPDATE_BY, decimal LOC_APPROVE_AMT_BANK)
        {
            string QueryString = "exec Usp_Update_Webx_Fleet_JS_Approval_Hrchy_Amt_NewPortal '" + Id + "','" + LOC_HRCHY_CODE + "','" + LOC_APPROVE_AMT + "','" + LOC_APPROVE_AMT_BANK + "','" + JS_TYPE + "','" + UPDATE_BY + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            //if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            //{
            //    if (DS.Tables[1].Rows.Count > 0)
            //    {
            //        List<Webx_Fleet_JS_Approval_Hrchy_Amt> Webx_Fleet_JS_Approval_Hrchy_AmtList = DataRowToObject.CreateListFromTable<Webx_Fleet_JS_Approval_Hrchy_Amt>(DS.Tables[1]);
            //        GF.SerializeParams<Webx_Fleet_JS_Approval_Hrchy_Amt>(Webx_Fleet_JS_Approval_Hrchy_AmtList, folderPath + "Webx_Fleet_JS_Approval_Hrchy_Amt.xml");
            //    }
            //    else
            //    {
            //        File.Delete(folderPath + "Webx_Fleet_JS_Approval_Hrchy_Amt.xml");
            //    }
            //}
            return DS.Tables[0];
        }


      

        #endregion

        #region  General Task

        public List<WEBX_FLEET_GENERALTASKMST> GetTaskListObject()
        {
            string QueryString = "exec usp_WorkGroupList @GroupCode='0'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_GENERALTASKMST> GENERALTASKList = DataRowToObject.CreateListFromTable<WEBX_FLEET_GENERALTASKMST>(dataTable);
            return GENERALTASKList;
        }

        public List<WEBX_FLEET_GENERALTASKMST> GetGeneralTaskListObject(string GRPCD)
        {
            string QueryString = "";
            QueryString = "Select CONVERT(int,W_GRPCD) as W_GRPCD,[TASKCD],[TASKDESC],[LABOUR_HRS],[ACTIVE_FLAG],TASKTYP,(SELECT TaskType FROM Webx_Fleet_TaskTypeMst Where TaskTypeId=TASKTYP ) AS [TASKTYPDesc] From WEBX_FLEET_GENERALTASKMST (nolock) Where W_GRPCD='" + GRPCD + "' and ACTIVE_FLAG='Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_GENERALTASKMST> GENERALTASKList = DataRowToObject.CreateListFromTable<WEBX_FLEET_GENERALTASKMST>(dataTable);
            return GENERALTASKList;
        }
        public DataTable AddEditGeneralTask(decimal W_GRPCD, decimal TASKCD, string TASKDESC, decimal LABOUR_HRS, string ACTIVE_FLAG, string ENTRY_BY, string TASKTYP)
        {
            var Active = "";
            if (ACTIVE_FLAG == "true")
            {
                Active = "Y";
            }
            else
            {
                Active = "N";
            }
            string QueryString = "exec usp_Insert_WEBX_FLEET_GENERALTASKMST'" + W_GRPCD + "','" + TASKCD + "','" + TASKDESC + "','" + LABOUR_HRS + "','" + Active + "','" + ENTRY_BY + "','" + TASKTYP + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Spare Part
        public List<Webx_Fleet_spareParthdr> GetSparePartObject(string Fromdate, string Todate, string PartID)
        {
            string QueryString = "exec usp_SparePartHdr_List'" + Fromdate + "','" + Todate + "','" + PartID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_spareParthdr> WEBX_FLEET_SPAREPARTHDR_List = DataRowToObject.CreateListFromTable<Webx_Fleet_spareParthdr>(dataTable);
            return WEBX_FLEET_SPAREPARTHDR_List;
        }
        public List<WEBX_FLEET_SPAREPARTDET> GetSparePartDETObject(string PartID)
        {
            string QueryString = "exec usp_SparePartDet_List'" + PartID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WEBX_FLEET_SPAREPARTDET> WEBX_FLEET_SPAREPARTDET_List = DataRowToObject.CreateListFromTable<WEBX_FLEET_SPAREPARTDET>(dataTable);
            return WEBX_FLEET_SPAREPARTDET_List;
        }
        public List<webx_PO_SKU_Master> GetWorkGroupListObject()
        {
            string QueryString = "exec usp_WorkGroupList_SaprePart '0','Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_PO_SKU_Master> WorkGroup_List = DataRowToObject.CreateListFromTable<webx_PO_SKU_Master>(dataTable);
            return WorkGroup_List;
        }
        public DataTable GetNewPartId()
        {
            string QueryString = "exec usp_CreateSparePartCode";
            return GF.GetDataTableFromSP(QueryString);
        }
        public DataTable AddEditSparePart(Webx_Fleet_spareParthdr WFSHM, string AddEdit)
        {
            string QueryString = "exec usp_SparePartHdrInsertUpdate '" + AddEdit + "','" + WFSHM.Part_Id + "','" + WFSHM.Part_Type + "','" + WFSHM.Part_Category + "','" + WFSHM.Selling_Price + "','" + WFSHM.Avg_Cost + "','" + WFSHM.Description + "','" + WFSHM.Unit_Of_Inventory + "','" + WFSHM.Reorder_Point + "','" + WFSHM.Reorder_Qty + "','" + WFSHM.ExpDt + "','" + WFSHM.ExpKms + "','" + WFSHM.W_GRPDC + "','" + WFSHM.SKU_GRPDC + "','" + WFSHM.TaskTypeId + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        //public DataTable AddEditSparePartDet(string Part_ID, string Vendorcode, string Vendor_Part_No, string Manufacturer, decimal Cost, string Leadtime, string AddEdit)
        //{
        //    string QueryString = "exec usp_SparePartDetInsertUpdate '" + AddEdit + "','" + Part_ID + "','" + Vendorcode + "','" + Vendor_Part_No + "','" + Manufacturer + "','" + Cost + "','" + Leadtime + "'";
        //    return GF.GetDataTableFromSP(QueryString);
        //}

      
        public DataTable AddEditSparePartDet(string XML, string AddEdit)
        {
            string QueryString = "exec usp_SparePartDetInsertUpdate '" + XML + "','" + AddEdit + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddEditSparePartDet", "", "");
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryString);
            return dataTableAdd;
        }
        #endregion

        #region Jobsheet Approved Amount Matrix Master

        public List<VW_FLEET_JS_APPROVAL_AMT_MATRIX> GetJobsheetApprovedAmountMatrixObject()
        {
            string QueryString = "Select * from VW_FLEET_JS_APPROVAL_AMT_MATRIX";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VW_FLEET_JS_APPROVAL_AMT_MATRIX> VW_FLEET_JS_APPROVAL_AMT_MATRIXList = DataRowToObject.CreateListFromTable<VW_FLEET_JS_APPROVAL_AMT_MATRIX>(dataTable);
            return VW_FLEET_JS_APPROVAL_AMT_MATRIXList;
        }

        public DataTable AddEditJobsheetApprovedAmountMatrixMaster(decimal Approved_Id, string Approver_UserId, string CC_UserId, string UPDATE_BY)
        {
            string QueryString = "exec Usp_Update_Fleet_JS_Approval_Matrix '" + Approved_Id + "','" + Approver_UserId + "','" + CC_UserId + "','" + UPDATE_BY + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region SM Task
        public List<SMTaskList> GetSmTaskList()
        {
            string QueryString = "SELECT [SMTask_Id], VEH_TYPE_CODE,(dbo.fc_VehicleModel(VEH_TYPE_CODE)) AS VEH_TYPE_NAME FROM [Webx_Fleet_SM_Task_Hdr]";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<SMTaskList> SmTaskList = DataRowToObject.CreateListFromTable<SMTaskList>(dataTable);
            return SmTaskList;
        }
        public List<SMTask> GetVehicleType()
        {
            string QueryString = "select TYPE_CODE,TYPE_NAME FROM webx_Vehicle_Type Where ActiveFlag='Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<SMTask> ItemList = DataRowToObject.CreateListFromTable<SMTask>(dataTable);
            return ItemList;
        }
        public List<SMTask> GetVehiclaDetforSmTask(int Id)
        {
            string QueryString = "";
            if (Id == 0)
            {
                QueryString = "exec USP_Fill_Vehicle_Model''";
            }
            else
            {
                QueryString = "exec USP_Get_Vehicle_Model_Edit'" + Id + "'";
            }
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<SMTask> VehiclaDetforSmTaskList = DataRowToObject.CreateListFromTable<SMTask>(dataTable);
            return VehiclaDetforSmTaskList;
        }
        public List<Webx_Fleet_SM_Task_Hdr> GetSmTask(int Id)
        {
            string QueryString = "select *from Webx_Fleet_SM_Task_Hdr Where SMTask_Id='" + Id + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_SM_Task_Hdr> SmTaskList = DataRowToObject.CreateListFromTable<Webx_Fleet_SM_Task_Hdr>(dataTable);
            return SmTaskList;
        }
        public List<Webx_Fleet_SM_Task_Det> GetSmTaskDet(int Id)
        {
            string QueryString = "select *from Webx_Fleet_SM_Task_Det Where SMTask_Id='" + Id + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Fleet_SM_Task_Det> SmTaskDetList = DataRowToObject.CreateListFromTable<Webx_Fleet_SM_Task_Det>(dataTable);
            return SmTaskDetList;
        }
        public List<TaskTypeList> GetTaskTypeList()
        {
            string QueryString = "exec usp_SMTaskTypeList'1'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<TaskTypeList> TaskTypeList = DataRowToObject.CreateListFromTable<TaskTypeList>(dataTable);
            return TaskTypeList;
        }
        public DataTable AddEditSMTask(Webx_Fleet_SM_Task_Hdr WFSHM, string AddEdit)
        {
            string QueryString = "";
            if (AddEdit == "Add")
            {
                QueryString = "exec USP_Insert_Webx_Fleet_SM_Task_Hdr'" + WFSHM.VEH_TYPE_CODEValue + "','" + WFSHM.Entry_By + "'";
            }
            else
            {
                QueryString = "exec USP_Update_Webx_Fleet_SM_Task_Hdr'" + WFSHM.SMTask_Id + "','" + WFSHM.VEH_TYPE_CODEValue + "','" + WFSHM.Entry_By + "'";
            }
            return GF.GetDataTableFromSP(QueryString);
        }
        public DataTable AddEditSMTaskDet(string W_GRPCD, string TASKDESC, string TASKTYP, string T_UOM, decimal SERVICE_INTERVALS_DAY, decimal SERVICE_INTERVALS_KMS, decimal ADV_NOTIFICATION_DAY, decimal ADV_NOTIFACATION_KMS, string ACTIVE_FLAG, string ENTRY_BY, int SMTask_Id, int Task_Id, decimal Estimated_Hrs)
        {
            string QueryString = "exec Usp_Insert_Webx_Fleet_SM_Task_Det'" + W_GRPCD + "','" + TASKDESC + "','" + TASKTYP + "','" + T_UOM + "','" + SERVICE_INTERVALS_DAY + "','" + SERVICE_INTERVALS_KMS + "','" + ADV_NOTIFICATION_DAY + "','" + ADV_NOTIFACATION_KMS + "','" + ACTIVE_FLAG + "','" + ENTRY_BY + "','','" + SMTask_Id + "','" + Task_Id + "','" + Estimated_Hrs + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion



        #region Set Opening Balance

        public List<webx_acctinfo> OpeningBalanceCriteria(string Brcd, string Accountcat, string AccountCode, string BRCD)
        {
            string SQRY = "";
            string strValue = BRCD;
            string[] strArray = strValue.Split('-');
            if (AccountCode == "All")
            {
                AccountCode = "";
            }
            SQRY = "exec Usp_Account_Data_Opnening_Balance '" + AccountCode + "', '" + Accountcat + "','" + Brcd + "','" + strArray[0] + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> MRList_Data = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt1);
            return MRList_Data;
        }


        public DataTable GetsetopnigBalancecategory(string Category)
        {
            string SQRY = "exec USP_Account_Category'" + Category + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }


        public DataTable GetsetopnigBalance()
        {
            string SQRY = "exec USP_Location_setopnigBalance";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable InsertSetOpeningBalnace(string XML)
        {
            string QueryString = "exec usp_SetOpeningBalance '" + XML + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            //if (DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0]["TranXaction"].ToString() == "Done")
            //{
            //    if (DS.Tables[1].Rows.Count > 0)
            //    {
            //        List<webx_acctinfo> webx_acctinfoGeneralList = DataRowToObject.CreateListFromTable<webx_acctinfo>(DS.Tables[1]);
            //        GF.SerializeParams<webx_acctinfo>(webx_acctinfoGeneralList, folderPath + "webx_acctinfo.xml");
            //    }
            //    else
            //    {
            //        File.Delete(folderPath + "webx_acctinfo.xml");
            //    }
            //}
            return DS.Tables[0];
        }


        #endregion

        #region Cheque Print

        public List<WEBX_chq_det> ChqPrintDetailLIST(string CHQNO, string DATEFROM, string DATETO, string transdate, string Voucherno)
        {
            string SQRY = "exec Usp_Chq_PrintList_New_Portal '" + CHQNO + "','" + DATEFROM + "','" + DATETO + "','" + transdate + "','" + Voucherno + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_chq_det> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_chq_det>(Dt1);
            return MRList_Data;
        }
        #endregion

        #endregion

        #region Stackholder

        #region CustomerGroup BusinessType Mapping
        public List<WebX_master_grpbus_mapping> CustomerGroupBusinessTypeMappingList()
        {
            string QueryString = "SELECT* from WebX_master_grpbus_mapping WITH (NOLOCK)";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<WebX_master_grpbus_mapping> itmList = DataRowToObject.CreateListFromTable<WebX_master_grpbus_mapping>(DT);
            return itmList;
        }
        public DataTable AddCustomerGroupBusinessTypeMapping(string strxml)
        {
            string QueryString = "exec USP_INSERT_grpbus_mapping'" + strxml + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion

        #region Get Vendor Service
        public List<Webx_Vendor_Service_Mapping_master> GetVendorServiceList(string Vendorcode, string ServiceCode)
        {
            string QueryString = "select * from Webx_Vendor_Service_Mapping_master where  vendorcode = '" + Vendorcode + "' or vendor_service_code = '" + ServiceCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Vendor_Service_Mapping_master> VendorServiceList = DataRowToObject.CreateListFromTable<Webx_Vendor_Service_Mapping_master>(dataTable);
            return VendorServiceList;
        }

        public DataTable DeleteVendoerServiceMapping(string mode, string Code, string VenCode)
        {
            string QueryString = "";
            if (mode == "1")
            {
                QueryString = "delete from Webx_Vendor_Service_Mapping_master where Vendorcode='" + VenCode + "'";
            }
            else
            {
                QueryString = "delete from Webx_Vendor_Service_Mapping_master where Vendor_Service_Code='" + Code + "'";
            }
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddVendoerServiceMapping(string mode, string Code, string VenCode)
        {
            string QueryString = "";
            if (mode == "1")
            {
                QueryString = "EXEC usp_InsertVendorServiceMapping'" + VenCode + "','" + Code + "'";
            }
            else
            {
                QueryString = "EXEC usp_InsertVendorServiceMapping'" + VenCode + "','" + Code + "'";
            }
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion

        #region Mapping Master

        public List<WebX_Master_Service> GetServiceList()
        {
            string QueryString = "SELECT * FROM WebX_Master_Service Where ISNULL(Active,'N')='Y' Order by ServiceName ASC";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<WebX_Master_Service> ServiceList = DataRowToObject.CreateListFromTable<WebX_Master_Service>(dataTable);
            return ServiceList;
        }
        public List<vw_ServiceList_Mapping> GetServiceListForUserModule()
        {
            string QueryString = "select * from vw_ServiceList_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_ServiceList_Mapping> ServiceList = DataRowToObject.CreateListFromTable<vw_ServiceList_Mapping>(dataTable);
            return ServiceList;
        }
        public List<vw_UserList_Mapping> GetUserListForUserModule()
        {
            string QueryString = "Select * from vw_UserList_Mapping Order by Name ASC";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_UserList_Mapping> UserList = DataRowToObject.CreateListFromTable<vw_UserList_Mapping>(dataTable);
            return UserList;
        }
        public List<vw_ModuleList_Mapping> GetModuleList()
        {
            string QueryString = "Select * from vw_ModuleList_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_ModuleList_Mapping> ModuleList = DataRowToObject.CreateListFromTable<vw_ModuleList_Mapping>(dataTable);
            return ModuleList;
        }
        public List<Webx_Cust_Service_Mapping> GetCustServiceList()
        {
            string QueryString = "SELECT * from Webx_Cust_Service_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Cust_Service_Mapping> ServiceList = DataRowToObject.CreateListFromTable<Webx_Cust_Service_Mapping>(dataTable);
            return ServiceList;
        }
        public List<Webx_Cust_Location_Mapping> GetCustLocationList()
        {
            string QueryString = "SELECT * from Webx_Cust_Location_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Cust_Location_Mapping> LocationList = DataRowToObject.CreateListFromTable<Webx_Cust_Location_Mapping>(dataTable);
            return LocationList;
        }
        public List<Webx_Service_Location_Mapping> GetServiceLocationList()
        {
            string QueryString = "SELECT * from Webx_Service_Location_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Service_Location_Mapping> ServiceLocationList = DataRowToObject.CreateListFromTable<Webx_Service_Location_Mapping>(dataTable);
            return ServiceLocationList;
        }
        public List<Webx_User_Service_Mapping> GetUserServiceList()
        {
            string QueryString = "select * from Webx_User_Service_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_User_Service_Mapping> UserServiceList = DataRowToObject.CreateListFromTable<Webx_User_Service_Mapping>(dataTable);
            return UserServiceList;
        }
        public List<Webx_User_Module_Mapping> GetUserModuleList()
        {
            string QueryString = "select * from Webx_User_Module_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_User_Module_Mapping> UserModuleList = DataRowToObject.CreateListFromTable<Webx_User_Module_Mapping>(dataTable);
            return UserModuleList;
        }
        public List<MappingService> GetMappingServiceList(string MapType, string Mode)
        {
            string QueryString = "EXEC USP_GetMappingServiceList'" + MapType + "','" + Mode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<MappingService> MappingServiceList = DataRowToObject.CreateListFromTable<MappingService>(dataTable);
            return MappingServiceList;
        }
        public DataTable DeleteMappingService(string MapType, string OppCode, string Code)
        {
            string QueryString = "EXEC USP_DeleteMappingService'" + MapType + "','" + OppCode + "','" + Code + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        public DataTable AddMappingService(string MapType, string OppCode, string Code, string Name, string UserName, string Scode)
        {
            string QueryString = "EXEC USP_AddEditMappingService'" + MapType + "','" + OppCode + "','" + Code + "','" + Name + "','" + UserName + "','" + Scode + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion

        #endregion

        #region Rights report / Module

        public DataTable Assign_View_TrackRights(string UserId, string l1, decimal l2, string l3)
        {
            String Str = "exec usp_Assign_View_TrackRights '" + UserId + "','" + l1 + "','" + l2 + "','" + l3 + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        public List<Webx_View_Track> Get_ListOfReportsView(string user)
        {
            string QueryString = "Select chacked=(Case When IsNull(UserId,'')='' then 'false' Else 'true' End),Level0Text=(case when title='O' then 'Operations Documnets' when title='A' then 'Accounts Documnets' when title='F' then 'Finance Documnets' end),Level1Text='',  DOC_Called_AS=[DOC_Called_AS],'' as L0_App_Module,srno as L1_App_Module,'' as App_Module From [Webx_View_Track] with(NOLOCK) Left Outer Join [Webx_Master_View_TracksAccess] with(NOLOCK)  On srno = ReportID and HasAccess='Y' and UserId='" + user + "'  where activeflag='Y' order by title";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_View_Track> itmList = DataRowToObject.CreateListFromTable<Webx_View_Track>(DT);
            return itmList;
        }

        public List<ModuleAcessModel> Get_ListOfModulRights(string user)
        {
            string QueryString = "exec usp_GetModuleAccessRights '" + user + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<ModuleAcessModel> itmList = DataRowToObject.CreateListFromTable<ModuleAcessModel>(DT);
            return itmList;
        }

        public List<ModuleAcessModel> GetLevel1(string strLevel, string ParentCode, string ModuleCode)
        {
            string SQRY = "exec usp_getmoduleLevel '" + strLevel + "','" + ParentCode + "','" + ModuleCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<ModuleAcessModel> UserList = DataRowToObject.CreateListFromTable<ModuleAcessModel>(Dt);
            return UserList;
        }

        public DataTable AssignModuleRight(string M1, string M2, string M3, string userid, string haseAccess)
        {
            String Str = "exec usp_AssignModuleRight '" + M1 + "','" + M2 + "','" + M3 + "','" + userid + "','" + haseAccess + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return Dt;
        }

        #endregion

        #region ChequeDepositVoucher

        public List<WEBX_chq_det> ChqDepositeCriteria(string CHQNO, string DATEFROM, string DATETO, string COLBRCD, string DATETYPE, string COMPANY_CODE, string TableName)
        {
            string SQRY = "exec Usp_DepoVoucher_Chqlist_Ver2 '" + CHQNO + "', '" + DATEFROM + "','" + DATETO + "','" + COLBRCD + "','" + DATETYPE + "','" + COMPANY_CODE + "','" + TableName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_chq_det> MRList_Data = DataRowToObject.CreateListFromTable<WEBX_chq_det>(Dt1);
            return MRList_Data;
        }

        public DataTable CashBankAccount(string BRCD, string Category)
        {
            string SQR = "exec USP_BankCODE '" + BRCD + "','" + Category + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable InsertChequeTransaction(string Voucherno, string TransDate, string Chqindex, string FinYear, string Narration, string empcd, string BankCode, string COMPANY_CODE)
        {
            string SQRY = "exec usp_ChequeTranscation_Ver2 '" + Voucherno + "','" + TransDate + "','" + Chqindex + "','" + FinYear + "','" + Narration + "','" + empcd + "','" + BankCode + "','" + COMPANY_CODE + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertChequeTransaction", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        //#region Customer Service Mapping

        //public DataTable GetByCustomerServiceList()
        //{
        //    string SQRY = "exec USP_ByCustomerServiceList";
        //    DataTable DT = GF.GetDataTableFromSP(SQRY);
        //    return DT;
        //}

        //public List<webx_CUSTHDR> GetByCustomerList()
        //{
        //    string SQR = "exec USP_ByCustomerList";
        //    DataTable Dt = GF.GetDataTableFromSP(SQR);
        //    List<webx_CUSTHDR> listDriver = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);
        //    return listDriver;
        //}

        //public List<webx_CUSTHDR> CustServiceList()
        //{
        //    string QueryString = "SELECT * from Webx_Cust_Service_Mapping";
        //    DataTable dataTable = GF.GetDataTableFromSP(QueryString);
        //    List<webx_CUSTHDR> ServiceList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
        //    return ServiceList;
        //}
        //public List<webx_CUSTHDR> MappingServiceList(string MapType, string Mode)
        //{
        //    string QueryString = "EXEC USP_GetCustMapingServiceList'" + "1" + "','" + Mode + "'";
        //    DataTable dataTable = GF.GetDataTableFromSP(QueryString);
        //    List<webx_CUSTHDR> MappingServiceList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
        //    return MappingServiceList;
        //}

        //public DataTable DeleteCustMappingService(string MapType, string OppCode, string Code)
        //{
        //    string QueryString = "EXEC USP_DeleteMappingService'" + MapType + "','" + OppCode + "','" + Code + "'";
        //    return GF.GetDataTableFromSP(QueryString);
        //}

        //public DataTable AddCustMappingService(string MapType, string OppCode, string Code, string Name, string UserName, string Scode)
        //{
        //    string QueryString = "EXEC USP_AddEditMappingService'" + MapType + "','" + OppCode + "','" + Code + "','" + Name + "','" + UserName + "','" + Scode + "'";
        //    return GF.GetDataTableFromSP(QueryString);
        //}

        //#endregion

        #region Handling Charge

        public DataTable DeleteHandlingChrge()
        {


            string sqlstr = " insert into CYGNUS_HandlingCharge_Branch_Edit(Type,ContractType,BranchCode,LoadingBy,RateType,Rate,MaxAmount,VendorCode,MonthlyAmount,";
            sqlstr = sqlstr + "IsMonthlyFixed,CheckCharge,EntryBy,EntryDate,IsCustomer,CustomerCode,CustomerCharges,Edit_EntryDate,srno,IsMathadi)   ";
            sqlstr = sqlstr + "select Type,ContractType,BranchCode,LoadingBy,RateType,Rate,MaxAmount,VendorCode,MonthlyAmount,  ";
            sqlstr = sqlstr + "IsMonthlyFixed,CheckCharge,EntryBy,EntryDate,IsCustomer,CustomerCode,CustomerCharges,getdate(),Id,IsMathadi  ";
            sqlstr = sqlstr + " from CYGNUS_HandlingCharge_Branch where isnull(CheckCharge,0)=1 AND ISNULL(IsMathadi,0)=0";

            sqlstr = sqlstr + "; DELETE FROM CYGNUS_HandlingCharge_Branch where CheckCharge=1 AND ISNULL(IsMathadi,0)=0";
            return GF.GetDataTableFromSP(sqlstr);
        }

        //public DataTable DeleteLoadingChrge(string Branch, string LoadingBy, string Type)
        //{
        //    string sqlstr = "exec USP_HandlingCharge_Branch_Delet '" + Branch + "','" + LoadingBy + "','" + Type + "'";
        //    return GF.GetDataTableFromSP(sqlstr);
        //}

        //public List<webx_HandlingCharge> GetHandlingChrgeLoad(string Type)

        public DataTable InsertHandlingChrge(string detail_XML, string Branch, string Type, string BaseUserName)
        {
            string sqlstr = "exec USP_Insert_HandlingChrge_Loading_Unloading '" + detail_XML + "','" + Branch + "','" + Type + "','" + BaseUserName + "' ";
            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "InsertHandlingChrge", "", "");
            return GF.GetDataTableFromSP(sqlstr);

            //string sqlstr = "INSERT INTO CYGNUS_HandlingCharge_Branch(BranchCode,Type,ContractType,LoadingBy,RateType,Rate,MaxAmount,CheckCharge,EntryBy,EntryDate)";
            //sqlstr = sqlstr + " VALUES('" + brcdCode + "',"; 
            //sqlstr = sqlstr + "'" + Type + "','" + ContractType + "','" + LoadingBy + "','" + RateType + "'," + Rate + "," + MaxAmount + "," + "1" + ",'" + username + "','" + date + "')";
            //return GF.GetDataTableFromSP(sqlstr);
        }

        public List<webx_HandlingCharge> GetHandlingChrgeLoad()
        {
            //string sqlstr = "SELECT * FROM CYGNUS_HandlingCharge_Branch ";
            string sqlstr = "SELECT * FROM CYGNUS_HandlingCharge_Branch A LEFT OUTER JOIN webx_location B on A.BranchCode = B.LocCode WHERE B.ActiveFlag='Y' ";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_HandlingCharge> HandlingChrgesList = DataRowToObject.CreateListFromTable<webx_HandlingCharge>(dataTable);
            return HandlingChrgesList;
        }

        public DataTable DeleteHandlingChrgeLoad(int Id)
        {
            string sqlstr = "exec USP_DeleteHandlingChrgeLoad '" + Id + "'";
            return GF.GetDataTableFromSP(sqlstr);
        }

        public DataTable InsertLoadingChrge(string detail_XML, string Type, string Branch, string LoadingBy, string BaseUserName)
        {
            string sqlstr = "exec USP_Insert_HandlingCharge_Branch_Wise '" + detail_XML + "','" + Type + "','" + Branch + "','" + LoadingBy + "','" + BaseUserName + "' ";
            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "InsertLoadingChrge", "", "");
            return GF.GetDataTableFromSP(sqlstr);
        }

        #endregion

        #region Customer Loading charges

        public DataTable InsertCustomerLoadingChrge(string brcdCode, string Type, string LoadingBy, string RateType, decimal Rate, decimal Customercharge, string CustomerCode, string IsCustomer, string username, string date)
        {
            string sqlstr = "INSERT INTO CYGNUS_HandlingCharge_Branch(BranchCode,Type,LoadingBy,RateType,Rate,CustomerCharges,CustomerCode,IsCustomer,EntryBy,EntryDate)";
            sqlstr = sqlstr + " VALUES('" + brcdCode + "',";
            sqlstr = sqlstr + "'" + Type + "','" + LoadingBy + "','" + RateType + "'," + Rate + "," + Customercharge + ",'" + CustomerCode + "'," + IsCustomer + ",'" + username + "','" + date + "')";
            return GF.GetDataTableFromSP(sqlstr);
        }

        public DataTable UpdatecustomerCharges(int Id, string brcdCode, string Type, string LoadingBy, string RateType, decimal Rate, decimal Customercharge, string CustomerCode)
        {
            string sqlstr = "Update CYGNUS_HandlingCharge_Branch SET Type ='" + Type + "',BranchCode='" + brcdCode + "',LoadingBy='" + LoadingBy + "',RateType='" + RateType + "'";
            sqlstr = sqlstr + ",CustomerCode='" + CustomerCode + "',Rate=" + Rate + ",CustomerCharges=" + Customercharge + " WHERE Id=" + Id + "";
            return GF.GetDataTableFromSP(sqlstr);
        }

        #endregion

        #region GetLoadingCharge

        public List<webx_VENDOR_HDR> getBranchWiseLoadingUnloadingVendorListJson()
        {
            string SQRY = "SELECT * FROM VW_getBranchWiseLoadingUnloadingVendorList";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> VendorList = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
            return VendorList;
        }

        public List<CYGNUS_THC_Financial_Details> GetLoadingCharge(string brdc, string loadingby, string chargeType, string TypeModule, string Vendorcode, string loadunloadType)
        {
            string QueryString = "EXEC Get_LoadinUnloadingCharge '" + brdc + "','" + loadingby + "','" + chargeType + "','" + TypeModule + "','" + Vendorcode + "','" + loadunloadType + "'";
            DataTable Dt = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_THC_Financial_Details> GetLoadingChargeList = DataRowToObject.CreateListFromTable<CYGNUS_THC_Financial_Details>(Dt);
            return GetLoadingChargeList;
        }

        public List<CYGNUS_THC_Financial_Details> GetBrachWiseMathadiCharge(string BRCD, string LoadingBy, string ContractType, string Type)
        {
            string QueryString = "EXEC USP_GetBranch_DocumentTypeWiseMathadiRate '" + BRCD + "','" + LoadingBy + "','" + ContractType + "','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_THC_Financial_Details> GetLoadingChargeList = DataRowToObject.CreateListFromTable<CYGNUS_THC_Financial_Details>(Dt);
            return GetLoadingChargeList;
        }

        public List<CYGNUS_THC_Financial_Details> GetChargesFromCNoteNo(string DockNo, string RateType, string LoadType, string LoadingBy, string TypeModule, string Vendorcode, string BRCD)
        {
            string QueryString = "EXEC USP_GetChargesFromCNoteNo '" + DockNo + "','" + RateType + "','" + LoadType + "','" + LoadingBy + "','" + TypeModule + "','" + Vendorcode + "','" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_THC_Financial_Details> GetLoadingChargeList = DataRowToObject.CreateListFromTable<CYGNUS_THC_Financial_Details>(Dt);
            return GetLoadingChargeList;
        }

        #endregion

        #region Change KM

        public DataTable GetKMListDetail(string VhNo, string type)
        {
            string SQRY = "";
            if (type == "V")
            {
                SQRY = "SELECT VEHNO,Current_KM_Read,PRS_DRS_THC_KM,VEH_INTERNAL_NO from webx_VEHICLE_HDR WHERE VEHNO='" + VhNo + "'";
            }
            else
            {
                SQRY = "SELECT f_issue_startkm,VehicleNo from WEBX_FLEET_VEHICLE_ISSUE WHERE VSlipNo='" + VhNo + "'";
            }
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable ChangeKM(string VEHNO, decimal Current_KM_Read, decimal PRS_DRS_THC_KM, int VEH_INTERNAL_NO, decimal f_issue_startkm, string KMChgTyp, string VehicleNo, string VSlipNo)
        {
            string QueryString = "";
            if (KMChgTyp == "V")
            {
                QueryString = "UPDATE webx_VEHICLE_HDR WITH(ROWLOCK) SET Current_KM_Read='" + Current_KM_Read + "',PRS_DRS_THC_KM='" + PRS_DRS_THC_KM + "' WHERE VEHNO='" + VEHNO + "' and VEH_INTERNAL_NO='" + VEH_INTERNAL_NO + "'";
                QueryString = QueryString + "Select VEHNO From webx_VEHICLE_HDR where VEHNO ='" + VEHNO + "'";
            }
            else
            {
                QueryString = "UPDATE webx_VEHICLE_HDR WITH(ROWLOCK) SET Current_KM_Read='" + f_issue_startkm + "',PRS_DRS_THC_KM='" + f_issue_startkm + "' WHERE VEHNO='" + VehicleNo + "' ";
                QueryString = QueryString + "UPDATE WEBX_FLEET_VEHICLE_ISSUE WITH(ROWLOCK) SET f_issue_startkm='" + f_issue_startkm + "' WHERE VehicleNo='" + VehicleNo + "' and VSlipNo='" + VSlipNo + "'";
                QueryString = QueryString + "Select VSlipNo From WEBX_FLEET_VEHICLE_ISSUE where VSlipNo ='" + VSlipNo + "'";
            }

            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Add Account

        public List<WEBX_GROUPS> Get_AccountGroupList(string Id)
        {
            string SQRY = "select Groupcode,Groupdesc + ' : ' + Company_Groupcode as Groupdesc,Groupdesc as GroupdescMain  FROM WEBX_GROUPS where Main_Category ='" + Id + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_GROUPS> TyreModelList = DataRowToObject.CreateListFromTable<WEBX_GROUPS>(Dt);
            return TyreModelList;
        }

        public List<Webx_Master_General> GetAccountCategoryList()
        {
            string SQRY = "select Acccategory as CodeId,Acccategory as CodeDesc from webx_Acccategory_master where ActiveFlag='Y' order by CodeDesc";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General> LocationList = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return LocationList;
        }

        public DataTable Insert_Account_Master(string AccountCode, string AccountDescription, string AccGroup, string Category, string BRCD, string BaseUserName, string ACTIVEFLAG, string AccountNo, string BankLocationCode, string company_acccode, string COMPANY_CODE)
        {
            string SQRY = "exec usp_Account_Master '" + AccountCode + "','" + AccountDescription + "','" + AccGroup + "','" + Category + "','" + BRCD + "','" + BaseUserName + "','" + ACTIVEFLAG + "','" + AccountNo + "','" + BankLocationCode + "','" + company_acccode + "','" + COMPANY_CODE + "'  ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Account_Master", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable usp_Update_Account_Master(string AccountCode, string AccountDescription, string AccGroup, string Category, string BRCD, string BaseUserName, string ACTIVEFLAG, string AccountNo, string BankLocationCode, string company_acccode, string COMPANY_CODE)
        {
            string SQRY = "exec usp_Update_Account_Master_newPortal '" + AccountCode + "','" + AccountDescription + "','" + AccGroup + "','" + Category + "','" + BRCD + "','" + BaseUserName + "','" + ACTIVEFLAG + "','" + AccountNo + "','" + BankLocationCode + "','" + company_acccode + "','" + COMPANY_CODE + "'  ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "usp_Update_Account_Master", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<WEBX_GROUPS> Get_AccountGroupListAll(string Id)
        {
            string SQRY = "select Acccode as Groupcode,(Accdesc + ' : ' + Acccode) as Groupdesc from webx_acctInfo WITH(NOLOCK) Where groupcode in(select groupcode from webx_groups where main_category = '" + Id + "') order by accdesc";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WEBX_GROUPS> TyreModelList = DataRowToObject.CreateListFromTable<WEBX_GROUPS>(Dt);
            return TyreModelList;
        }

        #endregion

        #region add/Edit Account Groups

        public List<webx_Groups> GroupMainCategoryList()
        {
            string SQRY = "";
            SQRY = "exec USP_GetGroupMainCategory";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_Groups> GMList_Data = DataRowToObject.CreateListFromTable<webx_Groups>(Dt1);
            return GMList_Data;
        }

        public DataTable GetGroupListObject(string searchTerm, string Maincategory, string locationCode)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_getGroupListFromMainCategory '" + searchTerm + "','" + Maincategory + "','" + locationCode + "'";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable GetGroupEntrytoEdit(string mainGroup, string GroupCodeforMainGroup, string GroupCode, string CompanyCode)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_getGroupEntry '" + mainGroup + "','" + GroupCodeforMainGroup + "','" + GroupCode + "','" + CompanyCode + "'";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable EditGroupEntry(string GroupCode, string GroupDescription, string CompanyGroupCode, string UserName, string CompanyCode)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_UpdateGroupEntry '" + GroupCode + "','" + GroupDescription + "','" + CompanyGroupCode + "','" + UserName + "','" + CompanyCode + "'";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable AddGroupEntry(string GroupDescription, string ParentCode, string CompanyGroupCode, string MainCategory, string UserName, string CompanyCode)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_InsertGroupEntry '" + GroupDescription + "','" + ParentCode + "','" + CompanyGroupCode + "','" + MainCategory + "','" + UserName + "','" + CompanyCode + "'";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }
        #endregion

        #region Customer Service Mapping

        public DataTable GetByCustomerServiceList()
        {
            string SQRY = "exec USP_ByCustomerServiceList";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<webx_CUSTHDR> GetByCustomerList()
        {
            string SQR = "exec USP_ByCustomerList";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_CUSTHDR> listDriver = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);
            return listDriver;
        }


        public List<webx_CUSTHDR> CustServiceList()
        {
            string QueryString = "SELECT * from Webx_Cust_Service_Mapping";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> ServiceList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return ServiceList;
        }

        public List<webx_CUSTHDR> MappingServiceList(string MapType, string Mode)
        {
            string QueryString = "EXEC USP_GetCustMapingServiceList '" + "1" + "','" + Mode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> MappingServiceList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return MappingServiceList;
        }

        public DataTable DeleteCustMappingService(string MapType, string OppCode, string Code)
        {
            string QueryString = "EXEC USP_DeleteMappingService '" + MapType + "','" + OppCode + "','" + Code + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddCustMappingService(string MapType, string OppCode, string Code, string Name, string UserName, string Scode)
        {
            string QueryString = "EXEC USP_AddEditMappingService '" + MapType + "','" + OppCode + "','" + Code + "','" + Name + "','" + UserName + "','" + Scode + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<webx_CUSTHDR> MappingCustomerListByService(string MapType, string Mode, string ServiceCode)
        {
            string QueryString = "EXEC USP_GetCustMapingServiceCustomerList '" + MapType + "','" + Mode + "','" + ServiceCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> MappingServiceList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return MappingServiceList;
        }

        #endregion

        #region Settings Opening Balance // Customer /Vendor / Driver / Employee

        public List<webx_acctinfo> CustomerTypeList()
        {
            string SQRY = "";
            SQRY = "exec USP_GetCustomerTypeList";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> MRList_Data = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt1);
            return MRList_Data;
        }

        public List<webx_acctinfo> EmployeeTypeList()
        {
            string SQRY = "";
            SQRY = "exec [USP_GetEmployeeTypeList]";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> MRList_Data = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt1);
            return MRList_Data;
        }

        public List<webx_acctinfo> DriverTypeList()
        {
            string SQRY = "";
            SQRY = "exec [USP_GetDriverTypeList]";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_acctinfo> MRList_Data = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt1);
            return MRList_Data;
        }
        public List<webx_VENDOR_HDR> GetEffectiveVendorTypeList()
        {
            string SQRY = "EXEC usp_GetEffectiveVendorTypeList";
            DataTable dt = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> List_Data = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(dt);
            return List_Data;
        }

        public DataTable GetSettingOpeningBalanceList(string partytyp, string VenType, string partyCode, string Account, string Location, string YearSuffix)
        {
            string SQRY = "Exec USP_GetListForOpeningBalance '" + partytyp + "','" + VenType + "','" + partyCode + "','" + Account + "','" + Location + "','" + YearSuffix + "'";
            DataTable dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable SettingOpeningBalance(string Type, string OpenDebit, string OpenCredit, string AccCode, string PartyCode, string Location, string UserName, string CompanyCode, string YearSuffix)
        {
            if (Type == "Customer")
            {
                string SQRY = "Exec usp_SetPartyOpeningBalance_NewPortal '" + OpenDebit + "','" + OpenCredit + "','" + AccCode + "','" + PartyCode + "','" + Location + "','" + UserName + "','" + CompanyCode + "','" + YearSuffix + "'";
                DataTable dt = GF.GetDataTableFromSP(SQRY);
                return dt;
            }
            else if (Type == "Vendor")
            {
                string SQRY = "Exec usp_SetPartyOpeningBalance_NewPortal '" + OpenDebit + "','" + OpenCredit + "','" + AccCode + "','" + PartyCode + "','" + Location + "','" + UserName + "','" + CompanyCode + "','" + YearSuffix + "'";
                DataTable dt = GF.GetDataTableFromSP(SQRY);
                return dt;
            }
            else if (Type == "Employee")
            {
                string SQRY = "Exec usp_SetPartyOpeningBalance_NewPortal '" + OpenDebit + "','" + OpenCredit + "','" + AccCode + "','" + PartyCode + "','" + Location + "','" + UserName + "','" + CompanyCode + "','" + YearSuffix + "'";
                DataTable dt = GF.GetDataTableFromSP(SQRY);
                return dt;
            }
            else if (Type == "Driver")
            {
                string SQRY = "Exec usp_SetPartyOpeningBalance_NewPortal '" + OpenDebit + "','" + OpenCredit + "','" + AccCode + "','" + PartyCode + "','" + Location + "','" + UserName + "','" + CompanyCode + "','" + YearSuffix + "'";
                DataTable dt = GF.GetDataTableFromSP(SQRY);
                return dt;
            }
            else
            {
                DataTable dt = new DataTable();
                return dt;
            }
        }
        #endregion

        #region Balanse Transfer

        public DataTable LedgerSumbitType(string TransType, string Ledger, string LedgerType, string BaseFinYear, string BaseUserName, string BaseCompanyCode)
        {
            string QueryString = "exec [Usp_Balance_TRF_New_Portal] '" + TransType + "','" + Ledger + "','" + LedgerType + "','" + BaseFinYear + "','" + BaseUserName + "','" + BaseCompanyCode + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "LedgerSumbitType", "", "");
            return DS.Tables[0];
        }

        public DataTable SubLedgerSumbitType(string TransType, string Ledger, string LedgerType, string BaseFinYear, string BaseUserName, string BaseCompanyCode)
        {
            string QueryString = "exec [Usp_Balance_TRF_New_Portal] '" + TransType + "','" + Ledger + "','" + LedgerType + "','" + BaseFinYear + "','" + BaseUserName + "','" + BaseCompanyCode + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "SubLedgerSumbitType", "", "");
            return DS.Tables[0];
        }

        public DataTable PndLedgerSumbitType(string TransType, string Ledger, string LedgerType, string BaseFinYear, string BaseUserName, string BaseCompanyCode)
        {
            string QueryString = "exec [Usp_Balance_TRF_New_Portal] '" + TransType + "','" + Ledger + "','" + LedgerType + "','" + BaseFinYear + "','" + BaseUserName + "','" + BaseCompanyCode + "'";
            DataSet DS = GF.GetDataSetFromSP(QueryString);
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "PndLedgerSumbitType", "", "");
            return DS.Tables[0];
        }

        #endregion

        public List<webx_pincode_master> GetPincodeList()
        {
            string QueryString = "SELECT pincode,statecode,cityname FROM webx_pincode_master";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_pincode_master> itmList = DataRowToObject.CreateListFromTable<webx_pincode_master>(DT);
            return itmList;
        }

        public List<webx_pincode_master> GetAreaList()
        {
            string QueryString = "SELECT pincode,Area FROM webx_pincode_master";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_pincode_master> itmList = DataRowToObject.CreateListFromTable<webx_pincode_master>(DT);
            return itmList;
        }

        #region Voucher BackDate Access

        public Cygnus_Master_Voucher_BackDate_Access GetVoucherBackDateAccess(string Username, string ModuleName)
        {
            Cygnus_Master_Voucher_BackDate_Access Entry = new Cygnus_Master_Voucher_BackDate_Access();
            string QRY = "exec VoucherBackDateAccess '" + Username + "','" + ModuleName + "'";
            DataTable DT = GF.GetDataTableFromSP(QRY);
            List<Cygnus_Master_Voucher_BackDate_Access> itmList = DataRowToObject.CreateListFromTable<Cygnus_Master_Voucher_BackDate_Access>(DT);
            if (itmList.Count > 0)
            {
                Entry = itmList.FirstOrDefault();
            }
            return Entry;
        }

        #endregion

        #region User Wise Reports Rights

        public List<CYGNUS_Master_Reports> GetReportList(string ReportType, string ReportSubType, string UserName, int Type)
        {
            //string SQRY = "select * from CYGNUS_Master_Reports Where ReportType= '" + ReportType.ToString() + "' and  ReportSubType= '" + ReportSubType.ToString() + "'";
            string SQRY = "exec USP_GetUserWiseReportAccessListForUser '" + UserName + "','" + ReportType + "','" + ReportSubType + "','" + Type + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            List<CYGNUS_Master_Reports> EnquiryList = DataRowToObject.CreateListFromTable<CYGNUS_Master_Reports>(Dt1);
            return EnquiryList;
        }


        public DataTable Add_Report_Rights(string XML, string Location, string UserName, string CompanyCode)
        {
            string SQRY = "exec Usp_InsertUpdate_UserWise_Reports_Rights '" + XML + "','" + Location + "','" + UserName + "','" + CompanyCode + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(SQRY);
            return Dt1;
        }

        #endregion

        #region Loading UnLoading VendorType Mapping

        public List<CYGNUS_LoadingUnLoading_VendorType_Mapping> GetLoadingUnLoadingVendorTypeMappingList(string Loadingtype, string Location, string DocumentType)
        {
            string QueryString = "USP_GetLoadingUnLoadingVendorTypeMappingList '" + Loadingtype + "','" + Location + "','" + DocumentType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_LoadingUnLoading_VendorType_Mapping> itmList = DataRowToObject.CreateListFromTable<CYGNUS_LoadingUnLoading_VendorType_Mapping>(DT);
            return itmList;
        }

        public bool InsertLoadingUnLoadingVendorTypeMapping(string XML, string branch)
        {
            bool Status = false;
            string SQRY = "exec USP_InsertLoadingUnLoading_VendorType_Mapping '" + XML + "','" + branch + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertLoadingUnLoadingVendorTypeMapping", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            if (DT.Rows.Count > 0 && DT.Rows[0]["STATUS"].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion

        #region Loading / Unloading Data Changes Process

        public List<CYGNUS_LoadingUnLoading_Charges> GetLoadingUnloadingDataList(LoadingUnloadingDataChange objData)
        {
            string QueryString = "EXEC USP_LoadingUnloadingDataListForUpdatation '" + GF.FormateDate(objData.FromDate) + "','" + GF.FormateDate(objData.ToDate) + "','" + objData.Branch + "','" + objData.Type + "','" + objData.DocumentNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_LoadingUnLoading_Charges> dataList = DataRowToObject.CreateListFromTable<CYGNUS_LoadingUnLoading_Charges>(DT);
            return dataList;
        }

        #endregion

        #region Vendor Employee Mapping

        public List<Cygnus_VendorEmployeeMapping> GetVendorEmployeeMappingList()
        {
            string QueryString = "SELECT * FROM Cygnus_VendorEmployeeMapping WITH(NOLOCK)";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Cygnus_VendorEmployeeMapping> itmList = DataRowToObject.CreateListFromTable<Cygnus_VendorEmployeeMapping>(DT);
            return itmList;
        }

        public bool InsertVendorEmployeeMapping(string XML, string UserName)
        {
            bool Status = false;
            string SQRY = "exec USP_InsertVendor_Employee_Mapping '" + XML + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertVendorEmployeeMapping", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            if (DT.Rows.Count > 0 && DT.Rows[0]["STATUS"].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion

        #region  Wilf Craft GC Multiple View & Print

        public string Check_DocketNo_GCView(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_Check_DocketNo '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        #endregion

        #region  Customer Employee Mapping Master

        public List<webx_CUSTHDR> GetEmployeeForMapping(string searchTerm, string Location)
        {
            string QueryString = "exec [USP_GetEmployeeForMapping] '" + searchTerm + "','" + Location + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public DataTable CustomerEmployeeMappingSubmit(string CustCode, string XML, string BaseUserName, string CustEmpType)
        {
            string SQRY = "exec usp_Map_Customer_To_Employee '" + CustCode + "','" + XML + "','" + BaseUserName + "','" + CustEmpType + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region  PRS DRS Max Limit Master

        public DataTable GePRSDRSMaxList(string PDCType, string BRANCH)
        {
            string SQRY = "exec USP_GET_PRDDRSMaxLimitList '" + PDCType + "','" + BRANCH + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public List<webx_Vehicle_Type> GetVehicleTypeList()
        {
            string QueryString = "select Type_Code,Type_Name from webx_Vehicle_Type where ActiveFlag='Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_Vehicle_Type> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_Vehicle_Type>(dataTable);
            return webx_CUSTHDRList;
        }

        public DataTable AddPRSDRSMaxLimit(string InnerXml, string PDCType, string BRCD, string BaseUserName)
        {
            string QueryString = "EXEC USP_AddPRSDRSMaxLimit'" + InnerXml + "','" + PDCType + "','" + BRCD + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion


        #region  PDC Attached Vendor master
        public List<webx_VENDOR_HDR> GetVenratetype(string VENDORCODE)
        {
            string QueryString = "select   (select   CodeDesc from Webx_Master_General where CodeId=RateType and CodeType='PDCRATETYP' and StatusCode='Y') as RateType, (select   CodeId from Webx_Master_General where CodeId = RateType and CodeType = 'PDCRATETYP' and StatusCode = 'Y') as RateID from webx_VENDOR_HDR where VENDORCODE='" + VENDORCODE + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_VENDOR_HDR> itmList = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(DT);
            return itmList;
        }
        public List<PDC_attached_vendor_master> GetPDCAttachedvendorDetails()
        {
            string SQRY = "exec USP_GetPDCAttachedvendorDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<PDC_attached_vendor_master> PDCAttachedvendorList = DataRowToObject.CreateListFromTable<PDC_attached_vendor_master>(Dt);
            return PDCAttachedvendorList;
        }
        public List<PDC_attached_vendor_master> GetPDCAttachedvendorDetailsDatetable()
        {
            string SQRY = "exec USP_GetPDCAttachedvendorDetailsDatatables";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<PDC_attached_vendor_master> PDCAttachedvendorList = DataRowToObject.CreateListFromTable<PDC_attached_vendor_master>(Dt);
            return PDCAttachedvendorList;
        }
        public bool AddEditPDCattachedvendor(string MstDetails, string BaseUserName, string BaseLocationCode)
        {
            bool Status = false;
            string SQRY = "EXEC USP_AddEditPDCattachedvendor'" + MstDetails + "','" + BaseUserName + "','" + BaseLocationCode + "','" + 1 + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "AddEditPDCattachedvendor", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }
        #endregion

        #region Contractual Vendor master

        public List<webx_rutmas> GetRoutemodewiseDetails(string Mode)
        {
            string SQR = "SELECT RUTCD,RUTCD +':'+RUTNM as RUTNM  FROM webx_rutmas (NOLOCK)    where RUTMOD='" + Mode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_rutmas> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_rutmas>(Dt);

            return GetCustomerVendorList;
        }

        public List<PDC_attached_vendor_master> GetContractualdvendorDetails()
        {
            string SQRY = "exec USP_GetContractualvendorDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<PDC_attached_vendor_master> PDCAttachedvendorList = DataRowToObject.CreateListFromTable<PDC_attached_vendor_master>(Dt);
            return PDCAttachedvendorList;
        }

        public List<PDC_attached_vendor_master> GetContractualdvendorDetailsDatatables()
        {
            string SQRY = "exec USP_GetContractualvendorDetailsDatatables";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<PDC_attached_vendor_master> PDCAttachedvendorList = DataRowToObject.CreateListFromTable<PDC_attached_vendor_master>(Dt);
            return PDCAttachedvendorList;
        }

        public bool AddEditContractualvendor(string MstDetails, string BaseUserName, string BaseLocationCode)
        {
            bool Status = false;
            string SQRY = "EXEC USP_AddEditPDCattachedvendor'" + MstDetails + "','" + BaseUserName + "','" + BaseLocationCode + "','" + 2 + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "AddEditPDCattachedvendor", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion


        #region  Customer Customer Mapping

        public List<webx_DCR_Header> GetSeriasList(string BookCode, string IsSuffix, string SuffixChar, string SrFrom, string Location, string UserName, string CompanyCode)
        {
            string QueryString = "EXEC USP_GetSeriasList '" + BookCode + "','" + IsSuffix + "','" + SuffixChar + "','" + SrFrom + "','" + Location + "','" + UserName + "','" + CompanyCode + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_DCR_Header> List = DataRowToObject.CreateListFromTable<webx_DCR_Header>(dataTable);
            return List;
        }

        public List<webx_CUSTHDR> GetMappedCustomerForSeries(string Location, string UserName)
        {
            string QueryString = "exec USP_GetMappedCustomerForSeries '" + Location + "','" + UserName + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> List = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return List;
        }

        public DataTable InsertCustomerSeries(string XML, string UserName, string Location, string CompnayCode)
        {
            string SQRY = "exec USP_SeriasAddToCustomer '" + XML + "','" + UserName + "','" + Location + "','" + CompnayCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertCustomerSeries", "", "");
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            return dataTable;
        }

        #endregion

        #region SubCustomer Assign Docket Series

        public List<CYGNUS_Docket_CustSeries_DET> Getdktseriessubcustomer(string Cust)
        {
            string SQRY = "EXEC USP_Get_SubCustomer_BookCode'" + Cust + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Docket_CustSeries_DET> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Docket_CustSeries_DET>(Dt);
            return itmList;
        }
        public List<CYGNUS_Docket_CustSeries_DET> Getdktseriescriteriasubcustomer(string SrFrom, string BookCode)
        {
            string SQRY = "SELECT (CustMapping.CustomerCode+':'+Cust.CUSTNM) as CustCode,CustomerCode,*  FROM CYGNUS_Docket_CustSeries_HDR CustMapping INNER JOIN webx_CUSTHDR Cust ON CustMapping.CustomerCode = Cust.CUSTCD where CustMapping.BookCode= '" + BookCode + "' or CustMapping.SrFrom = '" + SrFrom + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Docket_CustSeries_DET> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Docket_CustSeries_DET>(Dt);
            return itmList;
        }

        public DataTable AddEditdktseriessubcustomer(string InnerXml, string BRCD, string CustCode, string BaseUserName)
        {
            string QueryString = "EXEC USP_AddEditdktseriessubcustomer'" + InnerXml + "','" + BRCD + "','" + CustCode + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region  Customer to Customer Mapping

        public DataTable GeMappingCustList(string Cust)
        {
            string SQRY = "SELECT  (CustMapping.SubCustCode+':'+Cust.CUSTNM) as SubCustCode ,* FROM CYGNUS_CUST_To_CUST_Mapping CustMapping INNER JOIN webx_CUSTHDR Cust ON CustMapping.SubCustCode = Cust.CUSTCD where CustMapping.CustCode= '" + Cust + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable AddEditMappincust(string InnerXml, string BRCD, string CustCode, string BaseUserName)
        {
            string QueryString = "EXEC USP_AddEditCustToCustMapping'" + InnerXml + "','" + BRCD + "','" + CustCode + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region PRQ Location change

        public List<CYGNUSPickupRequest> GETPRQBRCDList(string DocketNo)
        {
            string SQRY = "exec USP_PRQ_BRCD_ChangeList '" + DocketNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUSPickupRequest> MRList_Data = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(Dt1);
            return MRList_Data;
        }

        public DataTable PRQ_BRCD_Chnage_Submit(string ChangeBRCD, string PRQNo, string BaseUserName)
        {
            string SQRY = "exec USP_PRQ_BRCD_Chnage_Submit '" + ChangeBRCD + "','" + PRQNo + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "PRQ_BRCD_Chnage_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Leave Master

        public string InsertLeaveDetails(string Xml_Mst_Details)
        {
            string Status = "";
            string sql = "";
            try
            {
                sql = "exec Usp_CYGNUS_LeaveDetail_Insert '" + Xml_Mst_Details.ReplaceSpecialCharacters() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertLeaveDetails", "", "");
                Status = GF.GetDataTableFromSP(sql).Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return Status;
        }

        public List<CYGNUS_Leave_Master> GetLeaveObject(string userId, string type)
        {
            string QueryString = "exec USP_LeaveDetail_List '" + userId + "','" + type + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Leave_Master> LeaveList = DataRowToObject.CreateListFromTable<CYGNUS_Leave_Master>(dataTable);
            return LeaveList;
        }

        public string LeaveApprovReject(string id, string userId, string type, string Approv_Reject_By)
        {
            string Status = "0";
            string SQRY = "exec USP_ApprovOrRejectUserLeave '" + id + "','" + userId + "','" + type + "','" + Approv_Reject_By + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "LeaveApprovReject", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            if (DT.Rows.Count > 0 && DT.Rows[0]["STATUS"].ToString() == "1")
            {
                Status = DT.Rows[0]["STATUS"].ToString();
            }
            return Status;
        }

        #endregion

        #region Designation Mapping

        public List<CYGNUS_Designation_Mapping> GetDesignationMappingList()
        {
            string QueryString = "SELECT CDM.SrNo,WMG.CodeId AS Category,ISNULL(CDM.Designation,'') AS Designation,CDM.Active,CDM.Entryby,CDM.Entrydt FROM Webx_Master_General WMG WITH(NOLOCK) LEFT JOIN CYGNUS_Designation_Mapping CDM WITH(NOLOCK) ON CDM.Category = WMG.CodeId WHERE CodeType='HIERARCHY'  AND WMG.StatusCode = 'Y'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Designation_Mapping> LeaveList = DataRowToObject.CreateListFromTable<CYGNUS_Designation_Mapping>(dataTable);
            return LeaveList;
        }

        public string InsertDesignationMappingDetails(string Xml_Mst_Details, string userName)
        {
            string Status = "";
            string sql = "";
            try
            {
                sql = "exec USP_InsertDesignationMapping '" + Xml_Mst_Details.ReplaceSpecialCharacters() + "','" + userName + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertDesignationMappingDetails", "", "");
                Status = GF.GetDataTableFromSP(sql).Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return Status;
        }

        #endregion

        #region Crossing Vendor Master
        public List<vw_Vendor_For_Crossing> GetVendor(string searchterm)
        {
            var Querystring = "Exec USP_GetVendorDetails_New_Portal '" + searchterm + "'";
            DataTable Dt = GF.GetDataTableFromSP(Querystring);
            List<vw_Vendor_For_Crossing> itmList = DataRowToObject.CreateListFromTable<vw_Vendor_For_Crossing>(Dt);
            return itmList;
        }
        //public List<webx_vendor_Crossing_Contract_Det> getCrossingVendordata(string vendor)
        //{
        //    var Querystring = "exec USP_GetCrossingVendordataDetails "+"'"+vendor+"'";
        //    DataTable Dt = GF.GetDataTableFromSP(Querystring);
        //    List<webx_vendor_Crossing_Contract_Det> itmList = DataRowToObject.CreateListFromTable<webx_vendor_Crossing_Contract_Det>(Dt);
        //    return itmList;
        //}
        public webx_vendor_Crossing_Contract_Det getCrossingVendorData(string vendorcode)
        {
            webx_vendor_Crossing_Contract_Det VM = new webx_vendor_Crossing_Contract_Det();
            try
            {
                string QueryString = "SELECT TOP 1 * FROM webx_vendor_Crossing_Contract_HDR WHERE isnull(ActiveFlag,'N')='Y' AND VendorCode='" + vendorcode.Trim() + "'";
                DataTable DT = GF.getdatetablefromQuery(QueryString);

                if (DT.Rows.Count > 0)
                {
                    foreach (DataRow dr in DT.Rows)
                    {
                        if (dr["ContractID"].ToString() != "")
                        {
                            VM = DataRowToObject.CreateItemFromRow<webx_vendor_Crossing_Contract_Det>(dr);
                            VM.ListWVCCD = GetByContractID(dr["ContractID"].ToString(), vendorcode);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                VM = new webx_vendor_Crossing_Contract_Det();

                throw ex;
            }
            return VM;

        }
        public List<webx_vendor_Crossing_Contract_Det> GetByContractID(string ContractID, string vendorcode)
        {
            string QueryString = "Exec GetByContractID '" + ContractID + "'";
            List<webx_vendor_Crossing_Contract_Det> ListRet = new List<webx_vendor_Crossing_Contract_Det>();
            try
            {
                DataTable DT = GF.GetDataTableFromSP(QueryString);
                if (DT.Rows.Count > 0)
                {
                    foreach (DataRow dr in DT.Rows)
                    {
                        if (dr["ContractID"].ToString() != "")
                        {
                            webx_vendor_Crossing_Contract_Det Object = DataRowToObject.CreateItemFromRow<webx_vendor_Crossing_Contract_Det>(dr);
                            if (Object.VendorCode == null)
                                Object.VendorCode = vendorcode;
                            ListRet.Add(Object);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ListRet = new List<webx_vendor_Crossing_Contract_Det>();
                throw ex;
            }

            return ListRet;
        }


        public DataTable AddEditVendorMaster(string XML, string VendorCode, string BaseUserName)
        {
            string QueryString = "exec webx_vendor_Crossing_Contract_Det_Insert_NewPortal '" + XML + "'," + VendorCode + "," + BaseUserName + "";
            DataTable DT = GF.GetDataTableFromSP(QueryString);

            return DT;
        }
        public DataTable deleteVendorMaster(String VendorCode)
        {
            string QueryString = "exec USP_DeleteVendorMaster'" + VendorCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);

            return DT;
        }
        public string GenerateContractId()
        {
            string QueryString = "Exec webx_Generate_VendorCrossing_ContractCode";
            string DS = GF.executeScalerQuery(QueryString);

            return DS;
        }
        public List<webx_location> GetLocation(string vendorcode)
        {
            string QueryString = "SELECT LocCode, (LocCode + ':' + LocName) as LocName from Webx_Location where (SELECT dbo.SplitWithSearch((SELECT VENDORBRCD From webx_Vendor_DET where VendorCode = '" + vendorcode + "'),',',LocCode))= '1'";
            DataTable DT = GF.getdatetablefromQuery(QueryString);
            List<webx_location> ListLocation = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return ListLocation;
        }
        public void Delete(string xml)
        {
            try
            {
                string QueryString = "Exec webx_vendor_Crossing_Contract_DET_Delete_New_Portal '" + xml + "'";
                int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DeleteCrossingVendorContract", "", "");
                GF.executeNonQuery(QueryString);
            }
            catch (Exception excp)
            {
                throw excp;
            }
        }
        public void Insert(string Xml)
        {
            try
            {
                string QueryString = "Exec webx_vendor_Crossing_Contract_Det_Insert_New_Portal '" + Xml + "'";
                int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "InsertCrossingVendorContract", "", "");
                GF.executeNonQuery(QueryString);

            }
            catch (Exception excp)
            {
                throw excp;
            }

        }
        public void Update(string Xml)
        {

            try
            {
                string QueryString = "Exec webx_vendor_Crossing_Contract_Det_Update_New_Portal '" + Xml + "'";
                int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "UpdateCrossingVendorContract", "", "");

                GF.executeNonQuery(QueryString);
            }
            catch (Exception excp)
            {
                throw excp;
            }
        }
        public void InsertHDR(string Xml)
        {

            try
            {
                string QueryString = "Exec webx_vendor_Crossing_Contract_HDR_Insert_New_Portal '" + Xml + "'";
                GF.executeNonQuery(QueryString);
            }
            catch (Exception excp)
            {
                throw excp;
            }
        }


        #endregion

        #region Industry - Product Mapping Master

        public List<Webx_Industry_Product_Mapping> GetIndustryProductCustomerdata(int Type)
        {
            string QueryString = "";
            QueryString = "EXEC usp_Get_IndustryWise_Product_Mapping'" + Type + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Industry_Product_Mapping> LeaveList = DataRowToObject.CreateListFromTable<Webx_Industry_Product_Mapping>(dataTable);
            return LeaveList;
        }

        public List<Webx_Industry_Product_Mapping> GetIndustryProductCustomer(int Type, string INDProductID)
        {
            string QueryString = "";
            if (Type == 1)
            {
                QueryString = "EXEC usp_Get_IndustryWise_Product_Mapping'" + INDProductID + "'";
            }
            else
            {
                QueryString = "exec usp_Get_ProductWise_Industry_Mapping'" + INDProductID + "'";
            }
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Industry_Product_Mapping> LeaveList = DataRowToObject.CreateListFromTable<Webx_Industry_Product_Mapping>(dataTable);
            return LeaveList;
        }
        public List<Webx_Industry_Product_Mapping> GetIndustryProductSave(int Type, int industry_code, string product_code, string EntryBy)
        {
            string QueryString = "";
            if (Type == 1)
            {
                QueryString = "exec usp_Map_Industry_To_Product'" + industry_code + "','" + product_code + "','" + EntryBy + "'";
            }
            else
            {
                QueryString = "exec usp_Map_Product_To_Industry'" + industry_code + "','" + product_code + "','" + EntryBy + "'";
            }
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Industry_Product_Mapping> LeaveList = DataRowToObject.CreateListFromTable<Webx_Industry_Product_Mapping>(dataTable);
            return LeaveList;
        }
        #endregion

        #region Role master
        public List<Webx_Master_Role> GetRoleDetails()
        {

            string SQRY = "select srno,Roleid,HYC=(select top 1 codedesc from webx_master_general where codetype='HRCHY' and codeid=Roleid),ROledesc,Multiple,ACTIVEFLAG=isnull(ACTIVEFLAG,'N') from Webx_Master_Role ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_Role> Roledata = DataRowToObject.CreateListFromTable<Webx_Master_Role>(Dt);

            return Roledata;
        }

        public DataTable AddEditRoleMaster(string XML, int srno)
        {
            string QueryString;
            if (srno > 0)
            {
                QueryString = "exec [webx_Role_Update] '" + XML + "'";

            }
            else
            {
                QueryString = "exec [webx_Role_insert_Update] '" + XML + "'";

            }
            DataTable DT = GF.GetDataTableFromSP(QueryString);

            return DT;
        }
        #endregion

        #region Role - Employee Intigration Master

        public List<Webx_Role_Employee_Intigration> RoleEmployee()
        {
            string QueryString = "Exec USP_GetRoleEmployeeDetails";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Role_Employee_Intigration> PEIList = DataRowToObject.CreateListFromTable<Webx_Role_Employee_Intigration>(dataTable);
            return PEIList;
        }
        public List<Webx_Role_Employee_Intigration> RoleEmployeeLocation()
        {
            string QueryString = "exec USP_GetRoleEmployeeLocations";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Role_Employee_Intigration> PELIList = DataRowToObject.CreateListFromTable<Webx_Role_Employee_Intigration>(dataTable);
            return PELIList;
        }
        public List<Webx_Role_Employee_Intigration> GetRoleEmployeeUser(string UserId, string LOC, int SetRole)
        {
            string QueryString = "exec USP_GetRoleEmployeeLocationDetails'" + UserId + "','" + LOC + "','" + SetRole + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Role_Employee_Intigration> PEUIList = DataRowToObject.CreateListFromTable<Webx_Role_Employee_Intigration>(dataTable);
            return PEUIList;

        }

        #endregion


        public List<Webx_Master_General> GetGeneralMasterFromId(string CodeType, string CodeId)
        {
            string QueryString = "exec Usp_getGeneralMaster_fromCodetypeandid '" + CodeType + "','" + CodeId + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList;
        }
      
        #region miscellaneous


        //-------------- Days wise holiday Add/Edit ------------
        public DataTable GetHolidayWeekDayDetails()
        {
            string SQRY = "exec Get_HolidayWeekDayDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        //-------------------------------------------------



        //-------------- Date wise holiday Add/Edit ------------
        public List<WebX_Master_Holiday_Datewise> GetDateWiseLocationDetails()
        {
            string SQRY = "exec Get_DateWiseLocationDetails";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Holiday_Datewise> List = DataRowToObject.CreateListFromTable<WebX_Master_Holiday_Datewise>(Dt);
            return List;
        }
        //-------------------------------------------------


        //-------------- Date wise holiday list ------------
        public List<WebX_Master_Holiday_Datewise> GetHolidayDateWise()
        {
            string SQRY = "exec Get_HolidayViewList";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Holiday_Datewise> List = DataRowToObject.CreateListFromTable<WebX_Master_Holiday_Datewise>(Dt);
            return List;
        }
        //-------------------------------------------------



        //-------------- Employee News  -------------------
        public List<webx_News_Updates> DeleteNews()
        {
            string QueryString = "DELETE FROM webx_News_Updates WHERE ID='N'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_News_Updates> itmList = DataRowToObject.CreateListFromTable<webx_News_Updates>(DT);
            return itmList;
        }
        public DataTable AddEmpNewsDetails(string XML)
        {
            string Sql = "EXEC Insert_webx_News_details'" + XML + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }
   
        //----------------------------------------------------


        //-------------- Customer News  -------------------
        public List<webx_Customer_News_updates> DeleteCustNews()
        {
            string QueryString = "DELETE FROM webx_Customer_News_updates WHERE ID='N'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_Customer_News_updates> itmList = DataRowToObject.CreateListFromTable<webx_Customer_News_updates>(DT);
            return itmList;
        }
        public DataTable AddCustNewsDetails(string XML)
        {
            string Sql = "EXEC Insert_webx_Cust_News_details'" + XML + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        //----------------------------------------------------


        //-------------- Employee Updates  -------------------
        public DataTable AddEmpUpdatesDetails(string Headline, string New, string Updates, string ID, string Color)
        {
            string Sql = "EXEC Insert_webx_News_updates'" + Headline + "','" + New + "','" + Updates + "','" + ID + "','" + Color + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }
        public DataSet GetEmpUpdates()
        {
            string SQRY = "exec Get_NewsUpdates";
            DataSet Dt = GF.GetDataSetFromSP(SQRY);
            return Dt;
        }
        public string InsertLoginDetails(string Userid, string SessionID, string BaseUserName, string BaseLocationCode, string BaseLocationName)
        {
            string SQRY = "exec USP_Insert_Login_Details '" + Userid + "','" + SessionID + "','" + BaseUserName +"','"+ BaseLocationCode +"','"+ BaseLocationName + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Convert.ToString(Dt.Rows.Count);
        }
        public string UpdateLoginDetails(string Userid)
        {
            string SQRY = "exec USP_Update_Login_Details '" + Userid + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Convert.ToString(Dt.Rows.Count);
        }
        //----------------------------------------------------


        //-------------- Customer Updates  -------------------
        public DataTable AddCustomerUpdatesDetails(string Headline, string New, string Updates, string ID, string Color)
        {
            string Sql = "EXEC Insert_webx_Customer_updates'" + Headline + "','" + New + "','" + Updates + "','" + ID + "','" + Color + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }
        public DataSet GetCustomerUpdates()
        {
            string SQRY = "Get_CustomerUpdates";
            DataSet Datatable = GF.GetDataSetFromSP(SQRY);
            return Datatable;
        }
        //----------------------------------------------------

        #endregion

        #region SealNo Validate THC Generation
        public DataTable GetExistOrNotSealNoDetail(string SealNo, string BRCD)
        {
            DataTable DT = new DataTable();
            string SQRY_DATA = "EXEC USP_IsExist_SealNo '" + SealNo + "','" + BRCD + "'";
            DT = GF.GetDataTableFromSP(SQRY_DATA);
            return DT;
        }
        #endregion
    }
}