﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using Fleet.ViewModels;
using CYGNUS.ViewModels;
using System.Xml;
using System.Data.SqlClient;
using CYGNUS.Controllers;
using Microsoft.ApplicationBlocks.Data;
using CYGNUS.Models;
using FleetDataService.ViewModels;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OperationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select OperationService.svc or OperationService.svc.cs at the Solution Explorer and start debugging.
    public class OperationService : IOperationService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        #region Loading Sheet

        public List<LoadingSheetDatatable> DocketListForMFGeneration(string BaseLocationCode, string NextStop, string TransferMode, string DateFrom, string DateTo, string DestinationList, string DocketList, string MFDate, string LoadingBy, string RateType)
        {
            //string QueryString = "exec usp_DocketList_For_MF_Generation_ver3 '" + BaseLocationCode + "','" + NextStop + "','" + TransferMode + "','" + DateFrom + "','" + DateTo + "','" + DestinationList + "','','','','" + DocketList + "','" + MFDate + "','N'";
            //string QueryString = "exec usp_DocketList_For_MF_Generation_ver4 '" + BaseLocationCode + "','" + NextStop + "','" + TransferMode + "','" + DateFrom + "','" + DateTo + "','" + DestinationList + "','','','','" + DocketList + "','" + MFDate + "','N','" + LoadingBy + "','" + RateType + "'";
            string QueryString = "exec usp_DocketList_For_MF_Generation_ver4 '" + BaseLocationCode + "','" + NextStop + "','" + TransferMode + "','" + DateFrom + "','" + DateTo + "','" + DestinationList + "','','','','" + DocketList + "','" + MFDate + "','N'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<LoadingSheetDatatable> DocketListForMFGeneration = DataRowToObject.CreateListFromTable<LoadingSheetDatatable>(DT);
            return DocketListForMFGeneration;
        }

        public List<CYGNUS_Internal_Movement> InternalDocumentListForMFGeneration(string BaseLocationCode, string NextStop, string DateFrom, string DateTo)
        {
            string QueryString = "exec USP_GetInternalDocument_For_MF_Generation '" + BaseLocationCode + "','" + NextStop + "','" + DateFrom + "','" + DateTo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Internal_Movement> InternalDocumentListForMFGeneration = DataRowToObject.CreateListFromTable<CYGNUS_Internal_Movement>(DT);
            return InternalDocumentListForMFGeneration;
        }

        public List<LoadingSheetDatatable> DocketListFromLSForMFGeneration(string TCNO)
        {
            string QueryString = "exec WebxNet_DocketList_LS_For_MF '" + TCNO + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<LoadingSheetDatatable> DocketListForMFGeneration = DataRowToObject.CreateListFromTable<LoadingSheetDatatable>(DT);
            return DocketListForMFGeneration;
        }

        public DataTable MFGeneration(string BaseLocationCode, string LsDate, string xmlTCHDR, string xmlTCTRN, string xmlWTDS, string xmlTCTRNIM, string BaseFinYear, string Type)
        {
            string QueryString = "";
            if (Type == "LS")
            {
                QueryString = "exec usp_LoadingSheet_Generate '" + BaseLocationCode + "','" + LsDate + "','" + xmlTCHDR.Trim() + "','" + xmlTCTRN.Trim() + "','" + xmlWTDS.Trim() + "','" + BaseFinYear + "'";
            }
            else
            {
                QueryString = "exec usp_Manifest_Generate_NewPortal'" + BaseLocationCode + "','" + LsDate + "','" + xmlTCHDR.Trim() + "','" + xmlTCTRN.Trim() + "','" + xmlWTDS.Trim() + "','" + xmlTCTRNIM.Trim() + "','" + BaseFinYear + "'";
            }

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "MFGeneration" + Type, "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable ListLsForMF(string BaseLocationCode)
        {
            string QueryString = "exec WebxNet_List_LS_For_MF '" + BaseLocationCode + "','N'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<Webx_master_LocationConnection> GetLocationConnectionObject(string Location)
        {
            string SQRY = "EXEC USP_GetLocationConnectionMappingList '" + Location + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<Webx_master_LocationConnection> LocationConnectionListMap = DataRowToObject.CreateListFromTable<Webx_master_LocationConnection>(Dt);
            return LocationConnectionListMap;
        }

        #endregion

        #region DRS Update

        public DataTable DRSUpdateList(string BaseLocationCode, string DateFrom, string DateTo, string DRSNoList)
        {
            string QueryString = "exec usp_drs_for_updation '" + BaseLocationCode + "','" + DateFrom + "','" + DateTo + "','" + DRSNoList + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<vw_DRS_Summary> GetUpdateDRSSummaryList(string DRSNo)
        {
            string SQRY = "exec usp_DRS_Summary'" + DRSNo + "'";
            return DataRowToObject.CreateListFromTable<vw_DRS_Summary>(GF.GetDataTableFromSP(SQRY));
        }
        public List<webx_BILLDET> GetUpdateDRSDocketBillList(string DRSNo)
        {
            string SQRY = "exec Usp_GetUpdateDRSDocketBillList'" + DRSNo + "'";
            return DataRowToObject.CreateListFromTable<webx_BILLDET>(GF.GetDataTableFromSP(SQRY));
        }
        public List<vw_DRS_Summary> GetDRSDevelieryList(string DRSNo)
        {
            string SQRY = "exec CYGNUS_DRS_DeliveryList'" + DRSNo + "'";
            return DataRowToObject.CreateListFromTable<vw_DRS_Summary>(GF.GetDataTableFromSP(SQRY));
        }

        public bool Insert_DRS_Update_Single(string empcd, string XML)
        {
            bool Status = false;
            //string SQRY = "exec usp_XML_DRS_Update_Single '" + empcd + "','" + XML + "'";
            string SQRY = "exec usp_XML_DRS_Update_Single_NewPortal '" + empcd + "','" + XML + "'";

            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_DRS_Update_Single", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][1].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        //public List<vw_dockets_for_drs_updation_New> GetUpdateDRSList(string DRSNo)
        //{
        //    string SQRY = "exec usp_dockets_for_drs_updation'" + DRSNo + "'";
        //    return DataRowToObject.CreateListFromTable<vw_dockets_for_drs_updation_New>(GF.GetDataTableFromSP(SQRY));
        //}

        public List<vw_dockets_for_drs_updation_New> GetUpdateDRSList(string DRSNo, string UnloadBy, string Ratetype, string Brcd)
        {
            string SQRY = "exec usp_dockets_for_drs_updation_NewPortal'" + DRSNo + "','" + UnloadBy + "','" + Ratetype + "','" + Brcd + "'";
            return DataRowToObject.CreateListFromTable<vw_dockets_for_drs_updation_New>(GF.GetDataTableFromSP(SQRY));
        }

        public List<Webx_Master_General> GetDeliveryResion()
        {
            string SQRY = "exec usp_Get_DeliveryReasons";
            return DataRowToObject.CreateListFromTable<Webx_Master_General>(GF.GetDataTableFromSP(SQRY));
        }

        public bool InsertUpdateDRS(string empcd, string XML)
        {
            bool Status = false;
            string SQRY = "exec usp_XML_DRS_Update '" + empcd + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertUpdateDRS", "", "");
            //DataSet DS = new DataSet();
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][0].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion

        #region THC ARRIVAL AND STOCK UPDATE


        public DataTable THCArrivalStockUpdateList(string incomingat, string fromdt, string todt, string thcno, string departedfrom, string vehno, string Dockno, string Type, string TransportMode)
        {
            string QueryString = "";
            if (Type == "1")
            {
                //QueryString = "exec usp_Incoming_THC_Ver1 '" + incomingat + "','" + fromdt + "','" + todt + "','" + thcno + "','" + departedfrom + "','" + vehno + "','" + Dockno + "','" + TransportMode + "'";
                //QueryString = "exec usp_Incoming_THC_Ver1 '" + incomingat + "','" + fromdt + "','" + todt + "','" + thcno + "','" + departedfrom + "','" + vehno + "','" + Dockno + "'";
                QueryString = "exec usp_Incoming_THC_For_All_Location '" + incomingat + "','" + fromdt + "','" + todt + "','" + thcno + "','" + departedfrom + "','" + vehno + "','" + Dockno + "'";
            }
            if (Type == "2")
            {
                QueryString = "exec usp_Stock_For_Update '" + incomingat + "','" + thcno + "','" + departedfrom + "','" + Dockno + "','" + vehno + "','" + fromdt + "','" + todt + "'";
            }
            //if (Type == "3")
            //{
            //    QueryString = "exec usp_Incoming_THC_Ver1 '" + incomingat + "','" + fromdt + "','" + todt + "','" + thcno + "','" + departedfrom + "','" + vehno + "','" + Dockno + "'";
            //}
            return GF.GetDataTableFromSP(QueryString);
        }
        //change Lavnit Get_Incoming_THC_details ->Get_Incoming_THC_details_NewPortal
        //public List<vw_Incoming_THC> GetTHCDetailsObject(string THCNO, string Loadby, string chargeType, string Brcd)
        //{
        //    //string QueryString = "exec Get_Incoming_THC_details'" + THCNO + "'";
        //    string QueryString = "exec Get_Incoming_THC_details_NewPortal'" + THCNO + "','" + Loadby + "','" + chargeType + "','" + Brcd + "'";
        //    return DataRowToObject.CreateListFromTable<vw_Incoming_THC>(GF.GetDataTableFromSP(QueryString));
        //}

        public List<vw_Incoming_THC> GetTHCDetailsObject(string THCNO)
        {
            string QueryString = "exec Get_Incoming_THC_details'" + THCNO + "'";
            return DataRowToObject.CreateListFromTable<vw_Incoming_THC>(GF.GetDataTableFromSP(QueryString));
        }

        public List<vw_Stock_For_Update> GetStockUpdateObject(string THCNO, string BRCD)
        {
            string QueryString = "exec usp_StockUpdateList'" + THCNO + "','" + BRCD + "'";
            return DataRowToObject.CreateListFromTable<vw_Stock_For_Update>(GF.GetDataTableFromSP(QueryString));
        }

        //public DataTable AddEditTHCArrivalDetails(string thcno, string brcd, DateTime arrvdt, string arrvtm, string seal, string sealstatus, string inremark, string empcd, string latearrival, string closekm, string Seal_in_Reason)
        //{
        //    string QueryStringAdd = "exec usp_Update_Incoming_THC_NewPortal '" + thcno + "','" + brcd + "','" + arrvdt + "','" + arrvtm + "','" + seal + "','" + sealstatus + "','" + inremark + "','" + empcd + "','" + latearrival + "','" + closekm + "','" + Seal_in_Reason + "'";
        //    int Id = GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditArrivalDetails", "", "");
        //    DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
        //    return dataTableAdd;
        //}
        //public DataTable AddEditTHCArrivalDetails(string thcno, string brcd, DateTime arrvdt, string arrvtm, string seal, string sealstatus, string inremark, string empcd, string latearrival, string closekm, string Seal_in_Reason, decimal Charge, string LoadingBy, string ChargeType, string VendorCode, string VendorName)
        //{
        //    string QueryStringAdd = "exec usp_Update_Incoming_THC_NewPortal '" + thcno + "','" + brcd + "','" + arrvdt + "','" + arrvtm + "','" + seal + "','" + sealstatus + "','" + inremark + "','" + empcd + "','" + latearrival + "','" + closekm + "','" + Seal_in_Reason + "','" + Charge + "','" + LoadingBy + "','" + ChargeType + "','" + VendorCode + "','" + VendorName + "'";
        //    DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
        //    return dataTableAdd;
        //}

        public DataTable AddEditTHCArrivalDetails(string thcno, string brcd, DateTime arrvdt, string arrvtm, string seal, string sealstatus, string inremark, string empcd, string latearrival, string openkm, string closekm, string Seal_in_Reason, decimal Charge, decimal Rate, string LoadingBy, string ChargeType, string VendorCode, string VendorName, bool IsMathadi, string SlipNo, decimal Amt, DateTime date)
        {
            string QueryStringAdd = "exec usp_Update_Incoming_THC_NewPortal '" + thcno + "','" + brcd + "','" + arrvdt + "','" + arrvtm + "','" + seal + "','" + sealstatus + "','" + inremark + "','" + empcd + "','" + latearrival + "','" + openkm + "','" + closekm + "','" + Seal_in_Reason + "','" + Charge + "','" + Rate + "','" + LoadingBy + "','" + ChargeType + "','" + VendorCode + "','" + VendorName + "','" + (IsMathadi == true ? "1" : "0") + "','" + SlipNo + "'," + Amt + ",'" + date + "'";
            int Id = GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditTHCArrivalDetails", "", "");
            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
            return dataTableAdd;
        }

        public DataTable AddEditStockUpdateDetails(string thcno, string brcd, string Xml)
        {
            string QueryStringAdd = "exec usp_XML_Stock_Update_NewPortal '" + thcno + "','" + brcd + "','" + Xml + "'";
            int Id = GF.SaveRequestServices(QueryStringAdd.Replace("'", "''"), "AddEditStockUpdateDetails", "", "");

            DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
            return dataTableAdd;
        }

        public string GetSealNoDetails(string thc)
        {
            string QueryString = "SELECT Sealno_out = ISNULL(Sealno_out,''),Openkm = ISNULL(Openkm,''),CloseKm = ISNULL(CloseKm,'') FROM webx_thc_summary WHERE thcno = '" + thc + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        #endregion

        #region Challan

        public DataTable ChallanEntry(string HDR_DETAILS, string DET_DETAILS, string findetval, string BaseLocationCode, string BaseFinYear, string DOCTYP)
        {
            string QueryString = " exec Webxnet_THC_PRS_DRS_InsertDATA_NewPortal '" + HDR_DETAILS.Replace("&", "&amp;").Trim() + "','" + DET_DETAILS.Replace("&", "&amp;").Trim() + "','" + findetval + "','" + BaseLocationCode + "','" + BaseFinYear + "','" + DOCTYP + "' ";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "ChallanEntry", "", "");

            //DataTable DT = GF.GetDataTableFromSP(QueryString);

            return GF.GetDataTableFromSP(QueryString);
        }

        public List<VendorType> GetVendorType(string RouteMode)
        {
            string QueryString = "exec [usp_Vendor_Types] '" + RouteMode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VendorType> VendorTypeList = DataRowToObject.CreateListFromTable<VendorType>(DT);
            return VendorTypeList;
        }

        public List<webx_Delivery_Zone> GetDeliveryZone(string Location)
        {
            string QueryString = "SELECT * FROM webx_Delivery_Zone where Location='" + Location + "' order by Zoneid";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_Delivery_Zone> VendorTypeList = DataRowToObject.CreateListFromTable<webx_Delivery_Zone>(DT);
            return VendorTypeList;
        }

        public List<Vendors> GetVendorsFromVendorType(string VendorType, string Location, string Username, string documentType)
        {
            //string QueryString = "EXEC usp_Vendors '" + VendorType + "', '" + Location + "', '" + Username + "', '" + documentType + "'";
            string QueryString = "EXEC usp_Vendors '" + VendorType + "', '" + Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Vendors> VendorList = DataRowToObject.CreateListFromTable<Vendors>(DT);
            return VendorList;
        }

        public List<Vendors> GetVendorsForChallanFromRouteVendType(string RouteMode, string RouteName, string VendType, string Location, string THCType)
        {
            string QueryString = "EXEC USP_GetVendorsForChallanFromRouteVendType '" + VendType + "', '" + Location + "', '" + RouteMode + "', '" + RouteName + "', '" + THCType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Vendors> VendorList = DataRowToObject.CreateListFromTable<Vendors>(DT);
            return VendorList;
        }

        public List<webx_VEHICLE_HDR> GetVehicleFromVendor(string VendorType, string Vendor)
        {
            string QueryString = "exec [usp_Vendor_Vehicles] '" + Vendor + "','" + VendorType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_VEHICLE_HDR> VehicleList = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(DT);
            return VehicleList;
        }

        public List<Routes> GetRouteFromRouteType(string RouteType, string Location, string IsEmpty)
        {
            string QueryString = "exec [usp_RouteNames_For_New_THC_ver2] '" + Location + "','" + RouteType + "','" + IsEmpty + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Routes> RouteList = DataRowToObject.CreateListFromTable<Routes>(DT);
            return RouteList;
        }

        public List<Tripsheet> GetTripsheetFromVehicle(string VehicleNo)
        {
            string QueryString = "exec [usp_TripSheetListForTHC] '" + VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Tripsheet> TripsheetList = DataRowToObject.CreateListFromTable<Tripsheet>(DT);
            return TripsheetList;
        }

        public List<VehicleType> GetVehicleTypeFromVehicle(string VehicleNo)
        {
            string QueryString = "exec [usp_Vehicle_Types] '" + VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VehicleType> itmList = DataRowToObject.CreateListFromTable<VehicleType>(DT);
            return itmList;
        }

        public List<VehicleType> GetVehicleTypesForChallanFromRouteVendType(string VehicleNo, string RouteMode, string RouteName, string vendor_type, string VendorCode, string Location, string THCType)
        {
            string QueryString = "exec [usp_Vehicle_TypesForChallanFromRouteVendType] '" + VehicleNo + "','" + Location + "','" + RouteMode + "','" +
                RouteName + "','" + vendor_type + "','" + VendorCode + "','" + THCType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<VehicleType> itmList = DataRowToObject.CreateListFromTable<VehicleType>(DT);
            return itmList;
        }

        public List<AssignAssetBranchWise> GetBranchWiseTablet(string AsOnDate, string BrachCode, string type, string assetCode, string EmpCode)
        {
            string QueryString = "EXEC USP_Assign_To_Employee_Report '" + AsOnDate + "','" + BrachCode + "','" + type + "','" + assetCode + "','" + EmpCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<AssignAssetBranchWise> itmList = DataRowToObject.CreateListFromTable<AssignAssetBranchWise>(DT);
            return itmList;
        }

        public List<FTLType> GetFTLTypeFromVehicle(string VehicleNo)
        {
            string QueryString = "exec [usp_FTL_Types] '" + VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<FTLType> itmList = DataRowToObject.CreateListFromTable<FTLType>(DT);
            return itmList;
        }

        public List<vw_Vehicles> GetVehicleDetails(string VehicleNo)
        {
            string QueryString = "exec [usp_Vehicles_NewPortal] '" + VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_Vehicles> itmList = DataRowToObject.CreateListFromTable<vw_Vehicles>(DT);
            return itmList;
        }

        public List<vw_Vehicles> GetVehicleStartKM(string VehicleNo)
        {
            string QueryString = "exec [Usp_Get_Start_KM_For_Vehicle] '" + VehicleNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_Vehicles> itmList = DataRowToObject.CreateListFromTable<vw_Vehicles>(DT);
            return itmList;
        }

        public List<Drivers> GetDriverDetailsFromTripsheet(string TripsheetNo)
        {
            string QueryString = "exec [usp_TS_DriverInfo_NewPortal] '" + TripsheetNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Drivers> itmList = DataRowToObject.CreateListFromTable<Drivers>(DT);
            return itmList;
        }

        public List<Webx_Master_General> GetAirport(string Location)
        {
            string QueryString = "select CodeId=Airportcode,CodeDesc=Airportname from webx_Master_Airport a inner join webx_location l on City=loccity where isnull(a.activeflag,'N')='Y'  and loccode='" + Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> itmList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return itmList;
        }

        public List<Webx_Master_General> GetFlights(string AirLine, string Airport)
        {
            string QueryString = "select CodeId=a.Flightcode,CodeDesc=Flightnumber from webx_Master_Flight_Schedule_Hdr  a inner join webx_Master_Flight_Schedule_Det b on a.Flightcode=b.Flightcode where Airlinecode='" + AirLine + "' and isnull(Airportcode,fromAirportcode)='" + Airport + "' and isnull(activeflag,'N')='Y'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> itmList = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return itmList;
        }

        public string GetFlightSchTime(string Flight, string Airport)
        {
            string SchTime = "00:00:00";

            string QueryString = "SELECT TM=convert(varchar,dep_time,108) FROM webx_Master_Flight_Schedule_Det WHERE Flightcode='" + Flight.ToString() + "' and Airportcode='" + Airport.ToString() + "'";
            SchTime = GF.executeScalerQuery(QueryString);

            if (SchTime == null)
                SchTime = "00:00:00";

            return SchTime;
        }

        public List<BookedBy> GetBookcedBy(string BookcedByTYpe, string Location, string Username)
        {
            //string QueryString = "exec [usp_AllotToList_For_PRS] '" + Location + "','" + BookcedByTYpe + "','" + Username + "'";
            string QueryString = "exec [usp_AllotToList_For_PRS] '" + Location + "','" + BookcedByTYpe + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<BookedBy> itmList = DataRowToObject.CreateListFromTable<BookedBy>(DT);
            return itmList;
        }

        public List<PRSDRSGC> GetDocketListfromPRSDRS(GCFilter GC, string Location)
        {
            string Fromdt = "", todt = "", DTTYP = "", paybas = "", TRN = "", BUSTYP = "", STATUS = "", docketList = "", Alloted_To = "", LoadingBy = "", ChrgType = "";

            Fromdt = GC.FromDate.ToString("dd MMM yyy");
            todt = GC.ToDate.ToString("dd MMM yyy");

            DTTYP = GC.DATETYPE;

            if (GC.PAYBAS == null || GC.PAYBAS == "")
                paybas = "All";
            else
                paybas = GC.PAYBAS;

            if (GC.TRNMOD == null || GC.TRNMOD == "")
                TRN = "All";
            else
                TRN = GC.TRNMOD;

            if (GC.BUSTYPE == null || GC.BUSTYPE == "")
                BUSTYP = "All";
            else
                BUSTYP = GC.BUSTYPE;

            if (GC.BookedByType == null || GC.BookedByType == "")
                STATUS = "All";
            else
                STATUS = GC.BookedByType;

            if (GC.BookedBy == null)
                Alloted_To = "";
            else
                Alloted_To = GC.BookedBy;

            if (GC.GCNO == null)
                docketList = "";
            else
                docketList = GC.GCNO;

            if (GC.LoadingBy == null)
                LoadingBy = "";
            else
                LoadingBy = GC.LoadingBy;

            if (GC.ChargeType == null)
                ChrgType = "";
            else
                ChrgType = GC.ChargeType;

            string QueryString = "exec [WEbx_Docketlist_for_PRSDRS_Generation_NewPortal] '" + Fromdt + "','" + todt + "','" + DTTYP + "','" + paybas + "','" + TRN + "','" + BUSTYP + "','" + STATUS + "','" + GC.DOCTYP + "','" + Location + "','" + docketList + "','" + Alloted_To + "','" + LoadingBy + "','" + ChrgType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<PRSDRSGC> itmList = DataRowToObject.CreateListFromTable<PRSDRSGC>(DT);
            return itmList;
        }

        public List<MFDetails> GeMFListFromRoute(string Route, string IsBCProcess)
        {
            string QueryString = "exec [usp_MF_Available_For_New_THC] '" + Route + "','" + IsBCProcess + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<MFDetails> itmList = DataRowToObject.CreateListFromTable<MFDetails>(DT);
            return itmList;
        }

        public List<ChallanCharges> GetChargesDetails(string ChargeType)
        {
            string QueryString = "exec [WebxNet_CLList_Of_FinDet] '" + ChargeType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<ChallanCharges> itmList = DataRowToObject.CreateListFromTable<ChallanCharges>(DT);
            return itmList;
        }

        public List<RouteSchedule> GetRouteScheduleDetails(string RouteCode, string DatDay, string DatTime)
        {
            string QueryString = "exec [WebXNet_GetScheduledRouteForRouteCode] '" + RouteCode + "','" + DatDay + "','" + DatTime + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<RouteSchedule> itmList = DataRowToObject.CreateListFromTable<RouteSchedule>(DT);
            return itmList;
        }

        public DataSet GetContractDetails(string THCTYPE, string VendorCode, string RouteCode, string RouteMODE, string FTL_Type, string Vehicle, string From_City, string To_City)
        {
            string QueryString = "exec [usp_Get_Vendor_Contract_Charge_Detail] '" + VendorCode + "','','','','" + RouteMODE + "','" + RouteCode + "','" + FTL_Type + "','" + Vehicle + "','" + THCTYPE + "','" + From_City + "','" + To_City + "'";
            DataSet DT = GF.GetDataSetFromSP(QueryString);
            return DT;
        }

        public decimal GetStandardContract(string RouteCode)
        {
            string QueryString = "select top 1 STDAMT=isnull(std_contamt,0.00) from webx_rutmas With(NOLOCK) where rutcd= '" + RouteCode + "'";
            string strStandardContractAmount = GF.executeScalerQuery(QueryString);

            if (strStandardContractAmount == null || strStandardContractAmount == "")
                strStandardContractAmount = "0";

            return Convert.ToDecimal(strStandardContractAmount);
        }

        public DataTable Get_ODA_Docket_Charges(string Dockno, int Type)
        {
            string QueryString = " exec USP_Get_DocketNo_ODA_Charges '" + Dockno + "','" + Type + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region  Docket

        public List<webx_location> GetDestinationLocations(string Prefix)
        {
            string QueryString = "exec [USP_Get_GCDestination] '" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_location> itmList = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return itmList;
        }

        public List<webx_location> GetDestinationLocations_With_HQTR(string Prefix)
        {
            string QueryString = "exec [USP_Get_GCDestination_With_HQTR] '" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_location> itmList = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return itmList;
        }

        public Step1Model GetStep1Details(string Location)
        {
            string QueryString = "exec [USP_GET_DOCKET_STEP1_DETAILS_MVC] '" + Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Step1Model> itmList = DataRowToObject.CreateListFromTable<Step1Model>(DT);
            return itmList.FirstOrDefault();
        }

        public Step2Model GetStep2Details(string strXML)
        {
            string QueryString = "exec [USP_GET_DOCKET_STEP2_DETAILS_MVC] '" + strXML + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Step2Model> itmList = DataRowToObject.CreateListFromTable<Step2Model>(DT);
            return itmList.FirstOrDefault();
        }

        public string GetServerDate()
        {
            string sqlstr = "SELECT TOP 1 date=CONVERT(VARCHAR,GETDATE(),103)";
            return GF.executeScalerQuery(sqlstr);
        }

        public List<QuickDocketBalancecharges> GetDocketChargesDetails(string Dockno)
        {
            string QueryString = "exec [USP_GetDKT_Charge_Unpivot] '" + Dockno + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<QuickDocketBalancecharges> itmList = DataRowToObject.CreateListFromTable<QuickDocketBalancecharges>(DT);
            return itmList;
        }

        public List<webx_custcontract_servicecharges> GetContractServiceChargessDetails(string ContractId, string TransType)
        {
            string QueryString = "SELECT ISNULL(gstpaidby,'T') as gstpaidby,* FROM webx_custcontract_servicecharges Where  contractId='" + ContractId + "' and  trans_type='" + TransType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_custcontract_servicecharges> itmList = DataRowToObject.CreateListFromTable<webx_custcontract_servicecharges>(DT);
            return itmList;
        }

        public ContractKeys ExecuteContractLocationFreight(ContractKeys ckeys)
        {
            string strXMLCriteria = "";
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(ckeys.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            x.Serialize(stream, ckeys);
            stream.Position = 0;
            XmlDocument xd = new XmlDocument();
            xd.Load(stream);
            strXMLCriteria = xd.InnerXml;

            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[2];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@strXMLCriteria", strXMLCriteria, SqlDbType.Xml);
                paramsToStore[1] = ControllersHelper.GetSqlParameter("@strXMLOutPut", "<root></root>", SqlDbType.Xml, ParameterDirection.Output);
                SqlHelper.ExecuteNonQuery(GF.GetConnstr(), CommandType.StoredProcedure, "USP_GET_CONTRACT_FREIGHT", paramsToStore);

                string strOUTPUT = paramsToStore[1].ToString();

                string QueryString = " exec USP_GET_CONTRACT_FREIGHT '" + strXMLCriteria + "','<root></root>'";
                //string strOUTPUT = GF.executeScalerQuery(QueryString);

                xd = new XmlDocument();
                //  xd.LoadXml(strOUTPUT);
                xd.LoadXml(paramsToStore[1].Value.ToString());

                ckeys.FreightCharge = SYSConvert.ToDouble(((XmlNodeList)xd.GetElementsByTagName("FreightCharge")).Item(0).InnerText);
                ckeys.FreightRate = SYSConvert.ToDouble(((XmlNodeList)xd.GetElementsByTagName("FreightRate")).Item(0).InnerText);
                ckeys.RateType = ((XmlNodeList)xd.GetElementsByTagName("RateType")).Item(0).InnerText.ToString();
                ckeys.TRDays = SYSConvert.ToInt32(((XmlNodeList)xd.GetElementsByTagName("TRDays")).Item(0).InnerText);
                ckeys.InvoiceRateApplay = ((XmlNodeList)xd.GetElementsByTagName("InvoiceRateApplay")).Item(0).InnerText;
                ckeys.InvoiceRate = SYSConvert.ToDouble(((XmlNodeList)xd.GetElementsByTagName("InvoiceRate")).Item(0).InnerText);

                if (Convert.ToString(ckeys.RateType).CompareTo("F") == 0)
                    ckeys.FlagProrata = "Y";
                else
                    ckeys.FlagProrata = "N";
            }
            catch (Exception excp)
            {
                ckeys.FlagProceed = "EXC_CNT_LOC_FRT";
                ckeys.Description = "Execption in ExecuteContractLocationFreight&" + excp.Message.Replace('\n', '_');
                return ckeys;
            }

            ckeys = ExecuteContractMinimumFreight(ckeys);

            if (ckeys.FlagProceed.CompareTo("NT") == 0)
            {
                if (ckeys.FreightCharge == 0 || ckeys.FreightRate == 0)
                {
                    ckeys.FlagProceed = "NIL_CNT_LOC_FRT";
                    ckeys.Description = "Contract Freight Charge Location-Location is not Found. Can't Enter ";
                    return ckeys;
                }
            }

            return ckeys;
        }

        public ContractKeys ExecuteContractMinimumFreight(ContractKeys ckeys)
        {
            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[2];

                paramsToStore[0] = new SqlParameter("@contractid", SqlDbType.VarChar);
                paramsToStore[0].Value = ckeys.ContractID;
                paramsToStore[0].Size = 30;
                paramsToStore[1] = new SqlParameter("@transmode", SqlDbType.VarChar);
                paramsToStore[1].Value = ckeys.TransMode;
                paramsToStore[1].Size = 10;

                SqlDataReader dr = SqlHelper.ExecuteReader(GF.GetConnstr(), CommandType.StoredProcedure, "USP_GET_MINIMUM_FREIGHT", paramsToStore);

                while (dr.Read())
                {

                    try
                    {
                        ckeys.MinFreightType = Convert.ToString(dr["minfreighttype"]);
                    }
                    catch (Exception) { ckeys.MinFreightType = "B"; }
                    ckeys.MinFreightBase = Convert.ToString(dr["minfreightbas"]);

                    try
                    {
                        ckeys.MinFreightBaseRate = Convert.ToDouble(dr["minfreightbasrate"]);
                    }
                    catch (Exception) { ckeys.MinFreightBaseRate = 0; }

                    try
                    {
                        ckeys.MinFreightRate = Convert.ToDouble(dr["minfreightrate"]);
                    }
                    catch (Exception) { }
                    try
                    {
                        ckeys.FreightRateLowerLimit = Convert.ToDouble(dr["freightratelowerlimit"]);
                    }
                    catch (Exception) { }
                    try
                    {
                        ckeys.FreightRateUpperLimit = Convert.ToDouble(dr["freightrateupperlimit"]);
                    }
                    catch (Exception) { }
                    try
                    {
                        ckeys.MinSubTotal = Convert.ToDouble(dr["minsubtotal"]);
                    }
                    catch (Exception) { }
                    try
                    {
                        ckeys.SubTotalLowerLimit = Convert.ToDouble(dr["subtotallowerlimit"]);
                    }
                    catch (Exception) { }
                    try
                    {
                        ckeys.SubTotalUpperLimit = Convert.ToDouble(dr["subtotalupperlimit"]);
                    }
                    catch (Exception) { }
                    try
                    {

                        ckeys.FlagFreight = Convert.ToString(dr["flag_freight"]);
                    }
                    catch (Exception) { ckeys.FlagFreight = "N"; }
                    try
                    {
                        ckeys.FlagSutotal = Convert.ToString(dr["flag_subtotal"]);
                    }
                    catch (Exception) { ckeys.FlagSutotal = "N"; }
                }
                dr.Close();

            }
            catch (Exception excp)
            {
                ckeys.Status = "EXC_MIN_FRT";
                ckeys.Description = "Exeption in ExecuteContractMinimumFreight" + excp.Message.Replace('\n', '_');
            }

            return ckeys;
        }

        public FOVCharge GetFOVCharge(FOVCharge fov)
        {
            string sqlstr = "SELECT activeflag FROM webx_master_charge WHERE chargecode='SCHG11'";
            sqlstr = sqlstr + " AND basedon='" + fov.ChargeRule + "'";
            sqlstr = sqlstr + " AND basecode='" + fov.BaseCode + "'";
            sqlstr = sqlstr + " AND activeflag='Y' AND chargetype='DKT'";
            fov.FlagFOV = GF.executeScalerQuery(sqlstr);

            sqlstr = "SELECT fovrate,ratetype,fovcharge=(CASE  ratetype WHEN '%' THEN fovrate*" + fov.DeclareValue + "/100 ";
            sqlstr = sqlstr + " WHEN 'F' THEN fovrate ELSE 0 END) FROM webx_custcontract_fovmatrix";
            sqlstr = sqlstr + " WHERE contractid='" + fov.ContractID + "' AND risktype='" + fov.RiskType + "'";
            sqlstr = sqlstr + " AND " + fov.DeclareValue + " BETWEEN slabfrom AND slabto";
            DataTable dt = GF.GetDataTableFromSP(sqlstr);
            if (dt.Rows.Count > 0)
            {
                fov.FOVRate = SYSConvert.ToDouble(Convert.ToString(dt.Rows[0]["fovrate"]));
                fov.FOVCharged = SYSConvert.ToDouble(Convert.ToString(dt.Rows[0]["fovcharge"]));
                fov.FOVRateType = Convert.ToString(dt.Rows[0]["ratetype"]);
            }
            else
            {
                fov.FOVRate = 0;
                fov.FOVCharged = 0;
                fov.FOVRateType = "F";
            }
            return fov;
        }

        public TaxRate GetTaxObject(TaxRate trate)
        {
            string sqlstr = "SELECT TOP 1 ISNULL(svctax_rate,0) AS servicetaxrate,";
            sqlstr = sqlstr + " ISNULL(edu_cess_rate,0) AS educessrate,";
            sqlstr = sqlstr + " ISNULL(hedu_cess_rate,0) AS heducessrate,SbcRate,KKCRate=KKCessRate FROM webx_chargemst ";
            sqlstr = sqlstr + " WHERE '" + trate.Date.ToString("dd MMM yyyy") + "' BETWEEN startdate AND enddate";
            DataTable dt;
            try
            {
                dt = GF.GetDataTableFromSP(sqlstr);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                trate.ServiceTaxRate = Convert.ToDouble(dt.Rows[0]["servicetaxrate"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                trate.EduCessRate = Convert.ToDouble(dt.Rows[0]["educessrate"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            try
            {
                trate.HEduCessRate = Convert.ToDouble(dt.Rows[0]["heducessrate"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /* SB Cess */
            try
            {
                trate.SbcRate = Convert.ToDouble(dt.Rows[0]["SbcRate"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /* SB Cess */

            /* KKC */
            try
            {
                trate.KKCRate = Convert.ToDouble(dt.Rows[0]["KKCRate"]);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            /* SB Cess */
            return trate;
        }

        public DocketServiceTax GetServiceTax(DocketServiceTax dst)
        {
            TaxRate trate = new TaxRate();
            trate.Date = dst.Date;
            trate = GetTaxObject(trate);

            dst.StdSTaxRate = trate.ServiceTaxRate;
            dst.StdEduCessRate = trate.EduCessRate;
            dst.StdHEduCessRate = trate.HEduCessRate;

            dst.STaxRate = trate.ServiceTaxRate;
            dst.EduCessRate = trate.EduCessRate;
            dst.HEduCessRate = trate.HEduCessRate;

            /* SB Cess*/
            dst.SbcRate = trate.SbcRate;
            dst.KKCRate = trate.KKCRate;
            /* SB Cess*/

            double rebateper = 0;
            string sqlstr = "SELECT stax_rebate_per,exceed_amt FROM webx_taxrebate ";
            sqlstr = sqlstr + " WHERE service_class='" + dst.ServiceType + "'";
            sqlstr = sqlstr + " AND trans_type='" + dst.TransMode + "'";
            sqlstr = sqlstr + " AND ('" + dst.Date.ToString("dd MMM yyyy") + "' BETWEEN startdate AND enddate)";
            DataTable dtstax = GF.GetDataTableFromSP(sqlstr);

            if (dtstax.Rows.Count > 0)
            {
                dst.ExceedAmount = SYSConvert.ToDouble(dtstax.Rows[0]["exceed_amt"].ToString());
                rebateper = SYSConvert.ToDouble(dtstax.Rows[0]["stax_rebate_per"].ToString());
            }


            dst.STaxRate = dst.STaxRate - ((dst.STaxRate * rebateper) / 100);

            /* SB Cess*/
            dst.SbcRate = dst.SbcRate - ((dst.SbcRate * rebateper) / 100);
            dst.KKCRate = dst.KKCRate - ((dst.KKCRate * rebateper) / 100);
            /* SB Cess*/

            if (dst.SubTotal > dst.ExceedAmount && dst.StaxPayer.CompareTo("T") == 0)
            {
                dst.ServiceTax = dst.SubTotal * dst.STaxRate / 100;

                /* SB Cess*/
                dst.SBCess = dst.SubTotal * dst.SbcRate / 100;

                dst.KKCAmount = dst.SubTotal * dst.KKCRate / 100;
                /* SB Cess*/

                dst.EduCess = dst.ServiceTax * dst.EduCessRate / 100;
                dst.HEduCess = dst.ServiceTax * dst.HEduCessRate / 100;
            }
            else
            {
                dst.ServiceTax = 0;

                /* SB Cess*/
                dst.SBCess = 0;
                dst.KKCAmount = 0;
                /* SB Cess*/

                dst.EduCess = 0;
                dst.HEduCess = 0;
            }

            return dst;
        }
        public DataTable MasterDocketEntry(string strXMLMasterEntry, string strXMLChargesEntry, string strXMLDocumentEntry, string strXMLInvoiceEntry, string strXMLBCSerialEntry, string FinYear4d, SqlTransaction trn)
        {

            DataTable DTOutPut = new DataTable();
            string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + FinYear4d + "','<root></root>'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry", "", "");

            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[7];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@strXMLdocketMasterEntry", strXMLMasterEntry, SqlDbType.Xml);
                paramsToStore[1] = ControllersHelper.GetSqlParameter("@strXMLdocketChargesEntry", strXMLChargesEntry, SqlDbType.Xml);
                paramsToStore[2] = ControllersHelper.GetSqlParameter("@strXMLdocketDocumentEntry", strXMLDocumentEntry, SqlDbType.Xml);
                paramsToStore[3] = ControllersHelper.GetSqlParameter("@strXMLdocketInvoiceEntry", strXMLInvoiceEntry, SqlDbType.Xml);
                paramsToStore[4] = ControllersHelper.GetSqlParameter("@strXMLBCSerialEntry", strXMLBCSerialEntry, SqlDbType.Xml);
                paramsToStore[5] = ControllersHelper.GetSqlParameter("@finyear4d", FinYear4d, SqlDbType.NVarChar);
                paramsToStore[6] = ControllersHelper.GetSqlParameter("@strXMLOutPut", "<root></root>", SqlDbType.Xml);
                try
                {
                    DTOutPut = SqlHelper.ExecuteDataset(trn, CommandType.StoredProcedure, "USP_DOCKET_ENTRY_NewPortal", paramsToStore).Tables[0];

                }
                catch (Exception excp)
                {
                    throw excp;
                }
            }
            catch (Exception excp)
            {
                throw excp;
            }
            return DTOutPut;
        }

        //public XmlDocument MasterDocketEntry(string strXMLMasterEntry, string strXMLChargesEntry, string strXMLDocumentEntry, string strXMLInvoiceEntry, string strXMLBCSerialEntry, string FinYear4d, SqlTransaction trn)
        //{
        //    XmlDocument xdOutPut = new XmlDocument();
        //    string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + FinYear4d + "','<root></root>'";
        //    int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry", "", "");

        //    try
        //    {
        //        SqlParameter[] paramsToStore = new SqlParameter[7];
        //        paramsToStore[0] = ControllersHelper.GetSqlParameter("@strXMLdocketMasterEntry", strXMLMasterEntry, SqlDbType.Xml);
        //        paramsToStore[1] = ControllersHelper.GetSqlParameter("@strXMLdocketChargesEntry", strXMLChargesEntry, SqlDbType.Xml);
        //        paramsToStore[2] = ControllersHelper.GetSqlParameter("@strXMLdocketDocumentEntry", strXMLDocumentEntry, SqlDbType.Xml);
        //        paramsToStore[3] = ControllersHelper.GetSqlParameter("@strXMLdocketInvoiceEntry", strXMLInvoiceEntry, SqlDbType.Xml);
        //        paramsToStore[4] = ControllersHelper.GetSqlParameter("@strXMLBCSerialEntry", strXMLBCSerialEntry, SqlDbType.Xml);
        //        paramsToStore[5] = ControllersHelper.GetSqlParameter("@finyear4d", FinYear4d, SqlDbType.NVarChar);
        //        paramsToStore[6] = ControllersHelper.GetSqlParameter("@strXMLOutPut", "<root></root>", SqlDbType.Xml, ParameterDirection.Output);

        //        try
        //        {
        //            SqlHelper.ExecuteDataset(trn, CommandType.StoredProcedure, "USP_DOCKET_ENTRY_NewPortal", paramsToStore);
        //            xdOutPut.LoadXml(paramsToStore[6].Value.ToString());
        //        }
        //        catch (Exception excp)
        //        {
        //            throw excp;
        //        }
        //    }
        //    catch (Exception excp)
        //    {
        //        throw excp;
        //    }
        //    return xdOutPut;
        //}

        public string GetBACode(string dockno)
        {
            //SELECT TOP 1 assignto=Alloted_To FROM dbo.vw_DCR_Register
            //WHERE '200004801' BETWEEN DOC_SR_FROM AND DOC_SR_TO
            //AND LEN(RTRIM(LTRIM('200004801'))) = LEN(DOC_SR_FROM)

            string sqlstr = "SELECT TOP 1 assignto=Alloted_To FROM dbo.vw_DCR_Register";
            sqlstr = sqlstr + " WHERE '" + dockno + "' BETWEEN DOC_SR_FROM AND DOC_SR_TO";
            sqlstr = sqlstr + " AND LEN(RTRIM(LTRIM('" + dockno + "'))) = LEN(DOC_SR_FROM)";
            DataTable dtstax = GF.GetDataTableFromSP(sqlstr);

            return dtstax.Rows[0]["assignto"].ToString();
        }

        public double GetFTLMaxCapacity(string ftltype)
        {
            double maxcapacity = 0.0;
            string sqlstr = "SELECT ISNULL(maxcapacity,0) AS maxcapacity FROM webx_master_ftlcapacity WHERE ftltype='" + ftltype + "'";
            maxcapacity = Convert.ToDouble(GF.executeScalerQuery(sqlstr));

            return maxcapacity;
        }

        public double GetProRataCharge(double freightcharge, double chargedweight, double maxcapacity)
        {
            double diff = 0.00, proratacharge = 0.0;
            diff = chargedweight - maxcapacity;
            if (diff > 0)
            {
                try
                {
                    if (maxcapacity > 0)
                        proratacharge = diff * freightcharge / maxcapacity;
                }
                catch (Exception) { }
            }

            return proratacharge;
        }

        public int GetContractCutoffDays(string contractid, string loccode, int hours, int minutes, string transmode)
        {
            string strsql = "SELECT cutoff_trdays FROM webx_custcontract_servicecharges ";
            strsql = strsql + " WHERE contractid='" + contractid + "' AND trans_type='" + transmode + "'";
            strsql = strsql + " AND (CASE WHEN " + hours.ToString("F0") + " > cutoff_hrs THEN 'Y' ";
            strsql = strsql + " WHEN " + hours.ToString("F0") + " < cutoff_hrs THEN 'N'  ";
            strsql = strsql + " ELSE (CASE WHEN " + minutes.ToString("F2");
            strsql = strsql + " >= cutoff_min THEN 'Y' ELSE 'N' END) END) ='Y'";
            int trdays = 0;
            try
            {
                trdays = SYSConvert.ToInt16(GF.executeScalerQuery(strsql));
            }
            catch (Exception)
            { }

            return trdays;

        }

        public int GetLocationCutoffDays(string loccode, int hours, int minutes, string transmode)
        {
            string strsql = "";
            strsql = "SELECT cutoff_yn FROM webx_location WHERE loccode='" + loccode + "'";
            int trdays = 0;
            if (SYSConvert.ToBoolean(GF.executeScalerQuery(strsql)))
            {
                strsql = "SELECT trdays FROM webx_master_cutofftime ";
                strsql = strsql + " WHERE loccode='" + loccode + "' AND trans_type='" + transmode + "'";
                strsql = strsql + " AND (CASE WHEN " + hours.ToString("F0") + " > hours THEN 'Y' ";
                strsql = strsql + " WHEN " + hours.ToString("F0") + " < hours THEN 'N'  ";
                strsql = strsql + " ELSE (CASE WHEN " + minutes.ToString("F2");
                strsql = strsql + " >= minutes THEN 'Y' ELSE 'N' END) END) ='Y'";

                try
                {
                    trdays = SYSConvert.ToInt16(GF.executeScalerQuery(strsql));
                }
                catch (Exception)
                { }
            }
            return trdays;

        }

        public int GetODADays(string contractid, string cityname, double chargedweight, Int64 pkgsno)
        {
            SqlParameter[] paramsToStore = new SqlParameter[6];
            paramsToStore[0] = ControllersHelper.GetSqlParameter("@contractid", contractid, SqlDbType.VarChar);
            paramsToStore[1] = ControllersHelper.GetSqlParameter("@cityname", cityname, SqlDbType.VarChar);
            paramsToStore[2] = ControllersHelper.GetSqlParameter("@chargedweight", chargedweight, SqlDbType.VarChar);
            paramsToStore[3] = ControllersHelper.GetSqlParameter("@noofpkgs", pkgsno, SqlDbType.VarChar);
            paramsToStore[4] = ControllersHelper.GetSqlParameter("@odacharge", 0, SqlDbType.Int, ParameterDirection.Output);
            paramsToStore[5] = ControllersHelper.GetSqlParameter("@trdays", 0, SqlDbType.Int, ParameterDirection.Output);
            SqlHelper.ExecuteNonQuery(GF.GetConnstr(), CommandType.StoredProcedure, "USP_GET_ODA_CHARGE", paramsToStore);
            int trdays = SYSConvert.ToInt32(paramsToStore[5].Value);
            return trdays;
        }

        public bool IsDateHoliday(DateTime dt, string loccode)
        {
            string strsql = "SELECT COUNT(*) FROM dbo.webx_master_holiday_datewise ";
            strsql = strsql + " WHERE MONTH(hday_date)=" + dt.Month.ToString() + " AND DAY(hday_date)=" + dt.Day.ToString() + " AND YEAR(hday_date)=" + dt.Year.ToString();
            strsql = strsql + " AND (hday_loc='ALL' OR PATINDEX('%" + loccode + "%',hday_loc)>0)";
            int cnt = SYSConvert.ToInt16(GF.executeScalerQuery(strsql));

            if (cnt > 0)
                return true;
            else
            {
                strsql = "SELECT COUNT(*) FROM dbo.webx_master_holiday_daywise ";
                strsql = strsql + " WHERE day_name='" + dt.DayOfWeek.ToString().ToUpper() + "'";
                strsql = strsql + " AND (hday_loc='ALL' OR PATINDEX('%" + loccode + "%',hday_loc)>0)";
                cnt = SYSConvert.ToInt16(GF.executeScalerQuery(strsql));

                if (cnt > 0)
                    return true;
            }

            return false;
        }

        public DateTime AddHolidaysToEDD(DateTime dt, string loccode, bool flag_rec)
        {
            if (IsDateHoliday(dt, loccode))
            {
                dt = dt.AddDays(1);
                flag_rec = true;
            }
            else
                flag_rec = false;

            if (flag_rec)
                dt = AddHolidaysToEDD(dt, loccode, true);

            return dt;
        }

        public DataTable GetStandardCharges(string strXMLCharges)
        {
            SqlParameter[] paramsToStore = new SqlParameter[1];
            paramsToStore[0] = ControllersHelper.GetSqlParameter("@strXMLCharges", strXMLCharges);

            DataTable dtcharges = SqlHelper.ExecuteDataset(GF.GetConnstr(), CommandType.StoredProcedure, "USP_GET_STD_CHARGES", paramsToStore).Tables[0];

            //if (dtcharges.Rows.Count > 0 && dt.Rows.Count > 0)
            //{
            //    foreach (DataRow dtr in dt.Rows)
            //    {
            //        foreach (DataRow dtrc in dtcharges.Rows)
            //        {
            //            if (dtrc[0].ToString().CompareTo(dtr["chargecode1"].ToString()) == 0)
            //            {
            //                dtr["chargevalue1"] = dtrc[1].ToString();
            //            }

            //            if (dtrc[0].ToString().CompareTo(dtr["chargecode2"].ToString()) == 0)
            //            {
            //                dtr["chargevalue2"] = dtrc[1].ToString();
            //            }
            //        }
            //    }
            //}
            return dtcharges;
        }

        //public Webx_Master_General CheckDocketNo(string dockno, string loccode)
        public Webx_Master_General CheckDocketNo(string dockno, string loccode, string Username)
        {
            //string SQR = "exec USP_CheckDocketNo '" + dockno + "','" + loccode + "'";
            string SQR = "exec USP_CheckDocketNo_NewPortal '" + dockno + "','" + loccode + "','" + Username + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<Webx_Master_General> GetVenderListSearch = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt);
            return GetVenderListSearch.FirstOrDefault();
        }

        public bool IsAvailableDocket(string dockno, string loccode)
        {
            int cnt = 0;
            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[2];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@DocketNo", dockno, SqlDbType.VarChar);
                paramsToStore[1] = ControllersHelper.GetSqlParameter("@LocCode", loccode, SqlDbType.VarChar);
                cnt = SYSConvert.ToInt16(SqlHelper.ExecuteScalar(GF.GetConnstr(), CommandType.StoredProcedure, "USP_VALIDATE_DOCKET_ENTRY", paramsToStore));
            }
            catch (Exception excp)
            {
                throw excp;
            }

            if (cnt == 0)
                return false;
            else
                return true;
        }

        public bool IsSerialDocket(string dockno)
        {
            bool flag = false;
            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[1];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@dockno", dockno, SqlDbType.VarChar);
                flag = SYSConvert.ToBoolean(Convert.ToString(SqlHelper.ExecuteScalar(GF.GetConnstr(), CommandType.StoredProcedure, "SP_FORCED_SERIAL", paramsToStore)));
            }
            catch (Exception excp)
            {
                throw excp;
            }

            return flag;
        }

        public bool IsVoidDocument(string dockno, string doctype)
        {
            int cnt = 0;
            try
            {
                string sql = "SELECT COUNT(docno) FROM webx_document_void WHERE doc_type='" + doctype + "' ";
                sql = sql + "AND docno='" + dockno + "' AND activeflag='Y'";
                cnt = SYSConvert.ToInt16(Convert.ToString(SqlHelper.ExecuteScalar(GF.GetConnstr(), CommandType.Text, sql)));
            }
            catch (Exception excp)
            {
                throw excp;
            }

            if (cnt == 0)
                return false;
            else
                return true;
        }

        public DataTable Get_Pincode_Matrix_CHarges(string PinCode)
        {
            string SQRY = "exec USP_Pincode_Checked_ODA '" + PinCode + "'";
            //int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Customergenerate_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Get_Pincode_Matrix_Rate(decimal KM_From_Location, decimal CHRGWT)
        {
            string SQRY = "exec Usp_GetODAMatrixCharge '" + KM_From_Location + "','" + CHRGWT + "'";
            //int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Customergenerate_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #region Customer Details

        public DataTable Customer_Details_Insert(string CUSTNM, string GRPCD, string CustAddress, string CUSTLOC, string MOBILENO, string GSTNO, string CUSTCAT, string UserName, string State, string PANNo, string AADHARNO)
        {
            string SQRY = "exec USP_InsertCustomerDetails '" + CUSTNM + "','" + GRPCD + "','" + CustAddress + "','" + CUSTLOC + "','" + MOBILENO + "','" + GSTNO + "','" + CUSTCAT + "','" + UserName + "','" + State + "','" + PANNo + "','" + AADHARNO + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Customer_Details", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #endregion

        #region Connection THC

        public DataTable ListOFTHCDepartures(string BaseLocationCode, string THCNo, string MFNo, string DockNo, string FromDate, string ToDate)
        {
            string QueryString = "exec usp_THC_For_Departure '" + BaseLocationCode + "','" + THCNo + "','" + MFNo + "','" + DockNo + "','" + FromDate + "','" + ToDate + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            //List<webx_THC_SUMMARY> DocketListForMFGeneration = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(DT);
            return DT;
        }

        public DataTable Get_THC_For_Departure_Summary(string THCNO, string baseloction)
        {
            string SQRY = "exec usp_THC_For_Departure_Summary '" + THCNO + "','" + baseloction + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable Get_MF_For_OutgoingTHC(string THCNO)
        {
            string SQRY = "exec usp_MF_For_OutgoingTHC_NewPortal  '" + THCNO + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Usp_do_THC_Departure(string thcno, DateTime deptdt, string depttm, string sealno, string outremark, string empcd, string brcd, string xmlTC)
        {
            string Sql = "EXEC [usp_do_THC_Departure_NewPortal] '" + thcno + "','" + deptdt + "','" + depttm + "','" + sealno + "','" + outremark + "','" + empcd + "','" + brcd + "','" + xmlTC + "'";
            int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "THC_Departure", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }


        #endregion

        #region Single CNote Update in DRS

        public DataTable get_ListOFSingleCNoteUpdat(string DOCKNO)
        {
            string QueryString = "SELECT WTDS.DOCKNO,WTDS.DRS,WTDS.Delivered,WTDS.DRSUP_DT,WTDS.Op_Status ,WTDS.DELYPERSON,WTDS.CODCollected,WTDS.DELYREASON,WmG.CodeDesc,WPT.PKGSNO,WMU.mobileno from WEBX_MASTER_DOCKET WMD INNER JOIN  WEBX_TRANS_DOCKET_STATUS WTDS  ON WMD.DOCKNO=WTDS.DOCKNO INNER JOIN webx_PDCTRN WPT ON WTDS.DRS=WPT.PDCNO AND WTDS.DOCKNO=WPT.DOCKNO INNER JOIN  WebX_Master_Users WMU ON WMU.userid=WMD.Entryby LEFT OUTER JOIN Webx_Master_General WMG On WMG.CodeId=WTDS.DELYREASON AND WMG.CodeType='DLYPRC' where WTDS.DRSUpdated='Y'  and  WTDS.DOCKNO='" + DOCKNO + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable get_ListOFCNoteUpdat(string DOCKNO, string baselocation)
        {
            string QueryString = "SELECT A.DOCKNO,S.DOCKSF,convert(varchar,DOCKDT,106) AS DOCKDT,DRS, convert(varchar,DRS_DT,106) AS DRS_DT FROM WEBX_MASTER_DOCKET A WITH(NOLOCK) INNER JOIN WEBX_TRANS_DOCKET_STATUS S WITH(NOLOCK) ON A.DOCKNO=S.DOCKNO AND DRS IS NOT NULL AND ISNULL(DELIVERED,'N')='N' AND ISNULL(DRSUpdated,'N')='N' AND A.dockno IN ('" + DOCKNO + "') and  (case when isnull(isDelCntrlLoc,'N')='N' then Curr_loc else Delivery_Control_Loc end)='" + baselocation + "'  and isnull(DRSupdated,'N')='N' ";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public List<vw_dockets_for_drs_updation_New> GetUpdateSingleDRSList(string DOCKNO, string DRSCode)
        {

            // string SQRY = "exec usp_XML_DRS_Update '" + DOCKNO + "','" + DRSCode + "'";
            string SQRY = "SELECT TOP 1 l.[Delivery_Control_Loc],* FROM vw_dockets_for_drs_updation_New vw LEFT OUTER JOIN webx_Location l ON vw.[destcd] = l.[Loccode] WHERE dockno= '" + DOCKNO + "' and dlypdcno='" + DRSCode + "'";
            return DataRowToObject.CreateListFromTable<vw_dockets_for_drs_updation_New>(GF.GetDataTableFromSP(SQRY));
        }

        public List<vw_Master_General> GetSingleDRSResion()
        {
            string SQRY = "exec usp_DeliveryReasons";
            return DataRowToObject.CreateListFromTable<vw_Master_General>(GF.GetDataTableFromSP(SQRY));
        }

        public List<vw_Master_General> GetDeliveryControlLoc()
        {
            string SQRY = "SELECT Loccode FROM [webx_location] WHERE ActiveFlag='Y' AND [Delivery_Control_Loc]='' UNION SELECT Loccode FROM [webx_location] WHERE ActiveFlag='Y' AND [Delivery_Control_Loc]='AMD'";
            return DataRowToObject.CreateListFromTable<vw_Master_General>(GF.GetDataTableFromSP(SQRY));
        }

        public string GetDKTStatusForSingleCNotUpdate(string dockno)
        {
            string SQR = "exec USP_CheckDocketForSingleCNoteUpdateDRS '" + dockno + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt.Rows[0][0].ToString();
        }

        #endregion


        /* Chirag JD */

        #region PoGeneration SKU

        public DataTable usp_Generate_PO_Asset_Details(string Xml_POASSET_HDR_Detail, string Xml_PO_Detail, string FinalYear)
        {
            string SQR = "exec usp_Generate_PO_Asset_DetailsGST '" + Xml_POASSET_HDR_Detail.Replace("&", "&amp;").Replace("'", "&#39;").Replace("’", "&#39;").Trim() + "','" + Xml_PO_Detail.Replace("&", "&amp;").Replace("'", "&#39;").Replace("’", "&#39;").Trim() + "','" + FinalYear + "'";
            int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "usp_Generate_PO_Asset_DetailsGST", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public List<webx_GENERAL_WorkASSET_HDR> GetVenderListSearch(string Serach)
        {
            string SQR = "exec USP_GetVenderList '" + Serach + "','1'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_GENERAL_WorkASSET_HDR> GetVenderListSearch = DataRowToObject.CreateListFromTable<webx_GENERAL_WorkASSET_HDR>(Dt);
            return GetVenderListSearch;
        }
        public List<webx_GENERAL_WorkASSET_HDR> GetVenderListSearchBranchWise(string Serach, string BaseLocationCode)
        {
            string SQR = "exec USP_GetVenderListbranchWise '" + Serach + "','" + BaseLocationCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_GENERAL_WorkASSET_HDR> GetVenderListSearch = DataRowToObject.CreateListFromTable<webx_GENERAL_WorkASSET_HDR>(Dt);
            return GetVenderListSearch;
        }

        public DataTable dtUnitType()
        {
            string SQR = "SELECT CodeId,CodeDesc FROM Webx_Master_General WHERE CodeType='UNTTYP' AND StatusCode='Y' ORDER BY CodeId";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable Get_GeneralAsset(string MatCat, string FabricSheet)
        {
            string Sql_GAsset = "";
            if (FabricSheet == null || FabricSheet == "")
            {
                Sql_GAsset = "SELECT SKU_Desc+'~'+SKU_ID AS Acccode,SKU_Desc AS Accdesc FROM webx_PO_SKU_Master WITH(NOLOCK) WHERE MatCat_Id='" + MatCat + "' AND SKU_ID IN('SKU05375')";
            }
            else if (FabricSheet == "Y")
            {
                Sql_GAsset = "SELECT SKU_Desc+'~'+SKU_ID AS Acccode,SKU_Desc AS Accdesc FROM webx_PO_SKU_Master WITH(NOLOCK) WHERE MatCat_Id='" + MatCat + "' AND SKU_ID IN('SKU03502','SKU05375','SKU03504')";
            }
            else if (FabricSheet == "N")
            {
                Sql_GAsset = "SELECT SKU_Desc+'~'+SKU_ID AS Acccode,SKU_Desc AS Accdesc FROM webx_PO_SKU_Master WITH(NOLOCK) WHERE ActiveFlag='Y' AND MatCat_Id='" + MatCat + "' AND SKU_ID NOT IN('SKU03502','SKU05375','SKU03504','SKU00857','SKU09593')";
            }

            DataTable Dt = GF.GetDataTableFromSP(Sql_GAsset);
            return Dt;
        }

        public DataTable GetTyreModel()
        {
            string SQR = "SELECT CAST(MODEL_NO as NVARCHAR) +'~'+ CAST(TYRE_MODEL_ID as NVARCHAR) as Acccode,MODEL_NO AS Accdesc  from WEBX_FLEET_TYREMODELMST WITH(NOLOCK)";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable GetTyreMFG()
        {
            string SQR = "SELECT MFG_ID AS Value,MFG_Name AS Text FROM WEBX_FLEET_TYREMFG WITH(NOLOCK)";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable GetTyreSize()
        {
            string SQR = "SELECT Tyre_SizeID as Value,Tyre_SizeName As Text FROM WEBX_FLEET_TYRESIZEMST WITH(NOLOCK) ";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable GetTyreType()
        {
            //string SQR ="Select '--SELECT--' as CodeDesc,'' AS CodeID union select CodeDesc,CodeID from webx_master_general where codetype ='TYTYP'";
            string SQR = "select CodeDesc as Text,CodeID as Value from webx_master_general where codetype ='TYTYP'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable GetSpareParts(string MatCat)
        {
            string SQR = "SELECT SKU_Desc+'~'+SKU_ID AS Acccode,SKU_Desc AS Accdesc FROM webx_PO_SKU_Master WITH(NOLOCK) WHERE ActiveFlag='Y' AND MatCat_Id='" + MatCat + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable Get_StationaryAsset(string MatCat)
        {
            string SQR = "SELECT SKU_Desc+'~'+SKU_ID AS Acccode,SKU_Desc AS Accdesc FROM webx_PO_SKU_Master WITH(NOLOCK) WHERE ActiveFlag='Y' AND MatCat_Id='" + MatCat + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable Get_FixAsset()
        {
            string SQR = "SELECT assetname+'~'+ASSETCD AS Acccode,assetname AS Accdesc FROM webx_ASSETMASTER WITH(NOLOCK)";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public string WorkOrder_Done(string fin_year, string PONO)
        {
            string Table_Name = "webx_acctrans_" + fin_year;
            string sql = "exec Usp_Show_Doc_Accounting '" + PONO + "','" + Table_Name + "'";
            return GF.executeScalerQuery(sql);
        }

        public DataTable POFROMsrno()
        {
            string SQR = "SELECT SKU_ID as CodeId ,SKU_Desc +'~'+SKU_UOM as CodeDesc FROM Webx_PO_SKU_Master where /*HasSerialNo=1 and*/ ActiveFlag='Y'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        public DataTable POGenerationWithSerilaNo(string Xml_POASSET_HDR_Detail, string Xml_PO_Detail, string FinalYear, string terms_condition)
        {
            string SQR = "exec usp_POGenerationWithSerilaNo '" + Xml_POASSET_HDR_Detail.Replace("&", "&amp;").Replace("'", "&#39;").Replace("’", "&#39;").Trim() + "','" + Xml_PO_Detail.Replace("&", "&amp;").Replace("'", "&#39;").Replace("’", "&#39;").Trim() + "','" + FinalYear + "','" + terms_condition + "'";
            GF.SaveRequestServices(SQR.Replace("'", "''"), "POGenerationWithSerilaNo", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        //public List<AgentBillModel> GetVendorList_NameList(string searchTerm)
        //{
        //    string SQRY = "SELECT VENDORCODE +'-'+ VENDORNAME as PartyCode  from  webx_VENDOR_HDR  WHERE VENDORCODE like '%" + searchTerm + "%' OR VENDORNAME LIKE '%" + searchTerm + "%'";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    List<AgentBillModel> GetDriver_NameList_XML = DataRowToObject.CreateListFromTable<AgentBillModel>(Dt);

        //    return GetDriver_NameList_XML;
        //}


        #endregion

        #region POApproval

        public List<webx_GENERAL_POASSET_HDR> GetPOData(string FilterType, string VendorCode, string FirstDate1, string LastDate1, string POCode, string ManualPO, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_PO_Listing_For_Approval_NewPortal '" + FilterType + "','" + VendorCode + "','" + FirstDate1 + "','" + LastDate1 + "','" + POCode + "','" + ManualPO + "','" + BaseLocationCode + "','1'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);

            return POList;
        }

        public List<webx_GENERAL_POASSET_HDR> GetWOData(string FilterType, string VendorCode, string FirstDate1, string LastDate1, string POCode, string ManualPO, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_WO_Listing_For_Approval_NewPortal '" + FilterType + "','" + VendorCode + "','" + FirstDate1 + "','" + LastDate1 + "','" + POCode + "','" + ManualPO + "','" + BaseLocationCode + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);

            return POList;
        }

        public DataTable GetPOAprovalLimit(string UserName)
        {
            string SQRY = "";
            SQRY = "EXEC Usp_CheckPOLimit '" + UserName + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable ApprovePO(string POcodeList, string UserName, string approveDate, string POorWo)
        {
            string SQRY = "";
            SQRY = "EXEC usp_POApprove_NewPortal '" + POcodeList + "','" + UserName + "','" + approveDate + "','" + POorWo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //DataTable Dt = new DataTable();
            return Dt;
        }

        #endregion

        #region SKU PO EDIT FOR WITHOUT SERIALNO

        public List<webx_GENERAL_POASSET_HDR> GetDataForPOEdit(string FilterType, string VendorCode, string FirstDate1, string LastDate1, string POCode, string ManualPONo, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "EXEC USP_PO_Listing_For_POEdit_NewPortal '" + FilterType + "','" + VendorCode + "','" + FirstDate1 + "','" + LastDate1 + "','" + POCode + "','" + ManualPONo + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_GENERAL_POASSET_HDR> POList = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(Dt1);

            return POList;
        }

        public DataTable GetPODetails(string POCode)
        {
            string SQRY = "exec Usp_GetPoEditHeaderDetail '" + POCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetPOHDRDetails(string POCode)
        {
            string SQRY = "exec Usp_GetPoHDRDetails '" + POCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetPODetDetails(string POCode)
        {

            string SQRY = "select srno=ROW_NUMBER() over (order by srno),TyreMFG_Code,qty,UnitType,rate,total,B.SKU_Desc+'~'+assetcd as assetcd,narration,WarrantyInYear from webx_GENERAL_POASSET_det A LEFT OUTER join Webx_PO_SKU_Master B on A.assetcd=B.SKU_ID  where pocode='" + POCode + "' order by srno";

            DataTable Dt = GF.GetDataTableFromSP(SQRY);

            return Dt;
        }

        public DataTable GetSKUPOUpdate(string XML1, string XML2, string EntryBy, string terms_condition, string remarks, string Specification, string PaymentTerms)
        {
            string SQRY = "exec Usp_Update_SKUPO_Edit_NewPortal '" + XML1 + "','" + XML2 + "','" + EntryBy + "','" + terms_condition + "','" + remarks + "','" + Specification + "','" + PaymentTerms + "'";

            string SQL = "INSERT CYGNUS_Called_GCServices (String,ModuleName,Succes,ErrorMessage,EntryDate)" +
                    "VALUES('" + SQRY.Replace("'", "''") + "','PO EDIT WITHOUT SERIALNO','','',getdate())";

            DataTable DT1 = GF.GetDataTableFromSP(SQL);

            DataTable DT = GF.GetDataTableFromSP(SQRY);

            return DT;
        }

        #endregion

        #region Bill Submission

        public List<webx_BILLMST> GetBillForSubmission(BillingFilterViewModel model, string Loccode, string CompanyCode, string UserID, string Finyear)//Enquiry Listing and Enquiry Single Record
        {
            model.ToDate = Convert.ToDateTime(model.ToDate.ToString("dd MMM yyyy") + " 23:59:59");

            string SQRY = "exec [webx_BillSubmission_NewMVCPortal] '" + model.BillNo + "','" + model.BillingParty + "','" + model.BillType + "','" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + Loccode + "','" + model.ManualBillNo + "','" + CompanyCode + "','" + Finyear + "'    ";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> BillList = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt1);
            return BillList;
        }

        public DataTable SubmissionDone(string XMLStr, string BSUBEMPCD, string BILLSUBTO, string SUBTOTEL, string SubDate, string Location, string CompanyCode)//Enquiry Listing and Enquiry Single Record
        {
            string SQRY = "exec [usp_XML_Bill_Submission_NewPortal] '" + BSUBEMPCD + "','" + XMLStr + "','" + BILLSUBTO + "','" + SUBTOTEL + "','" + SubDate + "','" + Location + "','" + BSUBEMPCD + "','" + CompanyCode + "'   ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "SubmissionDone", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public List<webx_CUSTHDR> GetCustomerListFromSearch(string Serach)
        {
            string SQR = "exec USP_GetCustomerList '" + Serach + "','1'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);

            return GetCustomerVendorList;
        }

        #endregion

        #region Bill Collection

        public List<webx_BILLMST> GetBillForCollection(BillingFilterViewModel model, string Loccode, string CompanyCode, string UserID)
        {
            model.ToDate = Convert.ToDateTime(model.ToDate.ToString("dd MMM yyyy") + " 23:59:59");
            if (model.BillType == "0")
            {
                model.BillType = "15";
            }
            else
            {
                model.BillType = model.BillType;
            }
            //string SQRY = "exec [webx_BillCollection_Ver2_New] '" + model.BillNo + "','" + model.BillingParty + "','" + model.BillType + "','" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + Loccode + "','" + model.ManualBillNo + "','" + model.BillType + "'";
            string SQRY = "exec [webx_BillCollection_Ver2_New] '" + model.BillNo + "','" + model.BillingParty + "','" + model.BillType + "','" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + Loccode + "','" + model.ManualBillNo + "','N'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> BillList = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt1);
            return BillList;
        }

        public DataTable CollectionDone(string GenXml, string Xml_Chq_Det, string Xml_BillMR_DET, string Xml_MRHDR, string BaseFinYear, string Location, string CompanyCode, string UserId)//Enquiry Listing and Enquiry Single Record
        {
            string SQRY = "exec [Usp_BillMR_Generate_NEWMVCPORTAL] '" + Location + "','" + GenXml + "','" + Xml_Chq_Det + "','" + Xml_BillMR_DET + "','" + Xml_MRHDR + "','" + BaseFinYear + "','" + UserId + "'   ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "CollectionDone", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public List<webx_acctinfo> GetSecurityLedger(string Branch)
        {
            string SQRY = "exec Usp_Bind_Security_deposite_ledger '" + Branch + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<webx_acctinfo> ListGetDSTimeSloteDetail = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return ListGetDSTimeSloteDetail;
        }

        public List<webx_acctinfo> GetTDSLedger(string Branch)
        {
            string SQRY = " select Acccode,Accdesc from Vw_Tds_Recivable  where  Acccode='CLA0014'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<webx_acctinfo> ListGetDSTimeSloteDetail = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return ListGetDSTimeSloteDetail;
        }

        public List<Webx_Master_General> Get_Bill_Type_List()
        {
            string SQRY = " SELECT CodeId as Code ,CodeDesc FROM WEBX_MASTER_GENERAL WHERE CODETYPE='BILLTYP' AND CODEID NOT IN('11','12') AND StatusCode='Y' ORDER BY CAST(CODEID AS NUMERIC(10))";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<Webx_Master_General> List = DataRowToObject.CreateListFromTable<Webx_Master_General>(DT);
            return List;
        }

        public List<webx_BILLMST> GetBillCollection_FromBillNo(string BillNo)
        {
            string SQRY = "exec [WEBX_BILLMST_Detail_Ver1_NewPortal] '" + BillNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> BillList = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt1);
            return BillList;
        }

        public string Duplicate_BillNO_Check(string BILLNO)
        {
            string Cnt = "";
            string SQRY = "EXEC WEBX_Bill_Duplicate_Check_Collection '" + BILLNO + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);

            try
            {
                Cnt = Dt1.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                Cnt = "F";
            }

            return Cnt;
        }

        public string GetLocation(string lococde)
        {
            string locname = "";
            string SQRY = "exec sp_get_locnm '" + lococde + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);

            locname = Dt1.Rows[0]["locname"].ToString();

            if (locname == null)
                locname = "";

            return locname;
        }

        public DataTable Collection_Process(string MR_Location, string Xml_Dkt_Bill, string Xml_Chq_Det, string Xml_BillMR_DET, string Xml_MRHDR, string FinYear, string EntryBy, string BillColType)
        {
            //string SQRY = "exec [Usp_BillMR_Generate_Ver2] '" + MR_Location + "','" + Xml_Dkt_Bill + "','" + Xml_Chq_Det + "','" + Xml_BillMR_DET + "','" + Xml_MRHDR + "','" + FinYear + "','" + EntryBy + "','" + BillColType + "'";
            string SQRY = "exec [Usp_BillMR_Generate_Ver2_NewPortal] '" + MR_Location + "','" + Xml_Dkt_Bill + "','" + Xml_Chq_Det + "','" + Xml_BillMR_DET + "','" + Xml_MRHDR + "','" + FinYear + "','" + EntryBy + "','" + BillColType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Collection_Process", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }


        #endregion

        #region DebitCreditVoucher

        public DataTable InsertmanualDebitCreditVoucher(string XML, string XMLArticle)
        {
            string SQR = "exec usp_Generate_ManualAccountDetails_Ver4_NewPortal '" + XML + "','" + XMLArticle + "'";
            int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "DebitCreditVoucher", "", "");

            DataTable Dt = GF.GetDataTableFromSP(SQR);

            return Dt;
        }

        public List<webx_acctinfo> GetTDSSectionList()
        {
            string SQRY = " select Acccode ,Accdesc  FROM webx_acctinfo where Acccode IN ('LIA0116','LIA0112','LIA1141','LIA0823','LIA0385','LIA0348','LIA1737','CLA0014') and ACTIVEFLAG='Y'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<webx_acctinfo> GetTDSSectionListDetail = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return GetTDSSectionListDetail;
        }

        public List<webx_acctinfo> GetTDSSectionList_NewPortal(string BRCD, string UserName, string Type)
        {
            string SQRY = "exec USP_Get_TDS_Details '" + BRCD + "','" + UserName + "','" + Type + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            List<webx_acctinfo> GetTDSSectionListDetail = DataRowToObject.CreateListFromTable<webx_acctinfo>(DT);
            return GetTDSSectionListDetail;
        }

        public DataTable GetCostTypeFromAccountCode(string AccountCode)
        {
            string SQRY = "exec Usp_GetCostCodeForDrcrVoucher'" + AccountCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetAccountCodeisValidorNot(string AccountCode, string CodeType)
        {
            string SQRY = "exec [USP_GetAccountCodeIsValidOrNot_New_Portal] '" + AccountCode + "','" + CodeType + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<webx_CUSTHDR> GetCustomerUserVehicleDriverListFromSearch(string Serach, string Type)
        {
            string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + Serach + "','1','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);

            return GetCustomerVendorList;
        }

        public List<webx_acctinfo> GetAccountCodeListFromSearch(string Serach, string BaseLocationCode)
        {
            string SQR = "exec USP_GetAccountCodeList '" + Serach + "','1','" + BaseLocationCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_acctinfo> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt);

            return GetCustomerVendorList;
        }

        public DataTable GetDR_VR_Trn_Halt(string ToTAMT, string BaseLocationCode, string FIN_Start, string fin_year, string BaseCompanyCode)
        {
            string SQRY = "exec WEBX_DR_VR_Trn_Halt '" + BaseLocationCode + "','" + System.DateTime.Now.ToString("dd MMM yyyy") + "','" + FIN_Start + "','" + fin_year + "','" + ToTAMT + "'/*,'" + BaseCompanyCode.Trim() + "'*/";

            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }


        #endregion

        #region Contra Voucher

        public DataTable GetContraVoucherSubmit(string Xml_Acccode_Details, string Xml_Other_Details)
        {
            string SQRY = "exec usp_Generate_Ins_Up_ContraAccountDetails_Ver3_NewPortal '" + Xml_Acccode_Details + "','" + Xml_Other_Details + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Contra Voucher", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable GetLedgerAccountFromPaymode(string Paymode, string LocCode)
        {
            string SQRY = "exec USP_GetAccoutLedgerForContra '" + Paymode + "','" + LocCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public string CheckManualNoForVouchers(string ManualNo)
        {
            string QueryString = "Exec Usp_CheckManualNoForVouchers '" + ManualNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }

        public string CheckReferenceNoForVouchers(string ReferenceNo)
        {
            string QueryString = "Exec Usp_CheckRefNoForVouchers '" + ReferenceNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable.Rows[0][0].ToString();
        }
        #endregion

        #region Journal Voucher

        public DataTable InsertJournalVoucher(string XML, string XMLArticle)
        {
            string SQR = "exec [usp_Generate_ManualJVAccountDetails_Ver3] '" + XML + "','" + XMLArticle + "'";
            int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "JournalVoucher", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        #region MJV

        public DataTable InsertMJV(string XML, string XMLArticle)
        {
            string SQR = "exec [usp_Generate_ManualJVAccountDetails_Ver3] '" + XML.Replace("&", "&amp;").Replace("–", "-").Trim() + "','" + XMLArticle.Replace("&", "&amp;").Replace("–", "-").Trim() + "'";
            int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "InsertMJV", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        #region Special Cost Voucher

        public DataTable InsertSpecialCostVoucher(string XML, string XMLArticle)
        {
            string SQR = "exec usp_Generate_ManualAccountDetails_Ver4_NewPortal '" + XML + "','" + XMLArticle + "'";
            int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "SpecialCostVoucher", "", "");

            DataTable Dt = GF.GetDataTableFromSP(SQR);

            return Dt;
        }

        public DataTable GetChequeNoDuplicateorNot(string ChequeNo, string ChequeDate)
        {
            string SQRY = "exec [WEBX_Chq_Duplicate_Check] '" + ChequeNo + "','" + ChequeDate + "' ";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Advice Generatoin

        public DataTable GetAdviceGenerationSubmit(string AdviceDt, string AdviceType, string RaisedOn, string RaisedBy, decimal PaymentAmount, string reason, string PayMode, string enclosed, string BaseFinYear, string AdvGenByCd, string fromAcccode, string ChequeNo, string ChequeDate, string ChqueEntry_YN_14, string COMPANY_CODE, string Adviceno)
        {
            string SQRY = "exec usp_Insert_webx_Advice_hdr_ChqDetail_NewPortal '" + AdviceDt + "','" + AdviceType + "','" + RaisedOn + "','" + RaisedBy + "','" + PaymentAmount + "','" + reason + "','" + PayMode + "','" + enclosed + "','" + BaseFinYear + "','" + AdvGenByCd + "','" + fromAcccode + "','" + ChequeNo + "','" + ChequeDate + "','" + ChqueEntry_YN_14 + "','" + COMPANY_CODE + "','" + Adviceno + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Advice Generation", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);

            return DT;
        }

        #endregion

        #region Advice Acknowledgement

        public List<WEBX_Advice_Hdr> GetAdivceAcknowledgementList(DateTime datefrom, DateTime dateto, string Adviceno, string raisedon, string transtable, string CompanyCode)
        {
            string SQRY = "";
            string datefrom1 = datefrom.ToString("dd MMM yyyy");

            string dateto1 = dateto.ToString("dd MMM yyyy");

            SQRY = "exec usp_Advice_Acknowledge_NewPortal '" + datefrom1 + "','" + dateto1 + "','" + Adviceno + "','" + raisedon + "','" + transtable + "','" + CompanyCode + "'";

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            List<WEBX_Advice_Hdr> AdviceAcknowledgementListView = DataRowToObject.CreateListFromTable<WEBX_Advice_Hdr>(DT);

            return AdviceAcknowledgementListView;
        }

        public DataTable GetAdviceAcknowledgementSubmit(string Adviceno, string EntryBy, string accdate1, string BaseLocationCode, string BaseLocationCode1, string EntryBy1, string Toacccode, string accdatedp, string Year)
        {
            string SQRY = "exec usp_Advice_updateData_NewPortal '" + Adviceno + "','" + EntryBy + "','" + accdate1 + "','" + BaseLocationCode + "','" + BaseLocationCode1 + "','" + EntryBy1 + "','" + Toacccode + "','" + accdatedp + "','" + Year + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Advice Acknowledgement", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);

            return DT;
        }

        #endregion

        #region Advice Cancellation

        public List<WEBX_Advice_Hdr> GetAdviceCancellationList(DateTime FromDate, DateTime ToDate, string VoucherNo)
        {
            string SQRY = "";
            string FromDate1 = FromDate.ToString("dd MMM yyyy");
            string ToDate1 = ToDate.ToString("dd MMM yyyy");

            SQRY = "exec USP_New_Advice_Cancellation_Listing_NewPortal '" + FromDate1 + "','" + ToDate1 + "','" + VoucherNo + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_Advice_Hdr> VoucherList_Data = DataRowToObject.CreateListFromTable<WEBX_Advice_Hdr>(Dt1);

            return VoucherList_Data;
        }

        public DataTable GetAdviceCancellationSubmit(string entryby, DateTime Cancel_Date, string Cancel_Reason, string Adviceno, string BaseFinancialYear)
        {
            string Cancel_Date1 = Cancel_Date.ToString("dd MMM yyyy");
            string SQRY = "usp_Advice_Cancellation_NewPortal '" + entryby + "','" + Cancel_Date1 + "','" + Cancel_Reason + "','" + Adviceno + "','" + BaseFinancialYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "AdviceCancellation", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        public double GetRebateRate(DateTime date)
        {
            string sqlstr = "SELECT TOP 1 Cast((100-stax_rebate_per)/100 as Numeric(18,2)) AS stax_rebate_per FROM WebX_taxrebate ";
            sqlstr = sqlstr + " WHERE '" + date.ToString("dd MMM yyyy") + "' BETWEEN startdate AND enddate and stax_rebate_per>0";

            double heducessrate;
            try
            {
                heducessrate = Convert.ToDouble(GF.executeScalerQuery(sqlstr));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return heducessrate;
        }

        public decimal GetServicetaxrate()
        {
            decimal Servicetaxrate = 0;
            DataTable Dt = new DataTable();
            var date = System.DateTime.Now.ToString("dd MMM yyyy");
            string sql = "";
            sql = "SELECT TOP 1 ISNULL(svctax_rate,0) AS servicetaxrate FROM webx_chargemst where  getdate() BETWEEN startdate AND enddate";

            Dt = GF.GetDataTableFromSP(sql);
            Servicetaxrate = Convert.ToDecimal(Dt.Rows[0][0].ToString());
            return Servicetaxrate;
        }
        #region BankingOperation

        public DataTable GetChequeDetail(string Chqno, string ChqDate)
        {
            //string SQRY = "select CHQ_B_R,chq_status,depoflag from webx_chq_det where chqno='" + Chqno + "' and convert(varchar,chqdt,106)=convert(datetime,'" + ChqDate + "',106)";
            string SQRY = "EXEC USP_GetChequeDetails '" + Chqno + "','" + ChqDate + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable checkBounceDetail(string Chqno, string ChqDate)
        {
            string date = Convert.ToDateTime(ChqDate).ToString("dd MMM yyyy");
            string SQRY = "select Availabe_FOR_USE_YN from webx_Chq_Bounce_Reoffer_Details where chqno='" + Chqno + "' and convert(varchar,chqdt,106)=convert(datetime,'" + ChqDate + "',106) and Availabe_FOR_USE_YN='U'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<Cheque_det> ListChqBounceDocument(string Chqno, string ChqDate)
        {
            string SQRY = "exec USP_Chq_Bounce_SHOW_Document '" + Chqno + "','" + ChqDate + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Cheque_det> ListChqBounceDocument = DataRowToObject.CreateListFromTable<Cheque_det>(Dt1);
            return ListChqBounceDocument;
        }

        public List<Cheque_det> ListChqDetail(string Chqno, string ChqDate)
        {
            string SQRY = "exec USP_Chq_Details '" + Chqno + "','" + ChqDate + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Cheque_det> ListChqDetail = DataRowToObject.CreateListFromTable<Cheque_det>(Dt1);
            return ListChqDetail;
        }

        public List<Cheque_det> ListChqDeposit(string Chqno, string ChqDate, string fin_year)
        {
            string TableName = "Webx_acctrans_" + fin_year;
            string SQRY = "exec USP_Chq_Deposit_Details '" + Chqno + "','" + ChqDate + "','" + TableName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<Cheque_det> ListChqDeposit = DataRowToObject.CreateListFromTable<Cheque_det>(Dt1);
            return ListChqDeposit;
        }

        public string Chq_Bounce_Updation(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        {
            try
            {
                string SQRY = "Exec USP_ChequeBounceProcess '" + Chqno + "','" + ChqDate + "','" + BounceDate + "','" + UserName + "','" + FinYear + "','" + SubYear + "'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Chq_Bounce_Updation", "", "");
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                return "Sucess";
            }
            catch (Exception ex)
            {
                return ex.Message.ToString();
            }

        }

        //public string Chq_Bounce_Updation1(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        //{
        //    string SQRY = "Exec USP_Chq_Bounce_Updation 1,'" + Chqno + "','" + ChqDate + "','" + BounceDate + "','" + UserName + "','" + FinYear + "'";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);

        //    return "Sucess";
        //}

        //public string Chq_Bounce_Updation2(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        //{
        //    string SQRY1 = "Exec USP_Chq_Bounce_Updation 2,'" + Chqno + "','" + ChqDate + "','" + BounceDate + "','" + UserName + "','" + FinYear + "'";
        //    DataTable Dt1 = GF.GetDataTableFromSP(SQRY1);
        //    return "Sucess";
        //}

        //public string Chq_Bounce_Updation3(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        //{
        //    string SQRY4 = "Exec USP_Chq_Bounce_Updation 3,'" + Chqno + "','" + ChqDate + "','" + BounceDate + "','" + UserName + "','" + FinYear + "'";
        //    DataTable Dt4 = GF.GetDataTableFromSP(SQRY4);
        //    return "Sucess";
        //}

        //public string USP_Chq_Bounce_Reverese_Accounting1(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        //{
        //    string TableName = "Webx_acctrans_" + SubYear;
        //    string SQRY2 = "Exec USP_Chq_Bounce_Reverese_Accounting 1,'" + Chqno + "','" + ChqDate + "','" + TableName + "','" + UserName + "','" + FinYear + "'";
        //    DataTable Dt2 = GF.GetDataTableFromSP(SQRY2);
        //    return "Sucess";

        //}

        //public string USP_Chq_Bounce_Reverese_Accounting2(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        //{
        //    string TableName = "Webx_acctrans_" + SubYear;
        //    string SQRY3 = "Exec USP_Chq_Bounce_Reverese_Accounting 2,'" + Chqno + "','" + ChqDate + "','" + TableName + "','" + UserName + "','" + FinYear + "'";
        //    DataTable Dt3 = GF.GetDataTableFromSP(SQRY3);
        //    return "Sucess";
        //}

        public string Chq_Reoffer_Updation1(string Chqno, string ChqDate, string BounceDate, string UserName, string FinYear, string SubYear)
        {
            string SQRY = "Exec USP_Chq_Bounce_Updation 1,'" + Chqno + "','" + ChqDate + "','" + BounceDate + "','" + UserName + "','" + FinYear + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);

            return "Sucess";
        }

        #endregion

        #region Bank ReConlication
        public List<webx_location> BankLocationList(string branchCode)
        {
            string SQRY = "";
            if (branchCode != "")
            {
                SQRY = "USP_Cygnus_BankLocationList'" + branchCode + "'";
            }
            else
            {
                SQRY = "USP_Cygnus_BankLocationList'" + branchCode + "'";
            }
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_location> BankLocationList = DataRowToObject.CreateListFromTable<webx_location>(Dt1);
            return BankLocationList;
        }





        public List<webx_location> RoLocationList(string Ro)
        {
            string SQRY = "USP_Cygnus_RoLocationList'" + Ro + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_location> RoLocationList = DataRowToObject.CreateListFromTable<webx_location>(Dt1);
            return RoLocationList;
        }


        public List<webx_location> LocationROWiseBANKList(string Code, string branchCode)
        {
            string SQRY = "";

            SQRY = "exec USP_Location_RO_Wise_BANK_Listing '" + Code + "','" + branchCode + "'";

            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_location> BankLocationList = DataRowToObject.CreateListFromTable<webx_location>(Dt1);
            return BankLocationList;
        }
        public string getAccountDesc(string ATableName, string Code)
        {
            string AccDesc = "";
            string SQRY = "select accdesc from " + ATableName + "  with(NOLOCK) where acccode='" + Code + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            AccDesc = Dt.Rows[0]["AccDesc"].ToString();

            if (AccDesc == null) AccDesc = "";
            return AccDesc;
        }
        //public DataTable gettranjaction(string transTable, string Bankcode, string Fromdate)
        //{
        //    string SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) < convert(datetime, '" + Fromdate + "' ,106)";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}


        public DataTable gettranjaction(string transTable, string Bankcode, string Fromdate, string LO, string RO, string comapnycode)
        {
            string SQRY = "";
            if (LO == "All" && RO == "All")
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) < convert(datetime, '" + Fromdate + "' ,106) and COMPANY_CODE='" + comapnycode + "'";
            else if ((LO != "All" && RO == "All") || (LO != "All" && RO != "All"))
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) < convert(datetime, '" + Fromdate + "' ,106) and COMPANY_CODE='" + comapnycode + "' and Brcd='" + LO + "'";
            else if (LO == "All" && RO != "All")
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) < convert(datetime, '" + Fromdate + "' ,106) and COMPANY_CODE='" + comapnycode + "' and Brcd in(SELECT Loccode FROM webx_location WHERE (LocCode='" + RO + "' OR Report_Loc='" + RO + "'))";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        //public DataTable getopening(string openTable, string Bankcode)
        //{
        //    string SQRY = "select isNull(sum(opencredit_i),0.00) as opencredit_i,isNull(sum(opendebit_i),0.00) as opendebit_i from " + openTable + "   With(NOLOCK) where   acccode= '" + Bankcode + "'";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}


        public DataTable getopening(string openTable, string Bankcode, string LO, string RO, string comapnycode)
        {
            string SQRY = "";
            if (LO == "All" && RO == "All")
                SQRY = "select isNull(sum(opencredit_i),0.00) as opencredit_i,isNull(sum(opendebit_i),0.00) as opendebit_i from " + openTable + "   With(NOLOCK) where   acccode= '" + Bankcode + "' and COMPANY_CODE='" + comapnycode + "'";
            else if (LO == "All" && RO != "All")
                SQRY = "select isNull(sum(opencredit_i),0.00) as opencredit_i,isNull(sum(opendebit_i),0.00) as opendebit_i from " + openTable + "   With(NOLOCK) where   acccode= '" + Bankcode + "' AND COMPANY_CODE='" + comapnycode + "' and Brcd in(SELECT Loccode FROM webx_location WHERE (LocCode='" + RO + "' OR Report_Loc='" + RO + "'))";
            else if ((LO != "All" && RO == "All") || (LO != "All" && RO != "All"))
                SQRY = "select isNull(sum(opencredit_i),0.00) as opencredit_i,isNull(sum(opendebit_i),0.00) as opendebit_i from " + openTable + "   With(NOLOCK) where   acccode= '" + Bankcode + "' AND COMPANY_CODE='" + comapnycode + "' and Brcd='" + LO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        //public DataTable getTotal_transactional(string transTable, string Bankcode, string Fromdate, string Todate)
        //{
        //    string SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) between convert(datetime, '" + Fromdate + "' ,106) and convert(datetime, '" + Todate + "' ,106) ";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}

        public DataTable getTotal_transactional(string transTable, string Bankcode, string Fromdate, string Todate, string LO, string RO, string comapnycode)
        {
            string SQRY = "";
            if (LO == "All" && RO == "All")
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) between convert(datetime, '" + Fromdate + "' ,106) and convert(datetime, '" + Todate + "' ,106)  and COMPANY_CODE='" + comapnycode + "'";
            else if ((LO != "All" && RO == "All") || (LO != "All" && RO != "All"))
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) between convert(datetime, '" + Fromdate + "' ,106) and convert(datetime, '" + Todate + "' ,106)  and COMPANY_CODE='" + comapnycode + "' and Brcd='" + LO + "'";
            else if (LO == "All" && RO != "All")
                SQRY = "Select isNull(sum(Credit),0.00) as Credit,isNull(sum(Debit),0.00) as Debit from " + transTable + "  With(NOLOCK) where   Acccode= '" + Bankcode + "' and Voucher_Cancel='N' and convert(varchar,transdate,106) between convert(datetime, '" + Fromdate + "' ,106) and convert(datetime, '" + Todate + "' ,106)  and COMPANY_CODE='" + comapnycode + "' and Brcd in(SELECT Loccode FROM webx_location WHERE (LocCode='" + RO + "' OR Report_Loc='" + RO + "'))";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;



        }
        public List<webx_acctrans_Model> GetAccountTranCreaditList(string Bankcode, string Fromdate, string Todate, string transTable, string TranType, string compayCode)
        {
            string SQRY = "exec USP_Bank_Reconsilation_Module_NewPortal '" + Bankcode + "','" + Fromdate + "','" + Todate + "','" + transTable + "','Credit','" + TranType + "','" + compayCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_acctrans_Model> GetAccountTranCreaditList = DataRowToObject.CreateListFromTable<webx_acctrans_Model>(Dt);
            return GetAccountTranCreaditList;
        }
        public List<webx_acctrans_Model> GetAccountTranDebitList(string Bankcode, string Fromdate, string Todate, string transTable, string TranType, string compayCode)
        {
            string SQRY = "exec USP_Bank_Reconsilation_Module_NewPortal '" + Bankcode + "','" + Fromdate + "','" + Todate + "','" + transTable + "','Debit','" + TranType + "','" + compayCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_acctrans_Model> GetAccountTranDebitList = DataRowToObject.CreateListFromTable<webx_acctrans_Model>(Dt);
            return GetAccountTranDebitList;
        }
        public DataTable Insert_BankRecoData(string Xml_PAY_Mst, string FinYear)
        {
            //string Voucherno = "";
            string Sql = "";
            Sql = "EXEC [usp_InsertChequeClearDtl_Ver4] '" + Xml_PAY_Mst + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "BankRecoSubmit", "", "");
            DataTable DT = GF.GetDataTableFromSP(Sql);
            //Voucherno = DT.Rows[0][0].ToString() ;
            return DT;
        }
        //public DataTable UpdateBankReModule(string transTable, string clearDate, string commet, decimal srno, string chkno, string chqdate, string ChqIssueddate)
        //{
        //    string SQRY = "Update " + transTable + " set chqcleardate='" + clearDate + "',Comment='" + commet + "',ChqIssueddate='" + ChqIssueddate + "' where  srno='" + srno + "'";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}


        public DataTable UpdateBankReModule(string transTable, string clearDate, string commet, decimal srno, string chkno, string chqdate)
        {
            string SQRY = "Update " + transTable + " set chqcleardate='" + clearDate + "',Comment='" + commet + "' where  srno='" + srno + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }


        //public DataTable UpdateBankReModule_06_07(string clearDate, string commet, string chkno, string chqdate, string ChqIssueddate)
        //{
        //    string SQRY = "Update webx_acctrans_06_07 set chqcleardate='" + clearDate + "',Comment='" + commet + "' ,ChqIssueddate='" + ChqIssueddate + "' where    Chqno='" + chkno + "' and convert(datetime,chqdate,106)=convert(varchar,'" + chqdate + "',106) and acccode in (select acccode from webx_acctinfo where acccategory='BANK')";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}

        public DataTable UpdateBankReModule_06_07(string clearDate, string commet, string chkno, string chqdate)
        {
            string SQRY = "[Usp_Cygnus_UpdateBankReModule_06_07]'" + chkno + "','" + chqdate + "','" + clearDate + "','" + commet + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        //public DataTable UpdateBankReModule_det(string chkno, string chqdate)
        //{
        //    string SQRY = "Update webx_chq_det set chq_clear='Y'  where  Chqno='" + chkno + "' and convert(datetime,chqdt,106)=convert(varchar,'" + chqdate + "',106)";
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}

        public DataTable UpdateBankReModule_det(string chkno, string chqdate, string chqclrdate)
        {
            string SQRY = "Usp_Cygnus_UpdateBankReModule_det'" + chkno + "','" + chqdate + "','" + chqclrdate + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public List<webx_acctrans_TDS_Model> GetTDSReconlicationList(string VR, string MR, string Fromdate, string Todate, string CustNo)
        {
            string SQRY = "exec USP_TDS_RECO_ACKNOWLEDGEMENT '" + Fromdate + "','" + Todate + "','" + VR + "','" + MR + "','" + CustNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_acctrans_TDS_Model> GetTDSReconlicationList = DataRowToObject.CreateListFromTable<webx_acctrans_TDS_Model>(Dt);
            return GetTDSReconlicationList;
        }
        public DataTable AddTDSReclocation(string FileName, string ReceiveDate, string SRNO)
        {
            string SQRY = "UPDATE WEBX_TDS_HDR SET FORM16_RCV_STATUS='Y',FORM16_RCV_COPYNM='" + FileName + "',FORM16_RCV_DT='" + ReceiveDate + "' WHERE Srno='" + SRNO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        //public DataTable UpdateBankReModuleSubmit(string XML, string transTable)
        //{
        //    string SQRY = "exec Usp_Update_BankReModule_Cygnus '" + XML + "','" + transTable + "'";
        //    int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UpdateBankReModule", "", "");
        //    DataTable DT = GF.GetDataTableFromSP(SQRY);
        //    return DT;
        //}
        public DataTable UpdateBankReModuleSubmit(string XML, string transTable)
        {
            string SQRY = "exec [usp_InsertChequeClearDtl_NewPortal] '" + XML + "','" + transTable + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "UpdateBankReModule", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Octrio Fizalization Bill

        public List<OctroiFinalizationBill> GetOctrioFizaBillList(DateTime FromDate, DateTime ToDate, string PartyCode, string DocketNo, string FinYear, string hidType, string RO, string LO, string BillType)
        {
            if (DocketNo != "")
            {
                hidType = "1";
            }
            else
            {
                hidType = "0";
            }

            string SQLStr = "EXEC USP_OCTROI_BILL_FOR_FINALIZATION_NewPortal 'Octroi','" + GF.FormateDate(FromDate) + "','" + GF.FormateDate(ToDate) + "','" + PartyCode + "','" + DocketNo + "','" + FinYear + "','" + hidType + "','" + RO + "','" + LO + "','" + BillType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQLStr);
            List<OctroiFinalizationBill> GetOctrioFizaBillList = DataRowToObject.CreateListFromTable<OctroiFinalizationBill>(Dt1);
            return GetOctrioFizaBillList;
            //string SQLStr = "EXEC USP_OCTROI_BILL_FOR_FINALIZATION 'Octroi','" + GF.FormateDate(FromDate) + "','" + GF.FormateDate(ToDate) + "','" + PartyCode + "','" + DocketNo + "','" + FinYear + "','" + hidType + "','" + RO + "','" + LO + "','" + BillType + "'";
            //DataTable Dt1 = GF.GetDataTableFromSP(SQLStr);
            //List<OctroiFinalizationBill> GetOctrioFizaBillList = DataRowToObject.CreateListFromTable<OctroiFinalizationBill>(Dt1);
            //return GetOctrioFizaBillList;
        }

        public DataTable AddOctroiBill(string BillType, string OctroiBill, string Finyear)
        {
            DataTable Finalizeddt = new DataTable();
            DataTable dt = new DataTable();
            string Str = "";
            string Finalized = "";
            if (BillType == "1")
            {
                string strcheck = "SELECT ISNULL(finalized,'N') as Finalized FROM webx_BILLMST WHERE BillNo='" + OctroiBill + "'";
                Finalizeddt = GF.GetDataTableFromSP(strcheck);
                Finalized = Finalizeddt.Rows[0]["finalized"].ToString();
                if (Finalized == "N")
                {
                    Str = "UPDATE dbo.webx_BILLMST WITH(ROWLOCK) SET finalized ='Y'";
                    Str = Str + " WHERE BillNo='" + OctroiBill + "';";
                    Str = Str + " exec usp_octroibill_transaction '1','" + OctroiBill + "','" + Finyear + "','06','E'";

                    dt = GF.GetDataTableFromSP(Str);
                }
            }
            else
            {
                string strcheck = "exec USP_Octroi_Bill_Finalized '" + OctroiBill + "','" + Finyear + "'";
                int Id = GF.SaveRequestServices(strcheck.Replace("'", "''"), "Octroi_Bill_Finalized", "", "");
                dt = GF.GetDataTableFromSP(strcheck);

                //string strcheck = "SELECT ISNULL(Finalized,'N') as Finalized FROM webx_oct_hdr WHERE OCBILLNO='" + OctroiBill + "'";
                //Finalizeddt = GF.GetDataTableFromSP(strcheck);
                //Finalized = Finalizeddt.Rows[0]["finalized"].ToString();
                //if (Finalized == "N")
                //{
                //    Str = "UPDATE dbo.webx_oct_hdr WITH(ROWLOCK) SET Finalized ='Y'";
                //    Str = Str + " WHERE OCBILLNO='" + OctroiBill + "';";
                //    Str = Str + " exec usp_OctroiAgentBILL_Transaction '1','" + OctroiBill + "','" + Finyear + "','07','E'";
                //}
            }
            return Finalizeddt;
        }

        //public DataTable AddOctroiBill(string BillType, string OctroiBill, string Finyear)
        //{
        //    DataTable Finalizeddt = new DataTable();
        //    DataTable dt = new DataTable();
        //    string Str = "";
        //    string Finalized = "";
        //    if (BillType == "1")
        //    {
        //        string strcheck = "SELECT ISNULL(finalized,'N') as Finalized FROM webx_BILLMST WHERE BillNo='" + OctroiBill + "'";
        //        Finalizeddt = GF.GetDataTableFromSP(strcheck);
        //        Finalized = Finalizeddt.Rows[0]["finalized"].ToString();
        //        if (Finalized == "N")
        //        {
        //            Str = "UPDATE dbo.webx_BILLMST WITH(ROWLOCK) SET finalized ='Y'";
        //            Str = Str + " WHERE BillNo='" + OctroiBill + "';";
        //            Str = Str + " exec usp_octroibill_transaction '1','" + OctroiBill + "','" + Finyear + "','06','E'";

        //            dt = GF.GetDataTableFromSP(Str);
        //        }

        //    }
        //    else
        //    {
        //        string strcheck = "SELECT ISNULL(Finalized,'N') as Finalized FROM webx_oct_hdr WHERE OCBILLNO='" + OctroiBill + "'";
        //        Finalizeddt = GF.GetDataTableFromSP(strcheck);
        //        Finalized = Finalizeddt.Rows[0]["finalized"].ToString();
        //        if (Finalized == "N")
        //        {
        //            Str = "UPDATE dbo.webx_oct_hdr WITH(ROWLOCK) SET Finalized ='Y'";
        //            Str = Str + " WHERE OCBILLNO='" + OctroiBill + "';";
        //            Str = Str + " exec usp_OctroiAgentBILL_Transaction '1','" + OctroiBill + "','" + Finyear + "','07','E'";
        //        }
        //    }
        //    return Finalizeddt;
        //}

        #endregion

        #region View Print
        public string Tracking_Report_Name(string Type)
        {
            string sqlstr = "exec Tracking_Report_Name '" + Type + "'";

            string Report;
            try
            {
                Report = Convert.ToString(GF.executeScalerQuery(sqlstr));
            }
            catch (Exception)
            {
                Report = "DKT_Summary_TAB";
            }
            return Report;
        }
        public List<webx_pdchdr> GetPRSData(string PRSNo, string TYP)
        {
            string SQR = "";

            if (TYP == "PRS" || TYP == "1")
            {
                SQR = "SELECT PDCNO,PDCTY,CONVERT(varchar,PDCDT,6) AS PRSDate,Manualpdcno FROM webx_pdchdr with(nolock) WHERE PDCNO='" + PRSNo + "'";
            }
            else if (TYP == "DRS" || TYP == "2")
            {
                SQR = "SELECT PDCNO,PDCTY,CONVERT(varchar,PDCDT,6) AS PRSDate,Manualpdcno, PDCUpdated=ISNULL(PDCUpdated,'N') FROM webx_pdchdr with(nolock) WHERE PDCNO='" + PRSNo + "'";
            }
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_pdchdr> List = DataRowToObject.CreateListFromTable<webx_pdchdr>(Dt);
            return List;
        }
        #endregion
        public DataTable GetServiceTaxRate()
        {
            string sqlstr = "SELECT TOP 1 ISNULL(svctax_rate,0) AS servicetaxrate,ISNULL(edu_cess_rate,0) AS educessrate,ISNULL(hedu_cess_rate,0) AS heducessrate FROM webx_chargemst ";
            sqlstr = sqlstr + " WHERE '" + System.DateTime.Now.ToString("dd MMM yyyy") + "' BETWEEN startdate AND enddate";

            DataTable DT = GF.GetDataTableFromSP(sqlstr);
            return DT;
        }
        public DataTable Duplicate_ChqNO(string Chqno, string ChqDate)
        {
            DataTable DT = new DataTable();
            string Sql = "EXEC WEBX_Chq_Duplicate_Check '" + Chqno + "','" + ChqDate + "'";
            DT = GF.GetDataTableFromSP(Sql);

            return DT;
        }

        public DataTable DR_VR_Trn_Halt(string Brcd, string Trn_date, string FIN_Start, string fin_year, string Netamt)
        {
            DataTable DT = new DataTable();
            string Sql = "exec WEBX_DR_VR_Trn_Halt '" + Brcd + "','" + Trn_date + "','" + FIN_Start + "','" + fin_year + "','" + Netamt + "'";
            DT = GF.GetDataTableFromSP(Sql);
            return DT;
        }

        public List<webx_CUSTHDR> GetCustmer_Type(string CUSTCD, string CUSTType)
        {
            string QueryString = "exec USP_GetName '" + CUSTType + "','" + CUSTCD + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public List<webx_CUSTHDR> GetCustmer(string CUSTCD)
        {
            string QueryString = "select * from webx_CUSTHDR where CUSTCD='" + CUSTCD + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> webx_CUSTHDRList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return webx_CUSTHDRList;
        }

        public string ManualVouchersRule(string Ruletype)
        {
            double count = 0;
            string sql = "", RuleValue = "N";

            if (Ruletype.CompareTo("RoundOff") == 0)
            {
                sql = "select count(*) FROM webx_modules_rules where Module_Name='Manual Vouchers' and RULE_DESC='Decimal Values Y/N'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "select RULE_Y_N FROM webx_modules_rules where Module_Name='Manual Vouchers' and RULE_DESC='Decimal Values Y/N'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }
            if (Ruletype.CompareTo("EditableServiceTax") == 0)
            {
                sql = "select count(*) FROM webx_modules_rules where Module_Name='Manual Vouchers' and RULE_DESC='Editable ServiceTax Rate Y/N'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "select RULE_Y_N FROM webx_modules_rules where Module_Name='Manual Vouchers' and RULE_DESC='Editable ServiceTax Rate Y/N'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }
            if (Ruletype.CompareTo("CompanyDropDown") == 0)
            {
                sql = "SELECT count(*) FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Company Drop Down Y/N'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Company Drop Down Y/N'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }
            if (Ruletype.CompareTo("RowwiseSvcTax") == 0)
            {
                sql = "SELECT count(*) FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Ledger Wise Service Tax Y/N'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Ledger Wise Service Tax Y/N'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }
            if (Ruletype.CompareTo("Transwiserule") == 0)
            {
                sql = "SELECT count(*) FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Transaction Wise Account Category Y/N'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Transaction Wise Account Category Y/N'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }
            if (Ruletype.CompareTo("CostCenterRule") == 0)
            {
                sql = "SELECT count(*) FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Cost Center Wise Accounting'";
                count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
                if (count > 0)
                {
                    sql = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Manual Vouchers' and RULE_DESC='Cost Center Wise Accounting'";
                    RuleValue = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql).ToString();
                }
            }

            return RuleValue;
        }

        public DataTable FindOutSystemAccountCode(string CompanyCode, string BranchCode)
        {
            // BranchCode = "HQTR";
            double count = 0;
            string BrcdCheck = "N";
            count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, "select count(*) from webx_modules_rules where Module_Name='Account Master' and RULE_DESC='Set Branch Code For Account Head'"));
            if (count > 0)
            {
                BrcdCheck = SqlHelper.ExecuteScalar(Connstr, CommandType.Text, "select RULE_Y_N from webx_modules_rules where Module_Name='Account Master' and RULE_DESC='Set Branch Code For Account Head'").ToString();
            }
            string strSql = "";
            if (BrcdCheck.ToString() == "Y")
            {
                strSql = "select Acccode,Accdesc from webx_acctinfo where Activeflag='Y' and Acccode='" + CompanyCode + "' and (brcd like 'All' or PATINDEx ('%" + BranchCode.ToString() + "%',brcd)>0)";
            }
            else
            {
                strSql = "select Acccode,Accdesc from webx_acctinfo WITH(NOLOCK) where Activeflag='Y' AND Acccode='" + CompanyCode + "'";
            }
            DataTable dt = SqlHelper.ExecuteDataset(Connstr, CommandType.Text, strSql).Tables[0];

            return dt;
        }

        public Boolean CheckLocation(string LocCode)
        {
            double count = 0;
            string sql = "Select Count(*) from webx_location where LocCode='" + LocCode + "'";
            count = Convert.ToDouble(SqlHelper.ExecuteScalar(Connstr, CommandType.Text, sql));
            if (count > 0)
                return true;
            else
                return false;
        }

        public string InsertVoucherDetails(string Xml_Acccode_Details, string Xml_Other_Details, string xmlDocGSTCha, SqlTransaction trans)
        {
            //SqlParameter[] arParms = new SqlParameter[2];

            //arParms[0] = ControllersHelper.GetSqlParameter("@Xml_Acccode_Details", Xml_Acccode_Details.Replace("&", "&amp;").Trim(), SqlDbType.Text);
            //arParms[1] = ControllersHelper.GetSqlParameter("@Xml_Other_Details", Xml_Other_Details.Replace("&", "&amp;").Trim(), SqlDbType.Text);

            string GenVoucherno = "";
            string sql = "";// "exec usp_Generate_ManualAccountDetail_Ver4123";
            try
            {
                sql = "exec usp_Generate_ManualAccountDetail_Ver4GST '" + Xml_Acccode_Details.ReplaceSpecialCharacters() + "','" + Xml_Other_Details.ReplaceSpecialCharacters() + "','" + xmlDocGSTCha.ReplaceSpecialCharacters() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertVoucherDetails", "", "");
                GenVoucherno = GF.GetDataTableFromSP(sql).Rows[0][0].ToString();
                //GenVoucherno = SqlHelper.ExecuteScalar(trans, CommandType.StoredProcedure, sql).ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }

        public double VoucherDiff(string Voucherno, string Fin_Year, SqlTransaction trans)
        {
            string sql = "select sum(debit) - sum(credit) as Differance from webx_acctrans_" + Fin_Year + " where voucher_cancel='N' and voucherno='" + Voucherno + "'";
            double differance = 0;
            try
            {
                differance = Convert.ToDouble(SqlHelper.ExecuteScalar(trans, CommandType.Text, sql));
            }
            catch (Exception e)
            {
                throw e;
            }
            return differance;
        }

        public Boolean UpdateVendorBillScanDetail(string FileName, string Voucherno, SqlTransaction trans)
        {
            string sql = "UPDATE WEBX_VOUCHERTRANS_ARCH SET VEN_INV_FILE='" + FileName + "',VEN_INVDT=GETDATE() WHERE VOUCHERNO='" + Voucherno + "'";
            try
            {
                SqlHelper.ExecuteNonQuery(trans, CommandType.Text, sql);
            }
            catch (Exception e)
            {
                throw e;
            }

            return true;
        }

        public string NextVoucherno(string loccode, string finyear)
        {
            string NewCode = "";
            // SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["dbConnection"]);
            SqlConnection conn = new SqlConnection(Connstr);
            conn.Open();
            string sql = "usp_next_VoucherNo_Wo_Output";
            SqlCommand cmd = new SqlCommand(sql, conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@brcd", SqlDbType.VarChar).Value = loccode.Trim();
            cmd.Parameters.Add("@finyear", SqlDbType.VarChar).Value = finyear.Trim();
            cmd.Parameters.Add("@NextVoucherNo", SqlDbType.VarChar).Value = "";
            SqlDataReader dr = null;
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                NewCode = Convert.ToString(dr[0]);
            }

            if (NewCode == null) NewCode = "";
            return NewCode;
        }

        public List<webx_CUSTHDR> GetCustomerListFromSearch_BranchWise(string Serach, string Location)
        {
            string SQR = "exec USP_GetCustomer_BranchWise_List '" + Serach + "','1','" + Location + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);
            return GetCustomerVendorList;
        }

        public List<webx_CUSTHDR> GetCustomerUserVehicleDriverListFromSearch_BranchWise(string Serach, string Type, string Location)
        {
            string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal_BranchWise '" + Serach + "','1','" + Type + "','" + Location + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);

            return GetCustomerVendorList;
        }

        #region Edit Docket

        public string CheckEditDocketAvilable(string XMLDocket)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string @strXMLOutPut = "<root></root>";
                string SQLStr = "EXEC USP_DOCKET_EDIT_ELIGIBLE '" + XMLDocket + "','" + @strXMLOutPut + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message;// +"Please Enter Another Docket No.";
                return Message;
            }
        }

        public List<WebX_Master_Docket> Get_master_docket(string dockno)
        {
            string strsql = "SELECT dockdt as DOCKDT,dockno as DOCKNO,convert(varchar,dockdt,103) as ddmmyydockdt,party_name,";
            strsql = strsql + " party_code as PARTY_CODE,bacode,paybas as PAYBAS,orgncd AS ORGNCD,reassign_destcd AS DESTCD,company_code,";
            strsql = strsql + " party_as,stax_exmpt_yn,doctype as DOCTYP,insuyn AS risktype,editinfo,";
            strsql = strsql + "trn_mod AS TRN_MOD,LTRIM(RTRIM(service_class)) AS Service_Class,ftl_types AS ftltype,";
            strsql = strsql + "LTRIM(RTRIM(pickup_dely)) AS Pickup_Dely,from_loc AS from_loc,to_loc AS to_loc,pkgsty AS PKGSTY,";
            strsql = strsql + "prodcd AS PRODCD,spl_svc_req,cft_yn AS flagvolumetric,cod_dod AS flagcoddod,";
            strsql = strsql + "diplomat AS flagoda,localcn_yn AS flaglocal,loadtype,LTRIM(RTRIM(businesstype)) AS businesstype,dacc_yn AS flagdacc,";
            strsql = strsql + "permit_yn AS flagpermit,multipickup_yn AS flagmultipickup,industry,";
            strsql = strsql + "sourcedockno,multidelivery_yn AS flagmultidelivery,hday_appl_yn AS flaghdayadded,chrgwt as CHRGWT,ACTUWT,PKGSNO,fincmplbr,cdeldt as CDELDT,csgeaddr as CSGEADDR,CSGECD,CSGENM,CSGECity,CSGEPinCode,csgemobile,";
            strsql = strsql + "CSGNCD,CSGNNM,CSGNADDR,CSGNCity,CSGNPinCode,csgnmobile,";
            strsql = strsql + "insupl,insupldt,tot_modvat,tot_covers,ctr_no,privatemark,tpnumber";
            strsql = strsql + " FROM webx_master_docket a WHERE dockno='" + dockno + "' AND docksf='.'";

            DataTable Dt = GF.GetDataTableFromSP(strsql);
            List<WebX_Master_Docket> GetDocket = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(Dt);
            return GetDocket;
        }

        public List<WebX_Master_Docket_Invoice> GetDocketInvoice(string dockno)
        {
            string sqlstr = "SELECT srno,invno as INVNO,invdt AS INVDT,CONVERT(NUMERIC(12),declval) AS DECLVAL,";
            sqlstr = sqlstr + "CONVERT(NUMERIC(12),pkgsno) AS PKGSNO,CONVERT(NUMERIC(12),actuwt) AS ACTUWT,vol_l as VOL_L,";
            sqlstr = sqlstr + "vol_b as VOL_B,vol_h as VOL_H,tot_cft as vol_cft,Part_No FROM webx_master_docket_invoice WHERE dockno='" + dockno + "'";

            DataTable Dt = GF.GetDataTableFromSP(sqlstr);
            List<WebX_Master_Docket_Invoice> DocketInvoiceList = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Invoice>(Dt);
            return DocketInvoiceList;
        }

        public List<WebX_Master_Docket_Charges> GetFinEditStep4(string dockno)
        {
            string strsql = "SELECT edd='',billedat='',staxregno='',stax_paidby='',codamt=0,FOV as FOVCalculated,KKCessRate as KKCRate,ISNULL(KKCess,0) as KKCAmount,* FROM webx_master_docket_charges";
            strsql = strsql + " WHERE dockno='" + dockno + "'";
            DataTable dtcharge = GF.GetDataTableFromSP(strsql);

            strsql = "SELECT CONVERT(VARCHAR,cdeldt,103) AS edd,fincmplbr AS billedat,stax_regno AS staxregno,stax_paidby,";
            strsql = strsql + "codamt FROM webx_master_docket WHERE dockno='" + dockno + "' AND docksf='.'";
            DataRow dtrdocket = GF.GetDataTableFromSP(strsql).Rows[0];

            dtcharge.Rows[0]["codamt"] = dtrdocket["codamt"].ToString();
            dtcharge.Rows[0]["edd"] = dtrdocket["edd"].ToString();
            dtcharge.Rows[0]["billedat"] = dtrdocket["billedat"].ToString();
            dtcharge.Rows[0]["staxregno"] = dtrdocket["staxregno"].ToString();
            dtcharge.Rows[0]["stax_paidby"] = dtrdocket["stax_paidby"].ToString();

            List<WebX_Master_Docket_Charges> DocketChargeList = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Charges>(dtcharge);
            return DocketChargeList;
        }

        public List<WebX_Master_Docket_Charges> GetFinEditStep4DataTable(DataTable DT1, DataTable DT2)
        {
            DataTable dtcharge = DT1;
            DataRow dtrdocket = DT2.Rows[0];

            dtcharge.Rows[0]["codamt"] = dtrdocket["codamt"].ToString();
            dtcharge.Rows[0]["edd"] = dtrdocket["edd"].ToString();
            dtcharge.Rows[0]["billedat"] = dtrdocket["billedat"].ToString();
            dtcharge.Rows[0]["staxregno"] = dtrdocket["staxregno"].ToString();
            dtcharge.Rows[0]["stax_paidby"] = dtrdocket["stax_paidby"].ToString();

            List<WebX_Master_Docket_Charges> DocketChargeList = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Charges>(dtcharge);
            return DocketChargeList;
        }

        #endregion

        #region Edit THC
        //THC
        public List<webx_THC_SUMMARY> GetTHCEditList(string FromDate, string ToDate, string THCNo, string FinYear, string Type)
        {
            string SQLStr = "EXEC USP_THC_Financial_Edit_Data '" + FromDate + "','" + ToDate + "','" + THCNo + "','" + FinYear + "','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_THC_SUMMARY> GetTHCList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
            return GetTHCList;
        }
        //PDC
        public List<webx_THC_SUMMARY> GetPDCEditList(string FromDate, string ToDate, string THCNo, string FinYear, string Type, string BaseLocationCode)
        {
            string SQLStr = "EXEC USP_PDC_Financial_Edit_Data_NewPortal '" + FromDate + "','" + ToDate + "','" + THCNo + "','" + FinYear + "','" + Type + "','" + BaseLocationCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_THC_SUMMARY> GetTHCList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
            return GetTHCList;
        }
        //THC
        public List<webx_THC_SUMMARY> GetTHCEditDetail(string THCNo)
        {
            string SQLStr = "select thcno as THCNO,pcamt,ADVAMT,fincmplbr,balamtbrcd,FinStatus,routecd,Isnull(Financial_Edit,'') AS Financial_Edit,CONTTYP,CONTTYP_Name =Isnull((select CodeId+' : '+CodeDesc ";
            SQLStr = SQLStr + " from Webx_Master_General where CodeType='VCTYP' and CodeID=CONTTYP),'') from webx_THC_SUMMARY where thcno='" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_THC_SUMMARY> GetTHCList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
            return GetTHCList;
        }
        //PDC
        public List<webx_THC_SUMMARY> GetPDCEditDetail(string THCNo)
        {
            string SQLStr = "Select PDCNo as THCNO,fincmplbr,balamtbrcd,isnull(PDC_AMT,0) as pcamt,ISNULL(ADVAMT,0) as ADVAMT,isnull(BALAMT,0) as BALAMT,FinStatus,Isnull(Financial_Edit,'') AS Financial_Edit,";
            SQLStr = SQLStr + " CONTTYP,CONTTYP_Name =Isnull((select CodeId+' : '+CodeDesc from Webx_Master_General where CodeType='VCTYP' and CodeID=CONTTYP),''),vehno ";
            SQLStr = SQLStr + " from webx_PDCHDR where pdcno='" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_THC_SUMMARY> GetTHCList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
            return GetTHCList;
        }
        public string StandardContractAmount(string RouteCode)
        {
            string StandAmt = "0.00";
            string SQL_STDAMT = "select top 1 STDAMT=isnull(std_contamt,0.00) from webx_rutmas With(NOLOCK) where rutcd='" + RouteCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQL_STDAMT);
            if (Dt.Rows.Count > 0)
            {
                StandAmt = Dt.Rows[0]["STDAMT"].ToString();
            }

            return StandAmt;
        }
        public List<webx_THC_SUMMARY> GetTHCCharge(string THCNo)
        {
            string SQLStr = "EXEC WebxNet_THC_ChargeDET_Edit 'GE','" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_THC_SUMMARY> GetTHCList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
            return GetTHCList;
        }
        public List<webx_master_charge> GetchargeCodeList()
        {
            string SQLStr = "EXEC WebxNet_CLList_Of_FinDet 'GE'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<webx_master_charge> GetChargeList = DataRowToObject.CreateListFromTable<webx_master_charge>(Dt);
            return GetChargeList;
        }
        public string UpdateFINDET(string findetval, string THCNo)
        {
            string Str = "UPDATE dbo.Webx_THC_PDC_FIN_DET SET " + findetval;
            Str = Str + " WHERE DocNo='" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return "SUCESS";
        }
        //THC
        public string UpdateTHCSUMMARY(decimal Contamt, decimal Advamt, string Advamtpdat, string Balamtpdat, string HidEditedFiled, string THCNo)
        {
            string Str = "UPDATE dbo.webx_THC_SUMMARY SET pcamt='" + Contamt + "',ADVAMT='" + Advamt + "',fincmplbr='" + Advamtpdat + "',";
            Str = Str + " BalAmtBRCD='" + Balamtpdat + "',FinStatus=(CASE WHEN " + Advamt + " > 0 THEN 'GE' ELSE 'AD' END),Financial_Edit='" + HidEditedFiled + "'";
            Str = Str + " WHERE thcno='" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return "SUCESS";
        }
        //PDC
        public string UpdatePDCSUMMARY(decimal Contamt, decimal Advamt, string Advamtpdat, string Balamtpdat, string HidEditedFiled, string THCNo)
        {
            string Str = "UPDATE dbo.webx_PDCHDR SET PDC_AMT='" + Contamt + "',ADVAMT='" + Advamt + "',fincmplbr='" + Advamtpdat + "',";
            Str = Str + "BalAmtBRCD='" + Balamtpdat + "', FinStatus=(CASE WHEN " + Advamt + " > 0 THEN 'GE' ELSE 'AD' END),Financial_Edit='" + HidEditedFiled + "'";
            Str = Str + " WHERE PDCNo='" + THCNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);
            return "SUCESS";
        }
        #endregion

        #region PRS ARRIVAL

        public DataTable PRSArrivalList(string fromdt, string todt, string DocNo, string UnloadBy, string RateType, string BaseLocationCode)
        {
            string QueryString = "EXEC Usp_Get_PRSArrivalList '" + DocNo + "','" + fromdt + "','" + todt + "','" + UnloadBy + "','" + RateType + "','" + BaseLocationCode + "'";//"select PDCNO,PDCDT,vendorcode,vendorname,PDCBR,pdc_status from webx_PDCHDR where PDCNO='" + DocNo + "'and Convert(DATETIME,PDCDT,106) BETWEEN '" + fromdt + "' and '" + todt + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<PRSArrivalsDetailViewModel> GetPDCDetailsObject(string PDCNO, string RateType, string UnloadBy, string BaseLocationCode)
        {
            string QueryString = "exec USP_GetPDC_Details '" + PDCNO + "','" + RateType + "','" + UnloadBy + "','" + BaseLocationCode + "'";
            return DataRowToObject.CreateListFromTable<PRSArrivalsDetailViewModel>(GF.GetDataTableFromSP(QueryString));
        }

        public DataSet UpdatePRSArrival(string PRS_DETAILS, PRSArrivalsViewModel PRSAVM, string UserId)
        {
            if ((PRSAVM.PAVM.UnloadBy == "A" || PRSAVM.PAVM.UnloadBy == "XX5" || PRSAVM.PAVM.UnloadBy == "XX8"))
            {
                PRSAVM.PAVM.LoadingCharge = PRSAVM.PAVM.LoadingCharge;
            }
            else if (Convert.ToDecimal(PRSAVM.PAVM.LoadingCharge) > Convert.ToDecimal(PRSAVM.PAVM.MaxLimit))
            {
                PRSAVM.PAVM.LoadingCharge = PRSAVM.PAVM.MaxLimit;
            }
            //if (Convert.ToDecimal(PRSAVM.PAVM.LoadingCharge) > Convert.ToDecimal(PRSAVM.PAVM.MaxLimit))
            //{
            //    PRSAVM.PAVM.LoadingCharge = PRSAVM.PAVM.MaxLimit;
            //}
            string QueryString = "exec USP_UpdatePDC_Details '" + PRS_DETAILS + "','" + PRSAVM.PAVM.PDCNO + "','" + PRSAVM.PAVM.RateType + "','" + PRSAVM.PAVM.UnloadBy + "','" + PRSAVM.PAVM.LoadingCharge + "','" + PRSAVM.PAVM.Rate + "','" + PRSAVM.PAVM.ArrivalDT + "','" + UserId + "','" + PRSAVM.PAVM.VendorCode_new + "','" + PRSAVM.PAVM.VendorName_new + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "UpdatePRSArrival", "", "");

            return GF.GetDataSetFromSP(QueryString);
        }

        //public DataTable UpdatePRSArrival(PRSArrivalsViewModel PRSAVM)
        //{
        //    string QueryString = "exec USP_UpdatePDC_Details '" + PRSAVM.PAVM.PDCNO + "','" + PRSAVM.PAVM.RateType + "','" + PRSAVM.PAVM.UnloadBy 
        //        + "','" + PRSAVM.PAVM.LoadingCharge + "','" + PRSAVM.PAVM.Rate + "','" + PRSAVM.PAVM.VendorCode_new + "','" + PRSAVM.PAVM.VendorName_new + "'";
        //    int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "UpdatePRSArrival", "", "");
        //    return GF.GetDataTableFromSP(QueryString);

        //}
        #endregion

        #region GRN Generation

        public List<POGRNGenerationDetails> GetPOGRNGeneration(FuelBillEntyQuery FBE, string CompanyCode)
        {
            //string QueryString = "EXEC USP_DOCUMENT_FOR_GRN_GENERATION '" + FBE.VehicleNo + "','" + FBE.ManualPONo + "','" + FBE.VendorCode + "','" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.MatCat + "','" + BRCD.ToString() + "','" + CompanyCode.ToString() + "'";
            string QueryString = "EXEC USP_DOCUMENT_FOR_GRN_GENERATION '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.RegionCode + "','" + FBE.LocationCode + "','" + FBE.VehicleNo /*PO No*/ + "','" + FBE.ManualPONo + "" + "','" + FBE.VendorCode + "','" + FBE.MatCat + "','" + CompanyCode.ToString() + "','PO'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POGRNGenerationDetails> ItemList = DataRowToObject.CreateListFromTable<POGRNGenerationDetails>(DT);

            return ItemList;
        }

        public List<POGRNGenerationDetails> GetGRNGenerationDetails(string PONo)
        {
            string QueryString = "EXEC USP_GENERAL_PO_LIST_FOR_GRN_NewPortal '" + PONo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POGRNGenerationDetails> ItemList = DataRowToObject.CreateListFromTable<POGRNGenerationDetails>(DT);
            return ItemList;
        }

        public string CheckTyreNowithManufacturer(string TyreNo, string MFGID)
        {
            string QueryString = "EXEC USP_CheckTyreNowithManufacturer '" + TyreNo + "','" + MFGID + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT.Rows[0][0].ToString();
        }

        #endregion

        #region GRN ASSIGN ND REASSIGN
        public List<POGRNGenerationDetails> GetIteamCode()
        {
            string commandText = "select (ICode+':'+IName)As IteamName ,ICode from WEBX_GRN_DET where Assign ='Y'";
            DataTable dataTable = GF.getdatetablefromQuery(commandText);
            List<POGRNGenerationDetails> ListLocation = DataRowToObject.CreateListFromTable<POGRNGenerationDetails>(dataTable);

            return ListLocation;
        }
        public List<POBillEntry> GRNGenerationReAssignList(FuelBillEntyQuery FBE)
        {
            string SQRY = "exec Usp_GRN_GenerationReAssignList '" + FBE.DocumentNo + "','" + FBE.ITEMCODE + "','" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.LocCode + "'";
            DataTable Dt = GF.getdatetablefromQuery(SQRY);
            List<POBillEntry> ListLocation = DataRowToObject.CreateListFromTable<POBillEntry>(Dt);
            return ListLocation;
        }
        public DataTable InsertGRNGenerationReAssign(string XML1, string BaseLocationCode, string UserName, string Baseyear, string SKUID)
        {
            string SQRY = "exec Usp_Insert_GRNGenerationReAssign '" + XML1 + "','" + BaseLocationCode + "','" + UserName + "','" + Baseyear + "','" + SKUID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertGRNGenerationReAssign", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            //DataTable Dt = new DataTable();
            return Dt;
        }
        public List<POBillEntry> GetGRNGenerationList(FuelBillEntyQuery FBE)
        {
            string QueryString = "EXEC Usp_GRN_GenerationAssignList '" + FBE.VehicleNo + "', '" + FBE.VendorCode + "',  '" + GF.FormateDate(FBE.FromDate) + "','" + GF.FormateDate(FBE.ToDate) + "','" + FBE.Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<POBillEntry> ItemList = DataRowToObject.CreateListFromTable<POBillEntry>(DT);

            return ItemList;
        }
        public DataTable InsertGRNGenerationAssign(string XML1, string BaseLocationCode, string UserName, string Baseyear, string SKUID)
        {
            string SQRY = "exec Usp_Insert_GRNGenerationAssign '" + XML1 + "','" + BaseLocationCode + "','" + UserName + "','" + Baseyear + "','" + SKUID + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertGRNGenerationAssign", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public List<CYGNUS_FixAssetType> Get_GRNASSIGNREASSIGNVIEWPRINPrintListView(string DocumentNo, string FromDate, string ToDate, string TYPE)
        {
            string QueryString = "exec USP_GetGRNASSIGNREASSIGNVIEWPRINT '" + DocumentNo + "','" + FromDate + "','" + ToDate + "','" + TYPE + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_FixAssetType> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_FixAssetType>(DT);
            return ItemList;
        }
        #endregion

        #region PO Advance Payment

        public DataTable GetPOAdvancData(string PONo, string FromDate, string ToDate)
        {
            DataTable DT = new DataTable();
            string Sql = "EXEC USP_FixAsset_PO_AdvancePayment_List_Cygnus '" + FromDate + "','" + ToDate + "','" + PONo + "'";
            DT = GF.GetDataTableFromSP(Sql);
            return DT;
        }

        public DataTable ChecPONo(string PoNo)
        {
            string QueryString = "exec USP_CheckPONo_For_Advance_Payment_Cygnus '" + PoNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable sp_Insert_PurchseOrderDetail_For_Advance_Payment(string pocode, decimal PO_AMOUNT, decimal PO_advamt, decimal pendamt, decimal ChequeAmount, decimal CashAmount, string ChequeDate, string PaymentMode, string ChequeNo, string BankLedger, string CashLedger, string BaseUserName, string BaseFinYear, string BaseCompanyCode, string XML_Chq_Det)
        {
            string sql = "exec sp_Insert_PurchseOrderDetail_For_Advance_Payment_Cygnus '" + pocode + "','" + PO_AMOUNT + "','" + PO_advamt + "','" + pendamt + "','" + ChequeAmount + "','" + CashAmount + "','" + ChequeDate + "','" + PaymentMode + "','" + ChequeNo + "','" + BankLedger + "','" + CashLedger + "','" + BaseUserName + "','" + BaseFinYear + "','" + BaseCompanyCode + "','" + XML_Chq_Det + "'";
            GF.SaveRequestServices(sql.Replace("'", "''"), "sp_Insert_PurchseOrderDetail_For_Advance_Payment", "", "");
            return GF.GetDataTableFromSP(sql);
        }

        //public DataTable usp_chq_Details(string Xml)
        //{
        //    string SQR = "exec usp_chq_Details_Fixasset_PO '" + Xml + "'";
        //    int Id = GF.SaveRequestServices(SQR.Replace("'", "''"), "usp_chq_Details_Fixasset_PO ", "", "");
        //    DataTable Dt = GF.GetDataTableFromSP(SQR);
        //    return Dt;
        //}


        #endregion

        public DataTable GetPDCRatePerGram(string BranchCode, string VehicleType, string UserName, string PDCType)
        {
            string SQRY = "EXEC USP_GetPDCRatePerGram  '" + BranchCode + "','" + VehicleType + "','" + UserName + "','" + PDCType + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #region PRQ
        public DataTable GetPRQHistoryDT(string FromDate, string ToDate, string PRQNo, string CustCode, string Driver, string Super, string Status)
        {
            string QueryString = "exec [USP_GetStatusList] '" + FromDate + "','" + ToDate + "','" + PRQNo + "','" + CustCode + "','" + Driver + "','" + Super + "','','" + Status + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            //List<CYGNUSPRQHistory> itmList = DataRowToObject.CreateListFromTable<CYGNUSPRQHistory>(DT);
            return DT;
        }

        public DataTable SubmitPRQ(string XML, string FinYear, string UserName, string CompanyCode, string BRCD)
        {
            string SQRY = "exec USP_QUICK_DOCKET_ENTRY_NewPortal '" + XML + "','" + FinYear + "','" + UserName + "','" + CompanyCode + "','" + BRCD + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "SubmitPRQ", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion


        #region Docket Financial Edit

        public DataTable GetOriginStateCodeDocket(string Paybase, string ORGNCD, string PartyCode, string DestCD, string UserName, string Location)
        {
            string sqlstr = "EXEC USP_GetOrigineStateCode_docket '" + Paybase + "','" + ORGNCD + "','" + PartyCode + "','" + DestCD + "','" + UserName + "','" + Location + "'";
            DataTable DT = GF.GetDataTableFromSP(sqlstr);
            return DT;
        }

        #endregion

        #region Debit Note

        public List<webx_BILLMST> GetBillForDebitNote(string vendorCode)
        {
            string SQRY = "exec usp_GetListOfBill_Generate_DebitNote '" + vendorCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> BillList_XML = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt);
            return BillList_XML;
        }

        public string InsertDebitNoteDetails(string Xml_Mst_Details, string Xml_Det_Details, string xmlDocGSTCha)
        {
            string GenVoucherno = "";
            string sql = "";
            try
            {
                sql = "exec usp_CYGNUS_CRDR_Insert '" + Xml_Mst_Details.ReplaceSpecialCharacters() + "','" + Xml_Det_Details.ReplaceSpecialCharacters() + "','" + xmlDocGSTCha.ReplaceSpecialCharacters() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertDebitNoteDetails", "", "");
                GenVoucherno = GF.GetDataTableFromSP(sql).Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }

        #endregion

        #region Docket HO Verification

        public DataTable DocketHOVerificationSubmit(string DocumentNo, string BaseUserName)
        {
            string sql = "";
            try
            {
                sql = "EXEC USP_DocketHOVerify '" + DocumentNo + "','" + BaseUserName + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "DocketHOVerifyDetails", "", "");
                DataTable DT = GF.GetDataTableFromSP(sql);
                return DT;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public List<WebX_Master_Docket> GetDocketForHOVerification(BillingFilterViewModel model)
        {
            string SQRY = "EXEC USP_GetCustomerWiseDocketForVerification '" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + model.BillingParty + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Docket> DocketList = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(Dt1);
            return DocketList;
        }

        #endregion

        #region Internal Movement

        public DataTable Internal_Movement_Insert(string XML1, string BRCD, string UserName, string basefinyear)
        {
            string SQRY = "exec Usp_Internal_Movement_Insert '" + XML1 + "','" + BRCD + "','" + UserName + "','" + basefinyear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Internal_Movement", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Delivery MR

        public bool IsDocketEligibleForDelMR(string dockno, string docksf, string loccode, string finyear4d)
        {
            bool flag = false;
            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[4];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@dockno", dockno, SqlDbType.VarChar);
                paramsToStore[1] = ControllersHelper.GetSqlParameter("@docksf", docksf, SqlDbType.VarChar);
                paramsToStore[2] = ControllersHelper.GetSqlParameter("@loccode", loccode, SqlDbType.VarChar);
                paramsToStore[3] = ControllersHelper.GetSqlParameter("@finyear4d", finyear4d, SqlDbType.VarChar);

                flag = SYSConvert.ToBoolean(Convert.ToString(SqlHelper.ExecuteScalar(GF.GetConnstr(), CommandType.StoredProcedure, "USP_DOCKET_DELMR_ELIGIBLE", paramsToStore)));
            }
            catch (Exception excp)
            {
                throw excp;
            }

            return flag;

        }

        public List<vw_DeliveryMR> GetDeliveryMR_DocketDetails(string DockNo, string DockSf)
        {
            string QueryString = "exec Usp_GetDeliveryMR_DocketDetails '" + DockNo + "','" + DockSf + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<vw_DeliveryMR> itmList = DataRowToObject.CreateListFromTable<vw_DeliveryMR>(DT);
            return itmList;
        }

        public List<BookingCharges> GetBookingCharges(string Dockno, string ChargeRule, string ServiceType, string BusinessType, string DocType, decimal chrgwt)
        {
            string QueryDktString = "exec [Usp_GetBookingDocketCharges] '" + Dockno + "'";
            DataTable DT_Charge = GF.GetDataTableFromSP(QueryDktString);

            string QueryString = "exec [Usp_GetBookingCharges] '" + ChargeRule + "','" + ServiceType + "','" + BusinessType + "','" + DocType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);


            DataRow dtr = DT.NewRow();
            dtr["ChargeCode"] = "weight";
            dtr["ChargeName"] = "Weight";
            dtr["DocketCharge"] = chrgwt.ToString(); //hdnchargedweight.Value;
            dtr["ContractCharge"] = chrgwt.ToString(); //hdnchargedweight.Value;
            dtr["activeflag"] = "Y";
            dtr["Operator"] = "";
            DT.Rows.InsertAt(dtr, 0);

            dtr = DT.NewRow();
            dtr["ChargeCode"] = "frt_rate";
            dtr["ChargeName"] = "Freight Rate";
            dtr["DocketCharge"] = DT_Charge.Rows[0]["frt_rate"].ToString();
            //hdnratetype.Value = DT_Charge.Rows[0]["rate_type"].ToString();
            dtr["ContractCharge"] = "0";
            dtr["activeflag"] = "Y";
            dtr["Operator"] = "";
            DT.Rows.InsertAt(dtr, 1);

            dtr = DT.NewRow();
            dtr["ChargeCode"] = "freight";
            dtr["ChargeName"] = "Freight Charge";
            dtr["DocketCharge"] = DT_Charge.Rows[0]["freight"].ToString();
            dtr["ContractCharge"] = "0";
            dtr["activeflag"] = "Y";
            dtr["Operator"] = "+";
            DT.Rows.InsertAt(dtr, 2);

            for (int i = 3; i < DT.Rows.Count; i++)
            {
                DT.Rows[i]["DocketCharge"] = Convert.ToString(DT_Charge.Rows[0][Convert.ToString(DT.Rows[i]["ChargeCode"])]);
                DT.Rows[i]["ContractCharge"] = Convert.ToString(DT_Charge.Rows[0][Convert.ToString(DT.Rows[i]["ChargeCode"])]);
            }

            DT.Rows[1]["DocketCharge"] = DT_Charge.Rows[0]["frt_rate"].ToString();
            DT.Rows[2]["DocketCharge"] = DT_Charge.Rows[0]["freight"].ToString();
            DT.Rows[1]["ContractCharge"] = DT_Charge.Rows[0]["frt_rate"].ToString();
            DT.Rows[2]["ContractCharge"] = DT_Charge.Rows[0]["freight"].ToString();

            List<BookingCharges> itmList = DataRowToObject.CreateListFromTable<BookingCharges>(DT);
            return itmList;
        }

        public List<DeliveryCharges> GetDeliveryCharges(string Dockno, string ChargeRule, string ServiceType, string BusinessType, string DocType)
        {
            //string QueryDktString = "exec [Usp_GetBookingDocketCharges] '" + Dockno + "'";
            //DataTable DT_Charge = GF.GetDataTableFromSP(QueryDktString);

            string QueryString = "exec [Usp_GetDeliveryCharges] '" + ChargeRule + "','" + ServiceType + "','" + BusinessType + "','" + DocType + "'";
            DataTable DT_Charge = GF.GetDataTableFromSP(QueryString);

            DataRow dtr = DT_Charge.NewRow();
            dtr["ChargeCode"] = "frt_diff";
            dtr["ChargeName"] = "Freight Difference";
            //TextBox txtdiffsubtotal = (TextBox)grvcharges.FooterRow.FindControl("txtdiffsubtotal");
            dtr["ChargeAmount"] = "0"; //txtdiffsubtotal.Text.CompareTo("") == 0 ? "0" : txtdiffsubtotal.Text;
            dtr["activeflag"] = "Y";
            dtr["Operator"] = "";
            DT_Charge.Rows.InsertAt(dtr, 0);

            List<DeliveryCharges> itmList = DataRowToObject.CreateListFromTable<DeliveryCharges>(DT_Charge);
            return itmList;
        }

        public string GetChargeSubRule(string chargetype, string chargerule, string basecode)
        {

            string QueryString = "SELECT ISNULL(chargesubrule,'NONE') FROM WEBX_CHARGE_BASE ";
            QueryString = QueryString + "WHERE chargetype='" + chargetype + "' AND chargerule='" + chargerule + "' AND basecode='" + basecode + "'";
            string subrule = GF.executeScalerQuery(QueryString);
            if (subrule.CompareTo("") == 0)
                subrule = "NONE";
            return subrule;
        }

        public DataTable DeliveryMREntry(string strMR_Entry, string strXMLMRChargesEntry, string strXMLMRDiffEntry, string strXMLGatepassEntry, string strXMLBillEntry, string flagaccounting, string finyear)
        {
            string QueryString = " exec USP_DELMR_ENTRY_NewPortal"
                + "'" + strMR_Entry.Replace("&", "&amp;").Trim() + "'"
                + ",'" + strXMLMRChargesEntry.Replace("&", "&amp;").Trim() + "'"
                + ",'" + strXMLMRDiffEntry.Replace("&", "&amp;").Trim() + "'"
                + ",'" + strXMLGatepassEntry.Replace("&", "&amp;").Trim() + "'"
                + ",'" + strXMLBillEntry.Replace("&", "&amp;").Trim() + "'"
                + ",'" + flagaccounting + "' "
                + ",'" + finyear + "' ";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DeliveryMREntry", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion


        #region CrossingChallan

        public List<Webx_Crossing_Docket_Detail> GetCrossingChallan_DocketDetails(string DockNo, string FromDate, string ToDate, string Brcd, string Vendor)
        {
            string QueryString = "exec usp_dockets_for_Crossing_generation_NewPortal '" + DockNo + "','" + FromDate + "','" + ToDate + "','" + Brcd + "','','1','" + Vendor + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Crossing_Docket_Detail> itmList = DataRowToObject.CreateListFromTable<Webx_Crossing_Docket_Detail>(DT);
            return itmList;
        }
        public DataTable GetDetailCrossingvendorContract(string vendorcode, string Origin, string ToCity, string ratetype)
        {
            DataTable dt = new DataTable();
            string strsql = "Exec Usp_Get_rate_for_Crossing_vendorContract '" + vendorcode + "','" + Origin + "','" + ToCity + "','" + ratetype + "' ";
            dt = GF.GetDataTableFromSP(strsql);
            return dt;
        }
        public DataTable GetVendorName(string vendorcode)
        {
            DataTable dt = new DataTable();
            string strsql = "SELECT vendorname  FROM webx_vendor_hdr ";
            strsql = strsql + " WHERE vendorcode='" + vendorcode + "'";
            try
            {
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public DataTable CrossingChallanEntry(string CrossignChallan, string CrossignChallanDocket, string EntryBy, string CompanyCode)
        {
            string QueryString = "exec Usp_CrossingChallan_Entry_NewPortal '" + CrossignChallan + "','" + CrossignChallanDocket + "','" + EntryBy + "','" + CompanyCode + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "CrossingChallanEntry", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<Webx_Crossing_Docket_Master> GetCrossingChallanListForPouchUpload(string fromDate, string toDate, string thcNo, string brcd, string vendor)
        {
            string QueryString = "exec Usp_GetCrossingChallanListForPouchUpload '" + fromDate + "','" + toDate + "','" + thcNo + "','" + brcd + "','" + vendor + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Webx_Crossing_Docket_Master> itmList = DataRowToObject.CreateListFromTable<Webx_Crossing_Docket_Master>(DT);
            return itmList;
        }

        public DataTable CrossingChallanPouchUpload(string CrossignChallan, string EntryBy)
        {
            string QueryString = "exec Usp_CrossingChallanDocumentUpload '" + CrossignChallan + "','" + EntryBy + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "CrossingChallanDocumentUpload", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region IDTEntry
        public XmlDocument IDTDocketEntry(string strXMLMasterEntry, string strXMLChargesEntry, string strXMLDocumentEntry, string strXMLInvoiceEntry, string strXMLBCSerialEntry, string FinYear4d, SqlTransaction trn)
        {
            XmlDocument xdOutPut = new XmlDocument();
            string SQRY = "exec USP_IDTDOCKET_ENTRY '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + FinYear4d + "','<root></root>'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "IDTDocketEntry", "", "");

            try
            {
                SqlParameter[] paramsToStore = new SqlParameter[7];
                paramsToStore[0] = ControllersHelper.GetSqlParameter("@strXMLdocketMasterEntry", strXMLMasterEntry, SqlDbType.Xml);
                paramsToStore[1] = ControllersHelper.GetSqlParameter("@strXMLdocketChargesEntry", strXMLChargesEntry, SqlDbType.Xml);
                paramsToStore[2] = ControllersHelper.GetSqlParameter("@strXMLdocketDocumentEntry", strXMLDocumentEntry, SqlDbType.Xml);
                paramsToStore[3] = ControllersHelper.GetSqlParameter("@strXMLdocketInvoiceEntry", strXMLInvoiceEntry, SqlDbType.Xml);
                paramsToStore[4] = ControllersHelper.GetSqlParameter("@strXMLBCSerialEntry", strXMLBCSerialEntry, SqlDbType.Xml);
                paramsToStore[5] = ControllersHelper.GetSqlParameter("@finyear4d", FinYear4d, SqlDbType.NVarChar);
                paramsToStore[6] = ControllersHelper.GetSqlParameter("@strXMLOutPut", "<root></root>", SqlDbType.Xml, ParameterDirection.Output);

                try
                {
                    SqlHelper.ExecuteDataset(trn, CommandType.StoredProcedure, "USP_IDTDOCKET_ENTRY", paramsToStore);
                    xdOutPut.LoadXml(paramsToStore[6].Value.ToString());
                }
                catch (Exception excp)
                {
                    throw excp;
                }
            }
            catch (Exception excp)
            {
                throw excp;
            }
            return xdOutPut;
        }


        #endregion IDTEntry

        #region AutounloadingViewPrint
        public DataTable AutoUnloadingList(string THCNO, string fromdt, string todt, string UnLoadingNo, string Brcd)
        {
            string QueryString = "exec usp_GetAutoUnloadingList '" + THCNO + "','" + fromdt + "','" + todt + "','" + UnLoadingNo + "','" + Brcd + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion

        #region IDTFreightMemo
        public List<IDTDetail> GetIDTListForFreightMemo(IDTFreightMemoCriteria model)
        {
            model.ToDate = Convert.ToDateTime(model.ToDate.ToString("dd MMM yyyy") + " 23:59:59");
            string SQRY = "exec [Usp_getIDTListForFrieghtMemoGenerate] '" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + model.PartyCode + "','" + model.IDTNO + "','" + model.Brcd + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<IDTDetail> IDTList = DataRowToObject.CreateListFromTable<IDTDetail>(Dt1);
            return IDTList;
        }

        public DataTable GetOtherCharges(string PartyCode, string ChargeCode, string Origin, string Destination, decimal chargedweight, decimal Pkgs)
        {
            string QueryString = "exec USP_GET_IDTCHARGES '" + PartyCode + "','" + ChargeCode + "','" + Origin + "','" + Destination + "','" + chargedweight + "','" + Pkgs + "'";
            return GF.GetDataTableFromSP(QueryString);

        }

        public DataTable IDTFreightMemoSubmitProcess(string Xml_FreightMemoHdr, string XML_FreightMemodetail, string EntryBy, string Brcd, string Finyear, string CompanyCode)
        {
            string SQRY = "exec [Usp_IDTFreightMemoEntry] '" + Xml_FreightMemoHdr + "','" + XML_FreightMemodetail + "','" + EntryBy + "','" + Brcd + "','" + Finyear + "','" + CompanyCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "IDTFreightMemoEntry", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }
        public List<IDTFreightMemoViewModel> GetFMListForPayment(IDTFreightMemoCriteria model)
        {
            string SQRY = "exec [Usp_getFMListFroPayment] '" + model.FromDate.ToString("dd MMM yyyy") + "','" + model.ToDate.ToString("dd MMM yyyy") + "','" + model.PartyCode + "','" + model.Brcd + "','" + model.FMNO + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<IDTFreightMemoViewModel> FMList = DataRowToObject.CreateListFromTable<IDTFreightMemoViewModel>(Dt1);
            return FMList;
        }

        #endregion IDTFreightMemo

        #region SpecialcostEntry
        public string InsertSpecialCostVoucherDetails(string Xml_Acccode_Details, string Xml_Other_Details, string xmlDocGSTCha, SqlTransaction trans)
        {
            string GenVoucherno = "";
            string sql = "";
            try
            {
                sql = "exec usp_Generate_SpecialCostAccountDetail '" + Xml_Acccode_Details.ReplaceSpecialCharacters() + "','" + Xml_Other_Details.ReplaceSpecialCharacters() + "','" + xmlDocGSTCha.ReplaceSpecialCharacters() + "'";
                int id = GF.SaveRequestServices(sql.Replace("'", "''"), "InsertSpecialCostVoucherDetails", "", "");
                GenVoucherno = GF.GetDataTableFromSP(sql).Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }
        #endregion

        #region Cheque Register Details

        public DataTable USPGETCHEQUEDETAILS(string cheqno, string Cheqdate)
        {
            string SQRY = "EXEC USP_GET_CHEQUE_DETAILS '" + cheqno + "','" + Cheqdate + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CHQ_Register_Details_DET> GETCHEQLIST(string cheqno, string cheqdate)
        {
            string SQRY = "EXEC GET_CHEQ_LIST '" + cheqno + "','" + cheqdate + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CHQ_Register_Details_DET> ViewList = DataRowToObject.CreateListFromTable<CHQ_Register_Details_DET>(Dt);
            return ViewList;
        }

        #endregion

        #region CustomerPortal
        public DataTable CheckCustomerDocketNo(string dockno, string Username)
        {
            string SQR = "exec USP_CheckCustomerDocketNo '" + dockno + "','" + Username + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        #region Accident / Break Down Selection

        public List<THCCriteariaSelection> GetJobOrderViewList(string THCNO, string TCNO, DateTime FromDate, DateTime ToDate)
        {
            string SQLStr = "EXEC usp_THC_Departed_But_Not_Yet_Arrived'" + THCNO + "','" + TCNO + "','" + FromDate + "','" + ToDate + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQLStr);
            List<THCCriteariaSelection> GetTHCList1 = DataRowToObject.CreateListFromTable<THCCriteariaSelection>(Dt);
            return GetTHCList1;
        }

        public List<THCCriteariaSelection> Get_MFs_List(string THCNO)
        {
            string QueryString = "";
            QueryString = "exec usp_MF_Attached_To_THC_Departed_But_Not_Yet_Arrived '" + THCNO + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<THCCriteariaSelection> MFsList = DataRowToObject.CreateListFromTable<THCCriteariaSelection>(DT);
            return MFsList;
        }
        public DataTable ExecuteExceptionSubmit(string THCNO, string BaseLocationCode)
        {
            string QueryString = "";
            QueryString = "exec usp_Download_Stock_On_THC_Exception '" + THCNO + "','" + BaseLocationCode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        public DataTable GetPartyEmailIdFromDocket(string DockNo)
        {
            return GF.GetDataTableFromSP("exec Usp_GetPartyEmailIdFromDocket '" + DockNo + "'");
        }

        #region ClaimDetails
        public DataTable Insert_CIR_Claim(claims objPCMRequest1)
        {
            DataTable dt = new DataTable();
            string SQRY0 = "exec usp_CIR_ClaimInsert '" + objPCMRequest1.DOCKNO + "','" + objPCMRequest1.DOCKSF + "','" + objPCMRequest1.CIRNo + "','" + objPCMRequest1.CIRBR + "','" + objPCMRequest1.CIR_PREPBY + "','" + objPCMRequest1.BR_PREPNM + "','" + objPCMRequest1.LOCEMPCD + "','" + objPCMRequest1.LOCEMPNM + "','" + objPCMRequest1.NONDELY + "','" + objPCMRequest1.Short + "','" + objPCMRequest1.Damg + "','" + objPCMRequest1.Leakg + "','" + objPCMRequest1.pilfer + "','" + objPCMRequest1.Other + "','" + objPCMRequest1.COF + "','" + objPCMRequest1.clmpty + "','" + objPCMRequest1.clmptynm + "','" + objPCMRequest1.clmptyadd + "','" + objPCMRequest1.clmptypin + "','" + objPCMRequest1.clmptytel + "','" + objPCMRequest1.CIRDT + "','" + objPCMRequest1.CLMAMT + "','" + objPCMRequest1.CName + "','" + objPCMRequest1.CValue + "','" + objPCMRequest1.SName + "','" + objPCMRequest1.SValue + "','" + objPCMRequest1.SEName + "','" + objPCMRequest1.SEValue + "','" + objPCMRequest1.IncCoName + "','" + objPCMRequest1.IncPNo + "','" + objPCMRequest1.ChallanNo + "','" + objPCMRequest1.MBusiness + "','" + objPCMRequest1.Booking + "','" + objPCMRequest1.Delivery + "','" + objPCMRequest1.NBusiness + "','" + objPCMRequest1.ApprLoc + "','" + objPCMRequest1.BR_FLAG + "','" + objPCMRequest1.AO_FLAG + "','" + objPCMRequest1.HO_FLAG + "','" + objPCMRequest1.BRATTPCL + "','" + objPCMRequest1.BRATTPCL_FILENAME + "','" + objPCMRequest1.BRATTPIC + "','" + objPCMRequest1.BRATTPIC_FILENAME + "','" + objPCMRequest1.BRATTPOD + "','" + objPCMRequest1.BRATTPOD_FILENAME + "','" + objPCMRequest1.BRATTTCTHC + "','" + objPCMRequest1.BRATTTCTHC_FILENAME + "','" + objPCMRequest1.BRATTFIR + "','" + objPCMRequest1.BRATTFIR_FILENAME + "','" + objPCMRequest1.BRATTSRPT + "','" + objPCMRequest1.BRATTSRPT_FILENAME + "','" + objPCMRequest1.BRATTPHOTO + "','" + objPCMRequest1.BRATTPHOTO_FILENAME + "'";
            dt = GF.GetDataTableFromSP(SQRY0);
            return dt;
        }

        public DataTable GetCIRDocketDetail(string brcd)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_CIR_generate '" + brcd + "' ";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable GetDetailRecord(string DokectNo)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_CIRData_List '" + DokectNo + "' ";
            dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        public DataTable getDocketDetail(string DokectNo)
        {
            DataTable dt = new DataTable();
            string SQRY0 = "exec usp_getDocketNo '" + DokectNo + "'";
            dt = GF.GetDataTableFromSP(SQRY0);
            return dt;
        }

        public List<claims> ClaimListForMFGeneration(string FromDate, string ToDate, string DOCKNO)
        {
            DataTable dt = new DataTable();
            string SQRY = "exec usp_ViewClaims '" + FromDate + "','" + ToDate + "','" + DOCKNO + "' ";
            dt = GF.GetDataTableFromSP(SQRY);

            List<claims> objClaimsList = DataRowToObject.CreateListFromTable<claims>(dt);
            return objClaimsList;

        }

        public List<claims> ClaimApproval_List(string FromDate, string ToDate, string CIRNo, string DOCKNO)
        {
            DataTable dt = new DataTable();
            string SQRY0 = "exec usp_CIRClaimsApproval_Criteria_Ver1 '" + FromDate + "','" + ToDate + "','" + CIRNo + "','" + DOCKNO + "'";
            dt = GF.GetDataTableFromSP(SQRY0);
            List<claims> ClmApproval = DataRowToObject.CreateListFromTable<claims>(dt);
            return ClmApproval;
        }
        #endregion
    }
}