﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using Fleet.ViewModels;
using CYGNUS.Models;
using System.Xml;
using System.Web;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ExceptionsService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ExceptionsService.svc or ExceptionsService.svc.cs at the Solution Explorer and start debugging.
    public class ExceptionsService : IExceptionsService
    {
        GeneralFuncations GF = new GeneralFuncations();

        public void DoWork() { }

        #region MF/THC/DRS/PRS Cancellation

        public DataTable Get_Data_From_MFNo(string Serach)
        {
            string SQRY = "select TCDT,tcbr,manual_tcno,* from webx_TCHDR a with(nolock) Cross apply(select CNT=count(*) from webx_trans_docket_status  where GenerateByMF=a.tcno )b where   tcno='" + Serach + "' and CNT=0 and  THCNO is NULL and isnull(Cancelled,'N') !='Y'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable usp_Manifest_Do_Cancel(string thcno, string empcd)
        {
            string Sql = "EXEC [usp_Manifest_Do_Cancel] '" + thcno + "','" + empcd + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        public DataTable ListOFTHCCancellation(string BaseLocationCode, string FromDate, string ToDate, string THCNo)
        {
            string QueryString = "exec USP_THClist_for_Cancellation_NewPortal '" + BaseLocationCode + "','" + FromDate + "','" + ToDate + "','" + THCNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable usp_XML_THC_Cancellation(string xml, string empcd)
        {
            string Sql = "EXEC [usp_XML_THC_Cancellation] '" + xml + "','" + empcd + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        public DataTable ListOFPDCCancellation(string BaseLocationCode, string filterTyp, string FromDate, string ToDate, string THCNo)
        {
            string QueryString = "exec USP_PDClist_for_Cancellation '" + BaseLocationCode + "','" + filterTyp + "','" + FromDate + "','" + ToDate + "','" + THCNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        public DataTable usp_XML_PDC_Cancellation(string xml, string empcd, string FilterType)
        {
            string Sql = "EXEC [usp_XML_PDC_Cancellation] '" + xml + "','" + empcd + "','" + FilterType + "'";
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        #endregion

        #region Thc Financial Tracker

        public string CheckValidDOCNO(string DocType, string DocNo, string NumberType)
        {
            string QueryString = "";
            if (DocType == "THC")
            {
                if (NumberType == "1")
                {
                    QueryString = "SELECT count(*) FROM webx_thc_summary WHERE MANUALTHCNO='" + DocNo.Trim() + "'";
                }
                else
                {
                    QueryString = "SELECT count(*) FROM webx_thc_summary WHERE THCNO='" + DocNo.Trim() + "'";
                }
            }
            if (DocType == "PRS")
            {
                if (NumberType == "1")
                {
                    QueryString = "SELECT count(*) FROM webx_pdchdr WHERE Manualpdcno='" + DocNo.Trim() + "' and PDCTY='P' ";
                }
                else
                {
                    QueryString = "SELECT count(*) FROM webx_pdchdr WHERE PDCNO='" + DocNo.Trim() + "' and PDCTY='P'";
                }
            }
            if (DocType == "DRS")
            {
                if (NumberType == "1")
                {
                    QueryString = "SELECT count(*) FROM webx_pdchdr WHERE Manualpdcno='" + DocNo.Trim() + "' and PDCTY='D' ";
                }
                else
                {
                    QueryString = "SELECT count(*) FROM webx_pdchdr WHERE PDCNO='" + DocNo.Trim() + "' and PDCTY='D'";
                }
            }
            return GF.executeScalerQuery(QueryString);
        }

        #endregion

        #region PO Cancellation

        public List<POCancellation> GetWorkGroupObject(POCancellationCriteria POCCM)
        {
            string QueryString = "exec USP_POEntry_Listing_Ver1 '" + GF.FormateDate(POCCM.FromDate) + "','" + GF.FormateDate(POCCM.ToDate) + "','" + POCCM.PONo + "','" + POCCM.ManualPONo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<POCancellation> POCancellationList = DataRowToObject.CreateListFromTable<POCancellation>(dataTable);
            return POCancellationList;
        }

        public DataTable AddPOCancellation(string PONO, string POCANCELDATE, string Reason, string UserName)
        {
            string QueryString = "exec USP_POEntry_Cancellation '" + PONO + "','" + POCANCELDATE + "','" + Reason + "','" + UserName + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Edit DFM

        public List<vw_Edit_ForwardDocuments_HeaderDetails> GetDFMHaderObject(string FmNo)
        {
            string QueryString = "Select * from vw_Edit_ForwardDocuments_HeaderDetails where FM_No='" + FmNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<vw_Edit_ForwardDocuments_HeaderDetails> DFMList = DataRowToObject.CreateListFromTable<vw_Edit_ForwardDocuments_HeaderDetails>(dataTable);
            return DFMList;
        }

        public List<DFMLIST> GetDFMGridObject(string FmNo)
        {
            string QueryString = "exec usp_Edit_ForwardDocuments_GridData '" + FmNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<DFMLIST> DFMGridList = DataRowToObject.CreateListFromTable<DFMLIST>(dataTable);
            return DFMGridList;
        }

        public DataTable UpdateDFMInfo(DFMLISTViewModel DLVM, string UserName)
        {
            string QueryString = "exec usp_Update_Forwards_HeaderData '" + DLVM.VWFDHD.FM_No + "','" + DLVM.VWFDHD.Manual_FM_No + "','" + DLVM.VWFDHD.Courier_Code + "','" + DLVM.VWFDHD.Courier_Way_Bill_No + "','" + DLVM.VWFDHD.Courier_Way_Bill_Date + "','" + UserName + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Voucher Cancellation

        public DataTable VoucherCancellationList(string voucherno)
        {
            string SQRY = "exec USP_VocherList '" + voucherno + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable VoucherCancellationProcess(string BaseYearVal, string VoucherNo, string CDate, string BaseUserName, string cancel)
        {
            //string SQRY = "exec USP_VoucherCancellation '" + CDate + "', '" + Cancel + "','" + voucherno + "'";
            string SQRY = "exec usp_ManualVoucherCancellation_Ver1_NewPortal '" + BaseYearVal + "', '" + VoucherNo + "','" + CDate + "', '" + BaseUserName + "','" + cancel + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "VoucherCancellationProcess ", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public string GetVocherDate(string voucherno, string BaseFinYear)
        {
            string Message = "Done";
            try
            {
                string SQRY = "exec USP_VOUCHERNO_ELIGIBLE_FOR_CANCELLATION '" + voucherno + "','" + BaseFinYear + "'";
                DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
                return Message = "Done - " + Dt1.Rows[0][0];
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return Message;
            }
        }

        #endregion

        #region Bill Entry Cancellation

        public List<webx_BILLMST> BillCancellationList(string DATEFROM, string DATETO, string BILLNO, string Billtype)
        {
            string SQRY = "exec USP_BillEntry_Listing_Ver1 '" + DATEFROM + "', '" + DATETO + "','" + BILLNO + "','" + Billtype + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> MRList_Data = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt1);
            return MRList_Data;
        }
        public DataTable BillCancellationListsubmit(string BILLNO, string betype, string BGNDT, string CancelRession, string BaseUserName, string finyear)
        {
            string SQRY = "exec USP_BillEntry_Cancellation_NewPortal '" + BILLNO + "', '" + betype + "','" + BGNDT + "','" + CancelRession + "','" + BaseUserName + "','" + finyear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_BillEntry_Cancellation_NewPortal", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        #endregion

        #region ReAssign BillSubmissionn Collection

        public DataTable GetExistingLocation(string Billno)
        {
            string SQRY = "exec USP_GetBillLocationDetails '" + Billno + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable GetSubmissionocation(string locname)
        {
            string SQRY = "exec USP_GetLocationSubmission '" + locname + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable BillGenerated(string SubLocation, string NewLocation, string BillNO)
        {

            string SQRY = "exec USP_GetUpadtelocation '" + SubLocation + "', '" + NewLocation + "','" + BillNO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable BillSubmitted(string NewLocation, string BillNO)
        {

            string SQRY = "exec USP_GetUpadtelocation_Genration '" + NewLocation + "','" + BillNO + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region DEPT/Detention CNote

        public DataTable ListOFCnotDetention(string BaseLocationCode, string FromDate, string ToDate, string THCNO)
        {
            string sql;
            if (!string.IsNullOrEmpty(THCNO))
            {
                sql = "SELECT * FROM VW_DOCKETS_FOR_DETENTION WHERE dockno='" + THCNO + "' AND Curr_loc='" + BaseLocationCode + "'";
            }
            else
            {
                sql = "SELECT * FROM VW_DOCKETS_FOR_DETENTION WHERE CONVERT(VARCHAR,Dockdt,06) BETWEEN CONVERT(DATETIME,'" + FromDate + "',103) AND CONVERT(DATETIME,'" + ToDate + "',103) AND Curr_Loc='" + BaseLocationCode + "'";
            }
            DataTable DT = GF.GetDataTableFromSP(sql);
            return DT;
        }

        public List<Webx_Master_General> GetDetentionReason()
        {
            string QueryString = "SELECT codeid AS CodeId,codedesc AS CodeDesc FROM webx_master_general WHERE codetype='DTAIN' ORDER BY CodeDesc";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public DataTable Insert_Webx_Docket_Detained_History(string dockno, string docksf, DateTime DetainedSysdate, string BaseUserName, string BaseLocationCode, decimal Detained_On_days, string DetentionReason)
        {
            string sql1 = "INSERT INTO Webx_Docket_Detained_History ";
            sql1 = sql1 + " (DockNo,DockSF,DetainedOn,DetainedBy,DetainedBranch,Detained_On_days,DetainedSysdate,Action)";
            sql1 = sql1 + " VALUES ('" + dockno + "','" + docksf + "','" + DetainedSysdate.ToString("dd MMM yyyy") + "',";
            sql1 = sql1 + " '" + BaseUserName + "','" + BaseLocationCode + "'," + Detained_On_days + ",getdate(),'ADD')";
            DataTable Dt = GF.GetDataTableFromSP(sql1);

            string sql = "update WebX_Trans_Docket_Status set detained='Y',Detained_reason='" + DetentionReason + "',DetainedSysDate=getdate(), ";
            sql = sql + " DetainedOn ='" + DetainedSysdate.ToString("dd MMM yyyy") + "', DetainedBy='" + BaseLocationCode + "',detain_no_days=" + Detained_On_days + ",detained_at='" + BaseLocationCode + "' where DOCKNO='" + dockno + "' and DOCKSf='" + docksf + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(sql);
            return Dt;
        }

        #endregion

        #region DEPT/Remove CNote From Detention

        public DataTable ListOFRemoveCnotDetention(string BaseLocationCode, string FromDate, string ToDate, string THCNO)
        {
            string sql;

            string Rule_Value = "N";
            string Rule_Val = "";
            string Rule_Y_N = "N";
            string Qry = "";
            Qry = "SELECT Rule_Y_N,RULE_VALUE FROM webx_modules_rules WHERE RULEID='REMOVE_DKT_DETAN_ALLOWED' AND Rule_Y_N='Y'";
            DataTable dtRule = GF.GetDataTableFromSP(Qry);
            if (dtRule.Rows.Count > 0)
            {
                Rule_Y_N = dtRule.Rows[0]["Rule_Y_N"].ToString();
                Rule_Val = dtRule.Rows[0]["RULE_VALUE"].ToString();
                if (Rule_Y_N == "Y")
                {
                    Rule_Value = "Y";
                }
                else
                {
                    Rule_Value = "N";
                }
            }
            else
            {
                Rule_Value = "N";
            }

            if (!string.IsNullOrEmpty(THCNO))
            {
                sql = "select dockno+docksf AS Docket,detained_reason AS DetentionReason,dockno,docksf,dockdt AS docdt,orgncd,destcd,(orgncd+' - '+destcd) AS Origin_Dest,detained,";
                sql = sql + " detainedby=(Select UserID+' : '+Name FROM webx_master_users WHERE userid=detainedby),";
                sql = sql + " detained_at,CONVERT(VARCHAR,DetainedOn,6) AS DetainedOn,DetainedOn AS DetainDt FROM vw_Docket_DetainDetail WHERE ISNULL(cancelled,'N')='N' AND ISNULL(detained,'N')='Y' AND ";
                if (Rule_Y_N == "N")
                {
                    sql = sql + " Diff_days >=detain_no_days AND ";
                }
                if (Rule_Val == "2" && Rule_Y_N == "Y" && BaseLocationCode != "HQTR")
                {
                    sql = sql + " Curr_Loc='" + BaseLocationCode + "' AND ";
                }
                else if (Rule_Val == "1" && Rule_Y_N == "Y")
                {
                    //sql = sql + " Curr_Loc='HQTR' and ";
                    sql = sql + "'" + BaseLocationCode + "' ='HQTR' AND ";
                }
                else
                {
                    sql = sql + "";
                }
                sql = sql + " dockno='" + THCNO + "'";
            }
            else
            {
                sql = "SELECT dockno+docksf AS Docket,detained_reason AS DetentionReason,dockno,docksf,dockdt AS docdt,orgncd,destcd,(orgncd+' - '+destcd) AS Origin_Dest,detained,";
                sql = sql + " detainedby=(SELECT UserID+' : '+Name FROM webx_master_users WHERE userid=detainedby),";
                sql = sql + " detained_at, CONVERT(VARCHAR,DetainedOn,6) AS DetainedOn,DetainedOn AS DetainDt FROM vw_Docket_DetainDetail WHERE ISNULL(cancelled,'N')='N' AND ISNULL(detained,'N')='Y' AND ";
                if (Rule_Y_N == "N")
                {
                    sql = sql + " Diff_days >=detain_no_days AND ";
                }
                if (Rule_Val == "2" && Rule_Y_N == "Y" && BaseLocationCode != "HQTR")
                {
                    sql = sql + " Curr_Loc='" + BaseLocationCode + "' AND ";
                }
                else if (Rule_Val == "1" && Rule_Y_N == "Y")
                {
                    sql = sql + "'" + BaseLocationCode + "' ='HQTR' AND ";
                }
                else
                {
                    sql = sql + "";
                }
                sql = sql + " CONVERT(VARCHAR,Dockdt,6) BETWEEN CONVERT(DATETIME,'" + FromDate + "',6) AND ";
                sql = sql + " CONVERT(DATETIME,'" + ToDate + "',6) ";

            }
            DataTable DT = GF.GetDataTableFromSP(sql);
            return DT;
        }

        public List<Webx_Master_General> GetRemovalReason()
        {
            string QueryString = "select CodeId,CodeDesc from webx_master_general where CodeType='RDTAIN' order by CodeDesc";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public DataTable Update_Webx_Docket_Detained_History(string dockno, string docksf, DateTime DetainedSysdate, string BaseUserName, string BaseLocationCode, string DetentionReason, string RemovalReason)
        {
            string sql1 = "INSERT INTO Webx_Docket_Detained_History ";
            sql1 = sql1 + " (DockNo,DockSF,DetainedOn,DetainedBy,DetainedBranch,Detained_On_days,DetainedSysdate,Action)";
            sql1 = sql1 + " VALUES ('" + dockno + "','" + docksf + "','" + DetainedSysdate.ToString("dd MMM yyyy") + "',";
            sql1 = sql1 + " '" + BaseUserName + "','" + BaseLocationCode + "',0,getdate(),'REMOVE')";
            DataTable Dt = GF.GetDataTableFromSP(sql1);

            string sql = "update WebX_Trans_Docket_Status set detained='N',";
            sql = sql + "dkt_detain_remove_reason='" + RemovalReason + "',";
            sql = sql + "dkt_detain_remove_on='" + DetainedSysdate.ToString("dd MMM yyyy") + "',";
            sql = sql + "dkt_detain_remove_by='" + BaseUserName + "'";
            sql = sql + "where DOCKNO='" + dockno + "' AND DockSF='" + docksf + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(sql);
            return Dt;
        }

        #endregion

        #region Docket Cancellation

        public DataTable CheckValidDocket(string Id, string BaseLocationCode)
        {
            string sqlstr = "EXEC IsValiedDocket '" + Id + "','" + BaseLocationCode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            return dataTable;
        }
      
        public DataTable CheckDocketStatus(string DOCKNO, string FinstartYear, string FinYearEndDate)
        {
            string QueryString = "SELECT paybas,convert(varchar, dockdt, 103) AS dockdate FROM webx_master_docket WHERE dockno = '" + DOCKNO + "' AND dockdt BETWEEN  '" + FinstartYear + "' AND '" + FinYearEndDate + " 23:59:59'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable EditNoteCancellation(string CNote, string Reason, string UserName)
        {
            string QueryString = "UPDATE webx_trans_docket_status WITH(ROWLOCK) SET cancelled='Y',cancomment='" + Reason + "',cancelledon=getdate(),cancelledby='" + UserName + "' WHERE dockno='" + CNote + "'";
            QueryString = QueryString + "Select DOCKNO From webx_trans_docket_status where DOCKNO ='" + CNote + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable ListOFDocketCancellation(string baseLocationCode, string fromDate, string toDate, string cNoteNo)
        {
            string QueryString = "exec Usp_GetDocketListForCancellation '" + baseLocationCode + "','" + fromDate + "','" + toDate + "','" + cNoteNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }

        #endregion

        #region  Vendor Code  Change

        public List<webx_VENDOR_HDR> GetVendorChange(string VENDORTYPE, string DATEFROM, string DATETO, string VENDORCODE, string DOC_NO, string Year1, string SelectionType, string RO, string location)
        {
            string SQRY = "exec USP_THC_PDC_FOR_VendorCodeChange_NewPortal '" + VENDORTYPE + "', '" + DATEFROM + "','" + DATETO + "','" + VENDORCODE + "','" + DOC_NO + "','" + Year1 + "','" + SelectionType + "','" + RO + "','" + location + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> MRList_Data = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt1);
            return MRList_Data;
        }
        //public DataTable VendorupdateThc(string VendorCode, string VendorName, string DockNo)
        //{
        //    string SQRY = "exec USP_VendorCodeChangeThc '" + VendorCode + "', '" + VendorName + "','" + DockNo + "'";
        //    int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "VendorupdateThc", "", "");
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}
        //public DataTable VendorupdatePDC(string VendorCode, string VendorName, string DockNo)
        //{
        //    string SQRY = "exec USP_VendorCodeChangePDC '" + VendorCode + "', '" + VendorName + "','" + DockNo + "'";
        //    int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "VendorupdatePDC", "", "");
        //    DataTable Dt = GF.GetDataTableFromSP(SQRY);
        //    return Dt;
        //}
        public DataTable VendorupdateThc(string XML_Det, string DocType)
        {
            string SQRY = "exec USP_THC_VendorCodeChange '" + XML_Det + "','" + DocType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "VendorupdateThc", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable VendorNameGate(string VendorCode)
        {
            string SQRY = "select VENDORNAME,VENDORCODE from  webx_VENDOR_HDR where  VENDORCODE='" + VendorCode + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region  Balance Location Change

        public List<webx_VENDOR_HDR> GetVendorPayment(string VENDORTYPE, string DATEFROM, string DATETO, string VENDORCODE, string DOC_NO, string BaseFinYear, string SelectionType, string RO, string location)
        {
            string SQRY = "exec USP_THC_PDC_FOR_BalanceLocation '" + VENDORTYPE + "', '" + DATEFROM + "','" + DATETO + "','" + VENDORCODE + "','" + DOC_NO + "','" + BaseFinYear + "','" + SelectionType + "','" + RO + "','" + location + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> MRList_Data = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt1);
            return MRList_Data;
        }

        public DataTable Balanceupdate(string BalanceBrcd, string AdvanceBrcd, string DocNo, string BaseUserName)
        {
            string SQRY = "exec USP_BalanceLocationChange '" + BalanceBrcd + "', '" + AdvanceBrcd + "', '" + DocNo + "', '" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Balanceupdate", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetLocationDetails()
        {
            string SQRY = "exec USP_GetLocationDetails";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public DataTable BalanceupdatePDC(string BalanceBrcd, string AdvanceBrcd, string DocNo, string BaseUserName)
        {
            string SQRY = "exec USP_BalanceLocationChangePDC '" + BalanceBrcd + "','" + AdvanceBrcd + "','" + DocNo + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "BalanceupdatePDC", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<webx_VENDOR_HDR> GetPDCTHCDetailByDocNo(string THCNo, string DocType)
        {
            string SQRY = "exec USP_Get_THC_PDC_Details '" + THCNo + "', '" + DocType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_VENDOR_HDR> ItemList = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt1);
            return ItemList;
        }

        #endregion

        #region  Material HeldUp Reason

        public List<Webx_Master_General> GetDelayReason(string Type)
        {
            string SQRY = "exec USP_GetDelayReason ";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            string GeneralType = "";
            if (Type == "Booking Stock")
            {
                GeneralType = "BSDely";
            }
            else if (Type == "Delivery Stock")
            {
                GeneralType = "DSDely";
            }
            else if (Type == "Transshipment Stock")
            {
                GeneralType = "TSDely";
            }
            List<Webx_Master_General> MRList_Data = DataRowToObject.CreateListFromTable<Webx_Master_General>(Dt1).Where(c => c.CodeType == GeneralType).ToList();
            return MRList_Data;
        }

        public DataTable CheckValidDKTSeries(string dockno,string LocCode)
        {
            string sqlstr = "select stocktype as StockType,DKT_Status as DKT_STATUS,dockno,curr_loc from VWNET_WebX_Docket_status where stocktype is not null and Delivered='N' and docksf='.' and dockno ='" + dockno + "'and curr_loc='"+LocCode+"'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);
            return Dt;
        }

        public DataTable MaterialHeldUp(string dockno)
        {
            string sqlstr = "select stocktype as StockType,DKT_Status as DKT_STATUS,dockno,curr_loc from VWNET_WebX_Docket_status where stocktype is not null and Delivered='N' and docksf='.' and dockno ='" + dockno + "'";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);
            return Dt;
        }

        public DataTable InsertMaterialHeldUp(string xmlDoc1, string xmlDoc2)
        {
            string QueryString = "exec USP_DocketStockDelayReason_ENTRY '" + xmlDoc1 + "', '" + xmlDoc2 + "'";
            DataTable Dt = GF.GetDataTableFromSP(QueryString);
            return Dt;
        }

        #endregion

        #region Shortage and Damage Declaration

        public List<ShortageModel> DOCKETLIST_FOR_DEPS_MODULES(string Brcd, string FromDt, string ToDt, string Docketlist, string SelectionType, string RO, string LO)
        {
            string sql = " exec USP_DOCKETLIST_FOR_DEPS_MODULES '" + Brcd + "','" + FromDt + "','" + ToDt + "','" + Docketlist + "','" + SelectionType + "','" + RO + "','" + LO + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(sql);
            List<ShortageModel> MRList_Data = DataRowToObject.CreateListFromTable<ShortageModel>(Dt1);
            return MRList_Data;
        }

        public List<Webx_Master_General> GetShortageReason()
        {
            string QueryString = "SELECT  CodeId,CodeDesc FROM webx_master_general WHERE StatusCode = 'Y' AND CodeType='SREASON'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public List<Webx_Master_General> GetDamageReason()
        {
            string QueryString = "SELECT  CodeId,CodeDesc FROM webx_master_general WHERE StatusCode = 'Y' AND CodeType='DREASON'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public DataTable DAMAGE_SHORTAGE_UPDATE(string xml)
        {
            string sql1 = "exec USP_XML_DAMAGE_SHORTAGE_UPDATE '" + xml + "' ";
            DataTable Dt = GF.GetDataTableFromSP(sql1);
            return Dt;
        }

        #endregion

        #region CNote MisRoute

        public List<DocketMisRouteModel> Get_DocketMisRouteList(string strpo)
        {
            strpo = strpo.Replace(",", "','");
            string QueryString = "Select * from VWNet_Docket_Details_For_MisRoute where DockNo IN('" + strpo + "')";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<DocketMisRouteModel> ItemList = DataRowToObject.CreateListFromTable<DocketMisRouteModel>(DT);
            return ItemList;
        }

        public DataTable Insert_DocketMisRoute(string dockcode, string baselocation, decimal ArrivedPkgs, decimal ArrivedWt)
        {
            string str = "Insert into webx_trans_docket_status(DOCKNO,DOCKSF,Docket_Mode,Curr_Loc,ArrPkgQty,ArrWeightQty,SuffixFlow,Arrived,Arrived_DT,Arrived_LOC) values ";
            str = str + "('" + dockcode + "','M','F','" + baselocation + "'," + ArrivedPkgs + "," + ArrivedWt + ",'.-M','Y',getdate(),'" + baselocation + "')";
            DataTable Dt = GF.GetDataTableFromSP(str);
            return Dt;
        }

        #endregion

        #region CNote Reassign Location

        public List<VWNET_WebX_Docket_status> GetCnoteList(CNoteReassignFilter CRF)
        {
            string QueryString = "EXEC webx_reassign_location'" + (CRF.Loccode == "" ? CRF.Loccode = "ALL" : CRF.Loccode) + "','" + (CRF.LocLevel == null ? CRF.LocLevel = 1 : CRF.LocLevel) + "','" + GF.FormateDate(CRF.FromDate) + "','" + GF.FormateDate(CRF.ToDate) + "','" + CRF.DT_TYPE + "','" + CRF.CnoteNo + "','" + 1 + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<VWNET_WebX_Docket_status> ItemList = DataRowToObject.CreateListFromTable<VWNET_WebX_Docket_status>(dataTable);
            return ItemList;
        }

        public DataTable CNoteReassignLocation(string Id, string LocCode, string CSGEPinCode, string DestStateName, string Destination_Area, string ToCity, string BaseUserName)
        {
            var flag = "Y"; 
            string QueryString = "EXEC USP_BACKUP_DOCKET '" + Id + "', '" + BaseUserName.ToUpper() + "'" +
                " UPDATE webx_master_docket SET reassign_destcd='" + LocCode.Trim() + "',CSGEPinCode='" + CSGEPinCode + "'," +
                " DestStateName='" + DestStateName + "',Destination_Area='" + Destination_Area + "',to_loc='" + ToCity + "'," +
                " reassignDate=GETDATE(),reassignBy='" + BaseUserName + "',UpdateComments=ISNULL(UpdateComments,'')+' Update From Reassign Destination Module' WHERE dockno='" + Id.Trim() + "'";
            QueryString = QueryString + "UPDATE webx_trans_docket_status set reassignYN='" + flag.Trim() + "' WHERE dockno='" + Id.Trim() + "'";

            int Ids = GF.SaveRequestServices(QueryString.Replace("'", "''"), "CNoteReassignLocation", "", "");

            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Tripsheet Advance Edit

        public DataTable GetTSAdvanceEditList(string Fromdt, string Todt, string TripSheetNo, string Flag, string location)
        {
            string SQR = "exec usp_VehicleIssueList_forEdit '" + TripSheetNo + "','" + Fromdt + "','" + Todt + "','" + Flag + "','" + location + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        public DataSet GetTSAdvanceEdit(string TripSheetNo, string location, string UserName)
        {
            string SQR = "exec Usp_Getwebx_fleet_vehicle_issue '" + TripSheetNo + "','" + location + "','" + UserName + "'";
            return GF.GetDataSetFromSP(SQR);
        }

        public DataTable UpdateTripsheetAdvanceEdit(string TripsheetId, string DatesXMl, string PaymentXML, string TSXML, string userName, string finYear, string locationCode, string CompanyCode, string YearSuffix)
        {
            string SQR = "exec Usp_Update_Advance_Edit '" + TripsheetId + "','" + DatesXMl + "','" + PaymentXML + "','" + TSXML + "','" + userName + "','" + finYear + "','" + locationCode + "','" + CompanyCode + "','" + YearSuffix + "'";
            //GF.SaveCRMXML(SQR.Replace("'", "''"), PartitionId.ToString(), "", "Partition", Type, false, "");
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion

        #region Tripsheet Financial Edit

        public DataTable GetTSFinancialEditList(string TripSheetNo, string Fromdt, string Todt, string UpdClose, string BranchCode, string Flag, string CompanyCode)
        {
            string SQR = "exec usp_New_Trip_VehicleIssueList_ver3 '" + TripSheetNo + "','" + Fromdt + "','" + Todt + "','" + UpdClose + "','" + BranchCode + "','" + Flag + "','" + CompanyCode + "'";
            return GF.GetDataTableFromSP(SQR);
        }

        public DataSet GetTSFinanceEdit(string TripSheetNo, string mode, string location, string UserName)
        {
            string SQR = "exec Usp_Getwebx_Financial_TripSheet '" + TripSheetNo + "','" + mode + "','" + location + "','" + UserName + "'";
            return GF.GetDataSetFromSP(SQR);
        }

        public DataTable Operationally_Financial_Edit_Close(string XML, string XML1, string XML2, string XML3, string TripsheetNo)
        {
            string SQR = "exec Usp_Operationally_Financial_Edit_TripClose '" + XML + "','" + XML1 + "','" + XML2 + "','" + XML3 + "','" + TripsheetNo + "'";
            int id = GF.SaveRequestServices(SQR.Replace("'", "''"), "Operationally_Financial_Close", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            return Dt;
        }

        #endregion

        #region TripSheet Advance Voucher Cancle

        public DataTable TripAdvanceVoucherCancellation(int transNo, string finyear, DateTime Canceldt, string comment, string voucherno, string YearSuffix, string BaseUserName, string BaseCompanyCode)
        {
            string SQRY = "exec usp_Fleet_TripAdvance_Cancellation '" + voucherno + "','" + transNo + "', '" + finyear + "','" + YearSuffix + "','" + Canceldt + "','" + BaseUserName + "','" + comment + "','" + BaseCompanyCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable GetValidateAdvanceVoucherDate(string SerchGroup, string YearSuffix, string CompanyCode)
        {
            string SQRY = "exec USP_Get_Validate_AdvanceVocher '" + SerchGroup + "','" + YearSuffix + "','" + CompanyCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
         }

        #endregion

        #region ADVICE Edit/Cancellation

        public DataTable GETAdviceDateDetails(string Adviceno)
        {
            string SQRY = "exec USP_GetAdviceDetail '" + Adviceno + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable SetEditandCancelAdvice(string Operation, string AdviceNo, string RaisedOn, string CancleDate, string CancleReason, string UserName, string FinYear)
        {
            string SQRY = "exec USP_EditandCancleAdvice '" + Operation + "','" + AdviceNo + "','" + RaisedOn + "','" + CancleDate + "','" + CancleReason + "','" + UserName + "','" + FinYear + "'";
            DataTable dt = GF.GetDataTableFromSP(SQRY);
            return dt;
        }

        #endregion

        #region Bill Cancel

        public List<Webx_Master_General> GetBillTypeList()
        {
            string QueryString = " select CodeId,CodeDesc from webx_master_general where codetype='billtyp' and statuscode='Y' and Codeid in('2','7','6')";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> ItemList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return ItemList;
        }

        public List<webx_CUSTHDR> GETBILLCANCELCUST(string Prefix)
        {
            string QueryString = "exec USP_GETBILLCANCELCUST '" + Prefix + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> itmList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(DT);
            return itmList;
        }

        public List<webx_BILLMST> BillCancelList(string DATEFROM, string DATETO, string BillNO, string ManualNO, string Custcd, string Billtype, string BaseFinYear)
        {
            string SQRY = "exec USP_Bill_Cancellation_List_Ver1_New_Portal '" + DATEFROM + "', '" + DATETO + "','" + BillNO + "','" + ManualNO + "','" + Custcd + "','" + Billtype + "','" + BaseFinYear + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<webx_BILLMST> MRList_Data = DataRowToObject.CreateListFromTable<webx_BILLMST>(Dt1);
            return MRList_Data;
        }

        public DataTable BillCancelListsubmit(string BILLNO, string BILLTYPE, string BGNDT, string CancelRession, string BaseUserName, string Finalized, string AcctReversal, string finyear)
        {
            string SQRY = "exec USP_Bill_Cancellation_Ver1_NewPortal '" + "Bill" + "','" + BILLNO + "', '" + BILLTYPE + "','" + BGNDT + "','" + CancelRession + "','" + BaseUserName + "','" + Finalized + "','" + AcctReversal + "','" + finyear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Bill_Cancellation_Ver1", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Check_Finalized_Status()
        {
            string QueryString = "SELECT count(*) as count FROM WEBX_MODULES_RULES WHERE Module_Name='Bill Cancellation' and RULEID='Check_Finalized_Status'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable Check_Finalized_Status_1()
        {
            string QueryString = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Bill Cancellation' and RULEID='Check_Finalized_Status'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable Bill_Reversal_Accounting()
        {
            string QueryString = "select count(*) as count from webx_modules_rules where Module_Name='Bill Cancellation' and RULEID='Bill_Reversal_Accounting'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable Bill_Reversal_Accounting_1()
        {
            string QueryString = "select RULE_Y_N from webx_modules_rules where Module_Name='Bill Cancellation' and RULEID='Bill_Reversal_Accounting'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        #endregion

        #region CNote Non Delivery reason update

        public List<WebX_Master_Docket> GetCnoteNonDeliveryList(CNoteNonDeliveryCriteria criteria)
        {
            string FDate = criteria.FromDate.ToString("dd MMM yyyy");
            string TDate = criteria.ToDate.ToString("dd MMM yyyy");
            string QRY = "exec USP_GetCNoteNonDeliveryList '" + criteria.CNoteNo + "','" + FDate + "','" + TDate + "','" + criteria.NoofDays.ToString() + "'";
            DataTable dt = GF.GetDataTableFromSP(QRY);
            List<WebX_Master_Docket> ItemList = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(dt);
            return ItemList;
        }

        public List<SubReasons> NonDeliverySubReasonList()
        {
            string QRY = "exec USP_GEtDocketNonDeliverySubReasonList";
            DataTable dt = GF.GetDataTableFromSP(QRY);
            List<SubReasons> ItemList = DataRowToObject.CreateListFromTable<SubReasons>(dt);
            return ItemList;
        }

        public DataSet SubmitNonDeliveryReasonUpdate(string XMLDocket)
        {
            string QRY = "EXEC USP_DOCKET_NON_DELIVERY '" + XMLDocket + "','',''";
            DataSet ds = GF.GetDataSetFromSP(QRY);
            return ds;
        }

        #endregion

        #region Service Tax Removal

        public string GetValid_Dockno_NO(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_DOCKET_ELIGIBLE_FOR_REMOVE_STAX_New_Portal '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public DataTable GetRemove_Service_Tax(string DocketNo, string BaseUsername)
        {
            string SQRY = "exec usp_Remove_Service_Tax_Ver1_NewPortal '" + DocketNo + "','','" + BaseUsername + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Octroi Bill Vendor Code Change

        public DataTable Get_Valid_Bill(string BillNo, string Location, string UserName, string FinYear)
        {
            string SQRY = "exec USP_Octroi_Agent_Bill_VendorCode_Valid '" + BillNo + "','" + Location + "','" + UserName + "','" + FinYear + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public DataTable OctroiBill_Submit_VendorChange(string BillNo, string Location, string UserName, string FinYear, string VendorCode)
        {
            string SQRY = "exec USP_Octroi_Agent_Bill_VendorCode_Change '" + BillNo + "','" + Location + "','" + UserName + "','" + FinYear + "','" + VendorCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "OctroiBill_Submit_VendorChange", "", "");
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        #endregion

        #region GRN Cancellation

        public DataTable ChecGRNNo(string GRNNo)
        {
            string QueryString = "exec USP_CheckGRNNo '" + @GRNNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }

        public DataTable GRNCancellation(string GRNNo, string POCANCELDATE, string Reason, string UserName)
        {
            string QueryString = "exec USP_GRNEntry_Cancellation '" + GRNNo + "','" + POCANCELDATE + "','" + Reason + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "GRNCancellation", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Doket Booking Exception

        public string Get_Dockno_NO(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_DOCKET_DoketBookingException '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public List<WebX_Master_Docket> USP_DOCKET_GETList(string DocketNo)
        {
            string SQRY = "exec USP_DOCKET_GETList '" + DocketNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Docket> MRList_Data = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(Dt1);
            return MRList_Data;
        }

        public DataTable DOCKET_ListSubmit(string DOCKNO, string SDD_Date, string BaseUserName)
        {
            string SQRY = "exec USP_Doket_Booking_Submit '" + DOCKNO + "','" + SDD_Date + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Doket_Booking_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Doket DashBoard Calander

        public List<CalenderEvent> GetCallingEvent()
        {
            string SQRY = "exec Usp_DocketEvent";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CalenderEvent> MRList_Data = DataRowToObject.CreateListFromTable<CalenderEvent>(Dt);
            return MRList_Data;
        }

        #endregion

        #region Doket Booking Exception

        public string Get_Dockno_NO_EDD_Date_Change(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_DOCKET_DoketBookingExceptionEDD_Validate '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message;// +"--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public DataTable DOCKET_List_EDD_Chnage_Submit(string DOCKNO, string EDD_Date, string BaseUserName)
        {
            string SQRY = "exec USP_Doket_Booking_EDD_Chnage_Submit '" + DOCKNO + "','" + EDD_Date + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Doket_Booking_EDD_Chnage_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region DACC AND COD/DOD Customer AUTO Mail

        public string Get_Dockno_NO_Mail(string DocketNo, string Type)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_Customer_generate_mail '" + DocketNo + "','" + Type + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public List<WebX_Master_Docket> Get_DOCKET_GETList_Mail(string DocketNo)
        {
            string SQRY = "exec USP_GETList_Generate_Mail '" + DocketNo + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WebX_Master_Docket> MRList_Data = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(Dt1);
            return MRList_Data;
        }

        public DataTable Customergenerate_ListSubmit(string DOCKNO, decimal COD, decimal DACC, string BaseUserName, string CDELDT, string FinYearStartDate, string BaseLocation)
        {
            string SQRY = "exec USP_Customergenerate_Submit '" + DOCKNO + "','" + COD + "','" + DACC + "','" + BaseUserName + "','" + CDELDT + "','" + FinYearStartDate + "','" + BaseLocation + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Customergenerate_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable SQL_DACC_COD_DOD_Mail(string strBody, string MailId1, string MailId2, string MailId3)
        {
            DataTable dt = new DataTable();
            string SQLStr = "EXEC USP_SQL_DACC_COD_DOD_Mail '" + strBody + "','" + MailId1 + "','" + MailId2 + "','" + MailId3 + "'";
            dt = GF.GetDataTableFromSP(SQLStr);
            return dt;
        }

        #endregion

        #region  DACC COD/DOD Cancellation Module

        public DataTable DACC_COD_DOD_ListSubmit(string DOCKNO, string BaseUserName, string status)
        {
            string SQRY = "exec USP_DACC_COD_DOD_Submit '" + DOCKNO + "','" + BaseUserName + "','" + status + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_DACC_COD_DOD_Submit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region  Docket Status Update

        public string Get_Dockno_NO_Status(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_Dockno_NO_Status_Check '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public DataTable Get_Dockno_NO_StatusSubmit(string CnoteNo, string DOCKET_DELY_REASON, string Reason, string UserName)
        {
            string SQRY = "exec USP_Dockno_NO_StatusSubmit '" + CnoteNo + "','" + DOCKET_DELY_REASON + "','" + Reason + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "USP_Dockno_NO_StatusSubmit", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion
        
        #region  View Print Module

        public string Get_Dockno_NO_Report(string DocketNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_Check_DocketNo_Report '" + DocketNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another Docket No.";
                return Message;
            }
        }

        public DataTable Get_DocktNo_From_TicketNo(string TicketNo)
        {
            string SQRY = "exec USP_Get_DocktNo_From_Ticket '" + TicketNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Get_DockeNo_From_ReferenceNo(string Reference)
        {
            string SQRY = "exec USP_Get_DockeNo_From_ReferenceNo '" + Reference + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region TripSheet Cancellation

        public string Get_TripSheet_NO_Status(string VSlipNo)
        {
            string Message = "Done";
            try
            {
                DataTable dt = new DataTable();
                string SQLStr = "EXEC USP_TripSheet_NO_Status_Check '" + VSlipNo + "'";
                dt = GF.GetDataTableFromSP(SQLStr);
                Message = "Done";
                return Message;
            }
            catch (Exception ex)
            {
                Message = ex.Message + "--" + "Please Enter Another TripSheet No.";
                return Message;
            }
        }

        public DataTable TripSheetCancellation_StatusSubmit(string VSlipNo, string Cancel_Remarks, string BaseUserName, string BaseFinYear)
        {
            string SQRY = "exec USP_TripSheetCancellation_StatusSubmit '" + VSlipNo + "','" + Cancel_Remarks + "','" + BaseUserName + "' ,'" + BaseFinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "TripSheetCancellation", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Internal Movement View Print

        public List<CYGNUS_Internal_Movement> Get_InternalMovementList(string FromDate, string ToDate, string IMNo)
        {
            string QueryString = "";

            QueryString = "exec Usp_Get_Internal_Movement_Date_For_View_Print '" + FromDate + "','" + ToDate + "','" + IMNo + "' ";

            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Internal_Movement> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_Internal_Movement>(DT);
            return ItemList;
        }

        #endregion

        #region Reassign BA Code
        public List<webx_VENDOR_HDR> GetBACode()
        {
            string Squery = "EXEC USp_GetBACode";
            DataTable Dt = GF.GetDataTableFromSP(Squery);
            List<webx_VENDOR_HDR> listBACode = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
            return listBACode;
        }
        #endregion
        
        #region Cnote Fields Update
        public DataTable UpdateCnoteFields(string xmlFieldsDet, string BaseUserName)
        {
            string Squery = "EXEC Usp_UpdateCnoteFields '" + xmlFieldsDet + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(Squery.Replace("'", "''"), "UpdateCnoteFields", "", "");
            DataTable dt = GF.GetDataTableFromSP(Squery);
            return dt;
        }

        #endregion

        #region Crossing Challan Cancellation
        public DataTable Get_Crossin_Challan_Data_HQTR(string ChallanNo)
        {
            string SQRY = "select CrossingChallanNo,Convert(Varchar,ChallanDate,06) as ChallanDate_str,ChallanLocCode,(VendorCode + ':' + VendorName) as VendorName,TotalToPay,TotalCrossing,TotalDoorDelyChrg,CAST(TotalBulky AS VARCHAR) AS TotalBulky,NetPayable,PaymentFlag,Cancel from Webx_Crossing_Docket_Master with(nolock) where isNULL(Cancel, 'N') = 'N' and isNULL(PaymentFlag,'N')= 'N' and CrossingChallanNo = '" + ChallanNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable Get_Crossin_Challan_Data_NotHQTR(string ChallanNo, string BaseLocationCode)
        {
            string SQRY = "select CrossingChallanNo,Convert(Varchar,ChallanDate,06) as ChallanDate_str,ChallanLocCode,(VendorCode + ':' + VendorName) as VendorName,TotalToPay,TotalCrossing,TotalDoorDelyChrg,CAST(TotalBulky AS VARCHAR) AS TotalBulky,NetPayable,PaymentFlag,Cancel from Webx_Crossing_Docket_Master with(nolock) where isNULL(Cancel, 'N') = 'N' and isNULL(PaymentFlag,'N')= 'N' and ChallanLocCode = '" + BaseLocationCode + "' and CrossingChallanNo = '" + ChallanNo + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable Usp_Cancel_CrossingChallan(string thcno, string empcd, string Finyear)
        {
            string Sql = "EXEC [Usp_Cancel_CrossingChallan] '" + thcno + "','" + empcd + "','" + Finyear + "'";
            int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "CancelCrossingChallan", "", "");
            DataTable Dt = GF.GetDataTableFromSP(Sql);
            return Dt;
        }

        #endregion

        #region IDT Cancellation
        public DataTable ChecIDTNo(string IDTNo)
        {
            string QueryString = "exec USP_CheckIDTNo '" + IDTNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }
        public DataTable CheckStatus(string IDTNo, string FinstartYear, string FinYearEndDate)
        {
            string QueryString = "SELECT paybas,convert(varchar, dockdt, 103) AS dockdate FROM webx_master_docket WHERE dockno = '" + IDTNo + "' AND DOCTYPE='IDT' AND dockdt BETWEEN  '" + FinstartYear + "' AND '" + FinYearEndDate + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }
        public string GetDefaultValue(string code)
        {
            string defaultvalue = "";
            try
            {
                string strsql = "SELECT ISNULL(defaultvalue,'Y') FROM webx_rules_docket WHERE code='" + code + "'";
                defaultvalue = GF.executeScalerQuery(strsql);
            }
            catch (Exception ex)
            {

            }
            return defaultvalue;
        }
        public DataTable GetTable(string code)
        {
            string QueryString = "SELECT drs,ls,mf,thc,cancelled,billed,delivered,billed,finalized FROM webx_trans_docket_status WHERE dockno = '" + code + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        public string GetDocketBillNo(string dockno, string paybas)
        {
            string QueryString = "SELECT TOP 1 m.billno FROM webx_billdet d,webx_billmst m WHERE m.billno=d.billno AND ISNULL(m.bill_cancel,'N')='N' AND d.dockno='" + dockno + "' AND paybas='" + paybas.Substring(2, 1) + "'";
            string billno = GF.executeScalerQuery(QueryString);
            return billno;
        }
        public string GetBillStatus(string billno)
        {
            string QueryString = "SELECT UPPER(billstatus) AS billstatus FROM webx_billmst WHERE billno='" + billno + "'";
            string status = "";
            try
            {
                status = Convert.ToString(GF.executeScalerQuery(QueryString));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return status;
        }
        public double GetAmount(string billno)
        {
            string QueryString = "SELECT billamt-pendamt FROM webx_billmst with(nolock) WHERE billno='" + billno + "'";
            double amount = Convert.ToDouble(GF.executeScalerQuery(QueryString));
            return amount;
        }
        public void Cancellation(string userName, string reason, string Dockno)
        {
            string Querystring = "UPDATE webx_trans_docket_status WITH(ROWLOCK) SET cancelled='Y',cancomment='" + reason + "',cancelledon=getdate(),cancelledby='" + userName + "' WHERE dockno='" + Dockno + "'";
            GF.executeNonQuery(Querystring);
        }
        public DataTable MultipleDocketCancellation(string docket_Xml, string userName)
        {
            string Querystring = "EXEC Usp_MultipleDocket_Cancellation '"+docket_Xml+"','"+userName+"'";
            int Id = GF.SaveRequestServices(Querystring.Replace("'", "''"), "MultipleDocketCancel", "", "");
            DataTable dataTable = GF.GetDataTableFromSP(Querystring);
            return dataTable;
        }

        public void Updatewithbill(string userName, string Reason, string BillNo)
        {
            string Querystring = "UPDATE webx_billmst WITH(ROWLOCK) SET bill_cancel='Y',billstatus='BILL CANCELLED',BCANEMPCD='" + userName + "',BCANDT=GETDATE(),CANCOMMENT='" + Reason + "' WHERE billno='" + BillNo + "'";
            GF.executeNonQuery(Querystring);
        }
        public DataTable ISBillAvailable(string BillNo)
        {
            string QueryString = "SELECT * from webx_billmst with(nolock) where billno= '" + BillNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            return dataTable;
        }
        #endregion

        #region MR Cancellation

        public List<WEBX_MR_VPLIST> GetMRCancellationList(DateTime FromDate, DateTime ToDate, string MRSNO, string ManualMRNO, string PAYBAS)
        {
            string SQRY = "", ToDate1 = "", FromDate1 = "";



            if (FromDate.ToString("dd MMM yyyy") == "" || FromDate.ToString("dd MMM yyyy") == null || FromDate.ToString("dd MMM yyyy") == "01 Jan 0001" || FromDate.ToString("dd MMM yyyy") == "01 Jan 1990")
            {
                FromDate1 = "";
            }

            else if (ToDate.ToString("dd MMM yyyy") == "" || ToDate.ToString("dd MMM yyyy") == null || ToDate.ToString("dd MMM yyyy") == "01 Jan 0001" || ToDate.ToString("dd MMM yyyy") == "01 Jan 1990")
            {
                ToDate1 = "";
            }

            else
            {
                FromDate1 = FromDate.ToString("dd MMM yyyy");
                ToDate1 = ToDate.ToString("dd MMM yyyy");
            }
            SQRY = "exec USP_MR_Cancellation_Listing_Cygnus '" + FromDate1 + "','" + ToDate1 + "','" + MRSNO + "','" + ManualMRNO + "','.','" + PAYBAS + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_MR_VPLIST> VoucherList_Data = DataRowToObject.CreateListFromTable<WEBX_MR_VPLIST>(Dt1);
            return VoucherList_Data;
        }

        public DataTable GetMRCancellationSubmit(string XML, DateTime Cancel_Date, string Cancel_Reason, string BaseFinancialYear, string entryby, string CompanyCode)
        {
            string Cancel_Date1 = Cancel_Date.ToString("dd MMM yyyy");
            string SQRY = "exec USP_MRS_Cancellation_VER1 '" + XML + "','" + Cancel_Date1 + "','" + Cancel_Reason + "','" + BaseFinancialYear + "','" + entryby + "','" + CompanyCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MRCancellation", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }
        #endregion

        #region IDT-Edit

        public DataTable CheckIdtNoForEdit(string IDTNo, string Location)
        {
            string QueryString = "EXEC Usp_Get_IDT_Edit_New_Portal '" + IDTNo + "','" + Location + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        public WebX_Master_Docket GetDetails(string DockNo)
        {
            string QueryString = "EXEC usp_Get_IDT_Detail_New_Portal '" + DockNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            WebX_Master_Docket Docket = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(DT).FirstOrDefault();
            return Docket;                   
        }
        public WebX_Master_Docket_Charges GetChargesDetails(string DockNo)
        {
            string QueryString = "EXEC usp_Get_Charges_Details '" + DockNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            WebX_Master_Docket_Charges Docket = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Charges>(DT).FirstOrDefault();
            return Docket;
        }
        public List<WebX_Master_Docket_Invoice> GetIDTInvoice(string DockNo)
        {
            string QueryString = "EXEC usp_Get_IDT_Invoice_Detail_New_Portal '" + DockNo + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<WebX_Master_Docket_Invoice> Invoice = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Invoice>(DT);
            return Invoice;
        }
        public List<webx_citymaster> GetCityList(string SearchTerm)
        {
            string QueryString = "EXEC GetCities '" + SearchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_citymaster> List = DataRowToObject.CreateListFromTable<webx_citymaster>(DT);
            return List;
        }
        public bool IsActiveGeneralMaster(string mastercode, string datacode)
        {
            string QueryString = "SELECT COUNT(codeid) FROM webx_master_general WHERE codetype='" + mastercode + "' AND codeid='" + datacode + "'"+ "AND statuscode='Y'";
            string Result = GF.executeScalerQuery(QueryString);
            if (Result == "0")
                return false;
            else
                return true;
        }
        public bool IsActiveCustomer(string custcode, string loccode)
        {
            string QueryString = "SELECT COUNT(custcd) FROM webx_custhdr  WHERE custcd='" + custcode + "' AND cust_active='Y'  AND PATINDEx ('%" + loccode + "%',custloc)>0";          
            string Result = GF.executeScalerQuery(QueryString);
            if (Result == "0")
                return false;
            else
                return true;
        }

        public bool IsBookDateAllowed(string strdockbook, string daterule,string FinYear)
        {
            System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");
            DateTime dtdockbook = Convert.ToDateTime(strdockbook, enGB);

            string[] arrtmp = new string[3];
            string[] arrblwrule = new string[2];
            string[] arrabvrule = new string[2];
            try
            {
                arrtmp = daterule.Split('|');

                if (arrtmp[0].CompareTo("Y") != 0)
                    return false;

                arrblwrule = arrtmp[1].Split(',');
                arrabvrule = arrtmp[2].Split(',');

                DateTime dtblwcompare = Convert.ToDateTime(arrblwrule[1], enGB);
                if (dtdockbook.CompareTo(dtblwcompare) < 0)
                    return false;

                DateTime dtabvcompare = Convert.ToDateTime(arrabvrule[1], enGB);
                if (dtdockbook.CompareTo(dtabvcompare) > 0)
                    return false;

                if (dtdockbook.Month < 4)
                {
                    if (Convert.ToString(dtdockbook.Year - 1) != FinYear)
                    {
                        return false;
                    }
                }
                else
                {
                    if (dtdockbook.Year.ToString() != FinYear)
                    {
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
       //public webx_rules_docket GetDocketRule(string key)
       //{
       //    webx_rules_docket dktr = new webx_rules_docket();
       //    XmlDocument xdc = new XmlDocument();
       //    xdc.Load(HttpContext.Current.Server.MapPath("~\\App_Data\\webx_rules_docket.xml"));
       //    XmlNode xn = ((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0]);
       //    dktr.code = key;
       //    try
       //    {
       //        dktr.description = ((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["description"].InnerText.ToString();
       //    }
       //    catch (Exception ex) { dktr.description = ""; }
       //    try
       //    {
       //        dktr.defaultvalue = ((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["defaultvalue"].InnerText.ToString();
       //    }
       //    catch (Exception ex) { dktr.defaultvalue = "N"; }
       //    try
       //    {
       //        dktr.minvalue = Convert.ToDecimal(((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["minvalue"].InnerText.ToString());
       //    }
       //    catch (Exception ex) { dktr.maxvalue = 0; }
       //    try
       //    {
       //        dktr.maxvalue = Convert.ToDecimal(((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["maxvalue"].InnerText.ToString());
       //    }
       //    catch (Exception ex) { dktr.minvalue = 0; }
       //    try
       //    {
       //        dktr.enabled = Convert.ToString(((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["enabled"].InnerText.ToString());
       //    }
       //    catch (Exception ex) { dktr.enabled = "N"; }
       //    try
       //    {
       //        dktr.paybas = ((XmlNode)((XmlNodeList)xdc.GetElementsByTagName(key))[0])["activeflag"].InnerText.ToString();
       //    }
       //    catch (Exception ex) { dktr.activeflag = "N"; }
       //    return dktr;
       //}

       public int ToInteger(string num)
       {
           if (num.ToString().CompareTo("") == 0 || num == null)
           {
               num = "0";
           }

           try
           {
               return Convert.ToInt16(num);
           }
           catch (Exception)
           {
               return 0;
           }
       }
       public string GetBookDateRule(string dbdaterule, string BaseFinYear)
        {
            System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");

            //string daterule = "N|";
            //string[] arrtmp = new string[2];
            //string[] arrdaterule = new string[7];
            //DateTime dtcompare;
            //arrtmp = dbdaterule.Split('|');
            //arrdaterule = arrtmp[0].Split(',');

            string daterule = "N|";
            string[] arrtmp = new string[2];
            string[] arrdaterule = new string[7];
            DateTime dtcompare;
            arrtmp = dbdaterule.Split('|');
            arrdaterule = arrtmp[0].Split(',');

            DateTime dtfinstart = new DateTime(ToInteger(BaseFinYear.Substring(0, 3)), 4, 1);
            DateTime dtfinend = new DateTime(ToInteger(BaseFinYear.Substring(0, 3)) + 1, 3, 31);








            // [0]=Y MEANS RULE FOR DATE APPLIES
            if (arrdaterule[0].CompareTo("Y") == 0)
            {
                daterule = "Y|";
                if (arrdaterule[1].CompareTo("B_D") == 0)
                {
                    int days = Convert.ToInt32(arrdaterule[3]);
                    dtcompare = Convert.ToDateTime(GetServerDate(), enGB);
                    dtcompare = dtcompare.AddDays(0 - days);
                    daterule = daterule + arrdaterule[1] + "," + dtcompare.ToString("dd/MM/yyyy");

                }
                else if (arrdaterule[1].CompareTo("B_T") == 0)
                {
                    dtcompare = Convert.ToDateTime(arrdaterule[5], enGB);
                    daterule = daterule + arrdaterule[1] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else
                {
                    daterule = "N|";
                }


                daterule = daterule + "|";
                // [2] MEANS WHICH RULE FOR ABOVE DATE
                if (arrdaterule[2].CompareTo("A_D") == 0)
                {
                    int days = Convert.ToInt32(arrdaterule[4]);
                    dtcompare = Convert.ToDateTime(GetServerDate(), enGB);
                    dtcompare = dtcompare.AddDays(days);
                    daterule = daterule + arrdaterule[2] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else if (arrdaterule[2].CompareTo("A_T") == 0)
                {
                    dtcompare = Convert.ToDateTime(arrdaterule[6], enGB);
                    daterule = daterule + arrdaterule[2] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else
                {
                    daterule = "N|";
                }

            }

            return daterule;
        }
        public string GetServerDate()
        {
            string QueryString = "SELECT TOP 1 date=CONVERT(VARCHAR,GETDATE(),103)";
            string Result = GF.executeScalerQuery(QueryString);
            return Result;
        }
        public DataTable UpdateIDT(string strXMLDocket, string strXMLDocketCharges, string strXMLDocketInvoice)
        {
            string QueryString = "exec usp_docket_update_New_Portal '" + strXMLDocket + "','" + strXMLDocketCharges + "','" + strXMLDocketInvoice + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "UpdateIDT", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Voucher Date Edit
        public DataTable VoucherDateEdit(string VoucherEditDate, string voucherno, string YearSuffix, string BaseUserName)
        {
            string SQRY = "exec USP_Update_Voucher_date '" + VoucherEditDate + "','" + voucherno + "','" + YearSuffix + "','" + BaseUserName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }
        public DataTable GetVoucherDateEdit(string voucherno, string YearSuffix, string CompanyCode)
        {
            string SQRY = "exec USP_Get_Voucher_date '" + voucherno + "','" + YearSuffix + "','" + CompanyCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }
        #endregion


    }
}