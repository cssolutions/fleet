﻿using Fleet.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using FleetDataService.Models;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ContractService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ContractService.svc or ContractService.svc.cs at the Solution Explorer and start debugging.
    public class ContractService : IContractService
    {
        GeneralFuncations GF = new GeneralFuncations();
        private void SerializeParams<T>(List<T> paramList, string folderPath)
        {
            XDocument doc = new XDocument();

            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(paramList.GetType());

            System.Xml.XmlWriter writer = doc.CreateWriter();

            serializer.Serialize(writer, paramList);

            writer.Close();

            doc.Save(folderPath);
        }

        private List<T> DeserializeParams<T>(string folderPath)
        {
            XDocument doc = XDocument.Load(folderPath);
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(typeof(List<T>));

            System.Xml.XmlReader reader = doc.CreateReader();

            List<T> result = (List<T>)serializer.Deserialize(reader);
            reader.Close();

            return result;
        }

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
        public void DoWork()
        {
        }

        #region Customer Contract

        public List<webx_custcontract_hdr> GetCustCont(string Name, string contractType)
        {
            string SQRY = "exec sp_GetLike_GetCustomer '" + Name + "','" + contractType + "'";
            return DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(GF.GetDataTableFromSP(SQRY));
        }

        public DataTable GetCustContName(string CoustomerCode, string contractID)
        {
            string sqlstr = "SELECT (SELECT custnm AS custname FROM webx_custhdr WHERE custcd=custcode) as Name, custcode,contractid,ISNULL(activeflag,'N') AS activeflag,status=(CASE activeflag WHEN 'Y' THEN 'Active' ELSE 'InActive' END),";
            sqlstr = sqlstr + "CONVERT(VARCHAR,contract_effectdate,106) AS startdate,";
            sqlstr = sqlstr + "CONVERT(VARCHAR,contract_eddate,106) AS enddate,";
            sqlstr = sqlstr + "contract_type,contracttype=(SELECT codedesc FROM webx_master_general WHERE codetype='PAYTYP' AND codeid=contract_type)";
            sqlstr = sqlstr + " FROM webx_custcontract_hdr WHERE custcode='" + CoustomerCode + "'AND  contractid='" + contractID + "'";
            return GF.GetDataTableFromSP(sqlstr); ;
        }

        public List<webx_custcontract_hdr> GetCustContList(string CoustomerCode)
        {
            string sqlstr = "SELECT (SELECT custnm AS custname FROM webx_custhdr WHERE custcd=custcode) as Name, Custcode,ContractId,ISNULL(activeflag,'N') AS activeflag,Status=(CASE activeflag WHEN 'Y' THEN 'Active' ELSE 'InActive' END),";
            sqlstr = sqlstr + "CONVERT(VARCHAR,Contract_Stdate,106) AS Nstartdate,";
            sqlstr = sqlstr + "CONVERT(VARCHAR,contract_eddate,106) AS Nenddate,Image_Path,";
            sqlstr = sqlstr + "Contract_Type=(SELECT codedesc FROM webx_master_general WHERE codetype='PAYTYP' AND codeid=contract_type),contracttype=(SELECT codedesc FROM webx_master_general WHERE codetype='PAYTYP' AND codeid=contract_type)";
            sqlstr = sqlstr + " FROM webx_custcontract_hdr WHERE custcode='" + CoustomerCode + "'";

            string SQRY = sqlstr;
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<webx_custcontract_hdr> ContractList = DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(dataTable);
            return ContractList;
        }

        public List<Webx_Contract_ScanCopy> GetContractScanCopyList(string contractID)
        {
            string sqlstr = "select ContractID,UploadDate,Filename from Webx_Contract_ScanCopy with(nolock) where ContractID='"+ contractID + "' Order by UploadDate";
             
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<Webx_Contract_ScanCopy> contractScanCopyList = DataRowToObject.CreateListFromTable<Webx_Contract_ScanCopy>(dataTable);
            return contractScanCopyList;
        }

        public List<webx_custcontract_hdr> GetCustomerCodeList(string CoustomerCode)
        {
            string sqlstr = "SELECT Contract_Stdate,Contract_Eddate,Contract_Effectdate,* FROM webx_custcontract_hdr WHERE contractid='" + CoustomerCode + "'";
            string SQRY = sqlstr;
            return DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(GF.GetDataTableFromSP(SQRY));
        }

        public DataTable UpdateCustomerCode(string CoustomerCode, string XML)
        {
            string sqlstr = "exec Usp_Update_webx_custcontract_hdr '" + CoustomerCode + "','" + XML.Trim() + "'";
            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "UpdateCustomerCode", "", "");
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            return Dt;
        }

        public DataTable AddUploadFile(string ContractId, string DocumentUploadedPath)
        {
            string QueryString = "";
            QueryString = "exec ADD_IMAGECostmorContract '" + ContractId + "','" + DocumentUploadedPath + "'";
            DataTable Dt = GF.GetDataTableFromSP(QueryString);
            return Dt;
        }

        #endregion

        #region Customer Charge

        public List<webx_custcontract_charge> GetContractchargeList(string contractid)
        {
            string sqlstr = "SELECT * FROM dbo.webx_custcontract_charge WITH(NOLOCK) WHERE contractid='" + contractid + "'";
            string SQRY = sqlstr;
            return DataRowToObject.CreateListFromTable<webx_custcontract_charge>(GF.GetDataTableFromSP(SQRY));
        }

        public List<webx_custcontract_hdr> GetCustomerContract(string str)
        {
            string QueryString = "exec sp_Getwebx_custcontract_hdr '" + str + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_custcontract_hdr> ContractList = DataRowToObject.CreateListFromTable<webx_custcontract_hdr>(dataTable);
            return ContractList;
        }

        public List<webx_CUSTHDR> Getwebx_CUSTHDR()
        {
            string QueryString = "exec sp_Getwebx_CUSTHDR ";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_CUSTHDR> ContractList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(dataTable);
            return ContractList;
        }

        #endregion

        #region Add Contract Basic service

        public DataTable AddContractService(string Contractid, string XML,string BaseUserName)
        {
            string QueryString = "";
            QueryString = "exec USP_INSERT_CCMSERVICES_NewPortal '" + Contractid + "','" + XML.Trim() + "','" + BaseUserName + "'";
            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddContractService", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable DeleteCustContMatrix(string Contractid, string Risktype)
        {

            string sqlstr = "DELETE FROM webx_custcontract_fovmatrix WHERE ";
            sqlstr = sqlstr + " contractid='" + Contractid + "'";
            sqlstr = sqlstr + " AND risktype='" + Risktype + "'";
            return GF.GetDataTableFromSP(sqlstr);
        }

        public DataTable InsertCustContMatrix(string Contractid, string Risktype, decimal SlabFrom, decimal SlabTo, decimal FOVRate, string RateType)
        {
            string sqlstr = "INSERT INTO webx_custcontract_fovmatrix(contractid,risktype,slabfrom,slabto,fovrate,ratetype)";
            sqlstr = sqlstr + " VALUES('" + Contractid + "',";
            sqlstr = sqlstr + "'" + Risktype + "'," + SlabFrom + "," + SlabTo + ",";
            sqlstr = sqlstr + FOVRate + ",'" + RateType + "')";

            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "InsertCustContMatrix", "", "");

            return GF.GetDataTableFromSP(sqlstr);
        }

        public List<webx_custcontract_fovmatrix> GetMatrixList(string Contractid, string Risktype)
        {
            string sqlstr = "SELECT risktype,slabfrom,slabto,fovrate,ratetype FROM webx_custcontract_fovmatrix";
            sqlstr = sqlstr + " WHERE contractid='" + Contractid + "' AND risktype='" + Risktype + "'";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_custcontract_fovmatrix> ContractMatrixList = DataRowToObject.CreateListFromTable<webx_custcontract_fovmatrix>(dataTable);
            return ContractMatrixList;
        }

        //loading - unloading
        public DataTable DeleteHandlingChrge(string Contractid, string Type)
        {

            string sqlstr = "DELETE FROM CYGNUS_HandlingCharge_Contarct WHERE ";
            sqlstr = sqlstr + " contractid='" + Contractid + "'";
            sqlstr = sqlstr + " AND Type='" + Type + "'";

            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "DeleteHandlingChrge", "", "");

            return GF.GetDataTableFromSP(sqlstr);
        }

        public DataTable InsertHandlingChrge(string Contractid, string Type, string ContractType, string LoadingBy, string RateType, decimal Rate, decimal Wt_Pkg, decimal TotalCharge, decimal MaxAmount, string username, DateTime date)
        {
            string sqlstr = "INSERT INTO CYGNUS_HandlingCharge_Contarct(ContractId,Type,ContractType,LoadingBy,RateType,Rate,Wt_Pkg,TotalCharge,MaxAmount,EntryBy,EntryDate)";
            sqlstr = sqlstr + " VALUES('" + Contractid + "',";
            sqlstr = sqlstr + "'" + Type + "','" + ContractType + "','" + LoadingBy + "','" + RateType + "'," + Rate + "," + Wt_Pkg + "," + TotalCharge + "," + MaxAmount + ",'" + username + "','" + date + "')";

            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "InsertHandlingChrge", "", "");

            return GF.GetDataTableFromSP(sqlstr);
        }

        public List<webx_HandlingCharge> GetHandlingChrge(string Contractid, string Type)
        {
            string sqlstr = "SELECT * FROM CYGNUS_HandlingCharge_Contarct";
            sqlstr = sqlstr + " WHERE ContractId='" + Contractid + "' AND Type='" + Type + "'";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_HandlingCharge> HandlingChrgesList = DataRowToObject.CreateListFromTable<webx_HandlingCharge>(dataTable);
            return HandlingChrgesList;
        }

        #endregion

        #region ContractMode

        public List<Webx_Master_General> GetContractTransMode(string contractid)
        {
            string strsql = "SELECT CodeId,CodeDesc FROM dbo.webx_master_general WHERE codetype='TRN'";
            strsql = strsql + " AND statuscode='Y' AND PATINDEX('%'+ codeid +'%',(SELECT trans_type FROM ";
            strsql = strsql + " webx_custcontract_charge WHERE contractid='" + contractid + "'))>0 ";
            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<Webx_Master_General> GMasterList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return GMasterList;
        }

        public List<webx_custcontract_servicecharges> GetListModeService(string ContractID, string TransMode)
        {
            string QueryString = "SELECT TOP 1 * FROM webx_custcontract_servicecharges";
            QueryString = QueryString + " WHERE contractid='" + ContractID + "' AND trans_type='" + TransMode + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<webx_custcontract_servicecharges> ServiceList = DataRowToObject.CreateListFromTable<webx_custcontract_servicecharges>(dataTable);
            return ServiceList;

        }

        public List<Webx_Master_General> GetContractMatrixType(string contractid)
        {
            string strsql = "SELECT CodeId,CodeDesc FROM dbo.webx_master_general WHERE codetype='MXTYPE'";
            strsql = strsql + " AND statuscode='Y' AND PATINDEX('%'+ codeid +'%',(SELECT matrix_type FROM ";
            strsql = strsql + " webx_custcontract_charge WHERE contractid='" + contractid + "'))>0 ";
            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<Webx_Master_General> GMasterList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return GMasterList;
        }

        public List<Webx_Master_General> GetContractRateType(string contractid)
        {
            string strsql = "SELECT CodeId,CodeDesc FROM dbo.webx_master_general WHERE codetype='RATETYPE'";
            strsql = strsql + " AND statuscode='Y' AND PATINDEX('%'+ codeid +'%',(SELECT rate_type FROM ";
            strsql = strsql + " webx_custcontract_charge WHERE contractid='" + contractid + "'))>0 ";
            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<Webx_Master_General> GMasterList = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return GMasterList;
        }

        public string CustomerCharge()
        {
            string sql = "SELECT ISNULL(defaultvalue,'Y') AS defaultvalue  FROM webx_rules_docket WHERE code='FLAG_MULTI_PICKUPDEL'";
            DataTable dataTable = GF.GetDataTableFromSP(sql);
            return dataTable.Rows[0]["defaultvalue"].ToString();
        }

        #endregion

        #region Add ModeVice Service

        public DataTable AddContractModeViceService(string strXMLCriteria, string strXMLOutPut)
        {
            string QueryString = "";
            QueryString = "exec USP_INSERT_CCMMODEWISE_SERVICECHARGES_NewPortal '" + strXMLCriteria.Trim() + "','" + strXMLOutPut.Trim() + "'";
            int id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddContractModeViceService", "", "");
            return GF.GetDataTableFromSP(QueryString);
        }

        public List<webx_custcontract_charge_constraint> GetchargeMatrixList(string Contractid, string drpval)
        {
            string strsql = "SELECT c.chargecode,c.chargename,varybyloc,operator as operator1,isnull(rate,0) as chargerate,'N'AS use_from,'N' AS use_to,";
            strsql = strsql + "'N' AS use_trans_type,'N' AS use_rate_type,'NONE' AS basedon,'N' AS slab_type,'N' AS Loop_Wise_Contract FROM (";
            strsql = strsql + " SELECT DISTINCT c.chargecode,c.chargename,c.basedon,c.basecode,c.varybyloc,operator ";
            strsql = strsql + " FROM webx_master_charge c WHERE c.chargetype='DKT' AND activeflag='Y'";
            if (drpval == "BKG")
                strsql = strsql + " AND booktimeflag='Y')C";
            else
                strsql = strsql + " AND deltimeflag='Y')C";
            strsql = strsql + " FULL JOIN webx_custcontract_othercharges cc  ";
            strsql = strsql + " ON c.chargecode=cc.chargecode AND c.basedon=cc.basedon1 AND c.basecode=cc.basecode1 ";
            strsql = strsql + " AND cc.contractid='" + Contractid + "' AND C.CHARGECODE IS NOT NULL ";
            strsql = strsql + " AND basedon='NONE'";
            strsql = strsql + " AND basecode='" + drpval + "' AND basedon2=''";
            strsql = strsql + " AND basecode2='NONE' AND fromloc='8888' AND toloc='8888'";
            strsql = strsql + " WHERE c.chargecode IS NOT NULL AND c.chargecode NOT IN ('SCHG11','SCHG12','SCHG13','SCHG20','UCHG01','UCHG02','UCHG03','UCHG04') ORDER BY c.chargename";//,'SCHG19' Removed as per Rajeshji's Instruction with Chirag Paneliya on 07 Jun 2017 at 2:30 PM

            DataTable dt = GF.GetDataTableFromSP(strsql);


            string strsql2 = "SELECT codeid,codedesc FROM webx_master_general  WHERE codetype='SVCTYP' AND statuscode='Y' ORDER BY codedesc";
            DataTable dt2 = GF.GetDataTableFromSP(strsql2);
            for (int i = dt2.Rows.Count - 1; i >= 0; --i)
            {
                DataRow dtr = dt.NewRow();
                dtr["chargecode"] = dt2.Rows[i]["codeid"].ToString();
                dtr["chargename"] = "FREIGHT - " + dt2.Rows[i]["codedesc"].ToString();
                dtr["operator1"] = "+";
                dt.Rows.InsertAt(dtr, 0);
            }

            string strsql1 = "SELECT * FROM webx_custcontract_charge_constraint ";
            strsql1 = strsql1 + " WHERE contractid='" + Contractid + "' ";

            DataTable dtcons = GF.GetDataTableFromSP(strsql1);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int j = 0; j < dtcons.Rows.Count; j++)
                {
                    if (dtcons.Rows[j]["chargecode"].ToString() == dt.Rows[i]["chargecode"].ToString())
                    {
                        dt.Rows[i]["use_from"] = dtcons.Rows[j]["use_from"].ToString();
                        dt.Rows[i]["use_to"] = dtcons.Rows[j]["use_to"].ToString();
                        dt.Rows[i]["use_trans_type"] = dtcons.Rows[j]["use_trans_type"].ToString();
                        dt.Rows[i]["use_rate_type"] = dtcons.Rows[j]["use_rate_type"].ToString();
                        dt.Rows[i]["slab_type"] = dtcons.Rows[j]["slab_type"].ToString();
                        dt.Rows[i]["Loop_Wise_Contract"] = dtcons.Rows[j]["Loop_Wise_Contract"].ToString();
                        dt.Rows[i]["basedon"] = dtcons.Rows[j]["basedon"].ToString();
                    }
                }
            }

            List<webx_custcontract_charge_constraint> ContractMatrixList = DataRowToObject.CreateListFromTable<webx_custcontract_charge_constraint>(dt);
            return ContractMatrixList;
        }

        public DataTable InsertChargeMatrix(string str)
        {
            return GF.GetDataTableFromSP(str);
        }

        public DataTable DeleteChargeMatrix(string str)
        {
            return GF.GetDataTableFromSP(str);
        }

        #endregion

        #region Standard Charges

        public List<webx_master_charge> GetChargeMatrixCriteria()
        {
            //string strsql = "SELECT ISNULL(defaultvalue,'Y') FROM webx_rules_docket WHERE code='CHRG_RULE'";
            string str = "SELECT * FROM dbo.webx_master_charge WHERE chargetype='DKT'"
            + " AND basedon='NONE' AND basecode='NONE' And  booktimeflag='Y' AND case when chargecode='SCHG05' then 'N' ELSE deltimeflag END='N'"
            + "AND activeflag='Y' AND ChargeCode NOT IN ('SCHG10','SCHG11','SCHG12','SCHG13','SCHG14','SCHG17','SCHG20','UCHG01','UCHG02','UCHG03','UCHG04')";//,'SCHG19' Removed as per Rajeshji's Instruction with Chirag Paneliya on 07 Jun 2017 at 2:30 PM
            DataTable dataTable = GF.GetDataTableFromSP(str);
            List<webx_master_charge> GetChargeMatrixList = DataRowToObject.CreateListFromTable<webx_master_charge>(dataTable);
            return GetChargeMatrixList;
        }

        public List<Webx_CustContract_FRTMatrix_SingleSlab> GetSinlgeslab(string ContractID, string BasedOn1, string BaseCode1, string BasedOn2, string BaseCode2,
                    string ChargeType, string ChargeCode, string MatrixType, string TransMode, string From, string To,string ContractType)
        {
            if (MatrixType == null)
                MatrixType = "NA";
            if (TransMode == null)
                TransMode = "NA";
            if (ContractType == null)
                ContractType = "S";

            string strsql = "SELECT ";
            if (MatrixType == "R")
            {
                //strsql = strsql + "(SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=fromloc) AS fromloc,";
                //strsql = strsql + "(SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=toloc) AS toloc,";

                strsql = strsql + "FromLoc,";
                strsql = strsql + "toloc,";
            }
            else
            {
                strsql = strsql + "FromLoc,toloc,";
            }
            strsql = strsql + "BillLoc,rate,ratetype,trdays,trans_type,cast(srno as int) as srno,FromRange,ToRange,RateUnits,LoopTransMode";
            strsql = strsql + " FROM dbo.Webx_CustContract_FRTMatrix_SingleSlab";
            strsql = strsql + " WHERE Contractid='" + ContractID + "'";
            strsql = strsql + " AND BasedOn1='" + BasedOn1 + "' AND BaseCode1='" + BaseCode1 + "'";
            strsql = strsql + " AND BasedOn2='" + BasedOn2 + "' AND BaseCode2='" + BaseCode2 + "'";
            strsql = strsql + " AND ChargeType='" + ChargeType + "' AND ChargeCode='" + ChargeCode + "'";
            strsql = strsql + " AND Loc_Reg='" + MatrixType + "'";
            strsql = strsql + " AND FromLoc LIKE '" + From + "%'";
            strsql = strsql + " AND toloc LIKE '" + To + "%'";
            if (MatrixType == "L")
            {
                strsql = strsql + " AND Fromloc in (SELECT LocCode FROM dbo.webx_location WHERE ActiveFlag='Y')";
                strsql = strsql + " AND toloc IN (SELECT LocCode FROM dbo.webx_location WHERE ActiveFlag='Y')";
            }
            if (TransMode != "A")
            {
                strsql = strsql + " AND Trans_Type LIKE '" + TransMode + "'";
            }
            else
            {
                strsql = strsql + " AND Trans_Type in (SELECT * FROM dbo.Split((SELECT Trans_Type FROM WEBX_CUSTCONTRACT_CHARGE WHERE contractid='" + ContractID + "'),','))";
            }

            if (ContractType != "S")
            {
                strsql = strsql + " AND ISNULL(NULLIF(ContractType,''),'S') = '" + ContractType + "'";
            }
            else
            {
                strsql = strsql + " AND ISNULL(NULLIF(ContractType,''),'S') = 'S'";
            }

            int Id = GF.SaveRequestServices(strsql.Replace("'", "''"), "GetSinlgeslab", "", "");

            DataTable dataTable = GF.GetDataTableFromSP(strsql);


            List<Webx_CustContract_FRTMatrix_SingleSlab> GetSinlgeslabList = DataRowToObject.CreateListFromTable<Webx_CustContract_FRTMatrix_SingleSlab>(dataTable);
            return GetSinlgeslabList;
        }

        public List<Webx_Master_General> Getlocationlist(string Name)
        {
            string sqlstr = "EXEC GetLocationList'" + Name + "'";
            return DataRowToObject.CreateListFromTable<Webx_Master_General>(GF.GetDataTableFromSP(sqlstr));
        }

        public DataTable AddFreightCharge(string XML)
        {
            string QueryString = "";
            QueryString = "exec USP_CCM_Validate_MatrixCombination '" + XML + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddFreightCharge", "", "");

            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable AddFreightChargeChargeMatrix(string XML, string SavingBy, string OutPut)
        {
            string QueryString = "";
            QueryString = "exec Usp_ChargeMatrix_Entry '" + XML + "','" + SavingBy + "','" + "<root></root>" + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "AddFreightChargeChargeMatrix", "", "");

            return GF.GetDataTableFromSP(QueryString);
        }

        public List<webx_custcontract_charge_constraint> Getfreightcharge(string contractid, string chargecode, string chargetype)
        {
            string strsql = "SELECT * FROM dbo.webx_custcontract_charge_constraint WITH(NOLOCK)";
            strsql = strsql + " WHERE contractid='" + contractid + "' AND chargecode='" + chargecode + "' AND chargetype='" + chargetype + "'";

            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<webx_custcontract_charge_constraint> GetfreightchargeList = DataRowToObject.CreateListFromTable<webx_custcontract_charge_constraint>(dataTable);
            return GetfreightchargeList;
        }

        //Get Slab
        public List<Webx_CustContract_FRTMatrix_SingleSlab> getstandardCharge(string ContractID)
        {
            string strsql = "SELECT FromLoc,toloc,BillLoc,rate,RateType,TrDays,trans_type,chargetype,basecode1,basecode2,basedon1,basedon2,contractid,chargecode FROM dbo.Webx_CustContract_FRTMatrix_SingleSlab";
            strsql = strsql + " WHERE Contractid='" + ContractID + "' AND BasedOn1='NONE' AND BaseCode1='NONE' AND BasedOn2='NONE'";
            strsql = strsql + "AND BaseCode2='NONE' AND ChargeType='BKG' AND ChargeCode='SCHG01' AND Loc_Reg='NA' AND FromLoc LIKE 'NA%' AND ToLoc LIKE 'NA%' AND Trans_Type LIKE 'NA'";
            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<Webx_CustContract_FRTMatrix_SingleSlab> getstandardChargeist = DataRowToObject.CreateListFromTable<Webx_CustContract_FRTMatrix_SingleSlab>(dataTable);
            return getstandardChargeist;
        }
        public List<Webx_CustContract_FRTMatrix_SingleSlab> GetFreightlab(string ContractID, string BasedOn1, string BaseCode1, string BasedOn2, string BaseCode2,
                    string MatrixType, string TransMode, string From, string To)
        {
            string strsql = "";
            if (MatrixType == "R")
            {
                strsql = "SELECT slab_code,";
                strsql = strsql + " (SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codedesc=from_loccode) AS FromLoc,";
                strsql = strsql + "(SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codedesc=to_loccode) AS toloc,";
            }
            else
            {
                strsql = strsql + "SELECT slab_code,from_loccode as FromLoc,to_loccode as toloc,";
            }

            strsql = strsql + " ISNULL(trdays,0) AS trdays,ISNULL(rate,0.00) AS Slab1,0.00 AS Slab2,";
            strsql = strsql + " 0.00 AS Slab3,0.00 AS Slab4,0.00 AS Slab5,0.00 AS Slab6";
            strsql = strsql + " FROM webx_custcontract_frtmatrix_slabdet ";
            strsql = strsql + " WHERE contractid='" + ContractID + "'";
            strsql = strsql + " AND basedon1='" + BasedOn1 + "' AND basecode1='" + BaseCode1 + "'";
            strsql = strsql + " AND basedon2='" + BasedOn2 + "' AND basecode2='" + BaseCode2 + "'";

            if (MatrixType.CompareTo("R") == 0)
            {
                strsql = strsql + " AND (SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codedesc=from_loccode) LIKE '" + From + "%'";
                strsql = strsql + " AND (SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codedesc=TO_loccode) LIKE '" + To + "%'";
            }
            else
            {
                strsql = strsql + " AND from_loccode LIKE '" + From + "%'";
                strsql = strsql + " AND to_loccode LIKE '" + To + "%'";
            }

            strsql = strsql + " AND loc_reg='" + MatrixType + "'";
            strsql = strsql + " AND trans_type='" + TransMode + "' AND slab_code IS NOT NULL";
            string strmain = strsql + " AND slab_code='SLAB1' ORDER BY srno,slab_code";
            DataTable dtcharges = GF.GetDataTableFromSP(strmain);

            DataTable dt = new DataTable();
            for (int i = 2; i < 7; i++)
            {
                strmain = strsql + " AND slab_code='SLAB" + i.ToString() + "' ORDER BY srno,slab_code";
                dt = GF.GetDataTableFromSP(strmain);
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    dtcharges.Rows[j]["slab" + i.ToString()] = dt.Rows[j]["slab1"].ToString();
                }
            }



            //  DataTable dataTable = GF.GetDataTableFromSP(strmain);
            List<Webx_CustContract_FRTMatrix_SingleSlab> GetSinlgeslabList = DataRowToObject.CreateListFromTable<Webx_CustContract_FRTMatrix_SingleSlab>(dtcharges);
            return GetSinlgeslabList;
        }

        public List<webx_custcontract_frtmatrix_slabhdr> GetMultiSlabList(string ContarctID, string TranMode)
        {
            string Strsql = "SELECT slab_code,slab_from,slab_to,rate_type FROM webx_custcontract_frtmatrix_slabhdr ";
            Strsql = Strsql + " WHERE contractid='" + ContarctID + "' AND trans_type='" + TranMode + "' ORDER BY srno";
            DataTable dataTable = GF.GetDataTableFromSP(Strsql);
            List<webx_custcontract_frtmatrix_slabhdr> GetSlabList = DataRowToObject.CreateListFromTable<webx_custcontract_frtmatrix_slabhdr>(dataTable);
            return GetSlabList;
        }
        public DataTable InsertContractfrtmatrix_slabhdr(string conatctId, decimal slab_from, decimal slab_to, string rate_type, string slab_code, string Trans_Type)
        {
            string QueryString = "";
            QueryString = "exec Insert_custcontract_frtmatrix_slabhdr '" + conatctId + "'," + slab_from + "," + slab_to + ",'" + rate_type + "','" + slab_code + "','" + Trans_Type + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "InsertContractfrtmatrix_slabhdr", "", "");

            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable DeleteContractfrtmatrix_slabhdr(string str)
        {
            int Id = GF.SaveRequestServices(str.Replace("'", "''"), "DeleteContractfrtmatrix_slabhdr", "", "");

            return GF.GetDataTableFromSP(str);
        }

        public DataTable USP_INSERT_CUSTCONTRACT_SUNDRY_MULTISLAB_DETAILS(string XML)
        {
            string QueryString = "";
            QueryString = "exec USP_INSERT_CUSTCONTRACT_SUNDRY_MULTISLAB_DETAILS '" + XML + "'";

            int Id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "USP_INSERT_CUSTCONTRACT_SUNDRY_MULTISLAB_DETAILS", "", "");

            return GF.GetDataTableFromSP(QueryString);
        }
        public List<Webx_Master_General> Get_ContractPaymentBasis(string CustCode)
        {
            string sqlstr = "EXEC Get_ContractPaymentBasis'" + CustCode + "'";
            return DataRowToObject.CreateListFromTable<Webx_Master_General>(GF.GetDataTableFromSP(sqlstr));
        }

        public DataTable AddUpdateContract(string PaymentBasis, string StartDate, string EndDate, string CustCode)
        {
            string QueryString = "";
            QueryString = "exec AddUpdateContract '" + PaymentBasis + "','" + StartDate + "','" + EndDate + "','" + CustCode + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        public DataTable Delete_Single_Slab_Rates(int ID, string Location, string CompanyCode, string UserId)
        {
            string QueryString = "";
            QueryString = "exec USP_Delete_Single_Slab_Rates '" + ID + "','" + Location + "','" + CompanyCode + "','" + UserId + "'";
            return GF.GetDataTableFromSP(QueryString);
        }

        #endregion

        #region Customer  Change Status Y / N
        public DataTable CustomerChnageStatus(string CostCode, string ContractID, string Flag, string BaseUserName)
        {
            string QueryString = "";
            if (Flag == "Active")
                QueryString = "USP_Customer_Contract_Active_Deactive '" + CostCode + "','" + ContractID + "','" + Flag + "','" + BaseUserName + "'";
            else
                QueryString = "USP_Customer_Contract_Active_Deactive '" + CostCode + "','" + ContractID + "','" + Flag + "','" + BaseUserName + "'";
            return GF.GetDataTableFromSP(QueryString);
        }
        #endregion

        public List<Webx_Master_General> Getcontactpay(string CustCode)
        {
            string sqlstr = "declare @a varchar(max),@b varchar(max) SELECT @b=(Char(39)+ REPLACE (custcat,',',''',''') + Char(39)) FROM webx_custhdr WHERE custcd='" + CustCode + "' ";
            sqlstr = sqlstr + " select @a='select CodeId,CodeDesc from webx_master_general where codetype=''PAYTYP'' AND codeID IN (' + @b + ')' exec( @a)";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<Webx_Master_General> Webx_Master_GeneralList_XML = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return Webx_Master_GeneralList_XML;
        }

        public DataTable GetVendorName(string vendorcode)
        {
            DataTable dt = new DataTable();
            string strsql = "SELECT vendorname AS CustName FROM webx_vendor_hdr ";
            strsql = strsql + " WHERE vendorcode='" + vendorcode + "'";
            try
            {
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetCustomerName(string CustomerCode)
        {
            DataTable dt = new DataTable();
            string strsql = "SELECT custnm AS CustName FROM webx_custhdr ";
            strsql = strsql + " WHERE custcd='" + CustomerCode + "'";
            try
            {
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public DataTable GetCustomerNameOnlyContract(string CustomerCode)
        {
            DataTable dt = new DataTable();
            string strsql = "SELECT custnm AS CustName,CustAddress FROM webx_custhdr ";
            strsql = strsql + " WHERE custcd='" + CustomerCode + "'";
            try
            {
                return GF.GetDataTableFromSP(strsql);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region  Add ContractID

        public string getCodeContractID()
        {
            string contractID = "";
            string SQLQuery = "SELECT MAX(contractid) FROM webx_custcontract_hdr WHERE contractid NOT LIKE 'P0%' AND LEN(CONTRACTID)>8";
            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            string NewContractID = "";
            contractID = dt.Rows[0]["column1"].ToString();
            Int64 MAXNumber = Convert.ToInt64(contractID.Substring(2, contractID.Length - 2));
            MAXNumber++;
            NewContractID = "CN" + MAXNumber.ToString().PadLeft(10, '0');
            return NewContractID;
        }

        public DataTable AddNewContract(string custcode, string Paybas, string startdate, string enddate, string ContarctID)
        {
            string sqlstr = "";
            sqlstr = "INSERT INTO webx_custcontract_hdr";
            sqlstr = sqlstr + "(contractid,custcode,contract_type,";
            sqlstr = sqlstr + "Contract_Eddate,Contract_Stdate,Contract_Effectdate,activeflag)";
            sqlstr = sqlstr + " VALUES('" + ContarctID + "','" + custcode + "',";
            sqlstr = sqlstr + "'" + Paybas + "','" + enddate + "',";
            sqlstr = sqlstr + "'" + startdate + "',";
            sqlstr = sqlstr + "'" + startdate + "','Y')";

            int Id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "AddNewContract", "", "");

            return GF.GetDataTableFromSP(sqlstr);
        }

        #endregion

        #region GetSundrySlabType

        public DataTable GetSundrySlabType(string contractid)
        {
            string strsql = "SELECT slab_type FROM dbo.webx_custcontract_charge_constraint";
            strsql = strsql + " WHERE contractid='" + contractid + "' AND chargetype='BKG' AND chargecode='1'";
            return GF.GetDataTableFromSP(strsql);

        }

        public DataTable GetServiceType(string CustCode, string ContractID)
        {
            string strsql = "SELECT service_type FROM dbo.webx_custcontract_charge";
            strsql = strsql + " WHERE Custcode='" + CustCode + "'AND ContractID='" + ContractID + "'";
            return GF.GetDataTableFromSP(strsql);

        }

        public DataTable Getservicecharge(string contractid)
        {
            string strsql = "SELECT top 1 isNewZoneContractApplied FROM dbo.webx_custcontract_servicecharges WITH(NOLOCK)";
            strsql = strsql + " WHERE contractid='" + contractid + "' and isnull(isNewZoneContractApplied,'') !='' ";
            return GF.GetDataTableFromSP(strsql);

        }

        #endregion

        public string GetDefaultValue(string code)
        {
            string defaultvalue = "";
            try
            {
                string strsql = "SELECT ISNULL(defaultvalue,'Y') FROM webx_rules_docket WHERE code='" + code + "'";
                DataTable dt = GF.GetDataTableFromSP(strsql);
                defaultvalue = dt.Rows[0]["defaultvalue"].ToString();
            }
            catch (Exception) { }
            return defaultvalue;
        }

        #region SUNDRY FTL

        public List<Webx_CustContract_FRTMatrix_SingleSlab> ListSundryFTL(string MatrixType, string ftltype, string ConntractId, string TranMode, string From, string To)
        {
            string strsql = "";
            strsql = "SELECT ";
            if (MatrixType == "R")
            {
                strsql = strsql + "CAST(srno AS int) AS srno,(SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=from_loccode) AS from_loccode,";
                strsql = strsql + "CAST(srno AS int) AS srno,(SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=to_loccode) AS to_loccode,";
            }
            else
            {
                strsql = strsql + "CAST(srno AS int) AS srno,from_loccode as FromLoc,to_loccode as toloc,";
            }
            strsql = strsql + "ftl1_trip_rate as rate,ftl1_trip_ratetype as ratetype,ftl1_trdays as trdays,trans_type as TransMode,ftltype as FTL,ftltype AS OldFTLType,ContractId as contractid";
            strsql = strsql + " FROM dbo.webx_custcontract_frtmatrix_ftlslabhdr";
            strsql = strsql + " WHERE contractid='" + ConntractId + "'";
            strsql = strsql + " AND loc_reg='" + MatrixType + "' ";
            if (TranMode != "A")
            {
                strsql = strsql + " AND trans_type='" + TranMode + "'";
            }
            else
            {
                strsql = strsql + " AND trans_type in (SELECT * FROM dbo.Split((SELECT trans_type FROM WEBX_CUSTCONTRACT_CHARGE WHERE contractid='" + ConntractId + "'),','))";
            }
            if (MatrixType == "R")
            {
                strsql = strsql + " AND (SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=from_loccode) LIKE '" + From + "%'";
                strsql = strsql + " AND (SELECT codedesc FROM webx_master_general WHERE codetype='ZONE' AND codeid=to_loccode) LIKE '" + To + "%'";
            }
            else if (MatrixType == "L" || MatrixType == "C")
            {
                strsql = strsql + " AND from_loccode LIKE '" + From + "%'";
                strsql = strsql + " AND to_loccode LIKE '" + To + "%'";
            }
            if (ftltype != "A")
                strsql = strsql + " AND ftltype='" + ftltype + "'";

            DataTable dataTable = GF.GetDataTableFromSP(strsql);
            List<Webx_CustContract_FRTMatrix_SingleSlab> Webx_SingleList = DataRowToObject.CreateListFromTable<Webx_CustContract_FRTMatrix_SingleSlab>(dataTable);
            return Webx_SingleList;
        }

        public DataTable InsertSundryFTL(string XML, string UserName)
        {
            string str = "exec USP_INSERT_CCM_FTLFREIGHT_RATE_NewPortal '" + XML + "','" + UserName + "'";

            int Id = GF.SaveRequestServices(str.Replace("'", "''"), "InsertSundryFTL", "", "");

            DataTable dt = GF.GetDataTableFromSP(str);
            return dt;
        }
        #endregion

        #region ODA Charge

        public DataTable DeleteSlabHDR(string ContractID)
        {
            string SQLQuery = "DELETE FROM WEBX_CUSTCONTRACT_ODAMATRIX_SLABHDR WHERE contractid = '" + ContractID + "'";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "DeleteSlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;

        }

        public DataTable InsertSlabHDR(string ContractID, string RateType, string SabCode, string fromsalb, string toslab)
        {
            //Insert Record in WEBX_CUSTCONTRACT_ODAMATRIX_SLABHDR table for Slab 1
            string SQLQuery = "Insert into WEBX_CUSTCONTRACT_ODAMATRIX_SLABHDR (contractid,  matrix_type, " +
                       "slab_code, slab_from, slab_to) VALUES ('" + ContractID + "','" + RateType + "', '" + SabCode + "', " +
                       fromsalb + ", " + toslab + ")";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "InsertSlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;
        }

        public DataTable DeleteODASlabHDR(string ContractID)
        {
            string SQLQuery = "DELETE FROM WEBX_CUSTCONTRACT_ODAMATRIX_DISTHDR WHERE contractid = '" + ContractID + "'";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "DeleteODASlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;

        }

        public DataTable InsertODASlabHDR(string ContractID, string RateType, decimal ODAChrage, string SabCode, string fromsalb, string toslab)
        {
            //Insert Record in WEBX_CUSTCONTRACT_ODAMATRIX_SLABHDR table for Slab 1
            string SQLQuery = "Insert into WEBX_CUSTCONTRACT_ODAMATRIX_DISTHDR (contractid,  matrix_type, " +
                     "MIN_ODACHRG, ess_code, ess_from, ess_to) VALUES ('" + ContractID + "','" + RateType + "','" + ODAChrage + "', '" + SabCode + "', " +
                       fromsalb + ", " + toslab + ")";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "InsertODASlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;
        }

        public DataTable DeleteESSCodeSlabHDR(string ContractID)
        {
            string SQLQuery = "DELETE FROM WEBX_CUSTCONTRACT_ODAMATRIX_CHARGE WHERE contractid = '" + ContractID + "'";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "DeleteESSCodeSlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;

        }

        public DataTable InsertESSCodeSlabHDR(string ContractID, string ESS_code, string trdays, string Slab1, string Slab2, string Slab3, string Slab4, string Slab5, string Slab6)
        {

            string SQLQuery = "Insert into WEBX_CUSTCONTRACT_ODAMATRIX_CHARGE (contractid,  ESS_code, trdays, " +
                         "Slab1, Slab2, Slab3, Slab4, Slab5, Slab6) VALUES ('" + ContractID + "','" + ESS_code + "', " + trdays + ", " + Slab1 + ", " + Slab2 + ", " + Slab3 + ", " + Slab4 + ", " + Slab5 + ", " + Slab6 + ")";

            int Id = GF.SaveRequestServices(SQLQuery.Replace("'", "''"), "InsertESSCodeSlabHDR", "", "");

            DataTable dt = GF.GetDataTableFromSP(SQLQuery);
            return dt;
        }

        public List<webx_custcontract_frtmatrix_slabhdr> GetSLABHDRList(string ContractID)
        {
            string sqlstr = "select ContractId,Custcode,matrix_type,slab_code,slab_from,slab_to,contract_type from WEBX_CUSTCONTRACT_ODAMATRIX_SLABHDR where ContractId='" + ContractID + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_custcontract_frtmatrix_slabhdr> webx_custcontract_frtmatrix_slabhdr_XML = DataRowToObject.CreateListFromTable<webx_custcontract_frtmatrix_slabhdr>(dataTable);
            return webx_custcontract_frtmatrix_slabhdr_XML;
        }

        public List<webx_custcontract_frtmatrix_slabhdr> GetDistHdrList(string ContractID)
        {
            string sqlstr = "select ContractId,Custcode,matrix_type,MIN_ODACHRG,ESS_code,ESS_from,ESS_to,contract_type from WEBX_CUSTCONTRACT_ODAMATRIX_DISTHDR where ContractId='" + ContractID + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_custcontract_frtmatrix_slabhdr> webx_custcontract_frtmatrix_slabhdr_XML = DataRowToObject.CreateListFromTable<webx_custcontract_frtmatrix_slabhdr>(dataTable);
            return webx_custcontract_frtmatrix_slabhdr_XML;
        }

        public List<webx_custcontract_frtmatrix_slabhdr> GetMatrixChargeList(string ContractID)
        {
            string sqlstr = "select ContractId,ESS_code,trdays,slab1 as Slab1,slab2 as Slab2,slab3 as Slab3,slab4 as Slab4 ,slab5 as Slab5,slab6 as Slab6 from WEBX_CUSTCONTRACT_ODAMATRIX_CHARGE where ContractId='" + ContractID + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(sqlstr);
            List<webx_custcontract_frtmatrix_slabhdr> webx_custcontract_frtmatrix_slabhdr_XML = DataRowToObject.CreateListFromTable<webx_custcontract_frtmatrix_slabhdr>(dataTable);
            return webx_custcontract_frtmatrix_slabhdr_XML;
        }

        #endregion

        #region  Customer contract zone location transmode wise master

        public List<Cygnus_Custcontract_locaton_zone_transmode_wise_master> GetCustconzonelocationtransmodewisemaster(string Loccode)
        {
            string QueryString = "USP_Custconzonelocationtransmodewisemaster '" + Loccode + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<Cygnus_Custcontract_locaton_zone_transmode_wise_master> itmList = DataRowToObject.CreateListFromTable<Cygnus_Custcontract_locaton_zone_transmode_wise_master>(DT);
            return itmList;
        }

        public bool InsertCustcon_zonelocationtransmodewisemaster(string XML, string branch)
        {
            bool Status = false;
            string SQRY = "exec USP_InsertCustconzonelocationtransmodewisemaster '" + XML + "','" + branch + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertCustconzonelocationtransmodewisemaster", "", "");

            DataTable DT = GF.GetDataTableFromSP(SQRY);
            if (DT.Rows.Count > 0 && DT.Rows[0]["STATUS"].ToString() == "1")
            {
                Status = true;
            }
            return Status;
        }

        #endregion

        #region Customer Contract Template 
        public List<CYGNUS_Charges_Customer_Template> GetChargeMatrixForContractTemplate()
        {
            //string strsql = "SELECT ISNULL(defaultvalue,'Y') FROM webx_rules_docket WHERE code='CHRG_RULE'";
            string str = "SELECT ROW_NUMBER() OVER (ORDER BY (SELECT NULL)) as RNo,chargecode,chargename FROM dbo.webx_master_charge WHERE chargetype='DKT'"
            + " AND basedon='NONE' AND basecode='NONE' AND booktimeflag='Y'"
            + " AND deltimeflag='N' AND activeflag='Y' AND ChargeCode NOT IN ('SCHG10','SCHG11','SCHG12','SCHG13','SCHG14','SCHG17','SCHG20','UCHG01','UCHG02','UCHG03','UCHG04')";//,'SCHG19' Removed as per Rajeshji's Instruction with Chirag Paneliya on 07 Jun 2017 at 2:30 PM
            DataTable dataTable = GF.GetDataTableFromSP(str);
            List<CYGNUS_Charges_Customer_Template> GetChargeMatrixList = DataRowToObject.CreateListFromTable<CYGNUS_Charges_Customer_Template>(dataTable);
            return GetChargeMatrixList;
        }

        public DataTable InsertCustomerContractTemplate(string XML, string CustChargesXML, string ContractTermsXML, string BaseUserName, string BaseLocationCode)
        {
            DataTable DT = new DataTable();
            string SQRY = "exec Usp_Insert_CustomerContractTemplate '" + XML + "','" + CustChargesXML + "','" + ContractTermsXML + "','" + BaseUserName + "','" + BaseLocationCode + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertCustomerContractTemplate", "", "");
            return GF.GetDataTableFromSP(SQRY);
        }

        public List<ContractTemplateRates> GetContractTemplateRates(string CustCD, string TransitMode, string Matrix, string RateType)
        {
            string str = "EXEC USP_GetContractRates '" + CustCD + "','" + TransitMode + "','" + Matrix + "','" + RateType + "'";
            DataTable dataTable = GF.GetDataTableFromSP(str);
            List<ContractTemplateRates> List = DataRowToObject.CreateListFromTable<ContractTemplateRates>(dataTable);
            return List;
        }

        #endregion
    }
}
