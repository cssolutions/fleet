﻿using Fleet.Classes;
using Fleet.Models;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BranchAccountingService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BranchAccountingService.svc or BranchAccountingService.svc.cs at the Solution Explorer and start debugging.
    public class BranchAccountingService : IBranchAccountingService
    {
        GeneralFuncations GF = new GeneralFuncations();

        #region Master

        public List<CYGNUS_Branch_Accounting_Master> GetBRCDAccList(string UserName, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_Acc_Master '" + UserName + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Accounting_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(Dt1);
            return List;
        }

        public DataTable InserBRCDAcc(string XML, string BaseLocationCode, string UserName, string XML_BRCD)
        {
            string SQRY = "exec Usp_Insert_BRCDAcc '" + XML + "','" + BaseLocationCode + "','" + UserName + "','" + XML_BRCD + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InserBRCDAcc", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CYGNUS_Branch_Accounting_Master> GetBRCD_Acc_Master_List(string UserName, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_Acc_Master_List '" + UserName + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Accounting_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(Dt1);
            return List;
        }

        public List<CYGNUS_Branch_Expance> Get_BR_ViewPrint_List(string Type, string Bill, string FromDate, string ToDate)
        {
            string SQRY = "";
            SQRY = "exec usp_BR_ViewPrint '" + Bill + "','" + FromDate + "','" + ToDate + "','" + Type + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expance> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(Dt1);
            return List;
        }

        #endregion

        #region Electricity Expance

        #region ElectriCity Master Office Godown Details Master

        public DataTable GetOfficeGodown(string BRCD)
        {
            string SQRY = "exec GetOffice_Godown_Details '" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable InserCEOGD(string XML, int ID, string BaseLocationCode, string UserName, string Location)
        {
            string SQRY = "exec Usp_InsertCodeForOfficeGodownDetails '" + XML + "','" + ID + "','" + BaseLocationCode + "','" + UserName + "','" + Location + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InserCEOGD", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CYGNUS_Branch_Electricity_Expenses_DET> Get_ElectriCity_Detail(string BRCD, string UserName, string Adress)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Electricity_Expences '" + BRCD + "','" + UserName + "','" + Adress + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Electricity_Expenses_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Electricity_Expenses_DET>(Dt1);
            return List;
        }

        public List<CYGNUS_Elect_Office_Godown_Details> Get_ElectriCity_Address(string BRCD, string UserName, string BaseLocation)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Electricity_Branch '" + BRCD + "','" + UserName + "','" + BaseLocation + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Elect_Office_Godown_Details> List = DataRowToObject.CreateListFromTable<CYGNUS_Elect_Office_Godown_Details>(Dt1);
            return List;
        }

        #endregion
              
        #region ElectriCity Entry

        public DataTable Insert_Electricity_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Insert_Electricity_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Electricity_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region ElectriCity  Approval

        public DataTable Insert_Electricity_Expence_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Electricity_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Electricity_Expence_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region ElectriCity   Rejection

        public DataTable USP_Elec_Expance_Branch_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "";
            SQRY = "exec USP_Elec_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Electricity_Expence_Reject", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #endregion

        #region Conveyance(Local) Expenses

        #region Conveyance(Local) Insert

        public DataTable Insert_Conveyance_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Conveyance_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Conveyance_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Conveyance(Local) Approval

        public DataTable Insert_Conveyance_Expence_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Conveyance_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Conveyance_Expence_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Conveyance(Local) Rejection

        public DataTable USP_Conveyance_Expance_Branch_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "";
            SQRY = "exec USP_Conveyance_Local_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Conveyance_Expence_Reject", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Conveyance(Local) Validation

        public DataTable CheckDocumentNoStatusDetails(string DocumentNo, string Type, string OppNonOpp_Type, string BaseLocationCode, string BaseUserName)
        {
            string SQRY = "EXEC USP_CheckDocumentNoStatusDetails '" + DocumentNo + "','" + Type + "','" + OppNonOpp_Type + "','" + BaseLocationCode + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public string MonthlyPassValidation(string EmpCode, string Month)
        {
            string SQRY = "EXEC USP_MonthlyPassValidation '" + EmpCode + "','" + Month + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT.Rows[0][0].ToString();
        }

        #endregion

        #region Conveyance(Local) Expenses Approval Reject Criteria

        public List<CYGNUS_Branch_Expance> GetStaffListFor_Conveyance(string Branch, string SubmitType)
        {
            string QueryString = "exec USP_Get_Staff_Conveyance '" + Branch + "','" + SubmitType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Expance> GetMonth_For_Conveyance(string Branch, string SubmitType)
        {
            string QueryString = "exec USP_Get_Month_Conveyance '" + Branch + "','" + SubmitType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Expenses_EntryHDR> GetConveyanceAprroval_List(string Location, string Staff, int Month, string SubmitType)
        {
            string SQRY = "";
            SQRY = "exec Get_BrancAcct_Aprroval_Conveyance '" + Location + "','" + Staff + "','" + Month + "','" + SubmitType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }
        public DataSet GetBrancAcct_Aprroval_Reject_Conveyance(string No, int Bill_Status_ID)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Approv_Reject_List_Conveyance '" + No + "', '" + Bill_Status_ID + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            return DS;
        }

        #endregion

        #region Conveyance Expance Master

        public List<CYGNUS_Conveyance_Expance_Master> Get_Conveyance_Expances(string BRCD, string UserName)
        {
            string QueryString = "exec USP_Get_Conveyance_Expance'" + BRCD + "','" + UserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Conveyance_Expance_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expance_Master>(DT);
            return List;
        }

        public DataTable Insert_Conveyance_Master(string XML, string BaseLocationCode, string UserName)
        {
            string SQRY = "exec Usp_Insert_Update_Conveyance_Master '" + XML + "','" + BaseLocationCode + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Conveyance_Master", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #endregion

        #region Travelling Tour Expenses

        #region Travelling Expenses Open

        public DataTable Travelling_Expenses_Open(string XML1, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Travelling_Expence_Open '" + XML1 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Travelling_Expence_Open", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Travelling Expense Advance

        public List<CYGNUS_Branch_Travelling_Tour_HDR> GetTravelling_Expense_Advance_BillList(string BillNo)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Travelling_Expense_Advance_List '" + BillNo + "'";
            DataTable dtBillNo = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Travelling_Tour_HDR> ListBillNo = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_HDR>(dtBillNo);
            return ListBillNo;
        }

        public List<CYGNUS_Branch_Travelling_Tour_HDR> Get_Travelling_Tour_Advance_Details(string BRCD, string BillNo, DateTime FromDate, DateTime ToDate, string Staff)
        {
            string QueryString = "exec USP_Get_Travelling_Expense_Advance_BillList '" + BRCD + "','" + BillNo + "','" + FromDate + "','" + ToDate + "','" + Staff + "'";
            DataTable listDET = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Travelling_Tour_HDR> ListDETDetails = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_HDR>(listDET);
            return ListDETDetails;
        }

        #endregion

        #region BR Advance

        public DataTable BR_Advance(string BRCD, string FIN_YEAR, string XML, string CompanyCode, string YearValFirst, string Type, string DocumentNo, string UserName)
        {
            string SQRY = "";
            SQRY = "exec Usp_BR_Advance '" + BRCD + "','" + FIN_YEAR + "','" + XML + "','" + CompanyCode + "','" + YearValFirst + "','" + Type + "','" + DocumentNo + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "BR_Advance", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<CYGNUS_BR_Advance_Entry> Get_BR_AdvanceList(string DocumentNo, string ExpanceType, string BRCD, string UserName)
        {
            string QueryString = "exec USP_BR_Advance_List'" + DocumentNo + "','" + ExpanceType + "','" + BRCD + "','" + UserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_BR_Advance_Entry> itmList = DataRowToObject.CreateListFromTable<CYGNUS_BR_Advance_Entry>(DT);
            return itmList;
        }

        #endregion

        #region  Travelling Expenses Close

        public DataTable Travelling_Expenses_Close(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Travelling_Expence_Close '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Travelling_Expence_Close", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CYGNUS_Branch_Expenses_EntryHDR> Get_Travelling_Expenses_Close_List(string BillNo, string FromDate, string ToDate, string Branch, string Staff)
        {
            string SQRY = "";
            SQRY = "exec Get_Travelling_Expenses_Close_List '" + BillNo + "','" + FromDate + "','" + ToDate + "','" + Branch + "','" + Staff + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }

        #endregion

        #region Travelling Tour Entry

        public DataTable Travelling_Tour_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            //string SQRY = "exec Usp_Travelling_Tour_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            string SQRY = "exec Usp_Travelling_Tour_Expence_Close '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Travelling_Tour_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Travelling Tour Approval

        public DataTable Travelling_Tour_Expence_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Travelling_Tour_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Travelling_Tour_Expence_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Travelling Tour Rejection

        public DataTable Travelling_Tour_Expence_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Travelling_Tour_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Travelling_Tour_Expence_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Travelling Payment

        public string TravellingTour_Exp_payments(string Xml_Acccode_Details, string Xml_Other_Details, /*string IDS, int Type,*/ string Advance_XML)
        {
            string SQRY = "exec usp_TravellingTour_Exp_payments '" + Xml_Acccode_Details.Replace("&", "&amp;").Trim() + "','" + Xml_Other_Details.Replace("&", "&amp;").Trim() + "','" + Advance_XML.Replace("&", "&amp;").Trim() + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "usp_TravellingTour_Exp_payments", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            string GenVoucherno = "";
            try
            {
                GenVoucherno = DT.Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }

        #endregion

        #endregion

        #region Driver Incentive TSP Expenses Expenses

        #region Driver Incentive TSP Expenses Expenses Entry

        public DataTable Insert_DriverIncentiveTSP_Unloading_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_DriverIncentiveTSP_Unloading_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "DriverIncentiveTSP_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Driver Incentive TSP Expenses Branch Approval

        public DataTable Insert_DriverIncentiveTSP_Branch_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_DriverIncentiveTSP_Branch_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "DriverIncentiveTSP_Branch_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Driver Incentive TSP Expenses Branch Reject

        public DataTable Insert_DriverIncentiveTSP_Branch_Reject(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_DriverIncentiveTSP_Branch_Rejection '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "DriverIncentiveTSP_Branch_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Branch Approval Screen Submit

        public DataTable Insert_DriverIncentiveTSP_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_DriverIncentiveTSP_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "DriverIncentiveTSP_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region ElectriCity   Rejection

        public DataTable USP_DriverIncentiveTSP_Branch_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "";
            SQRY = "exec Usp_Insert_DriverIncentiveTSP_Expence_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "DriverIncentiveTSP_Expence_Reject", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #endregion

        #region Portel Expence Expenses

        #region Portel Expence Entry

        public DataTable InsertPortelExpence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Insert_PortelExpence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Portel_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Portel Expence Entry Approval

        public DataTable Insert_PortelExpence_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_PortelExpence_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_PortelExpence_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Portel Expence Entry Rejection

        public DataTable USP_PortelExpence_Branch_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "";
            SQRY = "exec USP_PortelExpence_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_PortelExpence_Reject", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Porter Expenses Approval Reject Criteria

        public List<CYGNUS_Branch_Expance> GetStaffListFor_Porter(string Branch, string SubmitType)
        {
            string QueryString = "exec USP_Get_Staff_PoretrExpense '" + Branch + "','" + SubmitType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Expance> GetMonth_For_Porter(string Branch, string SubmitType)
        {
            string QueryString = "exec USP_Get_Month_Porter '" + Branch + "','" + SubmitType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Expenses_EntryHDR> GetPorterAprroval_List(string Location, string Staff, int Month, string SubmitType)
        {
            string SQRY = "";
            SQRY = "exec Get_BrancAcct_Aprroval_Porter '" + Location + "','" + Staff + "','" + Month + "','" + SubmitType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }
        public DataSet GetBrancAcct_Aprroval_Reject_Porter(string No, int Bill_Status_ID)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Approv_Reject_List_Porter '" + No + "', '" + Bill_Status_ID + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            return DS;
        }

        #endregion

        #endregion

        #region Office Fixed Expenses

        #region Office Fixed Expenses Entry

        public DataTable Insert_Office_Fixed_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_OfficerExpenses_Fixed_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Fixed_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Office Fixed Expenses Approval

        public DataTable Insert_Office_Fixed_Expenses_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Officer_Fixed_Expenses_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Fixed_Expenses_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Office Fixed Expenses Reject

        public DataTable Office_Fixed_Expenses_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Officer_Fixed_Expenses_Reject '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Office_Fixed_Expenses_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Tea Expense Branch & User Wise

        public List<CYGNUS_Tea_Expense_Master> GetBranchWiseUser(string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Branch_Wise_User '" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Tea_Expense_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Tea_Expense_Master>(Dt1);
            return List;
        }
        public DataTable InsertTeaExpenseBranchUserWise(string XML, string BaseLocationCode, string UserName, string XML_BRCD, string XML_Mapped, decimal Amount)
        {
            string SQRY = "exec Usp_Insert_Tea_Expense '" + XML + "','" + BaseLocationCode + "','" + UserName + "','" + XML_BRCD + "','" + XML_Mapped + "','" + Amount + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Tea_Expense", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CYGNUS_Tea_Expense_Master> GetBranchWiseUserEdit(string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Branch_Wise_User_Edit '" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Tea_Expense_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Tea_Expense_Master>(Dt1);
            return List;
        }
        #endregion

        #endregion

        #region Office Other Expenses

        #region Office Other Expenses Entry

        public DataTable Insert_Office_Other_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_OfficerExpensesOthers_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Other_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Office Other Expenses Entry

        public DataTable Insert_Office_Other_Expenses_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Officer_Other_Expenses_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Other_Expenses_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Office Other Expenses Reject

        public DataTable Office_Other_Expenses_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Officer_Other_Expenses_Reject '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Office_Fixed_Expenses_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #endregion

        #region Food Expenses

        #region Food Expenses Entry

        public DataTable Insert_Food_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Food_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Food_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Food Expenses Approval

        public DataTable Insert_Food_Expenses_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Food_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Food_Expenses_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Food Expenses Reject

        public DataTable Food_Expenses_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Food_Expenses_Reject '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Food_Expenses_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #endregion

        #region Detension Expenses Expenses

        #region Detension Expenses Entry

        public DataTable Insert_Detension_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_Detension_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Detension_Expence_Entry", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Detension Expenses Approval

        public DataTable Insert_Detension_Expenses_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_Detension_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Detension_Expenses_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Detension Expenses Reject

        public DataTable Detension_Expenses_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Detension_Expenses_Reject '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Detension_Expenses_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Vehicle No Change

        public DataTable Check_Vehicle_StatusDetails(string DocumentNo,string For_The_Month_Of)
        {
            string SQRY = "EXEC USP_Vehicle_No_Details '" + DocumentNo + "','" + For_The_Month_Of + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #endregion

        #region  Market Vehicle Expence

        #region Market Vehicle Expence Entry

        public DataTable Insert_MarketVehicleCharges_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_MarketVehicleCharges_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Usp_MarketVehicleCharges_Expence", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Market Vehicle Expence Approval

        public DataTable Insert_MarketVehicleCharges_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_MarketVehicleCharges_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_MarketVehicleCharges_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Market Vehicle Expence Rejection

        public DataTable Insert_MarketVehicleCharges_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "";
            SQRY = "exec USP_MarketVehicleCharges_Rejection '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MarketVehicleCharges_Expence_Reject", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #endregion

        #region Tripsheet Disputed Amount

        public List<CYGNUS_Tripsheet_Disputed_Amount_DET> GetPARTICULAR_TripsheetDisputedAmountDetail()
        {
            string SQRY = "";
            SQRY = "exec GET_PARTICULAR_TripsheetDisputedAmount ";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Tripsheet_Disputed_Amount_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_Tripsheet_Disputed_Amount_DET>(Dt1);
            return List;
        }

        public List<WEBX_FLEET_DRIVERMST> GetDriverList(string Prefix)
        {
            string SQRY = "";
            SQRY = "exec USP_GET_DRIVER_LIST '" + Prefix + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<WEBX_FLEET_DRIVERMST> List = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(Dt1);
            return List;
        }

        #region Tripsheet Disputed Amount Expenses Entry

        public DataTable Insert_TripsheetDisputedAmount(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_TripsheetDisputedAmount '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_TripsheetDisputedAmount", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Tripsheet Disputed Amount Expenses Approval

        public DataTable Insert_TripsheetDisputedAmount_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_TripsheetDisputedAmount_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Usp_TripsheetDisputedAmount_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Tripsheet Disputed Amount Expenses Reject

        public DataTable Tripsheet_Disputed_Amount_Expenses_Reject(string Brcd, string UserName, string BillNo, int ExpenceType, string XML)
        {
            string SQRY = "exec Usp_Insert_Tripsheet_Disputed_Amount_Expenses_Reject '" + Brcd + "','" + UserName + "','" + BillNo + "','" + ExpenceType + "','" + XML + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Tripsheet_Disputed_Amount_Expenses_Reject", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Tripsheet No Validation

        public DataTable Check_Tripsheet_No_StatusDetails(string DocumentNo, string BaseLocationCode, string BaseUserName)
        {
            string SQRY = "EXEC USP_Check_Tripsheet_No_Details '" + DocumentNo + "','" + BaseLocationCode + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #endregion

        #region Expenses Submit

        public DataTable Insert_HandlingLoading_Unloading_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_HandlingLoading_Unloading_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_HandlingrLoading_Unloading_Expence", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Insert_OfficerExpenses_Expence(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear)
        {
            string SQRY = "exec Usp_OfficerExpensesFixedOthers_Expence '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Usp_OfficerExpensesFixedOthers_Expence", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public List<CYGNUS_HandlingLoading_Unloading_Expence_DET> GetBRCD_HandlingLoading_Detail(int Type, string NO, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_ACCT_HDR_Detail '" + Type + "','" + NO + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_HandlingLoading_Unloading_Expence_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_HandlingLoading_Unloading_Expence_DET>(Dt1);
            return List;
        }

        public List<CYGNUS_MarketVehicleCharges_Expence_DET> GetBRCD_MarketVehicleCharges_Detail(int Type, string NO, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_ACCT_HDR_Detail '" + Type + "','" + NO + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_MarketVehicleCharges_Expence_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_MarketVehicleCharges_Expence_DET>(Dt1);
            return List;
        }

        #endregion

        #region Expenses Approval Submit
                
        public DataTable Insert_HandlingLoading_Unloading_Expenses_Approval(string XML1, string XML2, string BaseLocationCode, string UserName, string FinYear, string BillNo, int EXpenceType)
        {
            string SQRY = "exec Usp_Insert_HandlingLoading_Unloading_Expence_Approval '" + XML1 + "','" + XML2 + "','" + BaseLocationCode + "','" + UserName + "','" + FinYear + "','" + BillNo + "','" + EXpenceType + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_HandlingLoading_Unloading_Expenses_Approval", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetCHEAKPorterExpense(string BRCD)
        {
            string SQRY = "exec USP_CHEAKPorterExpenseRateCharged '" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        #region Accounting

        public string Insert_Branch_Voucher_Details(string Xml_Acccode_Details, string Xml_Other_Details, string IDS, int Type)
        {
            string SQRY = "exec usp_Generate_ManualAccountDetail_Ver4_BRCD_Accouting '" + Xml_Acccode_Details.Replace("&", "&amp;").Trim() + "','" + Xml_Other_Details.Replace("&", "&amp;").Trim() + "','" + IDS + "','" + Type + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Branch_Voucher_Details", "", "");
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            string GenVoucherno = "";
            try
            {
                GenVoucherno = DT.Rows[0][0].ToString();
            }
            catch (Exception e)
            {
                throw e;
            }

            return GenVoucherno;
        }

        #endregion

        #region Get Table and View Name

        public DataTable Get_BranchAccounting_Title(string BRCD, string UserName, int ExpanceType, string Type, string FinYear)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BranchAccounting_Title '" + BRCD + "','" + UserName + "','" + ExpanceType + "','" + Type + "','" + FinYear + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Get IS Bank Details Available

        public DataTable Get_ISBank_Details_Available(string BRCD, string UserName, int ExpanceType, string Empcode, int IS_Emp, int IS_Vend, int IS_Driver, string Other_Type)
        {
            string SQRY = "";
            SQRY = "exec USP_ISBank_Details_Available '" + BRCD + "','" + UserName + "','" + ExpanceType + "','" + Empcode + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Validations

        public List<CYGNUS_Branch_Expenses_EntryHDR> GetBRCDPortelExp(string NO, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_ACCT_TotalCount '" + NO + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }

        public DataTable Check_ValidNo(int Type, string NO, string Location)
        {
            string SQRY = "";
            SQRY = "exec USP_Check_ValidNo '" + Type + "','" + NO + "','" + Location + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }

        public List<CYGNUS_Branch_Expenses_EntryDET> GetBRCD_ACCT_HDR_Detail(int Type, string NO, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_ACCT_HDR_Detail '" + Type + "','" + NO + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryDET> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(Dt1);
            return List;
        }

        public List<CYGNUS_DriverIncentiveTSP_Expense_DET> GetBRCD_DriverIncentiveTSP_Detail(int Type, string NO, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BRCD_ACCT_HDR_Detail '" + Type + "','" + NO + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_DriverIncentiveTSP_Expense_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_DET>(Dt1);
            return List;
        }

        public List<CYGNUS_Branch_Expenses_EntryHDR> GetBrancAcct_Aprroval_List(string FromDate, string ToDate, string UserName, string ExpenceType, string Brcd, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "exec Get_BrancAcct_Aprroval_List '" + FromDate + "','" + ToDate + "','" + UserName + "','" + ExpenceType + "','" + Brcd + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }

        public DataSet GetBrancAcct_Aprroval_Reject(string No, string Brcd, string UserName, int ExpenseType)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Approv_Reject_List '" + No + "','" + Brcd + "','" + UserName + "','" + ExpenseType + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            return DS;
        }

        public DataTable USP_Branch_Reject(string No, string Brcd, string UserName, int ExpenceType)
        {
            string SQRY = "";
            SQRY = "exec USP_Branch_Reject '" + No + "','" + Brcd + "','" + UserName + "','" + ExpenceType + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<CYGNUS_Branch_Accounting_Master> GetBRCDAcc_FormBill(string BillNo, int Type, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_GetBRCDAcc_FormBill '" + BillNo + "','" + Type + "','" + BRCD + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Accounting_Master> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(Dt1);
            return List;
        }

        public List<webx_acctinfo> GetAccountCodeList_Expence_FromSearch()
        {
            string SQR = "exec USP_GetAccountCodeList_Expence '','1'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_acctinfo> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt);
            return GetCustomerVendorList;
        }

        public DataSet GetBrancAcct(string No, string Brcd, string UserName, int ExpenceType)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Acconting_Process_List '" + No + "','" + Brcd + "','" + UserName + "','" + ExpenceType + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            return DS;
        }

        #region Office Other_Fixed Expense Binding

        public List<CYGNUS_Branch_Office_Expenses_Other_DET> GetBRCD_ACCT_OtherExp_Det_Detail(string BRCD, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Other_Charges '" + BRCD + "','" + UserName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Office_Expenses_Other_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_DET>(Dt1);
            return List;
        }

        public List<CYGNUS_Branch_Office_Expenses_Fixed_DET> GetBRCD_ACCT_FixedExp_Det_Detail(string Location, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Fixed_Charges '" + Location + "','" + UserName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Office_Expenses_Fixed_DET> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_DET>(Dt1);
            return List;
        }

        public DataTable Get_OfficeExpance_HDR_Details(string Location, string UserName, int ExpanceType)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_OfficeExpance_HDR_Details '" + Location + "','" + UserName + "','" + ExpanceType + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            return Dt1;
        }
        public List<webx_VENDOR_HDR> Get_Vendor_BranchWise(string Serach, string Location, string UserName, string Type)
        {
            string SQR = "exec USP_Get_BranchWise_Active_Vendor_List '" + Serach + "','" + Location + "','" + UserName + "','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_VENDOR_HDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);

            return GetCustomerVendorList;
        }

        #endregion

        #endregion

        public List<CYGNUS_Branch_Expenses_EntryHDR> Get_DriverIncetive_List(string FromDate, string ToDate, string UserName, string ExpenceType, string Brcd, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "exec Get_BrancAcct_DriverIncetive_List '" + FromDate + "','" + ToDate + "','" + UserName + "','" + ExpenceType + "','" + Brcd + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }

        public List<CYGNUS_Branch_Expenses_EntryHDR> GetBrancAcct_AprrovalComplite_List(string ExpenceType, string Brcd, string FromDate, string ToDate, string BaseLocationCode)
        {
            string SQRY = "";
            SQRY = "exec USP_BrancAcct_Aprroval_List '" + ExpenceType + "','" + Brcd + "','" + FromDate + "','" + ToDate + "','" + BaseLocationCode + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expenses_EntryHDR> List = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(Dt1);
            return List;
        }

        public DataSet GetBrancAcct_Aprroval(string No, string Brcd, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Approv_List '" + No + "','" + Brcd + "','" + UserName + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            return DS;
        }

        #region Office Godown Details Master

        public DataTable Check_EqupmentGodown(string BRCD)
        {
            string SQRY = "exec [CheckBaseLocationCodeForEquipment] '" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }


        public DataTable Check_OfficeGodown(string BRCD)
        {
            string SQRY = "exec CheckBaseLocationCodeForOfficeGodownDetails '" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }


        #endregion

        #region Branch Accounting Master Master

        public List<CYGNUS_Master_Office_Other_Expenses> List_Other_Office_Expense(string Location, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Office_Expence_Data '" + Location + "','" + UserName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Master_Office_Other_Expenses> List = DataRowToObject.CreateListFromTable<CYGNUS_Master_Office_Other_Expenses>(Dt1);
            return List;
        }

        public List<CYGNUS_Master_Office_Fixed_Expenses> List_Fixed_Office_Expense(string Location, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Fixed_Office_Expence_Data '" + Location + "','" + UserName + "'";
            DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Master_Office_Fixed_Expenses> List = DataRowToObject.CreateListFromTable<CYGNUS_Master_Office_Fixed_Expenses>(Dt1);
            return List;
        }

        public DataTable Insert_Office_Expense_Other(string XML, string BaseLocationCode, string UserName)
        {
            string SQRY = "exec Usp_InsertUpdate_Office_Other_Expense '" + XML + "','" + BaseLocationCode + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Expense_Other", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Insert_Office_Expense_Fixed(string XML, string BaseLocationCode, string UserName)
        {
            string SQRY = "exec Usp_InsertUpdate_Office_Fixed_Expense '" + XML + "','" + BaseLocationCode + "','" + UserName + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Insert_Office_Expense_Fixed", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        public List<CYGNUS_Porter_Expense> GetPorterExpenseMasterDetails(string BRCD)
        {
            string SQRY = "select * from CYGNUS_Porter_Expense WHERE Location='" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Porter_Expense> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_Porter_Expense>(Dt);
            return ItemList;
        }

        public DataTable GetPorterExpenseMasterList(string BRCD)
        {
            string SQRY = "select * from CYGNUS_Porter_Expense WHERE BRCD='" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Porter_Master_Expense(string XML1, string BaseUserName, string BaseLocationCode, string Location)
        {
            string SQRY = "exec Usp_CYGNUS_Porter_Expense_Submit '" + XML1 + "','" + BaseUserName + "','" + BaseLocationCode + "','" + Location + "' ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Porter_Master_Expense", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable Get_PorterData_Details(string Porter_Type, string NameOfStation, string UserName, string BRCD)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_PorterData '" + Porter_Type + "','" + NameOfStation + "','" + UserName + "','" + BRCD + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<webx_CUSTHDR> GetCustomerUserList_BranchWise_FromSearch(string Serach, string Type, string BRCD)
        {
            string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal_BranchWise '" + Serach + "','1','" + Type + "','" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);

            return GetCustomerVendorList;
        }

        public List<webx_CUSTHDR> Get_CustomerList_TypeWise(string Serach, string Type, string BRCD, string SubType)
        {
            string SQR = "exec USP_CustomerList_TypeWise '" + Serach + "','1','" + Type + "','" + BRCD + "','" + SubType + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);
            List<webx_CUSTHDR> GetCustomerVendorList = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(Dt);
            return GetCustomerVendorList;
        }

        public DataTable GetIs_Office_Exepance_Available(int Type, int Month, int Year, string BRCD, string Name, string ExpenseType, int id, string EMPCode = null)
        {
            string SQRY = "";
            SQRY = "exec USP_Is_Office_Exepance_Available '" + Type + "','" + Month + "','" + Year + "','" + BRCD + "','" + Name + "','" + ExpenseType + "','" + id + "','" + EMPCode + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #region Electric Equipment's/Gadgets

        public DataTable InsertElectricEquipmentsGadgets(string XML1, string BaseUserName, string BaseLocationCode)
        {
            string SQRY = "exec Usp_CYGNUS_Electric_Equipments_Gadgets '" + XML1 + "','" + BaseUserName + "','" + BaseLocationCode + "' ";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "Usp_CYGNUS_Electric_Equipments_Gadgets", "", "");
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        public DataTable GetElctricID(string BRCD)
        {
            string SQRY = "select * from CYGNUS_Electric_Equipments_Gadgets WHERE BRCD='" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }

        #endregion

        public void DoWork()
        {
        }

        public DataTable GetCharge_Max_Limit(string BRCD, int ChargeID, string UserName)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Charge_MaxLimit '" + BRCD + "','" + ChargeID + "','" + UserName + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #region Common In Use DRiver List Get,Driver Bank Details, User Bank Details, Vendor Bank Details, Get Report Name

        public string Get_BranchAccounting_Report_Name(string Name, string Location, string Type)
        {
            string sqlstr = "exec Usp_BranchAccounting_ViewPrint '" + Name + "','" + Location + "','" + Type + "'";

            string Report;
            try
            {
                Report = Convert.ToString(GF.executeScalerQuery(sqlstr));
            }
            catch (Exception)
            {
                Report = "";
            }
            return Report;
        }

        public List<WEBX_FLEET_DRIVERMST> Get_Driver_BranchWise(string Serach, string Location, string UserName, int Type)
        {
            string SQR = "exec USP_Get_BranchWise_Active_Driver_List '" + Serach + "','" + Location + "','" + UserName + "','" + Type + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQR);

            List<WEBX_FLEET_DRIVERMST> GetCustomerVendorList = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST>(Dt);

            return GetCustomerVendorList;
        }

        public DataTable Get_Bank_Details(string No, string Location, string UserName, int Type)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BankDetails '" + No + "','" + Location + "','" + UserName + "','" + Type + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        //public DataTable GetUser_Detailes(string No, string No, string No, int No)
        //{
        //    string SQRY = "";
        //    SQRY = "exec USP_Get_UserBankDetails '" + No + "'";
        //    DataTable DT = GF.GetDataTableFromSP(SQRY);
        //    return DT;
        //}

        public DataTable Get_Vendor_Details(string No)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_Vendor_BankDetails '" + No + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        public List<Webx_Master_General> Get_Month(string Location, string UserName, string year, int IsPastYear, int IsFutureYear, int IsPastYear_Month, int IsFutureYear_Month, int IsEdit)
        {
            string QueryString = "exec USP_GetMonth '" + Location + "','" + UserName + "','" + year + "','" + IsPastYear + "','" + IsFutureYear + "','" + IsPastYear_Month + "','" + IsFutureYear_Month + "','" + IsEdit + "'";
            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
            List<Webx_Master_General> List = DataRowToObject.CreateListFromTable<Webx_Master_General>(dataTable);
            return List;
        }


        #endregion
      

        #region Bill Details

        public DataTable GetBill_Detailes(string No)
        {
            string SQRY = "";
            SQRY = "exec USP_Get_BillStatusDetails '" + No + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }

        #endregion

        #region Branch Accounting

        public List<CYGNUS_Branch_Expance> BRStatusSummary(int ExpenseType, string BRCD, string FromDate, string ToDate)
        {
            string SQRY = "exec USP_BR_Summary_Branch '" + ExpenseType + "','" + BRCD + "','" + FromDate + "','" + ToDate + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expance> ItemList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return ItemList;
        }
        public DataTable BRStatusSummaryCashOnToday(string BRCD, string FromDate, string ToDate, string basefinyear, decimal NetAmt)
        {
            string SQRY = "exec WEBX_DR_VR_Trn_Halt '" + BRCD + "','" + FromDate + "','" + ToDate + "','" + basefinyear + "','" + 0 + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            return DT;
        }
        public List<CYGNUS_Branch_Expance> GetStaffList(string ExpenseType, string Branch, int DllType)
        {
            string QueryString = "exec USP_Get_Staff_Vendor_List '" + ExpenseType + "','" + Branch + "','" + DllType + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(DT);
            return itmList;
        }

        public List<CYGNUS_Branch_Expance> GET_BR_Dashbord_Report_Approval(string BRCD)
        {
            string SQRY = "exec USP_BR_Dashbord_Report_Approval '" + BRCD + "'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Branch_Expance> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expance>(Dt);
            return itmList;
        }

        #endregion

        public List<webx_location> GetAllLocations(string searchTerm)
        {
            string QueryString = "exec USP_Get_Location'" + searchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_location> itmList = DataRowToObject.CreateListFromTable<webx_location>(DT);
            return itmList;
        }

        #region BR Reports

        public List<CYGNUS_Branch_Accounting_Master> GetAllExpense()
        {
            string QueryString = "exec USP_GETEXPENSETYPE";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Accounting_Master> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Accounting_Master> GetAllBRStatus(string searchTerm)
        {
            string QueryString = "exec USP_GETBR_Status'" + searchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Accounting_Master> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(DT);
            return itmList;
        }
        public List<webx_rutmas> GetAllRoute(string searchTerm)
        {
            string QueryString = "exec USP_Route'" + searchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<webx_rutmas> itmList = DataRowToObject.CreateListFromTable<webx_rutmas>(DT);
            return itmList;
        }
        public List<CYGNUS_Branch_Accounting_Master> GetSubHeadExpense(string searchTerm)
        {
            string QueryString = "exec USP_GetFixedExpense'" + searchTerm + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            List<CYGNUS_Branch_Accounting_Master> itmList = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Accounting_Master>(DT);
            return itmList;
        }

        #endregion

        #region  DriverIncentiveTSP_Expense_Master

        public DataTable GetDriverIncentiveTSP_Expense()
        {
            string SQRY = "exec USP_GET_DriverIncentiveTSP";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            return Dt;
        }
        public DataTable AddEDITDriverIncentiveTSP_Expense(string InnerXml, string BRCD, string BaseUserName)
        {
            string QueryString = "EXEC USP_AddEDITDriverIncentiveTSP_Expense'" + InnerXml + "','" + BRCD + "','" + BaseUserName + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            return DT;
        }
        #endregion
    }
}
