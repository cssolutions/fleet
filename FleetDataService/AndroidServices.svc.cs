﻿using Fleet.Classes;
using Fleet.ViewModels;
using FleetDataService.Models;
using CYGNUSDataService.Classes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Serialization;

namespace FleetDataService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AndroidServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AndroidServices.svc or AndroidServices.svc.cs at the Solution Explorer and start debugging.
    public class AndroidServices : IAndroidServices
    {
        GeneralFuncations GF = new GeneralFuncations();
        GeneralFuncations Live_GF = new GeneralFuncations();
        //Live_GeneralFuncations Live_GF = new Live_GeneralFuncations();
        string TestConnstr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public class WCFLoginModel
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public bool RememberMe { get; set; }
            public string APKVersion { get; set; }
            public string IMEINumber { get; set; }
        }

        public Stream RCPL_Login(Stream stream)
        {
            WCFLoginModel newENQ = GF.StreamToObject<WCFLoginModel>(stream);
            WebX_Master_Users WMU = new WebX_Master_Users();
            List<WebX_Master_Users> ListWMU = new List<WebX_Master_Users>();
            string str = "{IMEINumber:" + newENQ.IMEINumber + ",RememberMe:" + newENQ.RememberMe + ",UserName:" + newENQ.UserName + ",Password:" + newENQ.Password + "}";

            string Module = "Login";

            int ID = GF.SaveRequestServices(str.Replace("'", "''"), Module, "", "");

            if (ID == 0)
            {
                ListWMU = ListUser();
                WMU = ListWMU.First();
                WMU.Message = "Please Enter Valid Username Password";

                GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                return GF.ReturnJSONStream<WebX_Master_Users>(WMU);
            }

            string IMEICount = "exec USP_Check_ImeiValidation '" + newENQ.UserName + "','" + newENQ.IMEINumber + "'";
            DataTable IMEIDT = GF.GetDataTableFromSP(IMEICount);
            int IMEI_Count = Convert.ToInt32(IMEIDT.Rows[0][0].ToString());

            if (IMEI_Count == 0)
            {
                ListWMU = ListUser();
                WMU = ListWMU.First();
                WMU.Message = "Your Device is Not Register With RCPL Team. Please Contact RCPL Support MR.Rajesh Chaudhary.";
                GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                return GF.ReturnJSONStream<WebX_Master_Users>(WMU);
            }

            string DecryptPassword = "";
            string Password = "";
            string psSult = ConfigurationManager.AppSettings["PasswordSult"].ToString();
            string sql = "select Count(*) as Cnt from WebX_Master_Users where LTRIM(RTRIM(UserId))='" + newENQ.UserName.Trim() + "'";

            SqlConnection conn = new SqlConnection(TestConnstr);
            SqlCommand cmd = new SqlCommand(sql, conn);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            string Count = "";
            if (reader.HasRows)
            {
                reader.Read();
                Count = reader["Cnt"].ToString();
            }
            else
            {
                ListWMU = ListUser();
                WMU = ListWMU.First();
                WMU.Message = "Please Enter Valid Username Password";
                GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                return GF.ReturnJSONStream<WebX_Master_Users>(WMU);
            }

            reader.Close();
            int Count_Int = Convert.ToInt16(Count);

            if (Count_Int == 0)
            {
                ListWMU = ListUser();
                WMU = ListWMU.First();
                WMU.Message = "Please Enter Valid Username Password";
                return GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
            }
            else
            {
                string sql_Name = "select top 1 userpwd from WebX_Master_Users where LTRIM(RTRIM(UserId))='" + newENQ.UserName.Trim() + "'";

                SqlCommand cmd_Pass = new SqlCommand(sql_Name, conn);
                SqlDataReader reader_Pass = cmd_Pass.ExecuteReader();

                if (reader_Pass.HasRows)
                {
                    reader_Pass.Read();
                    DecryptPassword = reader_Pass["userpwd"].ToString();
                }

                reader_Pass.Close();
                Password = Decrypt(DecryptPassword, psSult);
                if (newENQ.Password != Password)
                {
                    ListWMU = ListUser();
                    WMU = ListWMU.First();
                    WMU.Message = "Password Does Not Match";
                    return GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                }
            }

            string UserLogin = "exec USP_LoginUser '" + newENQ.UserName.Trim() + "','" + DecryptPassword.Trim() + "'";
            DataTable dt = GF.GetDataTableFromSP(UserLogin);

            int IDS = GF.SaveRequestServices(UserLogin.Replace("'", "''"), "USP_LoginUser", "0", "");


            try
            {
                if (dt.Rows.Count > 0)
                {
                    WMU.BrachName = dt.Rows[0][0].ToString();
                    WMU.UserId = dt.Rows[0][1].ToString();
                    WMU.User_Type = dt.Rows[0][2].ToString();
                    WMU.BranchCode = dt.Rows[0][4].ToString();
                    WMU.Name = dt.Rows[0][5].ToString();
                    WMU.EmpId = dt.Rows[0][8].ToString();
                    WMU.EmailId = dt.Rows[0][10].ToString();
                    WMU.PhoneNo = dt.Rows[0][11].ToString();
                    WMU.Menu_1 = dt.Rows[0]["Menu_1"].ToString();
                    WMU.Menu_2 = dt.Rows[0]["Menu_2"].ToString();
                    WMU.Menu_3 = "OFFLINE DOCKET DELIVERY";

                    if (WMU.BrachName == null || WMU.BrachName == "")
                    {
                        ListWMU = ListUser();
                        WMU = ListWMU.First();
                        WMU.Message = "Branch Name Not Valid";
                        return GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                    }
                    else
                    {
                        WMU.Message = "DONE";
                    }

                    return GF.ReturnJSONStream<WebX_Master_Users>(WMU);
                }
                else
                {
                    ListWMU = ListUser();
                    WMU = ListWMU.First();
                    WMU.Message = "UserName and Password does not Match !!";
                    GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
                }
            }
            catch (Exception)
            {
                ListWMU = ListUser();
                WMU = ListWMU.First();
                WMU.Message = "UserName and Password does not Match or Branch Not Valid !!";
                GF.ReturnJSONStreamError<WebX_Master_Users>(WMU, "UserName and Password does not Match !!");
            }
            return stream;
        }

        public static string Decrypt(string strEncrypted, string strKey)
        {
            try
            {
                TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();

                byte[] byteHash, byteBuff;
                string strTempKey = strKey;

                byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                objHashMD5 = null;
                objDESCrypto.Key = byteHash;
                objDESCrypto.Mode = CipherMode.ECB; //CBC, CFB

                byteBuff = Convert.FromBase64String(strEncrypted);
                string strDecrypted = ASCIIEncoding.ASCII.GetString(objDESCrypto.CreateDecryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                objDESCrypto = null;

                return strDecrypted;
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        public List<WebX_Master_Users> ListUser()
        {

            List<WebX_Master_Users> ListUsers = new List<WebX_Master_Users>();

            WebX_Master_Users objUser = new WebX_Master_Users();
            objUser.BrachName = "";
            objUser.UserId = "";
            objUser.User_Type = "";
            objUser.BranchCode = "";
            objUser.Name = "";
            objUser.EmpId = "";
            objUser.EmailId = "";
            objUser.PhoneNo = "";
            //objUser.Message = "Not Done";
            //objUser.Message = "";

            ListUsers.Add(objUser);
            return ListUsers;
        }

        #region POD

        #region Get APK Version OF POD

        public string GetPODAPPVersion()
        {
            string Version = "0";
            try
            {
                try
                {
                    /*                     
                        INSERT INTO Webx_Master_General(CodeType,CodeId,CodeDesc,CodeAccess,StatusCode,EntryDate,EntryBy)
                        VALUES('PODVersion','1','1','U','Y',GETDATE(),'10001')
                    */
                    string SQRY = "SELECT codedesc FROM Webx_Master_General where CodeType='PODVersion'";
                    int IDS = GF.SaveRequestServices(SQRY.Replace("'", "''"), "GETPODVersion", "0", "");

                    DataTable DT = GF.GetDataTableFromSP(SQRY);
                    Version = DT.Rows[0][0].ToString();
                    return Version;
                }
                catch (Exception)
                {
                    return Version;
                }
            }
            catch (Exception)
            {
                return Version;
            }
        }

        #endregion

        #region CHECK GC VALIDATION FOR POD UPDATE

        public List<Webx_FM_Scan_Documents_Android> CheckGCValidationForPODUpdateDetail(string GCNO)
        {
            string SQRY = "exec Usp_CheckGCForPODUpload '" + GCNO + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_FM_Scan_Documents_Android> List = DataRowToObject.CreateListFromTable<Webx_FM_Scan_Documents_Android>(dataTable);
            return List;
        }

        public Stream CheckGCValidationForPODUpdate(string GCNO)
        {
            List<Webx_FM_Scan_Documents_Android> List = CheckGCValidationForPODUpdateDetail(GCNO);
            return GF.ReturnJSONStream<List<Webx_FM_Scan_Documents_Android>>(List);
        }

        #endregion

        #region GET GC FOR POD UPDATE

        public List<Webx_FM_Scan_DocumentsDone> GetGCForPODUpdateDetail(string GCNO, string ImageName)
        {
            string SQRY = "exec [Webx_SP_FM_ScanDocuments_new_NewPortal] '1','" + GCNO + "','','0','" + ImageName + "',''";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_FM_Scan_DocumentsDone> List = DataRowToObject.CreateListFromTable<Webx_FM_Scan_DocumentsDone>(dataTable);
            return List;
        }

        public Stream GetGCForPODUpdate(string GCNO, string ImageName)
        {
            List<Webx_FM_Scan_DocumentsDone> List = new List<Webx_FM_Scan_DocumentsDone>();
            Webx_FM_Scan_DocumentsDone FMSD = new Webx_FM_Scan_DocumentsDone();

            try
            {
                List = GetGCForPODUpdateDetail(GCNO, ImageName);
                return GF.ReturnJSONStream<List<Webx_FM_Scan_DocumentsDone>>(List);
            }
            catch (Exception ex)
            {
                FMSD.Status = ex.Message;
                List.Add(FMSD);
                return GF.ReturnJSONStream<List<Webx_FM_Scan_DocumentsDone>>(List);

            }
        }

        #endregion

        #region POD Upload Image

        ///* P_GCNO_1.JPG */
        ///* P_GCNO_2.JPG */
        //public string POD_UploadImage(Stream fileContents)
        //{
        //    MultipartParser parser = new MultipartParser(fileContents);
        //    string path = "", Type = "";
        //    var DocumentUploadedPath = "";
        //    try
        //    {
        //        string fileName1 = "", contentType = "";
        //        System.Drawing.Image image;
        //        try
        //        {
        //            fileName1 = parser.Filename;
        //            string[] Id_Arr = fileName1.Split('_');

        //            string Prefix = Id_Arr[0];
        //            string GcNo = Id_Arr[1];

        //            string[] Id1_Arr = Id_Arr[2].Split('.');

        //            string ImageOrder = Id1_Arr[0];
        //            string ImageType = Id1_Arr[1];

        //            var fileName = Path.GetFileName(fileName1);

        //            fileName1 = parser.Filename;
        //            contentType = parser.ContentType;
        //            byte[] fileContent = parser.FileContents;
        //            image = byteArrayToImage(fileContent);

        //            string FolDerPath = "E:\\Cygnus Projects\\RCPL\\RCPL_TEST\\Fleet\\Images\\FMScanDocument\\FM\\" + GcNo;
        //            string Image_Path = FolDerPath + "\\" + fileName1;

        //            if (!Directory.Exists(FolDerPath))
        //                Directory.CreateDirectory(FolDerPath);

        //            string QueryString = "exec Webx_SP_FM_ScanDocuments_new '1','" + GcNo + "','" + GcNo + "','" + 0 + "','" + fileName + "',''";
        //            DataTable dataTable = GF.GetDataTableFromSP(QueryString);

        //            image.Save(Image_Path);
        //            /*
        //            DocumentUploadedPath = AzureStorageHelper.UploadBlobFile_Test("1", parser, fileName, "CYGNUSTEAM");

        //            string QueryString = "exec Webx_SP_FM_ScanDocuments_NewPortal '1','" + GcNo + "','" + GcNo + "','" + 0 + "','" + DocumentUploadedPath + "','CYGNUSTEAM','HQTR',''";
        //            DataTable dataTable = GF.GetDataTableFromSP(QueryString);
        //            */
        //        }
        //        catch (Exception ex)
        //        {
        //            return "ERROR ex";
        //        }

        //        return "DONE";
        //    }
        //    catch (Exception ex)
        //    {
        //        return "ERROR 456" + ex.Message;
        //    }
        //}

        /* Azure Portal POD Upload */
        public string POD_UploadImage123(Stream fileContents)
        {
            MultipartParser parser = new MultipartParser(fileContents);
            var DocumentUploadedPath = "";
            string sopFileLocation = "";
            try
            {
                string fileName1 = "", contentType = "", UserName = "", Branch = "";
                string[] Id1_Arr;
                System.Drawing.Image image;
                try
                {
                    fileName1 = parser.Filename;
                    string[] Id_Arr = fileName1.Split('_');

                    string Prefix = Id_Arr[0];
                    string GcNo = Id_Arr[1];
                    try
                    {
                        UserName = Id_Arr[2];
                        Branch = Id_Arr[3];
                        Id1_Arr = Id_Arr[3].Split('.');
                    }
                    catch (Exception)
                    {
                        Id1_Arr = Id_Arr[2].Split('.');
                    }

                    string ImageOrder = Id1_Arr[0];
                    string ImageType = Id1_Arr[1];
                    Branch = ImageOrder;

                    var fileName = Path.GetFileName(fileName1);

                    fileName1 = parser.Filename;
                    contentType = parser.ContentType;
                    byte[] fileContent = parser.FileContents;
                    image = byteArrayToImage(fileContent);

                    /* Server URL */
                    string FolDerPath = "E:\\Projects\\RCPL\\RCPL Service\\Service\\Images\\" + GcNo;

                    /* PC URL */
                    //string FolDerPath = "E:\\Cygnus Projects\\RCPL\\RCPL_TEST\\FleetDataService\\Images\\" + GcNo;
                    string Image_Path = FolDerPath + "\\" + fileName1;

                    if (!Directory.Exists(FolDerPath))
                        Directory.CreateDirectory(FolDerPath);
                    string DocumentType = "POD";

                    sopFileLocation = DocumentType + "/" + System.DateTime.Now.ToString("yyyy/MMM") + "/" + fileName;
                    //string QueryString = "exec Webx_SP_FM_ScanDocuments_new_Android '1','" + GcNo + "','" + GcNo + "','" + 0 + "','" + fileName + "','','" + UserName + "','" + Branch + "'";
                    //DocumentUploadedPath = AzureStorageHelper.UploadBlobFile_Test(DocumentType, parser, fileName, "CYGNUSTEAM", image);

                    string QueryString = "exec Webx_SP_FM_ScanDocuments_new_Android '1','" + GcNo + "','" + GcNo + "','" + 0 + "','" + DocumentUploadedPath + "','','" + UserName + "','" + Branch + "'";
                    int ID1 = GF.SaveRequestServices(QueryString.Replace("'", "''"), "POD_Upload", "", "");
                    DataTable dataTable = GF.GetDataTableFromSP(QueryString);
                    image.Save(Image_Path);
                }
                catch (Exception ex)
                {
                    return "ERROR :-" + ex.Message.ToString();
                }

                string Link = "https://rcplstorageaccount.blob.core.windows.net/rcpl/" + sopFileLocation;

                return "DONE___" + Link;
            }
            catch (Exception ex)
            {
                return "ERROR 456" + ex.Message;
            }
        }


        public string POD_UploadImage(Stream fileContents)
        {
            MultipartParser parser = new MultipartParser(fileContents);
            try
            {
                string fileName1 = "", contentType = "", UserName = "", Branch = "";
                System.Drawing.Image image;
                try
                {
                    string[] Id1_Arr;
                    fileName1 = parser.Filename;
                    string[] Id_Arr = fileName1.Split('_');
                    string Prefix = Id_Arr[0];
                    string GcNo = Id_Arr[1];
                    try
                    {
                        UserName = Id_Arr[2];
                        Branch = Id_Arr[3];
                        Id1_Arr = Id_Arr[3].Split('.');
                    }
                    catch (Exception)
                    {
                        Id1_Arr = Id_Arr[2].Split('.');
                    }
                    string ImageOrder = Id1_Arr[0];
                    string ImageType = Id1_Arr[1];
                    Branch = ImageOrder;

                    fileName1 = parser.Filename;
                    var fileName = Path.GetFileName(fileName1);

                    fileName1 = parser.Filename;
                    contentType = parser.ContentType;
                    byte[] fileContent = parser.FileContents;
                    image = byteArrayToImage(fileContent);
                    /* Server URL */
                    string DocumentType = "POD";
                    string DB_SavedPath = DocumentType + "/" + System.DateTime.Now.ToString("yyyy/MMM");

                    string FolDerPath = "D:\\Code\\RCPL_Live\\Images\\FMScanDocument\\" + DB_SavedPath;
                    if (!Directory.Exists(FolDerPath))
                        Directory.CreateDirectory(FolDerPath);

                    string DocumentUploadedPath = DB_SavedPath + "/" + fileName1;
                    image.Save(FolDerPath + "\\" + fileName1);

                    string QueryString = "exec Webx_SP_FM_ScanDocuments_new_Android '1','" + GcNo + "','" + GcNo + "','" + 0 + "','" + DocumentUploadedPath + "','','" + UserName + "','" + Branch + "'";
                    int ID1 = GF.SaveRequestServices(QueryString.Replace("'", "''"), "POD_Upload", "", "");
                    DataTable dataTable = GF.GetDataTableFromSP(QueryString);

                    string Link = "";
                    string SQR_POD_PATH = "EXEC USP_Get_Document '" + GcNo + "' ";
                    DataTable SQRPOD_PATH = GF.GetDataTableFromSP(SQR_POD_PATH);
                    Link = SQRPOD_PATH.Rows[0]["PODLink"].ToString();

                    //string Link = "http://103.205.64.141/RCPL_Live//Images/FMScanDocument/" + DB_SavedPath;
                    return "DONE___" + Link;
                }
                catch (Exception)
                {
                    return "ERROR ex";
                }
            }
            catch (Exception ex)
            {
                return "ERROR 456" + ex.Message;
            }
        }

        #endregion

        #region Get POD GC list

        public List<Webx_DocketNo> Get_GC_POD_List(string BRCD, string UserName, string CompanyCode, string FromDate, string ToDate, string GCNo)
        {
            string SQRY = "exec USP_Get_Android_PodList '" + BRCD + "','" + UserName + "','" + CompanyCode + "','" + FromDate + "','" + ToDate + "','" + GCNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_DocketNo> List = DataRowToObject.CreateListFromTable<Webx_DocketNo>(dataTable);
            return List;
        }

        public Stream GC_POD_List(string BRCD, string UserName, string CompanyCode, string FromDate, string ToDate, string GCNo)
        {
            List<Webx_DocketNo> List = Get_GC_POD_List(BRCD, UserName, CompanyCode, FromDate, ToDate, GCNo);
            return GF.ReturnJSONStream<List<Webx_DocketNo>>(List);
        }

        #endregion

        #endregion

        #region Upload Image

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            System.Drawing.Image returnImage = System.Drawing.Image.FromStream(ms);
            return returnImage;
        }

        public void CreateDirectoryIfNotExists(string filePath)
        {
            var directory = new FileInfo(filePath).Directory;
            if (directory == null) throw new Exception("Directory could not be determined for the filePath");

            Directory.CreateDirectory(directory.FullName);
        }

        #endregion

        #region Tracking API LIVE

        public List<Detail_Track_ViewModel> Get_Tracking_Details(string GCNO)
        {
            string SQRY = "exec USP_Tracking_Details '" + GCNO + "'";
            DataTable dataTable = Live_GF.GetDataTableFromSP(SQRY);
            List<Detail_Track_ViewModel> List = DataRowToObject.CreateListFromTable<Detail_Track_ViewModel>(dataTable);
            return List;
        }

        public string Get_TrackingWebsite_API(string DOCKNO, string Type)
        {
            string st = "";

            List<Detail_Track_ViewModel> ListDTVW = new List<Detail_Track_ViewModel>();
            ListDTVW = Get_Tracking_Details(DOCKNO);

            string SQRY = "USP_GetTracking '" + DOCKNO + "'";
            DataTable dt = Live_GF.GetDataTableFromSP(SQRY);

            if (dt.Rows.Count == 0)
            {
                return "Please Enter Valid DOCKNO.";
            }

            foreach (DataRow row in dt.Rows)
            {
                if (Type == "XML")
                {
                    st = st + "<HeaderDetail><DOCKNO>" + row["DOCKNO"].ToString() + "</DOCKNO>"
                           + "<PICKUP_DATE>" + row["PICKUP_DATE"].ToString() + "</PICKUP_DATE>"
                           + "<ORDER_NO>" + row["ORDER_NO"].ToString() + "</ORDER_NO>"
                           + "<CURRENT_STATUS>" + row["CURRENT_STATUS"].ToString() + "</CURRENT_STATUS>"
                           + "<CURRENT_STATUS_CODE>" + row["CURRENT_STATUS_CODE"].ToString() + "</CURRENT_STATUS_CODE>"
                           + "<CURRENT_LOCATION>" + row["CURRENT_LOCATION"].ToString() + "</CURRENT_LOCATION>"
                           + "<DELIVERED_ON>" + row["DELIVERED_ON"].ToString() + "</DELIVERED_ON>"
                           + "<DELIVERED_TIME>" + row["DELIVERED_TIME"].ToString() + "</DELIVERED_TIME>"
                           + "<DELIVERED_TO>" + row["DELIVERED_TO"].ToString() + "</DELIVERED_TO>"
                           + "<REMARKS>" + row["REMARKS"].ToString() + "</REMARKS>"
                           + "<CSGE_NAME>" + row["CSGE_NAME"].ToString() + "</CSGE_NAME>"
                           + "<CONTACT_NO>" + row["CONTACT_NO"].ToString() + "</CONTACT_NO>"
                           + "<CSGE_ADDR>" + row["CSGE_ADDR"].ToString() + "</CSGE_ADDR>"
                           + "<CITY>" + row["CITY"].ToString() + "</CITY>"
                           + "<PINCODE>" + row["PINCODE"].ToString() + "</PINCODE>"
                           + "<PAYMENT>" + row["PAYMENT"].ToString() + "</PAYMENT>"
                           + "<VALUE_OF_SHIPMENT>" + row["VALUE_OF_SHIPMENT"].ToString() + "</VALUE_OF_SHIPMENT>"
                           + "<SAID_TO_CONTAIN>" + row["SAID_TO_CONTAIN"].ToString() + "</SAID_TO_CONTAIN>"
                           + "<ORIGIN>" + row["ORIGIN"].ToString() + "</ORIGIN>"
                           + "<DESTINATION>" + row["DESTINATION"].ToString() + "</DESTINATION>"
                           + "<NO_OF_PIECES>" + row["NO_OF_PIECES"].ToString() + "</NO_OF_PIECES>"
                           + "<WEIGHT>" + row["WEIGHT"].ToString() + "</WEIGHT>"
                           + "<RTO_ON>" + row["RTO_ON"].ToString() + "</RTO_ON>"
                           + "<RTO_REASON>" + row["RTO_REASON"].ToString() + "</RTO_REASON>"
                           + "<NDR_REASON>" + row["NDR_REASON"].ToString() + "</NDR_REASON>"
                           + "<NDR_DATE>" + row["NDR_DATE"].ToString() + "</NDR_DATE>"
                           + "<RELATION>" + row["RELATION"].ToString() + "</RELATION>"
                           + "<CLIENTCODE>" + row["CLIENTCODE"].ToString() + "</CLIENTCODE>"
                           + "<CLIENTNAME>" + row["CLIENTNAME"].ToString() + "</CLIENTNAME>"
                           + "<POD_Link>" + ListDTVW.First().PODLink + "</POD_Link>"
                           + "</HeaderDetail>";

                    st = st + "<Detail>";
                    foreach (var item in ListDTVW)
                    {
                        st = st + "<ConsignmentTrack>";
                        st = st + "<DOCKNO>" + item.DOCKNO + "</DOCKNO>";
                        st = st + "<TRANSIT_LOCATION>" + item.sourcehb + "</TRANSIT_LOCATION>";
                        st = st + "<ACTIVITY>" + item.ACTIVITY + "</ACTIVITY>";
                        st = st + "<EVENTDATE>" + item.ASDTDate + "</EVENTDATE>";
                        st = st + "<EVENTDATETIME>" + item.ASDTTime + "</EVENTDATETIME>";
                        st = st + "<NEXT_LOCATION>" + item.tobh_code + "</NEXT_LOCATION>";
                        st = st + "<TRACKCODE>" + item.TRACK_CODE + "</TRACKCODE>";
                        st = st + "</ConsignmentTrack>";
                    }
                    st = st + "</Detail>";

                }
                else if (Type == "JSON")
                {
                    st = st + "DOCKNO =" + row["DOCKNO"].ToString() + ", PICKUP_DATE=" + row["PICKUP_DATE"].ToString() + ", ORDER_NO=" + row["ORDER_NO"].ToString()
                        + ", CURRENT_STATUS=" + row["CURRENT_STATUS"].ToString() + ", CURRENT_STATUS_CODE=" + row["CURRENT_STATUS_CODE"].ToString() + ", CURRENT_LOCATION=" + row["CURRENT_LOCATION"].ToString()
                        + ", DELIVERED_ON=" + row["DELIVERED_ON"].ToString() + ", DELIVERED_TIME=" + row["DELIVERED_TIME"].ToString() + ", DELIVERED_TO=" + row["DELIVERED_TO"].ToString()
                        + ", REMARKS=" + row["REMARKS"].ToString() + ", CSGE_NAME=" + row["CSGE_NAME"].ToString() + ", CONTACT_NO=" + row["CONTACT_NO"].ToString()
                        + ", CSGE_ADDR=" + row["CSGE_ADDR"].ToString() + ", CITY=" + row["CITY"].ToString() + ", PINCODE=" + row["PINCODE"].ToString()
                        + ", PAYMENT=" + row["PAYMENT"].ToString() + ", VALUE_OF_SHIPMENT=" + row["VALUE_OF_SHIPMENT"].ToString() + ", SAID_TO_CONTAIN=" + row["SAID_TO_CONTAIN"].ToString()
                        + ", ORIGIN=" + row["ORIGIN"].ToString() + ", DESTINATION=" + row["DESTINATION"].ToString() + ", NO_OF_PIECES=" + row["NO_OF_PIECES"].ToString()
                        + ", WEIGHT=" + row["WEIGHT"].ToString() + ", RTO_ON=" + row["RTO_ON"].ToString() + ", RTO_REASON=" + row["RTO_REASON"].ToString()
                        + ", NDR_REASON=" + row["NDR_REASON"].ToString() + ", NDR_DATE=" + row["NDR_DATE"].ToString() + ", RELATION=" + row["RELATION"].ToString()
                        + ", CLIENTCODE=" + row["CLIENTCODE"].ToString() + ", CLIENTNAME=" + row["CLIENTNAME"].ToString()
                        + ",POD_Link=" + ListDTVW.First().PODLink;

                    foreach (var item in ListDTVW)
                    {
                        st = st + "ConsignmentTrack";
                        st = st + "DOCKNO" + item.DOCKNO;
                        st = st + "TRANSIT_LOCATION" + item.sourcehb;
                        st = st + "<ACTIVITY>" + item.ACTIVITY;
                        st = st + "<EVENTDATE>" + item.ASDTDate;
                        st = st + "<EVENTDATETIME>" + item.ASDTTime;
                        st = st + "<NEXT_LOCATION>" + item.tobh_code;
                        st = st + "<TRACKCODE>" + item.TRACK_CODE;
                        st = st + "ConsignmentTrack";
                    }
                }
            }

            return st;
        }

        public string TrackingWebsite_API_Xml(string DOCKNO)
        {
            try
            {
                string st = Get_TrackingWebsite_API(DOCKNO, "XML");
                return st.Replace("_XmlResponse", "");
            }
            catch (Exception)
            {
                return "Please Enter Valid DOCKNO.";
            }
        }

        public string TrackingWebsite_API_Json(string DOCKNO)
        {
            try
            {
                string st = Get_TrackingWebsite_API(DOCKNO, "JSON");
                return st;
            }
            catch (Exception)
            {
                return "Please Enter Valid DOCKNO.";
            }
        }

        #endregion

        #region POD_Image

        public string POD_Image_Link(string DOCKNO)
        {
            try
            {
                string st = "http://103.232.124.146/TMS_LIve/Images/FMScanDocument/FM/" + DOCKNO + "/" + DOCKNO + ".jpg";
                return st;
            }
            catch (Exception)
            {
                return "Please Enter Valid DOCKNO.";
            }
        }

        #endregion

        #region PasswordUpdate

        public Stream PasswordUpdate(string UserName, string NewPass)
        {
            DataTable DT = new DataTable();
            List<webx_CUSTHDR> List = new List<webx_CUSTHDR>();

            try
            {
                DT = DTPassword_Detail(UserName, NewPass);
                List = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(DT);
                return GF.ReturnJSONStream_DT<List<webx_CUSTHDR>>(List, DT);
            }
            catch (Exception ex)
            {
                string Message = ex.Message;
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string returnValue = jSearializer.Serialize(List);
                var jsonObj = new
                {
                    Success = 0,
                    Message = ex.Message,
                    Response = ""
                };
                JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
                returnValue = jScriptSerializer.Serialize(jsonObj);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
                return memoryStream;
            }
        }

        public DataTable DTPassword_Detail(string UserName, string NewPass)
        {
            string SQRY = "exec USP_User_Update_Password '" + UserName + "','" + NewPass + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            return dataTable;
        }

        #endregion

        #region Undevivered GC

        public Stream UnDeliveredGC(string StartDate, string EndDate, string DOCKNO, string InvoiceNO, string UserName)
        {
            DataTable DT = new DataTable();
            List<UnDiliverdGC> List = new List<UnDiliverdGC>();

            try
            {
                DT = DTUndiveleredGC_Detail(StartDate, EndDate, DOCKNO, InvoiceNO, UserName);
                if (DT != null && DT.Rows.Count > 0)
                {
                    List = DataRowToObject.CreateListFromTable<UnDiliverdGC>(DT);
                    return GF.ReturnJSONStream_DT<List<UnDiliverdGC>>(List, DT);
                }
                else
                {
                    System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    string returnValue = jSearializer.Serialize(List);
                    var jsonObj = new
                    {
                        Success = 0,
                        Message = "Not Have Any Record",
                        Response = ""
                    };
                    JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
                    returnValue = jScriptSerializer.Serialize(jsonObj);
                    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                    MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
                    return memoryStream;
                }
            }
            catch (Exception ex)
            {
                string Message = ex.Message;
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string returnValue = jSearializer.Serialize(List);
                var jsonObj = new
                {
                    Success = 0,
                    Message = ex.Message,
                    Response = ""
                };
                JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
                returnValue = jScriptSerializer.Serialize(jsonObj);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
                return memoryStream;
            }
        }

        public DataTable DTUndiveleredGC_Detail(string StartDate, string EndDate, string DOCKNO, string InvoiceNO, string UserName)
        {
            string SQRY = "exec Usp_UndeliverGC '" + StartDate + "','" + EndDate + "','" + DOCKNO + "','" + InvoiceNO + "','" + UserName + "'";
            DataTable dataTable = Live_GF.GetDataTableFromSP(SQRY);
            return dataTable;
        }

        #endregion

        public partial class PODUpload
        {
            public string PODFileName { get; set; }
            public string PODLink { get; set; }
            public string EventDateTime { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
        }

        public PODUpload GET_PODUpload(string DockNo)
        {
            string SQRY = "select 'PODFileName' as PODFileName,'PODLink' as PODLink,'" + System.DateTime.Now.ToString("dd MMM yyyy") + "' EventDateTime ";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<PODUpload> List = DataRowToObject.CreateListFromTable<PODUpload>(dataTable);
            //int IDS = SaveRequestServices(SQRY.Replace("'", "''"), "GET_PODUpload", "", "");
            return List.First();
        }

        public Stream Get_PODUpload_Listing(string DockNo)
        {
            var Get_CityArea = GET_PODUpload(DockNo);

            var jsonObj = new
            {
                UserName = "madura",
                Password = "mud@123",
                CustomerId = "C00012",
                LspId = "RCPL",
                DocumentNo = DockNo,
                POD = Get_CityArea
            };
            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        public void DoWork()
        {
        }

        #region Get DRS Docket List

        public Stream DRS_DOCKET_List_API(Stream DRS)
        {
            string data = GF.StreamToJsonString(DRS);
            SortedList<string, object> context = new SortedList<string, object>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            DataTable Dt = new DataTable();

            context = js.Deserialize<SortedList<string, object>>(data);
            Webx_DocketNo_Criteria ObjDRS = GF.DictionaryToObjectList<Webx_DocketNo_Criteria>(((object[])(context["DRSJson"]))).First();

            Dt = DRS_DOCKET_List(ObjDRS);

            Webx_DocketNo ObjResponse = new Webx_DocketNo();
            List<Webx_DocketNo> List = DataRowToObject.CreateListFromTable<Webx_DocketNo>(Dt);
            return GF.ReturnJSONStream<List<Webx_DocketNo>>(List);
        }

        public DataTable DRS_DOCKET_List(Webx_DocketNo_Criteria ObjDRS)
        {
            string SQR = "", ErrorMessage = "";
            int IDS = 0;

            DataTable Dt = new DataTable();
            try
            {
                SQR = "exec USP_Get_DRS_Docket_Detais '" + ObjDRS.DRSNO + "','" + ObjDRS.BRCD + "','" + ObjDRS.UserName + "','" + ObjDRS.CompanyCode + "','" + ObjDRS.FromDate + "','" + ObjDRS.ToDate + "','" + ObjDRS.IMEINo + "'";
                IDS = GF.SaveRequestServices(SQR.Replace("'", "''"), "USP_Get_DRS_Docket_Detais", "0", "");
                Dt = GF.GetDataTableFromSP(SQR);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            return Dt;
        }

        #endregion

        #region Submit DRS Delivered

        public Stream DRS_DOCKET_Delivered_API(Stream DRS_Delivered)
        {
            string data = GF.StreamToJsonString(DRS_Delivered);
            SortedList<string, object> context = new SortedList<string, object>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            DataTable Dt = new DataTable();

            context = js.Deserialize<SortedList<string, object>>(data);
            Webx_DRS_DocketNo_Delivered ObjDRS = GF.DictionaryToObjectList<Webx_DRS_DocketNo_Delivered>(((object[])(context["DRS_Delivered_Json"]))).First();

            string SQL_GET_Count = " SELECT X.CNT FROM WebX_Trans_Docket_Status A " +
                                   "CROSS APPLY(SELECT COUNT(*) AS CNT FROM WebX_Trans_Docket_Status B WHERE B.DRS=A.DRS AND B.Delivered='N') X " +
                                   "WHERE A.DOCKNO='" + ObjDRS.DOCKNO + "'";
            DataTable DT_GET_Count = GF.GetDataTableFromSP(SQL_GET_Count);
            int Counts = Convert.ToInt32(DT_GET_Count.Rows[0][0].ToString());
            string Types = "";
            if (Counts != 1 && Counts != 0)
            {
                Types = DRS_Delivered_List(ObjDRS, Counts);
            }

            Webx_DRS_DocketNo_Delivered_Response ObjResponse = new Webx_DRS_DocketNo_Delivered_Response();
            ObjResponse.MESSAGE = Types.ToUpper();

            if (Counts == 1)
            {
                Counts = 1;
                Types = "Last Docket Not Delivered From Tab.";
            }
            else
            {
                Counts = 0;
            }


            if (Types.ToUpper() == "DONE")
            {
                ObjResponse.STATUS = "1";
            }
            else
            {
                ObjResponse.STATUS = "0";
            }

            var jsonObj = new
            {
                IsLastDocket = Counts,
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE,
                Response = ObjResponse
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

            return memoryStream;
        }

        public string DRS_Delivered_List(Webx_DRS_DocketNo_Delivered ObjDRS, int Counts)
        {
            string SQR = "", Status = "";
            int IDS = 0;

            DataTable Dt = new DataTable();
            try
            {
                string SQL_DataGet = "SELECT isnull(DelPkgQty,0) as DelPkgQty,isnull(PendPkgQty,0) as PendPkgQty,isnull(Delivered,'N') as Delivered,isnull(DRSUpdated,'N') as  DRSUpdated FROM WebX_Trans_Docket_Status WHERE Dockno='" + ObjDRS.DOCKNO + "'";
                DataTable DT_DataGet = GF.GetDataTableFromSP(SQL_DataGet);

                string SQL_PDC = "SELECT PKGSNO_Load FROM webx_PDCTRN WITH(NOLOCK) WHERE PDCNO='" + ObjDRS.DRS + "' AND DOCKNO='" + ObjDRS.DOCKNO + "' and DOCKSF='" + ObjDRS.DOCKSF + "'";
                DataTable DT_PDC = GF.GetDataTableFromSP(SQL_PDC);

                int PKGSNO_Load = Convert.ToInt32(DT_PDC.Rows[0]["PKGSNO_Load"].ToString());
                //int PendPkgQty = Convert.ToInt32(DT_DataGet.Rows[0]["PendPkgQty"].ToString());
                string Delivered = DT_DataGet.Rows[0]["Delivered"].ToString();

                if (Delivered == "N" && Counts != 1 && Counts != 0)
                {
                    int PKGSDELIVERED = PKGSNO_Load;

                    var delyStatus = "U";
                    string XML = "<root>";
                    XML = XML + "<DRS>";
                    XML = XML + "<DRSCode>" + ObjDRS.DRS + "</DRSCode>";
                    XML = XML + "<CLOSEKM>0</CLOSEKM>";
                    XML = XML + "<DOCKET>" + ObjDRS.DOCKNO + "</DOCKET>";
                    XML = XML + "<DOCKETSF>" + ObjDRS.DOCKSF + "</DOCKETSF>";
                    XML = XML + "<PKGSWASPENDING>" + PKGSNO_Load + "</PKGSWASPENDING>";
                    XML = XML + "<PKGSDELIVERED>" + PKGSNO_Load + "</PKGSDELIVERED>";
                    delyStatus = "F"; // Full Dely.
                    XML = XML + "<DELYDATE>" + ObjDRS.DELYDATE + "</DELYDATE>";
                    XML = XML + "<DELYSTATUS>" + delyStatus + "</DELYSTATUS>";
                    XML = XML + "<DELYTIME>" + ObjDRS.DELYTIME + "</DELYTIME>";
                    XML = XML + "<DELYREASON>TAB OFFLINE DELIVERED</DELYREASON>";
                    XML = XML + "<DELYPERSON>OK " + ObjDRS.DELYPERSON + "</DELYPERSON>";
                    XML = XML + "<CODDODAMOUNT>0</CODDODAMOUNT>";
                    XML = XML + "<CODDODCOLLECTED>0</CODDODCOLLECTED>";
                    XML = XML + "<CODDODNO>0</CODDODNO>";
                    XML = XML + "</DRS>";
                    XML = XML + "</root>";

                    SQR = "exec  usp_XML_DRS_Update_Single_NewPortal_Tab '" + ObjDRS.DELYPERSON + "','" + XML + "'";
                    IDS = GF.SaveRequestServices(SQR.Replace("'", "''"), "Insert_DRS_Update_Single_TAB", "0", "");
                    //Dt = GF.GetDataTableFromSP(SQR);
                    DataSet DS = GF.GetDataSetFromSP(SQR);

                    if (DS != null && DS.Tables.Count > 0 && DS.Tables[0] != null && DS.Tables[0].Rows.Count > 0 && DS.Tables[0].Rows[0][1].ToString() == "1")
                    {
                        Status = "DONE";
                    }
                }
                else if (Counts == 1)
                {
                    Status = "This Is Last Docket Of This DRS. So Not Updated From Tab.";
                }
                else
                {
                    Status = "Allready Delivered.";
                }
            }
            catch (Exception ex)
            {
                Status = ex.Message;
            }
            return Status;
        }

        #endregion

        #region Customer RelationShip(Pickup Request) LOGIN

        #region Customer Login

        public Stream RCPL_Customer_Login(string UserName, string Pass)
        {
            DataTable DT_Login = new DataTable();
            List<webx_CUSTHDR> List = new List<webx_CUSTHDR>();
            webx_CUSTHDR ObJ = new webx_CUSTHDR();

            try
            {
                DT_Login = DTLogin_Detail(UserName, Pass);
                List = DataRowToObject.CreateListFromTable<webx_CUSTHDR>(DT_Login);
                ObJ = List.First();

                if (ObJ.FirstTimeLogin == 1)
                {
                    return GF.ReturnJSONStream_DT<List<webx_CUSTHDR>>(List, DT_Login);
                }
                else
                {
                    return GF.ReturnJSONStream_DT<List<webx_CUSTHDR>>(List, DT_Login);
                }
            }
            catch (Exception ex)
            {
                string Message = ex.Message;
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string returnValue = jSearializer.Serialize(List);

                var jsonObj = new
                {
                    Success = 0,
                    Message = ex.Message,
                    Response = ""
                };
                JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
                returnValue = jScriptSerializer.Serialize(jsonObj);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

                return memoryStream;
            }
        }

        public DataTable DTLogin_Detail(string UserName, string Pass)
        {
            string SQRY = "exec USP_User_Details '" + UserName + "','" + Pass + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            return dataTable;
        }

        #endregion

        #region Get City List

        public List<webx_citymaster> Get_City_List()
        {
            string SQRY = "exec USP_CityMaster_List";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<webx_citymaster> List = DataRowToObject.CreateListFromTable<webx_citymaster>(dataTable);
            return List;
        }

        public Stream City_List()
        {
            List<webx_citymaster> List = Get_City_List();
            return GF.ReturnJSONStream<List<webx_citymaster>>(List);
        }

        #endregion

        #region Get State List

        public List<Webx_State> Get_State_List()
        {
            string SQRY = "exec USP_StateMaster_List";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_State> List = DataRowToObject.CreateListFromTable<Webx_State>(dataTable);
            return List;
        }

        public Stream State_List()
        {
            List<Webx_State> List = Get_State_List();
            return GF.ReturnJSONStream<List<Webx_State>>(List);
        }

        #endregion

        #region Get Pincode List

        public List<webx_pincode_master> Get_Pincode_List(string CityCode)
        {
            string SQRY = "exec USP_PincodeMaster_List";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<webx_pincode_master> List = DataRowToObject.CreateListFromTable<webx_pincode_master>(dataTable);
            return List.Where(c => c.cityname == CityCode && c.ActiveFlag == "Y").ToList();
        }

        public Stream Pincode_List(string CityCode)
        {
            List<webx_pincode_master> List = Get_Pincode_List(CityCode);
            return GF.ReturnJSONStream<List<webx_pincode_master>>(List);
        }

        #endregion  

        #region PRQ Submit

        public Stream PRQ_Submit_API(PickupRequest PRQ)
        {
            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(PRQ.Obj_CPR.GetType());
            using (MemoryStream xmlStream1 = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream1, PRQ.Obj_CPR);
                xmlStream1.Position = 0;
                xmlDoc1.Load(xmlStream1);
            }

            string SQRY = "EXEC Usp_Submit_Pickup_Request '" + xmlDoc1.InnerXml + "','" + PRQ.Obj_CPR.BRCD + "','" + PRQ.Obj_CPR.EntryBy + "','2016'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "PRQ_Submit_API", "", "");
            DataSet DS = GF.GetDataSetFromSP(SQRY);
            CYGNUSPickupRequest_Response ObjResponse = new CYGNUSPickupRequest_Response();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest_Response>(DS.Tables[0]).First();
            List<CYGNUSPickupRequest> List_PRQ = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(DS.Tables[1]);

            if (ObjResponse.STATUS == 1 && ObjResponse.MESSAGE.ToUpper() == "DONE")
            {
                string CustomerCode = PRQ.Obj_CPR.EntryBy;
                try
                {
                    CYGNUS_Notification_Token OBJ_CNFT = new CYGNUS_Notification_Token();
                    OBJ_CNFT = Get_Notification_Token(CustomerCode);
                    string[] arr_Token = OBJ_CNFT.TokenNo.Split(',');
                    string[] arr = { OBJ_CNFT.TokenNo };
                    sendsinglenotfication_For_PRQ("New PRQ Generated From Customer :-" + PRQ.Obj_CPR.EntryBy, arr, 0, ObjResponse.PRQNO, "New PRQ Generated");
                }
                catch (Exception)
                {
                }
            }

            var jsonObj = new
            {
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE,
                Response = ObjResponse,
                List = List_PRQ,
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);
            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        public CYGNUS_Notification_Token Get_Notification_Token(string UserID)
        {
            string SQRY = "EXEC USP_Get_Notification_Token_List '" + UserID + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            CYGNUS_Notification_Token OBJ = DataRowToObject.CreateListFromTable<CYGNUS_Notification_Token>(dataTable).First();
            return OBJ;
        }

        #endregion

        #region Send Notification Submit Events

        public void sendsinglenotfication_For_PRQ(string notification, string[] registerTokens, int badge, string PRQNo, string Title)
        {
            try
            {
                //string ApplicationID = "AIzaSyDsK4WDjlL6dAKNsCFd7uVBBT0yckJhSEI";
                string ApplicationID = "AIzaSyBT2UfZToTDuVJTtUt-dg7eTxaZ7rZJJyU";

                WebRequest tRequest;
                tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationID));
                tRequest.Headers.Add(string.Format("project_id:", "547030126705"));

                string registerTokensStr = "";
                string[] arr_Token = registerTokens[0].ToString().Split(',');


                for (int i = 0; i < arr_Token.Length; i++)
                {
                    if (i == 0)
                    {
                        registerTokensStr = "\"" + arr_Token[i] + "\"";
                    }
                    else
                    {
                        registerTokensStr = registerTokensStr + ",\"" + arr_Token[i] + "\"";
                    }
                }

                string postData = "{\"registration_ids\":[" + registerTokensStr + "],\"data\":{\"PRQ\":\"" + PRQNo + "\",\"badge\":\"1\",\"title\":\"" + PRQNo + ":-" + Title + "\",\"body\":\"" + notification + "\"},\"priority\":\"high\",\"content_available\":true}";

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;
                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse tResponse = tRequest.GetResponse();
                dataStream = tResponse.GetResponseStream();
                StreamReader tReader = new StreamReader(dataStream);
                String sResponseFromServer = tReader.ReadToEnd();
                tReader.Close(); dataStream.Close();
                tResponse.Close();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                throw;
            }
        }

        #endregion

        #region Get PRQ List

        public List<CYGNUSPickupRequest> Get_PRQ_List(string FromDate = null, string ToDate = null, string PRQNo = null, string CustomerCode = null, string Driver = null,
        string SuperVisor = null, string Status = null, string BRCD = null, string UserName = null, string CompanyCode = null, string IMEINo = null, string CustType = null)
        {
            string SQRY = "exec USP_Get_PRQ_List '" + FromDate + "','" + ToDate + "','" + PRQNo + "','" + CustomerCode + "','" + Driver +
                "','" + SuperVisor + "','" + BRCD + "','" + UserName + "','" + CompanyCode + "','" + IMEINo + "','" + Status + "','" + CustType + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<CYGNUSPickupRequest> List = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(dataTable);
            return List;
        }

        public Stream PRQ_List(string FromDate = null, string ToDate = null, string PRQNo = null, string CustomerCode = null, string Driver = null,
        string SuperVisor = null, string Status = null, string BRCD = null, string UserName = null, string CompanyCode = null, string IMEINo = null, string CustType = null)
        {
            List<CYGNUSPickupRequest> List = Get_PRQ_List(FromDate, ToDate, PRQNo, CustomerCode, Driver, SuperVisor, Status, BRCD, UserName, CompanyCode, IMEINo, CustType);
            return GF.ReturnJSONStream<List<CYGNUSPickupRequest>>(List);
        }

        #endregion

        #region Add/Update New Token Service

        public Stream Token_Generation_API(NotiFication Notification)
        {
            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(Notification.GetType());
            using (MemoryStream xmlStream1 = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream1, Notification);
                xmlStream1.Position = 0;
                xmlDoc1.Load(xmlStream1);
            }

            string SQRY = "EXEC Usp_Submit_Notification_Token '" + xmlDoc1.InnerXml + "','" + Notification.Obj_NotiFication.EntryBy + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);

            CYGNUS_Notification_Token_Response ObjResponse = new CYGNUS_Notification_Token_Response();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUS_Notification_Token_Response>(DS.Tables[0]).First();

            var jsonObj = new
            {
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        #endregion

        #region Update PRQ Status

        //public Stream Update_PRQ_Status_API(PRQ_History PRQ_History)
        //{
        //    string SQRY = "EXEC USP_PRQ_History_Insert '" + PRQ_History.Obj_PRQ_History.PRQNo + "','" + PRQ_History.Obj_PRQ_History.Status + "','" + PRQ_History.Obj_PRQ_History.EntryBy + "','" + PRQ_History.Obj_PRQ_History.Remarks + "','" + PRQ_History.Obj_PRQ_History.IsFromTab + "'";
        //    DataSet DS = GF.GetDataSetFromSP(SQRY);
        //    CYGNUSPickupRequest Obj_PRQ = Get_PRQ_List("", "", PRQ_History.Obj_PRQ_History.PRQNo).First();

        //    var jsonObj = new
        //    {
        //        Success = 1,
        //        Message = "Done",
        //        PRQ = Obj_PRQ
        //    };

        //    JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
        //    string returnValue = jScriptSerializer.Serialize(jsonObj);

        //    WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
        //    MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
        //    return memoryStream;
        //}

        #endregion

        #region Driver Login

        public Stream RCPL_Driver_Login(string UserId, string Password)
        {
            DataTable DT_Login = new DataTable();
            List<WEBX_FLEET_DRIVERMST_Android> List = new List<WEBX_FLEET_DRIVERMST_Android>();
            WEBX_FLEET_DRIVERMST_Android ObJ = new WEBX_FLEET_DRIVERMST_Android();

            try
            {
                DT_Login = DT_Driver_Login_Detail(UserId, Password);
                List = DataRowToObject.CreateListFromTable<WEBX_FLEET_DRIVERMST_Android>(DT_Login);
                ObJ = List.First();

                if (ObJ.FirstTimeLogin == 1)
                {
                    return GF.ReturnJSONStream_DT<List<WEBX_FLEET_DRIVERMST_Android>>(List, DT_Login);
                }
                else
                {
                    return GF.ReturnJSONStream_DT<List<WEBX_FLEET_DRIVERMST_Android>>(List, DT_Login);
                }
            }
            catch (Exception ex)
            {
                string Message = ex.Message;
                System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string returnValue = jSearializer.Serialize(List);

                var jsonObj = new
                {
                    Success = 0,
                    Message = ex.Message,
                    Response = ""
                };
                JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
                returnValue = jScriptSerializer.Serialize(jsonObj);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
                MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

                return memoryStream;
            }
        }

        public DataTable DT_Driver_Login_Detail(string UserName, string Pass)
        {
            string SQRY = "exec [USP_Driver_Login_Details] '" + UserName + "','" + Pass + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            return dataTable;
        }

        #endregion

        #region Get Vehicle Type list

        public List<webx_Vehicle_Type_Android> Get_Vehicle_Type_List()
        {
            string SQRY = "exec Usp_GetVehicleTypeList";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<webx_Vehicle_Type_Android> List = DataRowToObject.CreateListFromTable<webx_Vehicle_Type_Android>(dataTable);
            return List;
        }

        public Stream Vehicle_Type_List()
        {
            List<webx_Vehicle_Type_Android> List = Get_Vehicle_Type_List();
            return GF.ReturnJSONStream<List<webx_Vehicle_Type_Android>>(List);
        }

        #endregion

        #region Get Branch Wise Vehicle Driver List

        public List<webx_VEHICLE_HDR_Andoid> Get_Vehicle_Driver_List(string UserName, string PrqNo, string Vehicle, string XML)
        {
            string SQRY = "exec USP_Get_Vehicle_Driver_List '" + UserName + "','" + PrqNo + "','" + Vehicle + "','" + XML + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<webx_VEHICLE_HDR_Andoid> List = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR_Andoid>(dataTable);
            return List;
        }

        public Stream Vehicle_Driver_List(string UserName, string PrqNo)
        {
            CYGNUSPickupRequest OBJPRQ = Get_PRQ_List(null, null, PrqNo).First();
            string URL = "http://203.115.101.109/otherapi/nearestvehicleget.php?Gps_latitude=" + OBJPRQ.OriginLat + "&Gps_longitude=" + OBJPRQ.OriginLong + "";
            string Response = GetVehicleList(URL);
            SortedList<string, object> context = new SortedList<string, object>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            context = js.Deserialize<SortedList<string, object>>(Response);
            List<NearestVehicle> NearestVehiclelist = GF.DictionaryToObjectList<NearestVehicle>(((object[])(context["Nearest_vehicle"])));

            string Vehicle = "";
            foreach (var item in NearestVehiclelist.OrderBy(c => c.Range).Take(20))
            {
                if (Vehicle == "")
                {
                    Vehicle = item.Veh_reg;
                }
                else
                {
                    Vehicle = Vehicle + "," + item.Veh_reg;
                }
            }

            string URL1 = "http://203.115.101.126/mobileapp/vehilestatus_rcpl.php?veh_reg=" + Vehicle;
            string Response1 = GetVehicleList(URL1);
            SortedList<string, object> context1 = js.Deserialize<SortedList<string, object>>(Response1);
            List<vehicleStatuslist> vehicleStatuslist = GF.DictionaryToObjectList<vehicleStatuslist>(((object[])(context1["vehicleStatuslist"])));

            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(vehicleStatuslist.GetType());
            using (MemoryStream xmlStream1 = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream1, vehicleStatuslist);
                xmlStream1.Position = 0;
                xmlDoc1.Load(xmlStream1);
            }


            List<webx_VEHICLE_HDR_Andoid> List = Get_Vehicle_Driver_List(UserName, PrqNo, Vehicle, xmlDoc1.InnerXml);
            return GF.ReturnJSONStream<List<webx_VEHICLE_HDR_Andoid>>(List);
        }

        public class vehicleStatuslist
        {
            public string Veh_reg { get; set; }
            //public string Updated { get; set; }
            //public string Location { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            //public string Speed { get; set; }
        }

        public class NearestVehicle
        {
            public string Veh_reg { get; set; }
            public int Range { get; set; }
        }

        private string GetVehicleList(string URL)
        {
            try
            {
                /*
                 * Create CookieContainer to contain session cookie
                 */
                var cookieJar = new CookieContainer();
                /*
                 * Open HttpWeb Request. Pass credentials to Logon.ashx page
                 */
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
                request.CookieContainer = cookieJar;
                request.ContentType = "application/json";
                request.Method = "POST";
                byte[] byteArray = Encoding.UTF8.GetBytes("");
                request.ContentLength = byteArray.Length;
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                /*
                 * Read HttpWeb Response
                 */
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string Response = reader.ReadToEnd();
                response.Close();

                return Response;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #endregion

        #region Update Vehicle Driver Assign PRQ

        public Stream Vehicle_Driver_Assign_PRQ_API(PRQ_Vehicle_Driver_Assign PRQ_Vehicle_Driver_Assign)
        {
            string SQRY = "EXEC USP_Vehicle_Driver_Assign_PRQ '" + PRQ_Vehicle_Driver_Assign.VehicleNo + "','" + PRQ_Vehicle_Driver_Assign.AssignBy + "','" + PRQ_Vehicle_Driver_Assign.DriverID + "','" + PRQ_Vehicle_Driver_Assign.DriverName + "','" + PRQ_Vehicle_Driver_Assign.PRQNo + "'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);

            string Message = "", Status = "";
            Message = DT.Rows[0]["Message"].ToString();
            Status = DT.Rows[0]["Statuss"].ToString();

            CYGNUSPickupRequest Obj_PRQ = Get_PRQ_List("", "", PRQ_Vehicle_Driver_Assign.PRQNo).First();

            var jsonObj = new
            {
                Success = Status,
                Message = Message,
                PRQ = Obj_PRQ
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        #endregion

        /*
         
        insert into Webx_Master_General values ('PRQ_STATUS','1','PRQ GENERATED','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','2','ASSIGN BUT NOT ACCEPT','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','3','PRQ ACCEPTED','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','4','DECLAINE PRQ','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','5','PRQ CANCEL','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','6','PRQ CANCEL AFTER ASSIGN','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','7','GO FOR PICKUP','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
insert into Webx_Master_General values ('PRQ_STATUS','8','COMPLETED','S','Y',GETDATE(),'CYGNSUTEAM',NULL,NULL,NULL,NULL,NULL)
         
         */

        #region Get PRQ Status List

        public List<Webx_Master_General_Tab> Get_PRQ_Status_List(string Status_Type)
        {
            string SQRY = "exec [USP_GeneralMaster_List] ";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General_Tab> List = DataRowToObject.CreateListFromTable<Webx_Master_General_Tab>(dataTable);
            return List.Where(c => c.CodeType == Status_Type).ToList();
        }

        public Stream PRQ_Status_List(string Status_Type)
        {
            List<Webx_Master_General_Tab> List = Get_PRQ_Status_List(Status_Type);
            return GF.ReturnJSONStream<List<Webx_Master_General_Tab>>(List);
        }

        #endregion

        #region Customer Prefered Location Submit

        public Stream CustomerPreferedLocation_Submit_API(CUST_PrefLocation PreferedLoc)
        {
            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(PreferedLoc.Obj_CCPL.GetType());
            using (MemoryStream xmlStream1 = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream1, PreferedLoc.Obj_CCPL);
                xmlStream1.Position = 0;
                xmlDoc1.Load(xmlStream1);
            }

            string SQRY = "EXEC Usp_Submit_PreferedLocation '" + xmlDoc1.InnerXml + "','" + PreferedLoc.Obj_CCPL.BRCD + "','" + PreferedLoc.Obj_CCPL.EntryBy + "'";
            DataSet DS = GF.GetDataSetFromSP(SQRY);

            CYGNUS_Customer_PreferedLocationResponse ObjResponse = new CYGNUS_Customer_PreferedLocationResponse();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUS_Customer_PreferedLocationResponse>(DS.Tables[0]).First();
            List<CYGNUS_Customer_PreferedLocation> List = DataRowToObject.CreateListFromTable<CYGNUS_Customer_PreferedLocation>(DS.Tables[1]);

            var jsonObj = new
            {
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE,
                List = List
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        #region Customer Prefered Location List

        public List<CYGNUS_Customer_PreferedLocation> Get_PreferedLocation_List(string CustCode, string BRCD)
        {
            string SQRY = "EXEC USP_GetCustPreferedLocation '" + CustCode + "','" + BRCD + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<CYGNUS_Customer_PreferedLocation> List = DataRowToObject.CreateListFromTable<CYGNUS_Customer_PreferedLocation>(dataTable);
            return List;
        }

        public Stream PreferedLocation_List(string CustCode, string BRCD)
        {
            List<CYGNUS_Customer_PreferedLocation> List = Get_PreferedLocation_List(CustCode, BRCD);
            return GF.ReturnJSONStream<List<CYGNUS_Customer_PreferedLocation>>(List);
        }

        #endregion

        #endregion

        #region Get Customer Address List

        public List<CYGNUSPickupRequest_Address> Get_CustAddress_List(string CustomerCode, string Brcd, string PreferedLocation)
        {
            string SQRY = "EXEC USP_Distinct_Customer_Address '" + CustomerCode + "','" + Brcd + "','" + PreferedLocation + "' ";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<CYGNUSPickupRequest_Address> List = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest_Address>(dataTable);
            return List;
        }

        public Stream CustAddress_List(string CustomerCode, string Brcd, string PreferedLocation)
        {
            List<CYGNUSPickupRequest_Address> List = Get_CustAddress_List(CustomerCode, Brcd, PreferedLocation);
            return GF.ReturnJSONStream<List<CYGNUSPickupRequest_Address>>(List);
        }

        #endregion

        #region Get Driver Assign PRQ

        public List<CYGNUSPickupRequest> Get_Driver_AssignPRQ_List(string DriverID, string UserName, string BRCD, string PRQNo)
        {
            string SQRY = "EXEC USP_GetDriver_AssignPRQList '" + DriverID + "','" + UserName + "' ,'" + BRCD + "','" + PRQNo + "'";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<CYGNUSPickupRequest> List = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(dataTable);
            return List;
        }

        public Stream Driver_AssignPRQ_List(string DriverID, string UserName, string BRCD, string PRQNo)
        {
            List<CYGNUSPickupRequest> List = Get_Driver_AssignPRQ_List(DriverID, UserName, BRCD, PRQNo);
            return GF.ReturnJSONStream<List<CYGNUSPickupRequest>>(List);
        }

        #endregion

        #region Assign Remove Driver Submit

        public Stream PRQAssignRemoveDriver(AssignRemoveDriver AssignRemoveDriver)
        {
            string SQRY = "EXEC USP_Assign_Declaine_Driver '" + AssignRemoveDriver.DriverID + "','" + AssignRemoveDriver.VehicleNo + "','" +
                AssignRemoveDriver.UserName + "','" + AssignRemoveDriver.Brcd + "','" + AssignRemoveDriver.PRQNo + "','" + AssignRemoveDriver.Flag + "'";

            DataSet DS = GF.GetDataSetFromSP(SQRY);

            CYGNUS_Customer_PreferedLocationResponse ObjResponse = new CYGNUS_Customer_PreferedLocationResponse();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUS_Customer_PreferedLocationResponse>(DS.Tables[0]).First();

            var jsonObj = new
            {
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        #endregion

        #region Complete PRQ Submit

        public Stream CompletePRQSubmit(CompletePRQ CompletePRQ)
        {
            string SQRY = "EXEC USP_Complete_PRQ '" + CompletePRQ.UserName + "','" + CompletePRQ.Brcd + "','" +
                CompletePRQ.DriverId + "','" + CompletePRQ.PrqNo + "','" + CompletePRQ.ActualWeight + "','" + CompletePRQ.ActualPKGS + "'";

            DataSet DS = GF.GetDataSetFromSP(SQRY);

            CYGNUS_Customer_PreferedLocationResponse ObjResponse = new CYGNUS_Customer_PreferedLocationResponse();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUS_Customer_PreferedLocationResponse>(DS.Tables[0]).First();

            var jsonObj = new
            {
                Success = ObjResponse.STATUS,
                Message = ObjResponse.MESSAGE
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        #endregion

        #endregion

        #region Quick Docket API

        /*
        Link :- http://103.232.124.146/RCPLService_Test/AndroidServices.svc/QuickDocketGeneration
        Json :- {"Address": "RC2564","CustomerCode": "123456","LandMark": "false","City": "868139027529863","Pincode": "3",
      "Name": "3","MobileNo": "3","Weight": "3","PKGS": "3","ImeiNo": "3","OriginLat": "3","OriginLong": "3","Orgncd": "SR1T",
      "Remarks": "3","Destcd": "SRT1","FinStartYear": "2017","FinEndYear": "2018"}
            */


        public Stream QuickDocketGeneration(QuickDocketViewModel QuickDocketViewModel)
        {
            string DocketXML = "<quick><docket>";
            DocketXML = DocketXML + "<party_code>" + QuickDocketViewModel.CustomerCode + "</party_code>";
            DocketXML = DocketXML + "<from_loc>" + QuickDocketViewModel.City + "</from_loc>";
            DocketXML = DocketXML + "<orgncd>" + QuickDocketViewModel.Orgncd + "</orgncd>";
            DocketXML = DocketXML + "<destcd>" + QuickDocketViewModel.Destcd + "</destcd>";
            DocketXML = DocketXML + "<pkgsno>" + QuickDocketViewModel.PKGS + "</pkgsno>";
            DocketXML = DocketXML + "<actuwt>" + QuickDocketViewModel.Weight + "</actuwt>";
            DocketXML = DocketXML + "<chrgwt>0</chrgwt>";
            DocketXML = DocketXML + "<entryby>" + QuickDocketViewModel.CustomerCode + "</entryby>";
            DocketXML = DocketXML + "<brcd>" + QuickDocketViewModel.Orgncd + "</brcd>";
            DocketXML = DocketXML + "</docket></quick>";

            string SQRY = "exec USP_Direct_QUICK_DOCKET_ENTRY '" + DocketXML + "','" + QuickDocketViewModel.FinStartYear + "','" + QuickDocketViewModel.CustomerCode + "','','" + QuickDocketViewModel.Orgncd + "'";
            int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "QuickDocketGeneration", "", "");

            DataSet DS = GF.GetDataSetFromSP(SQRY);

            CYGNUS_QuickDocketResponse ObjResponse = new CYGNUS_QuickDocketResponse();
            ObjResponse = DataRowToObject.CreateListFromTable<CYGNUS_QuickDocketResponse>(DS.Tables[0]).First();

            var jsonObj = new
            {
                Success = ObjResponse.Status,
                Message = ObjResponse.Message,
                DockNo = ObjResponse.DockNo
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            string returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));
            return memoryStream;
        }

        public partial class CYGNUS_QuickDocketResponse
        {
            public string DockNo { get; set; }
            public string Message { get; set; }
            public int Status { get; set; }
        }

        #endregion

        #region API Docket Tracking

        public Stream DocketTracking(string DocketNo)
        {
            string SQRY = "exec [webxNet_Tracking_Ver1] '','','','','','','" + DocketNo + "','','','','DKT','1','20'";
            DataTable DT = GF.GetDataTableFromSP(SQRY);
            List<vw_OperationTrack> List = DataRowToObject.CreateListFromTable<vw_OperationTrack>(DT);

            System.Web.Script.Serialization.JavaScriptSerializer jSearializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string returnValue = jSearializer.Serialize(List);
            string Success = "1",Message="DONE";
            if(List.Count == 0)
            {
                Success = "0";
                Message = "Docket No is not Valid";
            }
            var jsonObj = new
            {
                Success = Success,
                Message = Message,
                Response = List
            };
            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

            return memoryStream;
        }

        #endregion

        #region Change Customer PassWord

        public Stream Updte_Cust_Pass_API(Stream Cust)
        {
            string data = GF.StreamToJsonString(Cust);
            SortedList<string, object> context = new SortedList<string, object>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            DataTable Dt = new DataTable();

            context = js.Deserialize<SortedList<string, object>>(data);
            webx_CUSTHDR_Android ObjCUST = GF.DictionaryToObjectList<webx_CUSTHDR_Android>(((object[])(context["Cust_Change_Pass"]))).First();

            Dt = Update_Cust_Pass(ObjCUST);

            webx_CUSTHDR_Android_Response ObjResponse = new webx_CUSTHDR_Android_Response();
            List<webx_CUSTHDR_Android_Response> List = DataRowToObject.CreateListFromTable<webx_CUSTHDR_Android_Response>(Dt);
            return GF.ReturnJSONStream<List<webx_CUSTHDR_Android_Response>>(List);
        }

        public DataTable Update_Cust_Pass(webx_CUSTHDR_Android ObjCUST)
        {
            string SQR = "", ErrorMessage = "";
            int IDS = 0;
            bool status = false;

            DataTable Dt = new DataTable();
            try
            {
                SQR = "exec USP_Change_CustPass '" + ObjCUST.CUSTCD + "','" + ObjCUST.CUSTPASS + "','" + ObjCUST.UserName + "','" + ObjCUST.Location + "','" + ObjCUST.Company + "','" + ObjCUST.IMEINo + "'";
                IDS = GF.SaveRequestServices(SQR.Replace("'", "''"), "Update_Cust_Pass", "0", "");
                Dt = GF.GetDataTableFromSP(SQR);
                //Status = Dt.Rows[0]["Status"].ToString();
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            finally
            {
                if (ErrorMessage == "")
                {
                    ErrorMessage = Dt.Rows[0]["Message"].ToString();
                }
                if (ErrorMessage.ToUpper() == "DONE")
                {
                    status = true;
                }
                // IDS = SaveRequestServices("", "Insert_Update_User_API", status, ErrorMessage, UserName, "HQTR", "C001", IDS);
            }
            return Dt;
        }

        #endregion

        #region Check Docket Status API

        public Stream Check_Docket_Status_API(Stream Docket_st)
        {
            string data = GF.StreamToJsonString(Docket_st);
            SortedList<string, object> context = new SortedList<string, object>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            DataTable Dt = new DataTable();

            context = js.Deserialize<SortedList<string, object>>(data);
            Webx_DocketNo ObjDkt = GF.DictionaryToObjectList<Webx_DocketNo>(((object[])(context["Docket_List"]))).First();

            Dt = Docket_Delivered_Status(ObjDkt.DOCKNO);
            List<Webx_DocketNo_Del_Status> List = DataRowToObject.CreateListFromTable<Webx_DocketNo_Del_Status>(Dt);
            return GF.ReturnJSONStream<List<Webx_DocketNo_Del_Status>>(List);
        }

        public DataTable Docket_Delivered_Status(string Dockets)
        {
            string SQR = "", ErrorMessage = "";
            int IDS = 0;

            DataTable Dt = new DataTable();
            try
            {
                SQR = "exec USP_Get_Docket_Delivered_Status '" + Dockets + "'";
                IDS = GF.SaveRequestServices(SQR.Replace("'", "''"), "USP_Get_Docket_Delivered_Status", "0", "");
                Dt = GF.GetDataTableFromSP(SQR);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
            return Dt;
        }

        #endregion

        #region Get Customer APP Version Check

        public List<Webx_Master_General_Tab> GetCustomerAPPVersion_List()
        {
            string SQRY = "EXEC USP_CustAPKVersion ";
            DataTable dataTable = GF.GetDataTableFromSP(SQRY);
            List<Webx_Master_General_Tab> List = DataRowToObject.CreateListFromTable<Webx_Master_General_Tab>(dataTable);
            return List;
        }

        public Stream GetCustomerAPPVersion()
        {
            List<Webx_Master_General_Tab> List = GetCustomerAPPVersion_List();

            System.Web.Script.Serialization.JavaScriptSerializer jSearializer =
           new System.Web.Script.Serialization.JavaScriptSerializer();
            string returnValue = jSearializer.Serialize(List);

            var jsonObj = new
            {
                DRIVER = List.Where(c => c.CodeId == "DRIVER").ToList(),
                SUPERVISOR = List.Where(c => c.CodeId == "SUPERVISOR").ToList(),
                CUSTOMER = List.Where(c => c.CodeId == "CUSTOMER").ToList()
            };

            JavaScriptSerializer jScriptSerializer = new JavaScriptSerializer();
            returnValue = jScriptSerializer.Serialize(jsonObj);

            WebOperationContext.Current.OutgoingResponse.ContentType = "application/json; charset=utf-8";
            MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(returnValue));

            return memoryStream;

            //return GF.ReturnJSONStream<List<Webx_Master_General_Tab>>(List);
        }


        #endregion
        public string uploadImage(Stream fileContents)
        {
            MultipartParser parser = new MultipartParser(fileContents);
            try
            {
                string fileName1 = "", contentType = "";
                System.Drawing.Image image;
                try
                {                   
                    fileName1 = parser.Filename;
                    var fileName = Path.GetFileName(fileName1);
                    contentType = parser.ContentType;
                    byte[] fileContent = parser.FileContents;

                    if (contentType != "image/jpeg")
                    {                        

                        FileStream fileToupload = new FileStream("D:\\Code\\RCPL_Live\\Images\\FMScanDocument\\" + fileName, FileMode.Create);

                        byte[] bytearray = fileContent;
                        int bytesRead, totalBytesRead = 0;
                        do
                        {
                            bytesRead = fileContents.Read(bytearray, 0, bytearray.Length);
                            totalBytesRead += bytesRead;
                        } while (bytesRead > 0);

                        fileToupload.Write(bytearray, 0, bytearray.Length);
                        fileToupload.Close();
                        fileToupload.Dispose();
                    }
                    else
                    {
                        image = byteArrayToImage(fileContent);
                        string FolDerPath = "D:\\Code\\RCPL_Live\\Images\\FMScanDocument\\" + fileName1;   
                        image.Save(FolDerPath);
                    }
                    string Link = "http://103.232.124.146:5299/TestImage/" + fileName1;
                    return "DONE___" + Link;
                }
                catch (Exception exx)
                {
                    return "ERROR "+ exx.Message;
                }
            }
            catch (Exception ex)
            {
                return "ERROR 456" + ex.Message;
            }
        }

        #region Dashboard Data API

        //public string DashboardData(string ProcName)
        //{
        //    string SQR = "", Message = "";
        //    string folderPath = ConfigurationManager.AppSettings["App_Data"].ToString();
        //    string curFile = folderPath + ProcName + ".txt";

        //    DataTable Dt = new DataTable();
        //    try
        //    {
        //        SQR = "EXEC " + ProcName;
        //        Dt = GF.GetDataTableFromSP(SQR);
        //        File.WriteAllText(curFile, JsonConvert.SerializeObject(Dt));
        //        Message = "Done";
        //    }
        //    catch (Exception ex)
        //    {
        //        Message = ex.Message;
        //    }
        //    return Message;
        //}

        public DataTable GetDashboardData(string ProcName)
        {
            string SQR = "EXEC " + ProcName;
            return GF.GetDataTableFromSP(SQR);
        }

        #endregion
    }
}
