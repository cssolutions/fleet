﻿function EmpCode_Change(DomainName, Val, HDR_Prefix, IS_Emp, IS_Driver, IS_Vendor, IS_Labour) {

    var URL = "";
    if (IS_Emp == "1") {
        var URL = DomainName + '/BranchAccounting/Get_Bank_Details?No=' + Val + "&Type=1";
    }
    else if (IS_Driver == "1") {
        var URL = DomainName + '/BranchAccounting/Get_Bank_Details?No=' + Val + "&Type=3";
    }
    else if (IS_Vendor == "1") {
        var URL = DomainName + '/BranchAccounting/Get_Bank_Details?No=' + Val + "&Type=2";
    }
    if (Val != "") {

        App.blockUI({ boxed: true });
        $.ajax({
            url: URL,
            cache: false,
            data: {},
            type: 'POST',
            success: function (data) {

                $("#ShowEmpCode").val($("#" + HDR_Prefix + "EmpCode").val());

                if (data.IsRecordFound == "1") {

                    $("#" + HDR_Prefix + "EmpName").val(data.Name);
                    $("#" + HDR_Prefix + "BankName").val(data.Name_Of_bank);
                    $("#" + HDR_Prefix + "Bank_Name").val(data.Name_Of_bank);
                    $("#" + HDR_Prefix + "BankAccNumber").val(data.Bank_AC_Number);
                    $("#" + HDR_Prefix + "IFSCCode").val(data.IFSC_Code);
                    $("#" + HDR_Prefix + "Department").val(data.Department);
                    $("#" + HDR_Prefix + "HOD_Name").val(data.HOD_Name);
                    $("#" + HDR_Prefix + "Grade").val(data.Grade);
                    $("#" + HDR_Prefix + "Mobile_Number").val(data.MobileNo);
                    App.unblockUI();
                }
                else {
                    $("#" + HDR_Prefix + "EmpName").val($("#s2id_" + HDR_Prefix + "EmpCode span").html());
                    $("#" + HDR_Prefix + "BankName").val("");
                    $("#" + HDR_Prefix + "BankAccNumber").val("");
                    $("#" + HDR_Prefix + "IFSCCode").val("");
                    App.unblockUI();
                }
            },
            error: function (req, status, error) {
                $("#ShowEmpCode").val("");
                $("#" + HDR_Prefix + "EmpName").val($("#s2id_" + HDR_Prefix + "EmpCode span").html());
                $("#" + HDR_Prefix + "BankName").val("");
                $("#" + HDR_Prefix + "BankAccNumber").val("");
                $("#" + HDR_Prefix + "IFSCCode").val("");
                App.unblockUI();
            }
        });
    }
    else {
        $("#ShowEmpCode").val("");
        $("#" + HDR_Prefix + "EmpCode").val("");
        $("#" + HDR_Prefix + "EmpName").val("");
        $("#" + HDR_Prefix + "BankName").val("");
        $("#" + HDR_Prefix + "BankAccNumber").val("");
        $("#" + HDR_Prefix + "IFSCCode").val("");
    }
}

function BR_Request_Common_Function(DomainName, HDR_Prefix, DET_Prefix, FormID) {

    /* If User Bank Details Not Available Then Remove Bank At Payment Type */
    //if ($("#IS_EMP_BankDetails").val() == "0" || $("#IS_EMP_BankDetails").val() == "") {
    //    $("#PAYMENTMODE_1__PaymentMode option[value='Bank']").remove();
    //}
    /* Common Delet Row Option */
    $(".deleteRow").live("click", function () {
        $(this).parents("tr.editorRow:first").remove();
        var tableName = $('.deleteRow').parents('table').attr('id');
        if (tableName != undefined) {
            if ($("#" + tableName + " tbody tr").length == "1") {
                $(".deleteRow").hide();
            }
            else {
                $(".deleteRow").show();
            }
        }
        return false;
    });

    $("input[name='numberTypeR']").live("change", function () {
       
        var numberType = $('input[name=numberTypeR]:checked').val();
        $("#numberType").val(numberType);
        $("#PAYMENTMODE_1__ChequeNo").attr("placeholder", $('input[name=numberTypeR]:checked').parents('label').text().trim());
        if (numberType == "C") {
            $("#PAYMENTMODE_1__ChequeNo").val("");
            $("#PAYMENTMODE_1__ChequeNo").attr("maxlength", "6");
            $("#PAYMENTMODE_1__ChequeNo").attr("minlength", "6");
            $("#PAYMENTMODE_1__ChequeNo").attr("number", true);
        }
        else {
            $("#PAYMENTMODE_1__ChequeNo").val("");
            $("#PAYMENTMODE_1__ChequeNo").attr("maxlength", "22");
            $("#PAYMENTMODE_1__ChequeNo").attr("minlength", "6");
            $("#PAYMENTMODE_1__ChequeNo").removeAttr("number", false);
        }
    });

    /* Common Approval Show Screen */
    if ($("#SubType").val() == "2") {
        $(".clsApproval").show();
    }
    else {
        $(".clsApproval").hide();
    }

    /* Common Approval/Rejection Disable Fields */
    if ($("#SubType").val() == "2") {
        $(".clsApproval_Rejection").attr("disabled", true)
        $("#PaymentPart :input").attr("disabled", true);
        $("#Approval_Rejection_Part :input").attr("disabled", true);
    }

    if ($("#SubType").val() == "4") {
        $(".clsApproval_Rejection_Destion").attr("disabled", false)
    }

    /* Amount_Passed Change Event */
    var Passed_Amount = 0;
    var Rjected_Amount = 0;
    $(".clsAmount_Passed").each(function () {
        Passed_Amount = parseFloat(Passed_Amount) + parseFloat(($(this).val() == "" || $(this).val() == null) ? 0.00 : $(this).val());
    });
    $("#Amount_Passed_Tot").val(parseFloat(Passed_Amount).toFixed(2));

    $(".clsAmount_Rejected").each(function () {
        Rjected_Amount = parseFloat(Rjected_Amount) + parseFloat(($(this).val() == "" || $(this).val() == null) ? 0.00 : $(this).val());
    });
    $("#Amount_Rejected_Tot").val(parseFloat(Rjected_Amount).toFixed(2));

    $(".clsAmount_Passed").focusout(function () {
        Passed_Amount = 0;
        Rjected_Amount = 0;
        if ($("#" + HDR_Prefix + "ExpenceType").val() != "3") {
            var ID = $(this).attr("Id").replace("Amount_Passed", "");
            var TotalBill = "";

            if ($("#" + HDR_Prefix + "ExpenceType").val() == "4") {
                TotalBill = $("#" + ID + "AmountPaid_Per_Branch").val();
            }
            else if ($("#" + HDR_Prefix + "ExpenceType").val() == "5") {
                TotalBill = $("#" + ID + "TotalClaim_IncHandling").val();
            }
            else if ($("#" + HDR_Prefix + "ExpenceType").val() == "2" || $("#" + HDR_Prefix + "ExpenceType").val() == "6" || $("#" + HDR_Prefix + "ExpenceType").val() == "7" || $("#" + HDR_Prefix + "ExpenceType").val() == "8" || $("#" + HDR_Prefix + "ExpenceType").val() == "9" || $("#" + HDR_Prefix + "ExpenceType").val() == "12") {
                TotalBill = $("#" + ID + "Amount").val();
            }
            else if ($("#" + HDR_Prefix + "ExpenceType").val() != "4") {
                TotalBill = $("#" + ID + "Total_Amount").val();
            }
            var PassesAmt = $("#" + ID + "Amount_Passed").val();
            var Reject = parseFloat(TotalBill) - parseFloat(PassesAmt == 0 || PassesAmt == "" ? 0 : PassesAmt);

            $("#" + ID + "Amount_Rejected").val(parseFloat(Reject).toFixed(2));

            if ($("#" + HDR_Prefix + "ExpenceType").val() == "6") {
                var Tot_Amount_Passed = 0;
                var UNIT_COST_AS_PER_PASSED_AMOUNT = 0;
                $(".clsAmount_Passed").each(function () {
                    Tot_Amount_Passed = parseFloat(Tot_Amount_Passed) + +parseFloat(($(this).val() == "" || $(this).val() == null) ? 0.00 : $(this).val());
                })
                $("#MarketVehicleCharges_Expence_HDR_Total_Amount_Passed").val(Tot_Amount_Passed);

                UNIT_COST_AS_PER_PASSED_AMOUNT = parseFloat($("#MarketVehicleCharges_Expence_HDR_Total_Amount_Passed").val()) / parseFloat($("#MarketVehicleCharges_Expence_HDR_Total_Weight").val());
                $("#MarketVehicleCharges_Expence_HDR_Unit_Cost_Pased_Amount").val(parseFloat(UNIT_COST_AS_PER_PASSED_AMOUNT).toFixed(2));
            }
        }

        $(".clsAmount_Passed").each(function () {
            Passed_Amount = parseFloat(Passed_Amount) + parseFloat(($(this).val() == "" || $(this).val() == null) ? 0.00 : $(this).val());
        });


        $("#Amount_Passed_Tot").val(parseFloat(Passed_Amount).toFixed(2));

        $(".clsAmount_Rejected").each(function () {
            Rjected_Amount = parseFloat(Rjected_Amount) + parseFloat(($(this).val() == "" || $(this).val() == null) ? 0.00 : $(this).val());
        });
        $("#Amount_Rejected_Tot").val(parseFloat(Rjected_Amount).toFixed(2));
    });

    /* Disable field when Amount is Zero.*/

    $(".clsFixed_Amount_Passed").each(function () {
        if ($("#" + $(this).attr('id').replace("Amount_Passed", "Amount")).val() > 0) {
            $(this).attr("disabled", false);
        }
        else {
            $(this).attr("disabled", true);
        }
    });


    /* Payment Submit Process */
    if ($("#" + HDR_Prefix + "ExpenceType").val() == "1" || $("#" + HDR_Prefix + "ExpenceType").val() == "2" || $("#" + HDR_Prefix + "ExpenceType").val() == "3" || $("#" + HDR_Prefix + "ExpenceType").val() == "4" || $("#" + HDR_Prefix + "ExpenceType").val() == "6" || $("#" + HDR_Prefix + "ExpenceType").val() == "7" || $("#" + HDR_Prefix + "ExpenceType").val() == "8" || $("#" + HDR_Prefix + "ExpenceType").val() == "9" || $("#" + HDR_Prefix + "ExpenceType").val() == "12") {

        if ($("#SubType").val().replace(" ", "") == "3") {

            $("#PaymentControl").show();
            var AmountApplicable = 0;
            var AdvAmount = 0;

            $(".clsAmount_Passed").each(function () {
                AmountApplicable = parseFloat(AmountApplicable) + parseFloat($(this).val());
            });

            if ($("#" + HDR_Prefix + "ExpenceType").val() == "3") {
                $(".ClsAdvAmount").each(function () {
                    AdvAmount = parseInt(AdvAmount) + parseInt($(this).val());
                });
                AmountApplicable =  Math.abs(parseInt(AmountApplicable) - parseInt(AdvAmount));
            }


            $("#WAD_AmountApplicable").val(AmountApplicable)
            $("#PAYMENTMODE_1__AmountApplicable").val(AmountApplicable)
            $("#PAYMENTMODE_1__NETAMOUNT").val(AmountApplicable)

            $("#PaymentPart :input").attr("disabled", true);
            $("#PaymentPart1 :input").attr("disabled", true);

            if ($("#WAD_AmountApplicable").val() == "0") {
                $("#btnApprovSubmit").attr("disabled", "disabled");
            }
            $("#PAYMENTMODE_1__PaymentMode").change(function () {

                if ($(this).val() == "Bank") {
                    $("#PAYMENTMODE_1__ChequeAmount").val($("#PAYMENTMODE_1__AmountApplicable").val());
                }
                else {
                    $("#PAYMENTMODE_1__CashAmount").val($("#PAYMENTMODE_1__AmountApplicable").val());
                }
            })

            $("#btnApprovSubmit").click(function () {

                Formvalidate(DomainName, FormID);

                if ($('#' + FormID).valid()) {
                    App.blockUI({ boxed: true });

                    $("#PaymentPart :input").attr("disabled", false);
                    $("#PaymentPart1 :input").attr("disabled", false);

                    if ($("#" + HDR_Prefix + "ExpenceType").val() == "1") {
                        $('#form_sample').attr('action', DomainName + '/BranchAccounting/Accounting_Payment_Submit_New_Process');
                    }
                   else if ($("#" + HDR_Prefix + "ExpenceType").val() == "2")
                    {
                        $('#form_sample').attr('action', DomainName + '/BranchAccounting/Accounting_Payment_Submit_New_Process');
                    }
                    else if ($("#" + HDR_Prefix + "ExpenceType").val() == "3") {
                        $('#form_sample').attr('action', DomainName + '/BranchAccounting/Travel_Accounting_Advance_Payment_Submit');
                    }
                    else
                    {
                        $('#form_sample').attr('action', DomainName + '/BranchAccounting/Accounting_Payment_Submit');

                    }
                    $("#container :input").attr("disabled", false);
                    document.forms[FormID].submit()
                    $('#' + FormID + '').submit();
                }
            });
        }
    }
}

function IsServicetaxCalculated(DomainName, Prefix) {

    if ($('#WAD_IsServicetaxapply').prop('checked')) {

        $('#WAD_ServiceTaxRate').attr("readonly", false);
        $('#WAD_ServiceTaxRate').attr("readonly", false);
        $('#WAD_ServiceTaxRate').val($("#HdnServiceTaxRate").val());
        $('#WAD_ServiceTaxRate').attr("readonly", true);

        var servicetaxrate = $("#HdnServiceTaxRate").val();
        var ApplicaleAmount = $("#WAD_AmountApplicable").val();
        var Servicetax = Math.round(parseFloat(ApplicaleAmount) * parseFloat(servicetaxrate) / 100);
        var TotalAmount = Math.round(Math.round(ApplicaleAmount) + Math.round(Servicetax));

        $("#WAD_ServiceTax").val(Math.round(Servicetax));
        $("#WAD_AmountApplicableForTDS").val(Math.round(TotalAmount));
        $("#" + Prefix + "NETAMOUNT").val(Math.round(TotalAmount));
        $("#" + Prefix + "AmountApplicable").val(Math.round(TotalAmount));
    }
    else {
        $('#WAD_ServiceTaxRate').attr('readonly', true);
        $('#WAD_ServiceTaxRate').val(0)
        $("#WAD_ServiceTax").val(0);
        var ApplicaleAmount = $("#WAD_AmountApplicable").val();
        $("#WAD_AmountApplicableForTDS").val(Math.round(ApplicaleAmount));
    }

    if ($('#WAD_IsTDStaxapply').prop('checked')) {

        $('#WAD_TDSRate').attr("readonly", false);
        var TDSrate = $("#WAD_TDSRate").val();
        var TDSApplicaleAmount = $("#WAD_AmountApplicableForTDS").val();
        var TDStax = Math.round(parseFloat(TDSApplicaleAmount) * parseFloat(TDSrate) / 100);
        var TotalAmount = Math.round(parseFloat(TDSApplicaleAmount) - parseFloat(TDStax));

        $("#WAD_TDSSection").addClass("required required2");
        $("#WAD_TDSSection").attr("readonly", false);
        $("#WAD_TDSAmount").val(Math.round(TDStax));

        $("#" + Prefix + "NETAMOUNT").val(Math.round(TotalAmount));
        $("#" + Prefix + "AmountApplicable").val(Math.round(TotalAmount));

        $(".clsTotal").val(Math.round(TotalAmount));
        if ($('#' + Prefix + 'PaymentMode').val() == "Cash") {
            $("#" + Prefix + "CashAmount").val(Math.round(TotalAmount));
        }
        else if ($('#' + Prefix + 'PaymentMode').val() == "Bank") {
            $("#" + Prefix + "ChequeAmount").val(Math.round(TotalAmount));
        }
        else {
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "CashAmount").val(0);
        }
    }
    else {
        $('#WAD_TDSRate').attr("readonly", true);
        $('#WAD_TDSRate').val(0);
        $("#WAD_TDSAmount").val(0);
        $("#WAD_TDSAmount").attr("readonly", true);
        var ApplicaleAmount = $("#WAD_AmountApplicableForTDS").val();
        $("#WAD_AmountApplicableForTDS").val(ApplicaleAmount);
        $("#" + Prefix + "NETAMOUNT").val(Math.round($("#WAD_SumAmount").val()));

        $("#" + Prefix + "AmountApplicable").val(Math.round(ApplicaleAmount/*$("#WAD_SumAmount").val()*/));

        $(".clsTotal").val(Math.round(ApplicaleAmount));

        $("#WAD_TDSSection").val("");
        $("#WAD_TDSSection").select2().find(":selected").data("");
        $("#WAD_TDSSection").removeClass("required required2");
        $("#WAD_TDSSection").attr("readonly", true);

        if ($('#' + Prefix + 'PaymentMode').val() == "Cash") {
            $("#" + Prefix + "CashAmount").val(Math.round(ApplicaleAmount));
        }
        else if ($('#' + Prefix + 'PaymentMode').val() == "Bank") {
            $("#" + Prefix + "ChequeAmount").val(Math.round(ApplicaleAmount));
        }
        else {
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "CashAmount").val(0);
        }
    }
}

function OnLoadFunction(DomainName, Prefix, HDR_Prefix) {

    var GetChequedate = $("#" + Prefix + "Chequedate").val();

    if (jQuery().datepicker) {
        $("#" + Prefix + "Chequedate").datepicker({
            rtl: App.isRTL(),
            format: "dd M yyyy",
            autoclose: true,
            //startDate: new Date(),
            //endDate: new Date(),
        });
        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    $("#" + Prefix + "TransactionMode").change(function () {

        $("#" + Prefix + "CashAmount").val(0);
        $("#" + Prefix + "ChequeAmount").val(0);
        $("#" + Prefix + "ChequeNo").val("");
        $("#" + Prefix + "Chequedate").val("");
        $("#" + Prefix + "ReceivedFromBank").val("");
        $("#" + Prefix + "PaymentMode").val("");
        $("#" + Prefix + "PaymentMode").select2().find(":selected").data("");
        $("#" + Prefix + "CashAccount").select2('data', null);
        $("#" + Prefix + "DepositedInBank").select2('data', null);

        if ($("#" + Prefix + "TransactionMode").val() == "PAYMENT") {
            //$("input[name=optionsRadios1]").attr("disabled", true);
            //$('#' + Prefix + 'IsonAccount').attr("disabled", true);
            //$('#' + Prefix + 'IsonAccount').attr("checked", false);
            //$('#' + Prefix + 'IsonAccount').parent('span').removeClass('checked');
            //$('#' + Prefix + 'IsonAccount').val(false);CASHBANKLABEL
            $('#labelBank').text("Issued in bank");
            $('#CASHBANKLABEL').hide();
        }
        else {

            //$("input[name=optionsRadios1]").attr("disabled", false);
            //$('#' + Prefix + 'IsonAccount').attr("checked", false);
            //$('#' + Prefix + 'IsonAccount').parent('span').removeClass('checked');
            //$('#' + Prefix + 'IsonAccount').val(false);
            $('#labelBank').text("Deposited in bank");
            $("#" + Prefix + "DepositedInBank").attr("disabled", true);
            $('#CASHBANKLABEL').show();
        }
    });

    $("#" + Prefix + "PaymentMode").change(function () {

        var PaymentMode = $(this).val();

        if (PaymentMode == "Cash") {

            App.blockUI({ boxed: true });

            var StrURL = DomainName + "/Voucher/GetBankCashOtherContraList_BranchAccounting?Type=" + PaymentMode;
            FillDropDownfromOther(Prefix + "CashAccount", StrURL, "Cash Account");

            $("#" + Prefix + "CashAccount").select2('data', null);
            $("#" + Prefix + "DepositedInBank").select2('data', null);
            $("#" + Prefix + "ContraAccount").select2("data", null);
            $("#" + Prefix + "DepositedInBank").attr("disabled", true);
            $("#" + Prefix + "CashAccount").attr("disabled", false);
            $("#" + Prefix + "ContraAccount").attr("disabled", true);
            $("#" + Prefix + "CashAccount option:selected").remove();
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "CashAmount").val($(".clsTotal").val());
            $("#" + Prefix + "CashAmount").attr("readonly", true);
            $("#" + Prefix + "ChequeNo").attr("disabled", true);
            $("#" + Prefix + "ChequeAmount").attr("readonly", true);
            $("#" + Prefix + "Chequedate").attr("disabled", true);
            $("#" + Prefix + "Chequedate").val("");
            $(".input-group-btn").hide();
            //$('#' + Prefix + 'IsonAccount').attr("checked", false);
            //$('#' + Prefix + 'IsonAccount').parent('span').removeClass('checked');
            //$('#' + Prefix + 'IsonAccount').val(false);
            $("#" + Prefix + "ReceivedFromBank").removeClass('required required2');
            $("#" + Prefix + "CashAccount").addClass('required required2');
            $("#" + Prefix + "DepositedInBank").removeClass('required required2');
        }

        else if (PaymentMode == "Bank") {

            App.blockUI({ boxed: true });

            $("#" + Prefix + "DepositedInBank").select2('data', null);
            $("#" + Prefix + "ContraAccount").select2("data", null);
            $("#" + Prefix + "CashAccount").select2('data', null);
            $("#" + Prefix + "DepositedInBank").attr("disabled", false);
            $("#" + Prefix + "CashAccount").attr("disabled", true);
            $("#" + Prefix + "ContraAccount").attr("disabled", true);

            var StrURL = DomainName + "/Voucher/GetBankCashOtherContraList_BranchAccounting?Type=" + PaymentMode;
            FillDropDownfromOther(Prefix + "DepositedInBank", StrURL, "Bank Account");

            $(".input-group-btn").show();
            $("#" + Prefix + "CashAmount").val(0);
            $("#" + Prefix + "CashAmount").attr("readonly", true);
            $("#" + Prefix + "ChequeAmount").val($(".clsTotal").val());
            $("#" + Prefix + "ChequeAmount").attr("readonly", true);
            $("#" + Prefix + "ChequeNo").attr("disabled", false);
            $("#" + Prefix + "Chequedate").attr("disabled", false);
            $("#" + Prefix + "Chequedate").val(GetChequedate);
            $("#" + Prefix + "ChequeNo").addClass('required required2');
            $("#" + Prefix + "ReceivedFromBank").addClass('required required2');
            $("#" + Prefix + "CashAccount").removeClass('required required2');
            $("#" + Prefix + "DepositedInBank").addClass('required required2');

            App.unblockUI();
        }
        else if (PaymentMode == "CONTRA" || PaymentMode == "OTHER") {
            App.blockUI({ boxed: true });
            $("#" + Prefix + "DepositedInBank").select2('data', null);
            $("#" + Prefix + "CashAccount").select2('data', null);
            $("#" + Prefix + "DepositedInBank").attr("disabled", true);
            $("#" + Prefix + "CashAccount").attr("disabled", true);
            $("#" + Prefix + "ContraAccount").attr("disabled", false);

            var StrURL = DomainName + "/Voucher/GetBankCashOtherContraList_BranchAccounting?Type=" + PaymentMode + "&ExpenseType=" + HDR_Prefix;
            FillDropDownfromOther(Prefix + "ContraAccount", StrURL, "Contra Account");

            $(".input-group-btn").show();
            $("#" + Prefix + "CashAmount").val(0);
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "ChequeNo").attr("disabled", true);
            $("#" + Prefix + "CashAmount").attr("readonly", true);
            $("#" + Prefix + "ChequeAmount").attr("readonly", true);
            $("#" + Prefix + "Chequedate").attr("disabled", true);
            $("#" + Prefix + "Chequedate").val(GetChequedate);
            $("#" + Prefix + "ContraAccount").attr("disabled", false);
            $("#" + Prefix + "ChequeNo").removeClass('required required2');
            $("#" + Prefix + "ReceivedFromBank").removeClass('required required2');
            $("#" + Prefix + "CashAccount").removeClass('required required2');
            $("#" + Prefix + "DepositedInBank").removeClass('required required2');
            $("#" + Prefix + "ContraAccount").addClass('required required2');

            App.unblockUI();
        }
        else {

            App.blockUI({ boxed: true });

            var StrURL = DomainName + "/Voucher/GetBankCashList?Type=Cash";
            $("#" + Prefix + "CashAccount").select2('data', null);
            FillDropDownfromOther(Prefix + "CashAccount", StrURL, "Cash Account");
            var StrURL1 = DomainName + "/Voucher/GetBankCashList?Type=Bank";
            $("#" + Prefix + "DepositedInBank").select2('data', null);
            FillDropDownfromOther(Prefix + "DepositedInBank", StrURL1, "Bank Account");
            $("#" + Prefix + "CashAmount").val(0);
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "ChequeNo").attr("disabled", false);
            $("#" + Prefix + "CashAmount").attr("readonly", false);
            $("#" + Prefix + "ChequeAmount").attr("readonly", false);
            $("#" + Prefix + "Chequedate").attr("disabled", false);
            $("#" + Prefix + "Chequedate").val(GetChequedate);
            //$('#' + Prefix + 'IsonAccount').attr("checked", false);
            //$('#' + Prefix + 'IsonAccount').parent('span').removeClass('checked');
            //$('#' + Prefix + 'IsonAccount').val(false);
            $("#" + Prefix + "ReceivedFromBank").addClass('required required2');
            $("#" + Prefix + "DepositedInBank").attr("disabled", false);
            $("#" + Prefix + "CashAccount").attr("disabled", false);
            $("#" + Prefix + "CashAccount").addClass('required required2');
            $("#" + Prefix + "DepositedInBank").addClass('required required2');

            App.unblockUI();
        }
    });

    if ($("#WAD_Vouchertype").val() != "DEBITCREDIT") {
        $("#" + Prefix + "NETAMOUNT").attr("number", true).attr("min", 1).addClass("required required2");
    }

    $("#" + Prefix + "NETAMOUNT").live('change', function () {
        var PaymentMode = $("#" + Prefix + "PaymentMode").val();
        var PaymentAmount = $("#" + Prefix + "NETAMOUNT").val();
        if (PaymentMode.toUpperCase() == "CASH") {
            $("#" + Prefix + "ChequeAmount").val(0);
            $("#" + Prefix + "CashAmount").val(PaymentAmount);
            $(".clsTotal").val(PaymentAmount);
        }
        if (PaymentMode.toUpperCase() == "BANK") {
            $("#" + Prefix + "CashAmount").val(0);
            $("#" + Prefix + "ChequeAmount").val(PaymentAmount);
            $(".clsTotal").val(PaymentAmount);
        }
    });
}

function Formvalidate(DomainName, FormID) {

    var form1 = $('#' + FormID);
    var error1 = $('.alert-danger', form1);
    var success1 = $('.alert-success', form1);

    form1.validate({
        doNotHideMessage: true,
        errorElement: 'span', //default input error message container
        errorClass: 'help-block', // default input error message class
        // errorClass: 'validate-inline',
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",

        rules: {
            //account dateFuture
        },

        messages: {

        },

        invalidHandler: function (event, validator) { //display error alert on form submit
            success1.hide();
            error1.show();
            App.scrollTo(error1, -200);
        },

        highlight: function (element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        unhighlight: function (element) { // revert the change done by hightlight
            $(element)
                .closest('.form-group').removeClass('has-error'); // set error class to the control group
        },

        success: function (label) {
            label
                .closest('.form-group').removeClass('has-error'); // set success class to the control group
        },

        submitHandler: function (form) {
            success1.show();
            error1.hide();
            //document.form["form_sample"].submit();
        }
    });
}

