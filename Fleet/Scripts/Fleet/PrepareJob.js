﻿
var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input
            rules: {
            },
            messages: {
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {

        $('#btnSubmit').click(function () {

            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }

            App.blockUI({ boxed: true });
            $(".clsIsCloseDisables :input").attr("disabled", false);
            $(".clsIsCloseDisables").attr("disabled", false);
            document.forms["form_sample"].submit();
        });

        $('#btnOPClose').click(function () {

            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }

            $("#IsOperatinllyClose").val("1")
            $("#IsFinanciallyClose").val("0")

            $(".clsIsCloseDisables :input").attr("disabled", false);
            $(".clsIsCloseDisables").attr("disabled", false);

            $('#form_sample').attr('action', DomainName + '/Fleet/JobOrder_Operational_Financial');

            App.blockUI({ boxed: true });
            document.forms["form_sample"].submit();
        });

        $('#btnFinClose').click(function () {

            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }

            $("#IsOperatinllyClose").val("0")
            $("#IsFinanciallyClose").val("1")

            $(".clsIsCloseDisables :input").attr("disabled", false);
            $(".clsIsCloseDisables").attr("disabled", false);

            $('#form_sample').attr('action', DomainName + '/Fleet/JobOrder_Operational_Financial');
            App.blockUI({ boxed: true });
            document.forms["form_sample"].submit();
        });
    }

    var AddTaskDetailsRows = function (DomainName, itemIndex) {

        $("#btnAddRows").click(function () {

            App.blockUI({ boxed: true });

            var TOTRows = $("#TaskDetails").val();

            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;
                    AddTaskDetails(DomainName, itemIndex);

                }, 250 * i);
            }
        });
    }

    var AddTaskDetails = function (DomainName, itemIn) {

        var StrURL1 = DomainName + '/Fleet/_Task_Details/' + itemIn + "?ISClose=" + $("#ISClose").val();

        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                $(".clsSubmit").show(500);
                $("#Bodycontainer").append(html);
                App.unblockUI();
                Calculation(DomainName);

                var CommonIds = "TaskDetailsList_";

                var W_GRPCD = CommonIds + itemIn + "__W_GRPCD";
                var TaskTypeId = CommonIds + itemIn + "__TaskTypeId";
                var Taskcd = CommonIds + itemIn + "__Taskcd";
                var W_EST_LABOUR_HRS = CommonIds + itemIn + "__W_EST_LABOUR_HRS";
                var AMC = CommonIds + itemIn + "__AMC";
                var W_ACT_LABOUR_COST = CommonIds + itemIn + "__W_ACT_LABOUR_COST";

                select2ID(DomainName, W_GRPCD);
                select2ID(DomainName, TaskTypeId);
                select2ID(DomainName, Taskcd);

                $("#" + AMC).change(function () {

                    if ($(this).val() == "2") {
                        $("#" + W_ACT_LABOUR_COST).val("0");
                        Calculation(DomainName);
                    }
                });

                $("#" + W_GRPCD).change(function () {
                    App.blockUI({ boxed: true });
                    var StrURL = DomainName + "/Fleet/GetTaskDescription?id=" + $(this).val();
                    FillDropDownfromOther(Taskcd, StrURL, "GRPCD");
                });

                CommonFocusOut(DomainName, W_EST_LABOUR_HRS);
            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });
    }

    var AddTask_Spare_PartRows = function (DomainName, itemIndex) {

        $("#btnAddRows_Spare_Part").click(function () {

            App.blockUI({ boxed: true });

            var TOTRows = $("#Spare_Part_Requirements").val();

            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;
                    AddTask_Spare_Part(DomainName, itemIndex);

                }, 250 * i);
            }
        });
    }

    var AddTask_Spare_Part = function (DomainName, itemIn) {

        var StrURL1 = DomainName + '/Fleet/_Task_Spare_Part_Details/' + itemIn + "?ISClose=" + $("#ISClose").val();
        App.blockUI({ boxed: true });

        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                $(".clsSubmit").show(500);
                $("#Bodycontainer_Spare_Part").append(html);

                var CommonIDS = "Task_Spare_PartList_";
                var SKU_GRPCD = CommonIDS + itemIn + "__SKU_GRPCD";
                var PART_CODE = CommonIDS + itemIn + "__PART_CODE";
                var TaskTypeID = CommonIDS + itemIn + "__TaskTypeID";
                var PART_QTY = CommonIDS + itemIn + "__PART_QTY";
                var COST_UNIT = CommonIDS + itemIn + "__COST_UNIT";
                var COST = CommonIDS + itemIn + "__COST";
                var ACT_PART_QTY = CommonIDS + itemIn + "__ACT_PART_QTY";
                var ACT_COST_UNIT = CommonIDS + itemIn + "__ACT_COST_UNIT";


                //CommonFocusOut(DomainName, PART_QTY);
                CommonFocusOut(DomainName, COST_UNIT);
                CommonFocusOut(DomainName, COST);
                CommonFocusOut(DomainName, ACT_PART_QTY);
                CommonFocusOut(DomainName, ACT_COST_UNIT);

                select2ID(DomainName, SKU_GRPCD);
                select2ID(DomainName, PART_CODE);
                select2ID(DomainName, TaskTypeID);

                $("#" + SKU_GRPCD).change(function () {
                    var id = $(this).attr("id").replace("SKU_GRPCD", "");
                    SKU_GRPCD_PART_QTY_Change(DomainName, id);
                });

                $("#" + PART_QTY).focusout(function () {
                    var id = $(this).attr("id").replace("PART_QTY", "");
                    SKU_GRPCD_PART_QTY_Change(DomainName, id);
                });

                App.unblockUI();

                $("#" + SKU_GRPCD).change(function () {
                    App.blockUI({ boxed: true });
                    var StrURL = DomainName + "/Fleet/GetSparePart?id=" + $(this).val();
                    DropDown(DomainName, StrURL, PART_CODE);
                    App.unblockUI();
                });

                $("#ObjWFPJH_Asset_Type").change(function () {
                    App.blockUI({ boxed: true });
                    var StrURL = DomainName + "/Fleet/GetWork_Group?id=" + $(this).val();
                    DropDown(DomainName, StrURL, SKU_GRPCD);
                    App.unblockUI();
                });

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });
    }

    var SKU_GRPCD_PART_QTY_Change = function (DomainName, id) {

        Calculation(DomainName);

        var SKU_AvailableQTYURL = DomainName + '/Fleet/Check_SKU_AvailableQTY?Qty=' + $("#" + id + "PART_QTY").val() + '&SKU=' + $("#" + id + "SKU_GRPCD").val();

        if ($("#" + id + "SKU_GRPCD").val() != "") {

            App.blockUI({ boxed: true });
            $.ajax({
                url: SKU_AvailableQTYURL,
                cache: false,
                data: {},
                type: 'POST',
                success: function (data) {

                    if (data.Status == "N") {

                        $("#" + id + "PART_QTY").val("0");
                        alert("Please Enter Qty Less Than " + parseFloat(data.AvailableQTY).toFixed(2))
                        App.unblockUI();
                    }
                    else {
                        App.unblockUI();
                    }
                },
                error: function (req, status, error) {
                    $("#" + id + "PART_QTY").val("0");
                    alert("Please Enter Qty Less Than " + parseInt(data.Available_QTY))
                    App.unblockUI();
                }
            });
        }

    }

    var CommonFocusOut = function (DomainName, ID) {

        $("#" + ID).focusout(function () {
            Calculation(DomainName);
        });
    }

    var DropDown = function (DomainName, StrURL, DDLId) {
        $("#" + DDLId).select2('data', null)
        FillDropDownfromOtherSelectFirst(DDLId, StrURL, DDLId);
    }

    var select2Date = function (DomainName) {
        
        if (jQuery().datepicker) {

            $('.date-picker').datepicker({
                rtl: App.isRTL(),
                format: "dd M yyyy",
                autoclose: true,
                //startDate: new Date(),
                //endDate: new Date(),
            });

            $('.date-picker-RT').datepicker({
                rtl: App.isRTL(),
                format: "dd M yyyy",
                autoclose: true,
                startDate: new Date($("#ObjWFPJH_JOB_ORDER_DT").val()),
                endDate: new Date($("#FinYearEndDate").val()),
            });

            $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }
    }

    var select2ID = function (DomainName, ID) {

        $('select').select2({
            placeholder: "Select an option",
            allowClear: true
        });
    }

    var PageLoadEvents = function (DomainName) {

        $("#ObjWFPJH_Asset_Type").change(function () {

            var Asset_Type_Val = $(this).val();
            Asset_Type_Change(DomainName, Asset_Type_Val);
        });

        $("#ObjWFPJH_ORDER_TYPE").change(function () {

            var ORDER_TYPE_Val = $(this).val();
            ORDER_TYPE_Change(DomainName, ORDER_TYPE_Val);

        });

        $("#ObjWFPJH_SERVICE_CENTER_TYPE").change(function () {

            var SERVICE_CENTER_TYPE = $(this).val();
            SERVICE_CENTER_TYPE_Change(DomainName, SERVICE_CENTER_TYPE);
        });

        CommonFocusOut(DomainName, "ObjWFPJH_Estimated_Labour_Hrs");

        $("#ObjWFPJH_VEHNO").change(function () {

            var VehNo = $(this).val();
            VehicleChange(DomainName, VehNo);
        });
    }

    var SERVICE_CENTER_TYPE_Change = function (DomainName, SERVICE_CENTER_TYPE) {

        if (SERVICE_CENTER_TYPE == "Workshop") {
            $(".clsWS_LOCCODE").show(500);
            $(".clsVENDOR_CODE").hide(500);
        }
        else if (SERVICE_CENTER_TYPE == "Vendor") {
            $(".clsWS_LOCCODE").hide(500);
            $(".clsVENDOR_CODE").show(500);
        }
    }

    var ORDER_TYPE_Change = function (DomainName, ORDER_TYPE_Val) {

        if (ORDER_TYPE_Val == "") {
            $(".clsJobCardType").hide(500);
        }
        else {
            $(".clsJobCardType").show(500);
        }
    }

    var VehicleChange = function (DomainName, VehNo) {
        var StrURL1 = DomainName + '/Fleet/Check_Vehicle_NO?VehicleNo=' + VehNo;
        App.blockUI({ boxed: true });
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (data) {

                //if (data.IsRecordFound == "0") {
                //    $("#ObjWFPJH_Vehicle_Category").val("");
                //    $("#ObjWFPJH_Manufacturer").val("");
                //    $("#ObjWFPJH_Model").val("");
                //    $("#ObjWFPJH_Vehicle_Type_Category").val("");
                //    $("#ObjWFPJH_ORDER_STATUS").val("");
                //    $("#ObjWFPJH_Current_KM_Reading").val("");
                //    $("#ObjWFPJH_VEHNO").val("");
                //    alert("Please Enter Valid Vehicle No.")
                //    App.unblockUI();
                //}
                //else if (data.IsRecordFound == "1") {
                $("#ObjWFPJH_Vehicle_Category").val(data.Type_Name);
                $("#ObjWFPJH_Manufacturer").val(data.Made_by);
                $("#ObjWFPJH_Model").val(data.Model_No);
                $("#ObjWFPJH_Vehicle_Type_Category").val(data.Vehicle_Type);
                $("#ObjWFPJH_ORDER_STATUS").val("Open");
                $("#ObjWFPJH_Current_KM_Reading").val(data.Current_KM_Read);
                App.unblockUI();
                //}
            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
                $("#ObjWFPJH_Vehicle_Category").val("");
                $("#ObjWFPJH_Manufacturer").val("");
                $("#ObjWFPJH_Model").val("");
                $("#ObjWFPJH_Vehicle_Type_Category").val("");
                $("#ObjWFPJH_ORDER_STATUS").val("");
                $("#ObjWFPJH_Current_KM_Reading").val("");
                App.unblockUI();
            }
        });
    }

    var Asset_Type_Change = function (DomainName, Asset_Type_Val) {

        if (Asset_Type_Val == "01") {
            $(".clsAssetRefer").hide(500);
            $(".clsAssetContainer").hide(500);
            $(".clsVehicleDetails").show(500);
            if ($("#ISClose").val() == "1") {
                VehicleChange(DomainName, $("#ObjWFPJH_VEHNO").val());
            }
        }
        else if (Asset_Type_Val == "02") {
            $(".clsVehicleDetails").hide(500);
            $(".clsAssetContainer").hide(500);
            $(".clsAssetRefer").show(500);
        }
        else if (Asset_Type_Val == "03") {
            $(".clsVehicleDetails").hide(500);
            $(".clsAssetRefer").hide(500);
            $(".clsAssetContainer").show(500);
        }
    }

    var Calculation = function (DomainName) {

        var Task_EST_LABOUR_HRS = 0;
        $(".clsTask_EST_LABOUR_HRS").each(function () {
            var IDS = $(this).attr("id").replace("W_EST_LABOUR_HRS", "")
            Task_EST_LABOUR_HRS = Task_EST_LABOUR_HRS + parseFloat($(this).val());
            var Estimated_Labour_Cost = parseFloat($(this).val()).toFixed(2) * $("#ObjWFPJH_Estimated_Labour_Hrs").val();
            $("#" + IDS + "W_EST_LABOUR_COST").val(parseFloat(Estimated_Labour_Cost).toFixed(2));
        });
        $("#ObjWFPJH_TOT_EST_LABOUR_HRS_Total").val(Task_EST_LABOUR_HRS);

        $(".clsTask_EST_LABOUR_HRS").focusout(function () {
            var IDS = $(this).attr("id").replace("W_EST_LABOUR_HRS", "")
            Task_EST_LABOUR_HRS = Task_EST_LABOUR_HRS + parseFloat($(this).val());
            var Estimated_Labour_Cost = parseFloat($(this).val()).toFixed(2) * $("#ObjWFPJH_Estimated_Labour_Hrs").val();
            $("#" + IDS + "W_EST_LABOUR_COST").val(parseFloat(Estimated_Labour_Cost).toFixed(2));
        });
        $("#ObjWFPJH_TOT_EST_LABOUR_HRS_IsClose_Total").val(Task_EST_LABOUR_HRS);
        $("#ObjWFPJH_TOT_EST_LABOUR_HRS").val(Task_EST_LABOUR_HRS);
        $("#TOT_EST_LABOUR_HRS_Total").val(Task_EST_LABOUR_HRS);

        var Task_EST_LABOUR_COST = 0;
        $(".clsTask_EST_LABOUR_COST").each(function () {
            Task_EST_LABOUR_COST = parseInt(Task_EST_LABOUR_COST) + parseInt($(this).val());
        });
        $(".clsTask_EST_LABOUR_COST").focusout(function () {
            Task_EST_LABOUR_COST = parseInt(Task_EST_LABOUR_COST) + parseInt($(this).val());
        });
        $("#ObjWFPJH_TOT_EST_LABOUR_COST_IsClose_Total").val(parseFloat(Task_EST_LABOUR_COST).toFixed(2));
        $("#ObjWFPJH_TOT_EST_LABOUR_COST").val(parseFloat(Task_EST_LABOUR_COST).toFixed(2));
        $("#TOT_EST_LABOUR_COST_Total").val(parseFloat(Task_EST_LABOUR_COST).toFixed(2));



        var PART_QTY = 0;
        $(".clsPART_QTY").each(function () {
            var IDS = $(this).attr("id").replace("PART_QTY", "")
            PART_QTY = parseInt(PART_QTY) + parseInt($(this).val());
            var COST_UNIT = "0";
            COST_UNIT = parseInt($(this).val()) * parseInt($("#" + IDS + "COST_UNIT").val());
            $("#" + IDS + "COST").val(COST_UNIT);
        });
        $("#ObjWFPJH_Total_Estimated_Part_Quantity").val(PART_QTY);
        $(".clsPART_QTY").focusout(function () {
            var IDS = $(this).attr("id").replace("PART_QTY", "")
            PART_QTY = parseInt(PART_QTY) + parseInt($(this).val());
            var COST_UNIT = "0";
            COST_UNIT = parseInt($(this).val()) * parseInt($("#" + IDS + "COST_UNIT").val());
            $("#" + IDS + "COST").val(COST_UNIT);
        });
        $("#ObjWFPJH_Total_Estimated_Part_Quantity").val(PART_QTY);

        var PART_Cost_Unit = 0;
        $(".clsCOST_UNIT").focusout(function () {
            var IDS = $(this).attr("id").replace("COST_UNIT", "")
            PART_Cost_Unit = parseInt(PART_Cost_Unit) + parseInt($(this).val());
            var COST_UNIT = "0";
            COST_UNIT = parseFloat($(this).val()).toFixed(2) * parseFloat($("#" + IDS + "PART_QTY").val()).toFixed(2);
            $("#" + IDS + "COST").val(COST_UNIT);
        });
        $("#ObjWFPJH_Total_Estimated_Part_Unit").val(PART_Cost_Unit);
        $(".clsCOST_UNIT").each(function () {
            var IDS = $(this).attr("id").replace("COST_UNIT", "")
            PART_Cost_Unit = parseInt(PART_Cost_Unit) + parseInt($(this).val());
            var COST_UNIT = "0";
            COST_UNIT = parseFloat($(this).val()).toFixed(2) * parseFloat($("#" + IDS + "PART_QTY").val()).toFixed(2);
            $("#" + IDS + "COST").val(COST_UNIT);
        });
        $("#ObjWFPJH_Total_Estimated_Part_Unit").val(PART_Cost_Unit);


        var COST = 0;
        $(".clsCOST").each(function () {
            COST = parseInt(COST) + parseInt($(this).val());
        });
        $(".clsIsCloseDisables").focusout(function () {
            $("#ObjWFPJH_TOT_ESTIMATED_COST").val(parseInt($("#TOT_EST_LABOUR_COST_Total").val()) + parseInt($("#ObjWFPJH_TOT_EST_PART_COST").val()));
        });

        $(".clsCOST").focusout(function () {
            COST = parseInt(COST) + parseInt($(this).val());
        });
        $("#ObjWFPJH_Total_Estimated_Part_Cost").val(COST);
        $("#TOT_SPARE_COST").html(COST);

        if ($("#ISClose").val() == "1") {
            $("#ObjWFPJH_TOT_ESTIMATED_COST").val(parseInt($("#ObjWFPJH_TOT_EST_LABOUR_COST_IsClose_Total").val()) + parseInt($("#ObjWFPJH_Total_Estimated_Part_Cost").val()));
        }
        else {
            $("#ObjWFPJH_TOT_ESTIMATED_COST").val(parseInt($("#TOT_EST_LABOUR_COST_Total").val()) + parseInt($("#ObjWFPJH_TOT_EST_PART_COST").val()));
        }

        if ($("#ISClose").val() == "0") {
            $("#ObjWFPJH_TOT_EST_PART_COST").val(COST);
        }

        var W_ACT_LABOUR_COST = 0;
        $(".clsW_ACT_LABOUR_COST").each(function () {
            W_ACT_LABOUR_COST = parseFloat(W_ACT_LABOUR_COST) + parseFloat($(this).val());
            $("#ObjWFPJH_Total_Actual_Labour_Cost").val(W_ACT_LABOUR_COST);
        });
        $("#ObjWFPJH_TOT_ACT_LABOUR_COST").val(W_ACT_LABOUR_COST);
        $(".clsW_ACT_LABOUR_COST").focusout(function () {
            W_ACT_LABOUR_COST = parseFloat(W_ACT_LABOUR_COST) + parseFloat($(this).val());
            $("#ObjWFPJH_Total_Actual_Labour_Cost").val(W_ACT_LABOUR_COST);
        });

        console.log("W_ACT_LABOUR_COST1=" + W_ACT_LABOUR_COST);
        W_ACT_LABOUR_COST = parseFloat(W_ACT_LABOUR_COST).toFixed(0);
        console.log("W_ACT_LABOUR_COST2=" + W_ACT_LABOUR_COST);

        $("#ObjWFPJH_TOT_ACT_LABOUR_COST").val(W_ACT_LABOUR_COST);
        $("#TOT_LABOUR_COST2").html(parseInt(W_ACT_LABOUR_COST));
        $("#TOT_LABOUR_COST1").html(parseInt(W_ACT_LABOUR_COST));


        var ACT_PART_QTY = 0;
        $(".clsACT_PART_QTY").each(function () {
            ACT_PART_QTY = parseInt(ACT_PART_QTY) + parseInt($(this).val());
            var IDS = $(this).attr("id").replace("ACT_PART_QTY", "")

            var Actual_Cost_Unit = parseInt($("#" + IDS + "ACT_COST_UNIT").val());

            $("#ObjWFPJH_Total_Actual_Part_Quantity").val(ACT_PART_QTY);
            $("#" + IDS + "ACT_COST").val(parseInt($(this).val()) * parseInt(Actual_Cost_Unit));
        });
        $("#ObjWFPJH_Total_Actual_Part_Quantity").val(ACT_PART_QTY);
        $(".clsACT_PART_QTY").focusout(function () {
            ACT_PART_QTY = parseInt(ACT_PART_QTY) + parseInt($(this).val());
            var IDS = $(this).attr("id").replace("ACT_PART_QTY", "")

            var Actual_Cost_Unit = parseInt($("#" + IDS + "ACT_COST_UNIT").val());

            $("#ObjWFPJH_Total_Actual_Part_Quantity").val(ACT_PART_QTY);
            $("#" + IDS + "ACT_COST").val(parseInt($(this).val()) * parseInt(Actual_Cost_Unit));
        });
        $("#ObjWFPJH_Total_Actual_Part_Quantity").val(ACT_PART_QTY);


        var ACT_COST_UNIT = 0;
        $(".clsACT_COST_UNIT").each(function () {
            ACT_COST_UNIT = parseInt(ACT_COST_UNIT) + parseInt($(this).val());
            var IDS = $(this).attr("id").replace("ACT_COST_UNIT", "")

            var ACT_PART_QTY = $("#" + IDS + "ACT_PART_QTY").val();

            $("#ObjWFPJH_Total_Actual_Part_Unit").val(ACT_COST_UNIT);
            $("#" + IDS + "ACT_COST").val(parseFloat(parseFloat($(this).val()) * parseFloat(ACT_PART_QTY)).toFixed(2));
        });
        $("#ObjWFPJH_Total_Actual_Part_Unit").val(ACT_COST_UNIT);
        $(".clsACT_COST_UNIT").focusout(function () {
            ACT_COST_UNIT = parseInt(ACT_COST_UNIT) + parseInt($(this).val());
            var IDS = $(this).attr("id").replace("ACT_COST_UNIT", "")

            var ACT_PART_QTY = $("#" + IDS + "ACT_PART_QTY").val();

            $("#ObjWFPJH_Total_Actual_Part_Unit").val(ACT_COST_UNIT);
            $("#" + IDS + "ACT_COST").val(parseFloat(parseFloat($(this).val()) * parseFloat(ACT_PART_QTY)).toFixed(2));
        });
        $("#ObjWFPJH_Total_Actual_Part_Unit").val(ACT_COST_UNIT);

       

        var ACT_COST = 0;
        $(".clsACT_COST").each(function () {
            ACT_COST = parseFloat(ACT_COST) + parseFloat($(this).val());
            $("#ObjWFPJH_Total_Actual_Part_Cost").val(parseFloat(ACT_COST).toFixed(2));
        });
        $("#ObjWFPJH_Total_Actual_Part_Cost").val(parseFloat(ACT_COST).toFixed(2));

        //$(".clsACT_COST").focusout(function () {
        //    ACT_COST = parseFloat(ACT_COST) + parseInt($(this).val());
        //    $("#ObjWFPJH_Total_Actual_Part_Cost").val(parseFloat(ACT_COST).toFixed(2));
        //});
        ACT_COST = parseFloat(ACT_COST).toFixed(0);

        $("#ObjWFPJH_Total_Actual_Part_Cost").val(parseFloat(ACT_COST).toFixed(0));
        $("#TOT_SPARE_COST2").html("0");
        $("#TOT_SPARE_COST1").html(ACT_COST);

        $("#ObjWFPJH_TOT_ACT_PART_COST").val(parseFloat(parseFloat($("#ObjWFPJH_TOT_ACT_LABOUR_COST").val()) + parseFloat($("#ObjWFPJH_Total_Actual_Part_Cost").val())).toFixed(2));
    }

    var duplicateDDLCheckSundry = function (parIdName) {
        var GRPCD_ID = parIdName;
        var GRPCD_Value = $("#" + parIdName).val();
        $('.clsChkDuplicate').each(function () {
            var GRPCD_Other_Id = $(this).attr('id');
            var GRPCD_Other_Value = $(this).val();

            if (GRPCD_ID != GRPCD_Other_Id && GRPCD_Value == GRPCD_Other_Value) {
                $('#' + GRPCD_ID).val('');
                $('#' + GRPCD_ID).select2();
                TosterNotification("error", "Message !! Oppssss cannot select Duplicate Value........", "Message");
                return false;
            }

        });
    }

    var SparePartDubplicate = function (objID) {
        debugger
        var SKU_GRPCD_ID = objID;
        var SKU_GRPCD_Value = $("#" + objID).val();
        $(".CheckDuplicateValue").each(function () {
            var SKU_GRPCD_Other_Id = $(this).attr('id');
            var SKU_GRPCD_Other_Value = $(this).val();

            if (SKU_GRPCD_ID != SKU_GRPCD_Other_Id && SKU_GRPCD_Value == SKU_GRPCD_Other_Value) {
                $('#' + SKU_GRPCD_ID).val('');
                $('#' + SKU_GRPCD_ID).select2();
                TosterNotification("error", "Message !! Oppssss cannot select Duplicate Value........", "Message");
                return false;
            }
        });

    }

    return {
        //main function to initiate the module
        init: function (DomainName) {
            if ($("#ISClose").val() != "1") {
                ControlDate();
            }
            var IsClose = $("#ISClose").val();

            $(".clsPageLoadHide").hide(500);
            $(".clsJobCardType").hide(500);
            $(".clsIsCloseShow").hide(500);

            if (IsClose == "1") {
                Asset_Type_Change(DomainName, $("#ObjWFPJH_Asset_Type").val());
                ORDER_TYPE_Change(DomainName, $("#ObjWFPJH_ORDER_TYPE").val());
                SERVICE_CENTER_TYPE_Change(DomainName, $("#ObjWFPJH_SERVICE_CENTER_TYPE").val());
                $(".clsSubmit").show(500);
                if ($("#ISClose").val() != "1") {
                    $("#ObjWFPJH_Actual_Date_of_return").val("");
                }
                $("#ObjWFPJH_Actual_Date_of_return").val("");
                $(".clsIsCloseHide").hide(500);
                $(".clsIsCloseShow").show(500);

                $(".clsIsCloseDisables :input").attr("readonly", true);
                $(".clsIsCloseDisables").attr("readonly", true);
            }

            /*Start Job Ordre Advance Changes*/

            $("#PaymentMode option[value='Both']").remove();
            $("#PayAmount").attr("readonly", false);
            $("#PayAmount").attr("number", true);
            $("#PaymentMode").change(function () {

                App.blockUI({ boxed: true });

                $("#CashAmount").attr("readonly", "true");
                $("#ChequeAmount").attr("readonly", "true");

                $("#CashLedger").change(function () {
                    $("#CashLedgerName").val($(this).select2('data').text);
                });

                $("#BankLedger").change(function () {
                    $("#BankLedgerName").val($(this).select2('data').text);
                });


                if ($(this).val().toUpperCase() == "CASH") {
                    $(".clsBANK").hide(500);
                    $(".clsCASH").show(500);

                    $("#CashAmount").val($("#PayAmount").val());

                    var StrURL = DomainName + "/Payment/GetLedger?Category=Cash";
                    FillDropDownfromOther("CashLedger", StrURL, "Cash Ledger");

                    $("#ChequeNo").val("000000");
                    $("#BankLedger").select2("data", { id: $(this).val(), text: $(this).select2('data').text });

                }
                else if ($(this).val().toUpperCase() == "BANK") {
                    $(".clsBANK").show(500);
                    $(".clsCASH").hide(500);
                    $("#ChequeNo").val("");
                    var StrURL = DomainName + "/Payment/GetLedger?Category=Bank";
                    FillDropDownfromOther("BankLedger", StrURL, "Bank Ledger");

                    $("#ChequeAmount").val($("#PayAmount").val());

                    $("#CashLedger").select2("data", { id: $(this).val(), text: $(this).select2('data').text });
                }
                else if ($(this).val().toUpperCase() == "BOTH") {

                    $(".clsBANK").show(500);
                    $(".clsCASH").show(500);


                    $("#CashAmount").val("0");
                    $("#ChequeAmount").val("0");

                    $("#CashAmount").attr("readonly", false);
                    $("#ChequeAmount").attr("readonly", false);

                    var StrURL = DomainName + "/Payment/GetLedger?Category=Cash";
                    FillDropDownfromOther("CashLedger", StrURL, "Cash Ledger");

                    StrURL = DomainName + "/Payment/GetLedger?Category=Bank";
                    FillDropDownfromOther("BankLedger", StrURL, "Bank Ledger");
                }
            });

            $("#PayAmount").focusout(function () {
                if ($("#PaymentMode").val().toUpperCase() == "CASH") {
                    $("#CashAmount").val($(this).val());
                    $("#CashAmount").attr("min", 1);
                    $("#ChequeAmount").val("0");
                    $("#ChequeAmount").attr("min", 0);
                }
                else if ($("#PaymentMode").val().toUpperCase() == "BANK") {
                    $("#ChequeAmount").val($(this).val());
                    $("#ChequeAmount").attr("min", 1);
                    $("#CashAmount").val("0");
                    $("#CashAmount").attr("min", 0);
                }
            });

            $("#IsAdvancePaid").attr("value", "false");

            $("#Payment1 :input").attr("disabled", true);
            $("#Payment2 :input").attr("disabled", true);
            $("#Payment1").hide(500);
            $("#Payment2").hide(500);

            $("#IsAdvancePaid").click(function () {
                var checked = jQuery(this).is(":checked");
                $("#IsAdvancePaid").val(checked);

                if (checked) {

                    $("#Payment1").show(500);
                    $("#Payment2").show(500);
                    $("#Payment1 :input").attr("disabled", false);
                    $("#Payment2 :input").attr("disabled", false);

                    $("#Staff_Vendor").attr("readonly", false);
                    $("#Vendor_Employee").attr("readonly", false);
                }
                else {
                    $("#Payment1 :input").attr("disabled", true);
                    $("#Payment2 :input").attr("disabled", true);
                    $("#Payment1").hide(500);
                    $("#Payment2").hide(500);

                    $("#Staff_Vendor").val("Select");
                    $("#Staff_Vendor").select2().find(":selected").data("Select");
                    $("#Staff_Vendor").attr("readonly", true);
                    $("#Vendor_Employee").attr("readonly", true);
                }
            });

            $("#Staff_Vendor").change(function () {
                if ($("#Staff_Vendor").val() == "VENDOR") {
                    $("#Vendor_Employee").val("");
                    $("#Vendor_Employee").attr("placeholder", "Select For a Vendor");
                    GetVehicleEmployeeUserName("Vendor_Employee", "V")
                }
                else if ($("#Staff_Vendor").val() == "STAFF") {
                    $("#Vendor_Employee").val("");
                    $("#Vendor_Employee").attr("placeholder", "Select For a Staff");
                    GetVehicleEmployeeUserName("Vendor_Employee", "U")
                }
            });
            /*End Job Ordre Advance Changes*/
            select2Date(DomainName);
            Formvalidate(DomainName);
            SubmitClick(DomainName);
            PageLoadEvents(DomainName);
            $('.clsChkDuplicate').live('change', function () {
                duplicateDDLCheckSundry($(this).attr('id'));
            });

            $('.CheckDuplicateValue').live('change', function () {
                SparePartDubplicate($(this).attr('id'));
            });
         

            $(".clsTask_EST_LABOUR_HRS").each(function () {
                Calculation(DomainName);
            });
            $(".clsTask_EST_LABOUR_HRS").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsTask_EST_LABOUR_COST").each(function () {
                Calculation(DomainName);
            });
            $(".clsTask_EST_LABOUR_COST").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsPART_QTY").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsPART_QTY").each(function () {
                Calculation(DomainName);
            });
            $(".clsCOST_UNIT").each(function () {
                Calculation(DomainName);
            });
            $(".clsCOST_UNIT").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsCOST").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsCOST").each(function () {
                Calculation(DomainName);
            });
            $(".clsW_ACT_LABOUR_COST").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsW_ACT_LABOUR_COST").each(function () {
                Calculation(DomainName);
            });
            $(".clsACT_PART_QTY").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsACT_PART_QTY").each(function () {
                Calculation(DomainName);
            });
            $(".clsACT_PART_QTY").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsACT_PART_QTY").each(function () {
                Calculation(DomainName);
            });
            $(".clsACT_COST_UNIT").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsACT_COST_UNIT").each(function () {
                Calculation(DomainName);
            });
            $(".clsACT_COST").focusout(function () {
                Calculation(DomainName);
            });
            $(".clsACT_COST").each(function () {
                Calculation(DomainName);
            });

            Calculation(DomainName);
         
            //$(".clsAMC").change(function () {

            //    var id = $(this).attr("id").replace("AMC", "");
            //    //alert($(this).attr("id") + "--" + $(this).val());
            //    if ($(this).val() == "Yes") {
            //        $("#" + id + "W_ACT_LABOUR_COST").val("0");
            //        $("#" + id + "W_ACT_LABOUR_COST").attr("readonly", "true");
            //    }
            //    else if ($(this).val() == "No") {
            //        $("#" + id + "W_ACT_LABOUR_COST").attr("readonly", "false");
            //    }
            //});

            var itemIndex = parseInt($("#container input.iHidden").length) + parseInt($("#Task_Details_Count").val());
            AddTaskDetailsRows(DomainName, itemIndex);

            if (IsClose == "1") {

                var CommonIDs = "TaskDetailsList_";
                for (var ij = 1; ij <= parseInt($("#Task_Details_Count").val()) ; ij++) {

                    $("#" + CommonIDs + ij + "__W_ACT_LABOUR_COST").focusout(function () {
                        Calculation(DomainName);
                    });

                }
            }

            var itemIndex_Spare_Part = parseInt($("#container_Spare_Part input.iHidden").length) + parseInt($("#Task_Spare_Part_Count").val());
            AddTask_Spare_PartRows(DomainName, itemIndex_Spare_Part);

            var Vehicle_StrURL = DomainName + "/Fleet/Get_Vehicles";
            JsonDDL_Disable("ObjWFPJH_VEHNO", Vehicle_StrURL);

            //var Vehicle_StrURL = DomainName + "/Fleet/Get_Vehicles?searchTerm=";
            //FillDropDownfromOtherDisabled("ObjWFPJH_VEHNO", Vehicle_StrURL, "Vehicle No.");
        }
    }
}();