﻿var FormComponents = function(DomainName) {
    var Formvalidate = function(DomainName) {
        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        $.validator.addMethod("CheckDuplicateRecords", function(value, element) {
            var CurrId = $(element).attr("id");
            var ID = $(element).attr("id").replace("LedgerList_", "").replace("__DOCKNO", "");
            if ($("#BILLMST_PTMSCD").val() && $("#" + CurrId).val()) {
                var CurrVal = value;
                // alert(CurrId + "  " + CurrVal);
                var CurrLedgerId = $(element).attr("id").replace("DOCKNO", "Acccode");
                var CurrLedgerVal = $("#" + $(element).attr("id").replace("DOCKNO", "Acccode")).val();
                var DUPlicateFlg = "N";
                $(".clsDocument").each(function() {
                    var CurrLedgerId1 = $(this).attr("id").replace("DOCKNO", "Acccode");
                    var CurrLedgerVal1 = $("#" + $(this).attr("id").replace("DOCKNO", "Acccode")).val();
                    if (CurrId != $(this).attr("id") && CurrVal == $(this).val() && CurrLedgerVal1 == CurrLedgerVal) {
                        $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val("Duplicate Records Found !!!!!!!!");
                        DUPlicateFlg = "Y";
                        return false;
                    }
                });
                if (DUPlicateFlg == "N") {
                   
                    var ddlId = $(element).attr("id").replace("DOCKNO", "DocType");

                    var doctypeval= $('#' + ddlId).val();  
                    //alert("doctypeval "+doctypeval)     
                    var StrURL = DomainName + "/Finance/CheckDocumentNoForSuppBill?dockno=" + $(element).val().trim() + "&docType=" + doctypeval;
                    // alert("hello " + StrURL);
                    var result1 = $.ajax({
                        async: false,
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: StrURL,
                        dataType: 'json',
                        data: {},
                    });
                    var d1 = JSON.parse(result1.responseText);

                    if (d1.isValid=="False") {
                        $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val("Invalid Document");
                        return false;
                    } else {
                        return true;
                    }
                    return false;
                }
            } else {
                if ($("#BILLMST_PTMSCD").val() == "") {
                    $("#" + CurrId).val("");
                    alert("Please select Party.");
                }
            }
        }, function(value, element) {
            //  alert("asdads " + $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val());
            return $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val();
        });

        $.validator.addMethod("DocumentDateRule", function(value) {
            var StartDate = new Date($("#StartDate").val());
            var EntDate = new Date($("#EntDate").val());
            var DocDate = new Date(value);
            // alert(DocDate + "  " + StartDate + "  " + EntDate)
            if (DocDate >= StartDate && DocDate <= EntDate)
                return true;
            else
                return false;
        }, "Please Select Date ");

        $.validator.addMethod("DueDateRule", function(value) {
            var EntDate = new Date($("#BILLMST_BGNDT").val());
            var DocDate = new Date(value);
            //  alert(DocDate + "  "  + EntDate)
            if (DocDate >= EntDate)
                return true;
            else
                return false;

        }, "Please Select Date ");

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: true,
                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: true,
                },
                //'BILLMST.manualbillno': {
                //    required: function () {
                //        if ($("#ManualBillnoMandatory").val() == "Y") {
                //            return true;
                //        }
                //        else
                //            return false;
                //    }
                //},
            },
            messages: {
                'CUSTHDR.OutStdAmt': {
                    CheckOUTSTDAmount: jQuery.format("OutStanding Amount is Greater Then Credit Limit Of The same Customer. !!")
                },
                'BILLMST.BILLAMT': {
                    min: jQuery.format("Please Select at Least One Record"),
                    CheckBillAmount: jQuery.format("Bill Amount Should be Less then Or Equals to Credit Limit For The Same Customer.")
                },
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: jQuery.format("Please Select Date between " + $("#sStartDate").val() + " and " + $("#sEntDate").val()),
                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: jQuery.format("Please Select Date Greter or equal to Bill Date"),
                },
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function(DomainName) {
        $('#btnSubmit').click(function() {
            //alert($("#BILLMST_KKCAMT").val() + "  " + $("#BILLMST_SBCAMT").val());
            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }
            //   alert("Done");
            App.blockUI({
                boxed: true
            });
            document.forms["form_sample"].submit();
            //  form.submit();
            //  alert("Done...11");
            //if ($("#CTH_THCType").val() == "1")
            //    $(".clsMF").show();
            //else
            //    $(".clsGC").show();
            //$(this).attr("dissbled", true);
            //$('#divStep1 :button').attr('disabled', true);
            //$('#divStep1 :input').attr('disabled', true);
        });
    }

    var AddLedgerRows = function(DomainName, itemIndex) {
        $("#btnAddRows").click(function() {
            //    alert("Hello");
            var TOTRows = $("#LedgerRows").val();
            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function() {
                    itemIndex = itemIndex + 1;
                    AddLedgerRow(DomainName, itemIndex);
                }, 250 * i);
            }
        });
        LedgerDetails(1, DomainName);
    }

    var AddLedgerRow = function(DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Finance/LedgerDetails/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function(html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer").append(html);
                LedgerDetails(Rid, DomainName);
            },
            error: function(req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });
    }

    var LedgerDetails = function(Rid, DomainName) {
        $(".clsDocument").each(function() {
            $(this).rules("add", {
                CheckDuplicateRecords: true,
            });
        });

        var IsMultipleDoc = $('#IsMultipleDoc').is(':checked');

        if (IsMultipleDoc)
            $(".clsDocType").show();
        else
            $(".clsDocType").hide();

        var StrURL = DomainName + "/Payment/GetLedgerBySearchForSupplementaryBill";
        JsonDDLSelectCode("LedgerList_" + Rid + "__Acccode", StrURL);

        //if (Rid != "1") {
        $("#LedgerList_" + Rid + "__DocType").select2({
            placeholder: "Select an option",
            allowClear: true
        });
        //}

        $("#LedgerList_" + Rid + "__Acccode").change(function() {
            $("#LedgerList_" + Rid + "__Accdesc").val($(this).select2('data').text);
        });
    }

    var GetServiceTax = function(DomainName, DOCDT) {
        App.blockUI({
            boxed: true
        });
        var StrURL1 = DomainName + "/Finance/GetServiceTax?DOCKDT=" + DOCDT;
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL1,
            dataType: 'json',
            data: {},
            success: function(data1) {
                //  alert(data1.CONTRACTID + "  " + data1.TransMode + "  " + data1.ServiceType);
                $("#BILLMST_svctax_rate").val(data1.ServiceTaxRate);
                $("#BILLMST_cess_rate").val(data1.CessRate);
                $("#BILLMST_H_cess_rate").val(data1.HCessRate);
                $("#BILLMST_SBCRATE").val(data1.SBRate);
                $("#BILLMST_KKCRATE").val(data1.KKCRate);
                $("#StaxRebate").val(data1.StaxRebateRate);
                App.unblockUI();
            },
            error: function(req, status, error) {
                alert("error--" + req + "---" + status + "---" + error);
            }
        });
    }

    var CalculateStaxTDS1 = function(DomainName, DOCDT) {
        var IsApplyStax = $('#IsApplyStax').is(':checked');
        var IsAbatement = $('#IsAbatement').is(':checked');
        var IsRoundOff = $('#IsRoundOff').is(':checked');
        var SUBTOTEL = $("#BILLMST_SUBTOTEL").val();
        var SVRCAMT = $("#BILLMST_SVRCAMT").val();
        var StaxRebate = $("#StaxRebate").val();
        var cess_rate = $("#BILLMST_cess_rate").val();
        var svctax_rate = $("#BILLMST_svctax_rate").val();
        var H_cess_rate = $("#BILLMST_H_cess_rate").val();
        var SBCRATE = $("#BILLMST_SBCRATE").val();
        var cess_rate = $("#BILLMST_cess_rate").val();
        var KKCRATE = $("#BILLMST_KKCRATE").val();

        var Subtotal = 0;
        var STAX = 0;
        var CESS = 0;
        var HCESS = 0;
        var SB = 0;
        var PendAmt = 0;
        var TOTBillAmt = 0;
        var RoundOff = 0;
        var KKC = 0;

        if (IsApplyStax) {
            if (IsAbatement) {
                Subtotal = SUBTOTEL * StaxRebate;
                STAX = Subtotal * svctax_rate / 100;
                CESS = STAX * cess_rate / 100;
                HCESS = STAX * H_cess_rate / 100;
                SB = Subtotal * SBCRATE / 100;
                KKC = Subtotal * KKCRATE / 100;
            } else {
                Subtotal = SUBTOTEL;
                STAX = Subtotal * svctax_rate / 100;
                CESS = STAX * cess_rate / 100;
                HCESS = STAX * H_cess_rate / 100;
                SB = Subtotal * SBCRATE / 100;
                KKC = Subtotal * KKCRATE / 100;

            }
            $("#BILLMST_SVRCAMT").val(parseFloat(STAX).toFixed(2));
            $("#BILLMST_CESSAMT").val(parseFloat(CESS).toFixed(2));
            $("#BILLMST_hedu_cess").val(parseFloat(HCESS).toFixed(2));
            $("#BILLMST_SBCAMT").val(parseFloat(SB).toFixed(2));
            $("#BILLMST_KKCAMT").val(parseFloat(KKC).toFixed(2));
        } else {
            Subtotal = SUBTOTEL;
            $("#BILLMST_SVRCAMT").val(0);
            $("#BILLMST_CESSAMT").val(0);
            $("#BILLMST_hedu_cess").val(0);
            $("#BILLMST_SBCAMT").val(0);
            $("#BILLMST_KKCAMT").val(0);
        }

        if (IsAbatement) {
            Subtotal = $("#BILLMST_SUBTOTEL").val();
        }

        PendAmt = parseFloat(Subtotal) + parseFloat(STAX) + parseFloat(CESS) + parseFloat(HCESS) + parseFloat(SB) + parseFloat(KKC);

        $("#BILLMST_PENDAMT").val(rounditn(PendAmt, 2));

        if (IsRoundOff) {
            //TOTBillAmt = rounditn(PendAmt, 0);
            //RoundOff = TOTBillAmt - PendAmt;
            TOTBillAmt = rounditn($("#GSTSubTotal").val(), 0);
            RoundOff = TOTBillAmt - parseFloat($("#GSTSubTotal").val());
        } else {
            //TOTBillAmt = PendAmt;
            TOTBillAmt = $("#GSTSubTotal").val();
            // alert(TOTBillAmt + "  " + PendAmt);
        }

        $("#BILLMST_RoundOffAmt").val(rounditn(RoundOff, 2));
        $("#BILLMST_BILLAMT").val(rounditn(TOTBillAmt, 2));
    }

    var dueDateTimePicker = function(strDate) {
        //if (jQuery().datepicker) {
        if ($('#BILLMST_BDUEDT').val() < strDate) {
            $('#BILLMST_BDUEDT').val(strDate);
        }
        $('.duedatetime-picker').datepicker("remove");
        $('.duedatetime-picker').datepicker({
            isRTL: App.isRTL(),
            format: "dd MM yyyy",
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            startDate: new Date(strDate),
            todayBtn: true
        });
        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        //}
    }

    return {
        //main function to initiate the module
        init: function(DomainName) {

            ControlDate();
            Formvalidate(DomainName);
            $("#DOCTY").val("DKT")
            $(".clsDateControl").on("changeDate", function(e) {
                //  alert($("#BILLMST_BGNDT").val());
                dueDateTimePicker($("#BILLMST_BGNDT").val());
                GetServiceTax(DomainName, $("#BILLMST_BGNDT").val());
            });

            GetServiceTax(DomainName, $("#BILLMST_BGNDT").val());

            var itemIndex = 1; // $("#container input.iHidden").length;
            AddLedgerRows(DomainName, itemIndex);

            GetLocationNameCode(DomainName, "BILLMST_BILLSUBBRCD");
            GetLocationNameCode(DomainName, "BILLMST_BILLCOLBRCD");

            $("#BILLMST_BILLSUBBRCD").select2("data", {
                id: $("#BILLMST_BILLSUBBRCD").val(),
                text: $("#BillSubLocNM").val()
            });
            $("#BILLMST_BILLCOLBRCD").select2("data", {
                id: $("#BILLMST_BILLCOLBRCD").val(),
                text: $("#BillCollLocNM").val()
            });

            SubmitClick(DomainName);

            GetPartyCode("BILLMST_PTMSCD", $("#BILLMST_BBRCD"), "");

            $("#BILLMST_PTMSCD").change(function() {
                $("#BILLMST_PTMSNM").val($(this).select2('data').text);
            });

            $("input[name='DocType']").click(function() {
                var DOCTY = $('input[name=DocType]:checked').val();
                var text = DOCTY == "DKT" ? 'CNote' : 'THC';
                $("#DOCTY").val(DOCTY);
                var IsMultipleDoc = $('#IsMultipleDoc').is(':checked');
                if (IsMultipleDoc) {
                    $(".clsdoctypeDDL").each(function() {
                        $("#" + $(this).attr("id")).select2("data", {
                            value: DOCTY,
                            text: text
                        });
                    });
                }
            });

            $('.clsLedgerAmount').live('change', function() {
                var TotalLedgerAmount = 0;
                $(".clsLedgerAmount").each(function() {
                    //  alert($(this).val());
                    TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                });
                $("#BILLMST_SUBTOTEL").val(rounditn(TotalLedgerAmount, 2));
                $("#DOCTOT").val(rounditn(TotalLedgerAmount, 2));
                $("#BILLMST_PENDAMT").val(rounditn(TotalLedgerAmount, 2));
                $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                //$("#BILLMST_BILLAMT").val($("#GSTSubTotal").val());
                $("#BILLMST_BILLAMT").val(TotalLedgerAmount);
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $(".clsDocType").hide();

            $('#IsApplyStax').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $('#IsAbatement').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $('#IsRoundOff').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });
             $("#BILLMST_Dockno").change(function() {
              
                if ($("#BILLMST_PTMSCD").val() == "") {
                     $("#BILLMST_Dockno").val("");
                    alert("Please select Party.");
                    return false;
                }
                var docNo=$("#BILLMST_Dockno").val();
                var DOCTY = $('input[name=DocType]:checked').val();
                var IsMultipleDoc = $('#IsMultipleDoc').is(':checked');
                if(docNo!="" && !IsMultipleDoc)
                {
                    var StrURL = DomainName + "/Finance/CheckDocumentNoForSuppBill?dockno=" +docNo.trim() + "&docType=" + DOCTY;
                    var result1 = $.ajax({
                        async: false,
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: StrURL,
                        dataType: 'json',
                        data: {},
                    });
                    var d1 = JSON.parse(result1.responseText);

                    if (d1.isValid=="False") {
                        alert("Invalid Document");
                        $("#BILLMST_Dockno").val("");
                        return false;
                    } else {
                        return true;
                    }
                    return false;
                }
            });
            $('#IsMultipleDoc').click(function() {
                var IsMultipleDoc = $('#IsMultipleDoc').is(':checked');
                if (IsMultipleDoc) {
                    $(".clsDocType").show();
                    $("#BILLMST_Dockno").removeClass("required")
                    $("#BILLMST_Dockno").val("");
                    var DOCTY = $('input[name=DocType]:checked').val();
                    var text = DOCTY == "DKT" ? 'CNote' : 'THC';
                    $(".clsdoctypeDDL").each(function() {
                        $("#" + $(this).attr("id")).select2("data", {
                            value: DOCTY,
                            text: text
                        });
                    });
                } else {
                     $(".clsDocument").each(function() {
                         $(this).val("");
                     });

                    $(".clsDocType").hide();
                    $("#BILLMST_Dockno").addClass("required")
                    
                }

            });
            //CalculateStaxTDS(DomainName);
        }
    }
}();