﻿var FormComponents = function(DomainName) {
    var Formvalidate = function(DomainName) {
        var form1 = $('#form_IncomeBill');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        $.validator.addMethod("DocumentDateRule", function(value) {
            var StartDate = new Date($("#StartDate").val());
            var EntDate = new Date($("#EntDate").val());
            var DocDate = new Date(value);
            // alert(DocDate + "  " + StartDate + "  " + EntDate)
            if (DocDate >= StartDate && DocDate <= EntDate)
                return true;
            else
                return false;
        }, "Please Select Date ");

        $.validator.addMethod("DueDateRule", function(value) {
            var EntDate = new Date($("#BILLMST_BGNDT").val());
            var DocDate = new Date(value);
            //  alert(DocDate + "  "  + EntDate)
            if (DocDate >= EntDate)
                return true;
            else
                return false;

        }, "Please Select Date ");

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: true,
                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: true,
                },
             },
            messages: {
                'CUSTHDR.OutStdAmt': {
                    CheckOUTSTDAmount: jQuery.format("OutStanding Amount is Greater Then Credit Limit Of The same Customer. !!")
                },
                'BILLMST.BILLAMT': {
                    min: jQuery.format("Please Select at Least One Record"),
                    CheckBillAmount: jQuery.format("Bill Amount Should be Less then Or Equals to Credit Limit For The Same Customer.")
                },
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: jQuery.format("Please Select Date between " + $("#sStartDate").val() + " and " + $("#sEntDate").val()),
                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: jQuery.format("Please Select Date Greter or equal to Bill Date"),
                },
            },
            errorPlacement: function(error, element) { // render error placement for each input type
                error.insertAfter(element); // for other inputs, just perform default behavior
            },
            invalidHandler: function(event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function(element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function(label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function(form) {
                success1.show();
                error1.hide();
            }
        });
    }

    var SubmitClick = function(DomainName) {
        $('#btnSubmit').click(function() {
            var form = $('#form_IncomeBill');
            if (form.valid() == false) {
                return false;
            }

            App.blockUI({
                boxed: true
            });
            document.forms["form_IncomeBill"].submit();
        });
    }

    var AddLedgerRows = function(DomainName, itemIndex) {
        $("#btnAddRows").click(function() {

            var TOTRows = $("#LedgerRows").val();
            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function() {
                    itemIndex = itemIndex + 1;
                    AddLedgerRow(DomainName, itemIndex);
                }, 250 * i);
            }
        });
        LedgerDetails(1, DomainName);
    }

    var AddLedgerRow = function(DomainName, itemIn) {
        var StrURL1 = DomainName + '/Finance/FleetIncomeDet/' + itemIn;
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function(html) {
                var Rid = itemIn;
                $("#Bodycontainer").append(html);
                LedgerDetails(Rid, DomainName);
            },
            error: function(req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });
    }

    var LedgerDetails = function(Rid, DomainName) {
            var date = new Date($("#BILLMST_BGNDT").val());

            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();

          var date1 = new Date($("#WMD_DOCKDT").val());
            date1.setDate(date.getDate() - 15);

            var day1 = date1.getDate();
            var month1 = date1.getMonth();
            var year1 = date1.getFullYear();


            $("#BillDetailList_" + Rid + "__THCDate").attr("readonly", "true")
            handleDateTimePicker_ddmmyyyy("BillDetailList_" + Rid + "__THCDate", new Date((day1 + ' ' + months[month1] + ' ' + year1)), new Date((day + ' ' + months[month] + ' ' + year)));
    }

    var CalculateStaxTDS1 = function(DomainName, DOCDT) {
        var IsRoundOff = $('#IsRoundOff').is(':checked');
        var SUBTOTEL = $("#DOCTOT").val();
        
        var Subtotal = 0;
        var PendAmt = 0;
        var TOTBillAmt = 0;
        var RoundOff = 0;
        
        Subtotal = SUBTOTEL;
                                          
        PendAmt = parseFloat(Subtotal);

        $("#BILLMST_PENDAMT").val(rounditn(PendAmt, 2));

        if (IsRoundOff) {
            TOTBillAmt = rounditn(SUBTOTEL, 0);
            RoundOff = TOTBillAmt - parseFloat(SUBTOTEL);
        } else {
            TOTBillAmt = SUBTOTEL;
        }

        $("#BILLMST_RoundOffAmt").val(rounditn(RoundOff, 2));
        $("#BILLMST_BILLAMT").val(rounditn(TOTBillAmt, 2));
    }
    
    var CheckDuplicateTrip = function(mid, DomainName) {
            var mVal=$("#"+mid).val();
            $(".clsTripsheet").each(function() {
                    var CurrId = $(this).attr("id");
                    var CurrVal = $(this).val();
                    if(mid!=CurrId && mVal==CurrVal)
                    {
                        alert("Duplicate Records Found !!!!!!!!");
                        $("#"+mid).val("");
                        return false;    
                    }
            });

        }

    var dueDateTimePicker = function(strDate) {
        if ($('#BILLMST_BDUEDT').val() < strDate) {
            $('#BILLMST_BDUEDT').val(strDate);
        }
        $('.duedatetime-picker').datepicker("remove");
        $('.duedatetime-picker').datepicker({
            isRTL: App.isRTL(),
            format: "dd MM yyyy",
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            startDate: new Date(strDate),
            todayBtn: true
        });
        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    return {
        //main function to initiate the module
        init: function(DomainName) {

            ControlDate();
            Formvalidate(DomainName);
            $("#DOCTY").val("DKT")
            $(".clsDateControl").on("changeDate", function(e) {
                dueDateTimePicker($("#BILLMST_BGNDT").val());
            });

            var itemIndex = 1; // $("#container input.iHidden").length;
            AddLedgerRows(DomainName, itemIndex);

            GetLocationNameCode(DomainName, "BILLMST_BILLSUBBRCD");
            GetLocationNameCode(DomainName, "BILLMST_BILLCOLBRCD");

            $("#BILLMST_BILLSUBBRCD").select2("data", {
                id: $("#BILLMST_BILLSUBBRCD").val(),
                text: $("#BillSubLocNM").val()
            });
            $("#BILLMST_BILLCOLBRCD").select2("data", {
                id: $("#BILLMST_BILLCOLBRCD").val(),
                text: $("#BillCollLocNM").val()
            });

            SubmitClick(DomainName);

            GetPartyCode("BILLMST_PTMSCD", $("#BILLMST_BBRCD"), "");

            $("#BILLMST_PTMSCD").change(function() {
                $("#BILLMST_PTMSNM").val($(this).select2('data').text);
            });

            $('.clsLedgerAmount').live('change', function() {
                var TotalLedgerAmount = 0;
                $(".clsLedgerAmount").each(function() {
                    //  alert($(this).val());
                    TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                });
                $("#BILLMST_SUBTOTEL").val(rounditn(TotalLedgerAmount, 2));
                $("#DOCTOT").val(rounditn(TotalLedgerAmount, 2));
                $("#BILLMST_PENDAMT").val(rounditn(TotalLedgerAmount, 2));
                $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                //$("#BILLMST_BILLAMT").val($("#GSTSubTotal").val());
                $("#BILLMST_BILLAMT").val(TotalLedgerAmount);
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $('.clsTripsheet').live('change', function() {
                if ($("#BILLMST_PTMSCD").val() == "") {
                    $(this).val("");
                    alert("Please select Party.");
                }else
                {
                var CurrId = $(this).attr("id");
                CheckDuplicateTrip(CurrId,DomainName);
                }
            });


            $(".clsDocType").hide();

            $('#IsApplyStax').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $('#IsAbatement').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });

            $('#IsRoundOff').click(function() {
                //CalculateStaxTDS(DomainName);
                CalculateStaxTDS1(DomainName);
            });
           
           
            //CalculateStaxTDS(DomainName);
        }
    }
}();