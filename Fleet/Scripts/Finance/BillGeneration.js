﻿var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        $.validator.addMethod("DocumentDateRule", function (value) {

            var StartDate = new Date($("#StartDate").val());
            var EntDate = new Date($("#EntDate").val());
            var DocDate = new Date(value);

            // alert(DocDate + "  " + StartDate + "  " + EntDate)

            if (DocDate >= StartDate && DocDate <= EntDate)
                return true;
            else
                return false;

        }, "Please Select Date ");

        $.validator.addMethod("CheckDocketno", function (value, element) {

            //  alert($(element).attr("id").replace("DOCKNO", "DKTError"))

            var partytype = $("#BQM_PartyType").val();
            var party = $("#BILLMST_PTMSCD").val();

            var CurrId = $(element).attr("id");
            var CurrVal = value;

            // alert(CurrId + "  " + CurrVal);


            var DUPlicateFlg = "N";

            $(".clsDocument").each(function () {

                if (CurrId != $(this).attr("id") && CurrVal == $(this).val()) {
                    $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val("Duplicate Records Found !!!!!!!!");
                    DUPlicateFlg = "Y";
                    return false;

                }

            });

            if (DUPlicateFlg == "N") {
                var StrURL = DomainName + "/Finance/CheckOctroiDocketNo?dockno=" + $(element).val().trim() + "&party=" + party + "&partytype=" + partytype;
                // alert("hello " + StrURL);
                var result1 = $.ajax({
                    async: false,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: StrURL,
                    dataType: 'json',
                    data: {},
                });

                var d1 = JSON.parse(result1.responseText);

                //$("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val(d1.MSG);
                //$("#" + $(element).attr("id").replace("DOCKNO", "declval")).val(d1.DECVAL);
                //if (parseInt($("#" + $(element).attr("id").replace("DOCKNO", "OCT_AMT")).val()) == 0) {
                //    $("#" + $(element).attr("id").replace("DOCKNO", "OCT_AMT")).val(d1.OCT_AMT);
                //}
                //$("#" + $(element).attr("id").replace("DOCKNO", "oct_percentage")).val(parseFloat(d1.oct_percentage).toFixed(2));
                //$("#" + $(element).attr("id").replace("DOCKNO", "DKTTOT")).val(d1.DKTTOT);
                //$("#" + $(element).attr("id").replace("DOCKNO", "OCT_RECEIPTNO")).val(d1.OCT_RECEIPTNO);
                //$("#" + $(element).attr("id").replace("DOCKNO", "recptdt")).val(d1.recptdt);

                $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val(d1.MSG);
                $("#" + $(element).attr("id").replace("DOCKNO", "declval")).val(d1.DECVAL);
                if (parseInt($("#" + $(element).attr("id").replace("DOCKNO", "OCT_AMT")).val()) == 0 || parseInt($("#" + $(element).attr("id").replace("DOCKNO", "OCT_AMT")).val()) == "") {
                    $("#" + $(element).attr("id").replace("DOCKNO", "OCT_AMT")).val(d1.OCT_AMT);
                }
                if (parseFloat($("#" + $(element).attr("id").replace("DOCKNO", "oct_percentage")).val()) == 0 || parseFloat($("#" + $(element).attr("id").replace("DOCKNO", "oct_percentage")).val()) == "") {
                    $("#" + $(element).attr("id").replace("DOCKNO", "oct_percentage")).val(parseFloat(d1.oct_percentage).toFixed(2));
                }
                if (parseInt($("#" + $(element).attr("id").replace("DOCKNO", "DKTTOT")).val()) == 0 || parseInt($("#" + $(element).attr("id").replace("DOCKNO", "DKTTOT")).val()) == "") {
                    $("#" + $(element).attr("id").replace("DOCKNO", "DKTTOT")).val(d1.DKTTOT);
                }
                if (($("#" + $(element).attr("id").replace("DOCKNO", "OCT_RECEIPTNO")).val()) == 0 || ($("#" + $(element).attr("id").replace("DOCKNO", "OCT_RECEIPTNO")).val()) == "") {
                    $("#" + $(element).attr("id").replace("DOCKNO", "OCT_RECEIPTNO")).val(d1.OCT_RECEIPTNO);
                }
                if (($("#" + $(element).attr("id").replace("DOCKNO", "recptdt")).val()) == 0 || ($("#" + $(element).attr("id").replace("DOCKNO", "recptdt")).val()) == "") {
                    $("#" + $(element).attr("id").replace("DOCKNO", "recptdt")).val(d1.recptdt);
                }
                //   alert(d1.MSG + "  " + d1.Flg + "  " + d1.DECVAL);

                if (d1.Flg == "T") {
                    return true;
                }
                else {

                    return false;
                }

            }

        }, function (value, element) {

            //  alert("asdads " + $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val());

            return $("#" + $(element).attr("id").replace("DOCKNO", "DKTError")).val();
        });

        $.validator.addMethod("DueDateRule", function (value) {


            var EntDate = new Date($("#BILLMST_BGNDT").val());
            var DocDate = new Date(value);

            //  alert(DocDate + "  "  + EntDate)

            if (DocDate >= EntDate)
                return true;
            else
                return false;

        }, "Please Select Date ");

        $.validator.addMethod("CheckBillAmount", function (value) {


            var BILLAMT = $("#BILLMST_BILLAMT").val();
            var CRLimit = $("#CUSTHDR_credit_limit").val();



            if ($("#CRLimitAllow").val() == "N") {

                if (parseFloat(BILLAMT) > parseFloat(CRLimit))
                    return false;
                else
                    return true;
            }
            else
                return true;

        }, "Please Select Date ");

        $.validator.addMethod("CheckOUTSTDAmount", function (value) {


            var OutStdAmt = $("#CUSTHDR_OutStdAmt").val();
            var CRLimit = $("#CUSTHDR_credit_limit").val();

            //  alert(OutStdAmt + "  " + CRLimit)

            if ($("#OutStdLimitAllow").val() == "Y") {

                if (parseFloat(OutStdAmt) > parseFloat(CRLimit))
                    return false;
                else
                    return true;
            }
            else
                return true;

        }, "Please Select Date ");

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input


            rules: {


                'CUSTHDR.OutStdAmt': {
                    required: true,
                    CheckOUTSTDAmount: true,
                },
                'BILLMST.BILLAMT': {
                    required: true,
                    min: 1,
                    CheckBillAmount: true,

                },
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: true,

                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: true,

                },
                'BILLMST.manualbillno': {
                    required: function () {

                        if ($("#ManualBillnoMandatory").val() == "Y") {
                            return true;
                        }
                        else
                            return false;
                    }

                },



            },
            messages: {

                'CUSTHDR.OutStdAmt': {

                    CheckOUTSTDAmount: jQuery.format("OutStanding Amount is Greater Then Credit Limit Of The same Customer. !!")
                },

                'BILLMST.BILLAMT': {
                    min: jQuery.format("Please Select at Least One Record"),
                    CheckBillAmount: jQuery.format("Bill Amount Should be Less then Or Equals to Credit Limit For The Same Customer.")
                },
                'BILLMST.BGNDT': {
                    required: true,
                    DocumentDateRule: jQuery.format("Please Select Date between " + $("#sStartDate").val() + " and " + $("#sEntDate").val()),


                },
                'BILLMST.BDUEDT': {
                    required: true,
                    DueDateRule: jQuery.format("Please Select Date Greter or equal to Bill Date"),


                },
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {


        $('#btnSubmit').click(function () {


            // alert($("#BILLMST_BILLAMT").val());


            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");

            App.blockUI({ boxed: true });

            document.forms["form_sample"].submit();


            //  form.submit();

            //  alert("Done...11");


            //if ($("#CTH_THCType").val() == "1")
            //    $(".clsMF").show();
            //else
            //    $(".clsGC").show();

            //$(this).attr("dissbled", true);

            //$('#divStep1 :button').attr('disabled', true);

            //$('#divStep1 :input').attr('disabled', true);

        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0;
        $(".clsDocketCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "dkttot");

            if ($("#IsDAMBill").val() == "True")
                LoadWtId = $(this).attr("id").replace("IsEnabled", "Total");

            if (checked) {

                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
            }

        });

        totAMT = rounditn(totAMT, 2)

        $("#BILLMST_BILLAMT").val(totAMT);
        $("#BILLMST_BILLAMT").attr("value", totAMT);

    }

    var SelectAllCheckBox = function (DomainName) {

        $("#IsAllEnabled").live('change', function () {

            // alert("Heeell");

            var checked = jQuery(this).is(":checked");

            $(".clsDocketCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectCheckBox(DomainName);
        });

        $(".clsDocketCheck").live('change', function () {

            SelectCheckBox(DomainName);
        });
    }

    var dueDateTimePicker = function (strDate) {
        //if (jQuery().datepicker) {
        $('.duedatetime-picker').datepicker('remove');
        $('.duedatetime-picker').datepicker({
            isRTL: App.isRTL(),
            format: "dd M yyyy",
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            startDate: new Date(strDate),
            todayBtn: true
        });
        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        //}
    }

    var AddLedgerRows = function (DomainName, itemIndex) {




        $("#btnAddRows").click(function () {
            //  alert("Hello");

            var TOTRows = $("#LedgerRows").val();

            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;

                    AddLedgerRow(DomainName, itemIndex);

                }, 250 * i);
            }

        });

        LedgerDetails(1, DomainName);

    }

    var AddLedgerRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Finance/OctroiDetails/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer").append(html);
                // alert("asdasdads");
                $(".clsStax").hide();
                LedgerDetails(Rid, DomainName);

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var LedgerDetails = function (Rid, DomainName) {



        $(".clsDocument").each(function () {

            var id = $(this).attr("id").replace("DOCKNO", "DKTError");

            $(this).rules("add", {
                CheckDocketno: true,
                //message: function () {

                //    alert("sdfsdf " + $("#" + id).val());

                //    return $("#" + id).val();
                //}

            });

        });


        $("#LedgerList_" + Rid + "__recptdt").attr("readonly", "true")

        var startdate = new Date();
        var enddate = new Date();

        //startdate = startdate.getDate() - 200;
        //enddate = enddate.getDate() + 200;

        startdate.setDate(startdate.getDate() - 200);
        enddate.setDate(enddate.getDate() + 200);

        // alert(startdate + "  " + enddate);

        handleDateTimePicker_ddmmyyyy("LedgerList_" + Rid + "__recptdt", startdate, enddate);

        //var IsMultipleDoc = $('#IsMultipleDoc').is(':checked');

        //if (IsMultipleDoc)
        //    $(".clsDocType").show();
        //else
        //    $(".clsDocType").hide();


        //var StrURL = DomainName + "/Payment/GetLedgerBySearch";
        //JsonDDLSelectCode("LedgerList_" + Rid + "__Acccode", StrURL);

        //if (Rid != "1") {
        //    $("#LedgerList_" + Rid + "__DocType").select2({
        //        placeholder: "Select an option",
        //        allowClear: true
        //    });
        //}

        //$("#LedgerList_" + Rid + "__Acccode").change(function () {

        //    $("#LedgerList_" + Rid + "__Accdesc").val($(this).select2('data').text);

        //});




    }

    var CalculateOctroiCharges = function (Rid) {

        var OCT_AMT = $("#LedgerList_" + Rid + "__OCT_AMT").val();
        var declval = $("#LedgerList_" + Rid + "__declval").val();

        if (declval == "" || declval == null)
            declval = 0;
        if (OCT_AMT == "" || OCT_AMT == null)
            OCT_AMT = 0;

        var OctPer = parseFloat(OCT_AMT) * 100 / parseFloat(declval);

        $("#LedgerList_" + Rid + "__oct_percentage").val(rounditn(OctPer, 2));

        var OTCHG = $("#LedgerList_" + Rid + "__OTCHG").val();
        var clearance_chrg = $("#LedgerList_" + Rid + "__clearance_chrg").val();
        var processing_per = $("#LedgerList_" + Rid + "__processing_per").val();

        if (OTCHG == "" || OTCHG == null)
            OTCHG = 0;
        if (clearance_chrg == "" || clearance_chrg == null)
            clearance_chrg = 0;
        if (processing_per == "" || processing_per == null)
            processing_per = 0;

        var process_chrg = parseFloat(OCT_AMT) * parseFloat(processing_per) / 100;
        if (process_chrg == "" || process_chrg == null)
            process_chrg = 0;

        $("#LedgerList_" + Rid + "__process_chrg").val(rounditn(process_chrg, 2));

        //  alert(parseFloat(OCT_AMT) + " ~ " + parseFloat(process_chrg) + " ~ " + parseFloat(clearance_chrg) + " ~ " + parseFloat(OTCHG))

        var DKTTOT = parseFloat(OCT_AMT) + parseFloat(process_chrg) + parseFloat(clearance_chrg) + parseFloat(OTCHG);



        var svctax_rate = $("#BILLMST_svctax_rate").val();
        var H_cess_rate = $("#BILLMST_H_cess_rate").val();
        var SBCRATE = $("#BILLMST_SBCRATE").val();
        var cess_rate = $("#BILLMST_cess_rate").val();

        var Subtotal = 0;
        if ($("#IsOctroiStax").val() == "True") {


            STAX = DKTTOT * svctax_rate / 100;
            CESS = STAX * cess_rate / 100;
            HCESS = STAX * H_cess_rate / 100;
            SB = DKTTOT * SBCRATE / 100;


            $("#LedgerList_" + Rid + "__SVRCAMT").val(rounditn(STAX, 2));
            $("#LedgerList_" + Rid + "__CESSAMT").val(rounditn(CESS, 2));
            $("#LedgerList_" + Rid + "__Hedu_Cess").val(rounditn(HCESS, 2));
            $("#LedgerList_" + Rid + "__SBCAMT").val(rounditn(SB, 2));

            Subtotal = parseFloat(STAX) + parseFloat(CESS) + parseFloat(HCESS) + parseFloat(SB);

        }
        DKTTOT = DKTTOT + Subtotal;

        DKTTOT = rounditn(DKTTOT, 0)
        DKTTOT = rounditn(DKTTOT, 2)


        $("#LedgerList_" + Rid + "__DKTTOT").val(DKTTOT);



        var TOTBILLAMOUNT = 0;


        $(".clsDKTTOTAmount").each(function () {


            TOTBILLAMOUNT = TOTBILLAMOUNT + parseFloat($(this).val());

        });


        TOTBILLAMOUNT = rounditn(TOTBILLAMOUNT, 0)
        TOTBILLAMOUNT = rounditn(TOTBILLAMOUNT, 2)

        $("#BILLMST_BILLAMT").val(TOTBILLAMOUNT);

    }

    var GetServiceTax = function (DomainName, DOCDT) {

        $(".clsStax").hide();
        if ($("#IsOctroiStax").val() == "True") {


            $(".clsStax").show();

            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Finance/GetServiceTax?DOCKDT=" + DOCDT;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //  alert(data1.CONTRACTID + "  " + data1.TransMode + "  " + data1.ServiceType);

                    $("#BILLMST_svctax_rate").val(data1.ServiceTaxRate);
                    $("#BILLMST_cess_rate").val(data1.CessRate);
                    $("#BILLMST_H_cess_rate").val(data1.HCessRate);
                    $("#BILLMST_SBCRATE").val(data1.SBRate);
                    $("#StaxRebate").val(data1.StaxRebateRate);

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });
        }

    }

    var testing = function () {
        var table = $('#tblDKT').DataTable({
            //"aLengthMenu": [
            //[5, 10, 15, 20, -1],
            //[5, 10, 15, 20, "All"]
            //],
            //"iDisplayLength": 10,

        });



        $('#form_sample').on('submit', function (e) {
            //alert("nitin");
            // Force all the rows back onto the DOM for postback
            table.rows().nodes().page.len(-1).draw(false);  // This is needed
            if ($(this).valid()) {
                App.blockUI({ boxed: true });
                document.forms["form_sample"].submit();
                return true;
            }
            e.preventDefault();
        });

    }

    return {
        //main function to initiate the module
        init: function (DomainName) {
            //testing();
            ControlDate();
            dueDateTimePicker($("#BILLMST_BGNDT").val());
            $(".clsDateControl").on("changeDate", function (e) {

                //  alert($("#BILLMST_BGNDT").val());

                GetServiceTax(DomainName, $("#BILLMST_BGNDT").val());
                dueDateTimePicker($("#BILLMST_BGNDT").val());

            });

            GetServiceTax(DomainName, $("#BILLMST_BGNDT").val());

            $(".clsOctAmount").live("change", function (e) {
                var Rid = $(this).attr("id").replace("__OCT_AMT", "").replace("LedgerList_", "");
                CalculateOctroiCharges(Rid);
            });

            $(".clsOtherChrg").live("change", function (e) {
                var Rid = $(this).attr("id").replace("__OTCHG", "").replace("__SVRCAMT", "").replace("__CESSAMT", "").replace("__Hedu_Cess", "").replace("__SBCAMT", "").replace("LedgerList_", "");
                CalculateOctroiCharges(Rid);
            });

            $(".clsclrAmount").live("change", function (e) {
                var Rid = $(this).attr("id").replace("__clearance_chrg", "").replace("LedgerList_", "");
                CalculateOctroiCharges(Rid);
            });

            $(".clsPROSAmount").live("change", function (e) {
                var Rid = $(this).attr("id").replace("__processing_per", "").replace("LedgerList_", "");
                CalculateOctroiCharges(Rid);
            });

            if ($("#IsDAMBill").val() == "True") {
                $(".clsFRTBILL").hide();
                $(".clsDAMBILL").show();

            }
            else {
                $(".clsFRTBILL").show();
                $(".clsDAMBILL").hide();
            }

            Formvalidate(DomainName);

            var itemIndex = 1;// $("#container input.iHidden").length;
            AddLedgerRows(DomainName, itemIndex);

            GetLocationNameCode(DomainName, "BILLMST_BILLSUBBRCD");
            GetLocationNameCode(DomainName, "BILLMST_BILLCOLBRCD");

            $("#BILLMST_BILLSUBBRCD").select2("data", { id: $("#BILLMST_BILLSUBBRCD").val(), text: $("#BillSubLocNM").val() });
            $("#BILLMST_BILLCOLBRCD").select2("data", { id: $("#BILLMST_BILLCOLBRCD").val(), text: $("#BillCollLocNM").val() });

            SubmitClick(DomainName);

            SelectAllCheckBox(DomainName);

        }
    }
}();