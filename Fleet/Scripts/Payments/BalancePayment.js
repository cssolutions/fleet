﻿function pageLevelCallbackFunction() {
    //$("#TotVoucherAmount").val(parseFloat($("#TotVoucherAmount").val()) - parseFloat($("#TotalContractAmount").val()) + parseFloat($("#TotalPaymentAmount").val()) + parseFloat($("#PendAMT").val()))
    var isGSTReverse = $("#isGSTReverse").is(":checked");
    if (isGSTReverse) {
        $("#TotVoucherAmount").val(rounditn(parseFloat($("#StaxOnAmount").val()) - parseFloat($("#TDSAmount").val()),2));
    }
    if ($('input[name=PaymentTyp]:checked').val() == "1") {
       // $("#TotPendingAmount").val(rounditn(parseFloat($("#TotVoucherAmount").val()) - parseFloat($("#TotalAdvanceAmount").val()), 2));
        $("#TotPendingAmount").val(rounditn(parseFloat($("#TotalNetPaybleAmount").val()) - parseFloat($("#TotalPaymentAmount").val()), 2));

    }
    else {
   // alert("hwll")
        $("#PayAmount").val(parseFloat($("#TotVoucherAmount").val()) - parseFloat($("#PendAMT").val()));
        $("#TotPendingAmount").val($("#PendAMT").val());
    }
}
var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input

            rules: {
                'TotVoucherAmount': {
                    required: true,
                    min: function () {
                        if ($("#APFM_VendorCode").val() == "V01260" || $('input[name=PaymentTyp]:checked').val() == "1") {
                            return 0;
                        }
                        else
                            return 1;
                    },
                    number: true,
                },
                'TotalNetPaybleAmount': {
                    required: true,
                    min: function () {
                        if (parseFloat($('#TotalContractAmount').val()).toFixed(2) == parseFloat($('#TotalAdvanceAmount').val()).toFixed(2)) {
                            return 0;
                        }
                        else
                            return 1;
                    }
                },
            },
            messages: {

                'TotalRecods': {
                    min: jQuery.format("Please Select at Least One Record")
                },

                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {


        $('#btnSubmit').click(function () {


            // alert("Helloo");


            var form = $('#form_sample');



            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");

            App.blockUI({ boxed: true });

            document.forms["form_sample"].submit();



        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0;
        var StrDocno = "";
        $(".chkactive").each(function () {

            var checked = $(this).is(":checked");

            if (checked) {

                if (StrDocno == "")
                    StrDocno = $(this).attr("data-value");
                else
                    StrDocno = StrDocno + "," + $(this).attr("data-value");

                totAMT = totAMT + 1;
            }

        });

        $("#TotalRecods").val(totAMT);

        $("#APFM_SDocNo").val(StrDocno);



    }

    var SelectAllCheckBox = function (DomainName) {


        $("#IsAllEnabled").live('change', function () {

            // alert("Heeell");

            var checked = jQuery(this).is(":checked");

            $(".chkactive").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectCheckBox(DomainName);
        });


        $(".chkactive").live('change', function () {

            SelectCheckBox(DomainName);
        });

    }

    var TotalChargesDetails = function () {


        var TotContAmt = 0, TotOtherChrg = 0, TotAdvAmt = 0, TotBalAmt = 0, TotNetAmt = 0, TotPaymentAmt = 0, TotPendAmt = 0, TotGSTAmt = 0;

        $(".clsContAmt").each(function () {
            TotContAmt = TotContAmt + parseFloat($(this).val());
        });

        $(".clsOtherChrg").each(function () {
            TotOtherChrg = TotOtherChrg + parseFloat($(this).val());
        });

        $(".clsAdvAmt").each(function () {
            TotAdvAmt = TotAdvAmt + parseFloat($(this).val());
        });

        $(".clsBalAmt").each(function () {
            TotBalAmt = TotBalAmt + parseFloat($(this).val());
        });

        $(".clsPayableAmt").each(function () {
            TotNetAmt = TotNetAmt + parseFloat($(this).val());
        });

        $(".clsPaymentAmt").each(function () {
            TotPaymentAmt = TotPaymentAmt + parseFloat($(this).val());
        });

        $(".clsPendAmt").each(function () {
            TotPendAmt = TotPendAmt + parseFloat($(this).val());
        });

        $(".clsGSTChgAmt").each(function () {
            TotGSTAmt = TotGSTAmt + parseFloat($(this).val());
        });

        //var StaxRateAmount = $("#StaxRateAmount").val();
        //var CessAmount = $("#CessAmount").val();
        //var HCessAmount = $("#HCessAmount").val();
        //var SBAmount = $("#SBAmount").val();
        //var KKCAmount = $("#KKCAmount").val();

        var OtherAmountPlus = $("#OtherAmountPlus").val();
        var OtherAmountLess = $("#OtherAmountLess").val();
        var TDSAmount = $("#TDSAmount").val();

        //if (StaxRate == "" || StaxRate == null)
        //    StaxRate = "0";

        //if (CessRate == "" || CessRate == null)
        //    CessRate = "0";

        //if (HCessRate == "" || HCessRate == null)
        //    HCessRate = "0";

        //if (SBRate == "" || SBRate == null)
        //    SBRate = "0";

        if (OtherAmountPlus == "" || OtherAmountPlus == null)
            OtherAmountPlus = "0";

        if (TDSAmount == "" || TDSAmount == null)
            TDSAmount = "0";

        if (OtherAmountLess == "" || OtherAmountLess == null)
            OtherAmountLess = "0";

        TotNetAmt = parseFloat(TotNetAmt) //+ parseFloat(StaxRateAmount) + parseFloat(CessAmount) + parseFloat(HCessAmount) + parseFloat(SBAmount) + parseFloat(KKCAmount);

        TotNetAmt = parseFloat(TotNetAmt) - parseFloat(TDSAmount);

        TotNetAmt = parseFloat(TotNetAmt) - parseFloat(OtherAmountLess);

        TotNetAmt = parseFloat(TotNetAmt) - parseFloat(OtherAmountPlus);

        //TotContAmt = rounditn(TotContAmt, 0)
        //TotOtherChrg = rounditn(TotOtherChrg, 0)
        //TotAdvAmt = rounditn(TotAdvAmt, 0)
        //TotBalAmt = rounditn(TotBalAmt, 0)
        //TotNetAmt = rounditn(TotNetAmt, 0)
        //TotPaymentAmt = rounditn(TotPaymentAmt, 0);



        TotContAmt = rounditn(TotContAmt, 2)
        TotOtherChrg = rounditn(TotOtherChrg, 2)
        TotAdvAmt = rounditn(TotAdvAmt, 2)
        TotBalAmt = rounditn(TotBalAmt, 2)
        TotNetAmt = rounditn(TotNetAmt, 2)
        TotPaymentAmt = rounditn(TotPaymentAmt, 2);
        TotPendAmt = rounditn(TotPendAmt, 2);
        //alert(roundint(TotContAmt, 2));

        //$("#StaxOnAmount").val(rounditn(TotGSTAmt, 2));
        $("#StaxOnAmount").val(rounditn(TotNetAmt, 2));

        var TDSOnAmount1 = parseFloat(TotContAmt)
        //+ parseFloat(StaxRateAmount) + parseFloat(CessAmount) + parseFloat(HCessAmount) + parseFloat(SBAmount) + parseFloat(KKCAmount);

        var DiscountType = $("#DiscountType").val();
        var Discount = $("#Discount").val();

        if (parseFloat(Discount) > 0) {
            if (DiscountType == "F") {
                TotNetAmt = rounditn(parseFloat(TotNetAmt) - parseFloat(Discount), 0);
            }
            if (DiscountType == "P") {
                var DiscountAmount = rounditn((parseFloat(TotNetAmt) * parseFloat(Discount)) / 100, 0);

                TotNetAmt = rounditn(parseFloat(TotNetAmt) - parseFloat(DiscountAmount), 0);
            }

        }
        //alert(TotNetAmt)
        //$("#TDSOnAmount").val(rounditn(TDSOnAmount1, 2));
        $("#TDSOnAmount").val(rounditn(TotContAmt, 2));

        //$("#TotVoucherAmount").val(rounditn(TotNetAmt, 2));



        var PaymentType = $('input[name=PaymentTyp]:checked').val();

        var PaymentAmt = parseFloat(TotNetAmt) - parseFloat(TotPendAmt);

        $("#PayAmount").val(rounditn(PaymentAmt, 2));


        if ($("#PaymentMode").val().toUpperCase() == "CASH") {
            $("#CashAmount").val($("#PayAmount").val());
            $("#ChequeAmount").val("0");
        }
        if ($("#PaymentMode").val().toUpperCase() == "BANK") {
            $("#ChequeAmount").val($("#PayAmount").val());
            $("#CashAmount").val("0");
        }
        if ($("#PaymentMode").val().toUpperCase() == "BOTH") {
            $("#CashAmount").val("0");
            $("#ChequeAmount").val("0");
        }

        //if (PaymentType=="1")
        //    $("#PayAmount").val(rounditn(TotNetAmt, 2));
        //else
        //    $("#PayAmount").val(rounditn(TotPaymentAmt, 2));


        $("#TotalContractAmount").val(TotContAmt);
        $("#TotalOtherAmount").val(TotOtherChrg);
        $("#TotalAdvanceAmount").val(TotAdvAmt);
        $("#TotalBalanceAmount").val(TotBalAmt);
        $("#TotalNetPaybleAmount").val(TotNetAmt);
        //$("#TotalGSTChargeAmount").val(rounditn(TotGSTAmt, 2));
        $("#TotPendingAmount").val(rounditn(TotPendAmt, 2));
        $("#PendAMT").val(TotPendAmt);
        //console.log(PaymentType + " --- " + TotNetAmt)
        if (PaymentType == "1") {
            $("#TotPendingAmount").val(rounditn(TotNetAmt, 2));
        }

        $("#TotalPaymentAmount").val(TotPaymentAmt);
        CalculateStaxTDS();
     
    }

    var ChargesDetails = function () {

        $(".clsCharge").live('change', function () {

            if ($(this).val() == "") {
                $(this).val("0.00");
            }

            var TotaChrgAmt = 0;

            var RowTotalAmt = 0;

            $("." + $(this).attr("dataid")).each(function () {

                TotaChrgAmt = TotaChrgAmt + parseFloat($(this).val());

            });

            TotaChrgAmt = rounditn(TotaChrgAmt, 0);
            TotaChrgAmt = rounditn(TotaChrgAmt, 2);

            $(".TOT" + $(this).attr("dataid")).val(TotaChrgAmt);

            var RowCls = $(this).attr("id").replace("DocuemntList_", "").replace("__" + $(this).attr("dataid").replace("cls", ""), "");

            var ContractAmt = $("#DocuemntList_" + RowCls + "__Contract_Amt").val();
            var OTherChrg = $("#DocuemntList_" + RowCls + "__OTherChrg").val();
            var Advamt = $("#DocuemntList_" + RowCls + "__Advamt").val();
            var netbalamt = $("#DocuemntList_" + RowCls + "__netbalamt").val();

          

            $(".clsRow" + RowCls).each(function () {

                //  alert($("#"+  $(this).attr("Id")+"Operator").val());


                if ($("#" + $(this).attr("Id") + "Operator").val() == "-")
                    RowTotalAmt = RowTotalAmt - parseFloat($(this).val());
                else
                    RowTotalAmt = RowTotalAmt + parseFloat($(this).val());

            });

            var totAmt = parseFloat(RowTotalAmt) + parseFloat(netbalamt);

           
           
            totAmt = rounditn(totAmt, 0);
            totAmt = rounditn(totAmt, 2);
           

            $("#DocuemntList_" + RowCls + "__NET").val(totAmt);

            $("#DocuemntList_" + RowCls + "__PaymentAmount").attr('max', totAmt);
          
            var PaymentAmount = $("#DocuemntList_" + RowCls + "__PaymentAmount").val();
          

            if (PaymentAmount == "" || PaymentAmount == null)
                PaymentAmount = 0;
           
            var PaymentType = $('input[name=PaymentTyp]:checked').val();
          
            if (PaymentType == "2") {
                $("#DocuemntList_" + RowCls + "__PaymentAmount").val(totAmt);

            }

            var PendAmt = parseFloat(totAmt) - parseFloat(PaymentAmount);

            if (PaymentType == "1") {
              //  alert("1")
                $("#DocuemntList_" + RowCls + "__PendingAmount").val(rounditn(PendAmt, 2));

            }
            else
            {
              //  alert("2")
                $("#DocuemntList_" + RowCls + "__PendingAmount").val(0);
              
            }

            var GSTChgAmt = rounditn(parseFloat(ContractAmt) + parseFloat(RowTotalAmt), 2);

            $("#DocuemntList_" + RowCls + "__GSTChgAmt").val(GSTChgAmt);
            $("#DocuemntList_" + RowCls + "__CostPerKG").val(parseFloat(parseFloat(GSTChgAmt) / parseFloat($("#DocuemntList_" + RowCls + "__TotalWt").val())).toFixed(2));
            TotalChargesDetails();
        });


    }

    //var CalculateStaxTDS = function (DomainName) {

    //    //var StaxRate = $("#StaxRate").val();
    //    //var CessRate = $("#CessRate").val();
    //    //var HCessRate = $("#HCessRate").val();
    //    //var SBRate = $("#SBRate").val();
    //    //var KKCRate = $("#KKCRate").val();

    //    var StaxOnAmount = $("#StaxOnAmount").val();

    //    var TotPayAmt = $("#TotalNetPaybleAmount").val();

    //    //alert(StaxRate + " " + CessRate + " " + HCessRate + " " + SBRate);

    //    var StaxAmount = 0, CessAmount = 0, HCessAmount = 0, SBAmount = 0, KKCAmount = 0;

    //    //if ($("#IsStaxEnabled").is(':checked')) {

    //    //    StaxAmount = rounditn((parseFloat(StaxOnAmount) * parseFloat(StaxRate)) / 100, 0);
    //    //    CessAmount = rounditn((parseFloat(StaxAmount) * parseFloat(CessRate)) / 100, 0);
    //    //    HCessAmount = rounditn((parseFloat(StaxAmount) * parseFloat(HCessRate)) / 100, 0);
    //    //    SBAmount = rounditn((parseFloat(StaxOnAmount) * parseFloat(SBRate)) / 100, 0);
    //    //    KKCAmount = rounditn((parseFloat(StaxOnAmount) * parseFloat(KKCRate)) / 100, 0);
    //    //    $("#StaxRegNo").rules("add", "required");
    //    //    $("#StaxType").attr("disabled", false);
    //    //    $("#StaxType").rules("add", "required");
    //    //    if ($("#UserId").val().toUpperCase() == "HQT026") {
    //    //        $("#SBRate").attr("readonly", false);
    //    //        $("#KKCRate").attr("readonly", false);
    //    //    }
    //    //    $(".clsSTAX").show();
    //    //}
    //    //else {

    //    //    StaxAmount = 0;
    //    //    CessAmount = 0;
    //    //    HCessAmount = 0;
    //    //    SBAmount = 0;
    //    //    KKCAmount = 0;
    //    //    $("#StaxRegNo").rules("remove", "required");
    //    //    $("#StaxType").attr("disabled", true);
    //    //    $("#StaxType").rules("remove", "required");

    //    //    $("#SBRate").attr("readonly", true);
    //    //    $("#KKCRate").attr("readonly", true);

    //    //    $(".clsSTAX").hide();
    //    //}

    //    //$("#StaxRateAmount").val(StaxAmount);
    //    //$("#CessAmount").val(CessAmount);
    //    //$("#HCessAmount").val(HCessAmount);
    //    //$("#SBAmount").val(SBAmount);
    //    //$("#KKCAmount").val(KKCAmount);

    //    var IsTDSEnabled = $('#IsTDSEnabled').is(':checked');

    //    if ($('#IsTDSEnabled').is(':checked')) {
    //        $("#TDSRate").attr("readonly", false);
    //        $("#TDSRate").attr("min", 0);

    //        $("#TDSRate").rules("add", "required");
    //        $("#TDSAcccode").rules("add", "required");
    //        $("#PANNO").rules("add", "required");

    //        $(".clsTDS").show();
    //    }
    //    else {
    //        $("#TDSRate").attr("readonly", true);
    //        $("#TDSRate").attr("min", 0);

    //        $(".clsTDS").hide();

    //    }

    //    var TDSApplyOn = parseFloat(StaxOnAmount)
    //        //+ parseFloat(StaxAmount) + parseFloat(CessAmount) + parseFloat(HCessAmount) + parseFloat(SBAmount) + parseFloat(KKCAmount);

    //    var TDSApplyOn1 = parseFloat(TotPayAmt)
    //        //+ parseFloat(StaxAmount) + parseFloat(CessAmount) + parseFloat(HCessAmount) + parseFloat(SBAmount) + parseFloat(KKCAmount);
    //    // alert(parseFloat(StaxOnAmount) +" "+ parseFloat(StaxAmount)  +" "+  parseFloat(CessAmount) +" "+  parseFloat(HCessAmount) +" "+  parseFloat(SBAmount));
    //    //alert(parseFloat(StaxOnAmount))
    //    TDSApplyOn = rounditn(TDSApplyOn, 0);
    //    $("#TDSOnAmount").val(rounditn(parseFloat(StaxOnAmount), 0));

    //    var TDSAmount = 0;

    //    var TDSRate = $("#TDSRate").val();

    //    if (IsTDSEnabled) {
    //        //TDSAmount = rounditn((parseFloat(TDSApplyOn) * parseFloat(TDSRate)) / 100, 0);
    //        TDSAmount = rounditn((parseFloat(StaxOnAmount) * parseFloat(TDSRate)) / 100, 0);
    //    }
    //    else
    //        TDSAmount = 0;

    //    TDSAmount = rounditn(TDSAmount, 0);
    //    $("#TDSAmount").val(rounditn(TDSAmount, 0));

    //    var BillAmt1 = rounditn(parseFloat(TDSApplyOn1) - parseFloat(TDSAmount), 0);

    //    var OtherAmountPlus = $("#OtherAmountPlus").val();
    //    var OtherAmountLess = $("#OtherAmountLess").val();

    //    if (OtherAmountPlus == "" || OtherAmountPlus == null)
    //        OtherAmountPlus = "0";


    //    if (OtherAmountLess == "" || OtherAmountLess == null)
    //        OtherAmountLess = "0";

    //    //var DiscountType = $("#DiscountType").val();
    //    //var Discount = $("#Discount").val();

    //    //if (parseFloat(Discount) > 0) {
    //    //    if (DiscountType == "F") {                
    //    //        BillAmt1 = rounditn(parseFloat(BillAmt1) - parseFloat(Discount), 0);
    //    //    }
    //    //    if (DiscountType == "P") {
    //    //        var DiscountAmount = rounditn((parseFloat(BillAmt1) * parseFloat(Discount)) / 100, 0);

    //    //        BillAmt1 = rounditn(parseFloat(BillAmt1) - parseFloat(DiscountAmount), 0);
    //    //    }            
    //    //}

    //    var BillAmt2 = rounditn(parseFloat(BillAmt1) + parseFloat(OtherAmountPlus), 0);
    //    var BillAmt = rounditn(parseFloat(BillAmt2) - parseFloat(OtherAmountLess), 0);
    //    //alert(BillAmt1 + " + " + OtherAmountPlus + "  -  " + OtherAmountLess + "  = " + BillAmt);
    //    // alert($("#FiledNextAmount").val().replace(".", "_"))

    //    BillAmt = rounditn(BillAmt, 0)
    //    BillAmt = rounditn(BillAmt, 1)

    //    $("#" + $("#FiledNextAmount").val().replace(".", "_")).val(rounditn(BillAmt, 2));
    //    $("#PayAmount").val(rounditn(BillAmt, 2));

    //    TotalChargesDetails();
    //    //if (!$("#IsStaxEnabled").is(':checked') && !$("#IsTDSEnabled").is(':checked')) {
    //    //    $("#" + $("#FiledNextAmount").val().replace(".", "_")).val($("#TotalNetPaybleAmount").val());
    //    //    $("#PayAmount").val($("#TotalNetPaybleAmount").val());
    //    //}
    //}

    var StaxTDSEnabled = function (DomainName) {

        //  alert("Hello");
        //$("#StaxType").live('change', function () {
        //    GetStaxRate(DomainName);
        //});

        $(".clsSTAX").hide();
        $(".clsTDS").hide();

        var StrURL = DomainName + "/Payment/GetTDSLedger";
        FillDropDownfromOther("TDSAcccode", StrURL, "Billing Type");

        $('.clsStaxTDS').click(function () {
            // alert("Hello")
            CalculateStaxTDS(DomainName);
        });

        //$('#SBRate , #KKCRate').change(function () {
        //    CalculateStaxTDS(DomainName);
        //});

        $('.clsAMT').change(function () {
            CalculateStaxTDS(DomainName);
        });

        $('#DiscountType').change(function () {
            CalculateStaxTDS();
        });
    }

    //var GetStaxRate = function (DomainName) {
    //    //alert("Helloo   " + DomainName);
    //    //var StrURL1 = DomainName + "/Payment/GetTaxRate?DocuemntDate=" + $.datepicker.formatDate('dd M yy', new Date());
    //    var StrURL1 = DomainName + "/Payment/GetTaxRateFromID?Id=" + $("#StaxType").val();
    //    //alert(StrURL1)
    //    $.ajax({
    //        type: 'POST',
    //        contentType: 'application/json; charset=utf-8',
    //        url: StrURL1,
    //        dataType: 'json',
    //        data: {},
    //        success: function (data1) {

    //            //alert(data1.StaxRate + "  " + data1.SBRate + "  " + data1.KKCRate);

    //            $("#StaxRate").val(data1.StaxRate);
    //            $("#CessRate").val(data1.CessRate);
    //            $("#HCessRate").val(data1.HEduCessRate);
    //            $("#SBRate").val(data1.SBRate);
    //            $("#KKCRate").val(data1.KKCRate);
    //            CalculateStaxTDS(DomainName);
    //            //$("#SBRate").attr("readonly", false);
    //        },
    //        error: function () {
    //            alert("error");
    //        }
    //    });
    //}

    var GetVendorServiceNoAndPanNo = function (DomainName) {
        var StrURL1 = DomainName + "/Payment/GetVendorServiceNoAndPanNo?VendorCode=" + $("#APFM_VendorCode").val();
        //alert(StrURL1)
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL1,
            dataType: 'json',
            data: {},
            success: function (data1) {

                //alert(data1.StaxRate + "  " + data1.SBRate + "  " + data1.KKCRate);

                $("#StaxRegNo").val(data1.ServiceTaxNo);
                $("#PANNO").val(data1.PanNo);
            },
            error: function () {
                alert("error");
            }
        });
    }
    var DueDateValidation = function (DomainName) {
        $('#DueDays').change(function () {
            var BILLGNDT = $('#BILLDT').val();
            var strDT = new Date(BILLGNDT);
            var days = parseInt($(this).val());
            strDT.setDate(strDT.getDate() + days);
            $("#DUEDT").val($.datepicker.formatDate('dd M yy', new Date(strDT)));
        });
    };
  

    return {
        //main function to initiate the module
        init: function (DomainName) {

            ControlDate();

            ChargesDetails();

            StaxTDSEnabled(DomainName);

            PaymentControl(DomainName);

            Formvalidate(DomainName);

            SubmitClick(DomainName);

            SelectAllCheckBox(DomainName);

            TotalChargesDetails();

            GetVendorServiceNoAndPanNo(DomainName);

            $(".clsCostPerKG").each(function () {
                var TotalWt = $(this).attr("id").replace("CostPerKG", "TotalWt");
                var GSTChgAmt = $(this).attr("id").replace("CostPerKG", "GSTChgAmt");
                var CostPerKG = parseFloat($("#" + GSTChgAmt).val()) / parseFloat($("#" + TotalWt).val());
                $("#" + $(this).attr("id")).val(parseFloat(CostPerKG).toFixed(2));
            });

            $(".clsAmount").change(function () {
                var chrg = $(this).val();


                chrg = rounditn(chrg, 0);
                chrg = rounditn(chrg, 2);

                $(this).val(chrg);
            });
            $(".clsAmount").each(function () {

                var chrg = $(this).val();


                chrg = rounditn(chrg, 0);
                chrg = rounditn(chrg, 2);

                $(this).val(chrg);


            });

            
            //$('#DueDays').change(function () {
             
            //});
           

            $(".clsPaymentContraol").hide();
            $(".clsNoPayment").hide();
            $(".ClsbtnSave").hide();
          //  $('#PaymentType').val("1");
           
            $("input[name='PaymentTyp']").click(function () {
              //  $(".clsPaymentContraol").hide();
              //  $(".clsNoPayment").show();
                var PaymentType = $('input[name=PaymentTyp]:checked').val();
                if (PaymentType == "2") {
                    $(".clsNoPayment").show();
                    $(".clsPaymentContraol").show();
                    $(".ClsbtnSave").show();
                    $(".clsPaymentAmt").attr("readonly", false);
                    $(".clsPaymentAmt").each(function () {
                        var NETAMT = $("#" + $(this).attr("Id").replace("PaymentAmount", "NET")).val();
                        $("#" + $(this).attr("Id").replace("PaymentAmount", "PendingAmount")).val(0);
                        $(this).val(NETAMT);
                        TotalChargesDetails();
                        $(".clsPayableAmt ,.clsGSTChgAmt , #TotVoucherAmount").attr("min", 1);
                    });

                }
                else {
                  
                    $(".clsNoPayment").show();
                    $(".clsPaymentContraol").hide();
                    $(".ClsbtnSave").show();
                    $(".clsPaymentAmt").attr("readonly", true);
                    $(".clsPayableAmt ,.clsGSTChgAmt , #TotVoucherAmount").attr("min", 0);
                    $(".clsPaymentAmt").each(function () {
                        var NETAMT = $("#" + $(this).attr("Id").replace("PaymentAmount", "NET")).val();
                        $("#" + $(this).attr("Id").replace("PaymentAmount", "PendingAmount")).val(NETAMT);
                        $(this).val(0);
                        TotalChargesDetails();
                    });
                }
                $('#PaymentType').val(PaymentType);
            });

            $(".clsPaymentAmt").change(function () {

                var PAYAMT = $(this).val();
                var NETAMT = $("#" + $(this).attr("Id").replace("PaymentAmount", "NET")).val();

                //   alert(PAYAMT + "  " + NETAMT);

                var PENDAMT = parseFloat(NETAMT) - parseFloat(PAYAMT);

                $("#" + $(this).attr("Id").replace("PaymentAmount", "PendingAmount")).val(PENDAMT);

                TotalChargesDetails();

            });

            if ($("#APFM_VendorCode").val() == "V01260" || $('input[name=PaymentTyp]:checked').val() == "1") {
                $(".clsPayableAmt ,.clsGSTChgAmt , #TotVoucherAmount").attr("min", 0);
            }
            else {
                $(".clsPayableAmt ,.clsGSTChgAmt , #TotVoucherAmount").attr("min", 1);
            }

            $("#isGSTReverse").on("change", function () {
                CalculateStaxTDS();
            });
            $("#uniform-isGSTReverse").addClass("hide");
            CalculateStaxTDS(DomainName);

            DueDateValidation(DomainName);
            $('#DueDays').trigger("change");

        }
    }
}();