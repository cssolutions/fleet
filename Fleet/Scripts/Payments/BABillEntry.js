﻿function pageLevelCallbackFunction() {
    //$("#TotVoucherAmount").val(parseFloat($("#TotVoucherAmount").val()) - parseFloat($("#TotalContractAmount").val()) + parseFloat($("#TotalPaymentAmount").val()) + parseFloat($("#PendAMT").val()))
    var isGSTReverse = $("#BBEQ_isGSTReverse").is(":checked");
    if (isGSTReverse) {
        $("#WVH_NETAMT").val(rounditn(parseFloat($("#StaxOnAmount").val()) - parseFloat($("#TDSAmount").val()), 2));
    }

    var totAMT1 = 0;
    $(".clsDocketCheck").each(function () {
        // console.log('PA1');

        var checked = $(this).is(":checked");
        var ContractAmtId = $(this).attr("id").replace("IsEnabled", "Commision");
        var DocketType = $(this).attr("id").replace("IsEnabled", "DocketType");

        if (checked) {
            if (($("#" + DocketType).val() == "Booking") || ($("#" + DocketType).val() == "Delivery") || ($("#" + DocketType).val() == "IDTDelivery")) {
                totAMT1 = totAMT1 + parseFloat($("#" + ContractAmtId).val());
            }
        }
        $("#TDSOnAmount").val(rounditn(totAMT1, 0));

    });
}
var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input

            rules: {
                'WVH.STaxRegNo': {
                    required: function () {
                        return $('#WVH_IsStaxEnabled').is(':checked');
                    }
                },
                'WVH.PANNO': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
                'WVH.tdsacccode': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
                'WVH.TDSRATE': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
            },

            messages: {
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {

        $('#btnSubmit').click(function () {

            var form = $('#form_sample');
            $(".help-block").remove();

            if (form.valid() == false) {
                return false;
            }
            App.blockUI({ boxed: true });
            document.forms["form_sample"].submit();
        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0, totAMT1 = 0; totAMT2 = 0;
        var totCheckedCount = 0;
        $(".clsDocketCheck").each(function () {

            var checked = $(this).is(":checked");
            var ContractAmtId = $(this).attr("id").replace("IsEnabled", "Commision");
            var DocketType = $(this).attr("id").replace("IsEnabled", "DocketType");
            var TdsAmt = parseFloat($("#TDSAmount").val());
            //var ContractAmtId = $(this).attr("id").replace("IsEnabled", "ContractAmt");
            //var ODAChargeId = $(this).attr("id").replace("IsEnabled", "ODACharge");
            //var FTLChargeAmountId = $(this).attr("id").replace("IsEnabled", "FTLChargeAmount");
            if (checked) {

                if (($("#" + DocketType).val() == "Booking") || ($("#" + DocketType).val() == "Delivery") || ($("#" + DocketType).val() == "IDTDelivery")) {
                    totAMT1 = totAMT1 + parseFloat($("#" + ContractAmtId).val());

                }
                else {
                    totAMT2 = totAMT2 + parseFloat($("#" + ContractAmtId).val());
                }

                totAMT = totAMT2 - totAMT1 + TdsAmt;
                totCheckedCount = totCheckedCount + 1;
            }


          
            $(".TR_" + $("#" + DocketType).val()).each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });
        });
        $("#TotalCount").text(totCheckedCount);

     //   $("#WVH_ContactCommission1").val(rounditn(totAMT, 0));
      //  $("#WVH_ContactCommission2").val(rounditn(totAMT, 0));
        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        $("#WVH_NETAMT").val(rounditn(totAMT, 0));
        CalculateStaxTDS();
    }

    var SelectAllCheckBox = function (DomainName) {

        $("#IsAllEnabled").live('change', function () {

            var checked = jQuery(this).is(":checked");
            $(".clsDocketCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });
            SelectCheckBox(DomainName);
        });

        $(".clsDocketCheck").live('change', function () {
            SelectCheckBox(DomainName);
        });
    }

  


    return {
        //main function to initiate the module
        init: function (DomainName) {
            $('#IsAllEnabled').prop('checked', true);
            $('#IsAllEnabledCnote').prop('checked', true);
            ControlDate();
            StaxTDSEnabled(DomainName);

            Formvalidate(DomainName);
            SubmitClick(DomainName);
           // SelectAllCheckBox(DomainName);
            if ($("#IsAllEnabled").val() == "true") {
                SelectAllCheckBox(DomainName);
                $("#IsAllEnabled").trigger('change');

            }
           

            var StrURL = DomainName + "/Payment/GetTDSLedger";
            FillDropDownfromOther("TDSAcccode", StrURL, "Billing Type");

            if ($("#CurrFinYear").val() != $("#BaseFinYear").val()) {
                $(".clsDateControl").datepicker('remove');
                $(".clsDateControl").datepicker({
                    rtl: App.isRTL(),
                    format: "dd M yyyy",
                    showMeridian: true,
                    autoclose: true,
                    pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                    startDate: new Date($("#FinYearEndDate").val()),
                    endDate: new Date($("#FinYearEndDate").val()),
                    todayBtn: false
                });
            }

            $(document).on('change', '#WVH_BILLDT,#WVH_DueDays', function () {
                var days = parseInt($('#WVH_DueDays').val());
                var strDT = new Date($('#WVH_BILLDT').val());
                strDT.setDate(strDT.getDate() + days);
                $("#WVH_DUEDT").val($.datepicker.formatDate('dd M yy', new Date(strDT)));
            });

            if (parseFloat($('#WVH_NETAMT').val()) >= "0.00") {
                $("#WVH_BillPayType").val("R");
            }
            else {
                $("#WVH_BillPayType").val("P");
            }

            //$("#BBEQ_isGSTReverse").on("change", function () {
            //    CalculateStaxTDS();
            //});

            //$("#uniform-BBEQ_isGSTReverse").addClass("hide");
            //CalculateStaxTDS(DomainName);
        }
    }
}();