﻿
var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        jQuery.validator.addMethod("FutureDateValidate", function (value, element) {

            var PermitDate = value;

            var date = $('#datepicker').datepicker('getDate');
            var now = new Date();
            var myDate = new Date(ChangeInJQDate(PermitDate));
            //alert("myDate " + new Date(myDateFormatter(PermitDate)) + " now " + now + " value  " + new Date(value));
            return this.optional(element) || new Date(myDateFormatter(PermitDate)) < now;

            // else alert('Das passt nicht!' + mD +  '   ' + nowdate);

        });

        $.validator.addMethod('checkDKTAvailability', function (value) {

            //   $("#WMD_GCType").

            if ($("#DOCTYP").val() == "CMP") {
                return true;
            }
            else if ($("#DOCTYP").val() == "DKT") {
                var StrURL = DomainName + "/Operation/GetGCStatusDetails?dockno=" + $("#WMD_DOCKNO").val().trim();
                // alert("hello " + StrURL);
                var result1 = $.ajax({
                    async: false,
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: StrURL,
                    dataType: 'json',
                    data: {},
                });

                var d1 = JSON.parse(result1.responseText);

                $("#DKTError").val(d1.MSG);

                if (d1.Flg == "1") {
                    return true;
                }
                else {

                    return false;
                }
            }
        }, $("#DKTCalledAs").val() + " is Not Serial Wise or Duplicate " + $("#DKTCalledAs").val() + " !!!");

        $.validator.addMethod('checkChrgwt', function (value) {

            var CFTWeightType = $("#CFTWeightType").val();
            var CHRGWT = $("#WMD_CHRGWT").val();
            var ACTUWT = $("#WMD_ACTUWT").val();
            var CFTTOT = $("#WMD_CFTTOT").val();

            var IsVolumetric = $('#WMD_IsVolumetric').is(':checked');
            $("#ChrgErrorMsg").val("");
            if (IsVolumetric) {
                if (CFTWeightType == "A") {

                    if (parseFloat(CHRGWT) < parseFloat(ACTUWT)) {
                        //alert("Charged Weight Must Not Less than Actual Weight.");       
                        $("#ChrgErrorMsg").val("Charged Weight Must Not Less than Actual Weight.");
                        return false;
                    }

                } else if (CFTWeightType == "V") {
                    if (parseFloat(CHRGWT) < parseFloat(CFTTOT)) {
                        //alert("Charged Weight Must Not Less than CFT Weight.");
                        $("#ChrgErrorMsg").val("Charged Weight Must Not Less than CFT Weight.");
                        return false;
                    }
                } else if (CFTWeightType == "H") {
                    if (parseFloat(CHRGWT) < parseFloat(ACTUWT) || parseFloat(CHRGWT) < parseFloat(CFTTOT)) {
                        // alert("Charged Weight Must be Higher than CFT Weight and Actual Weight.");
                        $("#ChrgErrorMsg").val("Charged Weight Must be Higher than CFT Weight and Actual Weight.");
                        return false;
                    }
                }
                else {
                    return true;
                }
            } else {
                if (parseFloat(CHRGWT) < parseFloat(ACTUWT)) {
                    //    alert("Hello");
                    // alert("Charged Weight Must Not Less than Actual Weight.");
                    $("#ChrgErrorMsg").val("Charged Weight Must Not Less than Actual Weight.");
                    return false;
                } else {
                    return true;
                }
            }
        }, $("#ChrgErrorMsg").val());




        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input


            rules: {
                'WMD.CHRGWT': {
                    required: function () {

                        var doctype = "DKT";
                        if ($("#WMD_GCType").val() == "N")
                            return true;
                        else if ($("#WMD_GCType").val() == "Y") {
                            var doctype = $('input[name=BillingPartyAS]:checked').attr("id");
                            if (doctype == "DKT")
                                return true;
                            else
                                return false;
                        }
                        else if ($("#WMD_GCType").val() == "C") {
                            return false;
                        }
                    },
                    //min: function () {


                    //},
                    checkChrgwt: true,
                },
                'WMD.DOCKDT': {
                    FutureDateValidate: true,

                },
                'WMD.CFTWtRatio': {
                    min: function () {
                        var IsVolumetric = $('#WMD_IsVolumetric').is(':checked');
                        if (IsVolumetric) {
                            return 4.5;
                        }
                    },
                },
                'WMD.PKGSNO': {
                    min: 1,

                },
                'WMD.ACTUWT': {
                    min: 1,

                },
                'WMD.DOCKNO': {
                    checkDKTAvailability: true,

                },

                //'CTH.CloseKM': {
                //    number: true,
                //    min: function () {

                //        return parseFloat($("#CTH_OpenKM").val()) + 1;
                //    }
                //},
                //'CTH.TotalManifest': {
                //    number: true,
                //    min: function () {

                //        var MFTOT = 0;


                //        if ($('#CTH_IsEmpty').is(':checked')) {
                //            MFTOT = 0
                //        }
                //        else
                //            MFTOT = 1;

                //        return MFTOT;
                //    }
                //},



            },
            messages: {
                'WMD.DOCKNO': {
                    checkDKTAvailability: function () {
                        return $("#DKTError").val()
                    }


                },
                'WMD.CHRGWT': {
                    checkChrgwt: function () {
                        return $("#ChrgErrorMsg").val()
                    }

                },
                'WMD.DOCKDT': {
                    FutureDateValidate: jQuery.format("Future Date is not Allowed."),

                },
                //'CTH.TotalManifest': {
                //    min: jQuery.format("Please Select at Least One Manifest")
                //},
                //'CTH.TotalDockets': {
                //    min: jQuery.format("Please Select at Least One Record")
                //}
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                for (var i = 0; i < validator.errorList.length; i++) {
                    console.log(validator.errorList[i]);
                }

                //validator.errorMap is an object mapping input names -> error messages
                for (var i in validator.errorMap) {
                    console.log(i, ":", validator.errorMap[i]);
                }
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {

        $('#btnSubmit').click(function () {

            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }
            App.blockUI({ boxed: true });
            $(".ftdate-picker input").prop('disabled', false);
            document.forms["form_sample"].submit();
        });
    }

    var PaybasChange = function (DomainName) {

        $("#WMD_PAYBAS").change(function () {

            GetPartyCode("WMD_PARTY_CODE", $("#WMD_ORGNCD").val(), $("#WMD_PAYBAS").val());
            GetContacrtDetails(DomainName);

            // GetFreightContractDetails(DomainName);

            //$("#WMDC_FREIGHT").attr("min", "150");
            //$("#WMDC_FREIGHT_CALC").attr("min", "1");

            if (($("#WMD_PAYBAS").val()).toUpperCase() !== "P04") {
                if (($("#WMD_PAYBAS").val()).toUpperCase() == "P01" || ($("#WMD_PAYBAS").val()).toUpperCase() == "P03") {
                    if ($("#WMD_TRN_MOD").val() == "1") // Air
                    {
                        $("#WMDC_FREIGHT").attr("min", "1000");
                    }
                    else if ($("#WMD_TRN_MOD").val() == "3") { // Train
                        $("#WMDC_FREIGHT").attr("min", "800");
                    }
                    else if ($("#WMD_TRN_MOD").val() == "4") { // Express
                        $("#WMDC_FREIGHT").attr("min", "400");
                    }
                }
                else {
                    $("#WMDC_FREIGHT").attr("min", "1");
                }
                $("#WMDC_FREIGHT_CALC").attr("min", "1");
                $("#WMD_PARTY_CODE").attr("readonly", false);
                $(".clsBillingPartyAS").attr("disabled", false);
                $('.clsVehicle1').hide();
                $(".ftdate-picker input").prop('disabled', true);
                $("#WMD_PARTY_CODE").select2("data", null);
                $("#WMD_party_name").val("");
                $("#DocketChargesList_UCHG06__ChargeAmount").val("100.00");
                $('.clsROFixed').val("");
                $("#WMD_CSGNNM").val("");
            }
            else {

                $("#WMDC_FREIGHT").attr("min", "0");
                $("#WMDC_FREIGHT_CALC").attr("min", "0");
                $("#WMD_PARTY_CODE").select2("data", { id: "C001020028", text: "C001020028" });
                $("#WMD_party_name").val("RCPL LOGISTICS PVT. LTD.");
                $("#WMD_PARTY_CODE").attr("readonly", true);
                $(".clsBillingPartyAS").attr("disabled", true);
                $("#DocketChargesList_UCHG06__ChargeAmount").val("0.00");
                $('.clsVehicle1').show();
                //$('.clsROFixed').val("");
                $(".ftdate-picker input").prop('disabled', false);

                GetContacrtDetails(DomainName);

                BillingPartyAS(DomainName);
            }
        });

        $("#WMD_Service_Class").change(function () {
            GetBaseCode1();
            GetFreightContractDetails(DomainName);
            ClaculateSubtotal(DomainName);
        });

        $("#WMD_businesstype").change(function () {
            GetBaseCode1();
        });

        $("#WMD_PRODCD").change(function () {
            GetBaseCode2();
        });

        $("#WMD_PKGSTY").change(function () {
            GetBaseCode2();
        });

        $("#WMD_ftl_types").change(function () {
            GetFreightContractDetails(DomainName);
        });

        $("#WMD_from_loc").change(function () {
            GetFreightContractDetails(DomainName);
        });

        $("#WMD_to_loc").change(function () {
            GetFreightContractDetails(DomainName);
        });

        $("#WMD_TRN_MOD").change(function () {
            GetContacrtChargesDetails(DomainName);
            GetFreightContractDetails(DomainName);
            ClaculateSubtotal(DomainName);
            if (($("#WMD_PAYBAS").val()).toUpperCase() !== "P04") {
                if (($("#WMD_PAYBAS").val()).toUpperCase() == "P01" || ($("#WMD_PAYBAS").val()).toUpperCase() == "P03") {
                    if ($("#WMD_TRN_MOD").val() == "1") // Air
                    {
                        $("#WMDC_FREIGHT").attr("min", "1000");
                    }
                    else if ($("#WMD_TRN_MOD").val() == "3") { // Train
                        $("#WMDC_FREIGHT").attr("min", "800");
                    }
                    else if ($("#WMD_TRN_MOD").val() == "4") { // Express
                        $("#WMDC_FREIGHT").attr("min", "400");
                    }
                }
                else {
                    $("#WMDC_FREIGHT").attr("min", "1");
                    $("#WMDC_FREIGHT_CALC").attr("min", "1");
                }
            }
            else {
                $("#WMDC_FREIGHT").attr("min", "0");
                $("#WMDC_FREIGHT_CALC").attr("min", "0");
            }
        });

        $("#WMD_stax_paidby").change(function () {

            ClaculateSubtotal(DomainName);
        });

        $("#WMD_DESTCD").change(function () {
            GetContacrtDetails(DomainName);
            //GetFreightContractDetails(DomainName);
        });

        $('.clsEntryTYP').live('click', function () {
            //  alert(this.value);
            $("#DOCTYP").val(this.value);
            GetContacrtDetails(DomainName);
        });
    }

    var PageLoadJS = function (DomainName) {
        var StrURL = DomainName + "/Operation/GetStep1Details";
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL,
            dataType: 'json',
            data: {},
            success: function (data) {

                //$(".clsEntryType").hide();

                //  alert(data.FLAG_COMPUTERISED);

                if (data.FLAG_COMPUTERISED == "N") {
                    $(".clsEntryType").hide();
                    $("#WMD_DOCKNO").attr("readonly", false);
                    $("#WMD_DOCKNO").attr("placeholder", $("#DKTCalledAs").val() + " No.");
                }
                else if (data.FLAG_COMPUTERISED == "Y") {
                    $(".clsEntryType").show();
                    $("#WMD_DOCKNO").attr("readonly", true);
                    $("#WMD_DOCKNO").attr("placeholder", "System Generated");
                }
                else if (data.FLAG_COMPUTERISED == "C") {
                    $(".clsEntryType").hide();
                    $("#WMD_DOCKNO").attr("readonly", true);
                    $("#WMD_DOCKNO").attr("placeholder", "System Generated");
                }
                $("#WMD_GCType").val(data.FLAG_COMPUTERISED);

                var doctype = "DKT";
                if ($("#WMD_GCType").val() == "N")
                    doctype = "DKT";
                else if ($("#WMD_GCType").val() == "Y") {

                    var doctype = $('input[name=DKTEntryType]:checked').attr("id");
                }
                else if ($("#WMD_GCType").val() == "C") {
                    doctype = "CMP";
                }

                $("#DOCTYP").val(doctype);

                $("#WMD_FlagRoundOff").val(data.FLAG_ROUND);

                //alert(data.DATE_RULE)

                var StrURL1 = DomainName + "/Operation/GetBookDateRule?dbdaterule=" + data.DATE_RULE;
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: StrURL1,
                    dataType: 'json',
                    data: {},
                    success: function (data1) {

                        // alert(data1.daterule + "  " + data1.fromDate + "  " + data1.todate);

                        handleDateTimePicker(new Date(data1.fromDate), new Date(data1.todate));
                    },
                    error: function () {
                        alert("error");
                    }
                });
            },
            error: function () {
                alert("error");
            }
        });

        $('.clsEntryTYP').live('click', function () {

            if ($('input[class=clsEntryTYP]:checked').val() == "DKT") {
                $("#WMD_DOCKNO").attr("readonly", false);
                $("#WMD_DOCKNO").attr("placeholder", $("#DKTCalledAs").val() + " No.");
            } else if ($('input[class=clsEntryTYP]:checked').val() == "CMP") {
                $("#WMD_DOCKNO").attr("readonly", true);
                $("#WMD_DOCKNO").attr("placeholder", "System Generated");
            }
        });

    }

    var GetFuelChargesDetails = function (DomainName) {

        var FuelRateType = $("#WMD_FuelRateType").val();
        var FuelRate = $("#WMD_FuelRate").val();
        var MinFuleCharge = $("#WMD_MinFuleCharge").val();
        var MaxFuelCharge = $("#WMD_MaxFuelCharge").val();

        var fuelsurcharge = 0.00;

        var CHRGWT = $("#WMD_CHRGWT").val();
        var FREIGHT = $("#WMDC_FREIGHT").val();

        //alert(fuelsurcharge + " -- " + FuelRateType);
        switch (FuelRateType) {
            case "%":
                fuelsurcharge = parseFloat(FREIGHT) * parseFloat(FuelRate) / 100;
                break;
            case "W":
                fuelsurcharge = parseFloat(CHRGWT) * parseFloat(FuelRate) / 100;
                break;
            case "F":
                fuelsurcharge = parseFloat(FuelRate);
                break;
        }

        if (fuelsurcharge < parseFloat(MinFuleCharge))
            fuelsurcharge = parseFloat(MinFuleCharge);

        if (fuelsurcharge > parseFloat(MaxFuelCharge))
            fuelsurcharge = parseFloat(MaxFuelCharge);
        // alert(fuelsurcharge + " -- " + FuelRateType);

        $("#DocketChargesList_SCHG20__ChargeAmount").val(fuelsurcharge);

    }
    var GetContacrtChargesDetails = function (DomainName) {

        var ContractId = $("#WMD_ContractId").val();
        var TransType = $("#WMD_TRN_MOD").val();
        var Paybas = $("#WMD_PAYBAS").val();

        if (TransType == null)
            TransType = "";
        if (Paybas == null)
            Paybas = "";

        //  alert(TransType + "  " + ContractId);

        if (TransType != "" && Paybas != "") {
            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetContractServiceChargessDetails?ContractId=" + ContractId + "&TransType=" + TransType + "&Paybas=" + Paybas;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //  alert(data1.CONTRACTID + "  " + data1.TransMode + "  " + data1.ServiceType);

                    $("#WMD_VolMeasureType").val(data1.cft_measure);
                    $("#WMD_CFTWtRatio").val(data1.cft_ratio);
                    $("#WMD_MinFreightBas").val(data1.MinFreightBas);
                    $("#WMD_MinFreightBasRate").val(data1.MinFreightBasRate);
                    $("#WMD_SubTotalUpperLimit").val(data1.SubTotalUpperLimit);
                    $("#WMD_SubTotalLowerLimit").val(data1.SubTotalLowerLimit);
                    $("#WMD_MinSubtotal").val(data1.MinSubtotal);
                    $("#WMD_FreightRateUpperLimit").val(data1.FreightRateUpperLimit);
                    $("#WMD_FreightRateLowerLimit").val(data1.FreightRateLowerLimit);
                    $("#WMD_MinFreightRate").val(data1.MinFreightRate);
                    $("#WMD_FuelRateType").val(data1.FuelRateType);
                    $("#WMD_FuelRate").val(data1.FuelRate);
                    $("#WMD_MinFuleCharge").val(data1.MinFuleCharge);
                    $("#WMD_MaxFuelCharge").val(data1.MaxFuelCharge);

                    var STPAIDBY = data1.stax_paidby_opts;

                    if (STPAIDBY == null && STPAIDBY == "")
                        STPAIDBY = "0";

                    //  alert(data1.stax_paidby + "  " + data1.stax_paidby_opts);

                    var StrURL = DomainName + "/Operation/GetStaxPaidBy/" + STPAIDBY;
                    //  alert(StrURL);

                    //  alert(STPAIDBY.replace(",", ""))

                    var n = data1.stax_paidby_opts.indexOf(data1.stax_paidby);

                    if (n > 0)
                        FillDropDownfromOtherSelectFirst("WMD_stax_paidby", StrURL, " Type", data1.stax_paidby);
                    else
                        FillDropDownfromOtherSelectFirst("WMD_stax_paidby", StrURL, " Type", "T");
                    //$("#WMD_VolMeasureType").val(data1.cft_measure);
                    //$("#WMD_CFTWtRatio").val(data1.cft_ratio);

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });
        }

    }

    var GetContacrtDetails = function (DomainName) {

        var doctype = "DKT";
        if ($("#WMD_GCType").val() == "N")
            doctype = "DKT";
        else if ($("#WMD_GCType").val() == "Y") {

            var doctype = $('input[name=DKTEntryType]:checked').attr("id");
        }
        else if ($("#WMD_GCType").val() == "C") {
            doctype = "CMP";
        }

        $("#DOCTYP").val(doctype);

        var PartyCode = $("#WMD_PARTY_CODE").val();
        var Destination = $("#WMD_DESTCD").val();
        var Paybas = $("#WMD_PAYBAS").val();
        var DOCKDT = $("#WMD_DOCKDT").val();

        //  alert(PartyCode + "  " + Destination + "  " + Paybas + "  " + DOCKDT);

        if (PartyCode != "" && Destination != "" && Paybas != "" && DOCKDT != "") {

            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetStep2Details?PartyCode=" + PartyCode + "&Paybas=" + Paybas + "&Destination=" + Destination + "&doctype=" + doctype + "&DOCKDT=" + DOCKDT;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //  alert(data1.CONTRACTID + "  " + data1.TransMode + "  " + data1.ServiceType);

                    $("#WMD_from_loc").val(data1.FCITY);
                    $("#WMD_to_loc").val(data1.TCITY);


                    $("#WMD_CFTWeightType").val(data1.cft_weight_type);
                    $("#WMD_ContractId").val(data1.CONTRACTID);
                    $("#WMD_CFTWeightType").val(data1.CFTWeightType);

                    $("#WMD_ChargeSubRule").val(data1.ChargeBas);
                    $("#WMD_MinFreightType").val(data1.MinFreightType);
                    $("#WMD_FLAG_Freight").val(data1.FLAG_Freight);
                    $("#WMD_FLAG_Subtotal").val(data1.FLAG_Subtotal);


                    $("#WMD_CODRateType").val(data1.CODRateType);
                    $("#WMD_Min_CODCharged").val(data1.Min_CODCharged);
                    $("#WMD_CODCharged").val(data1.CODCharged);
                    $("#WMD_Codchrg").val(data1.CODCharged);


                    $("#WMD_DACCRateType").val(data1.DACCRateType);
                    $("#WMD_DACCCharged").val(data1.DACCCharged);
                    $("#WMD_Min_DACCCharged").val(data1.Min_DACCCharged);
                    $("#WMD_DKTDACCCharges").val(data1.DACCCharged);

                    $("#WMD_fincmplbr").val(data1.BillingLocation);

                    if (($("#WMD_PAYBAS").val()).toUpperCase() != "P02") {
                        var StrURL = DomainName + "/Operation/GetDestinationLocations";
                        JsonDDLSelectCode("WMD_fincmplbr", StrURL);


                        if (($("#WMD_PAYBAS").val()).toUpperCase() == "P03") {
                            $("#WMD_fincmplbr").select2("data", { id: $("#WMD_DESTCD").val(), text: $("#WMD_DESTCD").val() });
                        }
                        else {
                            $("#WMD_fincmplbr").select2("data", { id: $("#WMD_ORGNCD").val(), text: $("#WMD_ORGNCD").val() });
                        }

                        // $("#WMD_fincmplbr").select2("data", null);
                    }
                    else {

                        $("#WMD_fincmplbr").select2("data", { id: data1.BillingLocation, text: data1.BillingLocation });
                    }




                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/TRN?CodeList=" + data1.TransMode;
                    // alert(StrURL)
                    FillDropDownfromOtherSelectFirst("WMD_TRN_MOD", StrURL, " Type");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/SVCTYP?CodeList=" + data1.ServiceType;
                    FillDropDownfromOtherSelectFirst("WMD_Service_Class", StrURL, " Type");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/PKPDL?CodeList=" + data1.PKGDelyType;
                    FillDropDownfromOtherSelectFirst("WMD_Pickup_Dely", StrURL, " Type");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/PKGS?CodeList=";
                    FillDropDownfromOtherSelectFirst("WMD_PKGSTY", StrURL, " Type");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/PROD?CodeList=";
                    FillDropDownfromOtherSelectFirst("WMD_PRODCD", StrURL, " Type");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/FTLTYP?CodeList=" + data1.FTLType;
                    FillDropDownfromOtherSelectFirst("WMD_ftl_types", StrURL, " Type", "31");

                    var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/BUT?CodeList=";
                    FillDropDownfromOtherSelectFirst("WMD_businesstype", StrURL, " Type");

                    $("#WMD_IsVolumetric").attr("disabled", true);
                    $("#WMD_IsVolumetric").attr("checked", false);
                    $("#WMD_IsVolumetric").parents('span').removeClass("checked");
                    $(".clsVolumetric").hide();

                    if (data1.IsVolumentric == "Y") {
                        $("#WMD_IsVolumetric").attr("disabled", false);
                        $("#WMD_IsVolumetric").attr("checked", true);
                        $("#WMD_IsVolumetric").parents('span').addClass("checked");
                        $(".clsVolumetric").show();
                    }



                    if (data1.IsCODDOD == "Y") {
                        $("#WMD_IsCODDOD").attr("disabled", false);
                    }
                    else {
                        $("#WMD_IsCODDOD").attr("disabled", true);
                        $("#WMD_IsCODDOD").attr("checked", false);
                        $("#WMD_IsCODDOD").parents('span').removeClass("checked");
                    }


                    $("#RiskType2").attr("checked", true);
                    $("#RiskType2").parents('span').addClass("checked");
                    $("#RiskType1").attr("checked", false);
                    $("#RiskType1").parents('span').removeClass("checked");

                    $("#WMD_insuyn").val(data1.Risktype);
                    if (data1.Risktype == "C") {
                        $("#RiskType1").attr("checked", true);
                        $("#RiskType1").parents('span').addClass("checked");
                        $("#RiskType2").attr("checked", false);
                        $("#RiskType2").parents('span').removeClass("checked");
                    }



                    setTimeout(
       function () {

           GetContacrtChargesDetails(DomainName);

           GetRuleDetails(DomainName, "WMD_ContractDepth", $("#WMD_PAYBAS").val() + "DEPTH", $("#WMD_PAYBAS").val());
           GetRuleDetails(DomainName, "WMD_FlagProceed", $("#WMD_PAYBAS").val() + "PROCEED", $("#WMD_PAYBAS").val());

           GetBaseCode1();
           GetBaseCode2();

           GetFreightContractDetails(DomainName);
           GetFovContractDetailsDetails(DomainName);

           GetProRataCharge(DomainName);

           if (($("#WMD_PAYBAS").val()).toUpperCase() !== "P04") {
               if (($("#WMD_PAYBAS").val()).toUpperCase() == "P01" || ($("#WMD_PAYBAS").val()).toUpperCase() == "P03") {
                   // alert($("#WMD_PAYBAS").val() + "   " + $("#WMD_TRN_MOD").val())
                   if ($("#WMD_TRN_MOD").val() == "1") // Air
                   {
                       $("#WMDC_FREIGHT").attr("min", "1000");
                   }
                   else if ($("#WMD_TRN_MOD").val() == "3") { // Train
                       $("#WMDC_FREIGHT").attr("min", "800");
                   }
                   else if ($("#WMD_TRN_MOD").val() == "4") { // Express
                       $("#WMDC_FREIGHT").attr("min", "400");
                   }
               }
               else {
                   $("#WMDC_FREIGHT").attr("min", "1");
                   $("#WMDC_FREIGHT_CALC").attr("min", "1");
               }
           }
           else {
               $("#WMDC_FREIGHT").attr("min", "0");
               $("#WMDC_FREIGHT_CALC").attr("min", "0");
           }
       }, 500);
                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });

        }
    }

    var GetProRataCharge = function (DomainName) {



        var FLAG_PRORATA = $("#WMD_FLAG_PRORATA").val();
        var RateType = $("#WMDC_RATE_TYPE").val();
        var CHRGWT = $("#WMD_CHRGWT").val();
        var FREIGHT = $("#WMDC_FREIGHT").val();
        var FTLType = $("#WMD_ftl_types").val();
        var ServiceType = $("#WMD_Service_Class").val();
        var FREIGHTRate = $("#FREIGHT_CALC").val();


        // alert(FREIGHT);

        if (FLAG_PRORATA != "" && RateType != "" && FTLType != "" && ServiceType != "" && parseFloat(CHRGWT) > 0) {

            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetProRataCharge?FLAG_PRORATA=" + FLAG_PRORATA + "&RateType=" + RateType + "&FTLType=" + FTLType + "&ServiceType=" + ServiceType + "&CHRGWT=" + CHRGWT + "&FREIGHT=" + FREIGHT + "&FREIGHTRate=" + FREIGHTRate;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    // alert(data1.FreightCharge + "  " + data1.FreightCharge + "  " + data1.FreightRate);
                    FreightCharge = rounditn(data1.FreightCharge, 2);
                    FreightRate = rounditn(data1.FreightRate, 2);
                    $("#WMDC_FREIGHT").val(data1.FreightCharge);
                    $("#FREIGHT_CALC").val(data1.FreightRate);

                    GetFuelChargesDetails();

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });

        }
    }

    var GetFreightContractDetails = function (DomainName) {

        //alert("Frt");

        var ChargeRule = $("#WMD_ChargeRule").val();
        var BaseCode1 = $("#WMD_BaseCode1").val();
        var ChargeSubRule = $("#WMD_ChargeSubRule").val();
        var BaseCode2 = $("#WMD_BaseCode2").val();
        var ChargedWeight = $("#WMD_CHRGWT").val();
        var ContractID = $("#WMD_ContractId").val();
        var Destination = $("#WMD_DESTCD").val();
        var Depth = $("#WMD_ContractDepth").val();
        var FlagProceed = $("#WMD_FlagProceed").val();
        var FromCity = $("#WMD_from_loc").val();
        var FTLType = $("#WMD_ftl_types").val();
        var NoOfPkgs = $("#WMD_PKGSNO").val();

        var ToCity = $("#WMD_to_loc").val();
        var ServiceType = $("#WMD_Service_Class").val();
        var Origin = $("#WMD_ORGNCD").val();


        var PayBase = $("#WMD_PAYBAS").val();
        var TransMode = $("#WMD_TRN_MOD").val();
        var OrderID = $("#WMD_ContractId").val();
        var InvAmt = $("#WMD_DECVAL").val();
        var DOCKDT = $("#WMD_DOCKDT").val();

        var ChargedWeright = $("#WMD_CHRGWT").val();
        $("#WMD_CHRGWT").val($("#WMD_ACTUWT").val());
        //alert(TransMode);

        if (ServiceType != "" && Destination != "" && FromCity != "" && FTLType != "" && parseFloat(NoOfPkgs) > 0 && ToCity != "" && PayBase != ""
            && TransMode != "" && parseFloat(InvAmt) > 0 && parseFloat(ChargedWeight) > 0) {
            // alert("FrtChrg")
            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetFreightContractDetails?ChargeRule=" + ChargeRule + "&BaseCode1=" + BaseCode1 + "&ChargeSubRule=" + ChargeSubRule + "&BaseCode2=" + BaseCode2 + "&ChargedWeight=" + ChargedWeight
             + "&ContractID=" + ContractID + "&ChargedWeight=" + ChargedWeight + "&Destination=" + Destination + "&Depth=" + Depth + "&FlagProceed=" + FlagProceed
             + "&FromCity=" + FromCity + "&FTLType=" + FTLType + "&NoOfPkgs=" + NoOfPkgs + "&Origin=" + Origin + "&PayBase=" + PayBase + "&ChargedWeright=" + ChargedWeright
            + "&ServiceType=" + ServiceType + "&ToCity=" + ToCity + "&TransMode=" + TransMode + "&OrderID=" + OrderID + "&InvAmt=" + InvAmt + "&DOCKDT=" + DOCKDT;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //  alert(data1.CONTRACTID + " --- " + data1.TransMode + "  " + data1.ServiceType);
                    // alert(data1.FreightCharge + " r " + data1.FreightRate + " r " + data1.RateType + " r " + data1.TRDays);
                    FreightCharge = rounditn(data1.FreightCharge, 2);
                    FreightRate = rounditn(data1.FreightRate, 2);
                    $("#WMDC_FREIGHT").val(data1.FreightCharge);
                    $("#WMDC_FREIGHT_CALC").val(data1.FreightRate);


                    $("#WMD_CHRGWT").val(data1.ChargedWeight);
                    //if (data1.FlgChrgWTDisabled == "Y")
                    //    $("#WMD_CHRGWT").attr("readonly", true);
                    //else
                    $("#WMD_CHRGWT").attr("readonly", false);

                    $(".clsContractMessage").html(data1.ContractMessage);


                    GetFuelChargesDetails();

                    if (data1.RateType != "")
                        $("#WMDC_RATE_TYPE").select2("data", { id: data1.RateType, text: data1.strRateType });

                    $("#WMD_CDELDT").val(data1.EDD);
                    // $("#WMD_CDELDT").val(data1.EDD);

                    if (data1.TRDays == "")
                        $("#WMD_TRDays").val(0);
                    else
                        $("#WMD_TRDays").val(data1.TRDays);

                    //RateType

                    GetProRataCharge(DomainName);
                    GetOtherChargesDetails(DomainName);


                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });

        }
    }

    var GetOtherChargesDetails = function (DomainName) {

        //alert("Frt");

        var ChargeRule = $("#WMD_ChargeRule").val();
        var BaseCode1 = $("#WMD_BaseCode1").val();
        var ChargeSubRule = $("#WMD_ChargeSubRule").val();
        var BaseCode2 = $("#WMD_BaseCode2").val();
        var ChargedWeight = $("#WMD_CHRGWT").val();
        var ContractID = $("#WMD_ContractId").val();
        var Destination = $("#WMD_DESTCD").val();
        var Depth = $("#WMD_ContractDepth").val();
        var FlagProceed = $("#WMD_FlagProceed").val();
        var FromCity = $("#WMD_from_loc").val();
        var FTLType = $("#WMD_ftl_types").val();
        var NoOfPkgs = $("#WMD_PKGSNO").val();

        var ToCity = $("#WMD_to_loc").val();
        var ServiceType = $("#WMD_Service_Class").val();
        var Origin = $("#WMD_ORGNCD").val();


        var PayBase = $("#WMD_PAYBAS").val();
        var TransMode = $("#WMD_TRN_MOD").val();
        var OrderID = $("#WMD_ContractId").val();
        var InvAmt = $("#WMD_DECVAL").val();
        var DOCKDT = $("#WMD_DOCKDT").val();

        var PackType = $("#WMD_PKGSTY").val();
        var ProdType = $("#WMD_PRODCD").val();



        if (ServiceType != "" && Destination != "" && FromCity != "" && FTLType != "" && ProdType != "" && PackType != "" && parseFloat(NoOfPkgs) > 0 && ToCity != "" && PayBase != ""
            && TransMode != "" && parseFloat(InvAmt) > 0 && parseFloat(ChargedWeight) > 0) {
            // alert("FrtChrg")
            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetOtherChargesDetails?ChargeRule=" + ChargeRule + "&BaseCode1=" + BaseCode1 + "&ChargeSubRule=" + ChargeSubRule + "&BaseCode2=" + BaseCode2 + "&ChargedWeight=" + ChargedWeight
             + "&ContractID=" + ContractID + "&ChargedWeight=" + ChargedWeight + "&Destination=" + Destination + "&Depth=" + Depth + "&FlagProceed=" + FlagProceed
             + "&FromCity=" + FromCity + "&FTLType=" + FTLType + "&NoOfPkgs=" + NoOfPkgs + "&Origin=" + Origin + "&PayBase=" + PayBase
            + "&ServiceType=" + ServiceType + "&ToCity=" + ToCity + "&TransMode=" + TransMode + "&OrderID=" + OrderID + "&InvAmt=" + InvAmt + "&DOCKDT=" + DOCKDT
             + "&PackType=" + PackType + "&ProdType=" + ProdType;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {


                    var IsCODDOD = $('#WMD_IsCODDOD').is(':checked');
                    var IsDACC = $('#WMD_IsDACC').is(':checked');

                     //alert(IsCODDOD + "  " + IsDACC);

                    for (i = 0; i < data1.length; i++) {

                        // alert(data1[i].Value + "  " + data1[i].Text);

                        var Chrgamt = data1[i].Text;

                        if (Chrgamt == "" || Chrgamt == null)
                            Chrgamt = 0;

                        if (IsCODDOD) {
                            if (data1[i].Value == "SCHG12")
                                Chrgamt = 0;

                            if (data1[i].Value == "SCHG12")
                                $("#WMD_Codchrg").val(data1[i].Text);
                            //alert($("#WMD_Codchrg").val())
                        }

                       

                        if (data1[i].Value == "SCHG11") {
                            if (data1[i].Text != "" && data1[i].Text != "0")
                                $("#WMDC_FOV").val(data1[i].Text);
                           // alert("SSS "+data1[i].Text);
                        }

                        if (IsDACC) {
                            if (data1[i].Value == "SCHG13")
                                Chrgamt = 0;

                            if (data1[i].Value == "SCHG13")
                                $("#WMD_DKTDACCCharges").val(data1[i].Text);
                            //alert($("#WMD_DKTDACCCharges").val())
                        }
                        

                        $("#DocketChargesList_" + data1[i].Value + "__ChargeAmount").val(Chrgamt);
                    }

                    GetFovContractDetailsDetails(DomainName);

                    GetFuelChargesDetails();

                    ClaculateSubtotal(DomainName);


                    if (PayBase == "P02" || PayBase == "P04") {
                        $("#divStep5 :input").attr("readonly", true);

                        $("#WMD_stax_paidby").attr("readonly", false);

                        if (PayBase == "P04") {
                            var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/RATETYPE";
                            FillDropDownfromOtherSelectFirst("WMDC_RATE_TYPE", StrURL, "Billing Type");
                        }
                        //$("#WMDC_FREIGHT,#WMDC_FREIGHT_CALC").attr("min", "0");

                    }
                    else {
                        $("#divStep5 :input").attr("readonly", false);

                        //$("#WMDC_FREIGHT,#WMDC_FREIGHT_CALC").attr("min", "1");
                    }





                    if (($("#WMD_PAYBAS").val()).toUpperCase() !== "P04") {
                        if (($("#WMD_PAYBAS").val()).toUpperCase() == "P01" || ($("#WMD_PAYBAS").val()).toUpperCase() == "P03") {
                            if ($("#WMD_TRN_MOD").val() == "1") // Air
                            {
                                $("#WMDC_FREIGHT").attr("min", "1000");
                            }
                            else if ($("#WMD_TRN_MOD").val() == "3") { // Train
                                $("#WMDC_FREIGHT").attr("min", "800");
                            }
                            else if ($("#WMD_TRN_MOD").val() == "4") { // Express
                                $("#WMDC_FREIGHT").attr("min", "400");
                            }
                        }
                        else {
                            $("#WMDC_FREIGHT").attr("min", "1");
                        }
                        $("#WMDC_FREIGHT_CALC").attr("min", "1");
                    }
                    else {
                        $("#WMDC_FREIGHT").attr("min", "0");
                        $("#WMDC_FREIGHT_CALC").attr("min", "0");
                    }
                    $(".clsROFixed").attr("readonly", true);

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });

        }
    }


    var GetFovContractDetailsDetails = function (DomainName) {

        //   alert("FOV")

        var ChargeRule = $("#WMD_ChargeRule").val();
        var BaseCode1 = $("#WMD_BaseCode1").val();
        var RiskType = $('input[name=RiskType]:checked').val();
        var ContractID = $("#WMD_ContractId").val();
        var InvAmt = $("#WMD_DECVAL").val();

        if (parseFloat(InvAmt) > 0) {

            //  alert("FOVChrg")
            App.blockUI({ boxed: true });


            var StrURL1 = DomainName + "/Operation/GetFovContractDetailsDetails?ChargeRule=" + ChargeRule + "&BaseCode1=" + BaseCode1 + "&RiskType=" + RiskType + "&ContractID=" + ContractID + "&InvAmt=" + InvAmt;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //  alert(data1.CONTRACTID + "  " + data1.TransMode + "  " + data1.ServiceType);
                    if (data1.FOVCharged == "")
                        data1.FOVCharged = "0";
                    if (data1.FOVRate == "")
                        data1.FOVRate = "0";

                    $("#WMDC_FOVCalculated").val(data1.FOVCharged);
                    $("#WMDC_FovRate").val(data1.FOVRate);
                    $("#WMDC_FOV").val(data1.FOVCharged);
                  //  alert("ZZZ " +$("#WMDC_FOV").val());

                    //    $("#DocketChargesList_SCHG11__ChargeAmount").val(data1.FOVCharged);
                    //  FOVRateType


                    //RateType

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });

        }
    }

    var handleDatePicker1 = function () {
        //if (jQuery().datepicker) {
        $('.date-picker').datepicker({
            isRTL: App.isRTL(),
            format: "dd/mm/yyyy",
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            todayBtn: true
        });
        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        //}
    }

    var GetCODDODDetails = function () {


        var IsCODDOD = $('#WMD_IsCODDOD').is(':checked');
        // alert(IsVolumetric)
        var InvAmt = $("#WMD_DECVAL").val();


        if (!IsCODDOD && parseFloat(InvAmt) > 0) {

            var CODRateType = $("#WMD_CODRateType").val();
            var Min_CODCharged = $("#WMD_Min_CODCharged").val();
            var CODCharged = $("#WMD_CODCharged").val();
            //   var CODRateType = $("#WMD_CODRateType").val();
            var Charged = 0;

            if (CODRateType == "%")
                Charged = parseFloat(CODCharged) * parseFloat(InvAmt) / 100

            if (parseFloat(CODCharged) < parseFloat(Min_CODCharged))
                Charged = Min_CODCharged;


              $("#WMD_Codchrg").val(Charged);

        }

    }

    var GetDACCDetails = function () {

        var IsDACC = $('#WMD_IsDACC').is(':checked');
        // alert(IsVolumetric)

        var InvAmt = $("#WMD_DECVAL").val();

        if (!IsDACC && parseFloat(InvAmt) > 0) {

            var DACCRateType = $("#WMD_DACCRateType").val();
            var Min_DACCCharged = $("#WMD_Min_DACCCharged").val();
            var DACCCharged = $("#WMD_DACCCharged").val();
            //   var CODRateType = $("#WMD_CODRateType").val();
            var Charged = 0;

            if (DACCRateType == "%")
                Charged = parseFloat(DACCCharged) * parseFloat(InvAmt) / 100

            if (parseFloat(DACCCharged) < parseFloat(Min_DACCCharged))
                Charged = Min_DACCCharged;

            $("#WMD_DKTDACCCharges").val(Charged);
        }
    }

    var AddInvoiceRows = function (DomainName, itemIndex) {
        
        $('.clsVOL').live('change', function () {
            CalculateCFT();
            GetFreightContractDetails(DomainName);
        });

        $("#btnAddRows").click(function () {
            //  alert("Hello");

            var TOTRows = $("#InvoiceRows").val();


            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;

                    AddInvoiceRow(DomainName, itemIndex);

                }, 350 * i);
            }

        });

        InvoiceDetails(1, DomainName);

    }

    var CalculateWT = function () {

        var TotalActWt = $("#WMD_ACTUWT");
        var TotalCHRGWT = $("#WMD_CHRGWT");
        // alert("hello");
        TotalActWt.val("0");
        $(".ClsActWt").each(function () {
            //alert("Hiiii");
            $(this).val();
            TotalActWt.val(parseInt(TotalActWt.val()) + parseInt($(this).val()));
            // alert(TotalActWt.val());
        });
    }

    var CalculateDecVal = function () {

        var TotalActWt = $("#WMD_DECVAL");

        TotalActWt.val("0");
        $(".ClsDecVal").each(function () {
            //alert("Hiiii");
            $(this).val();
            TotalActWt.val(parseInt(TotalActWt.val()) + parseInt($(this).val()));
            // alert(TotalActWt.val());
            //$("#WMDC_ExtraGoodsValue").val($("#decVal").val());

        });

    }

    var CalculatePkgs = function () {

        var TotalActWt = $("#WMD_PKGSNO");
        // alert("hello");
        TotalActWt.val("0");
        $(".ClsPKGSNO").each(function () {
            //alert("Hiiii");
            $(this).val();
            TotalActWt.val(parseInt(TotalActWt.val()) + parseInt($(this).val()));
            // alert(TotalActWt.val());
        });

    }

    var CalculateCFT = function () {


        var TotalActWt = $("#WMD_CFTTOT");
        // alert("hello");
        TotalActWt.val("0");

        var totCFT = 0;

        $(".ClsCFT").each(function () {
            //alert("Hiiii");

            // alert(TotalActWt.val());

            var Rid = $(this).attr("id");
            Rid = Rid.replace("DocketInvoiceList_", "").replace("__vol_cft", "");
            //alert(Rid);
            var Length = $("#DocketInvoiceList_" + Rid + "__VOL_L").val();
            var Width = $("#DocketInvoiceList_" + Rid + "__VOL_B").val();
            var Height = $("#DocketInvoiceList_" + Rid + "__VOL_H").val();
            var PKGSNO = $("#DocketInvoiceList_" + Rid + "__PKGSNO").val();
            var CubicWt = $("#DocketInvoiceList_" + Rid + "__vol_cft").val();

            totCFT = totCFT + (parseFloat(Length) * parseFloat(Width) * parseFloat(Height) * parseFloat($("#WMD_CFTWtRatio").val()) * parseInt(PKGSNO)); //

            CalculateVOLCFT(Rid);


            $(this).val();
            TotalActWt.val(parseInt(TotalActWt.val()) + parseInt($(this).val()));
        });

        $("#WMD_CFT").val(totCFT);

        CalculateCHRGWT();



    }

    var CalculateCHRGWT = function () {

        var ACTUWT = $("#WMD_ACTUWT").val();
        var CFTTOT = $("#WMD_CFTTOT").val();
        var CFTWeightType = $("#WMD_CFTWeightType").val();
        var IsVolumetric = $('#WMD_IsVolumetric').is(':checked');
        // alert(IsVolumetric)



        if (!IsVolumetric) {

            $("#WMD_CHRGWT").val(ACTUWT);
        }
        else {
            if (CFTWeightType == "A")
                $("#WMD_CHRGWT").val(ACTUWT);
            else if (CFTWeightType == "V")
                $("#WMD_CHRGWT").val(CFTTOT);
            else {

                if (parseFloat(ACTUWT) > parseFloat(CFTTOT))
                    $("#WMD_CHRGWT").val(ACTUWT);
                else
                    $("#WMD_CHRGWT").val(CFTTOT);
            }
        }


    }

    var CalculateVOLCFT = function (Rid) {

        var Length = $("#DocketInvoiceList_" + Rid + "__VOL_L").val();
        var Width = $("#DocketInvoiceList_" + Rid + "__VOL_B").val();
        var Height = $("#DocketInvoiceList_" + Rid + "__VOL_H").val();
        //var CubicWt = $("#DocketInvoiceList_" + Rid + "__vol_cft").val();

        var VolMeasureType = $("#WMD_VolMeasureType").val();
        var CFTWtRatio = $("#WMD_CFTWtRatio").val();
        var PKGSNO = $("#DocketInvoiceList_" + Rid + "__PKGSNO").val();

        if (Length == "")
            Length = 0;
        if (Width == "")
            Width = 0;
        if (Height == "")
            Height = 0;
        if (CFTWtRatio == "")
            CFTWtRatio = 0;
        if (PKGSNO == "")
            PKGSNO = 0;


        //alert(Length + " -  " + Width + " - " + Height + " - " + VolMeasureType + " - " + CFTWtRatio + " - " + PKGSNO);

        var volume = 0;

        if (VolMeasureType == "INCHES")
            volume = Length * Width * Height * parseFloat(CFTWtRatio) / 1728;
        else if (VolMeasureType == "CM")
            volume = Length * Width * Height * parseFloat(CFTWtRatio) / 27000;
        else if (VolMeasureType == "FEET")
            volume = Length * Width * Height * parseFloat(CFTWtRatio);   // if not proper value then INCHES is applied

        volume = volume * parseFloat(PKGSNO);

        var txtcft = Math.round(volume);

        $("#DocketInvoiceList_" + Rid + "__vol_cft").val(txtcft);

        // CalculateCFT();
    }

    var InvoiceDetails = function (Rid, DomainName) {

        CalculateVOLCFT(Rid);

        $("#DocketInvoiceList_" + Rid + "__ACTUWT").change(function () {
            CalculateWT();
            CalculateCFT();
            GetFreightContractDetails(DomainName);
            // CalculateCHRGWT();
        });

        $("#DocketInvoiceList_" + Rid + "__PKGSNO").change(function () {
            CalculatePkgs();
            CalculateCFT();
            GetFreightContractDetails(DomainName);
        });

        $("#DocketInvoiceList_" + Rid + "__vol_cft").change(function () {
            CalculateCFT();
            GetFreightContractDetails(DomainName);
        });

        $("#DocketInvoiceList_" + Rid + "__DECLVAL").change(function () {
            CalculateDecVal();
            CalculateCFT();
            GetFreightContractDetails(DomainName);

            GetFovContractDetailsDetails(DomainName);

            GetCODDODDetails();
            GetDACCDetails();
        });

        var date = new Date($("#WMD_DOCKDT").val());
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();
        //  var date = new Date((day  + ' ' + months[month] + ' ' + year));
        //  $("#DocketInvoiceList_" + Rid + "__INVDT").val((day + ' ' + months[month] + ' ' + year));

        var date1 = new Date($("#WMD_DOCKDT").val());
        date1.setDate(date.getDate() - 15);

        var day1 = date1.getDate();
        var month1 = date1.getMonth();
        var year1 = date1.getFullYear();

        $("#DocketInvoiceList_" + Rid + "__INVDT").attr("readonly", "true")
        handleDateTimePicker_ddmmyyyy("DocketInvoiceList_" + Rid + "__INVDT", new Date((day1 + ' ' + months[month1] + ' ' + year1)), new Date((day + ' ' + months[month] + ' ' + year)));

        $(".clsVolumetric").hide();


        $(".clsVolumetric").hide();
        if ($('#WMD_IsVolumetric').is(':checked')) {

            $(".clsVolumetric").show();
        }

        $("a.InvoicedeleteRow").live("click", function () {
            $(this).parents("tr.editorRow:first").remove();
            return false;
        });
    }

    var AddInvoiceRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Operation/InvoiceDetails/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer").append(html);

                InvoiceDetails(Rid, DomainName);

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var AllCheckBoxClick = function (DomainName) {
        $(".clsVolumetric").hide();
        $('#WMD_IsVolumetric').click(function () {

            $(".clsVolumetric").hide();
            if ($(this).is(':checked')) {

                $(".clsVolumetric").show();
            }

            CalculateCFT();

            FreightChagne();
            GetFreightContractDetails(DomainName);

        });

        $('#WMD_IsStaxExemp').click(function () {

            $("WMD_stax_exmpt_yn").val("N");
            if ($(this).is(':checked')) {

                $("WMD_stax_exmpt_yn").val("Y");
            }

            ClaculateSubtotal(DomainName);

        });

        $(".clsDACC").hide();
        $('#WMD_IsDACC').click(function () {

            $(".clsDACC").hide();
            if ($(this).is(':checked')) {

                $(".clsDACC").show();
            }

            GetDACCDetails();
            GetOtherChargesDetails(DomainName);


        });

        $(".clsCODDOD").hide();
        $('#WMD_IsCODDOD').click(function () {

            $(".clsCODDOD").hide();
            if ($(this).is(':checked')) {

                $(".clsCODDOD").show();
            }

            GetCODDODDetails();
            GetOtherChargesDetails(DomainName);

        });

        $("input[name='RiskType']").click(function () {


            var RiskType = $('input[name=RiskType]:checked').val();

            $("#WMD_insuyn").val(RiskType);

            GetFovContractDetailsDetails(DomainName);
            GetOtherChargesDetails(DomainName);

        });


    }

    var CnorEnabledDisabled = function (boo, clr) {

        //  $(".clsCnor1").attr('readonly', boo);
        $(".clsCnor").attr('readonly', boo);

        if (clr) {
            //$(".clsCnor1").attr('readonly', boo);
            $(".clsCnor").val("");
            $(".clsCnor1").select2("data", null);
        }
        //$("#RDOCnor1").attr('disabled', boo);
        //$("#RDOCnor2").attr('disabled', boo);
    }

    ///  Cnee Enabled/Disabled
    var CneeEnabledDisabled = function (boo, clr) {

        // $(".clsCnee1").attr('readonly', boo);
        $(".clsCnee").attr('readonly', boo);

        if (clr) {
            // $(".clsCnee1").attr('readonly', boo);
            $(".clsCnee").val("");
            $(".clsCnee1").select2("data", null);
        }
        //$("#WMD_CSGECD").attr('readonly', boo);
        //$("#RDOCnee1").attr('disabled', boo);
        //$("#RDOCnee2").attr('disabled', boo);
    }

    var CnorCnee = function (DomainName) {

        CnorEnabledDisabled(true);
        CneeEnabledDisabled(true);

        $("input[name='RDOCnor']").click(function () {
            // alert(this.value);
            if ($(this).attr("id") == "RDOCnor1") {
                $('#WMD_CSGNCD').attr('readonly', false);
                $('#WMD_CSGNCD').select2('data', null);
                CnorEnabledDisabled(true);
            }
            else {
                $('#WMD_CSGNCD').attr('readonly', true);
                $("#WMD_CSGNCD").select2("data", { id: "8888", text: "8888" });
                CnorEnabledDisabled(false);
            }
            $("#WMD_CSGNNM").val("");
            $("#WMD_CSGNADDR").val("");
            $("#WMD_CSGNCity").val("");
            $("#WMD_CSGNPinCode").val("");
            $("#WMD_csgnmobile").val("");
        });

        $("input[name='RDOCnee']").click(function () {
            // alert(this.value);
            if ($(this).attr("id") == "RDOCnee1") {
                $('#WMD_CSGECD').attr('readonly', false);
                $('#WMD_CSGECD').select2('data', null);
                CneeEnabledDisabled(true);
            }
            else {
                $('#WMD_CSGECD').attr('readonly', true);
                $("#WMD_CSGECD").select2("data", { id: "8888", text: "8888" });
                CneeEnabledDisabled(false);
            }
            $("#WMD_CSGENM").val("");
            $("#WMD_CSGEADDR").val("");
            $("#WMD_CSGECity").val("");
            $("#WMD_CSGEPinCode").val("");
            $("#WMD_csgemobile").val("");
        });

        GetPartyCode("WMD_CSGNCD", "", "");

        $("#WMD_CSGNCD").change(function () {

            JsGetCNorCneeDetails(DomainName, $(this).val(), "CSGN");
        });

        GetPartyCode("WMD_CSGECD", "", "");

        $("#WMD_CSGECD").change(function () {
            JsGetCNorCneeDetails(DomainName, $(this).val(), "CSGE");
        });


        $("#WMD_PARTY_CODE").change(function () {

            GetContacrtDetails(DomainName);

            BillingPartyAS(DomainName);
            if ($(this).val() != "") {
                $("#WMD_party_name").val($(this).select2('data').text);
            }

        });

        $('.clsBillingPartyAS').live('click', function () {


            BillingPartyAS(DomainName);
        });

    }

    var BillingPartyAS = function (DomainName) {
        var radioid = $('input[name=BillingPartyAS]:checked').attr("id");
        var PartyCode = $("#WMD_PARTY_CODE").val();

        $("#WMD_party_as").val(radioid);
        if (radioid == "CSGN") {
            //alert(radioid)
            CneeEnabledDisabled(true, true);
            //$(".clsCnee").attr("disabled", true);
            //$(".clsCnor").attr("disabled", false);
            $("#WMD_CSGENM").val("");
            $("#WMD_CSGEADDR").val("");
            $("#WMD_CSGECity").val("");
            $("#WMD_CSGEPinCode").val("");
            $("#WMD_csgemobile").val("");
            $('#RDOCnor1').parent().addClass("checked");
            $('#RDOCnor2').parent().removeClass("checked");


            $('#RDOCnee1').parent().removeClass("checked");
            $('#RDOCnee2').parent().addClass("checked");

            //RDOCnor1

            $('#WMD_CSGNCD').attr('readonly', false);
            $('#WMD_CSGNCD').select2('data', null);

            $('#WMD_CSGECD').attr('readonly', true);
            $("#WMD_CSGECD").select2("data", { id: "8888", text: "8888" });

            $("#WMD_CSGENM").attr('readonly', false);
            CnorEnabledDisabled(true);

            $("#WMD_CSGNNM").val("");
            $("#WMD_CSGNADDR").val("");
            $("#WMD_CSGNCity").val("");
            $("#WMD_CSGNPinCode").val("");
            $("#WMD_csgnmobile").val("");
        }
        else if (radioid == "CSGE") {

            $("#WMD_CSGNNM").val("");
            $("#WMD_CSGNADDR").val("");
            $("#WMD_CSGNCity").val("");
            $("#WMD_CSGNPinCode").val("");
            $("#WMD_csgnmobile").val("");
            $('#RDOCnor2').parent().addClass("checked");
            $('#RDOCnor1').parent().removeClass("checked");

            $('#RDOCnee2').parent().removeClass("checked");
            $('#RDOCnee1').parent().addClass("checked");

            //RDOCnor2
            $('#WMD_CSGNCD').attr('readonly', true);
            $("#WMD_CSGNCD").select2("data", { id: "8888", text: "8888" });
            CnorEnabledDisabled(false);

            $('#WMD_CSGECD').attr('readonly', false);
            $('#WMD_CSGECD').select2('data', null);
        }

        JsGetCNorCneeDetails(DomainName, PartyCode, radioid);

    }

    var JsGetCNorCneeDetails = function (DomainName, Party, PartyAs) {

        if (Party != "") {
            App.blockUI({ boxed: true });
            var StrURL = DomainName + "/Operation/GetCustomerDetails?custcd=" + Party;
            // alert("hello123     " + StrURL + PartyAs);

            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL,
                dataType: 'json',
                data: {},
                success: function (data) {

                    if (PartyAs == "CSGN") {
                        $("#WMD_CSGNNM").val(data.Name);
                        $("#WMD_CSGNADDR").val(data.Address);
                        $("#WMD_CSGNCity").val(data.City);
                        $("#WMD_csgnmobile").val(data.Mobileno);
                        //if ("@Model.WMD.CSGNPinCode" == null) {
                        $("#WMD_CSGNPinCode").val(data.Pincode);
                        //}
                        $("#WMD_CSGNCD").select2("data", { id: Party, text: data.Name });

                        /// Cnor Disabled
                        CnorEnabledDisabled(true);
                    }
                    else {
                        $("#WMD_CSGENM").val(data.Name);
                        $("#WMD_CSGEADDR").val(data.Address);
                        $("#WMD_CSGECity").val(data.City);
                        $("#WMD_CSGEPinCode").val(data.Pincode);
                        $("#WMD_csgemobile").val(data.Mobileno);
                        $("#WMD_CSGECD").select2("data", { id: Party, text: data.Name });
                        // Cnoee Disabled
                        CneeEnabledDisabled(true);
                    }

                    if ($("#WMD_PAYBAS").val() == "P04") {
                        $("#WMD_CSGNNM").val("RCPL LOGISTICS PVT. LTD.");
                    }

                    App.unblockUI();
                },
                error: function () {
                    alert("error 1111");
                }
            });
        }
    }

    var GetBaseCode1 = function () {

        var ChargeRule = $("#WMD_ChargeRule").val();
        var SVCTYP = $("#WMD_Service_Class").val();
        var BUT = $("#WMD_businesstype").val();

        if (ChargeRule == "NONE")
            $("#WMD_BaseCode1").val(ChargeRule);
        else if (ChargeRule == "SVCTYP")
            $("#WMD_BaseCode1").val(SVCTYP);
        else if (ChargeRule == "BUT")
            $("#WMD_BaseCode1").val(BUT);
    }

    var GetBaseCode2 = function () {

        var ChargeRule = $("#WMD_ChargeSubRule").val();
        var PRODCD = $("#WMD_PRODCD").val();
        var PKGSTY = $("#WMD_PKGSTY").val();

        if (ChargeRule == "NONE")
            $("#WMD_BaseCode2").val(ChargeRule);
        else if (ChargeRule == "PROD")
            $("#WMD_BaseCode2").val(PRODCD);
        else if (ChargeRule == "PKGS")
            $("#WMD_BaseCode2").val(PKGSTY);
    }

    var GetRuleDetails = function (DomainName, FLDId, Key, Paybas) {

        App.blockUI({ boxed: true });

        var StrURL1 = DomainName + "/Operation/GetRuleDetails?Key=" + Key + "&Paybas=" + Paybas;
        $.ajax({
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL1,
            dataType: 'json',
            data: {},
            success: function (data1) {

                $("#" + FLDId).val(data1.DefaultValue);

                App.unblockUI();
            },
            error: function (req, status, error) {
                alert("error--" + req + "---" + status + "---" + error);
            }
        });

    }

    var FreightRateChagne = function () {
        var Freight = $("#WMDC_FREIGHT").val();
        var FREIGHT_CALC = $("#WMDC_FREIGHT_CALC").val();
        var RATE_TYPE = $("#WMDC_RATE_TYPE").val();
        var PKGSNO = $("#WMD_PKGSNO").val();
        var CHRGWT = $("#WMD_CHRGWT").val();

        if (RATE_TYPE == "W") {
            if (parseFloat(CHRGWT) > 0 && (parseFloat(FREIGHT_CALC) > 0)) {

                //if (parseFloat(FREIGHT_CALC) == 0)
                //    FREIGHT_CALC = Freight / CHRGWT;
                if (parseFloat(FREIGHT_CALC) > 0)
                    Freight = FREIGHT_CALC * CHRGWT;
            }
        }
        else if (RATE_TYPE == "P") {
            if (parseFloat(PKGSNO) > 0 && (parseFloat(FREIGHT_CALC) > 0)) {

                if (parseFloat(FREIGHT_CALC) > 0)
                    Freight = FREIGHT_CALC * PKGSNO;

            }
        } else if (RATE_TYPE == "T") {
            if (parseFloat(CHRGWT) > 0 && (parseFloat(FREIGHT_CALC) > 0)) {
                if (parseFloat(FREIGHT_CALC) > 0)
                    Freight = FREIGHT_CALC * CHRGWT / 1000;
            }
        } else if (RATE_TYPE == "F") {

            Freight = FREIGHT_CALC;
        }
        // alert(Freight + "  " + RATE_TYPE);

        Freight = rounditn(Freight, 2);

        $("#WMDC_FREIGHT").val(Freight);



    }

    var FreightChagne = function () {
        var Freight = $("#WMDC_FREIGHT").val();
        var FREIGHT_CALC = $("#WMDC_FREIGHT_CALC").val();
        var RATE_TYPE = $("#WMDC_RATE_TYPE").val();
        var PKGSNO = $("#WMD_PKGSNO").val();
        var CHRGWT = $("#WMD_CHRGWT").val();
        if (RATE_TYPE == "W") {
            if (parseFloat(CHRGWT) > 0 && (parseFloat(Freight) > 0)) {

                if (parseFloat(Freight) > 0)
                    FREIGHT_CALC = Freight / CHRGWT;
            }
        }
        else if (RATE_TYPE == "P") {
            if (parseFloat(PKGSNO) > 0 && (parseFloat(Freight) > 0)) {


                if (parseFloat(Freight) > 0)
                    FREIGHT_CALC = Freight / PKGSNO;

            }
        } else if (RATE_TYPE == "T") {
            if (parseFloat(CHRGWT) > 0 && (parseFloat(Freight) > 0)) {


                if (parseFloat(Freight) > 0)
                    FREIGHT_CALC = Freight / (CHRGWT * 1000);

            }
        } else if (RATE_TYPE == "F") {
            if (parseFloat(Freight) > 0)
                FREIGHT_CALC = Freight;
        }

        FREIGHT_CALC = rounditn(FREIGHT_CALC, 2);

        $("#WMDC_FREIGHT_CALC").val(FREIGHT_CALC);
    }

    var ClaculateSubtotal = function (DomainName) {
        var Subtotal = 0;
        //alert("Hellooooo 22");
        var Freight = $("#WMDC_FREIGHT").val();
        var FREIGHT_CALC = $("#WMDC_FREIGHT_CALC").val();
        var RATE_TYPE = $("#WMDC_RATE_TYPE").val();

        var PKGSNO = $("#WMD_PKGSNO").val();
        var CHRGWT = $("#WMD_CHRGWT").val();

        var ForvCharged = $("#WMDC_FOV").val();
        //var CODChrge = $("#WMD_codamt").val();
        var CODChrge = $("#WMD_Codchrg").val();
        var DACCCharge = $("#WMD_DKTDACCCharges").val();
        var IsDACC = $('#WMD_IsDACC').is(':checked');
        var IsCODDOD = $('#WMD_IsCODDOD').is(':checked');
        var FlagStaxBifer = $("#WMD_FlagStaxBifer").val();
        var FlagRoundOff = $("#WMD_FlagRoundOff").val();

        var TransType = $("#WMD_TRN_MOD").val();
        var ServiceType = $("#WMD_Service_Class").val();
        var PayBase = $("#WMD_PAYBAS").val();
        var StaxPaidBy = $("#WMD_stax_paidby").val();
        var DOCKDT = $("#WMD_DOCKDT").val();

        var IsStaxExemp = $('#WMD_IsStaxExemp').is(':checked');


        // alert(RATE_TYPE + " " + Freight + " " + FREIGHT_CALC);

        $("#WMDC_FREIGHT").val(Freight);
        $("#WMDC_FREIGHT_CALC").val(FREIGHT_CALC);

        //  alert($("#WMDC_FREIGHT").val());


        if (Freight == "" || Freight == null)
            Freight = 0;
        if (ForvCharged == "" || ForvCharged == null)
            ForvCharged = 0;
        if (CODChrge == "" || CODChrge == null)
            CODChrge = 0;
        if (DACCCharge == "" || DACCCharge == null)
            DACCCharge = 0;

        if (Freight == "" || Freight == null)
            Freight = 0;
        if (Freight == "" || Freight == null)
            Freight = 0;

        Subtotal = parseFloat(Subtotal) + parseFloat(Freight);

        Subtotal = parseFloat(Subtotal) + parseFloat(ForvCharged);

        if (IsCODDOD)
            Subtotal = parseFloat(Subtotal) + parseFloat(CODChrge);

        if (IsDACC)
            Subtotal = parseFloat(Subtotal) + parseFloat(DACCCharge);

         // alert(Freight + " - " + Subtotal + " - " + ForvCharged + " - " + CODChrge + " - " + DACCCharge);

        var TotalCharges = 0;

        $(".clsDKTCharge").each(function () {
            var ChrgId = $(this).attr("id").replace("DocketChargesList_", "").replace("__ChargeAmount", "");
            //alert(ChrgId);
            var OPRId = $(this).attr("id").replace("ChargeAmount", "Operator");
            var Operator = $("#" + OPRId).val();
            // $("#WMDC_" + ChrgId).val($(this).val());
            if (Operator == "-") {
                //   alert("Hello");
                TotalCharges = parseFloat(TotalCharges) - parseFloat($(this).val());
            }
            else
                TotalCharges = parseFloat(TotalCharges) + parseFloat($(this).val());
            // alert(TotalActWt.val());
        });


        Subtotal = parseFloat(Subtotal) + parseFloat(TotalCharges);

        Subtotal = rounditn(Subtotal, 2);

        $("#WMDC_SubTotal").val(Subtotal);


        //if (FlagStaxBifer == "Y")
        //    ClaculateServiceTaxWithBifercation(DomainName);
        //else
        //    ClaculateServiceTax(DomainName);

        if (parseFloat(Subtotal) > 0 && ServiceType != "" && TransType != "" && StaxPaidBy != "") {

            App.blockUI({ boxed: true });

            var StrURL1 = DomainName + "/Operation/GetServiceTax?&ServiceType=" + ServiceType + "&ServiceTaxPaidBy=" + StaxPaidBy + "&TransMode=" + TransType
                + "&DOCKDT=" + DOCKDT + "&FinSubtotal=" + Subtotal
             + "&IsStaxExemp=" + IsStaxExemp + "&FlagRoundOff=" + FlagRoundOff + "&FlagStaxBifer=" + FlagStaxBifer;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    $("#WMDC_SVCTAX_Rate").val(data1.ServiceTaxRate);
                    $("#SVCTAX_Rate").val(data1.CollServiceTaxRate);
                    $("#WMDC_SVCTAX").val(data1.CollServiceTaxRate);

                    $("#CessRate").val(data1.CessRate);
                    $("#CollCESS").val(data1.CollCess);
                    $("#WMDC_CESS").val(data1.CollCess);

                    $("#HECessRate").val(data1.HCessRate);
                    $("#Collhedu_cess").val(data1.CollHCess);
                    $("#WMDC_hedu_cess").val(data1.CollHCess);

                    $("#WMDC_KKCRate").val(data1.KKCRate);
                    $("#KKCCess").val(data1.CollKKCRate);
                    $("#WMDC_KKCAmount").val(data1.CollKKCRate);

                    $("#WMDC_SbcRate").val(data1.SBRate);
                    $("#SBCess").val(data1.CollSBRate);
                    $("#WMDC_SBCess").val(data1.CollSBRate);
                    //alert("Subtotal = " + Subtotal + " STax =  " + data1.CollServiceTaxRate + " SB Cess =  " + data1.CollSBRate + " KKC  =  " + data1.CollKKCRate)
                    $("#WMDC_DKTTOT").val(data1.DocketTotal);

                    App.unblockUI();
                },
                error: function (req, status, error) {
                    alert("error--" + req + "---" + status + "---" + error);
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function (DomainName) {

            //  alert("Hello");
            ControlDate();
            Formvalidate(DomainName);
            SubmitClick(DomainName);
            PageLoadJS(DomainName);
            $('.ftdate-picker').datepicker({
                isRTL: App.isRTL(),
                format: "dd-MM-yyyy",
                autoclose: true,
                pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                startDate: new Date(),
                todayBtn: true
            });
            AllCheckBoxClick(DomainName);


            $("#WMD_CHRGWT").change(function () {
                //CalculateWT();
                //CalculateCFT();
                GetFreightContractDetails(DomainName);
                // CalculateCHRGWT();
            });

            //$(".clsDateControl").on("changeDate", function (e) {

            //    alert("Hello");

            //    $(".clsinvdt").each( function (e) {

            //        var date = new Date($("#WMD_DOCKDT").val());
            //        var day = date.getDate();
            //        var month = date.getMonth();
            //        var year = date.getFullYear();
            //        //  var date = new Date((day  + ' ' + months[month] + ' ' + year));
            //        //  $("#DocketInvoiceList_" + Rid + "__INVDT").val((day + ' ' + months[month] + ' ' + year));
            //        alert($(this).attr("id"));
            //        $(this).attr("readonly", "true");
            //        handleDateTimePicker_ddmmyyyy($(this).attr("id"), new Date((day + ' ' + months[month] + ' ' + year)), new Date());


            //    });

            //});


            // PartyCodeChanges(DomainName);
            CnorCnee(DomainName);
            var itemIndex = $("#container input.iHidden").length;
            AddInvoiceRows(DomainName, itemIndex);

            var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/PAYTYP";
            FillDropDownfromOtherSelectFirst("WMD_PAYBAS", StrURL, "Billing Type");

            StrURL = DomainName + "/Operation/GetGeneralMasterDetails/RATETYPE";
            FillDropDownfromOtherSelectFirst("WMDC_RATE_TYPE", StrURL, "Billing Type");

            StrURL = DomainName + "/Operation/GetDestinationLocations";
            JsonDDLSelectCode("WMD_DESTCD", StrURL);

            PaybasChange(DomainName);

            $(".clsDKTCharge,.clsPayment").change(function () {

                //alert("Hello");

                // GetProRataCharge(DomainName);

                //   $("#"+$(this).attr("id")).change(function () {

                ClaculateSubtotal(DomainName);

            });

            $("#WMDC_FREIGHT,#WMDC_RATE_TYPE").change(function () {
                FreightChagne();

                GetFuelChargesDetails();
                ClaculateSubtotal(DomainName);


            });

            $("#WMDC_FREIGHT_CALC").change(function () {


                FreightRateChagne();

                GetFuelChargesDetails();

                ClaculateSubtotal(DomainName);
            });

            setTimeout(
                function () {

                    // alert("hhhh");
                    GetPartyCode("WMD_PARTY_CODE", $("#WMD_ORGNCD").val(), $("#WMD_PAYBAS").val());

                    //$("#WMDC_FREIGHT").attr("min", "150");
                    //$("#WMDC_FREIGHT_CALC").attr("min", "1");

                    if ($("#WMD_PAYBAS").val() !== "P04") {
                        if ($("#WMD_PAYBAS").val() == "P01" || $("#WMD_PAYBAS").val() == "P03") {
                            if ($("#WMD_TRN_MOD").val() == "1") // Air
                            {
                                $("#WMDC_FREIGHT").attr("min", "1000");
                            }
                            else if ($("#WMD_TRN_MOD").val() == "3") { // Train
                                $("#WMDC_FREIGHT").attr("min", "800");
                            }
                            else if ($("#WMD_TRN_MOD").val() == "4") { // Express
                                $("#WMDC_FREIGHT").attr("min", "400");
                            }
                        }
                        else {
                            $("#WMDC_FREIGHT").attr("min", "1");
                        }
                        $("#WMDC_FREIGHT_CALC").attr("min", "1");
                        $('.clsVehicle1').hide();
                        $(".ftdate-picker input").prop('disabled', true);
                    }
                    else {
                        $("#WMDC_FREIGHT").attr("min", "0");
                        $("#WMDC_FREIGHT_CALC").attr("min", "0");
                    }

                }, 500);
            $("#WMD_DOCKDT").live('change', function () {
                //alert($(this).val() + $(".clsinvoicedt").val());
                $(".clsinvoicedt").val($(this).val());
                $(".clsinvoicedt").datepicker("option", "endDate", $(this).val());
            });
            
            //$("#WMD_party_as").val($('input[name=BillingPartyAS]:checked').attr("id"));
            BillingPartyAS(DomainName);

            $('#WMD_CSGECD').attr('readonly', true);
            $("#WMD_CSGECD").select2("data", { id: "8888", text: "8888" });
            $('#WMD_CSGNCD').attr('readonly', false);
            $('#WMD_CSGNCD').select2('data', null);
            $("#WMD_CSGENM").attr('readonly', false)
        }
    }
}();