﻿using System.Security.Principal;

namespace Fleet.Security
{
    public interface ICustomIdentity : IIdentity
    {
        bool IsInRole(string role);
        string ToJson();
    }
}