﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Security;
using Fleet.Models;
using System.Web;
using System.Data;
using Fleet.Classes;
using FleetDataService.Models;
using System.Collections.Generic;

namespace Fleet.Security
{
    public class CustomIdentity : ICustomIdentity
    {
        /// <summary>
        /// Authenticate and get identity out with roles
        /// </summary>
        /// <param name="userName">User name</param>
        /// <param name="password">Password</param>
        /// <returns>Instance of identity</returns>
        /// 
        public static CustomIdentity GetCustomIdentity(string userName, string password, string Loccode, string CompanyCode)
        {
            FleetDataService.MasterService MS = new FleetDataService.MasterService();
            CustomIdentity identity = new CustomIdentity();
            if (Membership.ValidateUser(userName, password))
            {
                identity.IsAuthenticated = true;
                identity.Name = userName.ToUpper();
                //var roles = System.Web.Security.Roles.GetRolesForUser(userName);
                // identity.Roles = roles;

                WebX_Master_Users WMU = MS.GetUserDetails().FirstOrDefault(c => c.UserId.ToUpper() == userName.ToUpper() && c.Status == "100");

                identity.LocationCode = WMU.BranchCode;
                identity.MainLocCode = WMU.BranchCode;
                identity.User_Image = WMU.User_Image;
                identity.UserType = MS.GetLoginUserType("", userName.ToUpper());
                identity.UserReadWrite = WMU.Read_Witre;
                identity.ActionUrl = WMU.Action_Url;
                identity.UserReadWrite = WMU.Read_Witre;
                identity.ChangeMenuRights = WMU.Change_Menu_Rights;
                identity.ChangeReportsRights = WMU.Change_Reports_Rights;
                identity.ResetPassword = WMU.Reset_Password;
                identity.BlockUser = WMU.Block_User;
                List<webx_location> WL = MS.GetLocationDetails();
                identity.LocationName = WL.FirstOrDefault(c => c.LocCode == identity.LocationCode).LocName;
                //identity.LocationGroup = SL.lo;
                //webx_location HeadLoc = WL.FirstOrDefault(c => c.Loc_Level == 1 && c.ActiveFlag == "Y");
                identity.HeadOfficeCode = WL.FirstOrDefault(c => c.Loc_Level == 1 && c.ActiveFlag == "Y").LocCode;
                identity.LocationLevel = WL.FirstOrDefault(c => c.LocCode == identity.LocationCode).Loc_Level;

                vw_Get_Finacial_Years VFY = MS.GetFinacialYearDetails().FirstOrDefault(c => c.CurrentFinYear == "T");

                identity.FinYear = VFY.FinYear;
                identity.YearVal = VFY.YearVal;

                vw_Get_Finacial_Years VFYCURR = MS.GetFinacialYearDetails().FirstOrDefault(c => c.CurrentFinYear == "T");
                identity.CurrFinYear = VFYCURR.FinYear;
                identity.CurrYearVal = VFYCURR.YearVal;

                GeneralFuncations GF = new GeneralFuncations();
                string commandText = "Exec CompanyMappedToEmployee '" + userName + "'  ";
                DataTable dataTable = GF.getdatetablefromQuery(commandText);
                string compCode = "";

                if (dataTable.Rows.Count == 0 || string.IsNullOrEmpty(dataTable.Rows[0][0].ToString()))
                {
                    //compCode = "C004";
                    compCode = "C003";
                }
                else
                {
                    compCode = dataTable.Rows[0][0].ToString();
                }

                //WEBX_COMPANY_MASTER WMM = MS.GetComapanyDetails().FirstOrDefault(c => c.COMPANY_CODE == WMU.BranchCode);
                WEBX_COMPANY_MASTER WMM = MS.GetComapanyDetails().FirstOrDefault(c => c.COMPANY_CODE == compCode);
                identity.CompanyCode = WMM.COMPANY_CODE;
                identity.CompanyName = WMM.COMPANY_NAME;
                identity.emptype = WMU.emptype;
                return identity;
            }
            return identity;
        }

        public static CustomIdentity GetCustomIdentity(string userName, string Loccode, string FinYear, string CompanyCode, string ext)
        {
            FleetDataService.MasterService MS = new FleetDataService.MasterService();
            CustomIdentity identity = new CustomIdentity();
            WebX_Master_Users WMU = MS.GetUserDetails().FirstOrDefault(c => c.UserId.ToUpper() == userName.ToUpper() && c.Status == "100");
            WEBX_COMPANY_MASTER WMM = MS.GetComapanyDetails().FirstOrDefault(c => c.COMPANY_CODE == CompanyCode);
            identity.IsAuthenticated = true;
            identity.Name = userName.ToUpper();
            identity.MainLocCode = WMU.BranchCode;
            identity.User_Image = WMU.User_Image;
            identity.UserType = MS.GetLoginUserType("", userName.ToUpper());
            identity.UserReadWrite = WMU.Read_Witre;
            identity.ChangeMenuRights = WMU.Change_Menu_Rights;
            identity.ChangeReportsRights = WMU.Change_Reports_Rights;
            identity.ResetPassword = WMU.Reset_Password;
            identity.BlockUser = WMU.Block_User;
            identity.ActionUrl = WMU.Action_Url;
            //var roles = System.Web.Security.Roles.GetRolesForUser(userName);
            // identity.Roles = roles;

            //identity.LocationGroup = SL.GroupId;
            identity.LocationCode = Loccode;
            List<webx_location> WL = MS.GetLocationDetails();
            identity.LocationName = WL.FirstOrDefault(c => c.LocCode == identity.LocationCode).LocName;
            //identity.LocationGroup = SL.lo;
            //webx_location HeadLoc = WL.FirstOrDefault(c => c.Loc_Level == 1 && c.ActiveFlag == "Y");
            identity.HeadOfficeCode = WL.FirstOrDefault(c => c.Loc_Level == 1 && c.ActiveFlag == "Y").LocCode;
            identity.LocationLevel = WL.FirstOrDefault(c => c.LocCode == identity.LocationCode).Loc_Level;

            vw_Get_Finacial_Years VFY = MS.GetFinacialYearDetails().FirstOrDefault(c => c.FinYear == FinYear);

            identity.CompanyCode = WMM.COMPANY_CODE;
            identity.CompanyName = WMM.COMPANY_NAME;

            identity.FinYear = VFY.FinYear;
            identity.YearVal = VFY.YearVal;

            vw_Get_Finacial_Years VFYCURR = MS.GetFinacialYearDetails().FirstOrDefault(c => c.CurrentFinYear == "T");
            identity.CurrFinYear = VFYCURR.FinYear;
            identity.CurrYearVal = VFYCURR.YearVal;
            identity.emptype = WMU.emptype;
            return identity;
        }

        private CustomIdentity() { }

        public string AuthenticationType
        {
            get { return "Custom"; }
        }

        public bool IsAuthenticated { get; private set; }
        public string Name { get; private set; }
        public string LocationName { get; private set; }
        public Nullable<decimal> LocationLevel { get; private set; }
        public string LocationCode { get; private set; }
        public string LocationGroup { get; private set; }
        public string MainLocCode { get; private set; }
        public string FinYear { get; private set; }
        public string YearVal { get; private set; }
        public string CurrFinYear { get; private set; }
        public string CurrYearVal { get; private set; }
        public string HeadOfficeCode { get; private set; }
        public string CompanyCode { get; private set; }
        public string CompanyName { get; private set; }
        public string User_Image { get; private set; }
        private string[] Roles { get; set; }
        public string UserType { get; private set; }
        public string UserReadWrite { get; private set; }
        public string ChangeMenuRights { get; private set; }
        public string ChangeReportsRights { get; private set; }
        public string ResetPassword { get; private set; }
        public string BlockUser { get; private set; }
        public string ActionUrl { get; private set; }
        public string emptype { get; private set; }

        public bool IsInRole(string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                throw new ArgumentException("Role is null");
            }
            return Roles.Where(one => one.ToUpper().Trim() == role.ToUpper().Trim()).Any();
        }

        /// <summary>
        /// Create serialized string for storing in a cookie
        /// </summary>
        /// <returns>String representation of identity</returns>
        public string ToJson()
        {
            string returnValue = string.Empty;
            IdentityRepresentation representation = new IdentityRepresentation()
            {
                IsAuthenticated = this.IsAuthenticated,
                Name = this.Name,
                LocationCode = this.LocationCode,
                MainLocCode = this.MainLocCode,
                LocationName = this.LocationName,
                LocationLevel = this.LocationLevel,
                FinYear = this.FinYear,
                YearVal = this.YearVal,
                HeadOfficeCode = this.HeadOfficeCode,
                CompanyCode = this.CompanyCode,
                CompanyName = this.CompanyName,
                LocationGroup = this.LocationGroup,
                User_Image = this.User_Image,
                CurrFinYear = this.CurrFinYear,
                CurrYearVal = this.CurrYearVal,
                UserType = this.UserType,
                UserReadWrite = this.UserReadWrite,
                ChangeMenuRights = this.ChangeMenuRights,
                ChangeReportsRights = this.ChangeReportsRights,
                ResetPassword = this.ResetPassword,
                BlockUser = this.BlockUser,
                ActionUrl = this.ActionUrl,
                EmpType=this.emptype
                //,
                //Roles = string.Join("|", this.Roles)
            };
            DataContractJsonSerializer jsonSerializer =
                new DataContractJsonSerializer(typeof(IdentityRepresentation));
            using (MemoryStream stream = new MemoryStream())
            {
                jsonSerializer.WriteObject(stream, representation);
                stream.Flush();
                byte[] json = stream.ToArray();
                returnValue = Encoding.UTF8.GetString(json, 0, json.Length);
            }

            return returnValue;
        }

        /// <summary>
        /// Create identity from a cookie data
        /// </summary>
        /// <param name="cookieString">String stored in cookie, created via ToJson method</param>
        /// <returns>Instance of identity</returns>
        public static ICustomIdentity FromJson(string cookieString)
        {

            IdentityRepresentation serializedIdentity = null;
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(cookieString)))
            {
                DataContractJsonSerializer jsonSerializer =
                    new DataContractJsonSerializer(typeof(IdentityRepresentation));
                serializedIdentity = jsonSerializer.ReadObject(stream) as IdentityRepresentation;
            }
            CustomIdentity identity = new CustomIdentity()
            {
                IsAuthenticated = serializedIdentity.IsAuthenticated,
                Name = serializedIdentity.Name,
                LocationCode = serializedIdentity.LocationCode,
                MainLocCode = serializedIdentity.MainLocCode,
                LocationName = serializedIdentity.LocationName,
                LocationLevel = serializedIdentity.LocationLevel,
                FinYear = serializedIdentity.FinYear,
                YearVal = serializedIdentity.YearVal,
                HeadOfficeCode = serializedIdentity.HeadOfficeCode,
                CompanyCode = serializedIdentity.CompanyCode,
                CompanyName = serializedIdentity.CompanyName,
                LocationGroup = serializedIdentity.LocationGroup,
                User_Image = serializedIdentity.User_Image,
                CurrFinYear = serializedIdentity.CurrFinYear,
                CurrYearVal = serializedIdentity.CurrYearVal,
                UserType = serializedIdentity.UserType,
                UserReadWrite = serializedIdentity.UserReadWrite,
                ChangeMenuRights = serializedIdentity.ChangeMenuRights,
                ChangeReportsRights = serializedIdentity.ChangeReportsRights,
                ResetPassword = serializedIdentity.ResetPassword,
                BlockUser = serializedIdentity.BlockUser,
                ActionUrl = serializedIdentity.ActionUrl,
                emptype = serializedIdentity.EmpType,
                //,
                //Roles = serializedIdentity.Roles
                //    .Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries)
            };
            return identity;
        }

    }
}