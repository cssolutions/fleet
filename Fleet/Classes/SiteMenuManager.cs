﻿using System.Collections.Generic;
using System.Linq;
using Fleet.Models;
using System.Web.Mvc;
using System.Data;
using Fleet.Classes;
using System.Configuration;
using FleetDataService.Models;
namespace Fleet
{
    public class SiteMenuManager :BaseController
    {
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        //public string DomainName = ConfigurationManager.AppSettings["DomainName"].ToString();   

        public string IsDomainNameRequired = ConfigurationManager.AppSettings["IsDomainNameRequired"].ToString();        
        public List<ISiteLink> GetSitemMenuItems(string Userid)
        {
            var items = new List<ISiteLink>();
            string DomainName = ConfigurationManager.AppSettings["DomainName"].ToString();//"/" + MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "DOMIANNAME" && c.CodeId == "1").FirstOrDefault().CodeDesc.ToUpper();

            if (IsDomainNameRequired == "false")
                DomainName = "";

            //DataTable dataTable = ML.GetMenuWithRights(Userid, "BindMenu");
            //List<VW_GetUserMenuRights> MenuList = MS.GetMenuListWithRights().Where(c => c.UserId == Userid && c.HasAccess == true ).ToList();
            List<VW_GetUserMenuRights> MenuList = MS.GetMenuListWithRights(Userid, false, "0",BaseUserName).Where(c => c.HasAccess == true).ToList();

            foreach (var Mitem in MenuList)
            {
                items.Add(new SiteMenuItem
                {
                    MenuID = Mitem.MenuId,
                    ParentID = Mitem.ParentID,
                    DisplayName = Mitem.DisplayName,
                    NavigationURL =   DomainName + Mitem.NavigationURL,
                    //NavigationURL =  Mitem.NavigationURL,
                    MenuLevel = Mitem.MenuLevel,
                    //NavigationURL = Mitem.CYGNUS_Master_Menu.NavigationURL,
                    IsActive = false,
                    IsNewPortal = Mitem.IsNewPortal,
                    //Controller=Mitem.Controller,
                    //Action = Mitem.Action,
                    //Action1 = Mitem.Action1,
                    //Action2 = Mitem.Action2,
                    //Action3 = Mitem.Action3,

                    DisplayRank = Mitem.DisplayRank
                });
            }

            return items;
        }


    }
}