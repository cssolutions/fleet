﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="MvcReportViewer.MvcReportViewer, MvcReportViewer" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head id="Head1" runat="server">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
    <link href="~/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="~/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="~/assets/css/style-metronic.css" rel="stylesheet" type="text/css" />
    <link href="~/assets/css/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        $(function () {
            // Bug-fix on Chrome and Safari etc (webkit)
            if ($.browser.webkit) {
                // Start timer to make sure overflow is set to visible
                setInterval(function () {
                    var div = $('#<%=ReportViewer.ClientID %>_fixedTable > tbody > tr:last > td:last > div')
                    div.css('overflow', 'visible');
                }, 1000);
            }
        });
        function pageLoad() {
            // var usrName = '<%=@HttpContext.Current.User.Identity.Name%>'
            var element = document.getElementById("ctl31_ctl09");
            if (element) {
                element.style.overflow = "visible";
            }
        }

        //window.onload = pageLoad();
        //$(document).ready(function () {
        //    //$("a[title='PDF']").parent().hide();  // Remove from export dropdown.
        //    if (TempData["ReportId"] = "114") {
        //        $("a[title='Excel']").parent().hide();
        //    }
        //    else {
        //        $("a[title='TIFF file']").parent().hide();
        //    }
        //});

      

    </script>
</head>

<body>

    <div class="form-body">
        <div class="row pull-right" style="padding-right: 5px;">
            <%--<button id="printreport" type="button" class="btn btn-sm green">
                <i class="fa fa-print"></i>&nbsp;Print</button>--%>
            <%--<input id="printreport" type="button" class="btn btn-sm blue" value="Print this Report" />--%>
            <input id="PrintButton" title="Print" style="width: 16px; height: 16px;" type="image" alt="Print" runat="server" src="~/Reserved.ReportViewerWebControl.axd?OpType=Resource&amp;Version=11.0.3442.2&amp;Name=Microsoft.Reporting.WebForms.Icons.Print.gif" />
        </div>
    </div>
    <div class="pdf">
    </div>
    <form id="reportForm" runat="server">
        <div id="ctl31_ctl10" style="height: 100%; width: 100%;">
            <asp:ScriptManager runat="server"></asp:ScriptManager>
            <rsweb:ReportViewer ID="ReportViewer" ShowPrintButton="true" ShowParameterPrompts="true" ShowExportControls="true" runat="server" Width="100%" Height="100%"></rsweb:ReportViewer>
        </div>
    </form>
</body>
</html>

<script type="text/javascript">
    //------------------------------------------------------------------
    // Cross-browser Multi-page Printing with ASP.NET ReportViewer
    // by Chtiwi Malek.
    // http://www.codicode.com
    //------------------------------------------------------------------

    $(document).ready(function () {
      
        // Check if the current browser is IE (MSIE is not used since IE 11)
        var isIE = /MSIE/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent);
        var queryString = location.search;
        
        function printReport() {

            var reportViewerName = 'ReportViewer'; //Name attribute of report viewer control.
            var src_url = $find(reportViewerName)._getInternalViewer().ExportUrlBase + 'PDF';

            var contentDisposition = 'AlwaysInline'; //Content Disposition instructs the server to either return the PDF being requested as an attachment or a viewable report.
            var src_new = src_url.replace(/(ContentDisposition=).*?(&)/, '$1' + contentDisposition + '$2');

            var iframe = $('<iframe>', {
                src: src_new,
                id: 'pdfDocument',
                frameborder: 0,
                scrolling: 'no'
            }).hide().load(function () {
                var PDF = document.getElementById('pdfDocument');
                PDF.focus();
                try {
                    PDF.contentWindow.print();
                }
                catch (ex) {
                    //If all else fails, we want to inform the user that it is impossible to directly print the document with the current browser.
                    //Instead, let's give them the option to export the pdf so that they can print it themselves with a 3rd party PDF reader application.
                    if (confirm("ActiveX and PDF Native Print support is not supported in your browser. The system is unable to print your document directly. Would you like to download the PDF version instead? You may print the document by opening the PDF using your PDF reader application.")) {
                        window.open($find(reportViewerName)._getInternalViewer().ExportUrlBase + 'PDF');
                    }
                }
            })
            //Bind the iframe we created to an invisible div.
            $('.pdf').html(iframe);
        }

        // 2. Add Print button for non-IE browsers
        if (!isIE) {
            $('#PrintButton').click(function (e) {
                e.preventDefault();
                printReport();
            })
        }
    });

    // Linking the print function to the print button
    $('#printreport').click(function () {
        printReport('ReportViewer');
    });

    // Print function (require the reportviewer client ID)
    function printReport(report_ID) {
        var rv1 = $('#' + report_ID);
        var iDoc = rv1.parents('html');

        // Reading the report styles
        var styles = iDoc.find("head style[id$='ReportControl_styles']").html();
        if ((styles == undefined) || (styles == '')) {
            iDoc.find('head script').each(function () {
                var cnt = $(this).html();
                var p1 = cnt.indexOf('ReportStyles":"');
                if (p1 > 0) {
                    p1 += 15;
                    var p2 = cnt.indexOf('"', p1);
                    styles = cnt.substr(p1, p2 - p1);
                }
            });
        }
        if (styles == '') { alert("Cannot generate styles, Displaying without styles.."); }
        styles = '<style type="text/css">' + styles + "</style>";

        // Reading the report html
        var table = rv1.find("div[id$='_oReportDiv']");
        if (table == undefined) {
            alert("Report source not found.");
            return;
        }

        // Generating a copy of the report in a new window
        var docType = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/loose.dtd">';
        var docCnt = styles + table.parent().html();
        var docHead = '<head><title>Fleet</title><style>body{margin:5;padding:0;}</style></head>';
        var winAttr = "";//location=yes,statusbar=no,directories=no,menubar=no,titlebar=no,toolbar=no,dependent=no,width=720,height=600,resizable=yes,screenX=200,screenY=200,personalbar=no,scrollbars=yes";;
        var newWin = window.open("", winAttr);
        writeDoc = newWin.document;
        //writeDoc.open();
        writeDoc.write(docType + '<html>' + docHead + '<body onload="window.print();">' + docCnt + '</body></html>');
        writeDoc.close();

        // The print event will fire as soon as the window loads
        newWin.focus();
        // uncomment to autoclose the preview window when printing is confirmed or canceled.
        // newWin.close();
    };
</script>
