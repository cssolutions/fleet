﻿using CipherLib;
using CYGNUS.Classes;
using CYGNUS.Models;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;


namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class FleetController : BaseController
    {
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        string ControllerName = "FleetController";
        FleetDataService.FleetService FS = new FleetDataService.FleetService();
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        FleetDataService.TrackingService TS = new FleetDataService.TrackingService();
        FleetDataService.OperationService OS = new FleetDataService.OperationService();
        GeneralFuncations GF = new GeneralFuncations();

        public ActionResult Index()
        {
            return View();
        }

        #region Fleet Master

        public ActionResult FleetMaster()
        {
            return View();
        }

        #endregion

        #region Close Operational and Financially TripSheet

        public ActionResult CloseOperationalTripSheet()
        {
            CloseTripSheetViewModel CTSVM = new CloseTripSheetViewModel();

            return View(CTSVM);
        }

        public ActionResult CloseTripSheet(CloseTripSheetViewModel CTSVM)
        {
            CloseTripSheetViewModel POAVM = new CloseTripSheetViewModel();
            List<WEBX_FLEET_VEHICLE_ISSUE> POList;
            try
            {
                string FirstDate1 = GF.FormateDate(CTSVM.FromDate);
                string LastDate1 = GF.FormateDate(CTSVM.ToDate);
                string CurrentLocationCode = BaseLocationCode;
                POList = FS.GetTripSheetData(CTSVM.IssueID, FirstDate1, LastDate1, BaseLocationCode, CTSVM.TripsheetFlag, CTSVM.VehNo, BaseCompanyCode, CTSVM.SlipId);
                POAVM.POList = POList;
                POAVM.SlipId = CTSVM.SlipId;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(POAVM);
        }

        public ActionResult CloseVehicleIssueSlip(string VSlipNo, string mode)
        {
            CloseTripSheetViewModel CVIS = new CloseTripSheetViewModel();
            List<WEBX_FLEET_ENROUTE_EXP> ListWFEE = new List<WEBX_FLEET_ENROUTE_EXP>();
            List<WEBX_FLEET_VEHICLE_ISSUE> ListWFVI = new List<WEBX_FLEET_VEHICLE_ISSUE>();
            WEBX_FLEET_ENROUTE_EXP obj = new WEBX_FLEET_ENROUTE_EXP();
            WEBX_FLEET_VEHICLE_ISSUE WFVI = new WEBX_FLEET_VEHICLE_ISSUE();
            WEBX_TRIPSHEET_ADVEXP WTSA = new WEBX_TRIPSHEET_ADVEXP();
            List<WEBX_TRIPSHEET_ADVEXP> LISTAdvanceExp = new List<WEBX_TRIPSHEET_ADVEXP>();
            List<WEBX_FLEET_ENROUTE_EXP> LIST_TripExp = new List<WEBX_FLEET_ENROUTE_EXP>();
            LIST_TripExp = FS.Get_TripExp_List(VSlipNo);
            CVIS.Obj_WFT = FS.Get_Webx_Fleet_Trip().First();

            if (LIST_TripExp.Count() == 0)
            {
                WEBX_FLEET_ENROUTE_EXP OBjWTEM = new WEBX_FLEET_ENROUTE_EXP();
                OBjWTEM.SRNO = 1;
                LIST_TripExp.Add(OBjWTEM);
            }
            else
            {
                CVIS.EnrtExpList = LIST_TripExp;
            }

            //obj.SRNO = 1;
            //ListWFEE.Add(obj);
            //CVIS.EnrtExpList = ListWFEE;
            CVIS.List = ListWFVI;
            obj.id = VSlipNo;
            WFVI = FS.DetailsFromId(VSlipNo);
            LISTAdvanceExp = FS.DetailsForTrpAdvExp(VSlipNo);
            CVIS.WFVI = WFVI;
            CVIS.WTSA = WTSA;
            CVIS.LISTAdvanceExp = LISTAdvanceExp;

            CVIS.LIST_WTSADV = FS.Get_WTRSADV(VSlipNo);
            //TempData["locationcode"] = BaseLocationCode;
            CVIS.ListFuelSlip = DataRowToObject.CreateListFromTable<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO>(FS.Get_FuelSlipData(VSlipNo));
            CVIS.LIST_WTRSOP = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_OILEXPDET>(FS.Get_Oil_SlipData(VSlipNo));
            CVIS.SlipId = mode;
            return View(CVIS);
        }

        [HttpPost]
        public ActionResult CloseVehicleIssueSlip(CloseTripSheetViewModel VW, List<WEBX_TRIPSHEET_ADVEXP> LISTAdvanceExp, List<WEBX_TRIPSHEET_OILEXPDET> ListOILEXPDET, List<WEBX_FLEET_ENROUTE_EXP> fleetList)
        {
            DataTable DT = new DataTable();

            if (VW.WFVI.Oper_Close_Dt == null || VW.WFVI.Oper_Close_Dt.ToString("dd MMM yyyy") == "01 Jan 0001")
            {
                VW.WFVI.Oper_Close_Dt = System.DateTime.Now;
            }

            try
            {
                VW.Obj_WFT = FS.Get_Webx_Fleet_Trip().First();
                if (VW.Operational_Close == "1")
                {

                    #region WEBX_TRIPSHEET_OILEXPDET

                    XmlDocument xmlDoc1 = new XmlDocument();
                    List<WEBX_TRIPSHEET_OILEXPDET> List_OILEXPDET = new List<WEBX_TRIPSHEET_OILEXPDET>();

                    if (ListOILEXPDET != null)
                    {
                        foreach (var item in ListOILEXPDET)
                        {
                            WEBX_TRIPSHEET_OILEXPDET objWTOP = new WEBX_TRIPSHEET_OILEXPDET();
                            objWTOP.Place = item.Place;
                            objWTOP.PetrolPName = item.PetrolPName;
                            objWTOP.Brand = item.Brand;
                            objWTOP.FuelType = item.FuelType;
                            objWTOP.Last_Km_Read = item.Last_Km_Read;
                            objWTOP.KM_Reading = item.KM_Reading;
                            objWTOP.BillNo = item.BillNo;
                            objWTOP.BillDt = item.BillDt;
                            objWTOP.Diesel_Ltr = item.Diesel_Ltr;
                            objWTOP.Diesel_Rate = item.Diesel_Rate;
                            objWTOP.Amount = item.Amount;
                            objWTOP.EXE_AMT = item.EXE_AMT;
                            objWTOP.Card_Cash = item.Card_Cash;
                            objWTOP.Remark = item.Remark;
                            objWTOP.COMPANY_CODE = BaseCompanyCode;
                            objWTOP.Currloc = BaseLocationCode;
                            List_OILEXPDET.Add(objWTOP);
                        }
                    }
                    XmlSerializer xmlSerializer1 = new XmlSerializer(List_OILEXPDET.GetType());
                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, List_OILEXPDET);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }

                    #endregion

                    #region Webx_Fleet_Triprule

                    XmlDocument xmlDoc2 = new XmlDocument();
                    XmlSerializer xmlSerializer2 = new XmlSerializer(VW.Obj_WFT.GetType());
                    using (MemoryStream xmlStream2 = new MemoryStream())
                    {
                        xmlSerializer2.Serialize(xmlStream2, VW.Obj_WFT);
                        xmlStream2.Position = 0;
                        xmlDoc2.Load(xmlStream2);
                    }

                    #endregion

                    #region WEBX_TRIPSHEET_ADVEXP

                    XmlDocument xmlDoc3 = new XmlDocument();
                    List<WEBX_TRIPSHEET_ADVEXP> List_WTADVE = new List<WEBX_TRIPSHEET_ADVEXP>();

                    if (LISTAdvanceExp != null)
                    {
                        foreach (var item in LISTAdvanceExp)
                        {
                            WEBX_TRIPSHEET_ADVEXP ObjWTA = new WEBX_TRIPSHEET_ADVEXP();
                            ObjWTA.AdvLoc = item.AdvLoc;
                            ObjWTA.Advance_SlipNo = item.Advance_SlipNo;
                            ObjWTA.AdvDate = item.AdvDate;
                            ObjWTA.AdvAmt = item.AdvAmt;
                            ObjWTA.BranchCode = item.BranchCode;
                            ObjWTA.Signature = item.Signature;

                            List_WTADVE.Add(ObjWTA);
                        }
                        //List_WTADVE = LISTAdvanceExp;
                    }

                    XmlSerializer xmlSerializer3 = new XmlSerializer(List_WTADVE.GetType());
                    using (MemoryStream xmlStream3 = new MemoryStream())
                    {
                        xmlSerializer3.Serialize(xmlStream3, List_WTADVE);
                        xmlStream3.Position = 0;
                        xmlDoc3.Load(xmlStream3);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc4 = new XmlDocument();
                    List<WEBX_FLEET_ENROUTE_EXP> List_WFEXP = new List<WEBX_FLEET_ENROUTE_EXP>();

                    if (fleetList != null)
                    {
                        foreach (var item in fleetList.Where(c => c.Amount_Spent > 0 && c.id != "" && c.id != null).ToList())
                        {
                            WEBX_FLEET_ENROUTE_EXP objWFEE = new WEBX_FLEET_ENROUTE_EXP();
                            objWFEE.Polarity = item.Polarity;
                            objWFEE.TripsheetNo = item.TripsheetNo;
                            objWFEE.id = item.id;
                            objWFEE.Amount_Spent = item.Amount_Spent;
                            objWFEE.BillNo = item.BillNo;
                            objWFEE.Dt = item.Dt;
                            objWFEE.Exe_Appr_Amt = item.Exe_Appr_Amt;
                            objWFEE.Remarks = item.Remarks;

                            List_WFEXP.Add(objWFEE);
                        }
                    }
                    XmlSerializer xmlSerializer4 = new XmlSerializer(List_WFEXP.GetType());
                    using (MemoryStream xmlStream4 = new MemoryStream())
                    {
                        xmlSerializer4.Serialize(xmlStream4, List_WFEXP);
                        xmlStream4.Position = 0;
                        xmlDoc4.Load(xmlStream4);
                    }

                    #endregion

                    #region WEBX_FLEET_VEHICLE_ISSUE

                    XmlDocument xmlDoc5 = new XmlDocument();
                    XmlSerializer xmlSerializer5 = new XmlSerializer(VW.WFVI.GetType());
                    using (MemoryStream xmlStream5 = new MemoryStream())
                    {
                        xmlSerializer5.Serialize(xmlStream5, VW.WFVI);
                        xmlStream5.Position = 0;
                        xmlDoc5.Load(xmlStream5);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc6 = new XmlDocument();
                    XmlSerializer xmlSerializer6 = new XmlSerializer(VW.WFEE.GetType());
                    using (MemoryStream xmlStream6 = new MemoryStream())
                    {
                        xmlSerializer6.Serialize(xmlStream6, VW.WFEE);
                        xmlStream6.Position = 0;
                        xmlDoc5.Load(xmlStream6);
                    }

                    #endregion

                    #region XML

                    string XML = "<root>";
                    XML = XML + "<Closing_Km>" + VW.WFVI.f_closure_closekm + "</Closing_Km>";
                    XML = XML + "<VehicleNo>" + VW.WFVI.VehicleNo + "</VehicleNo>";
                    XML = XML + "<TripSheet_EndLoc>" + VW.WFVI.TripSheet_EndLoc + "</TripSheet_EndLoc>";
                    XML = XML + "<TripSheet_StartLoc>" + VW.WFVI.TripSheet_StartLoc + "</TripSheet_StartLoc>";
                    XML = XML + "<PreparedBy>" + VW.WFVI.PreparedBy + "</PreparedBy>";
                    XML = XML + "<CheckedBy>" + VW.WFVI.CheckedBy + "</CheckedBy>";
                    XML = XML + "<ApprovedBy>" + VW.WFVI.ApprovedBy + "</ApprovedBy>";
                    XML = XML + "<Oper_Close_Dt>" + VW.WFVI.Oper_Close_Dt.ToString("dd MMM yyyy") + "</Oper_Close_Dt>";
                    XML = XML + "<AuditedBy>" + VW.WFVI.AuditedBy + "</AuditedBy>";
                    XML = XML + "<WFEE_Total>" + VW.WFEE.Total + "</WFEE_Total>";
                    XML = XML + "<Category>" + VW.WFVI.Category + "</Category>";
                    XML = XML + "</root>";

                    #endregion

                    string SQRY = "exec Usp_Operationally_TripClose '" + xmlDoc1.InnerXml + "','" + xmlDoc2.InnerXml + "','" + xmlDoc3.InnerXml +
                        "','" + xmlDoc4.InnerXml + "' ,'" + XML + "','" + VW.WFVI.VSlipNo + "'";

                    DT = FS.TripOperationally_Close(xmlDoc1.InnerXml, xmlDoc2.InnerXml, xmlDoc3.InnerXml, xmlDoc4.InnerXml, XML, VW.WFVI.VSlipNo, BaseLocationCode, BaseCompanyCode);
                }
                else if (VW.Operational_Close == "0")
                {

                    #region usp_CloseVehicleIssue

                    string CloseVehicle = "<root>";

                    CloseVehicle = CloseVehicle + "<VSlipNo>" + VW.WFVI.VSlipNo + "</VSlipNo>";
                    CloseVehicle = CloseVehicle + "<Driver1>" + VW.WFVI.Driver1 + "</Driver1>";
                    CloseVehicle = CloseVehicle + "<VehicleNo>" + VW.WFVI.VehicleNo + "</VehicleNo>";
                    CloseVehicle = CloseVehicle + "<f_closure_closekm>" + VW.WFVI.f_closure_closekm + "</f_closure_closekm>";
                    CloseVehicle = CloseVehicle + "<f_closure_fill>" + VW.WFVI.Fuel_Filled_enroute + "</f_closure_fill>";
                    CloseVehicle = CloseVehicle + "<AKMPL>" + VW.WFVI.Actual_KMPL + "</AKMPL>";
                    CloseVehicle = CloseVehicle + "<APPKMPL>" + VW.WFVI.Approved_KMPL + "</APPKMPL>";
                    CloseVehicle = CloseVehicle + "<Closedt>" + VW.WFVI.TS_Close_Dt + "</Closedt>";
                    CloseVehicle = CloseVehicle + "<Oper_Closedt>" + VW.WFVI.Oper_Close_Dt + "</Oper_Closedt>";
                    CloseVehicle = CloseVehicle + "<Contractamt>0</Contractamt>";
                    CloseVehicle = CloseVehicle + "<c_Othercharg>0</c_Othercharg>";
                    CloseVehicle = CloseVehicle + "<c_TDSrate>0</c_TDSrate>";
                    CloseVehicle = CloseVehicle + "<c_TDSamt>0</c_TDSamt>";
                    CloseVehicle = CloseVehicle + "<c_TDStype>0</c_TDStype>";
                    CloseVehicle = CloseVehicle + "<c_Advamt_paid>0</c_Advamt_paid>";
                    CloseVehicle = CloseVehicle + "<c_issue_tot>0</c_issue_tot>";
                    CloseVehicle = CloseVehicle + "<c_dedamt>0</c_dedamt>";
                    CloseVehicle = CloseVehicle + "<c_Balamt_paid>0</c_Balamt_paid>";
                    CloseVehicle = CloseVehicle + "<c_closure_tot>0</c_closure_tot>";
                    CloseVehicle = CloseVehicle + "<c_net_amt>0</c_net_amt>";
                    CloseVehicle = CloseVehicle + "<e_issue_advamt>0</e_issue_advamt>";
                    CloseVehicle = CloseVehicle + "<e_issue_spare>0</e_issue_spare>";
                    CloseVehicle = CloseVehicle + "<e_issue_fuel>0</e_issue_fuel>";
                    CloseVehicle = CloseVehicle + "<e_issue_comm>0</e_issue_comm>";
                    CloseVehicle = CloseVehicle + "<e_closure_balamt>0</e_closure_balamt>";
                    CloseVehicle = CloseVehicle + "<e_closure_incentive>0</e_closure_incentive>";
                    CloseVehicle = CloseVehicle + "<e_closure_deposit>0</e_closure_deposit>";
                    CloseVehicle = CloseVehicle + "<e_closure_dedamt>0</e_closure_dedamt>";
                    CloseVehicle = CloseVehicle + "<e_issue_totamt>0</e_issue_totamt>";
                    CloseVehicle = CloseVehicle + "<e_closure_totamt>0</e_closure_totamt>";
                    CloseVehicle = CloseVehicle + "<e_net_amt>0</e_net_amt>";
                    CloseVehicle = CloseVehicle + "</root>";

                    #endregion

                    #region WEBX_TRIPSHEET_OILEXPDET

                    XmlDocument xmlDoc1 = new XmlDocument();
                    List<WEBX_TRIPSHEET_OILEXPDET> List_OILEXPDET = new List<WEBX_TRIPSHEET_OILEXPDET>();

                    if (ListOILEXPDET != null)
                    {
                        foreach (var item in ListOILEXPDET)
                        {
                            WEBX_TRIPSHEET_OILEXPDET objWTOP = new WEBX_TRIPSHEET_OILEXPDET();
                            objWTOP.Place = item.Place;
                            objWTOP.PetrolPName = item.PetrolPName;
                            objWTOP.Brand = item.Brand;
                            objWTOP.FuelType = item.FuelType;
                            objWTOP.Last_Km_Read = item.Last_Km_Read;
                            objWTOP.KM_Reading = item.KM_Reading;
                            objWTOP.BillNo = item.BillNo;
                            objWTOP.BillDt = item.BillDt;
                            objWTOP.Diesel_Ltr = item.Diesel_Ltr;
                            objWTOP.Diesel_Rate = item.Diesel_Rate;
                            objWTOP.Amount = item.Amount;
                            objWTOP.EXE_AMT = item.EXE_AMT;
                            objWTOP.Card_Cash = item.Card_Cash;
                            objWTOP.Remark = item.Remark;
                            objWTOP.COMPANY_CODE = BaseCompanyCode;
                            objWTOP.Currloc = BaseLocationCode;
                            List_OILEXPDET.Add(objWTOP);
                        }
                    }
                    XmlSerializer xmlSerializer1 = new XmlSerializer(List_OILEXPDET.GetType());
                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, List_OILEXPDET);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc4 = new XmlDocument();
                    List<WEBX_FLEET_ENROUTE_EXP> List_WFEXP = new List<WEBX_FLEET_ENROUTE_EXP>();

                    if (fleetList != null)
                    {
                        foreach (var item in fleetList)
                        {
                            WEBX_FLEET_ENROUTE_EXP objWFEE = new WEBX_FLEET_ENROUTE_EXP();
                            objWFEE.Polarity = item.Polarity;
                            objWFEE.TripsheetNo = item.TripsheetNo;
                            objWFEE.id = item.id;
                            objWFEE.Amount_Spent = item.Amount_Spent;
                            objWFEE.BillNo = item.BillNo;
                            objWFEE.Dt = item.Dt;
                            objWFEE.Exe_Appr_Amt = item.Exe_Appr_Amt;
                            objWFEE.Remarks = item.Remarks;

                            List_WFEXP.Add(objWFEE);
                        }
                    }
                    XmlSerializer xmlSerializer4 = new XmlSerializer(List_WFEXP.GetType());
                    using (MemoryStream xmlStream4 = new MemoryStream())
                    {
                        xmlSerializer4.Serialize(xmlStream4, List_WFEXP);
                        xmlStream4.Position = 0;
                        xmlDoc4.Load(xmlStream4);
                    }

                    #endregion

                    #region XML

                    string XML = "<root>";
                    XML = XML + "<Closing_Km>" + VW.WFVI.f_closure_closekm + "</Closing_Km>";
                    XML = XML + "<VehicleNo>" + VW.WFVI.VehicleNo + "</VehicleNo>";
                    XML = XML + "<TripSheet_StartLoc>" + VW.WFVI.TripSheet_StartLoc + "</TripSheet_StartLoc>";
                    XML = XML + "<TripSheet_EndLoc>" + VW.WFVI.TripSheet_EndLoc + "</TripSheet_EndLoc>";
                    XML = XML + "<PreparedBy>" + VW.WFVI.PreparedBy + "</PreparedBy>";
                    XML = XML + "<CheckedBy>" + VW.WFVI.CheckedBy + "</CheckedBy>";
                    XML = XML + "<ApprovedBy>" + VW.WFVI.ApprovedBy + "</ApprovedBy>";
                    XML = XML + "<Oper_Close_Dt>" + VW.WFVI.Oper_Close_Dt + "</Oper_Close_Dt>";
                    XML = XML + "<AuditedBy>" + VW.WFVI.AuditedBy + "</AuditedBy>";
                    XML = XML + "<Category>" + VW.WFVI.Category + "</Category>";
                    XML = XML + "<WFEE_Total>" + VW.WFEE.Total + "</WFEE_Total>";
                    XML = XML + "</root>";

                    #endregion

                    //string SQRY = "exec Usp_Operationally_Financial_Close_TripClose '" + CloseVehicle + "','" + xmlDoc1.InnerXml + "','" + xmlDoc4.InnerXml +
                    // "','" + XML + "','" + VW.WFVI.VSlipNo + "'";

                    DT = FS.Operationally_Financial_Close(CloseVehicle, xmlDoc1.InnerXml, xmlDoc4.InnerXml, XML, VW.WFVI.VSlipNo);
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("TripCloseDone", new { No = DT.Rows[0][2].ToString() });
        }

        public ActionResult TripCloseDone(string No)
        {
            ViewBag.No = No;
            return View();
        }

        public ActionResult EnRouteExpenses(int id)
        {
            WEBX_FLEET_ENROUTE_EXP obj = new WEBX_FLEET_ENROUTE_EXP();
            obj.SRNO = id;
            return PartialView("_EnRouteExpenses", obj);
        }

        public ActionResult _Tripsheet_HSD_OIL_Expenses(int id, string startkm)
        {
            WEBX_TRIPSHEET_OILEXPDET obj = new WEBX_TRIPSHEET_OILEXPDET();
            obj.SRNO = id;
            obj.Last_Km_Read = startkm;
            obj.Diesel_Ltr = "0";
            obj.Diesel_Rate = 0;
            obj.Amount = "0";
            obj.EXE_AMT = "0";
            obj.BillDt = System.DateTime.Now;
            return PartialView("_Tripsheet_HSD_OIL_Expenses", obj);
        }

        #endregion

        #region TripOpen

        public ActionResult TripOpen()
        {
            //if (BaseFinYear == CurrFinYear)
            //{
                TripOpen TO = new TripOpen();
                WEBX_FLEET_VEHICLE_ISSUE WFVI = new WEBX_FLEET_VEHICLE_ISSUE();
                WEBX_TRIPSHEET_ADVEXP WTA = new WEBX_TRIPSHEET_ADVEXP();
                List<VW_Fleet_CheckList_Module> ObjCheckLits = new List<VW_Fleet_CheckList_Module>();
                List<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO> ListFuelSlipNo = new List<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO>();
                Webx_Fleet_CheckList_hdr objFleetCheckList = new Webx_Fleet_CheckList_hdr();
                List<Webx_Fleet_CheckList_Detail> CheclListDetailList = new List<Webx_Fleet_CheckList_Detail>();
                objFleetCheckList.Checked_Dt = System.DateTime.Now;
                objFleetCheckList.Approved_Dt = System.DateTime.Now;
                TO.WFCLH = objFleetCheckList;

                PaymentControl objpayment = new PaymentControl();
                TO.ListCheckListDetail = FS.GetFleetCheckList().ToList();
                WFVI.Validdt1 = System.DateTime.Now;
                WFVI.Validdt2 = System.DateTime.Now;
                WTA.AdvDate = System.DateTime.Now;
                WTA.BranchCode = BaseLocationCode;
                WFVI.TripSheet_StartLoc = BaseLocationCode;
                WFVI.TripSheet_EndLoc = BaseLocationCode;
                WFVI.TripSheet_EndLocName = BaseLocationName;
                TO.WFVI = WFVI;
                TO.WTSA = WTA;
                TO.ListFCM = ObjCheckLits;
                TO.ListFuelSlipNo = ListFuelSlipNo;
                TO.PC = objpayment;
                return View(TO);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        #region Get Valid Driver

        public JsonResult GetValidDriver(string VechicalNo)
        {
            List<WEBX_FLEET_DRIVERMST> ListGetValidDriver = FS.GetValidDriverList("", BaseLocationCode, "Y", VechicalNo);


            var SearchList = (from e in ListGetValidDriver
                              select new
                              {
                                  Value = e.Driver_Id,
                                  Text = e.Driver_Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Get Valid Vehicle

        public JsonResult GetValidVehicle(string VendorType)
        {
            List<webx_VEHICLE_HDR> ListGetValidDriver = FS.GetValidDriverList("", BaseLocationCode, VendorType);


            var SearchList = (from e in ListGetValidDriver
                              select new
                              {
                                  Value = e.VEHNO,
                                  Text = e.VEHICLE,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region GetDriverDetails_New

        public JsonResult GetDriverDetails_New(string Driver_Id)
        {
            DataTable dtDriverdetail = FS.GetDriverDetails_New(Driver_Id);
            return new JsonResult()
            {

                Data = new
                {
                    driver_status = dtDriverdetail.Rows[0][1].ToString(),
                    License_No = dtDriverdetail.Rows[0][4].ToString(),
                    Driver_Name = dtDriverdetail.Rows[0][3].ToString(),
                    Valdity_dt = dtDriverdetail.Rows[0][5].ToString(),
                    Manual_Driver_Code = dtDriverdetail.Rows[0][6].ToString(),
                    Trip_Count = dtDriverdetail.Rows[0][9].ToString(),
                    Mobileno = dtDriverdetail.Rows[0][10].ToString()
                }
            };
        }

        #endregion

        #region Get Vechical KM

        public JsonResult GetVechicalKM(string VechicalNo)
        {
            DataTable dtVechicalKMdetail = FS.GetVechicalKM(VechicalNo);
            return new JsonResult()
            {
                Data = new
                {
                    current_KM_Read = dtVechicalKMdetail.Rows[0][0].ToString(),
                }
            };

        }

        #endregion

        #region Add TripOpen

        public ActionResult AddTripOpen(TripOpen ObjTripOpen, string Transit_Time, List<WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO> FuelSleepList, List<Webx_Fleet_CheckList_Detail> FleetCheckList, bool ChkslipProvide, PaymentControl PaymentControl)
        {
            DataTable Dt = new DataTable();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                //Check Vehicle Status
                string query = "SELECT Vehicle_Status FROM webx_VEHICLE_HDR WITH(NOLOCK) WHERE VEHNO = '" + ObjTripOpen.WFVI.VehicleNo + "'";
                DataTable DtQuery = GF.GetDataTableFromSP(query);
                if (!string.IsNullOrEmpty(DtQuery.Rows[0][0].ToString()) && DtQuery.Rows[0][0].ToString() == "In Transit")
                {
                    trn.Rollback();
                    con.Close();
                    ViewBag.StrError = "Error : " + ObjTripOpen.WFVI.VehicleNo + " , This Vehicle is " + DtQuery.Rows[0][0].ToString() + " , You cannot Open NewTrip.";
                    return View("Error");
                }

                //Get NextDocumentCode
                DataTable dtNextCode = FS.WebX_SP_GetNextDocumentCode(BaseLocationCode, BaseFinYear.Substring(0, 4), "TS");
                ObjTripOpen.WFVI.NextDocumentCode = dtNextCode.Rows[0]["NextDocumentCode"].ToString();
                ObjTripOpen.WFVI.finYear = BaseFinYear.Split('-')[0].ToString();
                ObjTripOpen.WFVI.yearSuffix = BaseYearVal;
                ObjTripOpen.WFVI.CompanyCode = BaseCompanyCode.ToString();
                //Insert Driver Process

                ObjTripOpen.WFVI.COMPANY_CODE = BaseCompanyCode;

                if (ObjTripOpen.WFVI.Transit_HH == null)
                {
                    ObjTripOpen.WFVI.Transit_HH = "0";
                }
                if (ObjTripOpen.WFVI.Transit_MM == null)
                {
                    ObjTripOpen.WFVI.Transit_MM = "0";
                }
                ObjTripOpen.WFVI.Transit_Time = ObjTripOpen.WFVI.Transit_HH + ":" + ObjTripOpen.WFVI.Transit_MM;
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(ObjTripOpen.WFVI.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, ObjTripOpen.WFVI);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                //Details of Advance Taken
                XmlDocument xmlDoc1 = new XmlDocument();
                if (Convert.ToInt32(Convert.ToDecimal(ObjTripOpen.WTSA.AdvAmt)) > 0)
                {
                    ObjTripOpen.WTSA.COMPANY_CODE = BaseCompanyCode;
                    XmlSerializer xmlSerializer1 = new XmlSerializer(ObjTripOpen.WTSA.GetType());
                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, ObjTripOpen.WTSA);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }
                }
                else
                {
                    xmlDoc1.InnerXml = "<?xml version='1.0'?><WEBX_TRIPSHEET_ADVEXP xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'></WEBX_TRIPSHEET_ADVEXP>";
                }

                //Fuel Slip Entry
                XmlDocument xmlDoc2 = new XmlDocument();
                if (FuelSleepList.Sum(c => c.AMOUNT) > 0)
                {
                    XmlSerializer xmlSerializer2 = new XmlSerializer(FuelSleepList.GetType());
                    using (MemoryStream xmlStream2 = new MemoryStream())
                    {
                        xmlSerializer2.Serialize(xmlStream2, FuelSleepList);
                        xmlStream2.Position = 0;
                        xmlDoc2.Load(xmlStream2);
                    }
                }
                else
                {
                    xmlDoc2.InnerXml = "<?xml version='1.0'?><ArrayOfWEBX_FLEET_TRIPSHEET_FUEL_SLIPNO xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'><WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO></WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO></ArrayOfWEBX_FLEET_TRIPSHEET_FUEL_SLIPNO>";
                }

                //CHECKLIST
                XmlDocument xmlDoc3 = new XmlDocument();
                XmlSerializer xmlSerializer3 = new XmlSerializer(ObjTripOpen.WFCLH.GetType());
                using (MemoryStream xmlStream3 = new MemoryStream())
                {
                    xmlSerializer3.Serialize(xmlStream3, ObjTripOpen.WFCLH);
                    xmlStream3.Position = 0;
                    xmlDoc3.Load(xmlStream3);
                }
                //CHECKLIST Partial View
                XmlDocument xmlDoc4 = new XmlDocument();
                XmlSerializer xmlSerializer4 = new XmlSerializer(FleetCheckList.GetType());
                using (MemoryStream xmlStream4 = new MemoryStream())
                {
                    xmlSerializer4.Serialize(xmlStream4, FleetCheckList);
                    xmlStream4.Position = 0;
                    xmlDoc4.Load(xmlStream4);
                }
                //CHECKLIST Partial View
                XmlDocument xmlDoc5 = new XmlDocument();
                XmlSerializer xmlSerializer5 = new XmlSerializer(PaymentControl.GetType());
                using (MemoryStream xmlStream5 = new MemoryStream())
                {
                    xmlSerializer5.Serialize(xmlStream5, PaymentControl);
                    xmlStream5.Position = 0;
                    xmlDoc5.Load(xmlStream5);
                }


                #region Transaction Validations
                if (!string.IsNullOrEmpty(PaymentControl.PaymentMode))
                {
                    if (PaymentControl.PaymentMode.ToUpper() == "BANK" && PaymentControl.PayAmount > 0)
                    {
                        DataTable DT = OS.Duplicate_ChqNO(PaymentControl.ChequeNo, PaymentControl.ChequeDate.ToString("dd MMM yyyy"));

                        string Cnt = DT.Rows[0][0].ToString();
                        if (Cnt == "F")
                        {
                            ViewBag.StrError = "Duplicate Cheque Entered";
                            return View("Error");
                        }
                        if (Cnt != "T")
                        {
                            ViewBag.StrError = "Duplicate Cheque Entered";
                            return View("Error");
                        }
                    }

                    if (PaymentControl.PaymentMode.ToUpper() == "CASH" && PaymentControl.PayAmount > 0)
                    {
                        string FIN_StartDt = "";
                        FIN_StartDt = "01 Apr " + BaseYearVal.Split('_')[0];

                        DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, Convert.ToDateTime(ObjTripOpen.WFVI.VSlipDt).ToString("dd MMM yyyy"), FIN_StartDt, BaseYearVal, PaymentControl.PayAmount.ToString());

                        string Cnt = DT.Rows[0][0].ToString();
                        if (Cnt == "F")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                            return View("Error");
                        }

                        if (Cnt != "T")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                            return View("Error");
                        }
                    }
                }
                #endregion

                Dt = FS.Insert_TripOpen(BaseUserName, xmlDoc.InnerXml, xmlDoc1.InnerXml, xmlDoc2.InnerXml, xmlDoc3.InnerXml, xmlDoc4.InnerXml, xmlDoc5.InnerXml, BaseLocationCode);

                //string sqlAcc2 = "exec Usp_FleetTransaction '" + BaseLocationCode + "','" + "3" + "','" + ObjTripOpen.WFVI.NextDocumentCode + "','" + BaseYearValFirst + "','" + BaseYearValFirst + "','" + BaseCompanyCode + "'";
                //DataTable DtAcc2 = GF.GetDataTableFromSP(sqlAcc2);
            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }

            string NextCode = "", S = "";
            NextCode = Dt.Rows[0]["NextDocumentCode"].ToString();
            S = Dt.Rows[0]["Message"].ToString();
            return RedirectToAction("TripOpenDone", new { No = NextCode, S = S });
        }

        public ActionResult TripOpenDone(string No, string S)
        {
            ViewBag.NextCode = No;
            ViewBag.S = S;
            return View();
        }

        #endregion

        public ActionResult ADDFuelSlipEntry(int id)
        {
            WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO ObjFuelSlip = new WEBX_FLEET_TRIPSHEET_FUEL_SLIPNO();
            ObjFuelSlip.BrcdCode = BaseLocationCode;
            ObjFuelSlip.Id = id;

            return PartialView("_FuelSlipList", ObjFuelSlip);
        }

        public JsonResult GetDocumentNoManullyNoCheck(string DocumentNo)
        {
            string CNT = "0";
            DataTable DT = new DataTable();
            DT = FS.CheckDocumentNoDetails(DocumentNo);
            CNT = DT.Rows[0][0].ToString();

            return new JsonResult()
            {
                Data = new
                {
                    CNT = CNT,
                }
            };
        }

        #endregion

        #region Fleet Fuel Managment

        public ActionResult FleetFuelMgtCriteria(string Mode, string Type) //mode - add/edit & type-fuelslipentry/edit
        {
            vw_FleetFuelMgt objvVM = new vw_FleetFuelMgt();
            objvVM.Mode = Mode;
            return View(objvVM);
        }//actionList use fot Type

        public ActionResult FuelTripSheetAllList(vw_FleetFuelMgt VM)
        {
            if (VM.FuelFilterType == "T")
            {
                return RedirectToAction("FuelTripSheetList", new { TripSheetNo = VM.TripSheetNo, FromDate = GF.FormateDate(VM.FromDate), ToDate = GF.FormateDate(VM.ToDate), TripActionType = VM.TripActionType, TripFTType = VM.TripFTType, Mode = VM.Mode, FuelFilterType = VM.FuelFilterType });
            }
            else
            {
                return RedirectToAction("FuelTHCList", new { THCNO = VM.THCNo, FromDate = GF.FormateDate(VM.FromDate), ToDate = GF.FormateDate(VM.ToDate) });
            }
        }

        public ActionResult FuelTripSheetList(string TripSheetNo, string FromDate, string ToDate, string TripActionType, string TripFTType, string Mode, string FuelFilterType)
        {
            vw_FleetFuelMgt VM = new vw_FleetFuelMgt();
            VM.ListTSFF = new List<vw_FleetFuelList>();
            List<vw_FleetFuelList> FleetFuelList = new List<vw_FleetFuelList>();
            if (Mode == "Add")
            {
                if (FuelFilterType == "T")
                {
                    DataTable DT = FS.Get_TripFuelListForEntry(TripSheetNo, FromDate, ToDate, TripActionType, BaseLocationCode, TripFTType, BaseCompanyCode);
                    VM.ListTSFF = DataRowToObject.CreateListFromTable<vw_FleetFuelList>(DT);
                    ViewBag.TripActionType = TripActionType;
                }
            }
            if (Mode == "Edit")
            {
                if (FuelFilterType == "T")
                {
                    DataTable DT = FS.Get_TripFuelListForEntry(TripSheetNo, FromDate, ToDate, TripActionType, BaseLocationCode, TripFTType, BaseCompanyCode);
                    VM.ListTSFF = DataRowToObject.CreateListFromTable<vw_FleetFuelList>(DT);
                    ViewBag.TripActionType = TripActionType;
                }
            }

            return View(VM);
        }

        public ActionResult FuelTripSheetSubmit(string id, string mode)
        {
            vw_FleetFuelList objissue = new vw_FleetFuelList();
            DataTable DT = FS.Get_FuelSlip_By_Id(id);
            objissue = DataRowToObject.CreateListFromTable<vw_FleetFuelList>(DT).FirstOrDefault();

            DataTable DT1 = FS.Get_FuelSlip_Row_By_ID(id, BaseLocationCode);
            objissue.FFRList = DataRowToObject.CreateListFromTable<vw_FleetFuelRowList>(DT1);
            ViewBag.mode = mode;
            return View(objissue);
        }

        [HttpPost]
        public ActionResult FuelTripSheetSubmit(vw_FleetFuelList vm, List<vw_FleetFuelRowList> TripAddList)
        {
            string mFuelCashAccFlag = "N";
            vm.FFRList = new List<vw_FleetFuelRowList>();
            if (vm.Mode == "FSE")
            {
                DataTable DT1 = FS.USP_FLEET_INSERT_FUELSLIP_DETAILS(vm.VSlipNo, vm.FFRModel.FUELSLIPNO, vm.FFRModel.FUELVENDOR, vm.FFRModel.QUANTITYINLITER, vm.FFRModel.RATE, vm.FFRModel.AMOUNT, BaseUserName, BaseLocationCode, vm.VehicleNo, vm.FFRModel.SLIPTYPE, BaseCompanyCode,BaseFinYear, BaseYearVal);
            }
            if (vm.Mode == "FSEEDIT")
            {
                foreach (var item in TripAddList)
                {
                    DataTable DT = FS.Usp_Get_Fuel_Edit_Record(item.SRNO);
                    DataRow dRow = DT.Rows[0];

                    if (dRow["FUELSLIPNO"].ToString() != item.FUELSLIPNO || dRow["FUELVENDOR"].ToString() != item.FUELVENDOR || Convert.ToDecimal(dRow["QUANTITYINLITER"].ToString()) != item.QUANTITYINLITER || Convert.ToDecimal(dRow["RATE"].ToString()) != item.RATE)
                    {
                        DataTable DT1 = FS.USP_FLEET_UPDATE_FUELSLIP_DETAILS(vm.VSlipNo, item.FUELSLIPNO, item.FUELVENDOR, item.QUANTITYINLITER, item.RATE, item.AMOUNT, BaseUserName, BaseLocationCode, vm.VehicleNo, item.SRNO);
                        if (item.SLIPTYPE == 2)
                        {
                            mFuelCashAccFlag = "Y";
                        }
                    }
                }
            }

            return RedirectToAction("FleetFuelDone", new { VSlipId = vm.VSlipNo, mode = vm.Mode, FuelCashAccFlag = mFuelCashAccFlag });
        }

        public ActionResult FleetFuelDone(string VSlipId, string mode, string FuelCashAccFlag)
        {
            string mvoucherno = "";
            if (FuelCashAccFlag == "Y")
            {
                string strMaxMin = "select top 1 VoucherRefno from WEBX_TRIPSHEET_ADVEXP where tripsheetno='" + VSlipId
                                 + "' and VoucherRefno is not null and branchcode='" + BaseLocationCode + "' ORDER BY EntryDt Desc";
                DataTable Dt1 = GF.getdatetablefromQuery(strMaxMin);
                mvoucherno = Dt1.Rows[0][0].ToString();
            }
            ViewBag.VSlipId = VSlipId;
            return View();
        }

        public ActionResult FuelTHCList(string THCNO, string FromDate, string ToDate)
        {
            vw_FleetFuelMgt VM = new vw_FleetFuelMgt();
            VM.ListTSFF = new List<vw_FleetFuelList>();
            List<vw_FleetFuelList> FleetFuelList = new List<vw_FleetFuelList>();

            DataTable DT = FS.USP_FLEET_THC_FUEL_QUERY(THCNO, FromDate, ToDate, BaseLocationCode, BaseCompanyCode);
            VM.ListTSFF = DataRowToObject.CreateListFromTable<vw_FleetFuelList>(DT);

            return View(VM);
        }

        #endregion

        #region Job Order Generation,  Operational Close,  Financial Close

        /*
         
        usp_Job_OrderHdrInsertUpdate
        gvTaskDetails :- usp_job_OrderDetInsertUpdate
        gvSMTaskDet :- usp_job_OrderDetInsertUpdate,usp_SMJob_OrderDetInsert
        dgSpare :- usp_Job_EstSparePart
         
        */

        /* 
         changed SP_Name:-
        [usp_Job_AllWorkGroupList],[usp_Job_AllWorkGroupList_SKU],Usp_JobOrder
         */

        public ActionResult PrepareJob(string ORDERNo)
        {
            //if (BaseFinYear == CurrFinYear || BaseUserName == "RC2453" || BaseUserName == "RC2454" || BaseUserName == "CYGNUSTEAM")
            //{
                JobOrderViewModel JOVW = new JobOrderViewModel();
                WEBX_FLEET_PM_JOBORDER_HDR Obj_ObjWFPJH = new WEBX_FLEET_PM_JOBORDER_HDR();
                JOVW.ListAssetType = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "VEHASSTYP" && c.StatusCode.ToUpper() == "Y").ToList();//.GetGeneralMasterList("VEHASSTYP");
                JOVW.ListJOBCARDTYP = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "JOBCARDTYP" && c.StatusCode.ToUpper() == "Y").ToList(); //GetGeneralMasterList("JOBCARDTYP");
                JOVW.ListVendor_List = FS.Get_Vendor_List(BaseLocationCode);
                JOVW.ListLocation = MS.GetLocationDetails();

                /* Job Order Advance Changes */
                PaymentControl objpayment = new PaymentControl();
                List<CYGNUS_Job_Order_AdvanceEntry> ListCJOAdv = new List<CYGNUS_Job_Order_AdvanceEntry>();
                JOVW.PC = objpayment;
                /* Job Order Advance Changes */

                if (ORDERNo == null || ORDERNo == "")
                {
                    JOVW.ISClose = "0";
                    Obj_ObjWFPJH.JOB_ORDER_DT = System.DateTime.Now;
                    Obj_ObjWFPJH.SENDDT_WORKSHOP = System.DateTime.Now;
                    Obj_ObjWFPJH.RETURNDT_WORKSHOP = System.DateTime.Now;
                    JOVW.ListWFPJOD = new List<WEBX_FLEET_PM_JOBORDER_DET>();
                    JOVW.ListWFPJOSED = new List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET>();
                    //  JOVW.ListWFVSMM = new List<Webx_Fleet_Vehicle_SMTask_Mst>();
                }
                else
                {
                    Obj_ObjWFPJH = FS.Get_JOBORDER_HDRList(ORDERNo).First();
                    JOVW.ListWFPJOD = FS.Get_JOBORDER_DETList(ORDERNo);
                    JOVW.ListWFPJOSED = FS.Get_SPARE_EST_DETList(ORDERNo);

                    /* Job Order Advance Changes */
                    JOVW.ListCJOAdv = FS.Get_JOBORDER_Advance(ORDERNo);
                    /* Job Order Advance Changes */

                    if (JOVW.ListWFPJOD.Count() == 0)
                    {
                        JOVW.ListWFPJOD = new List<WEBX_FLEET_PM_JOBORDER_DET>();
                    }
                    if (JOVW.ListWFPJOSED.Count() == 0)
                    {
                        JOVW.ListWFPJOSED = new List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET>();
                    }
                    JOVW.ISClose = "1";
                    JOVW.ListBindSummary = DataRowToObject.CreateListFromTable<VW_JOBSHEET_SUMMARY>(FS.GetVW_JOBSHEET_SUMMARY(ORDERNo));
                }
                JOVW.ObjWFPJH = Obj_ObjWFPJH;

                return View(JOVW);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        [HttpPost]
        public ActionResult PrepareJob(JobOrderViewModel JOVW, List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET> Task_Spare_PartList, List<WEBX_FLEET_PM_JOBORDER_DET> TaskDetailsList, PaymentControl PaymentControl)
        {
            try
            {
                List<webx_VEHICLE_HDR> CMPList = FS.Get_Vehicles_List(JOVW.ObjWFPJH.VEHNO,BaseLocationCode);
                webx_VEHICLE_HDR CMP = CMPList.First();

                /* Job Order Advance Changes */
                PaymentControl ObjPC = new PaymentControl();
                if (PaymentControl != null)
                {
                    ObjPC = PaymentControl;
                }
                if (JOVW.ObjWFPJH.TOT_ESTIMATED_COST < PaymentControl.PayAmount)
                {
                    ViewBag.StrError = "Please Enter Advance Amount Less Than Total Estimated Cost.";
                    return View("Error");
                }
                /* Job Order Advance Changes */

                //if (/*CMP.Vehicle_Available == "AVAILABLE" || */JOVW.ObjWFPJH.ORDER_TYPE == "04")
                //{
                System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
                dtfi.ShortDatePattern = "dd/MM/yyyy";
                dtfi.DateSeparator = "/";
                dtfi.ShortTimePattern = "hh:mm tt";
                DateTime mIssueDt = new DateTime();

                string txtOrderDate = JOVW.ObjWFPJH.JOB_ORDER_DT.ToString("dd MMM yyyy");
                string txtDCSendDt = JOVW.ObjWFPJH.SENDDT_WORKSHOP.ToString("dd MMM yyyy");
                string txtDCReturnDt = JOVW.ObjWFPJH.RETURNDT_WORKSHOP.ToString("dd MMM yyyy");

                if (txtOrderDate != "")
                {
                    mIssueDt = Convert.ToDateTime(txtOrderDate, dtfi);
                }
                DateTime mEstSendDt = new DateTime();

                mEstSendDt = Convert.ToDateTime(txtDCSendDt.ToString(), dtfi);
                DateTime mEstReturnDt = new DateTime();

                mEstReturnDt = Convert.ToDateTime(txtDCReturnDt.ToString(), dtfi);

                if (mEstSendDt < mIssueDt)
                {
                    ViewBag.StrError = "Send date to Workshop cannot be less than Job sheet Date !";
                    return View("Error");
                }
                if (mEstReturnDt < mEstSendDt)
                {
                    ViewBag.StrError = "Estimated return date to Workshop cannot be less than Send date to Workshop !";
                    return View("Error");
                }

                string OrderNo = "";

                // SqlConnection conn = new SqlConnection(Connstr);
                // SqlTransaction trans;
                // conn.Open();
                //trans = conn.BeginTransaction();

                try
                {
                    #region JobOrderNo Generation

                    //string YearVal = "";
                    //string msql = "USP_CREATE_JOB_ORDER_NO";
                    //SqlCommand cmd4 = new SqlCommand(msql, conn, trans);
                    //cmd4.CommandType = CommandType.StoredProcedure;

                    //YearVal = BaseYearVal;

                    //cmd4.Parameters.Add("@BRCD", SqlDbType.VarChar).Value = Convert.ToString(BaseLocationCode);
                    //cmd4.Parameters.Add("@FIN_YEAR", SqlDbType.VarChar).Value = YearVal;
                    //SqlDataReader dr4 = cmd4.ExecuteReader();
                    //if (dr4.HasRows)
                    //{
                    //    while (dr4.Read())
                    //    {
                    //        OrderNo = Convert.ToString(dr4[0]);
                    //    }
                    //}

                    //dr4.Close();

                    #endregion

                    #region Task Details

                    string txtVehicleNo = JOVW.ObjWFPJH.VEHNO;
                    string ddlOrderType = JOVW.ObjWFPJH.ORDER_TYPE;
                    string lblOrderStatus = "Open";
                    string ddlSCtype = JOVW.ObjWFPJH.SERVICE_CENTER_TYPE;
                    string ddlWLoc = JOVW.ObjWFPJH.WS_LOCCODE;
                    string ddlVendor = JOVW.ObjWFPJH.VENDOR_CODE;
                    string txt_Tmp_CurrentKM = Convert.ToString(JOVW.ObjWFPJH.KM_READING);
                    string txttotEstHrs = Convert.ToString(JOVW.ObjWFPJH.TOT_EST_LABOUR_HRS);
                    string txttotEstCost = Convert.ToString(JOVW.ObjWFPJH.TOT_EST_LABOUR_COST);
                    string txtEstLabourHrs = Convert.ToString(JOVW.ObjWFPJH.Estimated_Labour_Hrs);
                    string txtTotalEstimated = Convert.ToString(JOVW.ObjWFPJH.TOT_ESTIMATED_COST);
                    string txtTotalPartCost = Convert.ToString(JOVW.ObjWFPJH.Total_Estimated_Part_Cost);
                    string ddlAssetType = JOVW.ObjWFPJH.Asset_Type;
                    string ddlContainerReefer = JOVW.ObjWFPJH.Unit_Type;

                    string OrdHDR_InUp = "<root><usp_Job_OrderHdrInsertUpdate>";
                    OrdHDR_InUp = OrdHDR_InUp + "<OrderNo>" + OrderNo + "</OrderNo>";
                    OrdHDR_InUp = OrdHDR_InUp + "<PMSCHCD></PMSCHCD>";
                    OrdHDR_InUp = OrdHDR_InUp + "<VehicleNo>" + txtVehicleNo + "</VehicleNo>";
                    OrdHDR_InUp = OrdHDR_InUp + "<OrderType>" + ddlOrderType + "</OrderType>";
                    OrdHDR_InUp = OrdHDR_InUp + "<OrderDate>" + txtOrderDate + "</OrderDate>";
                    OrdHDR_InUp = OrdHDR_InUp + "<OrderStatus>" + lblOrderStatus + "</OrderStatus>";
                    OrdHDR_InUp = OrdHDR_InUp + "<SCType>" + ddlSCtype + "</SCType>";
                    if (ddlSCtype == "Vendor")
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<Vendor>" + ddlVendor + "</Vendor>";
                        OrdHDR_InUp = OrdHDR_InUp + "<Work_Loc></Work_Loc>";
                    }
                    if (ddlSCtype == "Workshop")
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<Vendor></Vendor>";
                        OrdHDR_InUp = OrdHDR_InUp + "<Work_Loc>" + ddlWLoc + "</Work_Loc>";
                    }
                    OrdHDR_InUp = OrdHDR_InUp + "<Km_Read>" + txt_Tmp_CurrentKM + "</Km_Read>";
                    OrdHDR_InUp = OrdHDR_InUp + "<EstHrs>" + txttotEstHrs + "</EstHrs>";
                    OrdHDR_InUp = OrdHDR_InUp + "<EstCost>" + txttotEstCost + "</EstCost>";
                    OrdHDR_InUp = OrdHDR_InUp + "<SENDDT_WORKSHOP>" + mEstSendDt.ToString("dd MMM yyyy") + "</SENDDT_WORKSHOP>";
                    OrdHDR_InUp = OrdHDR_InUp + "<RETURNDT_WORKSHOP>" + mEstReturnDt.ToString("dd MMM yyyy") + "</RETURNDT_WORKSHOP>";
                    if (txtEstLabourHrs != "")
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<Estimated_Labour_Hrs>" + txtEstLabourHrs + "</Estimated_Labour_Hrs>";
                    }
                    else
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<Estimated_Labour_Hrs>0</Estimated_Labour_Hrs>";
                    }
                    if (txtTotalEstimated != "")
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<TOT_ESTIMATED_COST>" + txtTotalEstimated + "</TOT_ESTIMATED_COST>";
                    }
                    else
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<TOT_ESTIMATED_COST>0</TOT_ESTIMATED_COST>";
                    }
                    OrdHDR_InUp = OrdHDR_InUp + "<JS_BRCD>" + BaseLocationCode + "</JS_BRCD>";
                    OrdHDR_InUp = OrdHDR_InUp + "<JS_Approve_By>" + BaseUserName + "</JS_Approve_By>";
                    if (txtTotalPartCost == "")
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<TOT_EST_PART_COST>0</TOT_EST_PART_COST>";
                    }
                    else
                    {
                        OrdHDR_InUp = OrdHDR_InUp + "<TOT_EST_PART_COST>" + Convert.ToDecimal(txtTotalPartCost) + "</TOT_EST_PART_COST>";
                    }
                    OrdHDR_InUp = OrdHDR_InUp + "<EntryBy>" + BaseUserName + "</EntryBy>";
                    OrdHDR_InUp = OrdHDR_InUp + "<Company_Code>" + BaseCompanyCode + "</Company_Code>";
                    OrdHDR_InUp = OrdHDR_InUp + "<Asset_Type>" + ddlAssetType + "</Asset_Type>";
                    OrdHDR_InUp = OrdHDR_InUp + "<Unit_Type>" + ddlContainerReefer + "</Unit_Type>";
                    OrdHDR_InUp = OrdHDR_InUp + "<Unit_SerialNo1></Unit_SerialNo1>";
                    OrdHDR_InUp = OrdHDR_InUp + "<Unit_SerialNo2></Unit_SerialNo2>";

                    /* Job Order Advance Changes */
                    OrdHDR_InUp = OrdHDR_InUp + "<IsAdvancePaid>" + JOVW.IsAdvancePaid + "</IsAdvancePaid>";
                    OrdHDR_InUp = OrdHDR_InUp + "<AdvanceAmount>" + ObjPC.PayAmount + "</AdvanceAmount>";
                    /* Job Order Advance Changes */

                    OrdHDR_InUp = OrdHDR_InUp + "</usp_Job_OrderHdrInsertUpdate></root>";

                    //string sql1 = "usp_Job_OrderHdrInsertUpdate";
                    //cmd.Parameters.Add("@Unit_SerialNo1", SqlDbType.VarChar).Value = "";// txtSerialNo1.Text.ToString();
                    //cmd.Parameters.Add("@Unit_SerialNo2", SqlDbType.VarChar).Value = "";//txtSerialNo2.Text.ToString();
                    #endregion

                    #region OrderDetInsertUpdate

                    string OrdDetIntUp = "<root>";
                    if (TaskDetailsList != null)
                    {
                        foreach (var item in TaskDetailsList)
                        {
                            string ddlTaskType = Convert.ToString(item.TaskTypeId);
                            string W_GRPCD = Convert.ToString(item.W_GRPCD);// ((DropDownList)gridrow.FindControl("ddlWorkGroup")).SelectedItem.Value.Trim();
                            string Desc = Convert.ToString(item.W_GRPCD);//((DropDownList)gridrow.FindControl("ddlTask")).SelectedItem.Text.Trim();
                            string TASKCD = Convert.ToString(item.Taskcd);//((DropDownList)gridrow.FindControl("ddlTask")).SelectedItem.Value.Trim();
                            string TaskType = ddlTaskType;
                            string Remarks = item.T_Remarks;
                            string EstHrs = Convert.ToString(item.W_EST_LABOUR_HRS);
                            string EstCost = Convert.ToString(item.W_EST_LABOUR_COST);
                            //string sql = "usp_job_OrderDetInsertUpdate";
                            OrdDetIntUp = OrdDetIntUp + "<TaskDetails>";
                            OrdDetIntUp = OrdDetIntUp + "<OrderNo>" + OrderNo + "</OrderNo>";
                            OrdDetIntUp = OrdDetIntUp + "<PMSCHCD></PMSCHCD>";
                            OrdDetIntUp = OrdDetIntUp + "<W_grpcd>" + W_GRPCD + "</W_grpcd>";
                            OrdDetIntUp = OrdDetIntUp + "<EstHrs>" + Convert.ToDouble(EstHrs) + "</EstHrs>";
                            OrdDetIntUp = OrdDetIntUp + "<EstCost>" + Convert.ToDouble(EstCost) + "</EstCost>";
                            OrdDetIntUp = OrdDetIntUp + "<Taskcd>" + TASKCD + "</Taskcd>";
                            OrdDetIntUp = OrdDetIntUp + "<T_Remarks>" + Remarks + "</T_Remarks>";
                            OrdDetIntUp = OrdDetIntUp + "<SMTask>No</SMTask>";
                            OrdDetIntUp = OrdDetIntUp + "<TaskTypeId>" + Convert.ToInt32(ddlTaskType) + "</TaskTypeId>";
                            OrdDetIntUp = OrdDetIntUp + "</TaskDetails>";
                        }
                    }
                    OrdDetIntUp = OrdDetIntUp + "</root>";

                    #endregion

                    #region

                    string OrdDetIntUp1 = "<root>";
                    OrdDetIntUp1 = OrdDetIntUp1 + "</root>";

                    //foreach (GridViewRow gridrow in gvSMTaskDet.Rows)/*tblSMTaskDet :- 546 :- PrepareJob.aspx*/
                    //{
                    //    HiddenField hfWrkGrpID = (HiddenField)gridrow.FindControl("hfWrkGrpID");
                    //    CheckBox chkSelect = (CheckBox)gridrow.FindControl("chkSelect");
                    //    HiddenField hfTaskId = (HiddenField)gridrow.FindControl("hfTaskId");
                    //    HiddenField hfTaskTypeId = (HiddenField)gridrow.FindControl("hfTaskTypeId");
                    //    TextBox txtEstHrs = (TextBox)gridrow.FindControl("txtEstHrs");
                    //    TextBox txtEstCost = (TextBox)gridrow.FindControl("txtEstCost");
                    //    TextBox txtRemarks = (TextBox)gridrow.FindControl("txtRemarks");

                    //    if (chkSelect.Checked == true)
                    //    {
                    //        string sql = "usp_job_OrderDetInsertUpdate";

                    //        SqlCommand cmd2 = new SqlCommand(sql, conn, trans);

                    //        cmd2.CommandType = CommandType.StoredProcedure;
                    //        cmd2.Parameters.Add("@OrderNo", SqlDbType.VarChar).Value = OrderNo.Trim(); ;
                    //        cmd2.Parameters.Add("@PMSCHCD", SqlDbType.VarChar).Value = "";
                    //        cmd2.Parameters.Add("@W_grpcd", SqlDbType.VarChar).Value = hfWrkGrpID.Value.Trim();
                    //        cmd2.Parameters.Add("@EstHrs", SqlDbType.Float).Value = (txtEstHrs.Text == "" ? 0 : Convert.ToDouble(txtEstHrs.Text));
                    //        cmd2.Parameters.Add("@EstCost", SqlDbType.Float).Value = (txtEstCost.Text == "" ? 0 : Convert.ToDouble(txtEstCost.Text));
                    //        cmd2.Parameters.Add("@Taskcd", SqlDbType.Int).Value = (hfTaskId.Value == "" ? 0 : Convert.ToDouble(hfTaskId.Value));
                    //        cmd2.Parameters.Add("@T_Remarks", SqlDbType.VarChar).Value = txtRemarks.Text;
                    //        cmd2.Parameters.Add("@SMTask", SqlDbType.VarChar).Value = "Yes";
                    //        cmd2.Parameters.Add("@TaskTypeId", SqlDbType.Int).Value = Convert.ToInt32(hfTaskTypeId.Value);

                    //        cmd2.ExecuteNonQuery();
                    //    }
                    //}

                    string OrdDetIn = "<root>";
                    OrdDetIn = OrdDetIn + "</root>";

                    //foreach (GridViewRow gridrow in gvSMTaskDet.Rows)
                    //{
                    //    HiddenField hfWrkGrpID = (HiddenField)gridrow.FindControl("hfWrkGrpID");
                    //    HiddenField hfTaskId = (HiddenField)gridrow.FindControl("hfTaskId");
                    //    HiddenField hfTaskTypeId = (HiddenField)gridrow.FindControl("hfTaskTypeId");
                    //    CheckBox chkSelect = (CheckBox)gridrow.FindControl("chkSelect");
                    //    if (chkSelect.Checked == true)
                    //    {
                    //        string sql = "usp_SMJob_OrderDetInsert";
                    //        SqlCommand cmd2 = new SqlCommand(sql, conn, trans);

                    //        cmd2.CommandType = CommandType.StoredProcedure;
                    //        cmd2.Parameters.Add("@Job_Order_No", SqlDbType.VarChar, 50).Value = OrderNo.Trim(); ;
                    //        cmd2.Parameters.Add("@Vehicle_No", SqlDbType.VarChar, 15).Value = txtVehicleNo.Text.ToString();
                    //        cmd2.Parameters.Add("@Task_Id", SqlDbType.Int).Value = Convert.ToInt32(hfTaskId.Value);
                    //        cmd2.Parameters.Add("@JS_KM", SqlDbType.Decimal).Value = Convert.ToDecimal(txt_Tmp_CurrentKM.Text);
                    //        cmd2.Parameters.Add("@Entry_By", SqlDbType.VarChar, 50).Value = Convert.ToString(Session["empcd"]);
                    //        cmd2.Parameters.Add("@JS_Dt", SqlDbType.VarChar, 13).Value = txtOrderDate.Text.Trim();

                    //        cmd2.ExecuteNonQuery();
                    //    }
                    //}
                    #endregion

                    #region Part Description

                    string EstSPart = "<root>";
                    if (Task_Spare_PartList != null)
                    {
                        foreach (var ite in Task_Spare_PartList)
                        {
                            EstSPart = EstSPart + "<Task_Spare_Part>";
                            string ddlWorkGroup = ite.SKU_GRPCD;
                            string ddlTaskType = Convert.ToString(ite.TaskTypeID);
                            string ddlPartNo = ite.PART_CODE;
                            string txtQty = Convert.ToString(ite.PART_QTY);
                            string txtCostUnit = Convert.ToString(ite.COST_UNIT);
                            string txtCost = Convert.ToString(ite.COST);
                            string txtSpareRemark = ite.S_Remarks;

                            //string spsql = "usp_Job_EstSparePart";
                            // SqlCommand spcmd = new SqlCommand(spsql, conn, trans);
                            //spcmd.CommandType = CommandType.StoredProcedure;

                            EstSPart = EstSPart + "<OrderNo>" + OrderNo + "</OrderNo>";
                            EstSPart = EstSPart + "<PartNo>" + ddlPartNo + "</PartNo>";
                            if (txtQty == "")
                            {
                                EstSPart = EstSPart + "<Qty>0</Qty>";
                            }
                            else
                            {
                                EstSPart = EstSPart + "<Qty>" + Convert.ToDouble(txtQty) + "</Qty>";
                            }
                            if (txtQty == "")
                            {
                                EstSPart = EstSPart + "<Cost_Unit>0</Cost_Unit>";
                            }
                            else
                            {
                                EstSPart = EstSPart + "<Cost_Unit>" + Convert.ToDouble(txtCostUnit) + "</Cost_Unit>";
                            }
                            if (txtQty == "")
                            {
                                EstSPart = EstSPart + "<Cost>0</Cost>";
                            }
                            else
                            {
                                EstSPart = EstSPart + "<Cost>" + Convert.ToDouble(txtCost) + "</Cost>";
                            }
                            EstSPart = EstSPart + "<S_Remark>" + txtSpareRemark + "</S_Remark>";
                            EstSPart = EstSPart + "<SKU_GRPCD>" + ddlWorkGroup + "</SKU_GRPCD>";
                            EstSPart = EstSPart + "<TaskTypeID>" + Convert.ToInt32(ddlTaskType) + "</TaskTypeID>";
                            EstSPart = EstSPart + "</Task_Spare_Part>";
                        }
                    }
                    EstSPart = EstSPart + "</root>";

                    #endregion

                    #region Job Order Advance Entry

                    string ADV_XML = "<root>";
                    if (JOVW.IsAdvancePaid)
                    {
                        string Ledger = "", ChequeNo = "", ChequeDate = "", Staff_Vendor_Name = "";
                        if (ObjPC.PaymentMode.ToUpper() == "CASH")
                        {
                            Ledger = "CAS0002";
                            ChequeNo = null; ChequeDate = "01 Jan 1990";
                        }
                        else if (ObjPC.PaymentMode.ToUpper() == "BANK")
                        {
                            Ledger = ObjPC.BankLedger;
                            ChequeNo = ObjPC.ChequeNo;
                            ChequeDate = ObjPC.ChequeDate.ToString("dd MMM yyyy");
                        }
                        else
                        {
                            ViewBag.StrError = "Please Select Proper Pay Mode Fro Advance.";
                            return View("Error");
                        }

                        if (JOVW.Staff_Vendor.ToUpper() == "STAFF")
                        {
                            Staff_Vendor_Name = MS.GetUserDetails().Where(c => c.UserId == JOVW.Vendor_Employee).First().Name;
                        }
                        else if (JOVW.Staff_Vendor.ToUpper() == "VENDOR")
                        {
                            Staff_Vendor_Name = MS.GetVendorObject().Where(c => c.VENDORCODE == JOVW.Vendor_Employee.Trim().ToUpper()).First().VENDORNAME;
                        }

                        ADV_XML = ADV_XML + "<JobAdvance>";
                        ADV_XML = ADV_XML + "<Staff_Vendor>" + JOVW.Staff_Vendor + "</Staff_Vendor>";
                        ADV_XML = ADV_XML + "<Staff_Vendor_Name>" + Staff_Vendor_Name + "</Staff_Vendor_Name>";
                        ADV_XML = ADV_XML + "<Vendor_Employee>" + JOVW.Vendor_Employee + "</Vendor_Employee>";
                        ADV_XML = ADV_XML + "<PaymentMode>" + ObjPC.PaymentMode + "</PaymentMode>";
                        ADV_XML = ADV_XML + "<PayAmount>" + ObjPC.PayAmount + "</PayAmount>";
                        ADV_XML = ADV_XML + "<Selected_Ledger>" + Ledger + "</Selected_Ledger>";
                        ADV_XML = ADV_XML + "<CashAmount>" + ObjPC.CashAmount + "</CashAmount>";
                        ADV_XML = ADV_XML + "<CashLedger>" + ObjPC.CashLedger + "</CashLedger>";
                        ADV_XML = ADV_XML + "<ChequeAmount>" + ObjPC.ChequeAmount + "</ChequeAmount>";
                        ADV_XML = ADV_XML + "<BankLedger>" + ObjPC.BankLedger + "</BankLedger>";
                        ADV_XML = ADV_XML + "<ChequeNo>" + ChequeNo + "</ChequeNo>";
                        ADV_XML = ADV_XML + "<ChequeDate>" + ChequeDate + "</ChequeDate>";
                        ADV_XML = ADV_XML + "</JobAdvance>";
                    }
                    ADV_XML = ADV_XML + "</root>";

                    #endregion

                    DataTable DT = FS.AddJobOrder(OrdHDR_InUp, OrdDetIntUp, OrdDetIntUp1, OrdDetIn, EstSPart, BaseLocationCode, BaseFinYear, ADV_XML, BaseCompanyCode, BaseYearValFirst);

                    if (DT.Rows[0]["Message"].ToString() == "Done" && DT.Rows[0]["Status"].ToString() == "1")
                    {
                        return RedirectToAction("JobOrderDone", new { JobOrderNo = DT.Rows[0]["JobOrderNo"].ToString() });
                    }
                    else
                    {
                        ViewBag.StrError = DT.Rows[0]["Message"].ToString();
                        return View("Error");
                    }
                }
                catch (Exception e1)
                {
                    ViewBag.StrError = e1.Message.Replace('\n', '_');
                    return View("Error");
                }
                finally
                {
                    //conn.Close();
                }
                //return RedirectToAction("JobOrderDone", new { JobOrderNo = OrderNo });
                //}
                //else
                //{
                //    ViewBag.StrError = "Vehicle Not Available. This vehicle In Transit.";
                //    return View("Error");
                //}
            }
            catch (Exception e1)
            {
                ViewBag.StrError = e1.Message.Replace('\n', '_');
                return View("Error");
            }
            #region Old Code

            /* usp_Job_OrderHdrInsertUpdate :- JOVW */
            /* usp_job_OrderDetInsertUpdate :- TaskDetailsList */

            /* 
             * try
             {
                 System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
                 dtfi.ShortDatePattern = "dd/MM/yyyy";
                 dtfi.DateSeparator = "/";
                 dtfi.ShortTimePattern = "hh:mm tt";
                 DateTime mIssueDt = new DateTime();
                 if (Convert.ToString(JOVW.ObjWFPJH.JOB_ORDER_DT).ToString() != "" || Convert.ToString(JOVW.ObjWFPJH.JOB_ORDER_DT).ToString() != "01 Jan 0001")
                 {
                     mIssueDt = JOVW.ObjWFPJH.JOB_ORDER_DT;
                 }
                 DateTime mEstSendDt = new DateTime();
                 mEstSendDt = JOVW.ObjWFPJH.SENDDT_WORKSHOP;
                 DateTime mEstReturnDt = new DateTime();
                 mEstReturnDt = JOVW.ObjWFPJH.RETURNDT_WORKSHOP;

                 //if (mEstSendDt < mIssueDt)
                 //{
                 //    @ViewBag.StrError = "Send date to Workshop cannot be less than Job sheet Date !";
                 //    return View("Error");
                 //}
                 //if (mEstReturnDt < mEstSendDt)
                 //{
                 //    @ViewBag.StrError = "Estimated return date to Workshop cannot be less than Send date to Workshop !";
                 //    return View("Error");
                 //}

                 #region Job_OrderHdrInsertUpdate

                 string Job_OrderHdrInsertUpdate = "<root>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<PMSCHCD></PMSCHCD>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<VehicleNo>" + JOVW.ObjWFPJH.VEHNO + "</VehicleNo>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<OrderType>" + JOVW.ObjWFPJH.ORDER_TYPE + "</OrderType>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<OrderDate>" + JOVW.ObjWFPJH.JOB_ORDER_DT.ToString("dd MMM yyyy") + "</OrderDate>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<OrderStatus>" + JOVW.ObjWFPJH.ORDER_STATUS + "</OrderStatus>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<SCType>" + JOVW.ObjWFPJH.SERVICE_CENTER_TYPE + "</SCType>";

                 if (JOVW.ObjWFPJH.SERVICE_CENTER_TYPE.Trim() == "Vendor")
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Vendor>" + JOVW.ObjWFPJH.VENDOR_CODE + "</Vendor>";
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Work_Loc></Work_Loc>";
                 }
                 if (JOVW.ObjWFPJH.SERVICE_CENTER_TYPE.Trim() == "Workshop")
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Vendor></Vendor>";
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Work_Loc>" + JOVW.ObjWFPJH.WS_LOCCODE + "</Work_Loc>";
                 }


                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Km_Read>" + JOVW.ObjWFPJH.Current_KM_Reading + "</Km_Read>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<EstHrs>" + JOVW.ObjWFPJH.TOT_EST_LABOUR_HRS + "</EstHrs>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<EstCost>" + JOVW.ObjWFPJH.TOT_EST_LABOUR_COST + "</EstCost>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<SENDDT_WORKSHOP>" + JOVW.ObjWFPJH.SENDDT_WORKSHOP.ToString("dd MMM yyyy") + "</SENDDT_WORKSHOP>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<RETURNDT_WORKSHOP>" + JOVW.ObjWFPJH.RETURNDT_WORKSHOP.ToString("dd MMM yyyy") + "</RETURNDT_WORKSHOP>";

                 if (JOVW.ObjWFPJH.Estimated_Labour_Hrs != null && JOVW.ObjWFPJH.Estimated_Labour_Hrs != 0)
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Estimated_Labour_Hrs>" + JOVW.ObjWFPJH.Estimated_Labour_Hrs + "</Estimated_Labour_Hrs>";
                 }
                 else
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Estimated_Labour_Hrs>0</Estimated_Labour_Hrs>";
                 }

                 if (JOVW.ObjWFPJH.TOT_EST_LABOUR_HRS != null && JOVW.ObjWFPJH.TOT_EST_LABOUR_HRS != 0)
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<TOT_ESTIMATED_COST>" + JOVW.ObjWFPJH.TOT_EST_LABOUR_HRS + "</TOT_ESTIMATED_COST>";
                 }
                 else
                 {
                     Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<TOT_ESTIMATED_COST>0</TOT_ESTIMATED_COST>";
                 }

                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<JS_BRCD>" + BaseLocationCode + "</JS_BRCD>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<JS_Approve_By>" + BaseUserName + "</JS_Approve_By>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<TOT_EST_PART_COST>" + JOVW.ObjWFPJH.TOT_EST_PART_COST + "</TOT_EST_PART_COST>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<EntryBy>" + BaseUserName + "</EntryBy>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Company_Code>" + BaseCompanyCode + "</Company_Code>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Asset_Type>" + JOVW.ObjWFPJH.Asset_Type + "</Asset_Type>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Unit_Type>" + JOVW.ObjWFPJH.Unit_Type + "</Unit_Type>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Unit_SerialNo1>" + JOVW.ObjWFPJH.Unit_SerialNo + "</Unit_SerialNo1>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "<Unit_SerialNo2>" + JOVW.ObjWFPJH.Unit_SerialNo + "</Unit_SerialNo2>";
                 Job_OrderHdrInsertUpdate = Job_OrderHdrInsertUpdate + "</root>";

                 #endregion

                 #region job_OrderDetInsertUpdate

                 string job_OrderDetInsertUpdate = "<root>";
                 if (TaskDetailsList != null)
                 {
                     foreach (var items in TaskDetailsList)
                     {
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<TaskDetails>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<PMSCHCD></PMSCHCD>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<W_GRPCD>" + items.W_GRPCD + "</W_GRPCD>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<EstHrs>" + items.W_EST_LABOUR_HRS + "</EstHrs>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<EstCost>" + items.W_EST_LABOUR_COST + "</EstCost>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<Taskcd>" + items.TaskTypeId + "</Taskcd>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<T_Remarks>" + items.T_Remarks + "</T_Remarks>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<SMTask>No</SMTask>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "<TaskTypeId>" + items.Taskcd + "</TaskTypeId>";
                         job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "</TaskDetails>";
                     }
                 }
                 job_OrderDetInsertUpdate = job_OrderDetInsertUpdate + "</root>";

                 #endregion

                 #region Job_EstSparePart

                 string Job_EstSparePart = "<root>";

                 if (Task_Spare_PartList != null)
                 {
                     foreach (var items in Task_Spare_PartList)
                     {
                         string Qty_SQRY = "exec USP_GetTotalAvailableQTY '" + items.SKU_GRPCD + "' ";
                         DataTable DT_QTY = GF.GetDataTableFromSP(Qty_SQRY);
                         decimal Available_QTY = 0;
                         try
                         {
                             Available_QTY = Convert.ToDecimal(DT_QTY.Rows[0][0].ToString());
                         }
                         catch (Exception)
                         {
                             Available_QTY = 0;
                         }

                         if (Available_QTY < items.PART_QTY)
                         {
                             @ViewBag.StrError = items.SKU_GRPCD + " Enter More Than Available Qty.";
                             return View("Error");
                         }

                         Job_EstSparePart = Job_EstSparePart + "<Task_Spare_Part>";
                         Job_EstSparePart = Job_EstSparePart + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";
                         Job_EstSparePart = Job_EstSparePart + "<PartNo>" + items.PART_CODE + "</PartNo>";
                         Job_EstSparePart = Job_EstSparePart + "<Qty>" + items.PART_QTY + "</Qty>";
                         Job_EstSparePart = Job_EstSparePart + "<Cost_Unit>" + items.COST_UNIT + "</Cost_Unit>";
                         Job_EstSparePart = Job_EstSparePart + "<Cost>" + items.COST + "</Cost>";
                         Job_EstSparePart = Job_EstSparePart + "<S_Remark>" + items.S_Remarks + "</S_Remark>";
                         Job_EstSparePart = Job_EstSparePart + "<W_GRPCD>" + items.W_GRPCD + "</W_GRPCD>";
                         Job_EstSparePart = Job_EstSparePart + "<TaskTypeID>" + items.TaskTypeID + "</TaskTypeID>";
                         Job_EstSparePart = Job_EstSparePart + "</Task_Spare_Part>";
                     }
                 }
                 Job_EstSparePart = Job_EstSparePart + "</root>";

                 #endregion

                 DataTable DT = FS.AddJobOrder(Job_OrderHdrInsertUpdate, job_OrderDetInsertUpdate, Job_EstSparePart, BaseLocationCode, BaseFinYear);

                 if (DT.Rows[0]["Message"].ToString() == "Done" && DT.Rows[0]["Status"].ToString() == "1")
                 {
                     return RedirectToAction("JobOrderDone", new { JobOrderNo = DT.Rows[0]["JobOrderNo"].ToString() });
                 }
                 else
                 {
                     @ViewBag.StrError = DT.Rows[0]["Message"].ToString();
                     return View("Error");
                 }
             }
             catch (Exception ex)
             {
                 @ViewBag.StrError = ex.Message;
                 return View("Error");
             }
             */
            #endregion
        }

        [HttpPost]
        public ActionResult JobOrder_Operational_Financial(JobOrderViewModel JOVW, List<WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET> Task_Spare_PartList, List<WEBX_FLEET_PM_JOBORDER_DET> TaskDetailsList)
        {
            /* usp_Job_JobHdrClose :- JOVW */
            /* usp_Job_Task_InsertUpdate :- gvTaskDetails :- TaskDetailsList */
            /* usp_Job_ActSparePart :- dgSpare :- Task_Spare_PartList */

            System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            DateTime mActReturnDt = new DateTime();
            DateTime mServerDt = DateTime.Now;

            try
            {
                #region Validations

                string txtDCActRetrunDt = Convert.ToDateTime(JOVW.ObjWFPJH.Actual_Date_of_return).ToString("dd MMM yyyy");
                string LblSendDt = Convert.ToDateTime(JOVW.ObjWFPJH.SENDDT_WORKSHOP).ToString("dd MMM yyyy");

                if (txtDCActRetrunDt == "01 Jan 0001" || txtDCActRetrunDt == "01 Jan 1990" || txtDCActRetrunDt == "")
                {
                    ViewBag.StrError = "Please enter Actual date of return";
                    return View("Error");
                }

                mActReturnDt = Convert.ToDateTime(txtDCActRetrunDt, dtfi);
                DateTime mSendDt = new DateTime();
                mSendDt = Convert.ToDateTime(LblSendDt, dtfi);
                if (mActReturnDt < mSendDt)
                {
                    ViewBag.StrError = "Actual return date cannot be less than Send date to Workshop  !";
                    return View("Error");
                }
                if (mActReturnDt > mServerDt)
                {
                    ViewBag.StrError = "Actual return date cannot be greater than today's Date !";
                    return View("Error");
                }

                #endregion

                #region usp_Job_JobHdrClose
                /* usp_Job_JobHdrClose */

                decimal Part = 0;

                if (Task_Spare_PartList != null)
                {
                    Part = Task_Spare_PartList.Sum(c => c.ACT_COST);
                }

                string UJHdrCXML = "<root>";
                UJHdrCXML = UJHdrCXML + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";
                UJHdrCXML = UJHdrCXML + "<TotALCost>" + JOVW.ObjWFPJH.TOT_ACT_LABOUR_COST/*JOVW.ObjWFPJH.TOT_EST_LABOUR_COST*/ + "</TotALCost>";
                UJHdrCXML = UJHdrCXML + "<PartTotCost>" + Part /*JOVW.ObjWFPJH.Total_Actual_Part_Cost*/ + "</PartTotCost>";
                UJHdrCXML = UJHdrCXML + "<CLOSE_KM_READING>" + JOVW.ObjWFPJH.CLOSE_KM_READING + "</CLOSE_KM_READING>";
                UJHdrCXML = UJHdrCXML + "<TOT_ACTUAL_COST>" + JOVW.ObjWFPJH.TOT_ACT_PART_COST /*JOVW.ObjWFPJH.TOT_ESTIMATED_COST*/ + "</TOT_ACTUAL_COST>";

                #region Financial or Operational Close

                if (JOVW.IsFinanciallyClose == "1")
                {
                    UJHdrCXML = UJHdrCXML + "<Job_Order_Close_Type>Financial</Job_Order_Close_Type>";
                }
                else if (JOVW.IsOperatinllyClose == "1")
                {
                    UJHdrCXML = UJHdrCXML + "<Job_Order_Close_Type>Operational</Job_Order_Close_Type>";
                }

                #endregion

                UJHdrCXML = UJHdrCXML + "<JS_Approve_By>" + BaseUserName + "</JS_Approve_By>";
                //UJHdrCXML = UJHdrCXML + "<JOB_Order_Closedt>" + Convert.ToDateTime(JOVW.ObjWFPJH.Actual_Date_of_return).ToString("dd MMM yyyy") + "</JOB_Order_Closedt>";
                UJHdrCXML = UJHdrCXML + "<JOB_Order_Closedt>" + mActReturnDt + "</JOB_Order_Closedt>";
                UJHdrCXML = UJHdrCXML + "</root>";

                #endregion

                #region usp_Job_Task_InsertUpdate
                /* usp_Job_Task_InsertUpdate */

                string UJTIUXML = "<root>";
                if (TaskDetailsList != null)
                {
                    foreach (var item in TaskDetailsList)
                    {
                        UJTIUXML = UJTIUXML + "<TaskDetails>";
                        UJTIUXML = UJTIUXML + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";/*JOB_ORDER_NO*/
                        UJTIUXML = UJTIUXML + "<W_grpcd>" + item.W_GRPCD + "</W_grpcd>";/*W_GRPCD*/
                        UJTIUXML = UJTIUXML + "<WDESC></WDESC>";
                        UJTIUXML = UJTIUXML + "<EstHrs>" + item.W_EST_LABOUR_HRS + "</EstHrs>";/*W_EST_LABOUR_HRS*/
                        UJTIUXML = UJTIUXML + "<EstCost>" + item.W_EST_LABOUR_COST + "</EstCost>";/*W_EST_LABOUR_COST*/
                        UJTIUXML = UJTIUXML + "<ActHrs>" + 0 /*item.W_ACT_LABOUR_HRS*/ + "</ActHrs>";/*W_ACT_LABOUR_HRS*/
                        UJTIUXML = UJTIUXML + "<ActCost>" + item.W_ACT_LABOUR_COST + "</ActCost>";/*W_ACT_LABOUR_COST*/
                        UJTIUXML = UJTIUXML + "<Taskcd>" + item.Taskcd + "</Taskcd>";/*Taskcd*/
                        UJTIUXML = UJTIUXML + "<Taskcd1>" + item.Taskcd + "</Taskcd1>";/*Taskcd*/
                        UJTIUXML = UJTIUXML + "<T_Remarks>" + item.T_Remarks + "</T_Remarks>";/*T_Remarks*/
                        UJTIUXML = UJTIUXML + "<Task_ActionDesc>" + item.TASK_ACTIONDESC + "</Task_ActionDesc>";/*TASK_ACTIONDESC*/
                        UJTIUXML = UJTIUXML + "<Task_Completed>Yes</Task_Completed>";/*Task_Completed*/
                        UJTIUXML = UJTIUXML + "<AMC>" + item.AMC + "</AMC>";/*AMC*/
                        UJTIUXML = UJTIUXML + "<TaskTypeId>" + item.TaskTypeId + "</TaskTypeId>";/*TaskTypeId*/

                        UJTIUXML = UJTIUXML + "</TaskDetails>";
                    }
                }
                UJTIUXML = UJTIUXML + "</root>";

                #endregion

                #region usp_Job_ActSparePart
                /* usp_Job_ActSparePart */

                string UJActSPXML = "<root>";
                if (Task_Spare_PartList != null)
                {
                    foreach (var item in Task_Spare_PartList)
                    {
                        UJActSPXML = UJActSPXML + "<TaskSpareDetails>";
                        UJActSPXML = UJActSPXML + "<OrderNo>" + JOVW.ObjWFPJH.JOB_ORDER_NO + "</OrderNo>";
                        UJActSPXML = UJActSPXML + "<W_GRPCD>" + item.SKU_GRPCD + "</W_GRPCD>";/*SKU_GRPCD*/
                        UJActSPXML = UJActSPXML + "<PartNo>" + item.PART_CODE + "</PartNo>";/*PART_CODE*/
                        UJActSPXML = UJActSPXML + "<S_Remark>" + item.S_Remarks + "</S_Remark>";/*S_Remarks*/
                        UJActSPXML = UJActSPXML + "<ACT_PART_QTY>" + item.ACT_PART_QTY + "</ACT_PART_QTY>";/*ACT_PART_QTY*/
                        UJActSPXML = UJActSPXML + "<ACT_COST_UNIT>" + item.ACT_COST_UNIT + "</ACT_COST_UNIT>";/*ACT_COST_UNIT*/
                        UJActSPXML = UJActSPXML + "<ACT_COST>" + item.ACT_COST + "</ACT_COST>";/*ACT_COST*/
                        UJActSPXML = UJActSPXML + "<Part_Code>" + item.PART_CODE + "</Part_Code>";
                        UJActSPXML = UJActSPXML + "<TaskTypeId>" + item.TaskTypeID + "</TaskTypeId>";/*TaskTypeId*/
                        UJActSPXML = UJActSPXML + "<PART_QTY>" + item.PART_QTY + "</PART_QTY>";/*TaskTypeId*/
                        UJActSPXML = UJActSPXML + "<COST_UNIT>" + item.COST_UNIT + "</COST_UNIT>";/*TaskTypeId*/
                        UJActSPXML = UJActSPXML + "<COST>" + item.COST + "</COST>";/*TaskTypeId*/

                        //Start New Part GST Change Chirag D
                        UJActSPXML = UJActSPXML + "<GSTPer>" + item.GSTPer + "</GSTPer>";
                        //End New Part GST Change Chirag D
                        UJActSPXML = UJActSPXML + "</TaskSpareDetails>";
                    }
                }
                UJActSPXML = UJActSPXML + "</root>";

                #endregion

                DataTable DT = FS.CloseJobOrder(JOVW.ObjWFPJH.JOB_ORDER_NO, UJHdrCXML, UJTIUXML, UJActSPXML, BaseUserName, BaseLocationCode, BaseFinYear);

                if (DT.Rows[0]["Message"].ToString() == "Done" && DT.Rows[0]["Status"].ToString() == "1")
                {
                    return RedirectToAction("JobOrderDone", new { JobOrderNo = DT.Rows[0]["JobOrderNo"].ToString() });
                }
                else
                {
                    @ViewBag.StrError = DT.Rows[0]["Message"].ToString();
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public ActionResult JobOrderDone(string JobOrderNo)
        {
            @ViewBag.No = JobOrderNo;
            return View();
        }

        public ActionResult _Task_Details(int id, string ISClose)
        {
            WEBX_FLEET_PM_JOBORDER_DET ObjWFPJD = new WEBX_FLEET_PM_JOBORDER_DET();
            ObjWFPJD.SRNo = id;
            ObjWFPJD.SMTask = "No";
            ObjWFPJD.ISClose = ISClose;
            return PartialView("_Task_Details", ObjWFPJD);
        }

        public ActionResult _Task_Spare_Part_Details(int id, string ISClose)
        {
            WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET ObjWFPJSED = new WEBX_FLEET_PM_JOBORDER_SPARE_EST_DET();
            ObjWFPJSED.SRNO = id;
            ObjWFPJSED.SMTask = "Yes";
            ObjWFPJSED.ISClose = ISClose;
            return PartialView("_Task_Spare_Part", ObjWFPJSED);
        }

        public JsonResult GetTaskDescription(string id)
        {
            id = id.ToUpper();

            List<WEBX_FLEET_GENERALTASKMST> List = new List<WEBX_FLEET_GENERALTASKMST>();
            List = FS.Get_TaskDescriptionList(id);

            var VehicleList = (from e in List
                               select new
                               {
                                   Value = e.TASKCD,
                                   Text = e.TASKDESC,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPartLoad(string id)
        {
            id = id.ToUpper();
            List<Webx_Fleet_spareParthdr> List = new List<Webx_Fleet_spareParthdr>();
            List = FS.Get_SpareParthdrList(id);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetWork_Group(string id)
        {
            id = id.ToUpper();
            List<WEBX_FLEET_WORKGROUPMST> List = new List<WEBX_FLEET_WORKGROUPMST>();
            List = FS.Get_AllWorkGroupList(id);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Check_Vehicle_NO(string VehicleNo)
        {
            string Current_KM_Read = "", Vehicle_Status = "", Made_by = "", Model_No = "", Type_Name = "", Type_Code = "", Tmp_Current_KM_Read = "",
                JS_Maintainance_Status = "", JOB_ORDER_NO = "", Vehicle_Type = "", IsRecordFound = "0";
            DataTable DT = new DataTable();

            try
            {
                DT = FS.GetValid_Vehicle_NO(VehicleNo);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";

                    Current_KM_Read = DT.Rows[0]["Current_KM_Read"].ToString();
                    Vehicle_Status = DT.Rows[0]["Vehicle_Status"].ToString();
                    Made_by = DT.Rows[0]["Made_by"].ToString();
                    Model_No = DT.Rows[0]["Model_No"].ToString();
                    Type_Name = DT.Rows[0]["Type_Name"].ToString();
                    Type_Code = DT.Rows[0]["Type_Code"].ToString();
                    Tmp_Current_KM_Read = DT.Rows[0]["Tmp_Current_KM_Read"].ToString();
                    JS_Maintainance_Status = DT.Rows[0]["JS_Maintainance_Status"].ToString();
                    JOB_ORDER_NO = DT.Rows[0]["JOB_ORDER_NO"].ToString();
                    Vehicle_Type = DT.Rows[0]["Vehicle_Type"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Current_KM_Read = Current_KM_Read,
                        Vehicle_Status = Vehicle_Status,
                        Made_by = Made_by,
                        Model_No = Model_No,
                        Type_Name = Type_Name,
                        Type_Code = Type_Code,
                        Tmp_Current_KM_Read = Tmp_Current_KM_Read,
                        JS_Maintainance_Status = JS_Maintainance_Status,
                        JOB_ORDER_NO = JOB_ORDER_NO,
                        Vehicle_Type = Vehicle_Type
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                    }
                };
            }
        }

        public JsonResult Get_Vehicles(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_VEHICLE_HDR> CMP = FS.Get_Vehicles_List(searchTerm, BaseLocationCode);

            var users = from user in CMP
                        select new
                        {
                            id = user.VEHNO,
                            text = user.Vehicle_Status

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSparePart(string id)
        {
            id = id.ToUpper();
            DataTable DT = new DataTable();
            DT = FS.GetSparePart_NO(id);
            List<SelectListItem> List = DataRowToObject.CreateListFromTable<SelectListItem>(DT);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Check_SKU_AvailableQTY(decimal Qty, string SKU)
        {
            decimal AvailableQTY = 0;
            string Status = "N";
            DataTable DT = new DataTable();

            string Qty_SQRY = "exec USP_GetTotalAvailableQTY '" + SKU + "' ";
            try
            {
                DataTable DT_QTY = GF.GetDataTableFromSP(Qty_SQRY);
                AvailableQTY = Convert.ToDecimal(DT_QTY.Rows[0][0].ToString());
            }
            catch (Exception)
            {
                AvailableQTY = 0;
            }

            if (Convert.ToDecimal(Qty) <= AvailableQTY)
                Status = "Y";

            return new JsonResult()
            {
                Data = new
                {
                    AvailableQTY = AvailableQTY,
                    Status = Status
                }
            };
        }

        public ActionResult ServiceCentreType_Changes()
        {
            JobOrderViewModel VW = new JobOrderViewModel();
            WEBX_FLEET_PM_JOBORDER_HDR ObjWFPJH = new WEBX_FLEET_PM_JOBORDER_HDR();
            ObjWFPJH.JOB_ORDER_DT = System.DateTime.Now;
            VW.ObjWFPJH = ObjWFPJH;
            VW.ListJOBCARDTYP = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "JOBCARDTYP").ToList(); //GetGeneralMasterList("JOBCARDTYP");
            VW.ListVendor_List = FS.Get_Vendor_List(BaseLocationCode);
            VW.ListLocation = MS.GetLocationDetails();
            return View(VW);
        }

        public JsonResult Check_JobOrder_NO(string No)
        {
            string Job_Order_Date = "", Service_Centre_Type = "", Workshop_Location = "", Vendor_Name = "", IsRecordFound = "", Job_Order_Close_Type = "";
            DataTable DT = new DataTable();
            try
            {
                DT = FS.GetValid_JobOrder_NO(No);
                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";
                    Job_Order_Date = DT.Rows[0]["JOB_ORDER_DT"].ToString();
                    Service_Centre_Type = DT.Rows[0]["SERVICE_CENTER_TYPE"].ToString();
                    Workshop_Location = DT.Rows[0]["WS_LOCCODE"].ToString();
                    Vendor_Name = DT.Rows[0]["VENDOR_CODE"].ToString();
                    Job_Order_Close_Type = DT.Rows[0]["Job_Order_Close_Type"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Job_Order_Date = Job_Order_Date,
                        Service_Centre_Type = Service_Centre_Type,
                        Workshop_Location = Workshop_Location,
                        Vendor_Name = Vendor_Name,
                        Job_Order_Close_Type = Job_Order_Close_Type
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = 0,
                    }
                };
            }
        }

        [HttpPost]
        public ActionResult ServiceCentreType_Changes(JobOrderViewModel VW)
        {
            string Message = "", Status = "";
            DataTable DT = new DataTable();
            try
            {
                if (VW.ObjWFPJH.SERVICE_CENTER_TYPE == "Vendor")
                {
                    VW.ObjWFPJH.WS_LOCCODE = "";
                }
                else if (VW.ObjWFPJH.SERVICE_CENTER_TYPE == "Workshop")
                {
                    VW.ObjWFPJH.VENDOR_CODE = "";
                }

                DT = FS.ServiceCentreType_Changes(VW.ObjWFPJH.JOB_ORDER_NO, VW.ObjWFPJH.SERVICE_CENTER_TYPE, VW.ObjWFPJH.VENDOR_CODE, VW.ObjWFPJH.WS_LOCCODE);
                Message = DT.Rows[0]["Message"].ToString();
                Status = DT.Rows[0]["Status"].ToString();

                if (Message != "Done" && Status != "1")
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
                else
                {
                    return RedirectToAction("JobOrderDone", new { JobOrderNo = VW.ObjWFPJH.JOB_ORDER_NO });
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            //return RedirectToAction("JobOrderDone", new { JobOrderNo = VW.ObjWFPJH.JOB_ORDER_NO });
        }

        #endregion

        #region Job Order Approval(Generate and Closure)

        public ActionResult JobApprovalCriteria()
        {
            JobApprovalViewModel VW = new JobApprovalViewModel();
            VW.ListJobCardType = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "JOBCARDTYP").ToList(); //GetGeneralMasterList("JOBCARDTYP");
            return View(VW);
        }

        [HttpPost]
        public ActionResult JobApprovalList(JobApprovalViewModel VW)
        {
            if (VW.Type == "1")
            {
                @ViewBag.Header = "Closed Jobsheet Approval";
                DataTable DT = FS.Get_ApproveList(VW.JobNo, VW.FromDate.ToString("dd/MM/yyyy"), VW.ToDate.ToString("dd/MM/yyyy"), VW.JobCardType, BaseLocationCode, BaseCompanyCode);
                VW.List_JAGVW = DataRowToObject.CreateListFromTable<JobApprovalGenerationViewModel>(DT);
            }
            else if (VW.Type == "2")
            {
                @ViewBag.Header = "Generation Jobsheet Approval";
                DataTable DT = FS.Get_GenerateApproveList(VW.JobNo, VW.FromDate.ToString("dd/MM/yyyy"), VW.ToDate.ToString("dd/MM/yyyy"), VW.JobCardType, BaseLocationCode, BaseCompanyCode);
                VW.List_JAGVW = DataRowToObject.CreateListFromTable<JobApprovalGenerationViewModel>(DT);
            }

            return View(VW);
        }

        [HttpPost]
        public ActionResult JobApprovalSubmit(JobApprovalViewModel VW, List<JobApprovalGenerationViewModel> JobApprovalList)
        {
            string JobOrderNos = "";
            try
            {
                foreach (var item in JobApprovalList)
                {
                    if (item.IsCheck)
                    {
                        if (item.Approve)
                        {
                            item.JS_Approve = "Yes";
                            item.JS_Approve_By = BaseUserName;
                            item.JS_Reject = "";
                            item.JS_Reject_By = "";
                        }
                        else if (item.Reject)
                        {
                            item.JS_Reject = "Yes";
                            item.JS_Reject_By = BaseUserName;
                            item.JS_Approve = "";
                            item.JS_Approve_By = "";
                        }
                        try
                        {
                            if (VW.Type == "1")
                            {
                                UpdateJobsheetApprovalClosure(item.JOB_ORDER_NO, item.JS_Approve, item.JS_Approve_By, item.JS_Reject, item.JS_Reject_By, BaseLocationCode, item.JS_Approval_Close_Remarks);
                            }
                            else
                            {
                                UpdateJobsheetApproval(item.JOB_ORDER_NO, item.JS_Approve, item.JS_Approve_By, item.JS_Reject, item.JS_Reject_By, BaseLocationCode, item.JS_Approval_Close_Remarks);
                            }

                            if (JobOrderNos == "")
                            {
                                JobOrderNos = item.JOB_ORDER_NO;
                            }
                            else
                            {
                                JobOrderNos = JobOrderNos + "," + item.JOB_ORDER_NO;
                            }

                            if (item.Approve)
                            {
                                AwaitingApprovalMail(item.JOB_ORDER_NO, "Y");
                            }
                            else if (item.Reject)
                            {
                                AwaitingApprovalMail(item.JOB_ORDER_NO, "N");
                            }
                        }
                        catch (Exception ex)
                        {
                            ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                            return View("Error");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("JobApproveDone", new { JobOrderNo = JobOrderNos, Type = VW.Type });
        }

        public void UpdateJobsheetApprovalClosure(string JobOrderNo, string JS_Approve, string JS_Approve_By, string JS_Reject, string JS_Reject_By, string JS_BRCD, string JS_Approval_Remarks)
        {
            SqlParameter[] prm = new SqlParameter[7];

            prm[0] = new SqlParameter("@JOB_ORDER_NO", JobOrderNo);
            prm[1] = new SqlParameter("@JS_Approve", JS_Approve);
            prm[2] = new SqlParameter("@JS_Approve_By", JS_Approve_By);
            prm[3] = new SqlParameter("@JS_Reject", JS_Reject);
            prm[4] = new SqlParameter("@JS_Reject_By", JS_Reject_By);
            prm[5] = new SqlParameter("@JS_Approved_LocCode_Close", JS_BRCD);
            prm[6] = new SqlParameter("@JS_Approval_Remarks", JS_Approval_Remarks);

            string strSQRY = "exec Usp_Update_Jobsheet_Approval_Clouare_NewPortal '" + JobOrderNo + "','" + JS_Approve + "','" + JS_Approve_By + "','" +
                JS_Reject + "','" + JS_Reject_By + "','" + JS_BRCD + "','" + JS_Approval_Remarks + "'";
            int Id = GF.SaveRequestServices(strSQRY.Replace("'", "''"), "UpdateJobsheetApprovalClosure", "", "");
            SqlHelper.ExecuteNonQuery(GF.GetConnstr(), CommandType.StoredProcedure, "Usp_Update_Jobsheet_Approval_Clouare_NewPortal", prm);
        }

        public void UpdateJobsheetApproval(string JobOrderNo, string JS_Approve, string JS_Approve_By, string JS_Reject, string JS_Reject_By, string JS_BRCD, string JS_Approval_Remarks)
        {
            SqlParameter[] prm = new SqlParameter[7];

            prm[0] = new SqlParameter("@JOB_ORDER_NO", JobOrderNo);
            prm[1] = new SqlParameter("@JS_Approve", JS_Approve);
            prm[2] = new SqlParameter("@JS_Approve_By", JS_Approve_By);
            prm[3] = new SqlParameter("@JS_Reject", JS_Reject);
            prm[4] = new SqlParameter("@JS_Reject_By", JS_Reject_By);
            prm[5] = new SqlParameter("@JS_Approved_LocCode", JS_BRCD);
            prm[6] = new SqlParameter("@JS_Approval_Remarks", JS_Approval_Remarks);

            string strSQRY = "exec Usp_Update_Jobsheet_Approval '" + JobOrderNo + "','" + JS_Approve + "','" + JS_Approve_By + "','" +
               JS_Reject + "','" + JS_Reject_By + "','" + JS_BRCD + "','" + JS_Approval_Remarks + "'";
            int Id = GF.SaveRequestServices(strSQRY.Replace("'", "''"), "UpdateJobsheetApproval", "", "");

            SqlHelper.ExecuteNonQuery(GF.GetConnstr(), CommandType.StoredProcedure, "Usp_Update_Jobsheet_Approval", prm);
        }

        public void AwaitingApprovalMail(string strJobOrderNo, string Status)
        {
            DataTable dtCheckAmt = new DataTable();
            dtCheckAmt = FS.Get_dtCheckAmt(strJobOrderNo, BaseUserName);

            DataTable dtLoginUsesDet = new DataTable();
            dtLoginUsesDet = FS.Get_dtLoginUsesDet(BaseUserName);

            if (dtCheckAmt.Rows.Count > 0)
            {
                double vActual_Amt = Convert.ToDouble(dtCheckAmt.Rows[0]["Actual_Amt"].ToString());
                double vApproved_Amt = Convert.ToDouble(dtCheckAmt.Rows[0]["Approved_Amt"].ToString());
                double vLoc_Level = Convert.ToDouble(dtCheckAmt.Rows[0]["Loc_Level"].ToString());

                if (vActual_Amt > vApproved_Amt)
                {
                    DataTable dtEmails = new DataTable();
                    dtEmails = FS.Get_dtEmails(strJobOrderNo, vLoc_Level);
                    string strEmails = "";
                    if (dtEmails.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtEmails.Rows)
                        {
                            strEmails += row["EmailUserID"].ToString() + ",";
                        }
                    }
                    SendMailAwaiting(strJobOrderNo, strEmails, dtLoginUsesDet.Rows[0]["EmailID"].ToString(), dtLoginUsesDet.Rows[0]["Name"].ToString(), dtLoginUsesDet.Rows[0]["PhoneNo"].ToString(), Status, vLoc_Level);
                }
                else
                {
                    DataTable dtEmails = new DataTable();
                    dtEmails = FS.Get_dtEmails_1(strJobOrderNo, vLoc_Level);
                    string strEmails = "";
                    if (dtEmails.Rows.Count > 0)
                    {
                        foreach (DataRow row in dtEmails.Rows)
                        {
                            strEmails += row["EmailUserID"].ToString() + ",";
                        }
                    }
                    SendMailApproved(strJobOrderNo, strEmails, dtLoginUsesDet.Rows[0]["EmailID"].ToString(), dtLoginUsesDet.Rows[0]["Name"].ToString(), dtLoginUsesDet.Rows[0]["PhoneNo"].ToString(), Status);
                }
            }
        }

        public void SendMailAwaiting(string strJobOrderNo, string strTO, string strFrom, string strName, string strContact, string Status, double vLoc_Level)
        {
            string strBody = "";

            strBody = " <html> ";
            strBody = strBody + "<body> ";
            strBody = strBody + "<table style='font-family: Calibri; font-size: 16px; color: darkblue;'> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "Dear Sir,<br /> ";
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "<br /> ";
            strBody = strBody + "Location : " + BaseLocationCode;
            strBody = strBody + "<br /> ";
            strBody = strBody + "Job Order Number " + strJobOrderNo;
            strBody = strBody + "<br /> ";
            if (Status == "Y" && vLoc_Level != 1)
            {
                strBody = strBody + "Remarks : Job Order is pending for approval";
            }
            else if (Status == "N" && vLoc_Level != 1)
            {
                strBody = strBody + "Remarks : Job Order was Rejected";
            }
            else if (vLoc_Level == 1)
            {
                strBody = strBody + "Remarks : Job Order Approved";
            }
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "<br /> ";
            strBody = strBody + "Regards,<br /> ";
            strBody = strBody + strName + "<br />";
            strBody = strBody + "Mobile + 91-" + strContact + "<br /> ";
            strBody = strBody + "Agility";
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "</table> ";
            strBody = strBody + "</body> ";
            strBody = strBody + "</html>";

            string[] ToMail = strTO.Replace(",,", ",").Split(',');

            MailMessageFleet msg = new MailMessageFleet();
            msg.EmailFrom = strFrom;

            if (ToMail.Length > 0)
            {
                for (int i = 0; i < ToMail.Length; i++)
                {
                    if (ToMail[i] != "")
                    {
                        msg.AddEmailTo(ToMail[i]);
                    }
                }
            }

            msg.EmailMessageType = MessageType.HTML;
            if (Status == "Y" && vLoc_Level != 1)
            {
                msg.EmailSubject = "Job Order Awaiting for Approval";
            }
            else if (Status == "N" && vLoc_Level != 1)
            {
                msg.EmailSubject = "Job Order Rejected";
            }
            else if (vLoc_Level == 1)
            {
                msg.EmailSubject = "Job Order Approved";
            }
            msg.EmailMessage = strBody;
        }

        public void SendMailApproved(string strJobOrderNo, string strTO, string strFrom, string strName, string strContact, string Status)
        {
            string strBody = "";

            strBody = " <html> ";
            strBody = strBody + "<body> ";
            strBody = strBody + "<table style='font-family: Calibri; font-size: 16px; color: darkblue;'> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "Dear Sir,<br /> ";
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "<br /> ";
            strBody = strBody + "Location : " + BaseLocationCode;
            strBody = strBody + "<br /> ";
            strBody = strBody + "Job Order Number " + strJobOrderNo;
            strBody = strBody + "<br /> ";
            if (Status == "Y")
            {
                strBody = strBody + "Remarks : Job Order was approved and get for Bill Entry.";
            }
            else
            {
                strBody = strBody + "Remarks : Job Order was rejected";
            }
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "<tr> ";
            strBody = strBody + "<td> ";
            strBody = strBody + "<br /> ";
            strBody = strBody + "Regards,<br /> ";
            strBody = strBody + strName + "<br />";
            strBody = strBody + "Mobile + 91-" + strContact + "<br /> ";
            strBody = strBody + "Agility";
            strBody = strBody + "</td> ";
            strBody = strBody + "</tr> ";
            strBody = strBody + "</table> ";
            strBody = strBody + "</body> ";
            strBody = strBody + "</html>";

            string[] ToMail = strTO.Replace(",,", ",").Split(',');

            MailMessageFleet msg = new MailMessageFleet();
            msg.EmailFrom = strFrom;

            if (ToMail.Length > 0)
            {
                for (int i = 0; i < ToMail.Length; i++)
                {
                    if (ToMail[i] != "")
                    {
                        msg.AddEmailTo(ToMail[i]);
                    }
                }
            }

            msg.EmailMessageType = MessageType.HTML;
            if (Status == "Y")
            {
                msg.EmailSubject = "Job Order Approved";
            }
            else
            {
                msg.EmailSubject = "Job Order Rejected";
            }
            msg.EmailMessage = strBody;
        }

        public ActionResult JobApproveDone(string JobOrderNo, string Type)
        {
            if (Type == "1")
            {
                @ViewBag.Header = "Job Order Closer Successfully Done.";
            }
            else
            {
                @ViewBag.Header = "Job Order Approval Successfully Done.";
            }
            @ViewBag.No = JobOrderNo;
            return View();
        }

        #endregion

        #region Job Order Cancel
        /*USP_GET_JOB_ORDER_NO_NewPortal*/

        public ActionResult JobOrderCancel()
        {
            JobOrderCancelViewModel VW = new JobOrderCancelViewModel();
            WEBX_FLEET_PM_JOBORDER_HDR ObjWFPJD = new WEBX_FLEET_PM_JOBORDER_HDR();
            List<WEBX_FLEET_PM_JOBORDER_HDR> ListWFPJD = new List<WEBX_FLEET_PM_JOBORDER_HDR>();

            ObjWFPJD.SrNo = 1;
            ListWFPJD.Add(ObjWFPJD);
            VW.ListWFPJH = ListWFPJD;
            return View(VW);
        }

        [HttpPost]
        public ActionResult JobOrder_Cancel(JobOrderCancelViewModel VW, List<WEBX_FLEET_PM_JOBORDER_HDR> JobCancel)
        {
            string JobOrderNo_Str = "";
            List<WEBX_FLEET_PM_JOBORDER_HDR> List_WFPJH = new List<WEBX_FLEET_PM_JOBORDER_HDR>();

            try
            {
                foreach (var item in JobCancel)
                {
                    WEBX_FLEET_PM_JOBORDER_HDR ObjWFPJH = new WEBX_FLEET_PM_JOBORDER_HDR();

                    ObjWFPJH.JOB_ORDER_NO = item.JOB_ORDER_NO;
                    ObjWFPJH.Cancel_Dt = item.Cancel_Dt;
                    ObjWFPJH.Cancel_Remarks = item.Cancel_Remarks;
                    ObjWFPJH.JOB_ORDER_NO = item.JOB_ORDER_NO;
                    List_WFPJH.Add(ObjWFPJH);
                }

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(List_WFPJH.GetType());
                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, List_WFPJH);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DT = FS.JobOrder_Cancel(xmlDoc1.InnerXml, BaseUserName, BaseCompanyCode, BaseLocationCode);

                if (DT.Rows[0]["Message"].ToString() != "Done" && DT.Rows[0]["Status"].ToString() != "1")
                {
                    @ViewBag.StrError = DT.Rows[0]["Message"].ToString();
                    return View("Error");
                }
                else
                {
                    JobOrderNo_Str = DT.Rows[0]["JobOrderNo_Str"].ToString();
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("JobOrderDone", new { JobOrderNo = JobOrderNo_Str });
        }

        public ActionResult _Job_Order_Cancel_Partial(int id)
        {
            WEBX_FLEET_PM_JOBORDER_HDR ObjWFPJH = new WEBX_FLEET_PM_JOBORDER_HDR();
            ObjWFPJH.SrNo = id;
            return PartialView("_Job_Order_Cancel_Partial", ObjWFPJH);
        }

        public JsonResult Check_Valid_JOB_ORDER_NO(string JobOrderNo)
        {
            string Job_Order_No = "", Job_Order_Dt = "", VEHNO = "", JS_BRCD = "", ORDER_STATUS = "", Cancel_Status = "";
            string IsRecordFound = "0";
            DataTable DT = new DataTable();

            try
            {
                DT = FS.GetValid_JOB_ORDER_NO(JobOrderNo, BaseCompanyCode);
                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";

                    Job_Order_No = DT.Rows[0]["Job_Order_No"].ToString();
                    Job_Order_Dt = DT.Rows[0]["Job_Order_Dt"].ToString();
                    VEHNO = DT.Rows[0]["VEHNO"].ToString();
                    JS_BRCD = DT.Rows[0]["JS_BRCD"].ToString();
                    ORDER_STATUS = DT.Rows[0]["ORDER_STATUS"].ToString();
                    Cancel_Status = DT.Rows[0]["Cancel_Status"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Job_Order_No = Job_Order_No,
                        Job_Order_Dt = Job_Order_Dt,
                        VEHNO = VEHNO,
                        JS_BRCD = JS_BRCD,
                        ORDER_STATUS = ORDER_STATUS,
                        Cancel_Status = Cancel_Status
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                    }
                };
            }
        }

        #endregion

        #region Job Order Close

        public ActionResult JobOrderClose()
        {
            JobApprovalViewModel VW = new JobApprovalViewModel();
            VW.ListJobCardType = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "JOBCARDTYP").ToList();//GetGeneralMasterList("JOBCARDTYP");
            return View(VW);
        }

        [HttpPost]
        public ActionResult JobOrderCloseList(JobApprovalViewModel VW)
        {
            DataTable DT = new DataTable();
            DT = FS.Get_JobOrderClose(VW.JobNo, VW.FromDate.ToString("dd/M/yyyy"), VW.ToDate.ToString("dd/M/yyyy"), VW.JobCardType, BaseLocationCode, BaseCompanyCode);
            VW.List_JAGVW = DataRowToObject.CreateListFromTable<JobApprovalGenerationViewModel>(DT);
            VW.ListJobCardType = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "JOBCARDTYP").ToList(); //GetGeneralMasterList("JOBCARDTYP");
            return View(VW);
        }

        #endregion

        //#region Vehicle Issue Slip Query

        //public ActionResult VehicleIssueSlipQueryCriteria(string Type)
        //{
        //    VehicleIssueSlipQueryViewModel VW = new VehicleIssueSlipQueryViewModel();
        //    List<List_Type> ListType = new List<List_Type>();
        //    VW.Passed_Type = Type;
        //    try
        //    {
        //        if (Type == "Advance")
        //        {
        //            ListType.Add(new List_Type { Text = "Advance", Value = "Update" });
        //            ViewBag.Header = "Trip Advance Entry >> Query";
        //            VW.Flag = "U";
        //        }
        //        else if (Type == "Close")
        //        {
        //            ListType.Add(new List_Type { Text = "Close - Operational", Value = "Close" });
        //            ListType.Add(new List_Type { Text = "Close - Financial", Value = "Close" });
        //            ViewBag.Header = "Close - Operational Trip Sheet >> Query";
        //            VW.Flag = "C";
        //        }
        //        else if (Type == "Settlement")
        //        {
        //            ListType.Add(new List_Type { Text = "Driver Settlement", Value = "DS" });
        //            ViewBag.Header = "Driver Settlement >> Query";
        //            VW.Flag = "DS";
        //        }
        //        else if (Type == "FE")
        //        {
        //            ListType.Add(new List_Type { Text = "Financial Edit", Value = "FE" });
        //            ViewBag.Header = "Tripsheet Financial Edit >> Query";
        //            VW.Flag = "FE";
        //        }
        //        else if (Type == "TripDetails")
        //        {
        //            ListType.Add(new List_Type { Text = "Close - Operational Leg Wise", Value = "TD" });
        //            ViewBag.Header = "Tripsheet Operational Edit Leg Wise >> Query";
        //            VW.Flag = "TD";
        //        }
        //        else if (Type == "TripCloseLegWise")
        //        {
        //            ListType.Add(new List_Type { Text = "Close - Financial Leg Wise", Value = "TLW" });
        //            ViewBag.Header = "Tripsheet Financial Edit Leg Wise >> Query";
        //            VW.Flag = "TLW";
        //        }
        //        else if (Type == "FuelSlipEntry")
        //        {
        //            ListType.Add(new List_Type { Text = "Fuel Slip Entry", Value = "FSE" });
        //            ViewBag.Header = "Attach Fuel Slip to Trip Sheet >> Query";
        //            VW.Flag = "FSE";
        //        }
        //        else if (Type == "FuelSlipEdit")
        //        {
        //            ListType.Add(new List_Type { Text = "Fuel Slip Edit", Value = "FSEEDIT" });
        //            ViewBag.Header = "Attach Fuel Slip to Trip Sheet >> Query";
        //            VW.Flag = "FSEEDIT";
        //        }
        //        else
        //        {
        //            VW.Flag = "C";
        //        }
        //        VW.ListType = ListType;
        //    }
        //    catch (Exception ex)
        //    {
        //        //throw;
        //    }
        //    return View(VW);
        //}

        //[HttpPost]
        //public ActionResult VehicleIssueSlipQuery(VehicleIssueSlipQueryViewModel VW)
        //{
        //    DataTable DT = FS.Get_VehicleIssueSlipQuery(VW.TripNo, VW.FromDate.ToString("dd MMM yyyy"), VW.ToDate.ToString("dd MMM yyyy"), VW.Type, BaseLocationCode, VW.TripType, BaseCompanyCode);
        //    VW.ListIsuueSlipVW = DataRowToObject.CreateListFromTable<IssueSlipQueryViewModel>(DT);
        //    return View(VW);
        //}

        //public ActionResult AdvanceEntry(string TripsheetNo, string Type)
        //{
        //    AdvanceEntryViewModel VW = new AdvanceEntryViewModel();
        //    VW.ObjWFVI = FS.Get_IssueslipData(TripsheetNo).First();
        //    return View(VW);
        //}

        //#endregion

        #region View Print
        public ActionResult TripSheetViewPrint()
        {
            return View();
        }

        public JsonResult GetDriver_Name(string searchTerm)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();

            ListLocations = FS.Get_DriverList(searchTerm);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetVehicle_Name(string searchTerm)
        {
            List<webx_VEHICLE_HDR> ListLocations = new List<webx_VEHICLE_HDR>();

            ListLocations = FS.Get_VehicleList(searchTerm, BaseLocationCode);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.VEHNO,
                                  text = e.VEHNO,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TripSheetViewPrintList(JobTripViewPrintViewModel vm)
        {
            //  stop By chiragbhai coz..  code comment in erp (on submit procrss)
            return View();
        }
        public ActionResult JobOrderViewPrint()
        {
            return View();
        }

        public ActionResult JobOrderViewPrintList(JobTripViewPrintViewModel vm)
        {
            List<vm_JobTripViewPrint> optrkList = FS.Get_JobOrderViewPrintList(vm.TripSheetNo, GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), vm.Status, BaseCompanyCode);
            vm.listJobSheet = optrkList;
            return View(vm);
        }
        public ActionResult JobViewPrint(string ONo, string PrintMode)
        {
            JobTripViewPrintViewModel EF = new JobTripViewPrintViewModel();
            EF.TripSheetNo = ONo;
            EF.Status = BaseCompanyCode;
            return View(EF);
        }

        #endregion

        #region Trip Route Location

        public ActionResult TripRoute()
        {
            return View(FS.GetTripRutTranDetails());
        }

        public ActionResult AddEditTripRoute(string id)
        {
            TripRouteViewModel WRMTVM = new TripRouteViewModel();
            List<webx_trip_ruttran> WRTM = new List<webx_trip_ruttran>();
            WRMTVM.listWRTT = WRTM;
            WRMTVM.WRMT = new webx_trip_rutmas();
            WRMTVM.WRTT = new webx_trip_ruttran();
            if (id != null && id != "0")
            {
                //WMFSDVM.EditFlag = true;
                WRMTVM.WRTT = FS.GetTripRutTranDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTVM.listWRTT = FS.GetTripRutTranDetails().Where(c => c.RUTCD == id).ToList();

                WRMTVM.WRMT = FS.GetTripRutMstDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTVM.listWRMT = FS.GetTripRutMstDetails().Where(c => c.RUTCD == id).ToList();
            }

            return View(WRMTVM);
        }

        [HttpPost]
        public ActionResult AddEditTripRoute(TripRouteViewModel WRMTVM, List<webx_trip_ruttran> RutTran)
        {
            TripRouteViewModel RutList = new TripRouteViewModel();
            RutList.listWRTT = RutTran.ToList();
            bool Status = false;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WRMTVM.WRMT.GetType());
                string MstHdrDetails = "<DocumentElementHdr>";
                MstHdrDetails = MstHdrDetails + "<RUTCD>" + WRMTVM.WRMT.RUTCD + "</RUTCD>";
                MstHdrDetails = MstHdrDetails + "<RUTSTBR>" + WRMTVM.WRMT.RUTSTBR + "</RUTSTBR>";
                MstHdrDetails = MstHdrDetails + "<RUTENDBR>" + WRMTVM.WRMT.RUTENDBR + "</RUTENDBR>";
                MstHdrDetails = MstHdrDetails + "<RUTCAT>" + WRMTVM.WRMT.RUTCAT + "</RUTCAT>";
                MstHdrDetails = MstHdrDetails + "<RUTKM>" + WRMTVM.WRMT.RUTKM + "</RUTKM>";
                //MstHdrDetails = MstHdrDetails + "<RUTSTDT>" + WRMTVM.WRMT.RUTSTDT + "</RUTSTDT>";
                //MstHdrDetails = MstHdrDetails + "<RUTENDDT>" + WRMTVM.WRMT.RUTENDDT + "</RUTENDDT>";
                MstHdrDetails = MstHdrDetails + "<ACTIVEFLAG>" + WRMTVM.WRMT.ACTIVEFLAG + "</ACTIVEFLAG>";
                MstHdrDetails = MstHdrDetails + "<UPDTBY>" + WRMTVM.WRMT.UPDTBY + "</UPDTBY>";
                MstHdrDetails = MstHdrDetails + "<RUTNM>" + WRMTVM.WRMT.RUTDESC + "</RUTNM>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_HR>" + WRMTVM.WRMT.SCHDEP_HR + "</SCHDEP_HR>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_MIN>" + WRMTVM.WRMT.SCHDEP_MIN + "</SCHDEP_MIN>";
                MstHdrDetails = MstHdrDetails + "<STD_CONTAMT>" + WRMTVM.WRMT.STD_CONTAMT + "</STD_CONTAMT>";
                MstHdrDetails = MstHdrDetails + "<RUTMOD>" + WRMTVM.WRMT.RUTMOD + "</RUTMOD>";
                MstHdrDetails = MstHdrDetails + "<ControlLoc>" + WRMTVM.WRMT.ControlLoc + "</ControlLoc>";
                MstHdrDetails = MstHdrDetails + "<TransHrs>" + WRMTVM.WRMT.TransHrs + "</TransHrs>";
                MstHdrDetails = MstHdrDetails + "<RUTDESC>" + WRMTVM.WRMT.RUTDESC + "</RUTDESC>";
                MstHdrDetails = MstHdrDetails + "<ROUND_TRIP>" + WRMTVM.WRMT.ROUND_TRIP + "</ROUND_TRIP>";
                MstHdrDetails = MstHdrDetails + "</DocumentElementHdr>";
                string MstDetails = "<root>";
                foreach (var row in RutList.listWRTT)
                {
                    //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                    MstDetails = MstDetails + "<DocumentElement>";
                    MstDetails = MstDetails + "<RUTNO>" + row.RUTNO + "</RUTNO>";
                    MstDetails = MstDetails + "<LOCCD>" + row.LOCCD + "</LOCCD>";
                    MstDetails = MstDetails + "<DIST_KM>" + row.DIST_KM + "</DIST_KM>";
                    MstDetails = MstDetails + "<TRTIME_HR>" + row.TRTIME_HR + "</TRTIME_HR>";
                    MstDetails = MstDetails + "<TRTIME_MIN>" + row.TRTIME_MIN + "</TRTIME_MIN>";
                    MstDetails = MstDetails + "<STTIME_HR>" + row.STTIME_HR + "</STTIME_HR>";
                    MstDetails = MstDetails + "<STTIME_MIN>" + row.STTIME_MIN + "</STTIME_MIN>";
                    MstDetails = MstDetails + "<ONW_RET>" + row.ONW_RET + "</ONW_RET>";
                    MstDetails = MstDetails + "</DocumentElement>";
                }
                MstDetails = MstDetails + "</root>";
                DataTable Dt = new DataTable();
                if (WRMTVM.WRMT.RUTCD != null)
                {
                    Dt = FS.AddEditTranRut(MstHdrDetails, MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = FS.AddEditTranRut(MstHdrDetails, MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                //Department = MS.GetDepartmentObject_XML();
                var RutCode = ""; var TranXaction = "";
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    RutCode = Dt.Rows[0]["RUTCD"].ToString();
                    TranXaction = Dt.Rows[0]["TranXaction"].ToString();
                }
                return RedirectToAction("TripRouteDone", new { RutCode = RutCode, TranXaction = TranXaction });
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult GetTripRouteListJson()
        {
            List<Webx_Master_General> rutmodeList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTMD" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<Webx_Master_General> rutcatList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTCT" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<webx_location> locList = MS.GetLocationDetails().Where(c => c.ActiveFlag == "Y").ToList().OrderBy(c => c.LocName).ToList();
            List<webx_trip_rutmas> listRut = FS.GetTripRutMstDetails();
            var data = (from e in listRut
                        select new
                        {
                            e.RUTCD,
                            //AirlineCode = airlineList.Where(c => c.CodeType == "ALN" && c.StatusCode == "Y").FirstOrDefault().CodeDesc,
                            e.RUTDESC,
                            RUTMOD = rutmodeList.Where(c => c.CodeType == "RTMD" && c.CodeId == e.RUTMOD).FirstOrDefault().CodeDesc,
                            e.RUTKM,
                            e.ControlLoc,// = locList.Where(c => c.LocCode == "" ? c.LocCode == "" : c.LocCode.ToString() == e.ControlLoc).FirstOrDefault().LocName,
                            e.ACTIVEFLAG,
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ADDTripRouteInfo(int id)
        {
            webx_trip_ruttran WRTM = new webx_trip_ruttran();
            if (id == 1)
            {
                WRTM.DIST_KM = 0;
                WRTM.TRTIME_HR = "0";
                WRTM.TRTIME_MIN = "0";
                WRTM.STTIME_HR = "0";
                WRTM.STTIME_MIN = "0";
            }
            WRTM.RUTNO = id;
            return PartialView("_PartialTripRoute", WRTM);
        }

        public JsonResult CheckDuplicateTripRouteName(string RouteName)
        {
            try
            {
                string Count = FS.GetTripRutMstDetails().Where(c => c.RUTNM.ToUpper() == RouteName.ToUpper()).Count().ToString();
                return Json(new { Count = Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(0);
            }
        }

        public ActionResult TripRouteDone(string RutCode, string TranXaction)
        {

            ViewBag.RutCode = RutCode;
            ViewBag.TranXaction = TranXaction;

            return View();

        }

        #endregion

        #region Tyre Odometer Search

        public ActionResult TyreOdometerSearch()
        {
            TyreOdometerSearch DRN = new TyreOdometerSearch();
            DRN.WVHL = new List<webx_VEHICLE_HDR>();
            return View(DRN);

        }

        public ActionResult VEHICLEList(TyreOdometerSearch VM)
        {
            VM.WVHL = FS.GetVEHICLE(VM.WVH.VEHNO).ToList();
            return PartialView("_TyreOdometer", VM.WVHL);
        }

        public JsonResult GetVEHICLE_NameJson(string VEHNO)
        {
            List<webx_VEHICLE_HDR> WVHL = new List<webx_VEHICLE_HDR>();

            WVHL = FS.GetVEHICLEList();
            var SearchList = (from e in WVHL
                              select new
                              {
                                  id = e.VEHNO,
                                  text = e.VEHNO,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TyreOdometern(int id)
        {
            TyreOdometerSearch WMFSM = new TyreOdometerSearch();
            WMFSM.WVH = new webx_VEHICLE_HDR();
            WMFSM.WVHL = new List<webx_VEHICLE_HDR>();
            WMFSM.WVH.SrNo = id;
            return View(WMFSM);
        }

        public ActionResult AddTyreOdometern(int id)
        {
            webx_VEHICLE_HDR WMFSM = new webx_VEHICLE_HDR();
            WMFSM.SrNo = id;
            return PartialView("_ADDTyreOdometern", WMFSM);
        }

        public JsonResult GeVEHNOKm_NameJson(string SerchGroup)
        {
            List<webx_VEHICLE_HDR> ListGroups = new List<webx_VEHICLE_HDR> { };
            try
            {
                DataTable Dt_VEHNO = FS.GetVEHNOCUKM(SerchGroup);
                string Result = "0";
                string status = "";
                if (Dt_VEHNO.Rows.Count > 0)
                {
                    Result = Dt_VEHNO.Rows[0][0].ToString();
                    status = Dt_VEHNO.Rows[0][1].ToString();
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        location = Result,
                        status = status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(ListGroups);
            }
        }

        public ActionResult TyreOdometerSearchSubmit(TyreOdometerSearch Tyre, List<webx_VEHICLE_HDR> VEHNOList)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                foreach (var item in VEHNOList)
                {
                    var Odometer_Reading_Dt = item.Odometer_Reading_Dt.ToString("dd MMM yyyy");
                    Dt_Name = FS.TyreOdometersubmit(item.Tran_No, item.VEH_INTERNAL_NO, item.Current_Km_Read, item.Odometer_Reading, Odometer_Reading_Dt, item.Odometer_Reason);
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("TyreOdometerSearch");
        }

        #endregion

        #region Vehicle Part

        public ActionResult VehiclePartAdd()
        {
            Webx_Master_GeneralViewModel WVM = new Webx_Master_GeneralViewModel();
            try
            {
                List<WebX_Master_CodeTypes> VehiclePartList = FS.GetVehiclePartList().ToList();
                WVM.VPart = VehiclePartList;
                WVM.WMG = new Webx_Master_General();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WVM);
        }

        public ActionResult AddEditPartMaster(string CodeId, string CodeDesc, string CodeType, bool StatusCode)
        {
            bool Status = true;
            string sql = "";
            try
            {
                if (CodeId != "" && CodeId != null)
                {
                    sql = "update WEBX_FLEET_PARTMST set PartName='" + CodeDesc + "',ActiveFlag='" + (StatusCode ? "Y" : "N") + "' where PartId='" + CodeId + "'";
                }
                else
                {
                    sql = "insert into WEBX_FLEET_PARTMST (PartName,ActiveFlag) values ('" + CodeDesc + "','" + (StatusCode ? "Y" : "N") + "')";
                }
                DataTable Dt = GF.GetDataTableFromSP(sql);

                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(Status);
            }
        }

        public JsonResult UpdateStatusForVehiclePart(string CodeId, string CodeType, bool StatusCode)
        {
            bool Status = true;
            string sql = "";
            try
            {
                sql = "update WEBX_FLEET_PARTMST set ActiveFlag='" + (StatusCode ? "Y" : "N") + "' where PartId='" + CodeId + "'";
                DataTable Dt = GF.GetDataTableFromSP(sql);
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(Status);
            }
        }

        public JsonResult CheckDuplicatePartName(string CodeDesc)
        {
            try
            {
                string Count = FS.CheckDuplicatePartName(CodeDesc);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(0);
            }
        }

        #endregion

        #region Fuel Brand Master

        public ActionResult FuelBrandList()
        {
            List<FuelBrandModel> FBList = new List<FuelBrandModel>();
            try
            {
                FBList = FS.GetFuelBrandList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error : Issue on Server...!!! " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(FBList);
        }

        public ActionResult FuelBrandMaster(string Id)
        {
            FuelBrandViewModel FBVM = new FuelBrandViewModel();
            try
            {
                FBVM.EditFlag = "false";
                List<FuelBrandModel> FuelBrandList = FS.GetFuelBrandList();
                FBVM.FBM = new FuelBrandModel();
                if (Id != null && Id != "")
                {
                    FBVM.EditFlag = "true";
                    FBVM.FBM = FuelBrandList.Where(c => c.Fuel_Brand_ID == Convert.ToInt32(Id)).FirstOrDefault();
                }


                FBVM.ListFuelBrand = FuelBrandList;

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(FBVM);
        }

        [HttpPost]
        public string FuelBrandMasterSubmit(FuelBrandViewModel VM)
        {
            bool Status = false;
            if (VM.FBM.Fuel_Brand_ID == 0)
            {
                Status = FS.Fleet_Fuel_Insert_WEBX_FLEET_FUELBRAND(VM.FBM.Fuel_Brand_ID, VM.FBM.Fuel_Brand_Name, VM.FBM.VendorCode, VM.FBM.Fuel_Type_ID, VM.FBM.Active_Flag);
            }
            else
            {
                Status = FS.Fleet_Fuel_Update_WEBX_FLEET_FUELBRAND(VM.FBM.Fuel_Brand_ID, VM.FBM.Fuel_Brand_Name, VM.FBM.VendorCode, VM.FBM.Fuel_Type_ID, VM.FBM.Active_Flag);
            }



            return Status.ToString();
        }

        #endregion

        #region Fuel Card Master

        public ActionResult FuelCardList()
        {
            List<WEBX_FLEET_FUELCARD> FCList = new List<WEBX_FLEET_FUELCARD>();
            try
            {
                FCList = FS.GetFuelCardList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(FCList);
        }

        public ActionResult FuelCardMaster(string Id)
        {
            FUELCARDViewModel FCVM = new FUELCARDViewModel();
            try
            {
                FCVM.EditFlag = "false";
                List<WEBX_FLEET_FUELCARD> FuelCardList = FS.GetFuelCardList();
                FCVM.FCM = new WEBX_FLEET_FUELCARD();
                if (Id != null && Id != "")
                {
                    FCVM.EditFlag = "true";
                    FCVM.FCM = FuelCardList.Where(c => c.Fuel_Card_ID == Convert.ToInt32(Id)).FirstOrDefault();
                }
                FCVM.ListFuelCard = FuelCardList;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(FCVM);
        }

        public JsonResult GetFuelBrand(string FType)
        {
            List<Webx_Master_General> DSFieldOfficerList = FS.GetFuelBrandFromType(FType);
            var users = (from user in DSFieldOfficerList
                         select new
                         {
                             Value = user.Id,
                             Text = user.CodeDesc
                         }).Distinct().ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string FuelCardMasterSubmit(FUELCARDViewModel VM)
        {
            bool Status = false;
            if (VM.FCM.Fuel_Card_ID == 0)
            {
                Status = FS.Fleet_Fuel_Insert_WEBX_FLEET_FUELCARD(VM.FCM.Fuel_Card_ID, VM.FCM.Manual_Fuel_Card_No, VM.FCM.VendorCode, GF.FormateDate(VM.FCM.Issue_Date), GF.FormateDate(VM.FCM.Expiry_Date), VM.FCM.Vehicle_No, VM.FCM.Fuel_Type_ID, VM.FCM.Fuel_Brand_ID, VM.FCM.Active_Flag, VM.FCM.parentAcccode,BaseCompanyCode);
            }
            else
            {
                Status = FS.Fleet_Fuel_Update_WEBX_FLEET_FUELCARD(VM.FCM.Fuel_Card_ID, VM.FCM.Manual_Fuel_Card_No, VM.FCM.VendorCode, GF.FormateDate(VM.FCM.Issue_Date), GF.FormateDate(VM.FCM.Expiry_Date), VM.FCM.Vehicle_No, VM.FCM.Fuel_Type_ID, VM.FCM.Fuel_Brand_ID, VM.FCM.Active_Flag, VM.FCM.parentAcccode);
            }
            return Status.ToString();
        }

        #endregion

        #region CHECKLIST

        public ActionResult CheckList()
        {
            CheckListViewModel WMAVM = new CheckListViewModel();
            try
            {
                WMAVM.listCheckList = new List<WEBX_FLEET_CHECKLIST_MST>();
                WMAVM.listCheckList = FS.GetListOfCheckList();
                WMAVM.CheckListModel = new WEBX_FLEET_CHECKLIST_MST();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMAVM);
        }

        public ActionResult GetCheckListDetails(int SrNo)
        {
            WEBX_FLEET_CHECKLIST_MST WMAM = new WEBX_FLEET_CHECKLIST_MST();
            try
            {
                if (SrNo > 0)
                {
                    WMAM = FS.GetListOfCheckList().FirstOrDefault(c => c.chk_id == SrNo);
                }
            }
            catch (Exception)
            {
            }

            return PartialView("_AddEditCheckList", WMAM);
        }


        public ActionResult GetCkeckList()
        {
            List<WEBX_FLEET_CHECKLIST_MST> listCheckList = new List<WEBX_FLEET_CHECKLIST_MST>();
            try
            {
                listCheckList = FS.GetListOfCheckList();
            }
            catch (Exception)
            {

            }
            return PartialView("_CheckListPartial", listCheckList);
        }

        public ActionResult AddEditCheckList(WEBX_FLEET_CHECKLIST_MST WMAM)
        {
            bool Status = true;
            try
            {
                var flag = WMAM.chk_id;
                DataTable Dt = new DataTable();
                if (flag > 0)
                {
                    Dt = FS.EditCheckList(WMAM.chk_id, WMAM.chk_cat, WMAM.Chk_Desc, WMAM.Chk_Document_Shown, WMAM.ActiveFlag == "True" ? "Y" : "N", BaseUserName, BaseCompanyCode);
                }
                else
                {
                    Dt = FS.AddCheckList(WMAM.chk_cat, WMAM.Chk_Desc, WMAM.Chk_Document_Shown, WMAM.ActiveFlag == "True" ? "Y" : "N", BaseUserName, BaseCompanyCode);
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Document Type Master

        public ActionResult DocTypeList()
        {
            DOCU_TYPEViewModel WMAVM = new DOCU_TYPEViewModel();
            try
            {
                WMAVM.ListDocType = new List<WEBX_FLEET_DOCU_TYPE_MST>();
                WMAVM.ListDocType = FS.GetDocumentTypeList("", "");
                WMAVM.ModelDocType = new WEBX_FLEET_DOCU_TYPE_MST();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMAVM);
        }

        public ActionResult GetDocTypeDetails(int SrNo)
        {
            WEBX_FLEET_DOCU_TYPE_MST WMAM = new WEBX_FLEET_DOCU_TYPE_MST();
            try
            {
                if (SrNo > 0)
                {
                    WMAM = FS.GetDocumentTypeList(SrNo.ToString(), "").FirstOrDefault(c => c.DOCU_TYPE_ID == SrNo);
                }
            }
            catch (Exception)
            {
            }
            return PartialView("_AddEditDocType", WMAM);
        }

        public ActionResult GetDockTypeById()
        {
            List<WEBX_FLEET_DOCU_TYPE_MST> listDockType = new List<WEBX_FLEET_DOCU_TYPE_MST>();
            try
            {
                listDockType = FS.GetDocumentTypeList("", "");
            }
            catch (Exception)
            {

            }
            return PartialView("_DocumentTypePartial", listDockType);
        }

        public ActionResult AddEditDockType(WEBX_FLEET_DOCU_TYPE_MST WMAM)
        {
            bool Status = true;
            try
            {
                var flag = WMAM.DOCU_TYPE_ID;
                DataTable Dt = new DataTable();
                if (flag > 0)
                {
                    Dt = FS.Update_Docu_Type(WMAM.DOCU_TYPE_ID, WMAM.DOCU_TYPE, WMAM.DECS, WMAM.APPLICABLE_STATE == "True" ? "Yes" : "No", WMAM.RENEW_AUTH_ID, WMAM.COST_CAPTURED == "True" ? "Yes" : "No", WMAM.ACTIVE_FLAG == "True" ? "Y" : "N");
                }
                else
                {
                    Dt = FS.Insert_Docu_Type(WMAM.DOCU_TYPE, WMAM.DECS, WMAM.APPLICABLE_STATE == "True" ? "Yes" : "No", WMAM.RENEW_AUTH_ID, WMAM.COST_CAPTURED == "True" ? "Yes" : "No", WMAM.ACTIVE_FLAG == "True" ? "Y" : "N");
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }


        #endregion

        #region Vehicle / Driver Settlement Criteria

        public ActionResult DriverSettlementCriteria()
        {

            CloseTripSheetViewModel CTSVM = new CloseTripSheetViewModel();
            List<vw_FleetFuelList> VehicleList = new List<vw_FleetFuelList>();
            CTSVM.ListTSFF = VehicleList;
            ViewBag.Type = "Advance";
            return View("DriverSettlementCriteria", CTSVM);
        }

        public ActionResult GetDriverSettlementList(CloseTripSheetViewModel CTS)
        {
            string FromDate = CTS.FromDate.ToString("dd MMM yyyy");
            string ToDate = CTS.ToDate.ToString("dd MMM yyyy");
            List<vw_FleetFuelList> VehicleList = new List<vw_FleetFuelList>();
            VehicleList = FS.GetTripFuelListForEntryList(CTS.IssueID, FromDate, ToDate, CTS.SlipId, BaseLocationCode, CTS.TripsheetFlag, BaseCompanyCode);
            return PartialView("_DriverSettlementList", VehicleList);
        }

        public ActionResult DriverTripSheetDetail(string TripId)
        {
            CloseTripSheetViewModel objClose = new CloseTripSheetViewModel();
            WEBX_FLEET_VEHICLE_ISSUE ObjFleetVehicle = new WEBX_FLEET_VEHICLE_ISSUE();
            PaymentControl PC = new PaymentControl();
            ObjFleetVehicle = FS.Get_DriverDetail(TripId).FirstOrDefault();
            objClose.PC = PC;
            string lblVehno = ObjFleetVehicle.vehno;
            objClose.WFVI = ObjFleetVehicle;


            Webx_Fleet_Triprule objTrip = new Webx_Fleet_Triprule();
            objTrip = FS.Get_CheckTripRule().FirstOrDefault();
            objClose.Diesel_CF = objTrip.Diesel_CF;
            if (objClose.Diesel_CF == "Y")
            {
                objClose.hdnDiesel_CF = "Y";
                ViewBag.txtOpeningFuel = "false";
                ViewBag.txtTotalFuelFilled = "false";
                ViewBag.txtFuelRate = "false";
                ViewBag.txtApprovedFuel = "false";
                ViewBag.ddlPlusMinus = "false";
                ViewBag.txtExtraDiesel = "false";
                ViewBag.txtFuelCarryFwd = "false";
                ViewBag.txtRemarks = "false";
            }
            else
            {
                objClose.hdnDiesel_CF = "N";
                ViewBag.txtOpeningFuel = "true";
                ViewBag.txtTotalFuelFilled = "true";
                ViewBag.txtFuelRate = "true";
                ViewBag.txtApprovedFuel = "true";
                ViewBag.ddlPlusMinus = "true";
                ViewBag.txtExtraDiesel = "true";
                ViewBag.txtFuelCarryFwd = "true";
                ViewBag.txtRemarks = "true";
            }
            //Get_CheckTripRule



            //** Fill Payment Detail
            string Amt = "", ExeAmt = "";
            string sqlstr = "";

            objClose.TotAdvancePaid = "0.00";
            objClose.TotAdvancePaidAppr = "0.00";

            objClose.TotDieselCashExp = "0.00";
            objClose.TotDieselCashExpAppr = "0.00";
            objClose.TotDieselCardExp = "0.00";
            objClose.TotDieselCardExpAppr = "0.00";
            objClose.TotDieselCreditExp = "0.00";
            objClose.TotDieselCreditExpAppr = "0.00";


            objClose.TotRepairExp = "0.00";
            objClose.TotRepairExpAppr = "0.00";

            objClose.TotSpareExp = "0.00";
            objClose.TotSpareExpAppr = "0.00";

            objClose.TotIncDedExp = "0.00";
            objClose.TotIncDedExpAppr = "0.00";

            objClose.TotClaimExp = "0.00";
            objClose.TotClaimExpAppr = "0.00";

            objClose.TotEnrouteExp = "0.00";
            objClose.TotEnrouteExpAppr = "0.00";

            double TotAdvAmt = 0;
            double TotAdvAmtAppr = 0;
            double TotExpAmt = 0;
            double TotExpAmtAppr = 0;
            double TotNetExpAmt = 0;
            double TotNetExpAmtAppr = 0;

            //Advance Expense
            sqlstr = "select  ISNULL(sum(Convert(numeric(18,2),AdvAmt)),0) as AdvAmt  from WEBX_TRIPSHEET_ADVEXP  where TripsheetNo='" + TripId + "' ";
            DataTable Dt = GF.GetDataTableFromSP(sqlstr);

            if (Dt.Rows.Count > 0)
            {
                Amt = Dt.Rows[0]["AdvAmt"].ToString();
                ExeAmt = Amt;
                TotAdvAmt = Convert.ToDouble(Amt.ToString());
                TotAdvAmtAppr = Convert.ToDouble(ExeAmt.ToString());
            }

            objClose.TotAdvancePaid = Amt;
            objClose.TotAdvancePaidAppr = ExeAmt;

            string sqlstrFuel = "";

            sqlstrFuel = "SELECT ISNULL(SUM(CONVERT(NUMERIC(18,2),Diesel_Ltr)),0) AS Diesel_Ltr FROM WEBX_TRIPSHEET_OILEXPDET WHERE /*ISNULL(vendor_billno,'')<>'' AND */ TripsheetNo='" + TripId + "' ";
            DataTable DtsqlstrFuel = GF.GetDataTableFromSP(sqlstrFuel);
            if (DtsqlstrFuel.Rows.Count > 0)
            {
                objClose.TotalFuelFilled = DtsqlstrFuel.Rows[0]["Diesel_Ltr"].ToString();
            }

            string sqlstrFuelrate = "";
            sqlstrFuelrate = "select top 1 VI.VSlipno,VI.rut_code,TR.RUTKM,VH.Vehicle_type,VT.Type_name,VI.Vehicleno,EF.KMPL,AR.AvgDieselRate from WEBX_FLEET_VEHICLE_ISSUE as VI inner join Webx_Fleet_AvgDieselRate as AR on VI.rut_code=AR.ROUTE_CODE inner join webx_VEHICLE_HDR as VH on VH.VEHNO=VI.VehicleNo inner join webx_Vehicle_Type as VT on VT.Type_Code=VH.Vehicle_Type inner join WEBX_TRIP_ROUTE_EXP_FUEL EF on VI.rut_code=EF.ROUTE_CODE  and  VT.Type_Code=EF.Vehicle_Type_Code inner join webx_trip_rutmas as TR on TR.RUTCD=VI.rut_code where VI.VehicleNo='" + lblVehno + "' and VI.VslipNo='" + TripId + "' ";
            DataTable DtsqlstrFuelrate = GF.GetDataTableFromSP(sqlstrFuelrate);

            if (DtsqlstrFuelrate.Rows.Count > 0)
            {
                objClose.txtFuelRate = DtsqlstrFuelrate.Rows[0]["AvgDieselRate"].ToString();
                objClose.mApprovedFuel = Math.Round((Convert.ToDouble(Convert.ToString(DtsqlstrFuelrate.Rows[0]["RUTKM"])) / Convert.ToDouble(Convert.ToString(DtsqlstrFuelrate.Rows[0]["KMPL"]))), 2);
                objClose.txtApprovedFuel = objClose.mApprovedFuel;
            }

            //Fuel Expense
            string Card_Cash = "";
            string sqlstrExpense = "select  Card_Cash,Sum(Convert(numeric(18,2),Amount)) as Amount,Sum(Convert(numeric(18,2),EXE_AMT)) as EXE_AMT  from WEBX_TRIPSHEET_OILEXPDET  where TripsheetNo='" + TripId + "' group by TripSheetNo,Card_Cash";
            DataTable Dtsqlstr = GF.GetDataTableFromSP(sqlstrExpense);
            if (Dtsqlstr.Rows.Count > 0)
            {
                Card_Cash = Dtsqlstr.Rows[0]["Card_Cash"].ToString();
                Amt = Dtsqlstr.Rows[0]["EXE_AMT"].ToString();
                ExeAmt = Dtsqlstr.Rows[0]["Amount"].ToString();

                if (Amt == "")
                    Amt = "0";

                if (ExeAmt == "")
                    ExeAmt = "0";

                if (Card_Cash == "Cash")
                {
                    objClose.TotDieselCashExp = Amt;
                    objClose.TotDieselCashExpAppr = ExeAmt;

                    TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                    TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());
                }
                else if (Card_Cash == "Diesel Card")
                {
                    objClose.TotDieselCardExp = Amt;
                    objClose.TotDieselCardExpAppr = ExeAmt;
                }
                else if (Card_Cash == "Credit Card" || Card_Cash == "Credit")
                {
                    objClose.TotDieselCreditExp = Amt;
                    objClose.TotDieselCreditExpAppr = ExeAmt;
                }
            }

            //Repair Expense
            sqlstr = "select  ISNULL(Sum(Convert(numeric(18,2),Amt)),0) as Amt   from WEBX_TRIPSHEET_REPAIREXP  where TripsheetNo='" + TripId + "' ";
            DataTable Dtrepair = GF.GetDataTableFromSP(sqlstr);
            if (Dtrepair.Rows.Count > 0)
            {
                Amt = Dtrepair.Rows[0]["Amt"].ToString();
                ExeAmt = Amt;
                TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());

            }
            objClose.TotRepairExp = Amt;
            objClose.TotRepairExpAppr = ExeAmt;


            //Spare Expense
            sqlstr = "select  ISNULL(Sum(Convert(numeric(18,2),Amt)),0) as Amt   from WEBX_FLEET_VEHICLE_ISSUE_PARTDET  where VSlipNo='" + TripId + "' ";
            DataTable DtSpare = GF.GetDataTableFromSP(sqlstr);
            if (DtSpare.Rows.Count > 0)
            {
                Amt = DtSpare.Rows[0]["Amt"].ToString();
                ExeAmt = Amt;

                TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());
            }


            objClose.TotSpareExp = Amt;
            objClose.TotSpareExpAppr = ExeAmt;


            //Incidental Expense
            sqlstr = "select  ISNULL(Sum(Convert(numeric(18,2),Amt)),0) as Amt   from WEBX_TRIPSHEET_INCDED  where TripSheetNo='" + TripId + "' ";
            DataTable DtIncidental = GF.GetDataTableFromSP(sqlstr);
            if (DtIncidental.Rows.Count > 0)
            {
                Amt = DtIncidental.Rows[0]["Amt"].ToString();
                ExeAmt = Amt;

                TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());
            }

            objClose.TotIncDedExp = Amt;
            objClose.TotIncDedExpAppr = ExeAmt;


            //Claims Expense
            sqlstr = "select  ISNULL(Sum(Convert(numeric(18,2),Approx_Value)),0) as Approx_Value   from WEBX_TRIPSHEET_CLAIMS  where TripsheetId='" + TripId + "' ";
            DataTable DtClaims = GF.GetDataTableFromSP(sqlstr);
            if (DtClaims.Rows.Count > 0)
            {
                Amt = DtClaims.Rows[0]["Approx_Value"].ToString();
                ExeAmt = Amt;

                TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());
            }
            objClose.TotClaimExp = Amt;
            objClose.TotClaimExpAppr = ExeAmt;


            //Enroute Expense
            sqlstr = "select  isnull(sum(convert(numeric(18,2),isnull(polarity,'+')  +  convert(varchar,Amount_Spent))),0) as Amount_Spent,isnull(sum(convert(numeric(18,2),isnull(polarity,'+')  +  convert(varchar,Exe_Appr_Amt))),0) as Exe_Appr_Amt  from WEBX_FLEET_ENROUTE_EXP  where TripsheetNo='" + TripId + "' ";
            DataTable DtEnroute = GF.GetDataTableFromSP(sqlstr);
            if (DtEnroute.Rows.Count > 0)
            {
                Amt = DtEnroute.Rows[0]["Amount_Spent"].ToString();
                ExeAmt = Amt;

                TotExpAmt = TotExpAmt + Convert.ToDouble(Amt.ToString());
                TotExpAmtAppr = TotExpAmtAppr + Convert.ToDouble(ExeAmt.ToString());
            }
            objClose.TotEnrouteExp = Amt;
            objClose.TotEnrouteExpAppr = "(" + ExeAmt + ")";

            objClose.TotExpense = TotExpAmt.ToString();
            objClose.TotExpenseAppr = TotExpAmtAppr.ToString();

            /*LblTotDieselCardExp = Amt;
            LblTotDieselCardExpAppr = ExeAmt;
            LblTotDieselCreditExp = Amt;
            LblTotDieselCreditExpAppr = ExeAmt;*/

            TotNetExpAmt = TotAdvAmt - TotExpAmt;// +(Convert.ToDouble(LblTotDieselCardExp) + Convert.ToDouble(LblTotDieselCreditExp));
            TotNetExpAmtAppr = TotAdvAmtAppr - TotExpAmtAppr;// +(Convert.ToDouble(LblTotDieselCardExpAppr) + Convert.ToDouble(LblTotDieselCreditExpAppr));

            objClose.NetExpAmt = TotNetExpAmt.ToString();
            objClose.NetExpAmtAppr = TotNetExpAmtAppr.ToString();

            string AmtPaidToDriver = "";
            string AmtRecvdFromDriver = "";
            ViewBag.AmtRecvdFromDriver = "false";

            if (AmtPaidToDriver == "")
            {
                AmtPaidToDriver = "0";
                AmtRecvdFromDriver = Convert.ToString(objClose.AmtRecvdFromDriver);
            }

            AmtRecvdFromDriver = Convert.ToString(objClose.AmtRecvdFromDriver);
            if (AmtRecvdFromDriver == "")
            {
                AmtRecvdFromDriver = "0";
            }

            if (AmtPaidToDriver == "0" && AmtRecvdFromDriver == "0")
            {
                objClose.BalToDriverLedger = "0";
            }
            if (TotNetExpAmt == 0)
            {
                ViewBag.AmtRecvdFromDriver = "true";
                objClose.AmtRecvdFromDriver = 0;
            }
            else if (TotNetExpAmt < 0)
            {
                ViewBag.AmtRecvdFromDriver = "true";
                objClose.AmtPaidToDriver = Math.Abs(Convert.ToDecimal(objClose.NetExpAmt)).ToString();
            }

            if (TotNetExpAmt == 0)
            {
                ViewBag.AmtPaidToDriver = "true";
                objClose.AmtPaidToDriver = "0";
            }
            else if (TotNetExpAmt > 0)
            {
                ViewBag.AmtPaidToDriver = "true";
                objClose.AmtRecvdFromDriver = Math.Abs(Convert.ToDouble(objClose.NetExpAmt));
                objClose.AmtPaidToDriver = "0";
            }
            objClose.PC.PayAmount = Math.Abs(Convert.ToDecimal(objClose.NetExpAmt));
            return View("DriverTripSheetDetail", objClose);
        }

        public ActionResult InsertDriverSheetDetail(CloseTripSheetViewModel CTS, PaymentControl PC)
        {
            string mTripSheetNo = "", Status = "", Message = "", VoucherNo = "";
            try
            {
                #region Transaction

                if (PC.PaymentMode.ToUpper() == "BANK")
                {
                    DataTable DT = OS.Duplicate_ChqNO(PC.ChequeNo, PC.ChequeDate.ToString("dd MMM yyyy"));

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                if (PC.PaymentMode.ToUpper() == "CASH" && CTS.AmtPaidToDriver != "0")
                {
                    string FIN_Start = "";
                    FIN_Start = "01 Apr " + BaseYearVal.Split('_')[0];
                    DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, Convert.ToDateTime(CTS.WFVI.settelDate).ToString("dd MMM yyyy"), FIN_Start, BaseYearVal, CTS.AmtPaidToDriver.ToString());

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }

                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                }

                #endregion


                #region Validations

                string AmtPaidToDriver = "";
                double AmtRecvdFromDriver = 0.00;

                AmtRecvdFromDriver = CTS.AmtRecvdFromDriver;
                if (AmtRecvdFromDriver == 0)
                {
                    AmtRecvdFromDriver = 0;
                }

                System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
                dtfi.ShortDatePattern = "dd/MM/yyyy";
                dtfi.DateSeparator = "/";
                dtfi.ShortTimePattern = "hh:mm tt";
                DateTime mDrvierSettlementDt = new DateTime();
                mDrvierSettlementDt = Convert.ToDateTime(CTS.WFVI.settelDate.ToString(), dtfi);

                string strDriverSettleDt = "";

                string ctr = "select  DriverSettleDt from WEBX_FLEET_VEHICLE_ISSUE where VslipNo='" + CTS.WFVI.VSlipNo + "'";
                DataTable Dt = GF.GetDataTableFromSP(ctr);
                strDriverSettleDt = Dt.Rows[0]["DriverSettleDt"].ToString();

                if (strDriverSettleDt != "")
                {
                    Status = "Not Done";
                    return RedirectToAction("DriverSettlemntDone", new { VSlipId = mTripSheetNo, Status = Status });
                }

                DateTime mServerDt = DateTime.Now;
                DateTime mFinCloseDt = new DateTime();
                mFinCloseDt = Convert.ToDateTime(CTS.WFVI.END_DT_TM, dtfi);

                mTripSheetNo = CTS.WFVI.VSlipNo;
                string mdlsttranstype = "";
                string mddrCashcode = "";
                string mddrBankaccode = "";
                string mddrFuelaccode = "";
                string mtxtCashAmt = "";
                string mtxtFuelCardAmt = "";
                string mtxtChqAmt = "";
                string mtxtChqNo = "";
                string mtxtChqDate = "";
                string mtxtAmtApplA = "";
                string mtxtNetPay = "";
                string mtxtRecBank = "";
                string mrdDiposited = "Y";

                if (CTS.AmtPaidToDriver != "" && CTS.AmtPaidToDriver != "0")
                {
                    string dlsttranstype = PC.PaymentMode;
                    string ddrCashcode = PC.CashLedger;
                    string ddrBankaccode = PC.BankLedger;
                    string ddrFuelaccode = PC.FuelCardNo;

                    decimal txtCashAmt = PC.CashAmount;
                    decimal txtChqAmt = PC.ChequeAmount;

                    string txtChqNo = PC.ChequeNo;
                    DateTime txtChqDate = PC.ChequeDate;

                    string txtAmtApplA = "0.00";
                    decimal txtNetPay = PC.PayAmount;

                    mdlsttranstype = dlsttranstype;
                    if (mdlsttranstype == "" || mdlsttranstype == null)
                    {
                        mdlsttranstype = "NULL";
                    }

                    mddrCashcode = Convert.ToString(ddrCashcode);
                    if (mddrCashcode == "" || mddrCashcode == null)
                    {
                        mddrCashcode = "NULL";
                    }

                    mddrBankaccode = ddrBankaccode;
                    if (mddrBankaccode == "" || mddrBankaccode == null)
                    {
                        mddrBankaccode = "NULL";
                    }

                    mddrFuelaccode = ddrFuelaccode;
                    if (mddrFuelaccode == "" || mddrFuelaccode == null)
                    {
                        mddrFuelaccode = "NULL";
                    }

                    if (dlsttranstype == "Cash")
                    {
                        mtxtCashAmt = CTS.AmtPaidToDriver;// txtCashAmt.Text;
                    }
                    else if (dlsttranstype == "Both")
                    {
                        mtxtCashAmt = Convert.ToString(txtCashAmt);// txtCashAmt.Text;
                    }
                    else if (dlsttranstype.ToUpper() == "FUEL CARD")
                    {
                        mtxtFuelCardAmt = Convert.ToString(PC.PayAmount);// txtCashAmt.Text;
                        mddrCashcode = "";
                        mddrBankaccode = "";
                    }
                    else
                    {
                        mtxtCashAmt = "";
                    }

                    if (mtxtCashAmt == "" || mtxtCashAmt == null)
                    {
                        mtxtCashAmt = "0";
                    }
                    if (mtxtFuelCardAmt == "" || mtxtFuelCardAmt == null)
                    {
                        mtxtFuelCardAmt = "0";
                    }
                    if (dlsttranstype == "Bank")
                    {
                        mtxtChqAmt = CTS.AmtPaidToDriver;
                    }
                    else if (dlsttranstype == "Both")
                    {
                        mtxtChqAmt = Convert.ToString(txtChqAmt);
                    }
                    else
                    {
                        mtxtChqAmt = "";
                    }
                    if (mtxtChqAmt == "" || mtxtChqAmt == null)
                    {
                        mtxtChqAmt = "0";
                    }
                    mtxtChqNo = txtChqNo;
                    if (mtxtChqNo == "" || mtxtChqNo == null)
                    {
                        mtxtChqNo = "NULL";
                    }

                    mtxtChqDate = txtChqDate.ToString("dd/MM/yyyy HH:mm:ss");
                    if (mtxtChqDate == "" || mtxtChqDate == null)
                    {
                        mtxtChqDate = "NULL";
                    }

                    mtxtAmtApplA = txtAmtApplA;
                    if (mtxtAmtApplA == "" || mtxtAmtApplA == null)
                    {
                        mtxtAmtApplA = "NULL";
                    }
                    mtxtNetPay = Convert.ToString(txtNetPay);
                    if (mtxtNetPay == "" || mtxtNetPay == null)
                    {
                        mtxtNetPay = "NULL";
                    }

                    if (mtxtRecBank == "" || mtxtRecBank == null)
                    {
                        mtxtRecBank = "NULL";
                    }

                    if (mrdDiposited == "" || mrdDiposited == null)
                    {
                        mrdDiposited = "NULL";
                    }
                }
                if (CTS.AmtRecvdFromDriver > 0)
                {
                    string dlsttranstype = PC.PaymentMode;
                    string ddrCashcode = PC.CashLedger;
                    string ddrBankaccode = PC.BankLedger;
                    string ddrFuelaccode = PC.FuelCardNo;


                    decimal txtCashAmt = PC.CashAmount;
                    decimal txtChqAmt = PC.ChequeAmount;

                    string txtChqNo = PC.ChequeNo;
                    DateTime txtChqDate = PC.ChequeDate;

                    string txtAmtApplA = "0.00";
                    decimal txtNetPay = PC.PayAmount;


                    mdlsttranstype = dlsttranstype;
                    if (mdlsttranstype == "" || mdlsttranstype == null)
                    {
                        mdlsttranstype = "NULL";
                    }

                    mddrCashcode = Convert.ToString(ddrCashcode);
                    if (mddrCashcode == "" || mddrCashcode == null)
                    {
                        mddrCashcode = "NULL";
                    }

                    else
                    {
                        mddrCashcode = "'" + mddrCashcode + "'";
                    }
                    mddrBankaccode = ddrBankaccode;
                    if (mddrBankaccode == "" || mddrBankaccode == null)
                    {
                        mddrBankaccode = "NULL";
                    }

                    mddrFuelaccode = ddrFuelaccode;
                    if (mddrFuelaccode == "" || mddrFuelaccode == null)
                    {
                        mddrFuelaccode = "NULL";
                    }

                    //mtxtCashAmt = txtCashAmt.Text;
                    if (dlsttranstype == "Cash")
                    {
                        mtxtCashAmt = Convert.ToString(CTS.AmtRecvdFromDriver);
                    }
                    else if (dlsttranstype == "Both")
                    {
                        mtxtCashAmt = Convert.ToString(txtCashAmt);
                    }
                    else if (dlsttranstype.ToUpper() == "FUEL CARD")
                    {
                        mtxtFuelCardAmt = Convert.ToString(PC.PayAmount);// txtCashAmt.Text;
                        mddrCashcode = "";
                        mddrBankaccode = "";
                    }
                    else
                    {
                        mtxtCashAmt = "";
                    }

                    if (mtxtCashAmt == "" || mtxtCashAmt == null)
                    {
                        mtxtCashAmt = "0";
                    }

                    if (mtxtFuelCardAmt == "" || mtxtFuelCardAmt == null)
                    {
                        mtxtFuelCardAmt = "0";
                    }

                    //mtxtChqAmt = txtChqAmt.Text;
                    if (dlsttranstype == "Bank")
                    {
                        mtxtChqAmt = Convert.ToString(CTS.AmtRecvdFromDriver);
                    }
                    else if (dlsttranstype == "Both")
                    {
                        mtxtChqAmt = Convert.ToString(txtChqAmt);
                    }
                    else
                    {
                        mtxtChqAmt = "";
                    }

                    if (mtxtChqAmt == "" || mtxtChqAmt == null)
                    {
                        mtxtChqAmt = "0";
                    }
                    mtxtChqNo = txtChqNo;
                    if (mtxtChqNo == "" || mtxtChqNo == null)
                    {
                        mtxtChqNo = "NULL";
                    }
                    else
                    {
                        mtxtChqNo = "'" + mtxtChqNo + "'";
                    }

                    mtxtChqDate = txtChqDate.ToString("dd/MM/yyyy HH:mm:ss");
                    if (mtxtChqDate == "" || mtxtChqDate == null)
                    {
                        mtxtChqDate = "NULL";
                    }

                    mtxtAmtApplA = txtAmtApplA;
                    if (mtxtAmtApplA == "" || mtxtAmtApplA == null)
                    {
                        mtxtAmtApplA = "NULL";
                    }
                    mtxtNetPay = Convert.ToString(txtNetPay);
                    if (mtxtNetPay == "" || mtxtNetPay == null)
                    {
                        mtxtNetPay = "NULL";
                    }

                    if (mtxtRecBank == "" || mtxtRecBank == null)
                    {
                        mtxtRecBank = "NULL";
                    }

                    //  mrdDiposited = rdDiposited;
                    if (mrdDiposited == "" || mrdDiposited == null)
                    {
                        mrdDiposited = "NULL";
                    }
                }

                if (CTS.TotAdvancePaidAppr == null) CTS.TotAdvancePaidAppr = "0.00";
                if (CTS.TotExpense == null) CTS.TotExpense = "0.00";
                if (CTS.TotDieselCardExp == null) CTS.TotDieselCardExp = "0.00";
                if (CTS.TotDieselCreditExp == null) CTS.TotDieselCreditExp = "0.00";
                if (CTS.NetExpAmt == null) CTS.NetExpAmt = "0.00";
                if (CTS.AmtPaidToDriver == "0")
                {
                    AmtPaidToDriver = "0.00";
                }
                else
                {
                    AmtPaidToDriver = CTS.AmtPaidToDriver;
                }
                if (CTS.AmtRecvdFromDriver == 0)
                {
                    AmtRecvdFromDriver = 0;
                }
                else
                {
                    AmtRecvdFromDriver = CTS.AmtRecvdFromDriver;
                }

                if (AmtPaidToDriver == "0.00" && AmtRecvdFromDriver == 0)
                {
                    mdlsttranstype = "";
                    mddrCashcode = "";
                    mddrBankaccode = "";
                    mddrFuelaccode = "";
                    mtxtFuelCardAmt = "0";
                    mtxtCashAmt = "0";
                    mtxtChqAmt = "0";
                    mtxtChqNo = "";
                    mtxtChqDate = null;
                    mtxtAmtApplA = "";
                    mtxtNetPay = "0";
                    mtxtRecBank = "";
                    mrdDiposited = "";
                    if (Convert.ToDecimal(CTS.TotAdvancePaid) != Convert.ToDecimal(CTS.TotExpense))
                    {
                        ViewBag.StrError = "Error ! Amount Paid to or Recieve from Driver both can not allow 0.";
                        return View("Error");
                    }
                }

                #endregion

                string XML = "<Webx_Fleet_DriverSettlement>";
                XML = XML + "<SettleDate>" + GF.FormateDate(mDrvierSettlementDt) + "</SettleDate>";
                XML = XML + "<TotAdvPaid>" + CTS.TotAdvancePaidAppr + "</TotAdvPaid>";
                XML = XML + "<TotExpense>" + CTS.TotExpense + "</TotExpense>";
                XML = XML + "<DieselExp_Card>" + CTS.TotDieselCardExp + "</DieselExp_Card>";
                XML = XML + "<DieselExp_Credit>" + CTS.TotDieselCreditExp + "</DieselExp_Credit>";
                XML = XML + "<NetAmount>" + CTS.NetExpAmt + "</NetAmount>";
                XML = XML + "<AmtPaidToDriver>" + AmtPaidToDriver + "</AmtPaidToDriver>";
                XML = XML + "<AmtRecvdFromDriver>" + AmtRecvdFromDriver + "</AmtRecvdFromDriver>";
                XML = XML + "<BalanceToDriverLedger>" + CTS.BalToDriverLedger + "</BalanceToDriverLedger>";
                XML = XML + "<Paymode>" + Convert.ToString(mdlsttranstype).Replace("'", "") + "</Paymode>";
                XML = XML + "<CashAccCode>" + Convert.ToString(mddrCashcode).Replace("'", "") + "</CashAccCode>";
                XML = XML + "<BankAccCode>" + Convert.ToString(mddrBankaccode).Replace("'", "") + "</BankAccCode>";
                XML = XML + "<FuelCardAccCode>" + Convert.ToString(mddrFuelaccode).Replace("'", "") + "</FuelCardAccCode>";
                XML = XML + "<FuelCardAmt>" + mtxtFuelCardAmt + "</FuelCardAmt>";
                XML = XML + "<CashAmt>" + mtxtCashAmt + "</CashAmt>";
                XML = XML + "<ChequeAmt>" + mtxtChqAmt + "</ChequeAmt>";
                XML = XML + "<ChequeNo>" + Convert.ToString(mtxtChqNo).Replace("'", "") + "</ChequeNo>";
                XML = XML + "<ChequeDate>" + mtxtChqDate + "</ChequeDate>";
                XML = XML + "<BankName>" + Convert.ToString(mtxtRecBank).Replace("'", "") + "</BankName>";
                XML = XML + "<Deposit_YN>" + Convert.ToString(mrdDiposited).Replace("'", "") + "</Deposit_YN>";
                XML = XML + "<f_closure_closekm>" + CTS.WFVI.f_closure_closekm + "</f_closure_closekm>";
                XML = XML + "<vehno>" + CTS.WFVI.vehno + "</vehno>";
                XML = XML + "<TripSheet_EndLoc>" + CTS.WFVI.TripSheet_EndLoc + "</TripSheet_EndLoc>";
                XML = XML + "</Webx_Fleet_DriverSettlement>";


                string SQRY = "exec USP_DriverSettlement '" + XML.Replace("'", "") + "','" + BaseYearVal + "','" + BaseCompanyCode + "','" + BaseUserName + "','" + mTripSheetNo + "','" + CTS.hdnDiesel_CF.Replace("'", "") + "'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "InsertDriverSheetDetail", "", "");
                DataTable DT_Driver_Settlement = GF.GetDataTableFromSP(SQRY);

                Message = DT_Driver_Settlement.Rows[0][0].ToString();
                Status = DT_Driver_Settlement.Rows[0][1].ToString();
                VoucherNo = DT_Driver_Settlement.Rows[0]["VoucherNo"].ToString();

                if (Message.ToUpper() == "DONE" && Status == "1")
                {
                    return RedirectToAction("DriverSettlemntDone", new { VSlipId = mTripSheetNo, Status = Message, VoucherNo = VoucherNo });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }

                #region
                /*
                string DriverSettlement = "";
                DriverSettlement = "Insert into Webx_Fleet_DriverSettlement " +
                    " (TripSheetNo,SettleDate,TotAdvPaid,TotExpense,DieselExp_Card,DieselExp_Credit,NetAmount,AmtPaidToDriver,AmtRecvdFromDriver,BalanceToDriverLedger,Paymode,CashAccCode,BankAccCode,"+
                    "CashAmt,ChequeAmt,ChequeNo,ChequeDate,BankName,Deposit_YN,EntryBy,ActBalanceToDriverLedger,IsCP) values ("
                        + "'" + mTripSheetNo + "','" + GF.FormateDate(mDrvierSettlementDt) + "'," + CTS.TotAdvancePaidAppr + "," + CTS.TotExpense + "," + CTS.TotDieselCardExp + "," + CTS.TotDieselCreditExp + 
                        "," + CTS.NetExpAmt + "," + AmtPaidToDriver + "," + AmtRecvdFromDriver + "," + CTS.BalToDriverLedger + ","+ mdlsttranstype + "," + mddrCashcode + "," + mddrBankaccode + "," 
                        + mtxtCashAmt + "," + mtxtChqAmt + "," + mtxtChqNo + "," + mtxtChqDate + "," + mtxtRecBank + "," + mrdDiposited + ",'" + BaseUserName + "'," + "0" + "," + "1" + ")";

                DataTable DtDriverSettlement = GF.GetDataTableFromSP(DriverSettlement);

                string sqlissue = "Update webx_fleet_vehicle_issue set DriverSettleDt ='" + GF.FormateDate(mDrvierSettlementDt) + "' Where VSlipNo='" + mTripSheetNo + "'";
                DataTable Dtissue = GF.GetDataTableFromSP(sqlissue);
                 * /
/*
                if (CTS.hdnDiesel_CF == "Y")
                {
                    if (CTS.txtFuelRate == "")
                    {
                        CTS.txtFuelRate = "0.00";
                    }
                    if (CTS.txtApprovedFuel == 0)
                    {
                        CTS.txtApprovedFuel = 0.00;
                    }
                    string EXTRA_DIESEL = CTS.ddlPlusMinus + CTS.txtExtraDiesel;
                    string SQRY = "";
                    /*
                    string SQRY = "exec USP_FLEET_INSERT_VEHICLE_CARRY_FORWARD '" + mTripSheetNo + "','" + CTS.VehNo + "','" + CTS.txtFuelCarryFwd + "','" + CTS.txtFuelCarryFwd + "'" + BaseUserName + "','" + CTS.txtFuelRate + "','" + CTS.OpeningFuel + "','" + "hdnAmt" + "','" + CTS.remark + "','" + EXTRA_DIESEL + "','" + CTS.txtApprovedFuel + "' ";
                    DataTable DtSQRY = GF.GetDataTableFromSP(SQRY);
                    */
                /*
                    SQRY = "Update webx_vehicle_hdr set Vehicle_Status='Available',current_KM_Read='" + CTS.WFVI.f_closure_closekm + "'   where vehno='" + CTS.WFVI.vehno + "'";
                    DataTable DtSQRY1 = GF.GetDataTableFromSP(SQRY);

                    SQRY = "Update WEBX_VEHICLE_HDR set CURLOC='" + CTS.WFVI.TripSheet_EndLoc + "' where VEHNO='" + CTS.WFVI.vehno + "'";
                    DataTable DtSQRY2 = GF.GetDataTableFromSP(SQRY);

                    SQRY = "Update WEBX_FLEET_DRIVERMST set Driver_Status='Available' where Driver_id in (select driver1  from WEBX_FLEET_VEHICLE_ISSUE  where VSlipNo='" + mTripSheetNo + "')";
                    DataTable DtSQRY3 = GF.GetDataTableFromSP(SQRY);

                    SQRY = "Update WEBX_FLEET_DRIVERMST set Driver_Status='Available' where Driver_id in (select  (CASE  WHEN ISNUMERIC(driver2)=0  THEN 0  ELSE  DRIVER2 END)  from WEBX_FLEET_VEHICLE_ISSUE  where VSlipNo='" + mTripSheetNo + "')";
                    DataTable DtSQRY4 = GF.GetDataTableFromSP(SQRY);

                }
                //////----------------- FLEET TRANSACTION -----------------------------------
                string mFinYear = "";
                string sqlFinYear = "Select left(YearVal,2) as YearVal From vw_Get_Finacial_Years Where CurrentFinYear='T'";
                DataTable DtFinYear = GF.GetDataTableFromSP(sqlFinYear);
                mFinYear = DtFinYear.Rows[0]["YearVal"].ToString();

                string sqlAcc2 = "exec usp_FleetTransaction '" + CTS.WFVI.TripSheet_EndLoc + "','" + "4" + "','" + mTripSheetNo + "','" + mFinYear + "','" + BaseYearVal + "','" + BaseCompanyCode + "'";
                DataTable DtAcc2 = GF.GetDataTableFromSP(sqlAcc2);

                string sqlAcc3 = "exec usp_FleetTransaction '" + CTS.WFVI.TripSheet_EndLoc + "','" + "5" + "','" + mTripSheetNo + "','" + mFinYear + "','" + BaseYearVal + "','" + BaseCompanyCode + "'";
                DataTable DtAcc3 = GF.GetDataTableFromSP(sqlAcc3);

                string sqlAcc4 = "exec usp_FleetTransaction '" + CTS.WFVI.TripSheet_EndLoc + "','" + "6" + "','" + mTripSheetNo + "','" + mFinYear + "','" + BaseYearVal + "','" + BaseCompanyCode + "'";
                DataTable DtAcc4 = GF.GetDataTableFromSP(sqlAcc4);
                */

                #endregion
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
                Status = "Not Done";
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            //return RedirectToAction("DriverSettlemntDone", new { VSlipId = mTripSheetNo, Status = Status });
        }

        public ActionResult DriverSettlemntDone(string VSlipId, string Status, string VoucherNo)
        {
            ViewBag.VNO = VSlipId;
            ViewBag.Status = Status;
            ViewBag.VoucherNo = VoucherNo;
            return View("DriverSettlemntDone");
        }

        #endregion

        #region Driver

        public ActionResult DriverList()
        {
            return View(FS.GetDriverDetDetails());
        }

        public JsonResult GetDriverListJson()
        {
            List<WEBX_FLEET_DRIVERMST> listWFDM = FS.GetDriverMstDetails();
            var data = (from e in listWFDM
                        select new
                        {
                            e.Driver_Name,
                            e.Driver_Location,
                            e.Driver_Status,
                            e.VEHNO,
                            e.ActiveFlag,
                            e.Driver_Id,
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditDriver(string id)
        {
            DriverViewModel DVM = new DriverViewModel();
            DVM.WFDM = new WEBX_FLEET_DRIVERMST();
            DVM.WFDDD = new WEBX_FLEET_DRIVER_DOCDET();
            DVM.WFDDD.License_Verified_Dt = System.DateTime.Now;
            DVM.WFDM.D_Lic_Current_Issuance_Date = System.DateTime.Now;
            DVM.WFDM.D_Lic_Initial_Issuance_Date = System.DateTime.Now;
            DVM.WFDM.D_DOB = System.DateTime.Now;
            DVM.WFDM.Valdity_dt = System.DateTime.Now;
            if (id != null && id != "0")
            {
                //WMFSDVM.EditFlag = true;
                DVM.WFDM = FS.GetDriverMstDetails().Where(c => c.Driver_Id == Convert.ToDecimal(id)).FirstOrDefault();
                DVM.WFDDD = FS.GetDriverDetDetails().Where(c => c.Driver_Id == Convert.ToDecimal(id)).FirstOrDefault();
            }

            return View(DVM);
        }
        [HttpPost]
        public ActionResult AddEditDriver(DriverViewModel DVM, IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                int id = 0;
                var FolderName = "";
                if (DVM.WFDM.Driver_Id == 0)
                {
                    var Maxcode = FS.GetMaxDriverCode().ToString();
                    FolderName = Convert.ToString((Convert.ToInt32(Maxcode) + 1));
                }
                else
                {
                    FolderName = Convert.ToString(DVM.WFDM.Driver_Id);
                }
                string[] array = new string[12];
                foreach (var item in file)
                {
                    var path = "";
                    var fileName = "";

                    if (item != null)
                    {
                        var files = item;
                        if (files.ContentLength > 0 && files.ContentLength < 1048576)
                        {
                            //string pat = @"(?:.+)(.+)\.(.+)";
                            //Regex r = new Regex(pat);
                            ////run
                            //Match m = r.Match(fileName);
                            //string file_ext = m.Groups[2].Captures[0].ToString();
                            //string filename = m.Groups[1].Captures[0].ToString();
                            //docno = docno.Replace("/", "$");

                            //strRet = docno + "." + file_ext;
                            DataTable Dt = FS.GetFileName(id, item.FileName);
                            fileName = Dt.Rows[0][id].ToString() + "." + (item.ContentType).Split('/')[1];
                            path = Server.MapPath("~/Images/Driver/") + FolderName + "/" + fileName;
                            string strDirectoryName = Server.MapPath("~/Images/Driver/") + FolderName;
                            if (Directory.Exists(strDirectoryName) == false)
                                Directory.CreateDirectory(strDirectoryName);
                            files.SaveAs(path);
                        }

                    }
                    array[id] = fileName;
                    id++;
                }
                DVM.WFDM.EntryBy = BaseUserName;
                DVM.WFDM.UpdatedBy = BaseUserName;
                if (array[0] == "") { array[0] = DVM.WFDDD.Electricity_Bill_File; }
                if (array[1] == "") { array[1] = DVM.WFDDD.Telephone_Bill_File; }
                if (array[2] == "") { array[2] = DVM.WFDDD.BankAcc_File; }
                if (array[3] == "") { array[3] = DVM.WFDDD.Passport_File; }
                if (array[4] == "") { array[4] = DVM.WFDDD.Rationcard_File; }
                if (array[5] == "") { array[5] = DVM.WFDDD.Driver_Registration_Form_File; }
                if (array[6] == "") { array[6] = DVM.WFDDD.ID_Passport_File; }
                if (array[7] == "") { array[7] = DVM.WFDDD.Driving_lic_File; }
                if (array[8] == "") { array[8] = DVM.WFDDD.VoterId_File; }
                if (array[9] == "") { array[9] = DVM.WFDDD.PAN_File; }
                if (array[10] == "") { array[10] = DVM.WFDM.Driver_Photo; }
                if (array[11] == "") { array[11] = DVM.WFDDD.Thumb_Impression_File; }
                DataTable DT = FS.AddEditDriver(DVM, array);
            }
            catch (Exception)
            {
            }
            return RedirectToAction("DriverList");
        }

        //public ActionResult Index(WebX_Master_UsersViewModel VW, HttpPostedFileBase[] file, LoginModel USER)
        //{
        //    ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
        //    try
        //    {
        //        DataTable DT = new DataTable();
        //        string imagepath = "";
        //        string FileName = "", extension = "";
        //        try
        //        {

        //            foreach (var fileobj in file)
        //            {
        //                if (fileobj != null)
        //                {
        //                    if (fileobj.ContentLength > 0)
        //                    {

        //                        string dirPath = Server.MapPath("~/Images/" + BaseUserName);

        //                        if (!Directory.Exists(dirPath))
        //                        {
        //                            Directory.CreateDirectory(dirPath);
        //                        }

        //                        FileName = fileobj.FileName;

        //                        extension = System.IO.Path.GetExtension(fileobj.FileName);


        //                        imagepath = string.Format("{0}/{1}", Server.MapPath("~/Images/" + BaseUserName), FileName);
        //                        string imagepath1 = string.Format("{0}/{1}", Server.MapPath("~/Images/" + BaseUserName), FileName);

        //                        if (System.IO.File.Exists(imagepath1))
        //                            System.IO.File.Delete(imagepath1);

        //                        fileobj.SaveAs(imagepath1);
        //                    }
        //                }

        //                DT = MS.InsertImage(FileName, imagepath, BaseUserName);

        //            }
        //        }

        //        catch (Exception ex)
        //        {
        //            //throw;
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        //throw;
        //    }
        //    return RedirectToAction("Index");
        //}
        #endregion

        #region Trip Route Exp

        public ActionResult TripRouteExpList()
        {
            TripRouteExpenceViewModel TREVM = new TripRouteExpenceViewModel();
            try
            {
                TREVM.listVWTRE = new List<vw_TRIPROUTEEXP_VIEW>();
                TREVM.VWTRE = new vw_TRIPROUTEEXP_VIEW();
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(TREVM);
        }

        public ActionResult GetTripRouteExpenceByRoute(string Id)
        {
            List<vw_TRIPROUTEEXP_VIEW> listRoute = new List<vw_TRIPROUTEEXP_VIEW>();
            try
            {

                if (Id != null && Id.Length > 0)
                {
                    listRoute = FS.GetTripRutExpenceDetails("0", Id);
                }
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                //return Json(Status);
            }
            return PartialView("_TripRouteExpenceList", listRoute);
        }

        public ActionResult AddEditTripRouteExpence(string Id)
        {
            TripRouteExpenceViewModel WRMTVM = new TripRouteExpenceViewModel();
            List<vw_TRIPROUTEEXP_VIEW> WRTM = new List<vw_TRIPROUTEEXP_VIEW>();
            WRMTVM.listVWTRE = WRTM;
            WRMTVM.VWTRE = new vw_TRIPROUTEEXP_VIEW();
            if (Id != null && Id != "0")
            {
                WRMTVM.VWTRE = FS.GetTripRutExpenceDetails(Id, "0").FirstOrDefault();
                WRMTVM.listVWTRE = FS.GetTripRutExpenceDetails(Id, "0").ToList();
            }

            return View(WRMTVM);
        }

        public ActionResult ADDTripRouteExpInfo(int id)
        {
            vw_TRIPROUTEEXP_VIEW WRTCM = new vw_TRIPROUTEEXP_VIEW();
            WRTCM.Srno = id;
            return PartialView("_PartialTripRouteExpence", WRTCM);
        }

        [HttpPost]
        public ActionResult AddEditTripRouteExpence(TripRouteExpenceViewModel TREVM, List<vw_TRIPROUTEEXP_VIEW> RutTran)
        {
            TREVM.listVWTRE = RutTran.ToList();
            bool Status = false;
            try
            {
                foreach (var row in TREVM.listVWTRE)
                {
                    DataTable Dt = FS.AddEditTripRutExpence(row.ID, row.route_exp_code, TREVM.VWTRE.route_code, row.vt, row.fec, row.standard_rate, row.active_flag, BaseUserName,row.VEHICLENO);
                }
                return RedirectToAction("TripRouteExpList");
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }


        public JsonResult CheckDuplicateFuelTypeCode(string RutCD, string VT, string FEC)
        {
            string Count = "";
            try
            {
                Count = FS.CheckDuplicateFuelTypeCode(RutCD, VT, FEC);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Trip Route Fuel Exp

        public ActionResult TripRouteFuelExpList()
        {
            TripRouteFuelExpenceViewModel TRFEVM = new TripRouteFuelExpenceViewModel();
            try
            {
                TRFEVM.listVWTRFE = new List<vw_TRIPROUTEFuelEXP_VIEW>();
                TRFEVM.VWTRFE = new vw_TRIPROUTEFuelEXP_VIEW();
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(TRFEVM);
        }

        public ActionResult GetTripRouteFuelExpenceByRoute(string Id)
        {
            List<vw_TRIPROUTEFuelEXP_VIEW> listRoute = new List<vw_TRIPROUTEFuelEXP_VIEW>();
            try
            {

                if (Id != null && Id.Length > 0)
                {
                    listRoute = FS.GetTripRutFuelExpenceDetails("0", Id);
                }
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                //return Json(Status);
            }
            return PartialView("_TripRouteFuelExpenceList", listRoute);
        }

        public ActionResult AddEditTripRouteFuelExpence(string Id)
        {
            TripRouteFuelExpenceViewModel WRMTVM = new TripRouteFuelExpenceViewModel();
            List<vw_TRIPROUTEFuelEXP_VIEW> WRTM = new List<vw_TRIPROUTEFuelEXP_VIEW>();
            WRMTVM.listVWTRFE = WRTM;
            WRMTVM.VWTRFE = new vw_TRIPROUTEFuelEXP_VIEW();
            if (Id != null && Id != "0")
            {
                WRMTVM.VWTRFE = FS.GetTripRutFuelExpenceDetails(Id, "0").FirstOrDefault();
                WRMTVM.listVWTRFE = FS.GetTripRutFuelExpenceDetails(Id, "0").ToList();
            }

            return View(WRMTVM);
        }

        public ActionResult ADDTripRouteFuelExpInfo(int id)
        {
            vw_TRIPROUTEFuelEXP_VIEW WRTCM = new vw_TRIPROUTEFuelEXP_VIEW();
            WRTCM.Srno = id;
            return PartialView("_PartialTripRouteFuelExpence", WRTCM);
        }

        [HttpPost]
        public ActionResult AddEditTripRouteFuelExpence(TripRouteFuelExpenceViewModel TREVM, List<vw_TRIPROUTEFuelEXP_VIEW> RutTran)
        {
            TREVM.listVWTRFE = RutTran.ToList();
            bool Status = false;
            try
            {
                DataTable Dt = new DataTable();
                Dt = FS.AddEditDieselRate(TREVM.VWTRFE.RateId, TREVM.VWTRFE.route_code, TREVM.VWTRFE.RutCharge, BaseUserName);
                foreach (var row in TREVM.listVWTRFE)
                {
                    Dt = FS.AddEditTripRouteFuelExp(row.ID, row.Route_Fuel_Exp_Code, TREVM.VWTRFE.route_code, Convert.ToString(row.Vehicle_Type_Code), row.KMPL, row.active_flag, BaseUserName, row.VEHICLENO, row.LTRS);
                }
                return RedirectToAction("TripRouteFuelExpList");
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult GetDieselPrize(string Id)
        {
            string Rate = "";
            int RateId = 0;
            try
            {
                DataTable Dt = FS.GetTripRutFuelDetails(Id);
                Rate = Dt.Rows[0]["AvgDieselRate"].ToString();
                RateId = Convert.ToInt32(Dt.Rows[0]["AvgDieselRateId"].ToString());
                return new JsonResult()
                {
                    Data = new
                    {
                        Rate = Rate,
                        RateId = RateId,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Rate, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckDuplicateVehicleNo(string Id, string RutCD, string VehicleNo)
        {
            string Count = "";
            try
            {
                Count = FS.CheckDuplicateVehicleNo(Id, RutCD, VehicleNo);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Tyre Removal Master

        public ActionResult TyreRemoval()
        {
            TyreRemovalViewModel TRVM = new TyreRemovalViewModel();
            TRVM.listVWTDNM = new List<VW_TyreDetails_New>();
            return View(TRVM);
        }

        [HttpPost]
        public ActionResult TyreRemoval(TyreRemovalViewModel TRVM)
        {
            DataTable Dt = FS.GetTyreRemovalDetail(TRVM.TrucNo);
            TRVM.Tyre_Pr_Alex = Convert.ToDecimal(Dt.Rows[0][1]);
            TRVM.listVWTDNM = FS.GetTyreRemovalList(TRVM.TrucNo);
            TRVM.Actual_Tyre = TRVM.listVWTDNM.Count();
            return View("TyreRemovalList", TRVM);
        }

        [HttpPost]
        public ActionResult TyreRemovalList(TyreRemovalViewModel TRVM, List<VW_TyreDetails_New> TyreRemoval)
        {
            try
            {
                TRVM.listVWTDNM = TyreRemoval;
                foreach (var item in TRVM.listVWTDNM.Where(c => c.active == true).ToList())
                {
                    //if (item.active == true)
                    //{
                    DataTable Dt = FS.EditTyreRemoval(TRVM.TrucNo, item.Removal_KM, item.TYRE_ID, BaseUserName, item.Distance, item.Per_KM_Cost, BaseLocationCode, TRVM.RemovalDt, item.LifeOfTyre);
                    //}
                }
                ViewBag.Message = "The Tyre has been removed from vehicle Successfully.";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View("TyreRemovalDone", new { Message = ViewBag.Message });
        }

        public ActionResult TyreRemovalDone(string Message)
        {
            ViewBag.Message = Message;
            return View();
        }

        #endregion

        #region Vehicle Document

        public ActionResult DocumentEntry(string Flag, string VEHNO, string TypeId)
        {
            DocumentEntryViewModel DEVM = new DocumentEntryViewModel();
            DEVM.WFDD = new WEBX_FLEET_DOCUMENT_DET();
            DEVM.WFDL = new List<WEBX_FLEET_DOCUMENT_DET>();
            //DEVM.WFDD.TYPE = "1";
            DEVM.WFDD.Flag = Flag;
            DEVM.WFDD.START_DT = System.DateTime.Now;
            DEVM.WFDD.EXPRITY_DT = System.DateTime.Now;
            ViewBag.foldername = BaseUserName;

            if (DEVM.WFDD.Flag == "Edit")
            {

                WEBX_FLEET_DOCUMENT_DET WFDD = new WEBX_FLEET_DOCUMENT_DET();
                List<WEBX_FLEET_DOCUMENT_DET> WFDL = FS.GetEditDocumentEntryList(Convert.ToInt32(VEHNO), Convert.ToInt32(TypeId));
                //WFDD.TYPE = "2";
                WFDL.Add(WFDD);
                DEVM.WFDD = WFDL.FirstOrDefault();
                DEVM.WFDD.Flag = "Edit";
                ViewBag.foldername = BaseUserName;
            }
            return View(DEVM);

        }

        public ActionResult DocumentEntryCriteriaList(string Flag)
        {
            DocumentEntryViewModel Doc = new DocumentEntryViewModel();

            Doc.WFDD = new WEBX_FLEET_DOCUMENT_DET();
            Doc.WFDL = new List<WEBX_FLEET_DOCUMENT_DET>();


            Doc.WFDD.Flag = Flag;

            return View(Doc);

        }

        public ActionResult GetDocumentEntryListDetails(int VEHNO, int TypeId)
        {
            DocumentEntryViewModel DEVM = new DocumentEntryViewModel();
            try
            {
                if (VEHNO != 0)
                {
                    DEVM.WFDD = FS.GetEditDocumentEntryList(VEHNO, TypeId).FirstOrDefault(c => c.VEH_NO == Convert.ToInt32(VEHNO) && c.DOCU_TYPE_ID == Convert.ToInt32(TypeId));
                }
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GeneralMasterMasters", "PartialView", "AddEdit", ex.Message);
            }

            return View(DEVM);
        }

        public ActionResult DocumentEntryDetail(DocumentEntryViewModel Doc)
        {
            Doc.WFDL = new List<WEBX_FLEET_DOCUMENT_DET>();
            Doc.WFDL = FS.DocumentDetailLIST(Doc.WFDD.VEHNOActive, Doc.WFDD.DOCU_TYPE_ID);
            return PartialView("_DocumentEntryDetail", Doc.WFDL);

        }

        public JsonResult GetState(string searchTerm)
        {
            List<WEBX_FLEET_DOCUMENT_DET> ListState = new List<WEBX_FLEET_DOCUMENT_DET>();

            ListState = FS.GetDOCMGT_STATE().Where(c => c.STNM.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.STNM).ToList();
            var SearchList = (from e in ListState
                              select new
                              {
                                  id = e.STNM,
                                  text = e.STNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DocumentEntrySubmit(DocumentEntryViewModel Doc, string Flag, HttpPostedFileBase[] file)
        {

            DataTable Dt_Name = new DataTable();
            try
            {
                string imagepath = "";
                string FileName = "", extension = "";
                foreach (var fileobj in file)
                {
                    if (fileobj != null)
                    {
                        if (fileobj.ContentLength > 0)
                        {

                            string dirPath = Server.MapPath("~/Images/" + BaseUserName);

                            if (!Directory.Exists(dirPath))
                            {
                                Directory.CreateDirectory(dirPath);
                            }

                            FileName = fileobj.FileName;

                            extension = System.IO.Path.GetExtension(fileobj.FileName);

                            imagepath = string.Format("{0}/{1}", Server.MapPath("~/Images/" + BaseUserName), FileName);
                            //string imagepath1 = string.Format("{0}/{1}", Server.MapPath("~/Images/" + BaseUserName), FileName);

                            if (System.IO.File.Exists(imagepath))
                                System.IO.File.Delete(imagepath);

                            fileobj.SaveAs(imagepath);
                            Doc.WFDD.FILENAME = FileName;
                        }
                    }

                }

                if (Doc.WFDD.Flag == "Add")
                {
                    int vehicleno = Int32.Parse(Doc.WFDD.VEHNO);
                    Dt_Name = FS.DocumentEntrysubmit(Doc.WFDD.RENEWAL_AUTU_NAME, Doc.WFDD.DOCUMENT_NO.Trim(), Doc.WFDD.START_DT, Doc.WFDD.EXPRITY_DT, Doc.WFDD.REMINDER_IN_DAYS, Doc.WFDD.APPLICABLE_STATE, Doc.WFDD.DOCU_COST, Doc.WFDD.REMARKS, Doc.WFDD.FILENAME, BaseUserName, Doc.WFDD.ENTER_BY, vehicleno, Doc.WFDD.Flag, Doc.WFDD.DOCU_TYPE_ID, Doc.WFDD.DOCU_ID);
                }

                else if (Doc.WFDD.Flag == "Edit")
                {
                    int vehicleno = Doc.WFDD.VEH_NO;
                    Dt_Name = FS.DocumentEntrysubmit(Doc.WFDD.RENEWAL_AUTU_NAME, Doc.WFDD.DOCUMENT_NO.Trim(), Doc.WFDD.START_DT, Doc.WFDD.EXPRITY_DT, Doc.WFDD.REMINDER_IN_DAYS, Doc.WFDD.APPLICABLE_STATE, Doc.WFDD.DOCU_COST, Doc.WFDD.REMARKS, Doc.WFDD.FILENAME, BaseUserName, Doc.WFDD.ENTER_BY, vehicleno, Doc.WFDD.Flag, Doc.WFDD.DOCU_TYPE_ID, Doc.WFDD.DOCU_ID);
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("DocumentEntryDone");
        }

        public ActionResult DocumentEntryDone()
        {
            return View();
        }

        #endregion

        #region  Tyre Interchange

        public ActionResult TyreInterchange()
        {
            TyreInterchangeViewModel TIVM = new TyreInterchangeViewModel();
            return View(TIVM);
        }

        public JsonResult GetTyreDetailByTruck(string Id)
        {
            List<TyreDetail> listTyre = new List<TyreDetail>();
            listTyre = FS.GetTyreDetailByTruck(Id).ToList();
            var Data = (from e in listTyre
                        select new
                        {
                            id = e.TYRE_ID,
                            text = e.TYRE_NO + ":" + e.MODEL_DESC,
                        }).Distinct().ToList();
            return Json(Data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult TyreInterchange(TyreInterchangeViewModel TIVM)
        {
            var Message = "";
            try
            {
                string[] str_Truck1Tyres = TIVM.Truck1Tyres.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                string[] str_Truck2Tyres = TIVM.Truck2Tyres.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in str_Truck1Tyres)
                {
                    DataTable Dt = FS.EditTyreInterchange(item, TIVM.TruckNo1, TIVM.TruckNo1, Convert.ToDecimal(TIVM.Read_ODO1), BaseCompanyCode, BaseUserName);
                }
                foreach (var item in str_Truck2Tyres)
                {
                    DataTable Dt = FS.EditTyreInterchange(item, TIVM.TruckNo2, TIVM.TruckNo2, Convert.ToDecimal(TIVM.Read_ODO2), BaseCompanyCode, BaseUserName);
                }
                Message = "Done";
            }
            catch (Exception ex)
            {
                Message = "Not Done";
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("TyreInterchangeDone", new { VEHNO1 = TIVM.TruckNo1, VEHNO2 = TIVM.TruckNo2, TranXaction = Message });
        }

        public ActionResult TyreInterchangeDone(string VEHNO1, string VEHNO2, string TranXaction)
        {
            ViewBag.VEHNO1 = VEHNO1;
            ViewBag.VEHNO2 = VEHNO2;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        public JsonResult GetVehicleBasedonTyreSize(string VehicleNo)
        {
            List<webx_VEHICLE_HDR> listVehicle = new List<webx_VEHICLE_HDR>();
            listVehicle = FS.GetVehicleBasedonTyreSize(VehicleNo).ToList();

            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.VEHNO,
                                   Text = e.VEHNO,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Tyre Issue Master

        public ActionResult TyreIssueMaster()
        {
            TyreIssueMasterViewModel TIMV = new TyreIssueMasterViewModel();
            return View(TIMV);
        }

        public JsonResult Check_Valid_timeofFitment(string TruckNo)
        {
            string Fitment = "0";
            string IsRecordFound = "0";
            DataTable DT = new DataTable();

            try
            {
                DT = FS.GetValid_Fitment(TruckNo);
                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";
                    Fitment = DT.Rows[0]["FITMENT_KM"].ToString();

                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Fitment = Fitment
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                    }
                };
            }
        }

        public ActionResult TyreIssueMasterlist(TyreIssueMasterViewModel BLC)
        {
            DataTable Dt_Name = new DataTable();
            VW_TyreDetails_New Tyredetail = new VW_TyreDetails_New();
            string Tyre_Attached = "select VEHNO ,Tyre_Attached=(case  when Tyre_Attached is null then '0'  else Tyre_Attached end)  from webx_VEHICLE_HDR where VEHNO='" + BLC.CheckVhNo + "'";
            Dt_Name = GF.GetDataTableFromSP(Tyre_Attached);
            //Tyredetail.Axel = Dt_Name.Rows[0]["Tyre_Attached"].ToString();
            //Tyre_Attached = BLC.VWTDN.Axel;

            BLC.VWTDNL = FS.GetTyreDetailVehicleWIse(BLC.CheckVhNo);
            //Tyredetail.ActualTyre = BLC.VWTDNL.Count();
            //Tyredetail.PendingTyre = Convert.ToInt32(Tyredetail.ActualTyre) - Convert.ToInt32(Tyredetail.Axel);



            foreach (var item in BLC.VWTDNL)
            {
                if (BLC.RemovalKM > item.MANUAL_MOUNT_KM_RUN)
                {
                    item.DistanceCovered = BLC.RemovalKM - item.MANUAL_MOUNT_KM_RUN;
                    //Double DistanceCovered = Convert.ToDouble(item.DistanceCovered);
                    item.PerKMCost = ((item.TYRE_COST) / (item.DistanceCovered));
                    //txtPerKMCost.Text = cost.ToString("F2");
                    //    item.PerKMCost = decimal.Format("{0:n4}", cost);


                    // decimal mNeTPay = Math.Round(item.TYRE_COST / item.DistanceCovered, 0, MidpointRounding.AwayFromZero);
                }
            }
            BLC.VWTDNL1 = FS.GetTyreDetailBranchWIseBasedonVehicle(BaseLocationCode, BLC.CheckVhNo);

            if (FS.GetTyreDetailBranchWIse(BaseLocationCode).Count() > 0)
            {
                BLC.VWTDN = FS.GetTyreDetailBranchWIse(BaseLocationCode).FirstOrDefault();
            }
            else
            {
                BLC.VWTDN = new VW_TyreDetails_New();
            }

            BLC.VWTDN.Axel = Dt_Name.Rows[0]["Tyre_Attached"].ToString();
            BLC.VWTDN.ActualTyre = BLC.VWTDNL.Count();
            BLC.VWTDN.PendingTyre = System.Math.Abs(Convert.ToInt32(BLC.VWTDN.ActualTyre) - Convert.ToInt32(BLC.VWTDN.Axel));
            //BLC.VWTDN.ActualTyre = BLC.VWTDNL.Count();

            //BLC.VWTDN.PendingTyre = BLC.VWTDNL.Count();
            if (BLC.VWTDNL.Count == 0)
            {

                BLC.VWTDNL = new List<VW_TyreDetails_New>();
            }

            if (BLC.VWTDNL1.Count == 0)
            {
                BLC.VWTDNL1 = new List<VW_TyreDetails_New>();
            }
            BLC.VWTDN.RemovalKM = BLC.RemovalKM;
            return View(BLC);
            //BLC.VWTDNL = FS.GetTyreDetailVehicleWIse(BLC.CheckVhNo);
            //foreach (var item in BLC.VWTDNL)
            //{
            //    if (BLC.RemovalKM != null && BLC.RemovalKM > item.MANUAL_MOUNT_KM_RUN)
            //    {
            //        item.DistanceCovered = BLC.RemovalKM - item.MANUAL_MOUNT_KM_RUN;
            //        //Double DistanceCovered = Convert.ToDouble(item.DistanceCovered);
            //        item.PerKMCost = ((item.TYRE_COST) / (item.DistanceCovered));
            //        //txtPerKMCost.Text = cost.ToString("F2");
            //        //    item.PerKMCost = decimal.Format("{0:n4}", cost);


            //        // decimal mNeTPay = Math.Round(item.TYRE_COST / item.DistanceCovered, 0, MidpointRounding.AwayFromZero);
            //    }
            //}
            //BLC.VWTDNL1 = FS.GetTyreDetailBranchWIse(BaseLocationCode);
            //BLC.VWTDN = FS.GetTyreDetailBranchWIse(BaseLocationCode).First();
            //if (BLC.VWTDNL.Count == 0)
            //{

            //    BLC.VWTDNL = new List<VW_TyreDetails_New>();
            //}

            //if (BLC.VWTDNL1.Count == 1)
            //{
            //    BLC.VWTDNL1 = new List<VW_TyreDetails_New>();
            //}
            //// BLC.VWTDN.RemovalKM = BLC.RemovalKM;
            //return View(BLC);
        }

        public ActionResult TyreIssueMasterlistsubmit(TyreIssueMasterViewModel BLC, List<VW_TyreDetails_New> BranchWise, List<VW_TyreDetails_New> VehicleWise)
        {
            DataTable Dt_Name = new DataTable();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                if (VehicleWise != null)
                {
                    foreach (var item in VehicleWise.Where(c => c.IsChecked == true).ToList())
                    {
                        item.CheckVhNo = BLC.CheckVhNo;
                        item.ChkDate = BLC.VWTDN.RemoveDT;
                        Dt_Name = FS.UpdateDismountTyre(item.CheckVhNo, BLC.VWTDN.RemovalKM, item.TYRE_ID, BaseUserName, item.DistanceCovered, item.PerKMCost, BaseLocationCode, GF.FormateDate(item.ChkDate), item.LifeOfTyre);
                    }
                }

                if (BranchWise.Count != 0)
                {
                    foreach (var item in BranchWise.Where(c => c.IsChecked == true).ToList())
                    {
                        item.CheckVhNo = BLC.CheckVhNo;
                        item.ChkDate = BLC.VWTDN.ChkDate;
                        Dt_Name = FS.UpdateTyre(item.CheckVhNo, GF.FormateDate(item.ChkDate), BLC.VWTDN.RemovalKM, item.TYRE_ID, BaseUserName, BaseLocationCode, item.LifeOfTyre);
                    }
                }
                trn.Commit();
            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("TyreIssueMasterDone");
        }

        public ActionResult TyreIssueMasterDone()
        {
            return View();
        }

        #endregion

        #region  OldTyreStock_Query

        public ActionResult OldTyreStockCriteria()
        {
            return View();
        }

        [HttpPost]
        public ActionResult OldTyreStock(WEBX_FLEET_TYREMST_HISTORY WFTHL)
        {
            OldTyreStockViewModel OTSV = new OldTyreStockViewModel();
            try
            {

                OTSV.WFTHL = FS.GetOldTyreList().ToList();

                if (WFTHL.MFG != "" && WFTHL.MFG != null)
                {
                    OTSV.WFTHL = OTSV.WFTHL.Where(c => c.MFG == WFTHL.MFG).ToList();
                }
                if (WFTHL.MODEL != "" && WFTHL.MODEL != null)
                {
                    OTSV.WFTHL = OTSV.WFTHL.Where(c => c.MODEL == WFTHL.MODEL).ToList();
                }
                if (WFTHL.SIZE != "" && WFTHL.SIZE != null)
                {
                    OTSV.WFTHL = OTSV.WFTHL.Where(c => c.SIZE == WFTHL.SIZE).ToList();
                }
                if (WFTHL.PATTERN != "" && WFTHL.PATTERN != null)
                {
                    OTSV.WFTHL = OTSV.WFTHL.Where(c => c.PATTERN == WFTHL.PATTERN).ToList();
                }
                if (WFTHL.TYRE_TYPE != "" && WFTHL.TYRE_TYPE != null)
                {
                    OTSV.WFTHL = OTSV.WFTHL.Where(c => c.TYRE_TYPE == WFTHL.TYRE_TYPE).ToList();
                }
            }
            catch (Exception)
            {
                return RedirectToAction("OldTyreStockCriteria");
            }

            return View(OTSV);
        }

        public ActionResult GetOldTyreStockList(OldTyreStockViewModel OTSV)
        {
            List<WEBX_FLEET_TYREMST_HISTORY> WFTHL = new List<WEBX_FLEET_TYREMST_HISTORY>();
            try
            {
                WFTHL = FS.GetOldTyreList().ToList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return PartialView("_OldTyreStock", WFTHL);

        }

        public ActionResult InternalActionList(List<WEBX_FLEET_TYREMST_HISTORY> TYRENO, OldTyreStockViewModel OTSV)
        {
            string TYRE_ID = "";
            int count = TYRENO.Where(c => c.IsChecked == true).Count();
            DataTable Dt_Name = new DataTable();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                if (TYRENO.Count() > 0)
                {
                    foreach (var item in TYRENO.Where(c => c.IsChecked == true).ToList())
                    {
                        if (TYRE_ID == "")
                        {
                            TYRE_ID = item.TYRE_ID;
                        }
                        else
                        {
                            TYRE_ID = TYRE_ID + "," + item.TYRE_ID;
                        }

                        if (OTSV.Action == "I")
                        {
                            //Dt_Name = FS.Internalsubmit(item.TYRE_ID, item.Suffix, item.ACTIONS);
                            Dt_Name = FS.Internalsubmit(item.TYRE_ID, item.Suffix, item.ACTIONS, BaseUserName, BaseLocationCode, item.LifeofTyreRemaining);
                            trn.Commit();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");

            }
            if (OTSV.Action == "S")
            {
                return RedirectToAction("TyreSale", new { TYRE_ID = TYRE_ID });
            }
            else if (OTSV.Action == "C")
            {
                return RedirectToAction("ClaimOfTyre", new { TYRE_ID = TYRE_ID });
            }
            else if (OTSV.Action == "R")
            {
                return RedirectToAction("TyreRemold", new { TYRE_ID = TYRE_ID });
            }
            return RedirectToAction("InternalActionListDone", new { TYRE_ID = TYRE_ID });
        }

        public ActionResult InternalActionListDone(string TYRE_ID)
        {
            ViewBag.TYRE_ID = TYRE_ID;
            return View();
        }

        public ActionResult ClaimOfTyre(string TYRE_ID)
        {
            OldTyreStockViewModel OTSV = new OldTyreStockViewModel();
            OTSV.WFTHL = new List<WEBX_FLEET_TYREMST_HISTORY>();

            try
            {
                OTSV.WFTHL = FS.GetTyresListClaim(TYRE_ID);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(OTSV);
        }

        public ActionResult ClaimOfTyreSubmit(OldTyreStockViewModel OTSV, List<WEBX_FLEET_TYREMST_HISTORY> TYRENO)
        {
            string CLAIM_REMOLD_SALE_ID = "";
            DataTable Dt_Name = new DataTable();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                string CliamCode = "exec WebX_SP_GetNextDocumentCode_FA '" + BaseLocationCode + "','" + BaseFinYear + "','" + "CLAIM" + "'";
                Dt_Name = GF.GetDataTableFromSP(CliamCode);
                CLAIM_REMOLD_SALE_ID = Dt_Name.Rows[0][0].ToString();
                foreach (var item in TYRENO.Where(c => c.IsChecked == true).ToList())
                {
                    var ClaimDT = OTSV.WFTH.ClaimDT.ToString("dd MMM yyyy");
                    Dt_Name = FS.Claimsubmit(item.TYRE_ID, CLAIM_REMOLD_SALE_ID, ClaimDT, BaseLocationCode, OTSV.WFTH.VENDORCODE, item.TYRE_STATUS, item.ACTIONS, item.CLAIM_AMT, item.Remark, item.TYRE_ACTIVEFLAG, item.Suffix, item.Dist_Covered, BaseUserName);

                    //string NextDocumentCode = Dt_Name.Rows[0][1].ToString();
                    
                }
                trn.Commit();
            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("ClaimOfTyreDone", new { CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID });
        }

        public ActionResult ClaimOfTyreDone(string CLAIM_REMOLD_SALE_ID)
        {
            ViewBag.CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID;

            return View();
        }

        public ActionResult TyreUpdate(string TYRE_ID)
        {
            OldTyreStockViewModel OTSV = new OldTyreStockViewModel();
            OTSV.WFTHL = new List<WEBX_FLEET_TYREMST_HISTORY>();

            try
            {
                OTSV.WFTHL = FS.GetTyresListClaim(TYRE_ID);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(OTSV);
        }

        public ActionResult TyreRemold(string TYRE_ID)
        {
            OldTyreStockViewModel OTSV = new OldTyreStockViewModel();
            OTSV.WFTHL = new List<WEBX_FLEET_TYREMST_HISTORY>();

            try
            {
                OTSV.WFTHL = FS.GetTyresListRemold(TYRE_ID);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(OTSV);
        }

        public ActionResult TyreRemoldSubmit(OldTyreStockViewModel OTSV, List<WEBX_FLEET_TYREMST_HISTORY> TYRENO)
        {
            string CLAIM_REMOLD_SALE_ID = "";
            DataTable Dt_Name = new DataTable();

            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                string CliamCode = "exec WebX_SP_GetNextDocumentCode_FA '" + BaseLocationCode + "','" + BaseFinYear + "','" + "Remold" + "'";
                Dt_Name = GF.GetDataTableFromSP(CliamCode);
                CLAIM_REMOLD_SALE_ID = Dt_Name.Rows[0][0].ToString();
                foreach (var item in TYRENO.Where(c => c.IsChecked == true).ToList())
                {
                    //string CliamCode = "exec WebX_SP_GetNextDocumentCode_FA '" + BaseLocationCode + "','" + BaseFinYear + "','" + "Remold" + "'";
                    //Dt_Name = GF.GetDataTableFromSP(CliamCode);
                    //CLAIM_REMOLD_SALE_ID = Dt_Name.Rows[0][0].ToString();
                    //var ClaimDT = OTSV.WFTH.ClaimDT.ToString("dd MMM yyyy");
                    Dt_Name = FS.TyreRemoldsubmit(item.TYRE_ID, CLAIM_REMOLD_SALE_ID, GF.FormateDate(OTSV.WFTH.ClaimDT), BaseLocationCode, OTSV.WFTH.VENDORCODE, item.TYRE_STATUS, item.ACTIONS, item.Suffix, item.Dist_Covered, BaseUserName);
                    //string NextDocumentCode = Dt_Name.Rows[0][1].ToString();
                }
                trn.Commit();
            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("TyreRemoldDone", new { CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID });
        }

        public ActionResult TyreRemoldDone(string CLAIM_REMOLD_SALE_ID)
        {
            ViewBag.CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID;
            return View();
        }

        public ActionResult TyreSale(string TYRE_ID)
        {
            OldTyreStockViewModel OTSV = new OldTyreStockViewModel();
            OTSV.WFTHL = new List<WEBX_FLEET_TYREMST_HISTORY>();

            try
            {
                OTSV.WFTHL = FS.GetTyresListRemold(TYRE_ID);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(OTSV);
        }

        public ActionResult TyreSaleSubmit(OldTyreStockViewModel OTSV, List<WEBX_FLEET_TYREMST_HISTORY> TYRENO, PaymentControl PC)
        {
            string CLAIM_REMOLD_SALE_ID = "";
            DataTable Dt_Name = new DataTable();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                string CliamCode = "exec WebX_SP_GetNextDocumentCode_FA '" + BaseLocationCode + "','" + BaseFinYear + "','" + "SALE" + "'";
                Dt_Name = GF.GetDataTableFromSP(CliamCode);
                CLAIM_REMOLD_SALE_ID = Dt_Name.Rows[0][0].ToString();
                foreach (var item in TYRENO.Where(c => c.IsChecked == true).ToList())
                {
                    // var ClaimDT = OTSV.WFTH.ClaimDT.ToString("dd MMM yyyy");
                    Dt_Name = FS.TyreSalesubmit(item.TYRE_ID, CLAIM_REMOLD_SALE_ID, item.Vendor, item.TYRE_STATUS, item.SALE_AMT, item.MobileNo, BaseLocationCode, PC.PaymentMode, PC.CashLedger, PC.ChequeNo, PC.ChequeDate, PC.ChequeAmount, PC.CashAmount, PC.CashLedger, PC.BankLedger, PC.PayAmount, BaseUserName);
                    //string NextDocumentCode = Dt_Name.Rows[0][1].ToString();
                }

            }
            catch (Exception ex)
            {
                trn.Rollback();
                con.Close();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            trn.Commit();
            return RedirectToAction("TyreSaleDone", new { CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID });
        }

        public ActionResult TyreSaleDone(string CLAIM_REMOLD_SALE_ID)
        {
            ViewBag.CLAIM_REMOLD_SALE_ID = CLAIM_REMOLD_SALE_ID;
            return View();
        }

        #endregion

        #region Update Remold Tyre

        public ActionResult UpdateRemoldTyreCriteria()
        {
            return View();
        }

        public ActionResult GetRemoldTyreList(UpdateRemoldTyreViewModel vm)
        {
            if (vm.RemoldModel != null)
            {
                vm.ListRemolTyre = FS.Get_RemoldList(GF.FormateDate(vm.RemoldModel.FromDate), GF.FormateDate(vm.RemoldModel.ToDate), vm.RemoldModel.PartyCode, vm.RemoldModel.RemoldNo);
            }
            else
            {
                vm = new UpdateRemoldTyreViewModel();
                vm.ListRemolTyre = new List<UpdateRemoldTyreList>();
            }
            return PartialView("_UpdateRemoldTyrePartial", vm.ListRemolTyre);
        }

        public ActionResult RemoldTyreDetailList(string id, decimal GSTPercentage, string GSTType, string StateCode)
        {
            UpdateRemoldTyreViewModel obj = new UpdateRemoldTyreViewModel();
            try
            {
                obj.RemolTyreDet = FS.Get_RemoldListDet(id);
                ViewBag.GSTPercentage = GSTPercentage;
                ViewBag.GSTType = GSTType;
                ViewBag.StateCode = StateCode;

            }
            catch (Exception)
            {

            }
            return View(obj);
        }

        [HttpPost]
        public ActionResult UpdateRemoldTyreSubmit(List<RemoldTyreList_DET> RemoldListDet, UpdateRemoldTyreViewModel vm, StaxTDSViewModel STVM)
        {
            string CLAIM_REMOLD_SALE_ID = "", BILL_NO = "", StateCode = "";


            SqlConnection conn = new SqlConnection(Connstr);

            SqlTransaction trans;
            conn.Open();
            trans = conn.BeginTransaction();
            try
            {

                foreach (var item in RemoldListDet.Where(c => c.Active.ToUpper() == "Y").ToList())
                {
                    StateCode = item.StateCode;
                }

                DataTable obj = new DataTable();
                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                obj = FS.GENERATE_BILLNO(StateCode, basefinyear);

                string STATUS = "";
                double REMOLD_AMT = 0.00;
                if (obj.Rows.Count > 0)
                {
                    BILL_NO = obj.Rows[0]["REMOLD_BILLNO"].ToString();
                }
                else
                {
                    BILL_NO = "";
                }
                if (vm.RemoldModelDet.NetPay != 0)
                {
                    vm.RemoldModelDet.NetPay = vm.RemoldModelDet.NetPay;
                }
                else
                {
                    vm.RemoldModelDet.NetPay = 0;
                }
                if (RemoldListDet != null && RemoldListDet.Count() > 0)
                {
                    int SR_NO = 0;
                    string TYRE_NO = "";
                    string VendorCode = "";
                    string Rejection = "";
                    string REMOLD_YN = "";
                    string CLAIM_REMOLD_SALE_ID1 = "";
                    string GSTType = "";
                    decimal Persentage = 0;


                    foreach (var item in RemoldListDet.Where(c => c.Active.ToUpper() == "Y").ToList())
                    {

                        SR_NO = item.SR_NO;
                        TYRE_NO = item.TYRE_NO;
                        VendorCode = item.VendorCode;
                        Rejection = item.Rejection;
                        GSTType = item.GSTType;
                        Persentage = item.GSTPercentage;

                        CLAIM_REMOLD_SALE_ID1 = item.CLAIM_REMOLD_SALE_ID;

                        if (item.Amount != 0)
                        {
                            item.Amount = item.Amount;
                            item.REMOLD_YN = "Y";
                        }
                        else
                        {
                            item.Amount = 0;
                            item.REMOLD_YN = "N";
                        }
                        if (item.Type == "P") { STATUS = "PASSED"; item.REMOLD_YN = "Y"; }
                        if (item.Type == "R") { STATUS = "REJECTED"; item.REMOLD_YN = "N"; }
                        if (item.Amount != 0) { REMOLD_AMT = Convert.ToDouble(item.Amount); } else { REMOLD_AMT = 0.00; }
                        CLAIM_REMOLD_SALE_ID = item.CLAIM_REMOLD_SALE_ID;
                        REMOLD_YN = item.REMOLD_YN;

                        DataTable objDT = new DataTable();
                        objDT = FS.UpdateRemoldTyreSubmit(SR_NO, TYRE_NO, BILL_NO, CLAIM_REMOLD_SALE_ID1, STATUS, VendorCode, REMOLD_AMT, Rejection, REMOLD_YN, BaseLocationCode, STVM.StaxOnAmount, GSTType, Persentage, vm.RemoldModelDet.NETAMT, StateCode, STVM.StaxRate, STVM.TDSRate, STVM.TDSAmount, STVM.TDSAcccode, STVM.TDSAccdesc, STVM.PANNO, BaseUserName, BaseCompanyCode, basefinyear);
                    }

                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("UpdateRemoldDone", new { RemoldID = CLAIM_REMOLD_SALE_ID, BILLNO = BILL_NO });
        }

        public ActionResult UpdateRemoldDone(string RemoldID, string BILLNO)
        {
            ViewBag.RemoldID = RemoldID;
            ViewBag.BILLNO = BILLNO;
            return View();
        }

        #endregion

        #region Update Claim of Tyre

        public ActionResult UpdateClaimTyreCriteria()
        {
            return View();
        }

        public ActionResult UpdateClaimTyreList(UpdateRemoldTyreViewModel vm)
        {
            if (vm.RemoldModel != null)
            {
                vm.RemolTyreDet = FS.Get_ClaimTyreList(GF.FormateDate(vm.RemoldModel.FromDate), GF.FormateDate(vm.RemoldModel.ToDate), vm.RemoldModel.PartyCode, vm.RemoldModel.RemoldNo);
            }
            else
            {
                vm = new UpdateRemoldTyreViewModel();
                vm.RemolTyreDet = new List<RemoldTyreList_DET>();
            }
            return View(vm);
        }

        [HttpPost]
        public ActionResult UpdateClaimTyreSubmit(List<RemoldTyreList_DET> RemoldListDet, UpdateRemoldTyreViewModel vm, PaymentControl PaymentControl)
        {
            string CLAIM_REMOLD_SALE_ID = "";
            SqlConnection conn = new SqlConnection(Connstr);

            SqlTransaction trans;
            conn.Open();
            trans = conn.BeginTransaction();
            try
            {
                #region Transaction

                if (PaymentControl.PaymentMode.ToUpper() == "BANK")
                {
                    DataTable DT = OS.Duplicate_ChqNO(PaymentControl.ChequeNo, PaymentControl.ChequeDate.ToString("dd MMM yyyy"));

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                if (PaymentControl.PaymentMode.ToUpper() == "CASH")
                {
                    string FIN_StartDt = "";//, Curr_Year = "", Finyear = "";
                    //string Financial_Year = BaseFinYear;
                    //string fin_year = BaseFinYear.ToString();
                    //double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
                    //fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
                    //Curr_Year = DateTime.Now.ToString("yyyy");
                    //Finyear = BaseFinYear.ToString();
                    //if (Finyear == Curr_Year)
                    //    FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
                    //else
                    FIN_StartDt = "01 Apr " + BaseYearVal.Split('_')[0];

                    DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, System.DateTime.Now.ToString("dd MMM yyyy"), FIN_StartDt, BaseYearVal, PaymentControl.PayAmount.ToString());

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }

                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                }

                #endregion

                string STATUS = "";
                double REMOLD_AMT = 0.00;

                if (vm.RemoldModelDet.NetPay != 0)
                {
                    vm.RemoldModelDet.NetPay = vm.RemoldModelDet.NetPay;
                }
                else
                {
                    vm.RemoldModelDet.NetPay = 0;
                }
                if (RemoldListDet != null && RemoldListDet.Count() > 0)
                {
                    foreach (var item in RemoldListDet.Where(c => c.Active.ToUpper() == "Y").ToList())
                    {
                        if (item.Amount != 0)
                        {
                            item.Amount = item.Amount;
                            item.REMOLD_YN = "Y";
                        }
                        else
                        {
                            item.Amount = 0;
                            item.REMOLD_YN = "N";
                        }
                        if (item.Type == "P") { STATUS = "PASSED"; item.REMOLD_YN = "Y"; }
                        if (item.Type == "R") { STATUS = "REJECTED"; item.REMOLD_YN = "N"; }
                        if (item.Amount != 0) { REMOLD_AMT = Convert.ToDouble(item.Amount); } else { REMOLD_AMT = 0.00; }
                        CLAIM_REMOLD_SALE_ID = item.CLAIM_REMOLD_SALE_ID;
                        DataTable objDT = new DataTable();
                        objDT = FS.UpdateClimeTyreSubmit(item.SR_NO, item.TYRE_NO, item.CLAIM_REMOLD_SALE_ID, item.VendorCode, STATUS, REMOLD_AMT, item.Rejection, item.REMOLD_YN, BaseLocationCode, PaymentControl.PaymentMode, PaymentControl.ChequeNo, GF.FormateDate(PaymentControl.ChequeDate), PaymentControl.ChequeAmount, PaymentControl.CashAmount, PaymentControl.CashLedger, PaymentControl.BankLedgerName, vm.RemoldModelDet.NetPay, BaseUserName);
                    }
                }
                trans.Commit();
            }
            catch (Exception ex)
            {
                trans.Rollback();
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("UpdateClaimDone", new { RemoldID = CLAIM_REMOLD_SALE_ID });
        }

        public ActionResult UpdateClaimDone(string RemoldID)
        {
            ViewBag.RemoldID = RemoldID;
            return View();
        }

        #endregion

        #region Remold Tyre Bill Payment

        public ActionResult RemoldBillPaymentCriteria()
        {
            return View();
        }

        public ActionResult RemoldBilPaymentList(UpdateRemoldTyreViewModel vm)
        {
            if (vm.RemoldModel != null)
            {
                vm.RemolBillPayment = FS.Get_RemoldTyreBillPaymentList(GF.FormateDate(vm.RemoldModel.FromDate), GF.FormateDate(vm.RemoldModel.ToDate), vm.RemoldModel.PartyCode, vm.RemoldModel.RemoldNo);
                if (vm.RemolBillPayment.Count() > 0)
                {
                    vm.BillPay = vm.RemolBillPayment.FirstOrDefault();
                }
                else
                {
                    vm.BillPay = new RemoldTyreBillPayment();
                }
            }
            else
            {
                vm = new UpdateRemoldTyreViewModel();
                vm.RemolBillPayment = new List<RemoldTyreBillPayment>();
                vm.BillPay = new RemoldTyreBillPayment();
            }
            vm.BillPay.selectDate = System.DateTime.Now.ToString("dd MMM yyyy");
            return View(vm);
        }

        [HttpPost]
        public ActionResult RemoldTyreBillSubmit(List<RemoldTyreBillPayment> RemoldPayment, UpdateRemoldTyreViewModel vm, PaymentControl PaymentControl)
        {
            string voucherNo = "";
            string PBOV_code = "";
            string PBOV_Name = "";
            //string PBOV_typ = "V";
            string Panno1 = "";
            decimal CurrentAmt = 0;
            double Currbalance = 0;
            double othChrg = 0;
            double DEDUCTION = 0;
            double TotChrg = 0;
            double servicetaxCharged = 0;
            string effected_acccode = "";
            string accountcode = "";

            string tdsacccode = "";
            string svctaxacccode = "";
            double mamul_chrg = 0;
            string servicetaxNo = "";
            string bookcode = "VENDOR BILL";

            #region Transaction

            if (PaymentControl.PaymentMode.ToUpper() == "BANK")
            {
                DataTable DT = OS.Duplicate_ChqNO(PaymentControl.ChequeNo, PaymentControl.ChequeDate.ToString("dd MMM yyyy"));

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
                if (Cnt != "T")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
            }

            if (PaymentControl.PaymentMode.ToUpper() == "CASH")
            {
                string FIN_StartDt = "";//, Curr_Year = "", Finyear = "";

                FIN_StartDt = "01 Apr " + BaseYearVal.Split('_')[0];

                DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, System.DateTime.Now.ToString("dd MMM yyyy"), FIN_StartDt, BaseYearVal, PaymentControl.PayAmount.ToString());

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }

                if (Cnt != "T")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }
            }

            #endregion

            string DocNo = "";

            try
            {

                string acccode1 = "";
                double debit = 0, credit = 0;

                if (PaymentControl.PaymentMode == "Cash")
                {
                    accountcode = PaymentControl.CashLedger;
                }
                if (PaymentControl.PaymentMode == "Bank")
                {
                    accountcode = PaymentControl.BankLedger;
                }

                if (RemoldPayment != null && RemoldPayment.Count() > 0)
                {
                    string Xml_Other_Details = "<root><Other>";
                    foreach (var item in RemoldPayment.Where(c => c.Active == "Y").ToList())
                    {
                        CurrentAmt = item.CurrentAmt;
                        PBOV_code = item.VENDORCODE;
                        PBOV_Name = item.VENDORNAME;
                        effected_acccode = item.acccode;
                        credit = credit + Convert.ToDouble(CurrentAmt);



                        Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
                        Xml_Other_Details = Xml_Other_Details + "<Transdate>" + vm.BillPay.selectDate + "</Transdate>";
                        Xml_Other_Details = Xml_Other_Details + "<Transtype>" + PaymentControl.PaymentMode + "</Transtype>";
                        Xml_Other_Details = Xml_Other_Details + "<brcd>" + BaseLocationCode + "</brcd>";
                        Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.ToUpper() + "</Entryby>";
                        Xml_Other_Details = Xml_Other_Details + "<Entrydt>" + System.DateTime.Now + "</Entrydt>";
                        Xml_Other_Details = Xml_Other_Details + "<voucherNo>" + voucherNo + "</voucherNo>";
                        Xml_Other_Details = Xml_Other_Details + "<CurrentAmt>" + item.CurrentAmt + "</CurrentAmt>";
                        Xml_Other_Details = Xml_Other_Details + "<pendamt>" + item.pendamt + "</pendamt>";
                        Xml_Other_Details = Xml_Other_Details + "<OppAcccode>" + accountcode + "</OppAcccode>";
                        Xml_Other_Details = Xml_Other_Details + "<oppaccount>" + acccode1 + "</oppaccount>";
                        Xml_Other_Details = Xml_Other_Details + "<chqno>" + PaymentControl.ChequeNo + "</chqno>";
                        Xml_Other_Details = Xml_Other_Details + "<chqdate>" + PaymentControl.ChequeDate + "</chqdate>";
                        Xml_Other_Details = Xml_Other_Details + "<Debit>" + debit + "</Debit>";
                        Xml_Other_Details = Xml_Other_Details + "<Credit>" + credit + "</Credit>";
                        Xml_Other_Details = Xml_Other_Details + "<Currbalance>" + Currbalance + "</Currbalance>";
                        Xml_Other_Details = Xml_Other_Details + "<DocNo>" + DocNo + "</DocNo>";
                        Xml_Other_Details = Xml_Other_Details + "<Voucher_Cancel>" + "N" + "</Voucher_Cancel>";
                        Xml_Other_Details = Xml_Other_Details + "<voucher_status>" + "Submitted" + "</voucher_status>";
                        Xml_Other_Details = Xml_Other_Details + "<preparefor>" + BaseUserName + "</preparefor>";
                        Xml_Other_Details = Xml_Other_Details + "<servicetaxrate>" + "0" + "</servicetaxrate>";
                        Xml_Other_Details = Xml_Other_Details + "<tdsrate>" + "0" + "</tdsrate>";
                        Xml_Other_Details = Xml_Other_Details + "<tds>" + "0" + "</tds>";
                        Xml_Other_Details = Xml_Other_Details + "<othertaxrate>" + "0" + "</othertaxrate>";
                        Xml_Other_Details = Xml_Other_Details + "<othChrg>" + othChrg + "</othChrg>";
                        Xml_Other_Details = Xml_Other_Details + "<OCTPaid>" + "0" + "</OCTPaid>";
                        Xml_Other_Details = Xml_Other_Details + "<AgentServiceChrg>" + "0" + "</AgentServiceChrg>";
                        Xml_Other_Details = Xml_Other_Details + "<ServiceChargPaid>" + "0" + "</ServiceChargPaid>";
                        Xml_Other_Details = Xml_Other_Details + "<TotalDed>" + "0" + "</TotalDed>";
                        Xml_Other_Details = Xml_Other_Details + "<ServiceChargPaid>" + "0" + "</ServiceChargPaid>";
                        Xml_Other_Details = Xml_Other_Details + "<ServiceChargPaid>" + mamul_chrg + "</ServiceChargPaid>";
                        Xml_Other_Details = Xml_Other_Details + "<Netamt>" + credit + "</Netamt>";
                        Xml_Other_Details = Xml_Other_Details + "<PBOV_code>" + PBOV_code + "</PBOV_code>";
                        Xml_Other_Details = Xml_Other_Details + "<PBOV_Name>" + PBOV_Name + "</PBOV_Name>";
                        Xml_Other_Details = Xml_Other_Details + "<PBOV_typ>" + "V" + "</PBOV_typ>";
                        Xml_Other_Details = Xml_Other_Details + "<panno>" + Panno1 + "</panno>";
                        Xml_Other_Details = Xml_Other_Details + "<servicetaxNo>" + servicetaxNo + "</servicetaxNo>";
                        Xml_Other_Details = Xml_Other_Details + "<bookcode>" + bookcode + "</bookcode>";
                        Xml_Other_Details = Xml_Other_Details + "<BANKNAME>" + PaymentControl.BankLedgerName + "</BANKNAME>";
                        Xml_Other_Details = Xml_Other_Details + "<CHQAMT>" + PaymentControl.ChequeAmount + "</CHQAMT>";
                        Xml_Other_Details = Xml_Other_Details + "<TRANSMODE>" + PaymentControl.PaymentMode + "</TRANSMODE>";
                        Xml_Other_Details = Xml_Other_Details + "<DEDUCTION>" + DEDUCTION + "</DEDUCTION>";
                        Xml_Other_Details = Xml_Other_Details + "<TotChrg>" + TotChrg + "</TotChrg>";
                        Xml_Other_Details = Xml_Other_Details + "<tdsacccode>" + tdsacccode + "</tdsacccode>";
                        Xml_Other_Details = Xml_Other_Details + "<svctaxacccode>" + svctaxacccode + "</svctaxacccode>";
                        Xml_Other_Details = Xml_Other_Details + "<servicetaxCharged>" + servicetaxCharged + "</servicetaxCharged>";
                        Xml_Other_Details = Xml_Other_Details + "<autoentry>" + "Y" + "</autoentry>";
                        Xml_Other_Details = Xml_Other_Details + "<effected_acccode>" + effected_acccode + "</effected_acccode>";
                        Xml_Other_Details = Xml_Other_Details + "<BILLNO>" + item.BILLNO + "</BILLNO>";
                        Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                    }

                    try
                    {
                        voucherNo = FS.Remold_Tyre_Bill_payments(Xml_Other_Details);
                    }
                    catch (Exception ex)
                    {
                        @ViewBag.StrError = ex.Message;
                        return View("Error");
                    }

                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("RemoldTyreBillDone", new { voucherNo = voucherNo });
        }


        public ActionResult RemoldTyreBillDone(string voucherNo)
        {
            ViewBag.RemoldID = voucherNo;
            return View();
        }

        #endregion

        #region Vehicle Issue Slip Query

        public ActionResult VehicleIssueSlipQueryCriteria(string Type)
        {
            VehicleIssueSlipQueryViewModel VW = new VehicleIssueSlipQueryViewModel();
            List<List_Type> ListType = new List<List_Type>();
            VW.Passed_Type = Type;
            try
            {
                if (Type == "Advance")
                {
                    ListType.Add(new List_Type { Text = "Advance", Value = "Update" });
                    ViewBag.Header = "Trip Advance Entry";
                    VW.Flag = "U";
                }
                else if (Type == "Close")
                {
                    ListType.Add(new List_Type { Text = "Close - Operational", Value = "Close" });
                    ListType.Add(new List_Type { Text = "Close - Financial", Value = "Close" });
                    ViewBag.Header = "Close - Operational Trip Sheet";
                    VW.Flag = "C";
                }
                else if (Type == "Settlement")
                {
                    ListType.Add(new List_Type { Text = "Driver Settlement", Value = "DS" });
                    ViewBag.Header = "Driver Settlement";
                    VW.Flag = "DS";
                }
                else if (Type == "FE")
                {
                    ListType.Add(new List_Type { Text = "Financial Edit", Value = "FE" });
                    ViewBag.Header = "Tripsheet Financial Edit";
                    VW.Flag = "FE";
                }
                else if (Type == "TripDetails")
                {
                    ListType.Add(new List_Type { Text = "Close - Operational Leg Wise", Value = "TD" });
                    ViewBag.Header = "Tripsheet Operational Edit Leg Wise";
                    VW.Flag = "TD";
                }
                else if (Type == "TripCloseLegWise")
                {
                    ListType.Add(new List_Type { Text = "Close - Financial Leg Wise", Value = "TLW" });
                    ViewBag.Header = "Tripsheet Financial Edit Leg Wise";
                    VW.Flag = "TLW";
                }
                else if (Type == "FuelSlipEntry")
                {
                    ListType.Add(new List_Type { Text = "Fuel Slip Entry", Value = "FSE" });
                    ViewBag.Header = "Attach Fuel Slip to Trip Sheet";
                    VW.Flag = "FSE";
                }
                else if (Type == "FuelSlipEdit")
                {
                    ListType.Add(new List_Type { Text = "Fuel Slip Edit", Value = "FSEEDIT" });
                    ViewBag.Header = "Attach Fuel Slip to Trip Sheet";
                    VW.Flag = "FSEEDIT";
                }
                else
                {
                    VW.Flag = "C";
                }
                VW.ListType = ListType;
            }
            catch (Exception ex)
            {

            }
            return View(VW);
        }

        //[HttpPost]
        public ActionResult VehicleIssueSlipQuery(VehicleIssueSlipQueryViewModel VW)
        {

            try
            {
                if (VW.FromDate == DateTime.MinValue || VW.ToDate == DateTime.MinValue)
                {
                    VW.ListIsuueSlipVW = new List<IssueSlipQueryViewModel>();
                }
                else
                {
                    DataTable DT = FS.Get_VehicleIssueSlipQuery(VW.TripNo, VW.FromDate.ToString("dd MMM yyyy"), VW.ToDate.ToString("dd MMM yyyy"), VW.Type, BaseLocationCode, VW.TripType, BaseCompanyCode);
                    VW.ListIsuueSlipVW = DataRowToObject.CreateListFromTable<IssueSlipQueryViewModel>(DT);
                }
                return View(VW);
            }
            catch (Exception)
            {
                return RedirectToAction("VehicleIssueSlipQueryCriteria", new { Type = VW.Passed_Type });
                throw;
            }

        }

        public ActionResult AdvanceEntry(string TripsheetNo, string Type)
        {
            ViewBag.Type = Type;
            AdvanceEntryViewModel VW = new AdvanceEntryViewModel();
            WEBX_TRIPSHEET_ADVEXP ObjWFVI = new WEBX_TRIPSHEET_ADVEXP();
            ObjWFVI.BranchCode = BaseLocationCode;
            ObjWFVI.Type = "M";
            ObjWFVI.AdvDate = System.DateTime.Now;

            WEBX_TRIPSHEET_ADVEXP ObjWTADV = new WEBX_TRIPSHEET_ADVEXP();
            ObjWTADV.adv_Chqdate = System.DateTime.Now.ToString("dd MMM yyyy");
            VW.ObjWTADV = ObjWTADV;
            VW.ObjWFVI = FS.Get_IssueslipData(TripsheetNo).First();
            VW.ListWFVI = FS.Get_VehicleAdvanceData(TripsheetNo);
            VW.ListWFVI.Add(ObjWFVI);
            VW.PC = new PaymentControl();

            return View(VW);
        }

        public JsonResult Getadv_acccode(string id)
        {
            List<Account_Cash_Bank_Code> List = new List<Account_Cash_Bank_Code>();
            List = FS.Get_adv_acccodeList(id, BaseLocationCode);
            return Json(List, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AdvanceEntry_Submit(AdvanceEntryViewModel VW, List<WEBX_TRIPSHEET_ADVEXP> Advance, PaymentControl PaymentControl)
        {
            string mTripSheetNo = "", VoucherNo = "", Message = "", Status = "", lblError = "";
            SqlCommand cmdDEL = new SqlCommand();

            System.Globalization.DateTimeFormatInfo dtfi = new System.Globalization.DateTimeFormatInfo();
            dtfi.ShortDatePattern = "dd/MM/yyyy";
            dtfi.DateSeparator = "/";
            dtfi.ShortTimePattern = "hh:mm tt";

            DateTime mServerDt = DateTime.Now;
            DateTime mIssueDt = new DateTime();
            mIssueDt = Convert.ToDateTime(VW.ObjWFVI.VSlipDtstr, dtfi);
            string accCode = PaymentControl.FuelCardNo;
            #region Transaction Validations

            if (PaymentControl.PaymentMode.ToUpper() == "BANK")
            {
                DataTable DT = OS.Duplicate_ChqNO(PaymentControl.ChequeNo, GF.FormateDate(PaymentControl.ChequeDate));
                accCode = PaymentControl.BankLedger;
                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
                if (Cnt != "T")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
            }
            if (PaymentControl.PaymentMode.ToUpper().ToUpper() == "CASH")
            {
                accCode = PaymentControl.CashLedger;
                PaymentControl.ChequeNo = null;
                string FIN_Start = "";
                FIN_Start = "01 Apr " + BaseYearVal.Split('_')[0];
                DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, System.DateTime.Now.ToString("dd MMM yyyy"), FIN_Start, BaseYearVal, PaymentControl.PayAmount.ToString());

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }

                if (Cnt != "T")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }
            }

            #endregion

            #region Validation

            if (lblError == "")
            {
                foreach (var item in Advance)
                {
                    if (item.Type == "M")
                    {
                        string mplace = item.AdvLoc;
                        string mAdDt = Convert.ToDateTime(item.AdvDate).ToString("dd MMM yyyy");
                        string mAmt1 = Convert.ToString(item.AdvAmt);
                        string mBCode = item.BranchCode;
                        string mSign = item.Signature;

                        if (mplace == "")
                        {
                            lblError = " Advance Place is Compulsory!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }
                        if (lblError == "" && mAdDt == "")
                        {
                            lblError = "Advance  Date is Compulsory!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }

                        if (lblError == "" && mAmt1 == "")
                        {
                            lblError = " Advance Amount is Compulsory!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }

                        if (lblError == "" && mBCode == "")
                        {
                            lblError = " Advance   Branchcode is Compulsory!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }

                        if (lblError == "" && mSign == "")
                        {
                            lblError = "Advance Payee is Compulsory!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }
                    }
                }
            }

            if (lblError == "")
            {
                foreach (var item in Advance)
                {
                    DateTime mAdvDt = new DateTime();
                    string mAdt = Convert.ToDateTime(item.AdvDate).ToString("dd MMM yyyy");

                    if (BaseYearVal != null)
                    {
                        if (mAdt != "" && item.Type == "M")
                        {
                            mAdvDt = Convert.ToDateTime(mAdt, dtfi);
                            DateTime dtFrom = new DateTime();
                            DateTime dtTo = new DateTime();

                            string From = "", To = "";

                            From = "01/04/" + BaseYearValFirst;
                            To = "31/03/" + Convert.ToDouble(Convert.ToDecimal(BaseYearValFirst) + 1);

                            dtFrom = Convert.ToDateTime(From, dtfi);
                            dtTo = Convert.ToDateTime(To, dtfi);

                            if (lblError == "")
                            {
                                if (mAdvDt > mServerDt)
                                {
                                    lblError = "Advance Date should not be greater than today's date!";
                                    ViewBag.StrError = lblError;
                                    return View("Error");
                                }

                                if (mAdvDt < mIssueDt)
                                {
                                    lblError = "Advance Date should not be less than Issue Slip Date!";
                                    ViewBag.StrError = lblError;
                                    return View("Error");
                                }
                            }
                        }
                    }
                }
            }

            if (lblError == "")
            {
                foreach (var item in Advance)
                {
                    if (item.Type == "M")
                    {
                        lblError = "";
                        string mBranchCode = item.BranchCode;
                        SqlConnection conn = new SqlConnection(Connstr);

                        conn.Open();
                        string DataFound = "N";

                        string msql = "select  * from webx_Location  where loccode='" + mBranchCode.Trim() + "' ";
                        SqlCommand cmd = new SqlCommand(msql, conn);
                        SqlDataReader mdr = cmd.ExecuteReader();
                        if (mdr.HasRows)
                        {
                            while (mdr.Read())
                            {
                                DataFound = "Y";
                            }
                        }
                        mdr.Close();

                        if (DataFound == "N")
                        {
                            lblError = "Invalid Branch Code!  Please Enter Valid Branch code!!";
                            ViewBag.StrError = lblError;
                            return View("Error");
                        }

                        conn.Close();
                    }
                }
            }

            #endregion

            if (lblError == "")
            {
                mTripSheetNo = VW.ObjWFVI.VSlipNo;
                string mBranchCode = "";
                string XML = "<root>";

                try
                {
                    foreach (var item in Advance)
                    {
                        if (item.Type == "M")
                        {
                            #region Validation

                            //string mAdvPlace = item.BranchCode;
                            string mAdvPlace = item.AdvLoc;
                            DateTime mAdvDt = new DateTime();
                            mAdvDt = Convert.ToDateTime(item.AdvDate, dtfi);

                            string mAdvAmt = item.AdvAmt;
                            mBranchCode = item.BranchCode;
                            string mSignature = item.Signature;
                            string dlsttranstype = PaymentControl.PaymentMode;
                            string dlstAcccode = accCode;
                            string txtChqNo = PaymentControl.ChequeNo;
                            string txtChqDate = PaymentControl.ChequeDate.ToString();
                            string txtAmount = PaymentControl.PayAmount.ToString();

                            string madv_Transtype = dlsttranstype;
                            if (madv_Transtype == "" || madv_Transtype == null)
                            {
                                madv_Transtype = "NULL";
                            }
                           


                            string madv_Chqno = txtChqNo;
                            //if (madv_Chqno == "" || madv_Chqno == null)
                            //{
                            //    madv_Chqno = "NULL";
                               
                            //}

                            string madv_acccode = dlstAcccode;
                            if (madv_acccode == "" || madv_acccode == null)
                            {
                                madv_acccode = "NULL";
                          
                            }
                           

                            string mEntryby = "" + BaseUserName + "";

                            #endregion

                            if (mAdvAmt != "" && mAdvAmt != null)
                            {
                                XML = XML + "<ADVEXP>";
                                XML = XML + "<TripSheetNo>" + mTripSheetNo + "</TripSheetNo>";
                                XML = XML + "<AdvLoc>" + mAdvPlace + "</AdvLoc>";
                                XML = XML + "<AdvDate>" + mAdvDt + "</AdvDate>";
                                XML = XML + "<AdvAmt>" + mAdvAmt + "</AdvAmt>";
                                XML = XML + "<BranchCode>" + mBranchCode + "</BranchCode>";
                                XML = XML + "<Signature>" + mSignature + "</Signature>";
                                XML = XML + "<adv_Transtype>" + madv_Transtype + "</adv_Transtype>";
                                if (madv_Transtype != "Cash")
                                {
                                    XML = XML + "<adv_Chqno>" + madv_Chqno + "</adv_Chqno>";
                                    XML = XML + "<adv_Chqdate>" + GF.FormateDate(PaymentControl.ChequeDate) + "</adv_Chqdate>";
                                }
                                XML = XML + "<adv_acccode>" + madv_acccode + "</adv_acccode>";
                                XML = XML + "</ADVEXP>";
                            }
                        }
                    }
                    XML = XML + "</root>";

                    string mFinYear = "";
                    mFinYear = BaseFinYear;
                    string mYearVal = "";

                    mYearVal = FS.FineYear(BaseYearValFirst);
                    DataTable DT_Add = FS.AddInsert_EnroutExpance(XML, mBranchCode, mTripSheetNo, mFinYear, mYearVal, BaseCompanyCode, BaseUserName);

                    VoucherNo = DT_Add.Rows[0]["VoucherNo"].ToString();
                    mTripSheetNo = DT_Add.Rows[0]["TripSheetNo"].ToString();
                    Message = DT_Add.Rows[0]["Message"].ToString();
                    Status = DT_Add.Rows[0]["Status"].ToString();

                    if (Message == "Done" && Status == "1")
                    {
                        return RedirectToAction("AdvanceEntryDone", new { No = mTripSheetNo, VoucherNo = VoucherNo });
                    }
                    else
                    {
                        ViewBag.StrError = Message;
                        return View("Error");
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                    return View("Error");
                }
            }
            return RedirectToAction("AdvanceEntryDone", new { No = mTripSheetNo, VoucherNo = VoucherNo });
        }

        public ActionResult AdvanceEntryDone(string No, string VoucherNo)
        {
            ViewBag.No = No;
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }

        #endregion

        #region TripSheet View Print

        public ActionResult TripSheetView()
        {
            TrackViewModel VM = new TrackViewModel();
            WEBX_FLEET_VEHICLE_ISSUE objissue = new WEBX_FLEET_VEHICLE_ISSUE();
            VM.TripTrackModel = objissue;
            return View(VM);
        }

        public ActionResult TripSheetViewList(TrackViewModel TrackV)
        {
            try
            {
                List<WEBX_FLEET_VEHICLE_ISSUE> WFVI = FS.Get_TripSheetView_List_By_Id(TrackV.ManualDocNo, TrackV.Type, GF.FormateDate(TrackV.FromDate), GF.FormateDate(TrackV.ToDate), TrackV.TripSheetStatus, TrackV.Branch, TrackV.Driverwise, TrackV.Vehiclewise);
                TrackV.TripTrackModelList = WFVI;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(TrackV);
        }

        public JsonResult GetDriver(string searchTerm)
        {
            List<VW_DRIVER_FINAL_INFO> ListLocations = new List<VW_DRIVER_FINAL_INFO>();

            ListLocations = TS.GetDriver_NameList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.Driver_Name,
                                  text = e.Driver_Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDriverNameOnly(string searchTerm)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();

            ListLocations = FS.Get_DriverList(searchTerm);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.CodeDesc,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region View / Print Job Order

        public ActionResult JobOrderCriteria()
        {
            JobOrderCriteria Criteria = new JobOrderCriteria();
            return View(Criteria);
        }

        [HttpPost]
        public string GetOrderList(JobOrderCriteria VM)
        {
            return JsonConvert.SerializeObject(FS.GetJobOrderViewList(VM, BaseCompanyCode));
        }

        #endregion

        #region View / Print Tyre Register

        public ActionResult TyreRegisterCriteria()
        {
            TyreRegisterViewModel Criteria = new TyreRegisterViewModel();
            return View(Criteria);
        }

        public ActionResult TyreRegisterViewPrintList(TyreRegisterViewModel TRV)
        {
            string FromDate = GF.FormateDate(TRV.FromDate);
            string ToDate = GF.FormateDate(TRV.ToDate);

            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(TRV);
        }

        #endregion

        #region View / Print Tyre life  cycle

        public ActionResult TyrelifecycleCriteria()
        {
            TyrelifecycleViewModel Criteria = new TyrelifecycleViewModel();
            return View(Criteria);
        }

        public ActionResult TyrelifecycleViewPrintList(TyrelifecycleViewModel TLCV)
        {
            string FromDate = GF.FormateDate(TLCV.FromDate);
            string ToDate = GF.FormateDate(TLCV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(TLCV);
        }

        #endregion

        #region View / Print Tyre Traking

        public ActionResult TyreTrakingCriteria()
        {
            TyreTrakingViewModel Criteria = new TyreTrakingViewModel();
            return View(Criteria);
        }

        public ActionResult TyreTrakingViewPrintList(TyreTrakingViewModel TKV)
        {
            return View(TKV);
        }

        #endregion

        #region AdvanceEntryXlsUpload
        public ActionResult UploadAdvanceEntryXLS()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UploadAdvanceEntryXLS(HttpPostedFileBase file)
        {
            try
            {
                if (file == null)
                {
                    ViewBag.StrError = "There is No Data In Excel Please Select Excel First!!!! ";
                    return View("Error");
                }
                else
                {
                    var filenameArr = file.FileName.Split('.');
                    var filextation = filenameArr[1].ToString();
                    if (filextation != "xls" && filextation != "xlsx" && filextation != "xml")
                    {
                        ViewBag.StrError = "Please Select Only Excel File!!!! ";
                        return View("Error");
                    }
                    else
                    {
                        DataSet ds = DataSetFromXLS();
                        DataTable Dt = ds.Tables[0];

                        List<TripAdvanceEntryXls> AdvanceEntryList = new List<TripAdvanceEntryXls>();
                        for (int i = 0; i < Dt.Rows.Count; i++)
                        {
                            TripAdvanceEntryXls advance = new TripAdvanceEntryXls();

                            advance.TripSheetNo = Dt.Rows[i]["TripSheetNo"].ToString();
                            advance.Place = Dt.Rows[i]["Place"].ToString();
                            advance.AdvanceDate = Convert.ToDateTime(Dt.Rows[i]["AdvanceDate"].ToString());
                            advance.Amount = Convert.ToDecimal(Dt.Rows[i]["Amount"].ToString());
                            advance.BranchCode = Dt.Rows[i]["BranchCode"].ToString();
                            advance.AdvancePaidBy = Dt.Rows[i]["AdvancePaidBy"].ToString();
                            advance.PaymetType = Dt.Rows[i]["PaymetType"].ToString();
                            advance.Chqno = Dt.Rows[i]["Chqno"].ToString();
                            advance.Chqdate = Dt.Rows[i]["Chqdate"].ToString();
                            advance.Bank = Dt.Rows[i]["Bank"].ToString();
                            AdvanceEntryList.Add(advance);
                        }
                        //List<TripAdvanceEntryXls> AdvanceEntryList = DataRowToObject.CreateListFromTable<TripAdvanceEntryXls>(Dt);
                        TempData["AdvanceEntryView"] = AdvanceEntryList;
                        return RedirectToAction("AdvanceEntryXLSView");
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
        }

        public DataSet DataSetFromXLS()
        {
            try
            {
                string sourceDir = Server.MapPath("~/Content/VoucherXLS/");
                string[] picList = Directory.GetFiles(sourceDir, "*.xlsx");

                foreach (string f in picList)
                {
                    FileInfo myfileinf = new FileInfo(f);
                    myfileinf.Delete();
                }
            }
            catch (Exception ex)
            {

            }
            DataSet ds = new DataSet();
            if (Request.Files["file"].ContentLength > 0)
            {
                string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);

                if (fileExtension == ".xls" || fileExtension == ".xlsx")
                {
                    string currDt = System.DateTime.Now.ToString("dd_MM_yyyy_hh_mm_ss");

                    string fileLocation = Server.MapPath("~/Content/VoucherXLS/") + Request.Files["file"].FileName.Replace(fileExtension, currDt + "" + fileExtension);
                    if (System.IO.File.Exists(fileLocation))
                    {

                        System.IO.File.Delete(fileLocation);
                    }
                    Request.Files["file"].SaveAs(fileLocation);
                    string excelConnectionString = string.Empty;
                    excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    //connection String for xls file format.
                    if (fileExtension == ".xls")
                    {
                        excelConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
                    }
                    //connection String for xlsx file format.
                    else if (fileExtension == ".xlsx")
                    {

                        excelConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
                    }
                    //Create Connection to Excel work book and add oledb namespace
                    OleDbConnection excelConnection = new OleDbConnection(excelConnectionString);
                    excelConnection.Open();
                    DataTable dt = new DataTable();

                    dt = excelConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    if (dt == null)
                    {
                        return null;
                    }

                    String[] excelSheets = new String[dt.Rows.Count];
                    int t = 0;
                    //excel data saves in temp file here.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[t] = row["TABLE_NAME"].ToString();
                        t++;
                    }
                    OleDbConnection excelConnection1 = new OleDbConnection(excelConnectionString);

                    string excelSheets_1 = "Sheet1$";

                    string query = string.Format("Select * from [{0}]", excelSheets_1);
                    using (OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, excelConnection1))
                    {
                        dataAdapter.Fill(ds);
                    }

                }
                if (fileExtension.ToString().ToLower().Equals(".xml"))
                {
                    string fileLocation = Server.MapPath("~/Content/VoucherXLS/") + Request.Files["FileUpload"].FileName;
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }

                    Request.Files["FileUpload"].SaveAs(fileLocation);
                    XmlTextReader xmlreader = new XmlTextReader(fileLocation);
                    // DataSet ds = new DataSet();
                    ds.ReadXml(xmlreader);
                    xmlreader.Close();
                }
            }


            return ds;
        }

        public ActionResult AdvanceEntryXLSView()
        {
            TripAdvanceXlsModel model = new TripAdvanceXlsModel();
            model.AdvanceVoucherList = TempData["AdvanceEntryView"] as List<TripAdvanceEntryXls>;

            if (model.AdvanceVoucherList == null)
                return RedirectToAction("UploadAdvanceEntryXLS");
            return View(model);
        }

        [HttpPost]
        public ActionResult AdvanceEntryXLSView(List<TripAdvanceEntryXls> AdvanceVoucherList)
        {
            List<TripAdvanceEntryXls> AdvanceEntryXLSDoneList = new List<TripAdvanceEntryXls>();
            string Status = "", ErrorMsg = "";
            try
            {
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(AdvanceVoucherList.GetType());

                //   ViewModel.CMPL.EntryBy = User.Identity.Name;

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, AdvanceVoucherList);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }


                DataSet DS = FS.UploadTripAdvanceEntryXls(xmlDoc1.InnerXml.Replace("'", "''"), BaseCompanyCode, BaseUserName);
                DataTable Dt1 = DS.Tables[0];
                Status = Dt1.Rows[0]["Status"].ToString();
                ErrorMsg = Dt1.Rows[0]["Message"].ToString();

                if (Status == "0")
                {
                    throw new Exception(ErrorMsg);
                }
                DataTable Dt2 = DS.Tables[1];

                AdvanceEntryXLSDoneList = DataRowToObject.CreateListFromTable<TripAdvanceEntryXls>(Dt2);

            }
            catch (Exception e)
            {
                ViewBag.StrError = e.Message;
                return View("Error");
            }
            TempData["AdvanceEntryXLSDoneList"] = AdvanceEntryXLSDoneList;
            return RedirectToAction("AdvanceEntryXLSDone");
        }

        public ActionResult AdvanceEntryXLSDone()
        {
            TripAdvanceXlsModel model = new TripAdvanceXlsModel();
            model.AdvanceVoucherList = (List<TripAdvanceEntryXls>)TempData["AdvanceEntryXLSDoneList"];
            return View(model);
        }
        #endregion AdvanceEntryXlsUpload
    }
}