﻿using CipherLib;
using CYGNUS.Classes;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class ContractController : BaseController
    {
        #region Declare

        FleetDataService.ContractService CS = new FleetDataService.ContractService();
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        GeneralFuncations GF = new GeneralFuncations();
        public static string xmlCONSER = "";
        public static string xmlTCTRN = "";
        public static string xmlWTDS = "";
        string[] arrzonename;
        string[] arrzonecode;
        int ZoneCount = 0;

        #endregion

        #region Customer Contract Master

        public ActionResult CustomerContract()
        {
            CustomerContractViewModel objCustomerContract = new CustomerContractViewModel();
            List<Webx_Master_General> ListCustomercontract = new List<Webx_Master_General>();
            List<webx_custcontract_hdr> LisContracttHdr = new List<webx_custcontract_hdr>();
            objCustomerContract.ListCustContract = LisContracttHdr;
            return View(objCustomerContract);
        }

        public JsonResult GetContractListJson(string searchTerm)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();
            ListLocations = MS.GetGeneralMasterObject().Where(c => c.CodeType == searchTerm && c.StatusCode == "Y").OrderBy(c => c.CodeId).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetConrtPayBasis(string custcode)
        {
            List<Webx_Master_General> Listpaycontract = new List<Webx_Master_General>();
            Listpaycontract = CS.Getcontactpay(custcode);
            if (Listpaycontract.Count <= 0 && Listpaycontract == null)
            {
                Listpaycontract = MS.GetGeneralMasterObject().Where(c => c.CodeType == "PAYTYP" && c.StatusCode == "Y").OrderBy(c => c.CodeDesc).ToList();
            }
            var LocationList = (from e in Listpaycontract
                                select new
                                {
                                    Value = e.CodeId,
                                    Text = e.CodeDesc,
                                }).Distinct().ToList();
            return Json(LocationList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCoustomerContList(CustomerContractViewModel CoustomerCode)
        {
            List<webx_custcontract_hdr> CusthdrList = new List<webx_custcontract_hdr>();
            string Name = "";
            if (CoustomerCode.CustomerContract.contractname != null)
                Name = CoustomerCode.CustomerContract.contractname;
            else
                Name = CoustomerCode.CustomerContract.Custcode;

            CusthdrList = CS.GetCustContList(Name);
            var Id = 0;
            foreach (var item in CusthdrList)
            {
                Id++;
                item.Srno = Id;
            }
            return PartialView("_Partial_CustContract", CusthdrList);
        }

        public JsonResult GetContractList(string str)
        {
            List<webx_custcontract_hdr> ListCustContract = new List<webx_custcontract_hdr>();
            ListCustContract = CS.GetCustomerContract(str).ToList();

            var SearchList = (from e in ListCustContract
                              select new
                              {
                                  id = e.ContractId,
                                  text = e.Cust_represent,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetCustomerName(string custcode)
        {
            string CustomerName = "";
            DataTable dt = CS.GetCustomerName(custcode);
            if (dt.Rows.Count > 0)
            {
                CustomerName = "<b> Customer Code & Name :</b> " + custcode + ": " + dt.Rows[0]["CustName"].ToString();
            }
            else
            {
                dt = CS.GetVendorName(custcode);
                CustomerName = "<b> Vendor Code & Name : </b>" + custcode + " : " + dt.Rows[0]["CustName"].ToString();
            }

            return CustomerName;
        }

        public JsonResult GetCustomerWithAddressList(string Type, string searchTerm)
        {
            List<webx_CUSTHDR> CUSTDHR = new List<webx_CUSTHDR>();
            //CUSTDHR = MS.GetCustomerMasterObject().Where(c => (c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper())) && c.CUST_ACTIVE.ToUpper() == "Y").ToList();
            //if (Type == "0")
            //    CUSTDHR = CUSTDHR.Take(30).ToList();
            //else
            //    CUSTDHR = CUSTDHR.Where(c => c.CUSTCAT.ToUpper() == Type.ToUpper()).Take(30).ToList();
            CUSTDHR = MS.GetCustomerList(Type, searchTerm).Take(30).ToList();

            var users = from user in CUSTDHR
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM + " : " + user.CUSTLOC
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public string GetCustomerNameOnlyContract(string custcode)
        {
            string CustomerName = "";
            DataTable dt = CS.GetCustomerNameOnlyContract(custcode);
            if (dt.Rows.Count > 0)
            {
                CustomerName = "<b> Customer Code & Name :</b> " + custcode + ": " + dt.Rows[0]["CustName"].ToString() + ": " + dt.Rows[0]["CustAddress"].ToString();
            }

            return CustomerName;
        }

        [HttpPost]
        public ActionResult AddContarct(string PaymentBasis, string StartDate, string EndDate, string CustCode)
        {
            List<webx_custcontract_hdr> CusthdrList = new List<webx_custcontract_hdr>();

            string Nocount = "";
            DataTable dt = CS.AddUpdateContract(PaymentBasis, StartDate, EndDate, CustCode);
            string contractID = CS.getCodeContractID();
            Nocount = dt.Rows[0]["Column1"].ToString();
            if (Nocount == "0")
            {
                Nocount = contractID;
                dt = CS.AddNewContract(CustCode, PaymentBasis, StartDate, EndDate, contractID);
                string Name = "";
                if (CustCode != null)
                    Name = CustCode;

                CusthdrList = CS.GetCustContList(Name);
                var Id = 0;
                foreach (var item in CusthdrList)
                {
                    Id++;
                    item.Srno = Id;
                }

            }
            return PartialView("_Partial_CustContract", CusthdrList);
        }
        [HttpPost]
        public ActionResult FinallyAddContarct(string PaymentBasis, string StartDate, string EndDate, string CustCode, string ContractId)
        {

            List<webx_custcontract_hdr> CusthdrList = new List<webx_custcontract_hdr>();
            string Name = "";
            if (CustCode != null)
                Name = CustCode;

            CusthdrList = CS.GetCustContList(Name);
            var Id = 0;
            foreach (var item in CusthdrList)
            {
                Id++;
                item.Srno = Id;
            }
            return PartialView("_Partial_CustContract", CusthdrList);
        }

        public static DateTime ToDateTime(string date, string culture)
        {
            System.Globalization.CultureInfo cif = new System.Globalization.CultureInfo(culture);
            DateTime dt;
            try
            {
                dt = Convert.ToDateTime(date, cif);
            }
            catch (Exception)
            {
                dt = DateTime.MinValue;
            }
            return dt;
        }

        [HttpPost]

        public void Upload(string ContractID, string files)
        {
            string extension = "", UserFileName = "", type = "Contract";
            var DocumentUploadedPath = "";

            webx_custcontract_hdr CusthdrList = new webx_custcontract_hdr();
            if (ContractID != null || ContractID != "" && files != null || files != "" || files != "undefine")
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    string Timedate = Convert.ToString(DateTime.Today.Day) + Convert.ToString(DateTime.Today.Month) + Convert.ToString(DateTime.Today.Year) + Convert.ToString(DateTime.Now.Hour) + Convert.ToString(DateTime.Now.Minute) + Convert.ToString(DateTime.Now.Minute) + Convert.ToString(DateTime.Now.Second);

                    var file = Request.Files[i];
                    var fileName = Path.GetFileName(file.FileName);
                    extension = System.IO.Path.GetExtension(file.FileName);
                    UserFileName = ContractID + "_" + Timedate + extension;
                    DocumentUploadedPath = AzureStorageHelper.UploadBlobFileForUser(file, UserFileName, ContractID.ToString(), type);
                    DataTable dt = CS.AddUploadFile(ContractID, DocumentUploadedPath);
                }

        }

        public ActionResult ContractScanCopyList(string contractID)
        {
            List<Webx_Contract_ScanCopy> contractScanCopyList = new List<Webx_Contract_ScanCopy>();

            contractScanCopyList = CS.GetContractScanCopyList(contractID);
            var Id = 0;
            foreach (var item in contractScanCopyList)
            {
                Id++;
                item.Srno = Id;
            }
            return PartialView("_PartialContractScanCopyList", contractScanCopyList);
        }

        #endregion

        #region Update Contract

        public ActionResult UpdateContract(string Custcode, string CustId, string ConType)
        {
            string serviceType = "";
            DataTable dtSlab = CS.GetSundrySlabType(CustId);
            ViewBag.ContractType = "";
            if (dtSlab.Rows.Count > 0)
            {
                ViewBag.ContractType = dtSlab.Rows[0]["slab_type"].ToString();

            }
            else
            {
                ViewBag.ContractType = "0";
            }
            DataTable dtservice = CS.GetServiceType(Custcode, CustId);
            if (dtservice.Rows.Count > 0)
            {

                serviceType = dtservice.Rows[0]["service_type"].ToString();
                string last = "";
                string first = "";
                if (serviceType == "2,1")
                {
                    first = serviceType.Substring(serviceType.LastIndexOf(',') + 1);
                    last = serviceType.Split(',')[0];
                }
                else if (serviceType == "1,2")
                {
                    last = serviceType.Substring(serviceType.LastIndexOf(',') + 1);
                    first = serviceType.Split(',')[0];
                }
                else
                {
                    last = serviceType.Substring(serviceType.LastIndexOf(',') + 1);
                    first = serviceType.Split(',')[0];
                }


                try
                {
                    if (first == "1")
                        ViewBag.SUNDRY = "Y";
                    else
                        ViewBag.SUNDRY = "N";

                    if (last == "2")
                        ViewBag.FTL = "Y";
                    else
                        ViewBag.FTL = "N";
                }

                catch (Exception) { }
            }

            CustomerContractViewModel objCustomerContract = new CustomerContractViewModel();

            /*START ODA Charge*/
            webx_custcontract_charge ObjGetODACharge = CS.GetContractchargeList(CustId).FirstOrDefault();

            if (ObjGetODACharge != null)
            {

                if (ObjGetODACharge.oda_yn == "Y")
                    ViewBag.ODA = "Y";
                else
                    ViewBag.ODA = "N";

            }
            /*END ODA Charge*/
            objCustomerContract.CustomerContract = CS.GetCustomerCodeList(CustId).FirstOrDefault();
            objCustomerContract.CustomerContract.slabType = ViewBag.ContractType;
            webx_custcontract_charge ChargeList = new webx_custcontract_charge();
            List<webx_custcontract_fovmatrix> ListMastrix = new List<webx_custcontract_fovmatrix>();
            List<Webx_Master_General> ListMGeneral = new List<Webx_Master_General>();
            List<webx_master_charge> ObjListcharge = new List<webx_master_charge>();
            webx_custcontract_charge_constraint objchrage = new webx_custcontract_charge_constraint();
            List<webx_custcontract_charge_constraint> ObjListconstatrin = new List<webx_custcontract_charge_constraint>();
            webx_custcontract_servicecharges objchargeservice = new webx_custcontract_servicecharges();
            objchargeservice.contractid = CustId;
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListsingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            Webx_CustContract_FRTMatrix_SingleSlab CustsingleSlab = new Webx_CustContract_FRTMatrix_SingleSlab();

            List<webx_custcontract_frtmatrix_slabhdr> Listslabhdr = new List<webx_custcontract_frtmatrix_slabhdr>();
            webx_custcontract_frtmatrix_slabhdr Custslabhdr = new webx_custcontract_frtmatrix_slabhdr();

            List<webx_custcontract_frtmatrix_slabhdr> ListslaODAbhdr = new List<webx_custcontract_frtmatrix_slabhdr>();
            objCustomerContract.ListslaODAbhdr = ListslaODAbhdr;

            List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
            objCustomerContract.ListLoading = ListLoading;
            List<webx_HandlingCharge> ListUnLoading = new List<webx_HandlingCharge>();
            objCustomerContract.ListUnloading = ListUnLoading;

            List<webx_custcontract_frtmatrix_slabhdr> ListMatrixCharge = new List<webx_custcontract_frtmatrix_slabhdr>();
            objCustomerContract.ListMatrixCharge = ListMatrixCharge;
            objCustomerContract.chargeContraint = objchrage;
            objCustomerContract.ListcustcontractMatrix = ListMastrix;
            objCustomerContract.ListcustcontractMatrixOwner = ListMastrix;
            objCustomerContract.CustomerCharge = ChargeList;
            objCustomerContract.ListMGeneral = ListMGeneral;
            objCustomerContract.Listcharge = ObjListcharge;
            objCustomerContract.ListContraint = ObjListconstatrin;
            objCustomerContract.CustServiceChrg = objchargeservice;
            objCustomerContract.ListsingleSlab = ListsingleSlab;
            objCustomerContract.CustsingleSlab = CustsingleSlab;

            objCustomerContract.Listslabhdr = Listslabhdr;
            objCustomerContract.Custslabhdr = Custslabhdr;

            objCustomerContract.BaseLoccode = BaseLocationCode;
            objCustomerContract.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            objCustomerContract.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();

            DataTable dt = CS.GetCustContName(Custcode, CustId);
            ViewBag.name = dt.Rows[0]["contractid"];
            ViewBag.cutscode = dt.Rows[0]["custcode"].ToString() + " : " + dt.Rows[0]["name"].ToString();
            ViewBag.type = ConType;


            return View(objCustomerContract);
        }

        public JsonResult GetMasterList(string str)
        {
            List<Webx_Master_General> MasterList = MS.GetGeneralMasterObject().Where(c => c.CodeType == str).OrderBy(c => c.CodeId).ToList();

            var SearchList = (from e in MasterList
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult TRNGetMasterList(string str)
        {
            List<Webx_Master_General> MasterList = MS.GetGeneralMasterObject().Where(c => c.CodeType == str && c.CodeId == "2").OrderBy(c => c.CodeId).ToList();

            var SearchList = (from e in MasterList
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add Contract basic Information

        public ActionResult TabService(webx_custcontract_hdr Custcontract)
        {
            string Id = Custcontract.ContractId;
            string RiskType = "";
            List<webx_custcontract_hdr> CustList = new List<webx_custcontract_hdr>();
            webx_custcontract_charge ChargeList = new webx_custcontract_charge();
            List<webx_custcontract_fovmatrix> ListMastrix = new List<webx_custcontract_fovmatrix>();
            List<webx_custcontract_fovmatrix> ListMastrixOwner = new List<webx_custcontract_fovmatrix>();
            webx_custcontract_fovmatrix objMatrix = new webx_custcontract_fovmatrix();
            CustomerContractViewModel ModelContract = new CustomerContractViewModel();
            webx_custcontract_servicecharges GetCustservChrg = new webx_custcontract_servicecharges();

            ListMastrix.Add(objMatrix);
            ListMastrixOwner.Add(objMatrix);

            ListMastrix = CS.GetMatrixList(Custcontract.ContractId, "C");

            ListMastrixOwner = CS.GetMatrixList(Custcontract.ContractId, "O");

            ModelContract.CustomerCharge = CS.GetContractchargeList(Id).FirstOrDefault();

            if (ModelContract.CustomerCharge != null)
                RiskType = ModelContract.CustomerCharge.risktype;
            else
                ModelContract.CustomerCharge = ChargeList;

            if (RiskType != null && RiskType != "")
                RiskType = ModelContract.CustomerCharge.risktype;
            else
                RiskType = "C";
            ChargeList.ContractId = Id;
            ChargeList.Custcode = Custcontract.Custcode;

            var AutoId = 0;
            var AutoId1 = 0;
            foreach (var item in ListMastrix)
            {
                AutoId++;
                item.Id = AutoId;
            }
            foreach (var item in ListMastrixOwner)
            {
                AutoId1++;
                item.Id = AutoId1;
            }

            //loading -- Unloading
            //Loading --- Unloading
            List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
            ListLoading = CS.GetHandlingChrge(Custcontract.ContractId, "L");
            if (ListLoading.Count == 0)
            {

                for (int i = 1; i < 4; i++)
                {
                    webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();

                    objHandlingCharge.Id = i;

                    if (i == 1)
                        objHandlingCharge.LoadingBy = "O";
                    if (i == 2)
                        objHandlingCharge.LoadingBy = "M";
                    if (i == 3)
                        objHandlingCharge.LoadingBy = "B";

                    ListLoading.Add(objHandlingCharge);
                }
            }
            else
            {
                int ID = 0;
                foreach (var item in ListLoading)
                {
                    ID++;
                    item.Id = ID;
                }

            }

            ModelContract.ListLoading = ListLoading;

            List<webx_HandlingCharge> ListUnLoading = new List<webx_HandlingCharge>();
            ListUnLoading = CS.GetHandlingChrge(Custcontract.ContractId, "U");

            if (ListUnLoading.Count == 0)
            {
                for (int i = 1; i < 4; i++)
                {
                    webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();

                    objHandlingCharge.Id = i;

                    if (i == 1)
                        objHandlingCharge.LoadingBy = "O";
                    if (i == 2)
                        objHandlingCharge.LoadingBy = "M";
                    if (i == 3)
                        objHandlingCharge.LoadingBy = "B";

                    ListUnLoading.Add(objHandlingCharge);
                }
            }
            else
            {
                int UID = 0;
                foreach (var item in ListUnLoading)
                {
                    UID++;
                    item.Id = UID;
                }
            }

            ModelContract.ListUnloading = ListUnLoading;

            ModelContract.BaseLoccode = BaseLocationCode;
            ModelContract.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            ModelContract.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();

            ModelContract.CustomerCharge.defaultvalue = CS.CustomerCharge();
            ModelContract.ListcustcontractMatrix = ListMastrix;
            ModelContract.ListcustcontractMatrixOwner = ListMastrixOwner;
            ModelContract.CustServiceChrg = GetCustservChrg;
            return PartialView("_ServiceSelection", ModelContract);
        }

        public ActionResult AddContractbasicInfo(webx_custcontract_hdr Custcontract)
        {
            string Id = Custcontract.ContractId;
            string RiskType = "";
            List<webx_custcontract_hdr> CustList = new List<webx_custcontract_hdr>();
            webx_custcontract_charge ChargeList = new webx_custcontract_charge();
            List<webx_custcontract_fovmatrix> ListMastrix = new List<webx_custcontract_fovmatrix>();
            List<webx_custcontract_fovmatrix> ListMastrixOwner = new List<webx_custcontract_fovmatrix>();
            webx_custcontract_fovmatrix objMatrix = new webx_custcontract_fovmatrix();
            CustomerContractViewModel ModelContract = new CustomerContractViewModel();
            webx_custcontract_servicecharges GetCustservChrg = new webx_custcontract_servicecharges();

            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                if (Custcontract.stax_yn_chek == true)
                    Custcontract.stax_yn = "Y";
                else
                    Custcontract.stax_yn = "N";
                XmlSerializer xmlSerializer = new XmlSerializer(Custcontract.GetType());
                if (Custcontract != null)
                {
                    using (MemoryStream xmlStream = new MemoryStream())
                    {
                        xmlSerializer.Serialize(xmlStream, Custcontract);
                        xmlStream.Position = 0;
                        xmlDoc.Load(xmlStream);
                        DataTable Dt = CS.UpdateCustomerCode(Id, xmlDoc.InnerXml.ReplaceSpecialCharacters());
                    }
                }
                objMatrix.Id = 1;
                ListMastrix.Add(objMatrix);
                ModelContract.ListcustcontractMatrix = ListMastrix;
                ModelContract.CustomerCharge = CS.GetContractchargeList(Id).FirstOrDefault();
                if (ModelContract.CustomerCharge != null)
                    RiskType = ModelContract.CustomerCharge.risktype;
                else
                    ModelContract.CustomerCharge = ChargeList;

                if (RiskType != null && RiskType != "")
                    RiskType = ModelContract.CustomerCharge.risktype;
                else
                    RiskType = "C";

                ChargeList.ContractId = Id;

                ChargeList.Custcode = Custcontract.Custcode;

                ListMastrix = CS.GetMatrixList(Custcontract.ContractId, "C");
                ListMastrixOwner = CS.GetMatrixList(Custcontract.ContractId, "O");

                //Loading --- Unloading
                List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
                ListLoading = CS.GetHandlingChrge(Custcontract.ContractId, "L");
                if (ListLoading.Count == 0)
                {

                    for (int i = 1; i < 4; i++)
                    {
                        webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();

                        objHandlingCharge.Id = i;

                        if (i == 1)
                            objHandlingCharge.LoadingBy = "O";
                        if (i == 2)
                            objHandlingCharge.LoadingBy = "M";
                        if (i == 3)
                            objHandlingCharge.LoadingBy = "B";

                        ListLoading.Add(objHandlingCharge);
                    }
                }
                else
                {
                    int ID = 0;
                    foreach (var item in ListLoading)
                    {
                        ID++;
                        item.Id = ID;
                    }

                }

                ModelContract.ListLoading = ListLoading;

                List<webx_HandlingCharge> ListUnLoading = new List<webx_HandlingCharge>();
                ListUnLoading = CS.GetHandlingChrge(Custcontract.ContractId, "U");
                if (ListUnLoading.Count == 0)
                {
                    for (int i = 1; i < 4; i++)
                    {
                        webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();

                        objHandlingCharge.Id = i;

                        if (i == 1)
                            objHandlingCharge.LoadingBy = "O";
                        if (i == 2)
                            objHandlingCharge.LoadingBy = "M";
                        if (i == 3)
                            objHandlingCharge.LoadingBy = "B";

                        ListUnLoading.Add(objHandlingCharge);
                    }
                }
                else
                {
                    int ID = 0;
                    foreach (var item in ListUnLoading)
                    {
                        ID++;
                        item.Id = ID;
                    }

                }

                ModelContract.ListUnloading = ListUnLoading;


                var AutoId = 0;
                foreach (var item in ListMastrix)
                {
                    AutoId++;
                    item.Id = AutoId;
                }

                var AutoId1 = 0;
                foreach (var item in ListMastrixOwner)
                {
                    AutoId1++;
                    item.Id = AutoId1;
                }

                ModelContract.ListcustcontractMatrix = ListMastrix;
                ModelContract.ListcustcontractMatrixOwner = ListMastrixOwner;
                ModelContract.CustServiceChrg = GetCustservChrg;
                ModelContract.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");

            }
            return PartialView("_ServiceSelection", ModelContract);
        }
        #endregion

        #region Add Matrix
        public ActionResult ADDCustContMatrix(int id)
        {
            webx_custcontract_fovmatrix ObjMatrix = new webx_custcontract_fovmatrix();
            ObjMatrix.Id = id;

            return PartialView("_PartialMatrix", ObjMatrix);
        }
        public ActionResult ADDCustContMatrixOwner(int id)
        {
            webx_custcontract_fovmatrix ObjMatrix = new webx_custcontract_fovmatrix();
            ObjMatrix.Id = id;

            return PartialView("_PartialMatrixOwner", ObjMatrix);
        }
        #endregion

        #region Add Contract Basic Information
        public ActionResult AddServiceSelection(CustomerContractViewModel Custcontract, List<webx_custcontract_fovmatrix> CustMatrix, List<webx_custcontract_fovmatrix> CustMatrixOwner, List<webx_HandlingCharge> LoadingList, List<webx_HandlingCharge> UNLoadingList)
        {
            string custcode = Custcontract.CustomerCharge.Custcode;
            string contractid = Custcontract.CustomerCharge.ContractId;
            webx_custcontract_servicecharges ObjModeservice = new webx_custcontract_servicecharges();
            Webx_Master_General ObjMGeneral = new Webx_Master_General();
            try
            {

                DataTable Dt = new DataTable();
                if (Custcontract.CustomerCharge.Volumetric == true)
                    Custcontract.CustomerCharge.vol_yn = "Y";
                else
                    Custcontract.CustomerCharge.vol_yn = "N";

                if (Custcontract.CustomerCharge.Codecharge == true)
                    Custcontract.CustomerCharge.cod_dod_yn = "Y";
                else
                    Custcontract.CustomerCharge.cod_dod_yn = "N";
                if (Custcontract.CustomerCharge.DACcarage == true)
                    Custcontract.CustomerCharge.dacc_yn = "Y";
                else
                    Custcontract.CustomerCharge.dacc_yn = "N";
                if (Custcontract.CustomerCharge.UseInvoiceNo == true)
                    Custcontract.CustomerCharge.use_invno = "Y";
                else
                    Custcontract.CustomerCharge.use_invno = "N";
                if (Custcontract.CustomerCharge.UserInvoiceDate == true)
                    Custcontract.CustomerCharge.use_invdt = "Y";
                else
                    Custcontract.CustomerCharge.use_invdt = "N";

                if (Custcontract.CustomerCharge.chkfuelsurcharge == true)
                    Custcontract.CustomerCharge.diesel_hike_yn = "Y";
                else
                    Custcontract.CustomerCharge.diesel_hike_yn = "N";

                if (Custcontract.CustomerCharge.chkoctroisurcharge == true)
                    Custcontract.CustomerCharge.oct_sur_yn = "Y";
                else
                    Custcontract.CustomerCharge.oct_sur_yn = "N";

                if (Custcontract.CustomerCharge.cod_chrg_rs == "" || Custcontract.CustomerCharge.cod_chrg_rs == null)
                    Custcontract.CustomerCharge.cod_chrg_rs = "0.00";

                if (Custcontract.CustomerCharge.dacc_chrg_rs == "" || Custcontract.CustomerCharge.dacc_chrg_rs == null)
                    Custcontract.CustomerCharge.dacc_chrg_rs = "0.00";

                xmlCONSER = "<root><contract_flags>";
                xmlCONSER = xmlCONSER + "<flatmultipickup_yn>" + Custcontract.CustomerCharge.flatmultipickup_yn + "</flatmultipickup_yn>" +
                                      "<custcode>" + Custcontract.CustomerCharge.Custcode + "</custcode>" +
                                      "<contractid>" + Custcontract.CustomerCharge.ContractId + "</contractid>" +
                                      "<locmultipickup_yn>" + Custcontract.CustomerCharge.locmultipickup_yn + "</locmultipickup_yn>" +
                                      "<flatloading_yn>" + Custcontract.CustomerCharge.flatloading_yn + "</flatloading_yn>" +
                                      "<locloading_yn>" + Custcontract.CustomerCharge.locloading_yn + "</locloading_yn>" +
                                      "<flatmultidelivery_yn>" + Custcontract.CustomerCharge.flatmultidelivery_yn + "</flatmultidelivery_yn>" +
                                      "<locmultidelivery_yn>" + Custcontract.CustomerCharge.locmultidelivery_yn + "</locmultidelivery_yn>" +
                                      "<flatunloading_yn>" + Custcontract.CustomerCharge.flatunloading_yn + "</flatunloading_yn>" +
                                      "<locunloading_yn>" + Custcontract.CustomerCharge.locunloading_yn + "</locunloading_yn>" +
                                      "<vol_yn>" + Custcontract.CustomerCharge.vol_yn + "</vol_yn>" +
                                      "<cft_weight_type>" + Custcontract.CustomerCharge.cft_weight_type + "</cft_weight_type>" +
                                      "<difer_disc_yn>" + Custcontract.CustomerCharge.difer_disc_yn + "</difer_disc_yn>" +
                                      "<difer_days>" + Custcontract.CustomerCharge.difer_days + "</difer_days>" +
                                      "<difer_disc_per>" + Custcontract.CustomerCharge.difer_disc_per + "</difer_disc_per>" +
                                      "<mp_chrg_appl>" + Custcontract.CustomerCharge.mp_chrg_appl + "</mp_chrg_appl>" +
                                      "<risktype>" + Custcontract.CustomerCharge.risktype + "</risktype>" +
                                      "<min_freight_type>" + Custcontract.CustomerCharge.min_freight_type + "</min_freight_type>" +
                                      "<min_freight_percent_yn>" + Custcontract.CustomerCharge.min_freight_percent_yn + "</min_freight_percent_yn>" +
                                      "<min_subtotal_percent_yn>" + Custcontract.CustomerCharge.min_subtotal_percent_yn + "</min_subtotal_percent_yn>" +
                                      "<cod_dod_yn>" + Custcontract.CustomerCharge.cod_dod_yn + "</cod_dod_yn>" +
                                      "<dacc_yn>" + Custcontract.CustomerCharge.dacc_yn + "</dacc_yn>" +
                                      "<cod_chrg_rs>" + Custcontract.CustomerCharge.cod_chrg_rs + "</cod_chrg_rs>" +
                                      "<cod_chrg_per>" + Custcontract.CustomerCharge.cod_chrg_per + "</cod_chrg_per>" +
                                      "<cod_chrg_ratetype>" + Custcontract.CustomerCharge.cod_chrg_ratetype + "</cod_chrg_ratetype>" +
                                      "<dacc_chrg_rs>" + Custcontract.CustomerCharge.dacc_chrg_rs + "</dacc_chrg_rs>" +
                                      "<dacc_chrg_per>" + Custcontract.CustomerCharge.dacc_chrg_per + "</dacc_chrg_per>" +
                                      "<dacc_chrg_ratetype>" + Custcontract.CustomerCharge.dacc_chrg_ratetype + "</dacc_chrg_ratetype>" +
                                      "<oda_yn>" + (Custcontract.CustomerCharge.ODAchatrge == true ? "Y" : "N") + "</oda_yn>" +
                                      "<del_before_dem_yn>" + Custcontract.CustomerCharge.del_before_dem_yn + "</del_before_dem_yn>" +
                                      "<dem_cal_bas>" + Custcontract.CustomerCharge.dem_cal_bas + "</dem_cal_bas>" +
                                      "<dem_day>" + Custcontract.CustomerCharge.dem_day + "</dem_day>" +
                                      "<dem_chrg_min>" + Custcontract.CustomerCharge.dem_chrg_min + "</dem_chrg_min>" +
                                      "<dem_chrg_per>" + Custcontract.CustomerCharge.dem_chrg_per + "</dem_chrg_per>" +
                                      "<dem_chrg_ratetype>" + Custcontract.CustomerCharge.dem_chrg_ratetype + "</dem_chrg_ratetype>" +
                                      "<dem_chrg_max>" + Custcontract.CustomerCharge.dem_chrg_max + "</dem_chrg_max>" +
                                      "<diesel_hike_yn>" + Custcontract.CustomerCharge.diesel_hike_yn + "</diesel_hike_yn>" +
                                      "<oct_sur_yn>" + Custcontract.CustomerCharge.oct_sur_yn + "</oct_sur_yn>" +
                                      "<oct_sur_bas>" + Custcontract.CustomerCharge.oct_sur_bas + "</oct_sur_bas>" +
                                      "<rate_oct_sur>" + Custcontract.CustomerCharge.rate_oct_sur + "</rate_oct_sur>" +
                                      "<min_oct_sur>" + Custcontract.CustomerCharge.min_oct_sur + "</min_oct_sur>" +
                                      "<max_oct_sur>" + Custcontract.CustomerCharge.max_oct_sur + "</max_oct_sur>" +
                                      "<billgen_loccode>" + Custcontract.CustomerCharge.billgen_loccode + "</billgen_loccode>" +
                                      "<bill_inst>" + Custcontract.CustomerCharge.bill_inst + "</bill_inst>" +
                                      "<billsub_loccode>" + Custcontract.CustomerCharge.billsub_loccode + "</billsub_loccode>" +
                                      "<credit_limit>" + Custcontract.CustomerCharge.credit_limit + "</credit_limit>" +
                                      "<billcol_loccode>" + Custcontract.CustomerCharge.billcol_loccode + "</billcol_loccode>" +
                                      "<credit_day>" + Custcontract.CustomerCharge.credit_day + "</credit_day>" +
                                      "<sku_yn>" + Custcontract.CustomerCharge.sku_yn + "</sku_yn>" +
                                      "<stax_paidby>" + Custcontract.CustomerCharge.stax_paidby + "</stax_paidby>" +
                                      "<comm_bus>" + Custcontract.CustomerCharge.comm_bus + "</comm_bus>" +
                                      "<charge_bas>" + Custcontract.CustomerCharge.charge_bas + "</charge_bas>" +
                                        "<rate_per_inv_yn>" + Custcontract.CustomerCharge.rate_per_inv_yn + "</rate_per_inv_yn>" +
                                      "<rate_per_inv>" + Custcontract.CustomerCharge.rate_per_inv + "</rate_per_inv>" +
                                      "<frt_disc_yn>" + Custcontract.CustomerCharge.frt_disc_yn + "</frt_disc_yn>" +
                                      "<frt_disc_rate>" + Custcontract.CustomerCharge.frt_disc_rate.ToString("F2") + "</frt_disc_rate>" +
                                      "<frt_disc_ratetype>" + Custcontract.CustomerCharge.frt_disc_ratetype + "</frt_disc_ratetype>" +
                                      "<frt_disc_contractid>" + Custcontract.CustomerCharge.frt_disc_contractid + "</frt_disc_contractid>" +
                                      "<use_invno>" + Custcontract.CustomerCharge.use_invno + "</use_invno>" +
                                      "<trans_type>" + Custcontract.CustomerCharge.trans_type + "</trans_type>" +
                                       "<rate_type>" + Custcontract.CustomerCharge.rate_type + "</rate_type>" +
                                       "<pkp_dly>" + Custcontract.CustomerCharge.pkp_dly + "</pkp_dly>" +
                                       "<service_type>" + Custcontract.CustomerCharge.service_type + "</service_type>" +
                                       "<matrix_type>" + Custcontract.CustomerCharge.matrix_type + "</matrix_type>" +
                                       "<bill_schedule>" + Custcontract.CustomerCharge.bill_schedule + "</bill_schedule>" +
                                       "<BillingState>" + Custcontract.CustomerCharge.BillingState + "</BillingState>" +
                                       "<AllowPartialPayment>" + Custcontract.CustomerCharge.AllowPartialPayment + "</AllowPartialPayment>" +
                                       "<use_invdt>" + Custcontract.CustomerCharge.use_invdt + "</use_invdt></contract_flags></root>";

                Dt = CS.AddContractService(Custcontract.CustomerCharge.ContractId, xmlCONSER, BaseUserName);

                if (CustMatrix != null)
                {
                    Dt = CS.DeleteCustContMatrix(Custcontract.CustomerCharge.ContractId, "C");
                    foreach (var item in CustMatrix)
                    {
                        if (item.slabto > 0)
                        {
                            Dt = CS.InsertCustContMatrix(Custcontract.CustomerCharge.ContractId, "C", item.slabfrom, item.slabto, item.fovrate, item.ratetype);
                        }
                    }
                }

                if (CustMatrixOwner != null)
                {
                    Dt = CS.DeleteCustContMatrix(Custcontract.CustomerCharge.ContractId, "O");
                    foreach (var item in CustMatrixOwner)
                    {
                        if (item.slabto > 0)
                        {
                            Dt = CS.InsertCustContMatrix(Custcontract.CustomerCharge.ContractId, "O", item.slabfrom, item.slabto, item.fovrate, item.ratetype);
                        }
                    }
                }
                //Loading -- UNloading
                //if (LoadingList != null)
                //{
                //    Dt = CS.DeleteHandlingChrge(Custcontract.CustomerCharge.ContractId, "L");
                //    foreach (var item in LoadingList)
                //    {

                //        Dt = CS.InsertHandlingChrge(Custcontract.CustomerCharge.ContractId, "L", item.ContractType, item.LoadingBy, item.RateType, item.Rate, item.Wt_Pkg, item.TotalCharge, item.MaxAmount, BaseUserName, System.DateTime.Now);
                //    }
                //}

                //if (UNLoadingList != null)
                //{
                //    Dt = CS.DeleteHandlingChrge(Custcontract.CustomerCharge.ContractId, "U");
                //    foreach (var item in UNLoadingList)
                //    {

                //        Dt = CS.InsertHandlingChrge(Custcontract.CustomerCharge.ContractId, "U", item.ContractType, item.LoadingBy, item.RateType, item.Rate, item.Wt_Pkg, item.TotalCharge, item.MaxAmount, BaseUserName, System.DateTime.Now);


                //    }
                //}

                ObjMGeneral = CS.GetContractTransMode(Custcontract.CustomerCharge.ContractId).FirstOrDefault();
                Custcontract.CustServiceChrg = CS.GetListModeService(Custcontract.CustomerCharge.ContractId, ObjMGeneral.CodeId).FirstOrDefault();
                if (Custcontract.CustServiceChrg != null)
                {
                    Custcontract.CustServiceChrg.custcode = Custcontract.CustomerCharge.Custcode;
                    Custcontract.CustServiceChrg.contractid = Custcontract.CustomerCharge.ContractId;

                }
                else
                {
                    Custcontract.CustServiceChrg = ObjModeservice;
                    Custcontract.CustServiceChrg.custcode = Custcontract.CustomerCharge.Custcode;
                    Custcontract.CustServiceChrg.contractid = Custcontract.CustomerCharge.ContractId;
                }
                /* Get List Of ModeVise Service */
                Custcontract.ListMGeneral = CS.GetContractTransMode(Custcontract.CustomerCharge.ContractId);

                Custcontract.CustomerCharge.defaultvalue = CS.CustomerCharge();
            }
            catch (Exception)
            {

            }
            return PartialView("_ModeViceService", Custcontract);
        }

        public ActionResult TabAddmodewise(CustomerContractViewModel Custcontract, string ContractId, string CustCode)
        {
            webx_custcontract_servicecharges ObjModeservice = new webx_custcontract_servicecharges();
            Webx_Master_General ObjMGeneral = new Webx_Master_General();
            ObjMGeneral = CS.GetContractTransMode(ContractId).FirstOrDefault();
            if (ObjMGeneral != null)
            {
                Custcontract.CustServiceChrg = CS.GetListModeService(ContractId, ObjMGeneral.CodeId).FirstOrDefault();
            }
            if (Custcontract.CustServiceChrg != null)
            {
                Custcontract.CustServiceChrg.custcode = CustCode;
                Custcontract.CustServiceChrg.contractid = ContractId;

            }
            else
            {
                Custcontract.CustServiceChrg = ObjModeservice;
                Custcontract.CustServiceChrg.custcode = CustCode;
                Custcontract.CustServiceChrg.contractid = ContractId;
            }
            /* Get List Of ModeVise Service */
            Custcontract.ListMGeneral = CS.GetContractTransMode(ContractId);
            return PartialView("_ModeViceService", Custcontract);
        }

        #endregion

        #region Add Mode Vice service
        [HttpPost]
        public ActionResult GetListService(string ContractId, string Id)
        {
            webx_custcontract_servicecharges ObjModeservice = new webx_custcontract_servicecharges();

            ObjModeservice = CS.GetListModeService(ContractId, Id).FirstOrDefault();
            if (ObjModeservice == null)
            {
                ObjModeservice = new webx_custcontract_servicecharges();
            }
            return PartialView("_PartialModeVice", ObjModeservice);
        }
        [HttpPost]
        public ActionResult AddModeViceService(CustomerContractViewModel CustContact, webx_custcontract_servicecharges ModeViseList)
        {
            List<webx_master_charge> ObjListcharge = new List<webx_master_charge>();
            List<webx_custcontract_charge_constraint> objchargcont = new List<webx_custcontract_charge_constraint>();
            List<webx_master_charge> objChrage = new List<webx_master_charge>();
            List<Webx_Master_General> objgeneral = new List<Webx_Master_General>();
            string xmlTCHDR = "";
            xmlTCHDR = "<root><CCMModeWiseService>";
            xmlTCHDR = xmlTCHDR + "<CustCode>" + CustContact.CustServiceChrg.custcode + "</CustCode>" +
                                  "<ContractID>" + CustContact.CustServiceChrg.contractid + "</ContractID>" +
                                  "<CutoffHours>" + ModeViseList.cutoff_hrs + "</CutoffHours>" +
                                  "<CutoffMinutes>" + ModeViseList.cutoff_min + "</CutoffMinutes>" +
                                  "<CutoffTrdays>" + ModeViseList.cutoff_trdays + "</CutoffTrdays>" +
                                  "<FreightLowerLimit>" + ModeViseList.lowlim_frt + "</FreightLowerLimit>" +
                                  "<FreightUpperLimit>" + ModeViseList.upplim_frt + "</FreightUpperLimit>" +
                                  "<FuelSurchargeBase>" + ModeViseList.fuelsurchrgbas + "</FuelSurchargeBase>" +
                                  "<FuelSurchargeRate>" + ModeViseList.fuelsurchrg + "</FuelSurchargeRate>" +
                                  "<MaxFuelSurcharge>" + ModeViseList.max_fuelsurchrg + "</MaxFuelSurcharge>" +
                                  "<MinFreightBase>" + ModeViseList.min_frtbas + "</MinFreightBase>" +
                                  "<MinFreightBaseRate>" + ModeViseList.min_frtbasrate + "</MinFreightBaseRate>" +
                                  "<MinFreightPer>" + ModeViseList.min_frtrate_per + "</MinFreightPer>" +
                                  "<MinFuelSurcharge>" + ModeViseList.min_fuelsurchrg + "</MinFuelSurcharge>" +
                                  "<MinSubtotalPer>" + ModeViseList.min_subtot_per + "</MinSubtotalPer>" +
                                  "<SubtotalLowerLimit>" + ModeViseList.lowlim_subtot + "</SubtotalLowerLimit>" +
                                  "<SubtotalUpperLimit>" + ModeViseList.upplim_subtot + "</SubtotalUpperLimit>" +
                                  "<TransMode>" + CustContact.CustServiceChrg.trans_type + "</TransMode>" +
                                  "<VolRatio>" + ModeViseList.cft_ratio + "</VolRatio>" +
                                  "<STaxPaidByOpts>" + ModeViseList.stax_paidby_opts + "</STaxPaidByOpts>" +
                                  "<STaxPaidBy>" + ModeViseList.stax_paidby + "</STaxPaidBy>" +
                                  "<STaxPaidByEnabled>" + "Y" + "</STaxPaidByEnabled>" +
                                  "<VolMeasure>" + ModeViseList.cft_measure + "</VolMeasure>" +
                                  "<InvoiceRateApplay>" + ModeViseList.InvoiceRateApplay + "</InvoiceRateApplay>" +
                                  "<InvoiceRate>" + (ModeViseList.InvoiceRateApplay == false ? 0 : ModeViseList.InvoiceRate) + "</InvoiceRate>" +
                                  /*START GST Changes Chirag D*/
                                  "<gstpaidby>" + ModeViseList.gstpaidby + "</gstpaidby>";
            /*END GST Changes Chirag D*/

            xmlTCHDR = xmlTCHDR + "</CCMModeWiseService></root>";
            DataTable Dt = CS.AddContractModeViceService(xmlTCHDR, "");
            objchargcont = CS.GetchargeMatrixList(CustContact.CustServiceChrg.contractid, "BKG");
            CustContact.ListContraint = objchargcont;
            CustContact.chargeContraint = objchargcont.FirstOrDefault();
            return PartialView("_ChargeMatrix", CustContact);
        }
        [HttpPost]
        public ActionResult GetContCharge(string ContractId, string Id)
        {
            CustomerContractViewModel CustContact = new CustomerContractViewModel();
            List<webx_custcontract_charge_constraint> objchargcont = new List<webx_custcontract_charge_constraint>();
            List<webx_master_charge> objChrage = new List<webx_master_charge>();
            List<Webx_Master_General> objgeneral = new List<Webx_Master_General>();
            objchargcont = CS.GetchargeMatrixList(ContractId, Id);

            return PartialView("_PartialChargeMatrix", objchargcont);
        }
        #endregion

        #region Insert AddModeChargeMatrix

        public ActionResult AddModeChargeMatrix(CustomerContractViewModel custcontract, List<webx_custcontract_charge_constraint> ChargeMatrixList)
        {
            List<webx_master_charge> objcharge = new List<webx_master_charge>();
            string strsql = "";
            try
            {
                strsql = "DELETE FROM dbo.webx_custcontract_charge_constraint ";
                strsql = strsql + " WHERE contractid='" + custcontract.CustServiceChrg.contractid + "'";
                strsql = strsql + " AND chargetype='" + custcontract.chargeContraint.chargename + "'";
                CS.DeleteChargeMatrix(strsql);

                foreach (var item in ChargeMatrixList)
                {
                    string strfields, strvalues;
                    strfields = "INSERT INTO dbo.webx_custcontract_charge_constraint ";
                    strfields = strfields + " (contractid,basedon,basecode,chargecode,chargetype,";
                    strfields = strfields + "use_from,use_to,use_trans_type,use_rate_type,slab_type,Loop_Wise_Contract)";

                    if (item.usefrom == true)
                        item.use_from = "Y";
                    else
                        item.use_from = "N";

                    if (item.useto == true)
                        item.use_to = "Y";
                    else
                        item.use_to = "N";

                    if (item.usetrantype == true)
                        item.use_trans_type = "Y";
                    else
                        item.use_trans_type = "N";
                    if (item.useratetype == true)
                        item.use_rate_type = "Y";
                    else
                        item.use_rate_type = "N";

                    if (item.LoopWiseContract == true)
                        item.Loop_Wise_Contract = "Y";
                    else
                        item.Loop_Wise_Contract = "N";

                    //if (item.chargecode == "2")
                    //{
                    //    item.use_from = "Y";
                    //    item.use_to = "Y";
                    //    item.use_trans_type = "Y";
                    //    item.use_rate_type = "Y";
                    //    item.basedon = "NONE";
                    //    item.slab_type = "N";
                    //}

                    strvalues = " VALUES('" + custcontract.CustServiceChrg.contractid + "',";
                    strvalues = strvalues + "'" + item.basedon + "','NONE',";
                    strvalues = strvalues + "'" + item.chargecode + "','" + custcontract.chargeContraint.chargename + "',";
                    strvalues = strvalues + "'" + item.use_from + "',";
                    strvalues = strvalues + "'" + item.use_to + "',";
                    strvalues = strvalues + "'" + item.use_trans_type + "',";
                    strvalues = strvalues + "'" + item.use_rate_type + "',";
                    strvalues = strvalues + "'" + item.slab_type + "',";
                    strvalues = strvalues + "'" + item.Loop_Wise_Contract + "')";

                    strsql = strfields + strvalues;
                    CS.InsertChargeMatrix(strsql);
                }

                webx_custcontract_charge_constraint objcustconstratint = new webx_custcontract_charge_constraint();
                List<Webx_CustContract_FRTMatrix_SingleSlab> ListsingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
                Webx_CustContract_FRTMatrix_SingleSlab objslab = new Webx_CustContract_FRTMatrix_SingleSlab();
                custcontract.CustsingleSlab = objslab;
                objcustconstratint.use_to = "NA";
                objcustconstratint.use_from = "NA";
                objcustconstratint.use_trans_type = "NA";
                objcustconstratint.use_rate_type = "NA";
                objcustconstratint.Loop_Wise_Contract = "NA";
                custcontract.CustsingleSlab = CS.getstandardCharge(custcontract.CustServiceChrg.contractid).FirstOrDefault();
                if (custcontract.CustsingleSlab == null)
                    custcontract.CustsingleSlab = objslab;
                custcontract.chargeContraint = objcustconstratint;
                custcontract.ListsingleSlab = ListsingleSlab;
                custcontract.Listcharge = CS.GetChargeMatrixCriteria().ToList();
            }

            catch (Exception)
            {
            }
            return PartialView("_StandardCharge", custcontract);
        }

        #endregion

        #region Standard Charge

        public ActionResult AddStandardCharge(CustomerContractViewModel custcontract, List<Webx_CustContract_FRTMatrix_SingleSlab> FTLList, List<webx_custcontract_charge_constraint> ObjChargeContraint1)
        {

            string RateType, TransMode, From, To, OldTransMode, MatrixType = "", FromLoc, OldFromLoc, ToLoc, OldToLoc;
            string xmlFRIGHT = "";
            string xmlFRIGHTCCM = "";

            webx_custcontract_charge_constraint objcharge = new webx_custcontract_charge_constraint();
            objcharge = ObjChargeContraint1.FirstOrDefault();

            xmlFRIGHT = "<ArrayOfCCMMatrixCombination>";
            xmlFRIGHTCCM = "<ArrayOfCCMChargeMatrix>";
            foreach (var item in FTLList)
            {
                //First XML
                if (objcharge.use_trans_type == "Y")
                    TransMode = item.TransMode;
                else
                    TransMode = "NA";

                xmlFRIGHT = xmlFRIGHT + "<CCMMatrixCombination>" + " <ContractID>" + item.contractid + "</ContractID>" +
                                        "<FromLoc>" + item.FromLoc + "</FromLoc>" +
                                        "<ToLoc>" + item.toloc + "</ToLoc>" +
                                        "<MatrixType>" + objcharge.FilterMatrixtype + "</MatrixType>" +
                                         "<FlagInsert>" + "Y" + "</FlagInsert>" +
                                         "<BillLoc>" + item.BillLoc + "</BillLoc>" +
                                         "<Transmode>" + TransMode + "</Transmode>" + "</CCMMatrixCombination>";
                if (objcharge.use_from != "Y")
                    item.FromLoc = "NA"; item.Matrixtype = "O";

                if (objcharge.use_to != "Y")
                    item.toloc = "NA"; item.Matrixtype = "D";

                if (objcharge.use_from != "Y" && objcharge.use_to != "Y")
                    objcharge.FilterMatrixtype = "NA";

                if (objcharge.use_rate_type == "Y")
                    RateType = item.ratetype;
                else
                    RateType = "F";




                if (objcharge.use_from == "Y")
                {
                    if (objcharge.FilterMatrixtype == "R")

                        From = item.FromLoc;
                }
                else
                    From = "NA";

                if (objcharge.use_to == "Y")
                {
                    if (objcharge.FilterMatrixtype == "R")

                        To = item.toloc;
                }
                else
                    To = "NA";
                if (objcharge.Matrixtype == null && objcharge.Matrixtype == "")
                    MatrixType = "NA";
                else
                    MatrixType = objcharge.Matrixtype;
                //second XML
                //*** if (hdnMatrixType.Value == "R") ***

                if (objcharge.Matrixtype == "R")
                {
                    FromLoc = ReturnZoneCode(item.FromLoc);
                    OldFromLoc = ReturnZoneCode(custcontract.CustsingleSlab.FromLoc);
                    ToLoc = ReturnZoneCode(item.FromLoc);
                    OldToLoc = ReturnZoneCode(custcontract.CustsingleSlab.toloc);
                }
                else
                {
                    if (objcharge.use_from == "Y")
                    {
                        OldFromLoc = item.OldFromLoc;
                        FromLoc = item.FromLoc;
                    }
                    else
                    {
                        FromLoc = "NA";
                        OldFromLoc = "NA";
                    }

                    if (objcharge.use_to == "Y")
                    {
                        OldToLoc = item.OldToLoc;
                        ToLoc = item.toloc;

                    }
                    else
                    {
                        OldToLoc = "NA";
                        ToLoc = "NA";
                    }
                }
                if (objcharge.use_rate_type == "Y")
                    RateType = item.ratetype;
                else
                    RateType = "F";
                if (objcharge.use_trans_type == "Y")
                    TransMode = item.TransMode;
                else
                    TransMode = "NA";
                if (objcharge.use_trans_type == "Y")
                    OldTransMode = item.OldTransType;
                else
                    OldTransMode = "NA";
                MatrixType = objcharge.FilterMatrixtype;

                xmlFRIGHTCCM = xmlFRIGHTCCM + "<CCMChargeMatrix>" + "<ContractID>" + custcontract.CustServiceChrg.contractid + "</ContractID>" +
                     "<BasedOn1>" + item.basedon1 + "</BasedOn1>" +
                     "<BaseCode1>" + item.basecode1 + "</BaseCode1>" +
                     "<BasedOn2>" + item.basedon2 + "</BasedOn2>" +
                     "<BaseCode2>" + item.basecode2 + "</BaseCode2>" +
                     "<ChargeType>" + "BKG" + "</ChargeType>" +
                     "<ChargeCode>" + custcontract.chargeContraint.basedon + "</ChargeCode>" +
                     "<BillLoc>" + item.BillLoc + "</BillLoc>" +
                     "<FromLoc>" + item.FromLoc + "</FromLoc>" +
                     "<OldFromLoc>" + item.OldFromLoc + "</OldFromLoc>" +
                      "<OldToLoc>" + item.OldToLoc + "</OldToLoc>" +
                     "<ToLoc>" + item.toloc + "</ToLoc>" +
                      "<Rate>" + item.rate + "</Rate>" +
                      "<TransMode>" + TransMode + "</TransMode>" +
                     "<OldTransMode>" + OldTransMode + "</OldTransMode>" +
                     "<FlagInsert>" + "Y" + "</FlagInsert>" +
                     "<TrDays>" + item.trdays + "</TrDays>" +
                      "<RateType>" + RateType + "</RateType>" +
                     "<MatrixType>" + objcharge.FilterMatrixtype + "</MatrixType>" +
                      "<FromRange>" + item.FromRange + "</FromRange>" +
                      "<ToRange>" + item.ToRange + "</ToRange>" +
                      "<RateUnits>" + item.RateUnits + "</RateUnits>" +
                      "<ContractType>" + objcharge.ContractType + "</ContractType>" +
                      "<LoopTransMode>" + item.LoopTransMode + "</LoopTransMode>" +
                     "</CCMChargeMatrix>";


            }
            xmlFRIGHT = xmlFRIGHT + "</ArrayOfCCMMatrixCombination>";
            xmlFRIGHTCCM = xmlFRIGHTCCM + "</ArrayOfCCMChargeMatrix>";
            if (objcharge.ContractType == "L")
            {
                DataTable dtcharge = CS.AddFreightChargeChargeMatrix(xmlFRIGHTCCM.Trim(), BaseUserName, "");
            }
            else
            {
                DataTable dt = CS.AddFreightCharge(xmlFRIGHT.Trim());
                if (dt == null || dt.Rows.Count == 0)
                {
                    DataTable dtcharge = CS.AddFreightChargeChargeMatrix(xmlFRIGHTCCM.Trim(), BaseUserName, "");
                }
            }
            return PartialView("_PartialBlank", custcontract);
        }

        public ActionResult GetDyamicCharge(string contractid, string chargecode)
        {
            string chargetype = "BKG";
            webx_custcontract_charge_constraint objCharge = new webx_custcontract_charge_constraint();

            string strsql = "SELECT * FROM dbo.webx_custcontract_charge_constraint WITH(NOLOCK)";
            strsql = strsql + " WHERE contractid='" + contractid + "' AND chargecode='" + chargecode + "' AND chargetype='" + chargetype + "'";
            List<webx_custcontract_charge_constraint> ChargeList = DataRowToObject.CreateListFromTable<webx_custcontract_charge_constraint>(GF.GetDataTableFromSP(strsql));

            objCharge = ChargeList.FirstOrDefault();
            if (objCharge == null)
            {
                objCharge = new webx_custcontract_charge_constraint();
                objCharge.contractid = contractid;
            }
            return PartialView("_Partial_Filter_Charge", objCharge);
        }

        public ActionResult GetStandardCharge(CustomerContractViewModel custcontract, List<webx_custcontract_charge_constraint> ObjChargeContraint1)
        {
            webx_custcontract_charge_constraint ObjChargeContraint = new webx_custcontract_charge_constraint();
            webx_custcontract_charge_constraint ObjChargeContraint12 = new webx_custcontract_charge_constraint();
            ObjChargeContraint = ObjChargeContraint1.First();
            ObjChargeContraint12 = ObjChargeContraint1.First();

            Webx_CustContract_FRTMatrix_SingleSlab ObjSlab = new Webx_CustContract_FRTMatrix_SingleSlab();

            ObjSlab.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");            
            ObjChargeContraint = CS.Getfreightcharge(custcontract.CustServiceChrg.contractid, custcontract.chargeContraint.basedon, "BKG").FirstOrDefault();
            if (ObjChargeContraint != null)
            {
                ObjSlab.basedon2 = ObjChargeContraint.basedon;
            }
            ObjSlab.basecode1 = ObjChargeContraint.basedon;
            ObjSlab.basecode2 = ObjChargeContraint.basedon;
            ObjSlab.chargetype = ObjChargeContraint.chargetype;


            ObjSlab.contractid = ObjChargeContraint12.contractid;
            ObjSlab.FilterFromLoc = ObjChargeContraint12.FilterFromLoc;
            ObjSlab.FilterToLoc = ObjChargeContraint12.FilterToLoc;
            ObjSlab.FilterTrnsMode = ObjChargeContraint12.FilterTrnsMode;
            ObjSlab.FilterMatrixtype = ObjChargeContraint12.FilterMatrixtype;



            List<Webx_CustContract_FRTMatrix_SingleSlab> ListSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            ListSingleSlab = CS.GetSinlgeslab(custcontract.CustServiceChrg.contractid, ObjSlab.basedon1, ObjSlab.basecode1, ObjSlab.basedon2, ObjSlab.basecode2,
                               ObjSlab.chargetype, custcontract.chargeContraint.basedon, ObjChargeContraint12.FilterMatrixtype, ObjChargeContraint12.FilterTrnsMode,
                               ObjChargeContraint12.FilterFromLoc, ObjChargeContraint12.FilterToLoc, ObjChargeContraint12.ContractType);
            int Id = 0;
            foreach (var item in ListSingleSlab)
            {
                Id++;
                item.Id = Id;
                item.FilterMatrixtype = ObjChargeContraint12.FilterMatrixtype;
                item.FilterTrnsMode = ObjChargeContraint12.FilterTrnsMode;
                item.chargecode = ObjChargeContraint12.chargecode;
                item.chargetype = ObjChargeContraint12.chargetype;
                item.FilterFromLoc = ObjChargeContraint12.FilterFromLoc;
                item.FilterToLoc = ObjChargeContraint12.FilterToLoc;
                item.contractid = ObjChargeContraint12.contractid;
                item.OldFromLoc = item.FromLoc;
                item.OldToLoc = item.toloc;
                item.OldTransType = item.trans_type;
                item.basecode1 = ObjSlab.basecode1;
                item.basecode2 = ObjSlab.basecode2;
                item.basedon1 = ObjSlab.basedon1;
                item.basedon2 = ObjSlab.basedon2;
            }

            if (ListSingleSlab.Count == 0)
            {
                ObjSlab.Id = 1;
                ObjSlab.rate = 0;
                ObjSlab.trdays = 0;
                ObjSlab.FilterMatrixtype = ObjChargeContraint12.FilterMatrixtype;
                ObjSlab.FilterTrnsMode = ObjChargeContraint12.FilterTrnsMode;
                ObjSlab.chargecode = ObjChargeContraint12.chargecode;
                ObjSlab.chargetype = ObjChargeContraint12.chargetype;
                ObjSlab.FilterFromLoc = ObjChargeContraint12.FilterFromLoc;
                ObjSlab.FilterToLoc = ObjChargeContraint12.FilterToLoc;
                ObjSlab.contractid = ObjChargeContraint12.contractid;
                ListSingleSlab.Add(ObjSlab);
            }
            TempData["use_trans_type"] = ObjChargeContraint12.use_trans_type;
            TempData["use_to"] = ObjChargeContraint12.use_to;
            TempData["use_from"] = ObjChargeContraint12.use_from;
            TempData["use_rate_type"] = ObjChargeContraint12.use_rate_type;
            TempData["ContractType"] = ObjChargeContraint12.ContractType;



            custcontract.ListsingleSlab = ListSingleSlab;
            return PartialView("_Standrad_PartialStandard_SundrySlab_obj", ListSingleSlab);
        }

        public ActionResult ADDfreightChargeMatrix(int id, string contractid, string chargecode, string MatrixType, string TransMode, string ContractType)
        {
            string chargetype = "BKG";
            Webx_CustContract_FRTMatrix_SingleSlab ObjMatrix = new Webx_CustContract_FRTMatrix_SingleSlab();
            ObjMatrix.contractid = contractid;
            ObjMatrix.FilterMatrixtype = MatrixType;
            ObjMatrix.Id = id;

            ObjMatrix.Id = id;
            ObjMatrix.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");
            ObjMatrix.basedon2 = "NONE";
            ObjMatrix.basecode1 = "NONE";
            ObjMatrix.basecode2 = "NONE";
            ObjMatrix.FilterTrnsMode = TransMode;
            ObjMatrix.ContractType = ContractType;


            webx_custcontract_charge_constraint objCharge = new webx_custcontract_charge_constraint();

            string strsql = "SELECT * FROM dbo.webx_custcontract_charge_constraint WITH(NOLOCK)";
            strsql = strsql + " WHERE contractid='" + contractid + "' AND chargecode='" + chargecode + "' AND chargetype='" + chargetype + "'";
            List<webx_custcontract_charge_constraint> ChargeList = DataRowToObject.CreateListFromTable<webx_custcontract_charge_constraint>(GF.GetDataTableFromSP(strsql));

            objCharge = ChargeList.FirstOrDefault();

            TempData["use_trans_type"] = objCharge.use_trans_type;
            TempData["use_to"] = objCharge.use_to;
            TempData["use_from"] = objCharge.use_from;
            TempData["use_rate_type"] = objCharge.use_rate_type;
            TempData["ContractType"] = ContractType;

            return PartialView("_Standrad_PartialStandard_SundrySlab", ObjMatrix);
        }

        public ActionResult TabClikstandardCharge(CustomerContractViewModel custcontract)
        {

            webx_custcontract_charge_constraint objcustconstratint = new webx_custcontract_charge_constraint();
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListsingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            Webx_CustContract_FRTMatrix_SingleSlab objslab = new Webx_CustContract_FRTMatrix_SingleSlab();
            custcontract.CustsingleSlab = objslab;
            objcustconstratint.use_to = "NA";
            objcustconstratint.use_from = "NA";
            objcustconstratint.use_trans_type = "NA";
            objcustconstratint.use_rate_type = "NA";
            custcontract.CustsingleSlab = CS.getstandardCharge(custcontract.CustServiceChrg.contractid).FirstOrDefault();
            if (custcontract.CustsingleSlab == null)
                custcontract.CustsingleSlab = objslab;
            custcontract.chargeContraint = objcustconstratint;
            custcontract.ListsingleSlab = ListsingleSlab;
            custcontract.Listcharge = CS.GetChargeMatrixCriteria().ToList();

            return PartialView("_StandardCharge", custcontract);
        }

        #endregion

        #region Freight Charge
        public ActionResult GetfreightCharge(CustomerContractViewModel custcontract)
        {
            Webx_CustContract_FRTMatrix_SingleSlab ObjSlab = new Webx_CustContract_FRTMatrix_SingleSlab();
            webx_custcontract_charge_constraint ObjChargeContraint = new webx_custcontract_charge_constraint();

            ObjSlab.basedon1 = custcontract.CustsingleSlab.basedon1;//CS.GetDefaultValue("CHRG_RULE");
            ObjSlab.chargecode = "1";
            ObjChargeContraint = CS.Getfreightcharge(custcontract.CustServiceChrg.contractid, ObjSlab.chargecode, "BKG").FirstOrDefault();
            if (ObjChargeContraint != null)
            {
                ObjSlab.basedon2 = ObjChargeContraint.basedon;
            }
            ObjSlab.basecode1 = custcontract.CustsingleSlab.basecode1;
            ObjSlab.basecode2 = custcontract.CustsingleSlab.basecode2;
            ObjSlab.chargetype = "BKG";

            ObjSlab.contractid = custcontract.CustServiceChrg.contractid;
            ObjSlab.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
            ObjSlab.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
            ObjSlab.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
            ObjSlab.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype;
            ObjSlab.ContractType = custcontract.CustsingleSlab.ContractType;


            ObjSlab.Flag = "Add";
            // Get webx_custcontract_charge_constraint Value hdn

            ObjChargeContraint = CS.Getfreightcharge(custcontract.CustServiceChrg.contractid, ObjSlab.chargecode, ObjSlab.chargetype).FirstOrDefault();

            List<Webx_CustContract_FRTMatrix_SingleSlab> ListSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            ListSingleSlab = CS.GetSinlgeslab(custcontract.CustServiceChrg.contractid, ObjSlab.basedon1, ObjSlab.basecode1, ObjSlab.basedon2, ObjSlab.basecode2,
                            ObjSlab.chargetype, ObjSlab.chargecode, custcontract.CustsingleSlab.FilterMatrixtype, custcontract.CustsingleSlab.FilterTrnsMode,
                            custcontract.CustsingleSlab.FilterFromLoc, custcontract.CustsingleSlab.FilterToLoc, custcontract.CustsingleSlab.ContractType);
            int Id = 0;
            foreach (var item in ListSingleSlab)
            {
                Id++;
                item.Id = Id;
                item.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype;
                item.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
                item.chargecode = custcontract.CustsingleSlab.chargecode;
                item.chargetype = custcontract.CustsingleSlab.chargetype;
                item.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
                item.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
                item.OldFromLoc = item.FromLoc;
                item.OldToLoc = item.toloc;
                item.OldTransType = item.trans_type;
                item.contractid = custcontract.CustServiceChrg.contractid;
                item.ContractType = custcontract.CustsingleSlab.ContractType;
            }

            if (ListSingleSlab.Count == 0)
            {
                ObjSlab.Id = 1;
                ObjSlab.rate = 0;
                ObjSlab.trdays = 0;
                ObjSlab.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype;
                ObjSlab.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
                ObjSlab.chargecode = custcontract.CustsingleSlab.chargecode;
                ObjSlab.chargetype = custcontract.CustsingleSlab.chargetype;
                ObjSlab.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
                ObjSlab.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
                ObjSlab.contractid = custcontract.CustServiceChrg.contractid;
                ObjSlab.ContractType = custcontract.CustsingleSlab.ContractType;
                ListSingleSlab.Add(ObjSlab);
            }

            custcontract.ListsingleSlab = ListSingleSlab;
            custcontract.CustsingleSlab = ObjSlab;
            custcontract.chargeContraint = ObjChargeContraint;
            return PartialView("_FreightCharge", custcontract);
        }

        public ActionResult AddfreightCharge(CustomerContractViewModel custcontract, List<Webx_CustContract_FRTMatrix_SingleSlab> FreightCharge)
        {
            try
            {
                string RateType, TransMode, From, To, OldTransMode, MatrixType = "", FromLoc, OldFromLoc, ToLoc, OldToLoc;
                string xmlFRIGHT = "";
                string xmlFRIGHTCCM = "";


                if (FreightCharge != null)
                {

                    xmlFRIGHT = "<ArrayOfCCMMatrixCombination>";
                    xmlFRIGHTCCM = "<ArrayOfCCMChargeMatrix>";
                    foreach (var item in FreightCharge)
                    {

                        //First XML
                        xmlFRIGHT = xmlFRIGHT + "<CCMMatrixCombination>" + " <ContractID>" + item.contractid + "</ContractID>" +
                                                "<FromLoc>" + item.FromLoc + "</FromLoc>" +
                                                "<ToLoc>" + item.toloc + "</ToLoc>" +
                                                "<MatrixType>" + MatrixType + "</MatrixType>" +
                                                 "<FlagInsert>" + "Y" + "</FlagInsert>" +
                                                 "<BillLoc>" + item.BillLoc + "</BillLoc>" +
                                                 "<Transmode>" + item.trans_type + "</Transmode>" + "</CCMMatrixCombination>";


                        xmlFRIGHTCCM = xmlFRIGHTCCM + "<CCMChargeMatrix>" + "<ContractID>" + custcontract.CustServiceChrg.contractid + "</ContractID>" +
                            "<BasedOn1>" + custcontract.CustsingleSlab.basedon1 + "</BasedOn1>" +
                            "<BaseCode1>" + custcontract.CustsingleSlab.basecode1 + "</BaseCode1>" +
                            "<BasedOn2>" + custcontract.CustsingleSlab.basedon2 + "</BasedOn2>" +
                            "<BaseCode2>" + custcontract.CustsingleSlab.basecode2 + "</BaseCode2>" +
                            "<ChargeType>" + "BKG" + "</ChargeType>" +
                            "<ChargeCode>" + "1" + "</ChargeCode>" +
                            "<BillLoc>" + custcontract.CustsingleSlab.BillLoc + "</BillLoc>" +
                            "<FromLoc>" + item.FromLoc + "</FromLoc>" +
                            "<OldFromLoc>" + item.OldFromLoc + "</OldFromLoc>" +
                             "<OldToLoc>" + item.OldToLoc + "</OldToLoc>" +
                            "<ToLoc>" + item.toloc + "</ToLoc>" +
                             "<Rate>" + item.rate + "</Rate>" +
                             "<TransMode>" + item.trans_type + "</TransMode>" +
                            "<OldTransMode>" + item.OldTransType + "</OldTransMode>" +
                            "<FlagInsert>" + "Y" + "</FlagInsert>" +
                            "<TrDays>" + item.trdays + "</TrDays>" +
                             "<RateType>" + item.ratetype + "</RateType>" +
                             "<isNewZoneContractApplied>" + custcontract.CustsingleSlab.isNewZoneContractApplied + "</isNewZoneContractApplied>" +
                            "<MatrixType>" + item.FilterMatrixtype + "</MatrixType>" + "<srno>" + item.srno + "</srno>" +
                             "<FromRange>" + item.FromRange + "</FromRange>" +
                             "<ToRange>" + item.ToRange + "</ToRange>" +
                             "<RateUnits>" + item.RateUnits + "</RateUnits>" +
                             "<LoopTransMode>" + item.LoopTransMode + "</LoopTransMode>" +
                             "<ContractType>" + custcontract.CustsingleSlab.ContractType + "</ContractType>" +
                            "</CCMChargeMatrix>";

                        if (custcontract.chargeContraint.use_from != "Y")
                            item.FromLoc = "NA"; item.Matrixtype = "O";

                        if (custcontract.chargeContraint.use_to != "Y")
                            item.toloc = "NA"; item.Matrixtype = "D";

                        if (custcontract.chargeContraint.use_from != "Y" && custcontract.chargeContraint.use_to != "Y")
                            MatrixType = "NA";

                        if (custcontract.chargeContraint.use_rate_type == "Y")
                            RateType = item.ratetype;
                        else
                            RateType = "F";

                        if (custcontract.chargeContraint.use_trans_type == "Y")
                            TransMode = item.trans_type;
                        else
                            TransMode = "NA";


                        if (custcontract.chargeContraint.use_from == "Y")
                        {
                            if (custcontract.CustsingleSlab.Matrixtype == "R")

                                From = item.FromLoc;
                        }
                        else
                            From = "NA";

                        if (custcontract.chargeContraint.use_to == "Y")
                        {
                            if (custcontract.CustsingleSlab.Matrixtype == "R")

                                To = item.toloc;
                        }
                        else
                            To = "NA";
                        if (MatrixType == "")
                            MatrixType = custcontract.CustsingleSlab.Matrixtype;
                        //second XML
                        //*** if (hdnMatrixType.Value == "R") ***

                        if (custcontract.CustsingleSlab.Matrixtype == "R")
                        {
                            FromLoc = ReturnZoneCode(item.FromLoc);
                            OldFromLoc = ReturnZoneCode(custcontract.CustsingleSlab.FromLoc);
                            ToLoc = ReturnZoneCode(item.FromLoc);
                            OldToLoc = ReturnZoneCode(custcontract.CustsingleSlab.toloc);
                        }
                        else
                        {
                            if (custcontract.chargeContraint.use_from == "Y")
                            {
                                OldFromLoc = custcontract.CustsingleSlab.FromLoc;
                                FromLoc = item.FromLoc;
                            }
                            else
                            {
                                FromLoc = "NA";
                                OldFromLoc = "NA";
                            }

                            if (custcontract.chargeContraint.use_to == "Y")
                            {
                                OldToLoc = custcontract.CustsingleSlab.toloc;
                                ToLoc = item.toloc;

                            }
                            else
                            {
                                OldToLoc = "NA";
                                ToLoc = "NA";
                            }
                        }
                        if (custcontract.CustsingleSlab.ratetype == "Y")
                            RateType = item.ratetype;
                        else
                            RateType = "F";
                        if (custcontract.CustsingleSlab.trans_type == "Y")
                            TransMode = item.trans_type;
                        else
                            TransMode = "NA";
                        if (custcontract.CustsingleSlab.trans_type == "Y")
                            OldTransMode = custcontract.CustsingleSlab.trans_type;
                        else
                            OldTransMode = "NA";
                        MatrixType = custcontract.CustsingleSlab.Matrixtype;
                    }
                    xmlFRIGHT = xmlFRIGHT + "</ArrayOfCCMMatrixCombination>";
                    xmlFRIGHTCCM = xmlFRIGHTCCM + "</ArrayOfCCMChargeMatrix>";
                    DataTable dt = CS.AddFreightCharge(xmlFRIGHT.Trim());
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        DataTable dtcharge = CS.AddFreightChargeChargeMatrix(xmlFRIGHTCCM.Trim(), BaseUserName, "");
                    }
                }


                //return PartialView("_PartialBlank", custcontract);
                return RedirectToAction("ContractDone", new { ContID = custcontract.CustServiceChrg.contractid, CustCode = custcontract.CustomerContract.Custcode, Type = "" });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        private string ReturnZoneCode(string ZoneName)
        {

            for (int i = 0; i < ZoneCount; i++)
            {
                if (ZoneName.ToUpper().CompareTo(arrzonename[i].ToString().ToUpper()) == 0)
                {
                    return arrzonecode[i].ToString();
                }
            }
            return "";
        }

        public ActionResult ADDfreightMatrix(int id, string TrnsMode, string chargecode, string Matrixtype, string chargetype, string FromLoc, string ToLoc, string ContractId, string ContractType)
        {
            Webx_CustContract_FRTMatrix_SingleSlab ObjMatrix = new Webx_CustContract_FRTMatrix_SingleSlab();
            ObjMatrix.FilterMatrixtype = Matrixtype;
            ObjMatrix.FilterTrnsMode = TrnsMode;
            ObjMatrix.chargecode = chargecode;
            ObjMatrix.chargetype = chargetype;
            ObjMatrix.FilterFromLoc = FromLoc;
            ObjMatrix.FilterToLoc = ToLoc;
            ObjMatrix.Id = id;
            ObjMatrix.contractid = ContractId;
            ObjMatrix.trans_type = TrnsMode;
            ObjMatrix.ContractType = ContractType;
            return PartialView("_PartialFreightCharge", ObjMatrix);
        }

        public ActionResult ADDSlabfreightMatrix(int id, string Matrixtype, string TransMode, string fromloc, string toloc)
        {
            Webx_CustContract_FRTMatrix_SingleSlab ObjMatrix = new Webx_CustContract_FRTMatrix_SingleSlab();
            ObjMatrix.FilterFromLoc = fromloc;
            ObjMatrix.FilterToLoc = toloc;
            ObjMatrix.FilterTrnsMode = TransMode;
            ObjMatrix.FilterMatrixtype = Matrixtype;
            ObjMatrix.Id = id;
            ObjMatrix.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");
            ObjMatrix.basedon2 = "NONE";
            ObjMatrix.basecode1 = "NONE";
            ObjMatrix.basecode2 = "NONE";

            return PartialView("_PartialFreightCharge_MultiSlab", ObjMatrix);
        }

        public JsonResult GetlocationListJson(string searchTerm)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();
            ListLocations = CS.Getlocationlist(searchTerm).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.loccode,
                                  text = e.locaname,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete_Single_Slab_Rates(int ID)
        {
            string IsRecordFound = "0";
            DataTable DT = new DataTable();
            try
            {
                DT = CS.Delete_Single_Slab_Rates(ID, BaseLocationCode, BaseCompanyCode, BaseUserName);
                IsRecordFound = "1";
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        #endregion

        #region Slab
        public ActionResult GetfreightSlab(CustomerContractViewModel custcontract)
        {
            Webx_CustContract_FRTMatrix_SingleSlab ObjSlab = new Webx_CustContract_FRTMatrix_SingleSlab();
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListFRTMatrixSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            List<webx_custcontract_frtmatrix_slabhdr> Listslabhdr = new List<webx_custcontract_frtmatrix_slabhdr>();
            webx_custcontract_frtmatrix_slabhdr Custslabhdr = new webx_custcontract_frtmatrix_slabhdr();
            if (custcontract.CustsingleSlab.InsertType == "MultiPleSlab")
            {
                if (custcontract.CustsingleSlab.TransMode == null)
                {
                    Listslabhdr = CS.GetMultiSlabList(custcontract.CustServiceChrg.contractid, "1").ToList();
                }
                else
                {
                    Listslabhdr = CS.GetMultiSlabList(custcontract.CustServiceChrg.contractid, custcontract.CustsingleSlab.TransMode).ToList();
                }
                foreach (var item in Listslabhdr)
                {
                    if (item.slab_code == "SLAB1")
                    {
                        Custslabhdr.Slab1Type = item.rate_type;
                        Custslabhdr.Slab1To = item.slab_to;
                        Custslabhdr.Slab1From = item.slab_from;
                        Custslabhdr.slab_code1 = item.slab_code;
                    }
                    if (item.slab_code == "SLAB2")
                    {
                        Custslabhdr.Slab2Type = item.rate_type;
                        Custslabhdr.Slab2To = item.slab_to;
                        Custslabhdr.Slab2From = item.slab_from;
                        Custslabhdr.slab_code2 = item.slab_code;
                    }
                    if (item.slab_code == "SLAB3")
                    {
                        Custslabhdr.Slab3Type = item.rate_type;
                        Custslabhdr.Slab3To = item.slab_to;
                        Custslabhdr.Slab3From = item.slab_from;
                        Custslabhdr.slab_code3 = item.slab_code;
                    }
                    if (item.slab_code == "SLAB4")
                    {
                        Custslabhdr.Slab4Type = item.rate_type;
                        Custslabhdr.Slab4To = item.slab_to;
                        Custslabhdr.Slab4From = item.slab_from;
                        Custslabhdr.slab_code4 = item.slab_code;
                    }
                    if (item.slab_code == "SLAB5")
                    {
                        Custslabhdr.Slab5Type = item.rate_type;
                        Custslabhdr.Slab5To = item.slab_to;
                        Custslabhdr.Slab5From = item.slab_from;
                        Custslabhdr.slab_code5 = item.slab_code;
                    }
                    if (item.slab_code == "SLAB6")
                    {
                        Custslabhdr.Slab6Type = item.rate_type;
                        Custslabhdr.Slab6To = item.slab_to;
                        Custslabhdr.Slab6From = item.slab_from;
                        Custslabhdr.slab_code6 = item.slab_code;
                    }
                }
            }
            else
            {
                ObjSlab.contractid = custcontract.CustServiceChrg.contractid;
                ObjSlab.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
                ObjSlab.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
                ObjSlab.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
                ObjSlab.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype;
                ObjSlab.basecode1 = "NONE";
                ObjSlab.basecode2 = "NONE";
                ObjSlab.basedon1 = "NONE";
                ObjSlab.basedon2 = "NONE";
                ObjSlab.chargetype = "BKG";

                // Get webx_custcontract_charge_constraint Value hdn                           
                ListFRTMatrixSingleSlab = CS.GetFreightlab(custcontract.CustServiceChrg.contractid, ObjSlab.basedon1, ObjSlab.basecode1, ObjSlab.basedon2, ObjSlab.basecode2, custcontract.CustsingleSlab.FilterMatrixtype, custcontract.CustsingleSlab.FilterTrnsMode, custcontract.CustsingleSlab.FilterFromLoc, custcontract.CustsingleSlab.FilterToLoc);
                int Id = 0;
                foreach (var item in ListFRTMatrixSingleSlab)
                {
                    Id++;
                    item.Id = Id;
                    item.contractid = custcontract.CustServiceChrg.contractid;
                    item.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
                    item.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
                    item.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
                    item.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype;
                    item.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");
                    item.basedon2 = "NONE";
                    item.basecode1 = "NONE";
                    item.basecode2 = "NONE";
                    item.OldFromLoc = item.FromLoc;
                    item.OldToLoc = item.toloc;
                }
                if (ListFRTMatrixSingleSlab.Count == 0)
                {
                    Webx_CustContract_FRTMatrix_SingleSlab ObjMatrix = new Webx_CustContract_FRTMatrix_SingleSlab();
                    ObjMatrix.FilterFromLoc = custcontract.CustsingleSlab.FilterFromLoc;
                    ObjMatrix.FilterToLoc = custcontract.CustsingleSlab.FilterToLoc;
                    ObjMatrix.FilterTrnsMode = custcontract.CustsingleSlab.FilterTrnsMode;
                    ObjMatrix.FilterMatrixtype = custcontract.CustsingleSlab.FilterMatrixtype; ;
                    ObjMatrix.Id = 1;
                    ObjMatrix.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");
                    ObjMatrix.basedon2 = "NONE";
                    ObjMatrix.basecode1 = "NONE";
                    ObjMatrix.basecode2 = "NONE";
                    ListFRTMatrixSingleSlab.Add(ObjMatrix);
                }

            }
            custcontract.Custslabhdr = Custslabhdr;
            custcontract.CustServiceChrg = custcontract.CustServiceChrg;
            custcontract.ListsingleSlab = ListFRTMatrixSingleSlab;
            custcontract.CustsingleSlab = ObjSlab;
            return PartialView("_FreightCharge_MultiSlab", custcontract);
        }
        [HttpPost]
        public ActionResult AddfreightSlab(CustomerContractViewModel custcontract, List<Webx_CustContract_FRTMatrix_SingleSlab> SlabList, List<webx_custcontract_frtmatrix_slabhdr> SingleSlabList)
        {
            if (custcontract.CustsingleSlab.InsertType == "MultiPleSlab")
            {
                string strsql = "";
                strsql = "DELETE FROM webx_custcontract_frtmatrix_slabhdr WHERE contractid='" + custcontract.CustServiceChrg.contractid + "' AND trans_type='" + custcontract.CustsingleSlab.SlabTransMode + "'";
                DataTable dt = CS.DeleteContractfrtmatrix_slabhdr(strsql);
                foreach (var item in SingleSlabList)
                {
                    DataTable dt0 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab1From, item.Slab1To, item.Slab1Type, "SLAB1", custcontract.CustsingleSlab.SlabTransMode);
                    DataTable dt1 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab2From, item.Slab2To, item.Slab2Type, "SLAB2", custcontract.CustsingleSlab.SlabTransMode);
                    DataTable dt2 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab3From, item.Slab3To, item.Slab3Type, "SLAB3", custcontract.CustsingleSlab.SlabTransMode);
                    DataTable dt3 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab4From, item.Slab4To, item.Slab4Type, "SLAB4", custcontract.CustsingleSlab.SlabTransMode);
                    DataTable dt4 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab5From, item.Slab5To, item.Slab5Type, "SLAB5", custcontract.CustsingleSlab.SlabTransMode);
                    DataTable dt5 = CS.InsertContractfrtmatrix_slabhdr(custcontract.CustServiceChrg.contractid, item.Slab6From, item.Slab6To, item.Slab6Type, "SLAB6", custcontract.CustsingleSlab.SlabTransMode);

                }
            }
            else
            {
                string strXMLMultiSlab = "<root>";
                foreach (var item in SlabList)
                {
                    string strpart1 = "<record>";
                    strpart1 = strpart1 + "<contractid>" + custcontract.CustServiceChrg.contractid + "</contractid>" + "<basedon1>" + item.basedon1 + "</basedon1>"
                        + "<basecode1>" + item.basecode1 + "</basecode1>"
                        + "<basedon2>" + item.basedon2 + "</basedon2>" + "<basecode2>" + item.basecode2 + "</basecode2>"
                    + "<isNewZoneContractApplied>" + custcontract.CustsingleSlab.isNewZoneContractApplied + "</isNewZoneContractApplied>";

                    if (custcontract.CustServiceChrg.contractid.CompareTo("R") == 0)
                    {
                        strpart1 = strpart1 + "<from_loccode>" + (item.FromLoc) + "</from_loccode>"
                            + "<to_loccode>" + (item.toloc) + "</to_loccode>";
                        strpart1 = strpart1 + "<oldfrom_loccode>" + item.OldFromLoc + "</oldfrom_loccode>"
                         + "<oldto_loccode>" + item.OldToLoc + "</oldto_loccode>";
                    }
                    else
                    {
                        strpart1 = strpart1 + "<from_loccode>" + item.FromLoc + "</from_loccode>" + "<to_loccode>" + item.toloc + "</to_loccode>";
                        strpart1 = strpart1 + "<oldfrom_loccode>" + item.OldFromLoc + "</oldfrom_loccode>" + "<oldto_loccode>" + item.OldToLoc + "</oldto_loccode>";
                    }


                    strpart1 = strpart1 + "<trans_type>" + item.FilterTrnsMode + "</trans_type>"
                        + "<loc_reg>" + item.FilterMatrixtype + "</loc_reg>" + "<trdays>" + item.trdays + "</trdays>"
                        + "<slab1>" + item.Slab1 + "</slab1>" + "<slab2>" + item.Slab2 + "</slab2>"
                        + "<slab3>" + item.Slab3 + "</slab3>" + "<slab4>" + item.Slab4 + "</slab4>"
                        + "<slab5>" + item.Slab5 + "</slab5>" + "<slab6>" + item.Slab6 + "</slab6>"
                        + "<flaginsert>" + "Y" + "</flaginsert>";
                    strpart1 = strpart1 + "</record>";
                    strXMLMultiSlab = strXMLMultiSlab + strpart1;
                }
                strXMLMultiSlab = strXMLMultiSlab + "</root>";
                DataTable Dt = CS.USP_INSERT_CUSTCONTRACT_SUNDRY_MULTISLAB_DETAILS(strXMLMultiSlab.Trim());
            }
            return PartialView("_PartialBlank", custcontract);
        }
        #endregion

        #region Bind SlabValues
        public ActionResult BindSlabValues(string contarctId, string TypeMode)
        {
            webx_custcontract_frtmatrix_slabhdr objslabhdr = new webx_custcontract_frtmatrix_slabhdr();
            List<webx_custcontract_frtmatrix_slabhdr> SlabhdrList = new List<webx_custcontract_frtmatrix_slabhdr>();
            SlabhdrList = CS.GetMultiSlabList(contarctId, TypeMode).ToList();
            foreach (var item in SlabhdrList)
            {
                if (item.slab_code == "SLAB1")
                {
                    objslabhdr.Slab1From = item.slab_from;
                    objslabhdr.Slab1To = item.slab_to;
                    objslabhdr.Slab1Type = item.rate_type;
                }
                if (item.slab_code == "SLAB2")
                {
                    objslabhdr.Slab2From = item.slab_from;
                    objslabhdr.Slab2To = item.slab_to;
                    objslabhdr.Slab2Type = item.rate_type;
                }
                if (item.slab_code == "SLAB3")
                {
                    objslabhdr.Slab3From = item.slab_from;
                    objslabhdr.Slab3To = item.slab_to;
                    objslabhdr.Slab3Type = item.rate_type;
                }
                if (item.slab_code == "SLAB4")
                {
                    objslabhdr.Slab4From = item.slab_from;
                    objslabhdr.Slab4To = item.slab_to;
                    objslabhdr.Slab4Type = item.rate_type;
                }
                if (item.slab_code == "SLAB5")
                {
                    objslabhdr.Slab5From = item.slab_from;
                    objslabhdr.Slab5To = item.slab_to;
                    objslabhdr.Slab5Type = item.rate_type;
                }
                if (item.slab_code == "SLAB6")
                {
                    objslabhdr.Slab6From = item.slab_from;
                    objslabhdr.Slab6To = item.slab_to;
                    objslabhdr.Slab6Type = item.rate_type;

                }
            }
            return PartialView("_PartialFreightCharge_MultiSlab_Single", objslabhdr);
        }
        #endregion

        #region Customer Chnage Status
        public ActionResult Changestatus(string CostCode, string Flag, string ContractID)
        {
            DataTable dt = CS.CustomerChnageStatus(CostCode, ContractID, Flag, BaseUserName);
            List<webx_custcontract_hdr> CusthdrList = new List<webx_custcontract_hdr>();
            string Name = "";
            if (CostCode != "")
                Name = CostCode;

            CusthdrList = CS.GetCustContList(Name);
            var Id = 0;
            foreach (var item in CusthdrList)
            {
                Id++;
                item.Srno = Id;
            }
            return PartialView("_Partial_CustContract", CusthdrList);

        }
        #endregion

        #region Tab Freight Charge
        public ActionResult TabFreightCharge(CustomerContractViewModel custcontract, string ContractId)
        {
            string ReditectView = "";
            string SlabType = "";
            bool isNewZoneContractApplied = false;
            DataTable dt = CS.GetSundrySlabType(ContractId);
            DataTable dts = CS.Getservicecharge(ContractId);
            if (dt.Rows.Count > 0)
            {
                SlabType = dt.Rows[0]["slab_type"].ToString();
            }
            if (dts.Rows.Count > 0)
            {
                isNewZoneContractApplied = Convert.ToBoolean(dts.Rows[0]["isNewZoneContractApplied"]);
            }

            Webx_CustContract_FRTMatrix_SingleSlab objSingleSlab = new Webx_CustContract_FRTMatrix_SingleSlab();
            webx_custcontract_frtmatrix_slabhdr Objslabhdr = new webx_custcontract_frtmatrix_slabhdr();
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            webx_custcontract_servicecharges objservicecharges = new webx_custcontract_servicecharges();
            webx_custcontract_charge_constraint objChargeConstraint = new webx_custcontract_charge_constraint();

            objSingleSlab.chargecode = "1";
            objSingleSlab.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");

            objChargeConstraint = CS.Getfreightcharge(ContractId, objSingleSlab.chargecode, "BKG").FirstOrDefault();
            custcontract.chargeContraint = objChargeConstraint;
            if (objChargeConstraint != null)
                objSingleSlab.basedon2 = objChargeConstraint.basedon;
            objSingleSlab.basecode1 = "NONE";
            objSingleSlab.basecode2 = "NONE";
            objservicecharges.contractid = ContractId;
            objSingleSlab.isNewZoneContractApplied = isNewZoneContractApplied;
            custcontract.CustServiceChrg = objservicecharges;
            custcontract.CustsingleSlab = objSingleSlab;
            custcontract.ListsingleSlab = ListSingleSlab;
            custcontract.Custslabhdr = Objslabhdr;
            if (SlabType == "D")
            {
                custcontract.CustsingleSlab.Flag = "Get";
                ReditectView = "_FreightCharge_MultiSlab";
            }
            else
                ReditectView = "_FreightCharge";
            return PartialView(ReditectView, custcontract);
        }

        public JsonResult Getwebx_CUSTHDR(string searchTerm)
        {
            List<webx_CUSTHDR> ListCustHdr = new List<webx_CUSTHDR>();
            ListCustHdr = MS.GetCustomerMasterObject().Where(c => c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CUSTNM).ToList();

            var SearchList = (from e in ListCustHdr
                              select new
                              {
                                  id = e.CUSTCD,
                                  text = e.CUSTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Freight Charge -FTL
        public ActionResult SundryChargeFTL(string ContractId, string CustCode)
        {
            bool isNewZoneContractApplied = false;
            DataTable dts = CS.Getservicecharge(ContractId);
            if (dts.Rows.Count > 0)
            {
                isNewZoneContractApplied = Convert.ToBoolean(dts.Rows[0]["isNewZoneContractApplied"]);
            }
            CustomerContractViewModel custcontract = new CustomerContractViewModel();
            webx_custcontract_servicecharges objservice = new webx_custcontract_servicecharges();
            Webx_CustContract_FRTMatrix_SingleSlab ObjSlab = new Webx_CustContract_FRTMatrix_SingleSlab();
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListFRTMatrixSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            objservice.custcode = CustCode;
            objservice.contractid = ContractId;
            custcontract.CustServiceChrg = objservice;
            custcontract.ListsingleSlab = ListFRTMatrixSingleSlab;
            custcontract.CustsingleSlab = ObjSlab;
            ObjSlab.isNewZoneContractApplied = isNewZoneContractApplied;
            return PartialView("_SundryChargeFTL", custcontract);
        }
        public ActionResult GetSundryFTL(CustomerContractViewModel con)
        {
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListFRTMatrixSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            ListFRTMatrixSingleSlab = CS.ListSundryFTL(con.CustsingleSlab.FilterMatrixtype, con.CustsingleSlab.FTL, con.CustsingleSlab.contractid, con.CustsingleSlab.FilterTrnsMode, con.CustsingleSlab.FilterFromLoc, con.CustsingleSlab.FilterToLoc);
            Webx_CustContract_FRTMatrix_SingleSlab objSlab = new Webx_CustContract_FRTMatrix_SingleSlab();
            if (ListFRTMatrixSingleSlab.Count == 0)
            {
                objSlab.Id = 1;
                objSlab.FilterMatrixtype = con.CustsingleSlab.FilterMatrixtype;
                objSlab.contractid = con.CustsingleSlab.contractid;

                ListFRTMatrixSingleSlab.Add(objSlab);

            }
            else
            {
                int AutoId = 0;
                foreach (var item in ListFRTMatrixSingleSlab)
                {
                    AutoId++;
                    item.Id = AutoId;
                    item.FilterMatrixtype = con.CustsingleSlab.FilterMatrixtype;
                }

            }
            return PartialView("_PartialSundry_FTL", ListFRTMatrixSingleSlab);
        }
        public ActionResult ADDFTL(int id, string Matrixtype, string TransMode, string fromloc, string toloc, string ContractID, string FTL)
        {
            Webx_CustContract_FRTMatrix_SingleSlab ObjMatrix = new Webx_CustContract_FRTMatrix_SingleSlab();
            ObjMatrix.FilterFromLoc = fromloc;
            ObjMatrix.FilterToLoc = toloc;
            ObjMatrix.FilterTrnsMode = TransMode;
            ObjMatrix.FilterMatrixtype = Matrixtype;
            ObjMatrix.Id = id;
            ObjMatrix.basedon1 = "NONE";//CS.GetDefaultValue("CHRG_RULE");
            ObjMatrix.basedon2 = "NONE";
            ObjMatrix.basecode1 = "NONE";
            ObjMatrix.basecode2 = "NONE";
            ObjMatrix.contractid = ContractID;
            ObjMatrix.OldFTLType = FTL;
            return PartialView("_PartialSundryFlat", ObjMatrix);
        }
        public ActionResult AddSundryFTL(CustomerContractViewModel con, List<Webx_CustContract_FRTMatrix_SingleSlab> FTLList)
        {
            if (FTLList.Count > 0)
            {
                if (FTLList != null)
                {
                    foreach (var item in FTLList)
                    {
                        item.isNewZoneContractApplied = con.CustsingleSlab.isNewZoneContractApplied;
                    }
                }

                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(FTLList.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, FTLList);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                DataTable dt = CS.InsertSundryFTL(xmlDoc.InnerXml, BaseUserName.ToUpper());
            }
            List<Webx_CustContract_FRTMatrix_SingleSlab> ListFRTMatrixSingleSlab = new List<Webx_CustContract_FRTMatrix_SingleSlab>();
            return PartialView("_PartialSundry_FTL", ListFRTMatrixSingleSlab);
        }
        #endregion

        #region ODA Charge
        public ActionResult ODACharge(string ContractId)
        {
            CustomerContractViewModel ObjContract = new CustomerContractViewModel();
            webx_custcontract_frtmatrix_slabhdr objslabhdr = new webx_custcontract_frtmatrix_slabhdr();
            Webx_CustContract_FRTMatrix_SingleSlab objslab = new Webx_CustContract_FRTMatrix_SingleSlab();
            ObjContract.CustsingleSlab = objslab;
            objslabhdr.ContractId = ContractId;
            ObjContract.Custslabhdr = objslabhdr;
            //Slabtype
            webx_custcontract_frtmatrix_slabhdr objslabtype = CS.GetSLABHDRList(ContractId).FirstOrDefault();
            if (objslabtype != null)
                ObjContract.CustsingleSlab.Slabtype = objslabtype.matrix_type;

            List<webx_custcontract_frtmatrix_slabhdr> Listslabhdr = CS.GetSLABHDRList(ContractId).ToList();
            ObjContract.Listslabhdr = Listslabhdr;

            //***********
            //
            webx_custcontract_frtmatrix_slabhdr objRatetype = CS.GetDistHdrList(ContractId).FirstOrDefault();
            if (objRatetype != null)
            {
                ObjContract.CustsingleSlab.ratetype = objRatetype.matrix_type;
                ObjContract.CustsingleSlab.ODACharge = objRatetype.MIN_ODACHRG;
            }

            List<webx_custcontract_frtmatrix_slabhdr> ListslaODAbhdr = CS.GetDistHdrList(ContractId).ToList();
            ObjContract.ListslaODAbhdr = ListslaODAbhdr;
            //**************     
            List<webx_custcontract_frtmatrix_slabhdr> ListMatrixCharge = CS.GetMatrixChargeList(ContractId).ToList();
            ObjContract.ListMatrixCharge = ListMatrixCharge;

            return PartialView("_ODACharge", ObjContract);
        }

        public ActionResult AddODACharge(CustomerContractViewModel ObjContract, FormCollection frm)
        {
            //Define Weight/Package Slab
            DataTable dt = CS.DeleteSlabHDR(ObjContract.Custslabhdr.ContractId);

            for (int i = 1; i < 7; i++)
            {
                string slab = "SLAB" + i;
                string Slabfrom = "Slabfrom" + i;
                string Slabto = "Slabto" + i;
                Slabfrom = frm[Slabfrom];
                Slabto = frm[Slabto];
                dt = CS.InsertSlabHDR(ObjContract.Custslabhdr.ContractId, ObjContract.CustsingleSlab.Slabtype, slab, Slabfrom, Slabto);
            }
            //Define ODA Charges Distance (ODA Charges in Km)

            dt = CS.DeleteODASlabHDR(ObjContract.Custslabhdr.ContractId);
            for (int i = 1; i < 7; i++)
            {
                string slab = "ODA" + i;
                string SlabODAfrom = "SlabODAfrom" + i;
                string SlabODAto = "SlabODAto" + i;
                SlabODAfrom = frm[SlabODAfrom];
                SlabODAto = frm[SlabODAto];
                dt = CS.InsertODASlabHDR(ObjContract.Custslabhdr.ContractId, ObjContract.CustsingleSlab.Slabtype, ObjContract.CustsingleSlab.ODACharge, slab, SlabODAfrom, SlabODAto);
            }
            //Define ODA Charges Charges
            dt = CS.DeleteESSCodeSlabHDR(ObjContract.Custslabhdr.ContractId);
            for (int i = 1; i < 7; i++)
            {
                string slab = "ODA" + i;
                string Days = "Days" + i;
                string SlabA = "SlabA" + i;
                string SlabB = "SlabB" + i;
                string SlabC = "SlabC" + i;
                string SlabD = "SlabD" + i;
                string SlabE = "SlabE" + i;
                string SlabF = "SlabF" + i;
                Days = frm[Days];
                SlabA = frm[SlabA];
                SlabB = frm[SlabB];
                SlabC = frm[SlabC];
                SlabD = frm[SlabD];
                SlabE = frm[SlabE];
                SlabF = frm[SlabF];

                dt = CS.InsertESSCodeSlabHDR(ObjContract.Custslabhdr.ContractId, slab, Days == "" ? "0.00" : Days, SlabA == "" ? "0.00" : SlabA, SlabB == "" ? "0.00" : SlabB, SlabC == "" ? "0.00" : SlabC, SlabD == "" ? "0.00" : SlabD, SlabE == "" ? "0.00" : SlabE, SlabF == "" ? "0.00" : SlabF);
            }

            return PartialView("_PartialBlank", ObjContract);
        }

        #endregion

        #region ContractDone
        public ActionResult ContractDone(string ContID, string CustCode, string Type)
        {
            DataTable dt = CS.GetCustContName(CustCode, ContID);

            ViewBag.ContaractID = ContID;
            ViewBag.ContarctName = dt.Rows[0]["custcode"].ToString() + " : " + dt.Rows[0]["name"].ToString(); ;
            ViewBag.ConType = dt.Rows[0]["contracttype"].ToString();
            ViewBag.CustCode = CustCode;
            return View("ContractDone");
        }
        #endregion

        #region Customer contract zone location transmode wise master

        public ActionResult Custcon_zonelocationtransmodewisemaster()
        {
            CustomerContractViewModel ObjContract = new CustomerContractViewModel();
            return View(ObjContract);
        }

        public ActionResult Custcon_zonelocationtransmodewisemasterlist(CustomerContractViewModel ObjContract)
        {
            try
            {
                List<Cygnus_Custcontract_locaton_zone_transmode_wise_master> ObjContractList = new List<Cygnus_Custcontract_locaton_zone_transmode_wise_master>();
                ObjContractList = CS.GetCustconzonelocationtransmodewisemaster(ObjContract.ObjCCLZTWM.Loccode);
                ObjContract.ListCCLZTWM = ObjContractList;
                return View(ObjContract);
            }
            catch (Exception)
            {
                return RedirectToAction("Custcon_zonelocationtransmodewisemaster");
            }
        }

        public ActionResult AddCustcon_zonelocationtransmodewisemasterlist(int Id)
        {
            Cygnus_Custcontract_locaton_zone_transmode_wise_master objcharge = new Cygnus_Custcontract_locaton_zone_transmode_wise_master();
            objcharge.Id = Id;
            return PartialView("_Custcon_zonelocationtransmodewisemaster", objcharge);
        }

        public string AddCustcon_zonelocationtransmodewisemaster(CustomerContractViewModel ObjContract, List<Cygnus_Custcontract_locaton_zone_transmode_wise_master> ObjCCLZTWM)
        {
            bool Status = false;
            try
            {
                if (ObjCCLZTWM != null)
                {
                    string detail_XML = "<ArrayOfCCLZTWMData>";
                    foreach (var item in ObjCCLZTWM)
                    {
                        detail_XML = detail_XML + "<CCLZTWMData>";
                        detail_XML = detail_XML + "<ReportZone>" + item.ReportZone + "</ReportZone>";
                        detail_XML = detail_XML + "<ZoneId>" + item.ZoneId + "</ZoneId>";
                        detail_XML = detail_XML + "<Mode>" + item.Mode + "</Mode>";
                        detail_XML = detail_XML + "<EntryBy>" + BaseUserName.ToUpper() + "</EntryBy>";
                        detail_XML = detail_XML + "</CCLZTWMData>";

                    }
                    detail_XML = detail_XML + "</ArrayOfCCLZTWMData>";
                    Status = CS.InsertCustcon_zonelocationtransmodewisemaster(detail_XML, ObjContract.ObjCCLZTWM.Loccode);
                }
            }
            catch (Exception)
            {
                Status = false;
            }

            return Status.ToString();
        }
        public ActionResult Custcon_zonelocationtransmodewisemasterDone(string status)
        {
            ViewBag.status = status.ToUpper();
            return View();
        }

        #endregion

        #region Customer Contract Template
        public ActionResult CustomerContractTemplate()
        {
            CustomerContractTemplateViewModel CCTVM = new CustomerContractTemplateViewModel();
            CCTVM.CCCTM = new CYGNUS_Customer_Contract_Template();
            CCTVM.ListCCCTM = new List<CYGNUS_Charges_Customer_Template>();
            CCTVM.ListTerms = new List<CYGNUS_Contract_Terms>();
            CCTVM.ListCCCTM = CS.GetChargeMatrixForContractTemplate();
            return View(CCTVM);
        }

        public ActionResult AddTerms(int id)
        {
            CYGNUS_Contract_Terms ObjTerms = new CYGNUS_Contract_Terms();
            ObjTerms.RNo = id;
            return PartialView("_PartialContractTerms", ObjTerms);
        }

        public ActionResult GetContract_Rates(string CustCD, string TransitMode, string Matrix, string RateType)
        {
            List<ContractTemplateRates> ListCTR = new List<ContractTemplateRates>();
            ListCTR = CS.GetContractTemplateRates(CustCD, TransitMode, Matrix, RateType);
            return PartialView("_Contract_Rates", ListCTR);
        }

        [HttpPost]
        public ActionResult CustomerContractTemplateSubmit(CustomerContractTemplateViewModel CCTVM, List<CYGNUS_Contract_Terms> ContractTerms, List<CYGNUS_Charges_Customer_Template> CustCharges)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(CCTVM.CCCTM.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, CCTVM.CCCTM);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }
            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(CustCharges.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream, CustCharges);
                xmlStream.Position = 0;
                xmlDoc1.Load(xmlStream);
            }
            XmlDocument xmlContractTerms = new XmlDocument();
            XmlSerializer xmlSerializerContractTerms = new XmlSerializer(ContractTerms.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializerContractTerms.Serialize(xmlStream, ContractTerms);
                xmlStream.Position = 0;
                xmlContractTerms.Load(xmlStream);
            }
            bool Status = false;
            DataTable DT = CS.InsertCustomerContractTemplate(xmlDoc.InnerXml, xmlDoc1.InnerXml, xmlContractTerms.InnerXml, BaseUserName, BaseLocationCode);

            string Statusstr = DT.Rows[0]["Status"].ToString();
            if (Statusstr == "1")
            {
                Status = true;
            }

            string VendorCode = DT.Rows[0]["ContractId"].ToString();
            string ReturnStr = Status + "," + VendorCode;
            return RedirectToAction("ContractTemplateDone", new { CustCode = CCTVM.CCCTM.CustCD });
        }


        public ActionResult ContractTemplateDone(string CustCode)
        {
            ViewBag.CustCode = CustCode;
            return View();
        }

        #endregion
    }

}
