﻿using CipherLib;
using CYGNUS.Models;
using CYGNUS.ViewModels;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using FleetDataService.ViewModels;
using CYGNUS.ViewModel;
using CYGNUS.Classes;

//using CYGNUS.ViewModels;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class FinanceController : BaseController
    {
        //
        // GET: /Finance/

        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        GeneralFuncations GF = new GeneralFuncations();

        OperationService OS = new OperationService();
        MasterService MS = new MasterService();
        FinanceService FS = new FinanceService();
        ContractService CS = new ContractService();
        PaymentService PS = new PaymentService();

        #region Bill Submission

        public ActionResult BillSubmission()
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName == "ADMIN")
            //{
            //    BillingFilterViewModel BGVM = new BillingFilterViewModel();
            //    return View(BGVM);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}

            BillingFilterViewModel BGVM = new BillingFilterViewModel();
            return View(BGVM);
        }

        public ActionResult BillSubmissionProcess(BillingFilterViewModel model)
        {
            BillSubmissionViewModel VM = new BillSubmissionViewModel();
            VM.BillList = OS.GetBillForSubmission(model, BaseLocationCode, BaseCompanyCode, BaseUserName.ToUpper(), ViewBag.FinYearStart);
            var CustomerList = MS.GetCustomerMasterObject().Where(c => c.CUSTCD.ToUpper() == model.BillingParty.ToUpper()).ToList();
            VM.Party = CustomerList.Count() > 0 ? model.BillingParty.ToUpper() + " : " + CustomerList.FirstOrDefault().CUSTNM.ToUpper() : "";
            List<Webx_Master_General> ListPayBasis = MS.GetGeneralMasterObject().Where(c => c.CodeType == "BILLTYP").ToList();
            VM.BillType = ListPayBasis.Where(c => c.CodeId == model.BillType).ToList().FirstOrDefault().CodeDesc;
            return View(VM);
        }

        [HttpPost]
        public ActionResult BillSubmissionProcessSubmit(BillSubmissionViewModel model, List<webx_BILLMST> BillList, IEnumerable<HttpPostedFileBase> files)
        {
            DataTable DT = new DataTable();
            try
            {
                string Xml_Bill_Submission = "<root>";

                foreach (var itm in BillList.Where(c => c.IsBillSubmitted == true))
                {
                    if (itm.Dockno == null)
                        itm.Dockno = "";

                    string ImageName = "";
                    #region BillSubmission ImageUpload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["UploadImage_" + itm.BILLNO];
                            if (File.ContentLength > 0)
                            {
                                ImageName = SaveImage(File, itm.BILLNO, "BillSubmissionImage");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ViewBag.StrError = ex.Message.ToString();
                        return View("Error");
                    }
                    #endregion
                    Xml_Bill_Submission = Xml_Bill_Submission + "<Submission>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<billno>" + itm.BILLNO + "</billno>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<DueDt></DueDt>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<Loading>" + itm.Loading.ToString() + "</Loading>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<UnLoading>" + itm.UnLoading.ToString() + "</UnLoading>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<Detention>" + itm.Detention.ToString() + "</Detention>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<Other>" + itm.Other.ToString() + "</Other>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<DockNo>" + itm.Dockno.ToString() + "</DockNo>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<Penalty>" + itm.Penalty.ToString() + "</Penalty>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<DKTTOT>" + itm.BILLAMT.ToString() + "</DKTTOT>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "<ImageName>" + ImageName + "</ImageName>";
                    Xml_Bill_Submission = Xml_Bill_Submission + "</Submission> ";
                }
                Xml_Bill_Submission = Xml_Bill_Submission + "</root> ";

                DT = OS.SubmissionDone(Xml_Bill_Submission, BaseUserName.ToUpper(), model.BILLSUBTO, model.SUBTOTEL, model.BillSubDate.ToString("dd MMM yyyy"), BaseLocationCode, BaseCompanyCode);
                string BilSUBNO = DT.Rows[0]["BILLSUBDOCNO"].ToString();
                string TranXaction = DT.Rows[0]["TranXaction"].ToString();

                if (TranXaction == "1")
                {
                    return RedirectToAction("BillSubmissionDone", new { BilSUBNO = BilSUBNO });
                }
                else
                {
                    //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                    ViewBag.StrError = "Error " + TranXaction.ToString().Replace('\n', '_');
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
        }
        private string SaveImage(HttpPostedFileBase File, string Id, string FolderName)
        {
            string extension = "", name = "", path = "";
            string IdPath = Server.MapPath("~/Images/" + FolderName + "/" + GF.GenerateString(8));
            if (!Directory.Exists(IdPath))
                Directory.CreateDirectory(IdPath);

            string FileName = File.FileName;
            extension = System.IO.Path.GetExtension(File.FileName);
            name = GF.GenerateString(10) + extension;
            path = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);
            string path1 = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);

            if (System.IO.File.Exists(path1))
                System.IO.File.Delete(path1);

            File.SaveAs(path1);

            return name;
        }
        public ActionResult BillSubmissionDone(string BilSUBNO)
        {
            ViewBag.BilSUBNO = BilSUBNO;
            ViewBag.TranXaction = "Bill Submission Done";
            return View();
        }

        public JsonResult GetCustomer(string searchTerm)
        {
            var CMP = OS.GetCustomerListFromSearch(searchTerm);

            var users = from user in CMP

                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BillSubmitViaLink(string party_code, string fromtdate, string todate)
        {
            try
            {
                BillingFilterViewModel model = new BillingFilterViewModel();
                model.BillNo = "";
                model.BillingParty = party_code;
                model.FromDate = Convert.ToDateTime(fromtdate);
                model.ToDate = Convert.ToDateTime(todate);
                model.Paybasis = "All";
                model.BillType = "All";

                BillSubmissionViewModel VM = new BillSubmissionViewModel();
                VM.BillList = OS.GetBillForSubmission(model, BaseLocationCode, BaseCompanyCode, BaseUserName.ToUpper(), ViewBag.FinYearStart);
                var CustomerList = MS.GetCustomerMasterObject1(party_code, "", "");
                if (CustomerList.Count > 0)
                {
                    VM.Party = CustomerList.First().CUSTNM;
                }
                List<Webx_Master_General> ListPayBasis = MS.GetGeneralMasterObject().Where(c => c.CodeType == "BILLTYP").ToList();
                VM.BillType = model.BillType;
                return View("BillSubmissionProcess", VM);
            }
            catch (Exception)
            {
                ViewBag.StrError = "Bill(s) not Valid.";
                return View("Error");
            }
        }
        #endregion

        #region Bill Collection

        public ActionResult BillCollection()
        {
            //if (BaseFinYear == CurrFinYear || BaseUserName == "ADMIN")
            //{
            //    BillingFilterViewModel BGVM = new BillingFilterViewModel();
            //    return View(BGVM);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}

            BillingFilterViewModel BGVM = new BillingFilterViewModel();
            return View(BGVM);
        }

        public ActionResult BillCollectionList(BillingFilterViewModel model)
        {
            BillCollectionViewModel VM = new BillCollectionViewModel();
            List<webx_BILLMST> BillList = OS.GetBillForCollection(model, BaseLocationCode, BaseCompanyCode, BaseUserName.ToUpper());
            VM.BillList = BillList;
            VM.PAYBAS = model.BillType;

            return View(VM);
        }

        public ActionResult BillCollectionProcess(string stringBIllNO, string PAYBAS)
        {
            try
            {
                BillingFilterViewModel model = new BillingFilterViewModel();
                model.BillType = PAYBAS;
                model.FromDate = System.DateTime.Now;
                model.ToDate = System.DateTime.Now;
                model.BillNo = stringBIllNO;

                List<webx_BILLMST> BillList = OS.GetBillCollection_FromBillNo(model.BillNo);

                BillCollectionViewModel VM = new BillCollectionViewModel();
                VM.ObjBillMst = BillList.FirstOrDefault();
                VM.BillList = BillList;
                VM.PC = new PaymentControl();
                VM.PC.ChequeDate = System.DateTime.Now;
                VM.MRBranch = BaseLocationCode;
                VM.PC.PayAmount = BillList.Sum(c => c.PENDAMT);
                VM.PC.ISDepositedInBank = true;
                VM.PC.IsOnAccount = true;
                return View(VM);
            }
            catch (Exception)
            {
                return RedirectToAction("BillCollection");
            }
        }

        [HttpPost]
        public ActionResult BillCollectionProcessSubmit(BillCollectionViewModel VM, List<webx_BILLMST> BillList, PaymentControl PC, CYGNUS_Collection_Payment CCP, List<CouponCodeDataFromBillNo> BillCoupon)
        {
            try
            {
                #region Get All Data

                string txtChqNo = PC.ChequeNo;
                string txtChqDate = PC.ChequeDate.ToString("dd MMM yyyy");
                string txtChqAmt = Convert.ToString(PC.ChequeAmount);
                string txtAmtApplA = Convert.ToString(PC.PayAmount);
                string txtCashAmt = Convert.ToString(PC.CashAmount);
                string Txt_OnAccount_Balance = PC.BankLedger;
                string Txt_ColAmt = Convert.ToString(PC.Collection_Amt_From_Cheque);
                string txtRecBank = Convert.ToString(PC.ChequeAmount_Fromcheque);
                string Txt_BankBR = PC.Bank_Branch;
                string txtNetPay = Convert.ToString(PC.NET_RECEIVED);
                string ddlPayMode = PC.PaymentMode;
                string ddrCashcode = PC.CashLedger;
                string ddrBankaccode = PC.DepositedInBank;
                string DDL_Tdstype = PC.TDSType;
                bool Onaccount = PC.ISDepositedInBank;
                //bool rdDiposited = PC.ISDepositedInBank;
                string Hnd_Party = VM.ObjBillMst.PTMSCD;
                string Str_Onaccount = "N";
                if (Onaccount)
                {
                    Str_Onaccount = "Y";
                }

                string Diposited = "Y";

                if ((ddrBankaccode == "" || ddrBankaccode == null) && ddlPayMode.ToUpper() != "CASH")
                {
                    ViewBag.StrError = "Please select the Bank accode, PLz Try Again";
                    return View("Error");
                }

                string SQRY_count = "select count(*) from webx_acctinfo where Acccode='" + ddrBankaccode + "'";
                DataTable Dt_count = GF.GetDataTableFromSP(SQRY_count);
                string Counts = Dt_count.Rows[0][0].ToString();

                if (Counts != "1" && ddlPayMode.ToUpper() != "CASH")
                {
                    ViewBag.StrError = "Please select the Bank accode, PLz Try Again";
                    return View("Error");
                }

                #endregion

                #region ChqDet

                string Xml_Chq_Det = "<root><ChqDet>";
                Xml_Chq_Det = Xml_Chq_Det + "<Chqno>" + txtChqNo + "</Chqno>";
                Xml_Chq_Det = Xml_Chq_Det + "<Chqdt>" + txtChqDate + "</Chqdt>";
                Xml_Chq_Det = Xml_Chq_Det + "<Chqamt>" + txtChqAmt + "</Chqamt>";
                Xml_Chq_Det = Xml_Chq_Det + "<Banknm>" + txtRecBank + "</Banknm>";
                Xml_Chq_Det = Xml_Chq_Det + "<Bankbrn>" + Txt_BankBR + "</Bankbrn>";
                Xml_Chq_Det = Xml_Chq_Det + "<Ptmsptcd>" + Hnd_Party + "</Ptmsptcd>";
                Xml_Chq_Det = Xml_Chq_Det + "<Ptmsptnm></Ptmsptnm>";
                Xml_Chq_Det = Xml_Chq_Det + "<ColAmt>" + Txt_ColAmt + "</ColAmt>";
                Xml_Chq_Det = Xml_Chq_Det + "<brcd>" + BaseLocationCode + "</brcd>";
                Xml_Chq_Det = Xml_Chq_Det + "<Acccode>" + ddrBankaccode + "</Acccode>";
                Xml_Chq_Det = Xml_Chq_Det + "<Onaccount>" + Str_Onaccount.ToString() + "</Onaccount>";
                Xml_Chq_Det = Xml_Chq_Det + "<Diposited>" + Diposited + "</Diposited>";
                Xml_Chq_Det = Xml_Chq_Det + "</ChqDet></root>";

                #endregion

                #region Bill List XML

                string Xml_BillMR_DET = "<root>";
                double Tot_Tds = 0, Tot_Ded = 0;
                string BILLNO = "";

                foreach (var item in BillList)
                {
                    string Hnd_Billno = item.BILLNO;
                    string txtpendamt = Convert.ToString(item.PENDAMT);
                    string Hidden_svctax_rate = "0";//(HiddenField)gridrow.FindControl("Hidden_svctax_rate");
                    string Hidden_cess_rate = "0";//(HiddenField)gridrow.FindControl("Hidden_cess_rate");
                    string Hidden_H_cess_rate = "0";//(HiddenField)gridrow.FindControl("Hidden_H_cess_rate");
                    string Totalamt = Convert.ToString(item.Netamt);
                    string unexpamt = Convert.ToString(item.UNEXPDED);
                    string Txt_Remarks = item.REMARK;
                    string txtbillcolamt = Convert.ToString(item.Col_Amt);
                    string txtSvctax = Convert.ToString(0);
                    string txtCesstax = Convert.ToString(0);
                    string txtHEduCesstax = Convert.ToString(0);

                    // TextBox[] txtcharge = new TextBox[10];
                    string tds = Convert.ToString(item.TDSDED);
                    Tot_Tds = Tot_Tds + Convert.ToDouble(tds);

                    Xml_BillMR_DET = Xml_BillMR_DET + "<BillMRDET>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<TOTBILL>" + txtpendamt + "</TOTBILL>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<TDSDED>" + tds + "</TDSDED>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<NETAMT>" + Totalamt + "</NETAMT>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<UNEXPDED>" + unexpamt + "</UNEXPDED>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<DOCNO>" + Hnd_Billno + "</DOCNO>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<STax_Rate>" + Hidden_svctax_rate + "</STax_Rate>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<Cess_Rate>" + Hidden_cess_rate + "</Cess_Rate>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<HCess_Rate>" + Hidden_H_cess_rate + "</HCess_Rate>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<Col_Amt>" + txtbillcolamt + "</Col_Amt>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG1>" + item.Freight_Deduction + "</CHG1>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG2>" + item.Claim_Deduction + "</CHG2>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG3>" + 0 + "</CHG3>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG4>" + 0 + "</CHG4>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG5>" + 0 + "</CHG5>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG6>" + 0 + "</CHG6>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG7>" + 0 + "</CHG7>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG8>" + 0 + "</CHG8>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG9>" + 0 + "</CHG9>";
                    Xml_BillMR_DET = Xml_BillMR_DET + "<CHG10>" + item.Other_Amount + "</CHG10>";

                    BILLNO = BILLNO + Hnd_Billno + ",";
                    Tot_Ded = Convert.ToDouble(item.Freight_Deduction) + Convert.ToDouble(item.Claim_Deduction) +
                        Convert.ToDouble(item.Other_Amount);

                    Tot_Ded = Tot_Ded + Convert.ToDouble(tds);
                    Xml_BillMR_DET = Xml_BillMR_DET + "<Remarks>" + Txt_Remarks + "</Remarks>";

                    Xml_BillMR_DET = Xml_BillMR_DET + "</BillMRDET>";
                }
                Xml_BillMR_DET = Xml_BillMR_DET + "</root>";

                #endregion

                #region Validations

                string TotalAmt = "0.00";
                if (ddlPayMode == "Cash")
                {
                    TotalAmt = txtCashAmt;
                }
                else if (ddlPayMode == "Bank")
                {
                    TotalAmt = txtChqAmt;
                }
                else if (ddlPayMode == "-1")
                {
                    ViewBag.StrError = "Please select the paymode, PLz Tyr Again";
                    return View("Error");
                }
                else if (ddlPayMode == "Bank" && txtChqNo == "")
                {
                    ViewBag.StrError = "Please enter the cheque number, PLz Tyr Again";
                    return View("Error");
                }
                else if (ddlPayMode == "Bank" && txtChqAmt == "0.00")
                {
                    ViewBag.StrError = "Please enter the cheque amount, PLz Tyr Again";
                    return View("Error");
                }
                if (ddlPayMode.ToUpper() == "CASH" || ddlPayMode.ToUpper() == "BANK")
                {
                    string Cnt = OS.Duplicate_BillNO_Check(BILLNO);
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Bill Entered, This Bill Already Collected";
                        return View("Error");
                    }
                }

                if (ddlPayMode.ToUpper() == "BANK")
                {
                    string count = "select count(*) from webx_Chq_Bounce_Reoffer_Details where Chqno='" + PC.ChequeNo + "'";
                    DataTable Dt = GF.GetDataTableFromSP(count);
                    string TotalCounts = Dt.Rows[0][0].ToString();

                    if (TotalCounts == "1" && ddlPayMode.ToUpper() == "BANK")
                    {
                        ViewBag.StrError = "This Cheque No Is a Bounced,  Please Enter Other ChequeNo";
                        return View("Error");
                    }
                }

                //if (Convert.ToDecimal(Tot_Tds) != PC.totTDSAmt)
                //{
                //    ErrMsg = "TDS Calculation is wrong.";
                //    ViewBag.StrError = ErrMsg;
                //    return View("Error");
                //}

                #endregion

                #region MRHDR

                string PAYBAS = VM.ObjBillMst.PAYBAS;
                string txtVoucherDate = VM.MRDate.ToString("dd MMM yyyy");
                string Txt_manualMRsno = VM.ManualMRNO;

                string Xml_MRHDR = "<root><MRHDR>";
                Xml_MRHDR = Xml_MRHDR + "<MRSDT>" + txtVoucherDate + "</MRSDT>";
                Xml_MRHDR = Xml_MRHDR + "<MRSTYPE>" + PAYBAS + "</MRSTYPE>";
                Xml_MRHDR = Xml_MRHDR + "<MRSBR>" + BaseLocationCode + "</MRSBR>";
                Xml_MRHDR = Xml_MRHDR + "<MRBRNNM>" + OS.GetLocation(BaseLocationCode) + "</MRBRNNM>";
                Xml_MRHDR = Xml_MRHDR + "<PTCD>" + Hnd_Party.Trim() + "</PTCD>";
                Xml_MRHDR = Xml_MRHDR + "<PTMSNM></PTMSNM>";
                Xml_MRHDR = Xml_MRHDR + "<PTMSTEL></PTMSTEL>";
                Xml_MRHDR = Xml_MRHDR + "<PTMSADDR></PTMSADDR>";
                Xml_MRHDR = Xml_MRHDR + "<PAYBAS>" + PAYBAS + "</PAYBAS>";
                Xml_MRHDR = Xml_MRHDR + "<MRSAMT>" + TotalAmt + "</MRSAMT>";
                Xml_MRHDR = Xml_MRHDR + "<NETAMT>" + TotalAmt + "</NETAMT>";
                Xml_MRHDR = Xml_MRHDR + "<DEDUCTION>" + Tot_Ded + "</DEDUCTION>";
                Xml_MRHDR = Xml_MRHDR + "<MRSCASH>" + Math.Round(Convert.ToDecimal(txtCashAmt)) + "</MRSCASH>";
                Xml_MRHDR = Xml_MRHDR + "<MRSCHQ>" + Math.Round(Convert.ToDecimal(txtChqAmt)) + "</MRSCHQ>";
                Xml_MRHDR = Xml_MRHDR + "<MRSCHQNO>" + txtChqNo + "</MRSCHQNO>";
                Xml_MRHDR = Xml_MRHDR + "<MRSCHQDT>" + Convert.ToDateTime(txtChqDate).ToString("dd MMM yyyy") + "</MRSCHQDT>";
                Xml_MRHDR = Xml_MRHDR + "<MRSBANK>" + txtRecBank + "</MRSBANK>";
                Xml_MRHDR = Xml_MRHDR + "<MRS_CLOSED>" + "Y" + "</MRS_CLOSED>";
                Xml_MRHDR = Xml_MRHDR + "<DED_TDS>" + Tot_Tds + "</DED_TDS>";
                Xml_MRHDR = Xml_MRHDR + "<MR_CANCEL>" + "N" + "</MR_CANCEL>";
                Xml_MRHDR = Xml_MRHDR + "<BILLMR>" + "Y" + "</BILLMR>";
                Xml_MRHDR = Xml_MRHDR + "<finclosedt>" + txtVoucherDate + "</finclosedt>";
                Xml_MRHDR = Xml_MRHDR + "<paymode>" + ddlPayMode + "</paymode>";
                Xml_MRHDR = Xml_MRHDR + "<BankAcccode>" + ddrBankaccode + "</BankAcccode>";
                Xml_MRHDR = Xml_MRHDR + "<BankAccdesc>" + "" + "</BankAccdesc>";
                Xml_MRHDR = Xml_MRHDR + "<tdsacccode>" + DDL_Tdstype + "</tdsacccode>";
                Xml_MRHDR = Xml_MRHDR + "<tdsaccdesc>" + DDL_Tdstype + "</tdsaccdesc>";
                Xml_MRHDR = Xml_MRHDR + "<tdsfor>" + "" + "</tdsfor>";
                Xml_MRHDR = Xml_MRHDR + "<ManualMrsno>" + Txt_manualMRsno + "</ManualMrsno>";
                Xml_MRHDR = Xml_MRHDR + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_MRHDR = Xml_MRHDR + "<IsGSTApplied>1</IsGSTApplied>";
                Xml_MRHDR = Xml_MRHDR + "<remark>" + VM.Remarks + "</remark>";
                Xml_MRHDR = Xml_MRHDR + "</MRHDR></root>";

                string MRSNO = "", TranXaction = "", status = "", GenXml = "";
                GenXml = "<root>" + GenXml + "</root>";

                #endregion

                DataTable DT = new DataTable();
                string Year = System.DateTime.Now.Year.ToString();

                try
                {
                    DT = OS.Collection_Process(BaseLocationCode, GenXml.Replace("&", "&amp;").Replace("–", "-").Trim(), Xml_Chq_Det.Replace("&", "&amp;").Replace("–", "-").Trim(),
                        Xml_BillMR_DET.Replace("&", "&amp;").Replace("–", "-").Trim(), Xml_MRHDR.Replace("&", "&amp;").Replace("–", "-").Trim(),
                        BaseYearVal.Split('_')[0], BaseUserName.ToUpper(), "N");

                    status = DT.Rows[0]["Status"].ToString();
                    TranXaction = DT.Rows[0]["Message"].ToString();

                    if (status == "0" && TranXaction != "Done")
                    {
                        ViewBag.StrError = TranXaction.ToString();
                        return View("Error");
                    }
                    if (status == "1")
                    {
                        MRSNO = DT.Rows[0]["MRSNO"].ToString();
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = ex.Message.ToString();
                    return View("Error");
                }
                return RedirectToAction("BillCollectionDone", new { MRSNO = MRSNO, TranXaction = TranXaction });


            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
        }

        public ActionResult BillCollectionDone(string MRSNO, string TranXaction)
        {
            ViewBag.MRSNO = MRSNO;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        public JsonResult GetBankCashList(string Type)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                if (Type.ToUpper() == "CASH")
                {
                    string SQRY = "select acccode as Value,accdesc as Text from webx_acctinfo where acccategory='CASH'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                    List<GetCashDetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCashDetailList>(Dt);

                    return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                }
                if (Type.ToUpper() == "BANK")
                {
                    string SQRY = "select acccode as Value,accdesc as Text from webx_acctinfo where (((bkloccode like 'All' or PATINDEx ('%" + BaseLocationCode + "%',bkloccode)>0) AND acccategory='BANK')) order by Text asc";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                    List<GetBankDetailList> BankDetailList = DataRowToObject.CreateListFromTable<GetBankDetailList>(Dt);

                    return Json(BankDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                }

                return Json(list);
            }
            catch (Exception ex)
            {
                Error_Logs("Operation", "GetBankCashList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        public JsonResult GetTDS_ReceivableList()
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                string SQRY = "exec USP_Get_TDS_Receivable '" + BaseLocationCode + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<GetCashDetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCashDetailList>(Dt);

                return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs("Finance", "GetTDS_ReceivableList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        public JsonResult GetDepositedInbankList(string Type, string moduleType)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                string SQRY = "exec USP_GetDepositedInbank '" + Type + "','" + BaseLocationCode + "','" + moduleType + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<GetCashDetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCashDetailList>(Dt);

                return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs("Finance", "GetDepositedInbankList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }


        #endregion

        #region Billing Generation

        #region Bill Generation

        public ActionResult BillGenerationQuery(string Id)
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName == "ADMIN")
            //{
            BillingQueryModel BQM = new BillingQueryModel();
            BQM.Bill_POD_Approval_YN = "N";
            BQM.StaxYN = "N";
            BQM.StaxRate = 0;
            BQM.Trans_Type = "All";
            BQM.BusinessType = "";
            if (Id != "")
            {
                BQM.BillingParty = Id;
            }
            GSTCriteria ObjGC = new GSTCriteria();
            ObjGC.ActionName = "BillGeneration";
            ObjGC.ControllerName = "Finance";
            ObjGC.IsCustomer = true;
            ObjGC.IsState = true;
            ObjGC.IsGSTType = true;
            ObjGC.IsGSTApply = true;
            ObjGC.IsVendor = false;
            ObjGC.IsTransitMode = true;
            BQM.GC = ObjGC;

            return View(BQM);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        public ActionResult DAMBillGenerationQuery()
        {
            BillingQueryModel BQM = new BillingQueryModel();
            BQM.IsDAMBill = true;

            GSTCriteria ObjGC = new GSTCriteria();
            ObjGC.ActionName = "BillGeneration";
            ObjGC.ControllerName = "Finance";
            ObjGC.IsGSTType = true;
            ObjGC.IsGSTApply = true;
            BQM.GC = ObjGC;

            return View(BQM);
        }

        public ActionResult OctroiBillGenerationQuery(string Id)
        {
            BillingQueryModel BQM = new BillingQueryModel();
            BQM.IsDAMBill = true;
            BQM.IsOctroiBill = true;
            if (Id != "")
            {
                BQM.BillingParty = Id;
            }
            return View(BQM);
        }

        public ActionResult OctroiDetails(int id)
        {

            webx_BILLDET WMDI = new webx_BILLDET();
            WMDI.Id = id;

            return PartialView("_OctroiBill", WMDI);
        }



        public JsonResult CheckDocumentNoForSuppBill(string dockno, string docType)
        {
            return new JsonResult()
            {
                Data = new
                {
                    isValid= FS.CheckDocumentNoForSuppBill(dockno, docType, BaseFinYear),
                    //  TRDays = fovchrg.TRDays,
                }
            };
        }

        public JsonResult CheckOctroiDocketNo(string dockno, string partytype, string party)
        {
            string Flg = "N", MSG = "", DECVAL = "";
            string SQY_Get_Status = "exec usp_get_Status '" + dockno + "'";
            DataTable Dt_Get_Status = GF.GetDataTableFromSP(SQY_Get_Status);
            Flg = Dt_Get_Status.Rows[0][0].ToString();
            Webx_Master_General WMG = new Webx_Master_General();
            try
            {
                WMG = FS.CheckOctroiDocketNo(dockno, "Cust", partytype, party, BaseFinYear, BaseLocationCode);

                if (WMG.CodeId != "F")
                {
                    string SQRY_DEC_Val = "exec usp_get_PrepareOctroiBill_GridDetails_NewPortal '" + dockno + "','" + partytype + "'";
                    DataTable Dt_DEC_Val = GF.GetDataTableFromSP(SQRY_DEC_Val);
                    WMG.CodeType = Dt_DEC_Val.Rows[0][0].ToString();
                    WMG.OCT_AMT = Dt_DEC_Val.Rows[0][1].ToString();
                    WMG.oct_percentage = Convert.ToString((100 * Convert.ToDecimal(WMG.OCT_AMT)) / Convert.ToDecimal(WMG.CodeType));
                    WMG.DKTTOT = Dt_DEC_Val.Rows[0][1].ToString();
                    WMG.OCT_RECEIPTNO = Dt_DEC_Val.Rows[0][2].ToString();
                    WMG.recptdt = Dt_DEC_Val.Rows[0][3].ToString();
                }
                Flg = WMG.CodeId;
                MSG = WMG.CodeDesc;
                DECVAL = WMG.CodeType;

                return new JsonResult()
                {
                    Data = new
                    {
                        Flg = Flg,
                        MSG = MSG,
                        DECVAL = DECVAL,
                        OCT_AMT = WMG.OCT_AMT,
                        oct_percentage = WMG.oct_percentage,
                        DKTTOT = WMG.DKTTOT,
                        OCT_RECEIPTNO = WMG.OCT_RECEIPTNO,
                        recptdt = WMG.recptdt,
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Flg = Flg,
                        MSG = MSG,
                        DECVAL = DECVAL,
                        OCT_AMT = WMG.OCT_AMT,
                        oct_percentage = WMG.oct_percentage,
                        DKTTOT = WMG.DKTTOT,
                        OCT_RECEIPTNO = WMG.OCT_RECEIPTNO,
                        recptdt = WMG.recptdt,
                    }
                };
            }

        }

        public ActionResult BillGeneration(BillingQueryModel BQM)
        {
            try
            {
                BillGeneration BG = new BillGeneration();
                BQM.CurLocation = BaseLocationCode;
                if (!BQM.IsOctroiBill)
                {

                    if (!BQM.IsDAMBill)
                    {
                        //START GST Changes By Chirag D
                        if (BQM.GC.IsGSTApply)
                        {
                            BQM.CurLocation = BaseLocationCode;

                        }
                        else
                        {
                            BQM.CurLocation = "All";
                        }

                        BQM.FromDate = BQM.GC.FromDate;
                        BQM.ToDate = BQM.GC.ToDate;
                        BQM.Paybas = BQM.GC.PAYBAS;
                        BQM.BillingParty = BQM.GC.PARTY_CODE;
                        BQM.manual_dockno = BQM.GC.manual_dockno;
                        BQM.Service_Class = BQM.GC.Service_Class;

                        if (BQM.GC.TransitMode != "" && BQM.GC.TransitMode != null)
                        {
                            BQM.Trans_Type = BQM.GC.TransitMode;
                        }
                        else if (BQM.Trans_Type != "" && BQM.Trans_Type != null)
                        {
                            BQM.Trans_Type = BQM.Trans_Type;
                        }
                        else
                        {
                            BQM.Trans_Type = "All";
                        }
                        BQM.DateType = BQM.GC.DateType;
                    }
                    else
                    {
                        BQM.CurLocation = "All";
                        BQM.PartyType = "Party";
                    }

                    BG.DocketList = FS.GetBillDocket(BQM);
                    //END GST Changes By Chirag D
                }

                if (BQM.IsOctroiBill)
                {
                    BG.BILLDET = new List<webx_BILLDET>();
                    webx_BILLDET WBD = new webx_BILLDET();
                    WBD.Id = 1;
                    BG.BILLDET.Add(WBD);
                }

                BG.BQM = BQM;
                BG.CUSTHDR = FS.GetBillCustDetails(BQM.BillingParty).FirstOrDefault();
                webx_BILLMST BILLMST = new webx_BILLMST();
                BG.IsDAMBill = BQM.IsDAMBill;
                BG.IsOctroiBill = BQM.IsOctroiBill;
                BG.DeliveryDate = BQM.DeliveryDate;

                if (BQM.IsOctroiBill)
                {
                    string RULDESC = "Service Tax For Octroi Bill";
                    string Module_Name = "Prepare Octroi Bill";
                    string IsOctroiStax = "N";

                    var OcbList = MS.GetModuleRule().Where(c => c.RULE_DESC.ToUpper() == RULDESC.ToUpper()
                         && c.Module_Name.ToUpper() == Module_Name.ToUpper()).ToList();

                    if (OcbList.Count > 0) { IsOctroiStax = OcbList.FirstOrDefault().RULE_Y_N; }
                    BG.IsOctroiStax = false;
                    if (IsOctroiStax == "Y") { BG.IsOctroiStax = true; }
                }

                BILLMST.BBRCD = BaseLocationCode;
                BILLMST.PTMSCD = BG.CUSTHDR.Custcd;
                BILLMST.PTMSNM = BG.CUSTHDR.custnm;
                BILLMST.PTMSADDR = BG.CUSTHDR.custaddress;
                BILLMST.PTMSTEL = BG.CUSTHDR.telno;
                BILLMST.PTMSEMAIL = BG.CUSTHDR.emailids;
                BILLMST.BILLSUBBRCD = BG.CUSTHDR.billsub_loccode.ToUpper();
                BILLMST.BILLCOLBRCD = BG.CUSTHDR.billcol_loccode.ToUpper();
                BG.CUSTHDR.OutStdAmt = FS.GetBillCustDetails(BG.CUSTHDR.Custcd, BG.CUSTHDR.billcol_loccode).FirstOrDefault().Generated;
                BG.BILLMST = BILLMST;
                return View(BG);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
        }

        public ActionResult BillGenerationSubmit(BillGeneration BG, BillingQueryModel BQM, List<BillDocket> DocketList, List<webx_BILLDET> LedgerList)
        {
            try
            {
                string Billtype = "";
                if (BQM.Paybas == "P01")
                    Billtype = "1";
                else if (BQM.Paybas == "P03")
                    Billtype = "3";
                else
                    Billtype = "2";
                if (BQM.Paybas == "P02")
                {
                    BG.BILLMST.BILLSUBBRCD = MainLocCode;
                }

                if (BQM.StaxYN == null)
                {
                    BQM.StaxYN = "N";
                    BQM.StaxRate = 0;
                }
                if (BQM.BusinessType == null)
                    BQM.BusinessType = "";

                string BILLSTATUS = "BILL GENERATED";
                string Businesstype = BQM.BusinessType;

                string TransportMode = "";

                if (BQM.TransportMode == "" || BQM.TransportMode == null)
                {
                    TransportMode = "All";
                }
                else
                {
                    TransportMode = BQM.TransportMode;
                }

                string Bill_Location = BaseLocationCode;
                if (BG.BillLocation == "BBRCD")
                {
                    Bill_Location = BaseLocationCode;
                }
                else if (BG.BillLocation == "BILLSUBBRCD")
                {
                    Bill_Location = BG.BILLMST.BILLSUBBRCD;
                }

                if (BG.BILLMST.REMARK == null)
                    BG.BILLMST.REMARK = "";


                if (BG.BILLMST.spe_inst == null)
                    BG.BILLMST.spe_inst = "";

                if (BG.BILLMST.BILLAMT == 0)
                {
                    ViewBag.StrError = "Bill Amount is 0";
                    //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                    return View("Error");
                }

                string Xml_BillMst = "<root><Bill>";
                if (BG.IsOctroiBill)
                {

                    Xml_BillMst = "<root><BillMst>";
                }

                if (BG.IsOctroiBill)
                {
                    double STAXAMT = 0, CessAmt = 0, HCessAmt = 0, SBAmt = 0;

                    foreach (var Itm in LedgerList.ToList())
                    {
                        STAXAMT = STAXAMT + Itm.SVRCAMT;
                        CessAmt = CessAmt + Itm.CESSAMT;
                        HCessAmt = HCessAmt + Itm.Hedu_Cess;
                        SBAmt = SBAmt + Itm.SBCAMT;
                    }

                    if (BG.BILLMST.PTMSEMAIL != null)
                    {
                        BG.BILLMST.PTMSEMAIL = BG.BILLMST.PTMSEMAIL.Trim();
                    }
                    if (BG.BILLMST.PTMSADDR != null)
                    {
                        BG.BILLMST.PTMSADDR = BG.BILLMST.PTMSADDR;
                    }
                    if (BG.BILLMST.REMARK != null)
                    {
                        BG.BILLMST.REMARK = BG.BILLMST.REMARK.ToUpper().Trim();
                    }
                    if (BG.BILLMST.manualbillno != null)
                    {
                        BG.BILLMST.manualbillno = BG.BILLMST.manualbillno.ToUpper().Trim();
                    }
                    if (BG.BILLMST.spe_inst != null)
                    {
                        BG.BILLMST.spe_inst = BG.BILLMST.spe_inst.ToUpper().Trim();
                    }
                    if (Businesstype != null)
                    {
                        Businesstype = Businesstype.Trim();
                    }




                    //Xml_BillMst = Xml_BillMst + "<Billno>" + Billno.Trim() + "</Billno>";
                    Xml_BillMst = Xml_BillMst + "<BGNDT>" + BG.BILLMST.BGNDT.ToString("dd MMM yyyy") + "</BGNDT>";
                    Xml_BillMst = Xml_BillMst + "<BDUEDT>" + BG.BILLMST.BDUEDT.ToString("dd MMM yyyy") + "</BDUEDT>";
                    Xml_BillMst = Xml_BillMst + "<BBRCD>" + BG.BILLMST.BBRCD + "</BBRCD>";
                    Xml_BillMst = Xml_BillMst + "<BBRNM>" + BaseLocationName + "</BBRNM>";
                    Xml_BillMst = Xml_BillMst + "<PAYBAS>6</PAYBAS>";
                    Xml_BillMst = Xml_BillMst + "<PTMSCD>" + BG.BILLMST.PTMSCD.Trim() + "</PTMSCD>";
                    Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.BILLMST.PTMSNM.Trim() + "</PTMSNM>";
                    Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.BILLMST.PTMSTEL + "</PTMSTEL>";
                    Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.BILLMST.PTMSEMAIL + "</PTMSEMAIL>";
                    Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.BILLMST.PTMSADDR.ReplaceSpecialCharacters() + "</PTMSADDR>";
                    Xml_BillMst = Xml_BillMst + "<REMARK>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters() + "</REMARK>";
                    Xml_BillMst = Xml_BillMst + "<BILLAMT>" + BG.BILLMST.BILLAMT.ToString().Trim() + "</BILLAMT>";
                    Xml_BillMst = Xml_BillMst + "<PENDAMT>" + BG.BILLMST.BILLAMT.ToString().Trim() + "</PENDAMT>";
                    Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BILLSTATUS.Trim() + "</BILLSTATUS>";
                    Xml_BillMst = Xml_BillMst + "<BILLSUBBRCD>" + BG.BILLMST.BILLSUBBRCD.Trim() + "</BILLSUBBRCD>";
                    Xml_BillMst = Xml_BillMst + "<BILLCOLBRCD>" + BG.BILLMST.BILLCOLBRCD.Trim() + "</BILLCOLBRCD>";
                    Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst + "</spe_inst>";
                    Xml_BillMst = Xml_BillMst + "<manualbillno>" + BG.BILLMST.manualbillno + "</manualbillno>";
                    Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + TransportMode + "</TRN_Mod>";
                    Xml_BillMst = Xml_BillMst + "<Businesstype>" + Businesstype + "</Businesstype>";
                    Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                    Xml_BillMst = Xml_BillMst + "<BGENEMPCD>" + BaseUserName.ToUpper() + "</BGENEMPCD>";
                    if (BG.IsOctroiStax)
                    {
                        Xml_BillMst = Xml_BillMst + "<STax_Y_N>Y</STax_Y_N>";
                    }
                    else
                        Xml_BillMst = Xml_BillMst + "<STax_Y_N>N</STax_Y_N>";

                    Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                    Xml_BillMst = Xml_BillMst + "<cess_rate>" + BG.BILLMST.cess_rate + "</cess_rate>";
                    Xml_BillMst = Xml_BillMst + "<H_cess_rate>" + BG.BILLMST.SBCRATE + "</H_cess_rate>";
                    Xml_BillMst = Xml_BillMst + "<SBCRATE>" + BG.BILLMST.SBCRATE + "</SBCRATE>";
                    Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                    Xml_BillMst = Xml_BillMst + "<BILL_CANCEL>" + "N" + "</BILL_CANCEL>";
                    Xml_BillMst = Xml_BillMst + "<closebill>" + "N" + "</closebill>";
                    Xml_BillMst = Xml_BillMst + "<SVRCAMT>" + STAXAMT + "</SVRCAMT>";
                    Xml_BillMst = Xml_BillMst + "<CESSAMT>" + CessAmt + "</CESSAMT>";


                    // = 0, CessAmt = 0, HCessAmt = 0, SBAmt = 0;
                }
                else
                {
                    //Xml_BillMst = Xml_BillMst + "<Billno>" + Billno.Trim() + "</Billno>";
                    Xml_BillMst = Xml_BillMst + "<Bgndt>" + BG.BILLMST.BGNDT.ToString("dd MMM yyyy") + "</Bgndt>";
                    Xml_BillMst = Xml_BillMst + "<Bduedt>" + BG.BILLMST.BDUEDT.ToString("dd MMM yyyy") + "</Bduedt>";
                    Xml_BillMst = Xml_BillMst + "<Bbrcd>" + BG.BILLMST.BBRCD + "</Bbrcd>";
                    Xml_BillMst = Xml_BillMst + "<Brnm>" + BaseLocationName + "</Brnm>";

                    if (BG.IsDAMBill)
                        Xml_BillMst = Xml_BillMst + "<Paybas>13</Paybas>";
                    else
                        Xml_BillMst = Xml_BillMst + "<Paybas>" + Billtype.Trim() + "</Paybas>";

                    Xml_BillMst = Xml_BillMst + "<PTMSCD>" + BG.BILLMST.PTMSCD.Trim() + "</PTMSCD>";
                    Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.BILLMST.PTMSNM.Trim() + "</PTMSNM>";
                    Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.BILLMST.PTMSTEL + "</PTMSTEL>";
                    Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.BILLMST.PTMSEMAIL + "</PTMSEMAIL>";
                    Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.BILLMST.PTMSADDR.ReplaceSpecialCharacters() + "</PTMSADDR>";
                    Xml_BillMst = Xml_BillMst + "<remarks>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters().Trim() + "</remarks>";
                    Xml_BillMst = Xml_BillMst + "<BILLAMT>" + BG.BILLMST.BILLAMT.ToString().Trim() + "</BILLAMT>";
                    Xml_BillMst = Xml_BillMst + "<PENDAMT>" + BG.BILLMST.BILLAMT.ToString().Trim() + "</PENDAMT>";
                    Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BILLSTATUS.Trim() + "</BILLSTATUS>";
                    Xml_BillMst = Xml_BillMst + "<billsubbrcd>" + BG.BILLMST.BILLSUBBRCD.Trim() + "</billsubbrcd>";
                    Xml_BillMst = Xml_BillMst + "<billcolbrcd>" + BG.BILLMST.BILLCOLBRCD.Trim() + "</billcolbrcd>";
                    Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst.ToUpper().Trim() + "</spe_inst>";
                    Xml_BillMst = Xml_BillMst + "<Manualbillno>" + BG.BILLMST.manualbillno + "</Manualbillno>";
                    Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + TransportMode + "</TRN_Mod>";
                    Xml_BillMst = Xml_BillMst + "<Businesstype>" + Businesstype.Trim() + "</Businesstype>";
                    Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                    Xml_BillMst = Xml_BillMst + "<Empcd>" + BaseUserName.ToUpper() + "</Empcd>";
                    Xml_BillMst = Xml_BillMst + "<STax_Y_N>" + BQM.StaxYN + "</STax_Y_N>";
                    Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                    Xml_BillMst = Xml_BillMst + "<cess_rate>" + BG.BILLMST.cess_rate + "</cess_rate>";
                    Xml_BillMst = Xml_BillMst + "<H_cess_rate>" + BG.BILLMST.SBCRATE + "</H_cess_rate>";
                    Xml_BillMst = Xml_BillMst + "<SBCRATE>" + BG.BILLMST.SBCRATE + "</SBCRATE>";
                    Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                    Xml_BillMst = Xml_BillMst + "<AutoBillGenerate>Y</AutoBillGenerate>";
                }

                if (BG.IsDAMBill)
                {
                    Xml_BillMst = Xml_BillMst + "<ProposalDelyDt>" + BG.DeliveryDate.ToString("dd MMM yyyy") + "</ProposalDelyDt>";
                }

                if (BG.IsOctroiBill)
                {
                    Xml_BillMst = Xml_BillMst + "</BillMst></root>";
                }
                else
                {
                    Xml_BillMst = Xml_BillMst + "</Bill></root>";
                }

                string Xml_BillDet = "<root>";

                if (!BG.IsOctroiBill)
                {
                    foreach (var Itm in DocketList.Where(c => c.IsEnabled).ToList())
                    {
                        Xml_BillDet = Xml_BillDet + "<Det>";
                        Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.dockno.Trim() + "</DOCKNO>";
                        if (BG.IsDAMBill)
                        {

                            Xml_BillDet = Xml_BillDet + "<TOTAL_DAYS>" + Itm.Total_Storage_Day + "</TOTAL_DAYS>";
                            Xml_BillDet = Xml_BillDet + "<FREE_DAYS>" + Itm.Total_Free_Days + "</FREE_DAYS>";
                            Xml_BillDet = Xml_BillDet + "<ACTUAL_DAYS>" + Itm.Actual + "</ACTUAL_DAYS>";
                            Xml_BillDet = Xml_BillDet + "<TOTALDAMAMT>" + Itm.DemAmt + "</TOTALDAMAMT>";
                            Xml_BillDet = Xml_BillDet + "<NETDAMAMT>" + Itm.Total + "</NETDAMAMT>";
                            Xml_BillDet = Xml_BillDet + "<CONTRACTID>" + Itm.ContractID + "</CONTRACTID>";
                        }
                        Xml_BillDet = Xml_BillDet + "</Det> ";
                    }
                }

                if (BG.IsOctroiBill)
                {
                    foreach (var Itm in LedgerList.ToList())
                    {
                        Xml_BillDet = Xml_BillDet + "<BillDet>";
                        Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.DOCKNO.Trim() + "</DOCKNO>";
                        Xml_BillDet = Xml_BillDet + "<DOCKSF>" + "." + "</DOCKSF>";
                        Xml_BillDet = Xml_BillDet + "<DKTTOT>" + Itm.DKTTOT + "</DKTTOT>";
                        Xml_BillDet = Xml_BillDet + "<OCT_RECEIPTNO>" + Itm.OCT_RECEIPTNO + "</OCT_RECEIPTNO>";
                        Xml_BillDet = Xml_BillDet + "<OCT_AMT>" + Itm.OCT_AMT + "</OCT_AMT>";
                        Xml_BillDet = Xml_BillDet + "<totamt>" + Itm.totamt + "</totamt>";
                        Xml_BillDet = Xml_BillDet + "<declval>" + Itm.declval + "</declval>";
                        Xml_BillDet = Xml_BillDet + "<OTCHG>" + Itm.OTCHG + "</OTCHG>";
                        Xml_BillDet = Xml_BillDet + "<process_chrg>" + Itm.process_chrg + "</process_chrg>";
                        Xml_BillDet = Xml_BillDet + "<oct_percentage>" + Itm.oct_percentage + "</oct_percentage>";
                        Xml_BillDet = Xml_BillDet + "<clearance_chrg>" + Itm.clearance_chrg + "</clearance_chrg>";
                        Xml_BillDet = Xml_BillDet + "<processing_per>" + Itm.processing_per + "</processing_per>";
                        Xml_BillDet = Xml_BillDet + "<recptdt>" + GF.FormateDate(Itm.recptdt) + "</recptdt>";
                        Xml_BillDet = Xml_BillDet + "<octroipaid_by>" + Itm.DKTTOT + "</octroipaid_by>";
                        Xml_BillDet = Xml_BillDet + "<oct_Custcd>" + BG.BILLMST.PTMSCD.Trim() + "</oct_Custcd>";
                        Xml_BillDet = Xml_BillDet + "<oct_Custnm>" + BG.BILLMST.PTMSNM.Trim() + "</oct_Custnm>";
                        Xml_BillDet = Xml_BillDet + "<bill_mr_dt>" + GF.FormateDate(BG.BILLMST.BGNDT) + "</bill_mr_dt>";
                        Xml_BillDet = Xml_BillDet + "<customer_paidby>" + BaseUserName.ToUpper() + "</customer_paidby>";
                        Xml_BillDet = Xml_BillDet + "<customer_paidat>" + Itm.DKTTOT + "</customer_paidat>";
                        Xml_BillDet = Xml_BillDet + "<OCT_DET_YN>Y</OCT_DET_YN>";

                        if (BG.IsOctroiStax)
                        {
                            Xml_BillDet = Xml_BillDet + "<SVRCAMT>" + Itm.SVRCAMT + "</SVRCAMT>";
                            Xml_BillDet = Xml_BillDet + "<CESSAMT>" + Itm.CESSAMT + "</CESSAMT>";
                            Xml_BillDet = Xml_BillDet + "<Hedu_Cess>" + Itm.Hedu_Cess + "</Hedu_Cess>";
                            Xml_BillDet = Xml_BillDet + "<SBCAMT>" + Itm.SBCAMT + "</SBCAMT>";

                        }
                        else
                        {
                            Xml_BillDet = Xml_BillDet + "<SVRCAMT>" + "0" + "</SVRCAMT>";
                            Xml_BillDet = Xml_BillDet + "<CESSAMT>" + "0" + "</CESSAMT>";
                            Xml_BillDet = Xml_BillDet + "<Hedu_Cess>" + "0" + "</Hedu_Cess>";
                            Xml_BillDet = Xml_BillDet + "<SBCAMT>" + "0" + "</SBCAMT>";

                        }

                        if (BG.BILLMST.PTMSCD == "8888")
                        {
                            Xml_BillDet = Xml_BillDet + "<Party>" + "8888" + "</Party>";
                            //if (rdbParty.Checked == true)
                            //{
                            Xml_BillDet = Xml_BillDet + "<rbPartyStatus>" + "T" + "</rbPartyStatus>";
                            Xml_BillDet = Xml_BillDet + "<PartyName>" + BG.BILLMST.PTMSNM + "</PartyName>";
                            //}
                            //else
                            //{
                            //    Xml_BillDet = Xml_BillDet + "<rbPartyStatus>" + "F" + "</rbPartyStatus>";
                            //    Xml_BillDet = Xml_BillDet + "<PartyName>" + BG.BILLMST.PTMSNM + "</PartyName>";
                            //}
                        }
                        else
                        {
                            Xml_BillDet = Xml_BillDet + "<Party>" + "NA" + "</Party>";
                            Xml_BillDet = Xml_BillDet + "<rbPartyStatus>" + "NA" + "</rbPartyStatus>";
                            Xml_BillDet = Xml_BillDet + "<PartyName>" + "NA" + "</PartyName>";
                        }

                        Xml_BillDet = Xml_BillDet + "</BillDet>";
                    }
                }


                Xml_BillDet = Xml_BillDet + "</root>";


                string BillType = "2";

                if (BG.IsDAMBill)
                {
                    BillType = "13";
                }

                if (BG.IsOctroiBill)
                {
                    BillType = "OCT";
                }

                DataTable DT = FS.BillSubmit(Bill_Location, Xml_BillMst, Xml_BillDet, BaseFinYear, BQM.BusinessType, BillType, BQM.GC.IsGSTApply, BQM.GC.StateCode);
                TempData["BMQ"] = BQM;
                return RedirectToAction("BillDone", new { BILLNO = DT.Rows[0]["Billno"].ToString(), TranXaction = DT.Rows[0]["TranXaction"].ToString() });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult AutoBillGearationProcess()
        {
            var listOfCycleType = MS.GetGeneralMasterObject().Where(c => c.CodeType == "CYCLETYPE" && c.StatusCode == "Y" && c.CodeId == "2").ToList();
            foreach (var generalItem in listOfCycleType)
            {
                var sqlCutomerListforBillGeneration = "SELECT CCBCS.BillGenDate,CCBCS.DateRange,WCH.CUSTCD,WCH.CycleType,WCH.BillGenType,ISNULL(WCH.ModeWise,0) AS ModeWise,WCC.billgen_loccode,CCBCS.CodeId " +
                    "FROM webx_CUSTHDR WCH WITH(NOLOCK) " +
                    "INNER JOIN WEBX_CUSTCONTRACT_HDR WCCH WITH(NOLOCK) ON WCH.CUSTCD = WCCH.Custcode " +
                    "INNER JOIN Cygnus_CustomerBillCycleSetting CCBCS WITH(NOLOCK) ON CCBCS.CUSTCD = WCH.CUSTCD " +
                    "LEFT JOIN WEBX_CUSTCONTRACT_CHARGE WCC WITH(NOLOCK) ON WCCH.ContractId = WCC.ContractId " +
                    "WHERE WCH.CUST_ACTIVE = 'Y' AND 'P02' IN (SELECT * FROM dbo.SplitString(WCH.CUSTCAT,',')) AND WCCH.activeflag = 'Y' AND ISNULL(WCH.AutoBillAllowed,0) = 1 AND WCCH.Contract_Eddate > GETDATE() " +
                    "AND (WCH.CycleType IN ('" + generalItem.CodeId + "') AND CCBCS.BillGenDate = CAST(DAY(GETDATE()) AS INT))";
                DataTable DTCutomerListforBillGeneration = GF.GetDataTableFromSP(sqlCutomerListforBillGeneration);
                List<Cygnus_CustomerBillCycleSetting> CutomerListforBillGeneration = DataRowToObject.CreateListFromTable<Cygnus_CustomerBillCycleSetting>(DTCutomerListforBillGeneration);

                foreach (var custItem in CutomerListforBillGeneration) // List of Cutomer for whom need to generate bill for Today
                {
                    int fromDate = Convert.ToInt32(custItem.DateRange.Split('-')[0]);
                    int toDate = Convert.ToInt32(custItem.DateRange.Split('-')[1]);
                    int codeId = Convert.ToInt32(custItem.CodeId);
                    int cycleType = Convert.ToInt32(custItem.CycleType);

                    var sqlDocketList = "USP_GetDatewiseDocketList '" + fromDate + "','" + toDate + "','" + codeId + "','" + cycleType + "','" + custItem.CUSTCD + "'";
                    DataTable DTDocketList = GF.GetDataTableFromSP(sqlDocketList);
                    List<BillDocket> CustomerWiseDocketList = DataRowToObject.CreateListFromTable<BillDocket>(DTDocketList); // List of Docket between fromDate and toDate and Customer wise

                    //List<BillDocket> CustomerWiseDocketList = new List<BillDocket>();
                    //CustomerWiseDocketList = DocketList.Where(c => c.PARTY_CODE == custItem.CUSTCD).ToList(); // Customer wise Docket List

                    if (custItem.BillGenType == 1) //DATE WISE
                    {
                        if (custItem.ModeWise == true) // MODE WISE
                        {
                            var listTRN_MOD = CustomerWiseDocketList.Select(c => c.trn_mod).Distinct().ToList();
                            foreach (var trn_modItem in listTRN_MOD) // List of Trans Mode in Docketlist
                            {
                                List<BillDocket> CustomerTRN_ModWiseDocketList = new List<BillDocket>();
                                CustomerTRN_ModWiseDocketList = CustomerWiseDocketList.Where(c => c.trn_mod == "4"/*trn_modItem*/).ToList();

                                var listOfDates = CustomerTRN_ModWiseDocketList.Select(c => c.dockdt).Distinct().ToList();
                                foreach (var dateItem in listOfDates)
                                {
                                    List<BillDocket> CustomerWiseDateWiseDocketList = new List<BillDocket>();
                                    CustomerWiseDateWiseDocketList = CustomerTRN_ModWiseDocketList.Where(c => c.dockdt == dateItem).ToList(); // Cutomer wise TRN Mode wise Date waise Docket List

                                    var listGSTType = CustomerWiseDateWiseDocketList.Select(c => c.GSTType).Distinct().ToList();
                                    foreach (var gstTypeItem in listGSTType) // List of GST Type in Docketlist
                                    {
                                        List<BillDocket> CustomerGSTTypeWiseDocketList = new List<BillDocket>();
                                        CustomerGSTTypeWiseDocketList = CustomerWiseDocketList.Where(c => c.GSTType == "I"/*gstTypeItem*/).ToList();
                                        var listFincmplBRState = CustomerGSTTypeWiseDocketList.Select(c => c.fincmplbrState).Distinct().ToList();
                                        foreach (var fincmplBRStateItem in listFincmplBRState) // List of State in Docketlist
                                        {
                                            List<BillDocket> CustomerGSTTypeStateWiseDocketList = new List<BillDocket>();
                                            CustomerGSTTypeStateWiseDocketList = CustomerGSTTypeWiseDocketList.Where(c => c.fincmplbrState == fincmplBRStateItem).ToList();

                                            /*Bill Generation Started*/
                                            BillGeneration BG = new BillGeneration();
                                            BG.BILLMST = new webx_BILLMST();
                                            BG.CUSTHDR = new BillCustDetails();
                                            BG.BILLMST.PAYBAS = "2";
                                            BG.BILLMST.STax_Y_N = "N";
                                            BG.BILLMST.svctax_rate = 0;
                                            BG.BILLMST.Businesstype = "";
                                            BG.BILLMST.BILLSTATUS = "BILL GENERATED";
                                            BG.BILLMST.TRN_MOD = "";
                                            BG.BILLMST.REMARK = "AUTO BILL GENERATED ON " + GF.FormateDate(System.DateTime.Now) + " AT " + System.DateTime.Now.TimeOfDay.ToString();
                                            BG.BILLMST.spe_inst = "";
                                            BG.CUSTHDR = FS.GetBillCustDetails(custItem.CUSTCD).FirstOrDefault();

                                            string Xml_BillMst = "<root><Bill>";
                                            Xml_BillMst = Xml_BillMst + "<Bgndt>" + GF.FormateDate(System.DateTime.Now) + "</Bgndt>";
                                            Xml_BillMst = Xml_BillMst + "<Bduedt>" + GF.FormateDate(System.DateTime.Now.AddDays(Convert.ToInt32(BG.CUSTHDR.credit_day))) + "</Bduedt>";
                                            Xml_BillMst = Xml_BillMst + "<Bbrcd>" + custItem.billgen_loccode + "</Bbrcd>";
                                            Xml_BillMst = Xml_BillMst + "<billsubbrcd>" + BG.CUSTHDR.billsub_loccode + "</billsubbrcd>";
                                            Xml_BillMst = Xml_BillMst + "<billcolbrcd>" + BG.CUSTHDR.billcol_loccode + "</billcolbrcd>";
                                            Xml_BillMst = Xml_BillMst + "<Brnm>" + custItem.billgen_loccode + "</Brnm>";
                                            Xml_BillMst = Xml_BillMst + "<Paybas>" + BG.BILLMST.PAYBAS + "</Paybas>";
                                            Xml_BillMst = Xml_BillMst + "<PTMSCD>" + custItem.CUSTCD + "</PTMSCD>";
                                            Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.CUSTHDR.custnm + "</PTMSNM>";
                                            Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.CUSTHDR.telno + "</PTMSTEL>";
                                            Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.CUSTHDR.emailids + "</PTMSEMAIL>";
                                            Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.CUSTHDR.custaddress.ReplaceSpecialCharacters() + "</PTMSADDR>";
                                            Xml_BillMst = Xml_BillMst + "<remarks>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters().Trim() + "</remarks>";
                                            Xml_BillMst = Xml_BillMst + "<BILLAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</BILLAMT>";
                                            Xml_BillMst = Xml_BillMst + "<PENDAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</PENDAMT>";
                                            Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BG.BILLMST.BILLSTATUS.Trim() + "</BILLSTATUS>";
                                            Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst + "</spe_inst>";
                                            Xml_BillMst = Xml_BillMst + "<Manualbillno>" + BG.BILLMST.manualbillno + "</Manualbillno>";
                                            Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + BG.BILLMST.TRN_MOD + "</TRN_Mod>";
                                            Xml_BillMst = Xml_BillMst + "<Businesstype>" + BG.BILLMST.Businesstype + "</Businesstype>";
                                            Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                                            Xml_BillMst = Xml_BillMst + "<Empcd>SYSGEN</Empcd>";
                                            Xml_BillMst = Xml_BillMst + "<STax_Y_N>" + BG.BILLMST.STax_Y_N + "</STax_Y_N>";
                                            Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                                            Xml_BillMst = Xml_BillMst + "<cess_rate>0</cess_rate>";
                                            Xml_BillMst = Xml_BillMst + "<H_cess_rate>0</H_cess_rate>";
                                            Xml_BillMst = Xml_BillMst + "<SBCRATE>0</SBCRATE>";
                                            Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>C003</COMPANY_CODE>";
                                            Xml_BillMst = Xml_BillMst + "<AutoBillGenerate>Y</AutoBillGenerate>";
                                            Xml_BillMst = Xml_BillMst + "</Bill></root>";

                                            string Xml_BillDet = "<root>";
                                            foreach (var Itm in CustomerGSTTypeStateWiseDocketList.ToList())
                                            {
                                                Xml_BillDet = Xml_BillDet + "<Det>";
                                                Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.dockno.Trim() + "</DOCKNO>";
                                                Xml_BillDet = Xml_BillDet + "</Det> ";
                                            }
                                            Xml_BillDet = Xml_BillDet + "</root>";
                                            DataTable DT = FS.BillSubmit(custItem.billgen_loccode, Xml_BillMst, Xml_BillDet, BaseFinYear, BG.BILLMST.Businesstype, BG.BILLMST.PAYBAS, true, fincmplBRStateItem);
                                            /*Bill Generation Ended*/
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            var listOfDates = CustomerWiseDocketList.Select(c => c.dockdt).Distinct().ToList();
                            foreach (var dateItem in listOfDates)
                            {
                                List<BillDocket> CustomerWiseDateWiseDocketList = new List<BillDocket>();
                                CustomerWiseDateWiseDocketList = CustomerWiseDocketList.Where(c => c.dockdt == dateItem).ToList(); // Cutomer wise Date waise Docket List

                                var listGSTType = CustomerWiseDateWiseDocketList.Select(c => c.GSTType).Distinct().ToList();
                                foreach (var gstTypeItem in listGSTType) // List of GST Type in Docketlist
                                {
                                    List<BillDocket> CustomerGSTTypeWiseDocketList = new List<BillDocket>();
                                    CustomerGSTTypeWiseDocketList = CustomerWiseDocketList.Where(c => c.GSTType == "I"/*gstTypeItem*/).ToList();
                                    var listFincmplBRState = CustomerGSTTypeWiseDocketList.Select(c => c.fincmplbrState).Distinct().ToList();
                                    foreach (var fincmplBRStateItem in listFincmplBRState) // List of State in Docketlist
                                    {
                                        List<BillDocket> CustomerGSTTypeStateWiseDocketList = new List<BillDocket>();
                                        CustomerGSTTypeStateWiseDocketList = CustomerGSTTypeWiseDocketList.Where(c => c.fincmplbrState == fincmplBRStateItem).ToList();

                                        /*Bill Generation Started*/
                                        BillGeneration BG = new BillGeneration();
                                        BG.BILLMST = new webx_BILLMST();
                                        BG.CUSTHDR = new BillCustDetails();
                                        BG.BILLMST.PAYBAS = "2";
                                        BG.BILLMST.STax_Y_N = "N";
                                        BG.BILLMST.svctax_rate = 0;
                                        BG.BILLMST.Businesstype = "";
                                        BG.BILLMST.BILLSTATUS = "BILL GENERATED";
                                        BG.BILLMST.TRN_MOD = "";
                                        BG.BILLMST.REMARK = "AUTO BILL GENERATED ON " + GF.FormateDate(System.DateTime.Now) + " AT " + System.DateTime.Now.TimeOfDay.ToString();
                                        BG.BILLMST.spe_inst = "";
                                        BG.CUSTHDR = FS.GetBillCustDetails(custItem.CUSTCD).FirstOrDefault();

                                        string Xml_BillMst = "<root><Bill>";
                                        Xml_BillMst = Xml_BillMst + "<Bgndt>" + GF.FormateDate(System.DateTime.Now) + "</Bgndt>";
                                        Xml_BillMst = Xml_BillMst + "<Bduedt>" + GF.FormateDate(System.DateTime.Now.AddDays(Convert.ToInt32(BG.CUSTHDR.credit_day))) + "</Bduedt>";
                                        Xml_BillMst = Xml_BillMst + "<Bbrcd>" + custItem.billgen_loccode + "</Bbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<billsubbrcd>" + BG.CUSTHDR.billsub_loccode + "</billsubbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<billcolbrcd>" + BG.CUSTHDR.billcol_loccode + "</billcolbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<Brnm>" + custItem.billgen_loccode + "</Brnm>";
                                        Xml_BillMst = Xml_BillMst + "<Paybas>" + BG.BILLMST.PAYBAS + "</Paybas>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSCD>" + custItem.CUSTCD + "</PTMSCD>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.CUSTHDR.custnm + "</PTMSNM>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.CUSTHDR.telno + "</PTMSTEL>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.CUSTHDR.emailids + "</PTMSEMAIL>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.CUSTHDR.custaddress.ReplaceSpecialCharacters() + "</PTMSADDR>";
                                        Xml_BillMst = Xml_BillMst + "<remarks>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters().Trim() + "</remarks>";
                                        Xml_BillMst = Xml_BillMst + "<BILLAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</BILLAMT>";
                                        Xml_BillMst = Xml_BillMst + "<PENDAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</PENDAMT>";
                                        Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BG.BILLMST.BILLSTATUS.Trim() + "</BILLSTATUS>";
                                        Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst + "</spe_inst>";
                                        Xml_BillMst = Xml_BillMst + "<Manualbillno>" + BG.BILLMST.manualbillno + "</Manualbillno>";
                                        Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + BG.BILLMST.TRN_MOD + "</TRN_Mod>";
                                        Xml_BillMst = Xml_BillMst + "<Businesstype>" + BG.BILLMST.Businesstype + "</Businesstype>";
                                        Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                                        Xml_BillMst = Xml_BillMst + "<Empcd>SYSGEN</Empcd>";
                                        Xml_BillMst = Xml_BillMst + "<STax_Y_N>" + BG.BILLMST.STax_Y_N + "</STax_Y_N>";
                                        Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                                        Xml_BillMst = Xml_BillMst + "<cess_rate>0</cess_rate>";
                                        Xml_BillMst = Xml_BillMst + "<H_cess_rate>0</H_cess_rate>";
                                        Xml_BillMst = Xml_BillMst + "<SBCRATE>0</SBCRATE>";
                                        Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>C003</COMPANY_CODE>";
                                        Xml_BillMst = Xml_BillMst + "<AutoBillGenerate>Y</AutoBillGenerate>";
                                        Xml_BillMst = Xml_BillMst + "</Bill></root>";

                                        string Xml_BillDet = "<root>";
                                        foreach (var Itm in CustomerGSTTypeStateWiseDocketList.ToList())
                                        {
                                            Xml_BillDet = Xml_BillDet + "<Det>";
                                            Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.dockno.Trim() + "</DOCKNO>";
                                            Xml_BillDet = Xml_BillDet + "</Det> ";
                                        }
                                        Xml_BillDet = Xml_BillDet + "</root>";
                                        //DataTable DT = FS.BillSubmit(custItem.billgen_loccode, Xml_BillMst, Xml_BillDet, BaseFinYear, BG.BILLMST.Businesstype, BG.BILLMST.PAYBAS, true, fincmplBRStateItem);
                                        /*Bill Generation Ended*/
                                    }
                                }
                            }
                        }
                    }
                    else if (custItem.BillGenType == 2) //DOCKET WISE
                    {

                    }
                    else if (custItem.BillGenType == 3) // CYCLE WISE
                    {
                        if (custItem.ModeWise == true) // MODE WISE
                        {
                            var listTRN_MOD = CustomerWiseDocketList.Select(c => c.trn_mod).Distinct().ToList();
                            foreach (var trn_modItem in listTRN_MOD) // List of Trans Mode in Docketlist
                            {
                                List<BillDocket> CustomerTRN_ModWiseDocketList = new List<BillDocket>();
                                CustomerTRN_ModWiseDocketList = CustomerWiseDocketList.Where(c => c.trn_mod == "4"/*trn_modItem*/).ToList();

                                var listGSTType = CustomerTRN_ModWiseDocketList.Select(c => c.GSTType).Distinct().ToList();
                                foreach (var gstTypeItem in listGSTType) // List of GST Type in Docketlist
                                {
                                    List<BillDocket> CustomerGSTTypeWiseDocketList = new List<BillDocket>();
                                    CustomerGSTTypeWiseDocketList = CustomerTRN_ModWiseDocketList.Where(c => c.GSTType == "I"/*gstTypeItem*/).ToList();
                                    var listFincmplBRState = CustomerGSTTypeWiseDocketList.Select(c => c.fincmplbrState).Distinct().ToList();
                                    foreach (var fincmplBRStateItem in listFincmplBRState) // List of State in Docketlist
                                    {
                                        List<BillDocket> CustomerGSTTypeStateWiseDocketList = new List<BillDocket>();
                                        CustomerGSTTypeStateWiseDocketList = CustomerGSTTypeWiseDocketList.Where(c => c.fincmplbrState == fincmplBRStateItem).ToList();

                                        /*Bill Generation Started*/
                                        BillGeneration BG = new BillGeneration();
                                        BG.BILLMST = new webx_BILLMST();
                                        BG.CUSTHDR = new BillCustDetails();
                                        BG.BILLMST.PAYBAS = "2";
                                        BG.BILLMST.STax_Y_N = "N";
                                        BG.BILLMST.svctax_rate = 0;
                                        BG.BILLMST.Businesstype = "";
                                        BG.BILLMST.BILLSTATUS = "BILL GENERATED";
                                        BG.BILLMST.TRN_MOD = "";
                                        BG.BILLMST.REMARK = "AUTO BILL GENERATED ON " + GF.FormateDate(System.DateTime.Now) + " AT " + System.DateTime.Now.TimeOfDay.ToString();
                                        BG.BILLMST.spe_inst = "";
                                        BG.CUSTHDR = FS.GetBillCustDetails(custItem.CUSTCD).FirstOrDefault();

                                        string Xml_BillMst = "<root><Bill>";
                                        Xml_BillMst = Xml_BillMst + "<Bgndt>" + GF.FormateDate(System.DateTime.Now) + "</Bgndt>";
                                        Xml_BillMst = Xml_BillMst + "<Bduedt>" + GF.FormateDate(System.DateTime.Now.AddDays(Convert.ToInt32(BG.CUSTHDR.credit_day))) + "</Bduedt>";
                                        Xml_BillMst = Xml_BillMst + "<Bbrcd>" + custItem.billgen_loccode + "</Bbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<billsubbrcd>" + BG.CUSTHDR.billsub_loccode + "</billsubbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<billcolbrcd>" + BG.CUSTHDR.billcol_loccode + "</billcolbrcd>";
                                        Xml_BillMst = Xml_BillMst + "<Brnm>" + custItem.billgen_loccode + "</Brnm>";
                                        Xml_BillMst = Xml_BillMst + "<Paybas>" + BG.BILLMST.PAYBAS + "</Paybas>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSCD>" + custItem.CUSTCD + "</PTMSCD>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.CUSTHDR.custnm + "</PTMSNM>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.CUSTHDR.telno + "</PTMSTEL>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.CUSTHDR.emailids + "</PTMSEMAIL>";
                                        Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.CUSTHDR.custaddress.ReplaceSpecialCharacters() + "</PTMSADDR>";
                                        Xml_BillMst = Xml_BillMst + "<remarks>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters().Trim() + "</remarks>";
                                        Xml_BillMst = Xml_BillMst + "<BILLAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</BILLAMT>";
                                        Xml_BillMst = Xml_BillMst + "<PENDAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</PENDAMT>";
                                        Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BG.BILLMST.BILLSTATUS.Trim() + "</BILLSTATUS>";
                                        Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst + "</spe_inst>";
                                        Xml_BillMst = Xml_BillMst + "<Manualbillno>" + BG.BILLMST.manualbillno + "</Manualbillno>";
                                        Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + BG.BILLMST.TRN_MOD + "</TRN_Mod>";
                                        Xml_BillMst = Xml_BillMst + "<Businesstype>" + BG.BILLMST.Businesstype + "</Businesstype>";
                                        Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                                        Xml_BillMst = Xml_BillMst + "<Empcd>SYSGEN</Empcd>";
                                        Xml_BillMst = Xml_BillMst + "<STax_Y_N>" + BG.BILLMST.STax_Y_N + "</STax_Y_N>";
                                        Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                                        Xml_BillMst = Xml_BillMst + "<cess_rate>0</cess_rate>";
                                        Xml_BillMst = Xml_BillMst + "<H_cess_rate>0</H_cess_rate>";
                                        Xml_BillMst = Xml_BillMst + "<SBCRATE>0</SBCRATE>";
                                        Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>C003</COMPANY_CODE>";
                                        Xml_BillMst = Xml_BillMst + "<AutoBillGenerate>Y</AutoBillGenerate>";
                                        Xml_BillMst = Xml_BillMst + "</Bill></root>";

                                        string Xml_BillDet = "<root>";
                                        foreach (var Itm in CustomerGSTTypeStateWiseDocketList.ToList())
                                        {
                                            Xml_BillDet = Xml_BillDet + "<Det>";
                                            Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.dockno.Trim() + "</DOCKNO>";
                                            Xml_BillDet = Xml_BillDet + "</Det> ";
                                        }
                                        Xml_BillDet = Xml_BillDet + "</root>";
                                        DataTable DT = FS.BillSubmit(custItem.billgen_loccode, Xml_BillMst, Xml_BillDet, BaseFinYear, BG.BILLMST.Businesstype, BG.BILLMST.PAYBAS, true, fincmplBRStateItem);
                                        /*Bill Generation Ended*/
                                    }
                                }
                            }
                        }
                        else
                        {
                            var listGSTType = CustomerWiseDocketList.Select(c => c.GSTType).Distinct().ToList();
                            foreach (var gstTypeItem in listGSTType) // List of GST Type in Docketlist
                            {
                                List<BillDocket> CustomerGSTTypeWiseDocketList = new List<BillDocket>();
                                CustomerGSTTypeWiseDocketList = CustomerWiseDocketList.Where(c => c.GSTType == "I"/*gstTypeItem*/).ToList();
                                var listFincmplBRState = CustomerGSTTypeWiseDocketList.Select(c => c.fincmplbrState).Distinct().ToList();
                                foreach (var fincmplBRStateItem in listFincmplBRState) // List of State in Docketlist
                                {
                                    List<BillDocket> CustomerGSTTypeStateWiseDocketList = new List<BillDocket>();
                                    CustomerGSTTypeStateWiseDocketList = CustomerGSTTypeWiseDocketList.Where(c => c.fincmplbrState == fincmplBRStateItem).ToList();

                                    /*Bill Generation Started*/
                                    BillGeneration BG = new BillGeneration();
                                    BG.BILLMST = new webx_BILLMST();
                                    BG.CUSTHDR = new BillCustDetails();
                                    BG.BILLMST.PAYBAS = "2";
                                    BG.BILLMST.STax_Y_N = "N";
                                    BG.BILLMST.svctax_rate = 0;
                                    BG.BILLMST.Businesstype = "";
                                    BG.BILLMST.BILLSTATUS = "BILL GENERATED";
                                    BG.BILLMST.TRN_MOD = "";
                                    BG.BILLMST.REMARK = "AUTO BILL GENERATED ON " + GF.FormateDate(System.DateTime.Now) + " AT " + System.DateTime.Now.TimeOfDay.ToString();
                                    BG.BILLMST.spe_inst = "";
                                    BG.CUSTHDR = FS.GetBillCustDetails(custItem.CUSTCD).FirstOrDefault();

                                    string Xml_BillMst = "<root><Bill>";
                                    Xml_BillMst = Xml_BillMst + "<Bgndt>" + GF.FormateDate(System.DateTime.Now) + "</Bgndt>";
                                    Xml_BillMst = Xml_BillMst + "<Bduedt>" + GF.FormateDate(System.DateTime.Now.AddDays(Convert.ToInt32(BG.CUSTHDR.credit_day))) + "</Bduedt>";
                                    Xml_BillMst = Xml_BillMst + "<Bbrcd>" + custItem.billgen_loccode + "</Bbrcd>";
                                    Xml_BillMst = Xml_BillMst + "<billsubbrcd>" + BG.CUSTHDR.billsub_loccode + "</billsubbrcd>";
                                    Xml_BillMst = Xml_BillMst + "<billcolbrcd>" + BG.CUSTHDR.billcol_loccode + "</billcolbrcd>";
                                    Xml_BillMst = Xml_BillMst + "<Brnm>" + custItem.billgen_loccode + "</Brnm>";
                                    Xml_BillMst = Xml_BillMst + "<Paybas>" + BG.BILLMST.PAYBAS + "</Paybas>";
                                    Xml_BillMst = Xml_BillMst + "<PTMSCD>" + custItem.CUSTCD + "</PTMSCD>";
                                    Xml_BillMst = Xml_BillMst + "<PTMSNM>" + BG.CUSTHDR.custnm + "</PTMSNM>";
                                    Xml_BillMst = Xml_BillMst + "<PTMSTEL>" + BG.CUSTHDR.telno + "</PTMSTEL>";
                                    Xml_BillMst = Xml_BillMst + "<PTMSEMAIL>" + BG.CUSTHDR.emailids + "</PTMSEMAIL>";
                                    Xml_BillMst = Xml_BillMst + "<PTMSADDR>" + BG.CUSTHDR.custaddress.ReplaceSpecialCharacters() + "</PTMSADDR>";
                                    Xml_BillMst = Xml_BillMst + "<remarks>" + BG.BILLMST.REMARK.ReplaceSpecialCharacters().Trim() + "</remarks>";
                                    Xml_BillMst = Xml_BillMst + "<BILLAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</BILLAMT>";
                                    Xml_BillMst = Xml_BillMst + "<PENDAMT>" + CustomerGSTTypeStateWiseDocketList.Sum(c => c.dkttot) + "</PENDAMT>";
                                    Xml_BillMst = Xml_BillMst + "<BILLSTATUS>" + BG.BILLMST.BILLSTATUS.Trim() + "</BILLSTATUS>";
                                    Xml_BillMst = Xml_BillMst + "<spe_inst>" + BG.BILLMST.spe_inst + "</spe_inst>";
                                    Xml_BillMst = Xml_BillMst + "<Manualbillno>" + BG.BILLMST.manualbillno + "</Manualbillno>";
                                    Xml_BillMst = Xml_BillMst + "<TRN_Mod>" + BG.BILLMST.TRN_MOD + "</TRN_Mod>";
                                    Xml_BillMst = Xml_BillMst + "<Businesstype>" + BG.BILLMST.Businesstype + "</Businesstype>";
                                    Xml_BillMst = Xml_BillMst + "<BillGen_Loc_BasedOn>" + BG.BillLocation + "</BillGen_Loc_BasedOn>";
                                    Xml_BillMst = Xml_BillMst + "<Empcd>SYSGEN</Empcd>";
                                    Xml_BillMst = Xml_BillMst + "<STax_Y_N>" + BG.BILLMST.STax_Y_N + "</STax_Y_N>";
                                    Xml_BillMst = Xml_BillMst + "<STax_Rate>" + BG.BILLMST.svctax_rate + "</STax_Rate>";
                                    Xml_BillMst = Xml_BillMst + "<cess_rate>0</cess_rate>";
                                    Xml_BillMst = Xml_BillMst + "<H_cess_rate>0</H_cess_rate>";
                                    Xml_BillMst = Xml_BillMst + "<SBCRATE>0</SBCRATE>";
                                    Xml_BillMst = Xml_BillMst + "<COMPANY_CODE>C003</COMPANY_CODE>";
                                    Xml_BillMst = Xml_BillMst + "<AutoBillGenerate>Y</AutoBillGenerate>";
                                    Xml_BillMst = Xml_BillMst + "</Bill></root>";

                                    string Xml_BillDet = "<root>";
                                    foreach (var Itm in CustomerGSTTypeStateWiseDocketList.ToList())
                                    {
                                        Xml_BillDet = Xml_BillDet + "<Det>";
                                        Xml_BillDet = Xml_BillDet + "<DOCKNO>" + Itm.dockno.Trim() + "</DOCKNO>";
                                        Xml_BillDet = Xml_BillDet + "</Det> ";
                                    }
                                    Xml_BillDet = Xml_BillDet + "</root>";
                                    DataTable DT = FS.BillSubmit(custItem.billgen_loccode, Xml_BillMst, Xml_BillDet, BaseFinYear, BG.BILLMST.Businesstype, BG.BILLMST.PAYBAS, true, fincmplBRStateItem);
                                    /*Bill Generation Ended*/
                                }
                            }
                        }
                    }
                }
            }
            return View();
        }

        public ActionResult BillDone(string BILLNO, string TranXaction)
        {
            ViewBag.BILLNO = BILLNO;
            ViewBag.TranXaction = TranXaction;
            BillingQueryModel BQM = TempData.Peek("BMQ") as BillingQueryModel;
            return View(BQM);
        }

        public JsonResult GetModeWiseStax()
        {

            List<Webx_Master_General> listitm = new List<Webx_Master_General>();

            Webx_Master_General GM = new Webx_Master_General();

            GM.CodeId = "ALL";
            GM.CodeDesc = "ALL With W/O Stax";
            listitm.Add(GM);

            foreach (var itm in FS.GetModeWiseStax())
                listitm.Add(itm);



            var ItemList = (from e in listitm
                            select new
                            {
                                Value = e.CodeId,
                                Text = e.CodeDesc,
                            }).Distinct().ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModeWiseStaxwithType()
        {

            List<Webx_Master_General> listitm = new List<Webx_Master_General>();

            Webx_Master_General GM = new Webx_Master_General();

            GM.CodeId = "ALL";
            GM.CodeDesc = "ALL With W/O Stax";
            listitm.Add(GM);

            //foreach (var itm in FS.GetModeWiseStax())
            foreach (var itm in FS.GetModeWiseStaxwithType())
                listitm.Add(itm);



            var ItemList = (from e in listitm
                            select new
                            {
                                Value = e.CodeId,
                                Text = e.CodeDesc,
                            }).Distinct().ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Supplementary Bill

        public ActionResult SupplementaryBillCriteria()
        {
            Webx_Account_Details WAD = new Webx_Account_Details();
            return View(WAD);
        }


        public ActionResult SupplementaryBill(Webx_Account_Details objWAD)
        {
            try
            {
                BillGeneration BG = new BillGeneration();
                BG.BILLMST = new webx_BILLMST();
                BG.BILLMST.BBRCD = BaseLocationCode;
                BG.BILLDET = new List<webx_BILLDET>();
                webx_BILLDET WBD = new webx_BILLDET();
                WBD.Id = 1;
                BG.BILLDET.Add(WBD);
                BG.objWAD = new Webx_Account_Details();
                BG.objWAD = objWAD;
                return View(BG);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult SupplementaryBillSubmit(BillGeneration BG, string DOCTY, List<webx_BILLDET> LedgerList, List<CygnusChargesHeader> DynamicList, StaxTDSViewModel objStaxTDSViewModel)
        {

            string MultiLrThcFlag = "N", svctaxFlag = "N", PTMSNM = "", PTMSTEL = "", PTMSEMAIL = "";

            DataTable Dt_Cust = FS.CustomerDetails(BG.BILLMST.PTMSCD);

            if (Dt_Cust.Rows.Count > 0)
            {
                PTMSNM = Dt_Cust.Rows[0]["custnm"].ToString().Replace("\n", "");
                PTMSTEL = Dt_Cust.Rows[0]["cstno"].ToString().Replace("\n", "");
                PTMSEMAIL = Dt_Cust.Rows[0]["emailids"].ToString().Replace("\n", "");
            }
            else
            {
                ViewBag.StrError = "Error : Customer Code (" + BG.BILLMST.PTMSCD + ") Not Exists In The System";
                return View("Error");
            }

            if (BG.IsMultipleDoc)
                MultiLrThcFlag = "Y";

            if (BG.IsApplyStax)
                svctaxFlag = "Y";

            string Xml_Other_Details = "<root><Other>";
            Xml_Other_Details += "<Finyear>" + BaseFinYear + "</Finyear>";
            Xml_Other_Details += "<BGNDT>" + GF.FormateDate(BG.BILLMST.BGNDT) + "</BGNDT>";
            Xml_Other_Details += "<BDUEDT>" + GF.FormateDate(BG.BILLMST.BDUEDT) + "</BDUEDT>";
            Xml_Other_Details += "<BBRCD>" + BaseLocationCode + "</BBRCD>";
            Xml_Other_Details += "<BBRNM>" + BaseLocationName + "</BBRNM>";
            Xml_Other_Details += "<PTMSCD>" + BG.BILLMST.PTMSCD + "</PTMSCD>";
            Xml_Other_Details += "<PTMSNM>" + PTMSNM.Trim() + "</PTMSNM>";
            Xml_Other_Details += "<PTMSTEL>" + PTMSTEL.Trim() + "</PTMSTEL>";
            Xml_Other_Details += "<PTMSEMAIL>" + PTMSEMAIL.Trim() + "</PTMSEMAIL>";
            //Xml_Other_Details += "<PTMSADDR>" + PTMSADDR.Trim() + "</PTMSADDR>";
            Xml_Other_Details += "<BILLAMT>" + BG.BILLMST.BILLAMT + "</BILLAMT>";
            Xml_Other_Details += "<BILLSUBTO></BILLSUBTO>";
            Xml_Other_Details += "<SUBTOTEL>" + objStaxTDSViewModel.StaxOnAmount + "</SUBTOTEL>";
            Xml_Other_Details += "<bgenempcd>" + BaseUserName.ToUpper() + "</bgenempcd>";
            Xml_Other_Details += "<ptmsbrcd>" + BaseLocationCode + "</ptmsbrcd>";
            Xml_Other_Details += "<billsubbrcd>" + BG.BILLMST.BILLSUBBRCD + "</billsubbrcd>";
            Xml_Other_Details += "<billcolbrcd>" + BG.BILLMST.BILLCOLBRCD + "</billcolbrcd>";
            Xml_Other_Details += "<billcolptcd></billcolptcd>";
            Xml_Other_Details += "<manualbillno>" + BG.BILLMST.manualbillno + "</manualbillno>";
            Xml_Other_Details += "<svrcamt>" + BG.BILLMST.SVRCAMT + "</svrcamt>";
            Xml_Other_Details += "<cessamt>" + BG.BILLMST.CESSAMT + "</cessamt>";
            //Xml_Other_Details += "<bdatefrom>" + fn.Mydate1(txtDateFrom.Text).Trim() + "</bdatefrom>";
            //Xml_Other_Details += "<bdateto>" + fn.Mydate1(txttodate.Text).Trim() + "</bdateto>";
            Xml_Other_Details += "<AggrementNo>" + BG.BILLMST.AggrementNo + "</AggrementNo>";
            Xml_Other_Details += "<Aggrementdt>" + GF.FormateDate(BG.BILLMST.Aggrementdt) + "</Aggrementdt>";
            Xml_Other_Details += "<svctax_rate>" + BG.BILLMST.svctax_rate + "</svctax_rate>";
            Xml_Other_Details += "<cess_rate>" + BG.BILLMST.cess_rate + "</cess_rate>";
            Xml_Other_Details += "<H_cess_rate>" + BG.BILLMST.H_cess_rate + "</H_cess_rate>";
            Xml_Other_Details += "<hedu_cess>" + BG.BILLMST.hedu_cess + "</hedu_cess>";
            Xml_Other_Details += "<SBC>" + BG.BILLMST.SBCAMT + "</SBC>";
            Xml_Other_Details += "<SBCRate>" + BG.BILLMST.SBCRATE + "</SBCRate>";
            Xml_Other_Details += "<KKC>" + BG.BILLMST.KKCAMT + "</KKC>";
            Xml_Other_Details += "<KKCRate>" + BG.BILLMST.KKCRATE + "</KKCRate>";
            Xml_Other_Details += "<Narration>" + BG.BILLMST.REMARK + "</Narration>";
            Xml_Other_Details += "<MultiLrThcFlag>" + MultiLrThcFlag.Trim() + "</MultiLrThcFlag>";
            Xml_Other_Details += "<rad_dkt_flag>N</rad_dkt_flag>";
            Xml_Other_Details += "<rad_thc_flag>N</rad_thc_flag>";
            Xml_Other_Details += "<dockno>" + BG.BILLMST.Dockno + "</dockno>";
            Xml_Other_Details += "<doctype>" + DOCTY.Trim() + "</doctype>";
            Xml_Other_Details += "<RoundOff>" + BG.BILLMST.RoundOffAmt + "</RoundOff>";
            Xml_Other_Details += "<svctaxFlag>" + svctaxFlag.Trim() + "</svctaxFlag>";
            Xml_Other_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
            //Xml_Other_Details += "<GSTType>" + BG.objWAD.GSTType.Trim() + "</GSTType>";
            //Xml_Other_Details += "<StateCode>" + BG.objWAD.StateCode.Trim() + "</StateCode>";
            Xml_Other_Details += "</Other></root>";

            string Xml_MultiLrThc_Details = "<root>";

            foreach (var itm in LedgerList)
            {
                Xml_MultiLrThc_Details += "<LrThc>";
                Xml_MultiLrThc_Details += "<dockno>" + itm.DOCKNO + "</dockno>";
                Xml_MultiLrThc_Details += "<doctype>" + itm.DocType + "</doctype>";
                Xml_MultiLrThc_Details += "<Acccode>" + itm.Acccode + "</Acccode>";
                Xml_MultiLrThc_Details += "<Accdesc>" + itm.Accdesc + "</Accdesc>";
                Xml_MultiLrThc_Details += "<totamt>" + itm.totamt + "</totamt>";
                Xml_MultiLrThc_Details += "<Narration>" + itm.Narration + "</Narration>";
                Xml_MultiLrThc_Details += "</LrThc>";
            }

            Xml_MultiLrThc_Details += "</root>";

            /* START GST Changes On 26 June 2017 By Chirag D */
            //XmlDocument xmlDocGSTCha = new XmlDocument();
            //if (DynamicList != null)
            //{
            //    XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
            //    using (MemoryStream xmlStreamCha = new MemoryStream())
            //    {
            //        xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
            //        xmlStreamCha.Position = 0;
            //        xmlDocGSTCha.Load(xmlStreamCha);
            //    }
            //}
            //else
            //{
            //    xmlDocGSTCha.InnerXml = "<root></root>";
            //}

            /* END GST Changes On 26 June 2017 By Chirag D */

            //return View();
            SqlConnection conn = new SqlConnection(GF.GetConnstr());
            conn.Open();

            SqlTransaction trans;
            trans = conn.BeginTransaction();
            try
            {
                //DataTable DT = FS.InsertSupBillDetails(Xml_MultiLrThc_Details.ReplaceSpecialCharacters(), Xml_Other_Details, xmlDocGSTCha.InnerXml, trans);
                DataTable DT = FS.InsertSupBillDetails(Xml_MultiLrThc_Details.ReplaceSpecialCharacters(), Xml_Other_Details, trans);
                trans.Commit();
                conn.Close();
                return RedirectToAction("SupplementaryBillDone", new { BILLNO = DT.Rows[0][0].ToString() });

            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                trans.Rollback();
                ViewBag.StrError = e1.Message.ToString();
                return View("Error");
                //Response.Redirect("../../../ErrorPage.aspx?PageHead=" + "Transaction Error" + "&ErrorMsg=" + ErrorMsg);
                //Response.End();
            }
        }

        public JsonResult GetServiceTax(DateTime DOCKDT)
        {

            TaxRate trate = new TaxRate();
            trate.Date = DOCKDT;
            trate = OS.GetTaxObject(trate);

            //double StaxRebateRate = OS.GetRebateRate(DOCKDT);
            double StaxRebateRate = 0.00;

            return new JsonResult()
            {
                Data = new
                {
                    ServiceTaxRate = trate.ServiceTaxRate,

                    CessRate = trate.EduCessRate,

                    HCessRate = trate.HEduCessRate,

                    SBRate = trate.SbcRate,

                    StaxRebateRate = StaxRebateRate,
                    KKCRate = trate.KKCRate,

                }
            };
        }

        public ActionResult SupplementaryBillDone(string BILLNO)
        {
            ViewBag.BILLNO = BILLNO;
            return View();
        }

        public ActionResult LedgerDetails(int id)
        {

            webx_BILLDET WMDI = new webx_BILLDET();
            WMDI.Id = id;

            return PartialView("_SupplementaryBill", WMDI);
        }

        public ActionResult FleetIncomeBill(Webx_Account_Details objWAD)
        {
            try
            {
                BillGeneration BG = new BillGeneration();
                BG.BILLMST = new webx_BILLMST();
                BG.BILLMST.BBRCD = BaseLocationCode;
                BG.BILLDET = new List<webx_BILLDET>();
                webx_BILLDET WBD = new webx_BILLDET();
                WBD.Id = 1;
                BG.BILLDET.Add(WBD);
                BG.objWAD = new Webx_Account_Details();
                BG.objWAD = objWAD;
                return View(BG);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
        }

        public ActionResult FleetIncomeDet(int id)
        {

            webx_BILLDET WMDI = new webx_BILLDET();
            WMDI.Id = id;

            return PartialView("_FleetIncomeBill", WMDI);
        }

        [HttpPost]
        public ActionResult FleetIncomeBillSubmit(BillGeneration BG, string DOCTY, List<webx_BILLDET> BillDetailList, List<CygnusChargesHeader> DynamicList, StaxTDSViewModel objStaxTDSViewModel)
        {

            string svctaxFlag = "N", PTMSNM = "", PTMSTEL = "", PTMSEMAIL = "";

            DataTable Dt_Cust = FS.CustomerDetails(BG.BILLMST.PTMSCD);

            if (Dt_Cust.Rows.Count > 0)
            {
                PTMSNM = Dt_Cust.Rows[0]["custnm"].ToString().Replace("\n", "");
                PTMSTEL = Dt_Cust.Rows[0]["cstno"].ToString().Replace("\n", "");
                PTMSEMAIL = Dt_Cust.Rows[0]["emailids"].ToString().Replace("\n", "");
            }
            else
            {
                ViewBag.StrError = "Error : Customer Code (" + BG.BILLMST.PTMSCD + ") Not Exists In The System";
                return View("Error");
            }


            if (BG.IsApplyStax)
                svctaxFlag = "Y";

            string Xml_Other_Details = "<root><Other>";
            Xml_Other_Details += "<Finyear>" + BaseFinYear + "</Finyear>";
            Xml_Other_Details += "<BGNDT>" + GF.FormateDate(BG.BILLMST.BGNDT) + "</BGNDT>";
            Xml_Other_Details += "<BDUEDT>" + GF.FormateDate(BG.BILLMST.BDUEDT) + "</BDUEDT>";
            Xml_Other_Details += "<BBRCD>" + BaseLocationCode + "</BBRCD>";
            Xml_Other_Details += "<BBRNM>" + BaseLocationName + "</BBRNM>";
            Xml_Other_Details += "<PTMSCD>" + BG.BILLMST.PTMSCD + "</PTMSCD>";
            Xml_Other_Details += "<PTMSNM>" + PTMSNM.Trim() + "</PTMSNM>";
            Xml_Other_Details += "<PTMSTEL>" + PTMSTEL.Trim() + "</PTMSTEL>";
            Xml_Other_Details += "<PTMSEMAIL>" + PTMSEMAIL.Trim() + "</PTMSEMAIL>";
            //Xml_Other_Details += "<PTMSADDR>" + PTMSADDR.Trim() + "</PTMSADDR>";
            Xml_Other_Details += "<BILLAMT>" + BG.BILLMST.BILLAMT + "</BILLAMT>";
            Xml_Other_Details += "<BILLSUBTO></BILLSUBTO>";
            Xml_Other_Details += "<SUBTOTEL>" + objStaxTDSViewModel.StaxOnAmount + "</SUBTOTEL>";
            Xml_Other_Details += "<bgenempcd>" + BaseUserName.ToUpper() + "</bgenempcd>";
            Xml_Other_Details += "<ptmsbrcd>" + BaseLocationCode + "</ptmsbrcd>";
            Xml_Other_Details += "<billsubbrcd>" + BG.BILLMST.BILLSUBBRCD + "</billsubbrcd>";
            Xml_Other_Details += "<billcolbrcd>" + BG.BILLMST.BILLCOLBRCD + "</billcolbrcd>";
            Xml_Other_Details += "<billcolptcd></billcolptcd>";
            Xml_Other_Details += "<manualbillno>" + BG.BILLMST.manualbillno + "</manualbillno>";
            Xml_Other_Details += "<svrcamt>" + BG.BILLMST.SVRCAMT + "</svrcamt>";
            Xml_Other_Details += "<cessamt>" + BG.BILLMST.CESSAMT + "</cessamt>";
            //Xml_Other_Details += "<bdatefrom>" + fn.Mydate1(txtDateFrom.Text).Trim() + "</bdatefrom>";
            //Xml_Other_Details += "<bdateto>" + fn.Mydate1(txttodate.Text).Trim() + "</bdateto>";
            Xml_Other_Details += "<AggrementNo>" + BG.BILLMST.AggrementNo + "</AggrementNo>";
            Xml_Other_Details += "<Aggrementdt>" + GF.FormateDate(BG.BILLMST.Aggrementdt) + "</Aggrementdt>";
            Xml_Other_Details += "<svctax_rate>" + BG.BILLMST.svctax_rate + "</svctax_rate>";
            Xml_Other_Details += "<cess_rate>" + BG.BILLMST.cess_rate + "</cess_rate>";
            Xml_Other_Details += "<H_cess_rate>" + BG.BILLMST.H_cess_rate + "</H_cess_rate>";
            Xml_Other_Details += "<hedu_cess>" + BG.BILLMST.hedu_cess + "</hedu_cess>";
            Xml_Other_Details += "<SBC>" + BG.BILLMST.SBCAMT + "</SBC>";
            Xml_Other_Details += "<SBCRate>" + BG.BILLMST.SBCRATE + "</SBCRate>";
            Xml_Other_Details += "<KKC>" + BG.BILLMST.KKCAMT + "</KKC>";
            Xml_Other_Details += "<KKCRate>" + BG.BILLMST.KKCRATE + "</KKCRate>";
            Xml_Other_Details += "<Narration>" + BG.BILLMST.REMARK + "</Narration>";
            Xml_Other_Details += "<rad_dkt_flag>N</rad_dkt_flag>";
            Xml_Other_Details += "<rad_thc_flag>N</rad_thc_flag>";
            //Xml_Other_Details += "<doctype>" + DOCTY.Trim() + "</doctype>";
            Xml_Other_Details += "<RoundOff>" + BG.BILLMST.RoundOffAmt + "</RoundOff>";
            Xml_Other_Details += "<svctaxFlag>" + svctaxFlag.Trim() + "</svctaxFlag>";
            Xml_Other_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
            Xml_Other_Details += "</Other></root>";

            string Xml_MultiLrThc_Details = "<root>";

            foreach (var itm in BillDetailList)
            {
                Xml_MultiLrThc_Details += "<BillDet>";
                Xml_MultiLrThc_Details += "<TripsheetNo>" + itm.TripsheetNo + "</TripsheetNo>";
                Xml_MultiLrThc_Details += "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                Xml_MultiLrThc_Details += "<THCRoute>" + itm.THCRoute + "</THCRoute>";
                Xml_MultiLrThc_Details += "<THCNo>" + itm.THCNo + "</THCNo>";
                Xml_MultiLrThc_Details += "<totamt>" + itm.totamt + "</totamt>";
                Xml_MultiLrThc_Details += "<THCDate>" + itm.THCDate + "</THCDate>";
                Xml_MultiLrThc_Details += "</BillDet>";
            }

            Xml_MultiLrThc_Details += "</root>";

            try
            {
                DataTable DT = FS.InsertFleetIncomeBillDetails(Xml_MultiLrThc_Details.ReplaceSpecialCharacters(), Xml_Other_Details);
                return RedirectToAction("FleetIncomeBillDone", new { BILLNO = DT.Rows[0][0].ToString() });

            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                ViewBag.StrError = e1.Message.ToString();
                return View("Error");
            }
        }

        public ActionResult FleetIncomeBillDone(string BILLNO)
        {
            ViewBag.BILLNO = BILLNO;
            return View();
        }

        #endregion

        #endregion

        #region CNote / Bill Finalization

        public ActionResult DocketFinalizationCriteria(string id)
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName == "ADMIN")
            //{
            FinalizationModel CNoteFM = new FinalizationModel();
            CNoteFM.Type = id;
            CNoteFM.LocationLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            CNoteFM.BaseFinYear = BaseFinYear.Split('-')[0].ToString();
            CNoteFM.SelectionType = "0";
            CNoteFM.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (CNoteFM.LocationLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CNoteFM.RegionList.Insert(0, objNew);
                CNoteFM.LocationList = new List<webx_location>();
                CNoteFM.LocationList.Insert(0, objNew);
            }
            if (CNoteFM.LocationLevel == 2)
            {
                CNoteFM.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CNoteFM.LocationList.Insert(0, objNew);
            }
            if (CNoteFM.LocationLevel == 3)
            {
                CNoteFM.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }

            return View(CNoteFM);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        [HttpPost]
        public ActionResult DocketFinalizationCriteria(FinalizationModel objCNoteFM)
        {
            return RedirectToAction("DocketFinalization", objCNoteFM);
        }

        public ActionResult DocketFinalization(FinalizationModel objCNoteFM)
        {
            DataTable objDT = new DataTable();
            if (objCNoteFM.DocumentNo != "" && objCNoteFM.DocumentNo != null)
            {
                objCNoteFM.SelectionType = "1";
            }
            string type = "1";
            if (CurrFinYear != BaseFinYear && BaseUserName == "ADMIN")
            {
                type = "2";
            }
            objDT = FS.GetDocketFinalizationDetails(objCNoteFM, type);
            if (objCNoteFM.Type.ToUpper() == "DKT")
            {
                objCNoteFM.ListCNFVM = DataRowToObject.CreateListFromTable<CNoteFinalizationViewModel>(objDT);
            }
            if (objCNoteFM.Type.ToUpper() == "BILL")
            {
                objCNoteFM.ListBillFVM = DataRowToObject.CreateListFromTable<BillFinalizationViewModel>(objDT);
            }
            if (objCNoteFM.Type.ToUpper() == "MR")
            {
                objCNoteFM.ListMrFM = DataRowToObject.CreateListFromTable<MRFinalizationViewModel>(objDT);
            }
            return View(objCNoteFM);
        }

        public JsonResult GetLocationListFromROJson(string LocCode)
        {
            List<webx_location> ListLocations = new List<webx_location>();
            ListLocations = MS.GetRegionList(LocCode, "LO").ToList();
            webx_location objNew = new webx_location();
            objNew.LocCode = "All";
            objNew.LocName = "All";
            ListLocations.Insert(0, objNew);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DocketFinalizationSubmit(FinalizationModel objCNoteFM, List<CNoteFinalizationViewModel> CNoteFinalizationList, List<BillFinalizationViewModel> BillFinalizationList, List<MRFinalizationViewModel> MRFinalizationList)
        {
            string FinalizedDocket = "", FinalizedBill = "", FinalizedMR = "", Bill = "", MRSNos = "";//, DockNo = "", Str = "";
            if (objCNoteFM.Type.ToUpper() == "DKT")
            {
                try
                {
                    if (CNoteFinalizationList.Count() > 0)
                    {
                        foreach (var item in CNoteFinalizationList.Where(c => c.IsChecked == true).ToList())
                        {
                            if (FinalizedDocket == "")
                            {
                                FinalizedDocket = item.LRNo;
                            }
                            else
                            {
                                FinalizedDocket = FinalizedDocket + "," + item.LRNo;
                            }
                            DataTable objDT = new DataTable();
                            objDT = FS.CNoteFinalizationSubmit(item.LRNo, BaseFinYear.Split('-')[0].ToString(), item.Finalized_Date);
                            if (objDT.Rows.Count > 0)
                            {
                                if (objDT.Rows[0][2].ToString() == "1")
                                {
                                    if (Bill == "")
                                    {
                                        Bill = objDT.Rows[0][0].ToString();
                                    }
                                    else
                                    {
                                        Bill = Bill + "," + objDT.Rows[0][0].ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                    return View("Error");
                }
            }
            if (objCNoteFM.Type.ToUpper() == "BILL")
            {
                try
                {

                    FinalizedBill = "<root>";
                    foreach (var item in BillFinalizationList.Where(c => c.IsChecked == true))
                    {
                        FinalizedBill = FinalizedBill + "<DocumentElement>";
                        FinalizedBill = FinalizedBill + "<BillNo>" + item.BillNo + "</BillNo>";
                        FinalizedBill = FinalizedBill + "</DocumentElement>";

                        if (FinalizedDocket == "")
                        {
                            FinalizedDocket = item.BillNo;
                        }
                        else
                        {
                            FinalizedDocket = FinalizedDocket + "," + item.BillNo;
                        }
                    }
                    FinalizedBill = FinalizedBill + "</root>";
                    DataTable objDT = new DataTable();
                    objDT = FS.BillFinalizationSubmit(FinalizedBill, BaseFinYear.Split('-')[0].ToString());

                }
                catch (Exception ex)
                {
                    ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                    return View("Error");
                }
            }

            //MR Finalize
            if (objCNoteFM.Type.ToUpper() == "MR")
            {
                try
                {

                    FinalizedMR = "<root>";
                    foreach (var item in MRFinalizationList.Where(c => c.IsChecked == true))
                    {
                        FinalizedMR = FinalizedMR + "<DocumentElement>";
                        FinalizedMR = FinalizedMR + "<MRSNo>" + item.MRSNO + "</MRSNo>";
                        FinalizedMR = FinalizedMR + "<paymode>" + item.paymode + "</paymode>";
                        FinalizedMR = FinalizedMR + "<BankAcccode>" + item.BankAcccode + "</BankAcccode>";
                        FinalizedMR = FinalizedMR + "</DocumentElement>";

                        if (Bill == "")
                        {
                            Bill = item.MRSNO;
                        }
                        else
                        {
                            Bill = Bill + "," + item.MRSNO;
                        }
                    }
                    FinalizedMR = FinalizedMR + "</root>";
                    DataTable objDT = new DataTable();
                    objDT = FS.MRFinalizationSubmit(FinalizedMR, BaseFinYear.Split('-')[0].ToString());

                }
                catch (Exception ex)
                {
                    ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                    return View("Error");
                }
            }
            return RedirectToAction("DocketFinalizationDone", new { Type = objCNoteFM.Type, FinalizedDocket = FinalizedDocket, Bill = Bill });
        }

        public ActionResult DocketFinalizationDone(string Type, string FinalizedDocket, string Bill)
        {
            ViewBag.Type = Type;
            ViewBag.FinalizedDocket = FinalizedDocket;
            ViewBag.Bill = Bill;
            return View();
        }
        #endregion

        #region Bill/BillEntry Adjustment

        public ActionResult BillEntryAdjustment()
        {
            BillEntryAdjustmentViewModel BEAVM = new BillEntryAdjustmentViewModel();
            BEAVM.PaymentBillEntry = new List<BillEntryModel>();
            BEAVM.IncomeBillEntry = new List<BillEntryModel>();
            return View(BEAVM);
        }

        public ActionResult AddAddBillDetail(int id, string BillType)
        {
            BillEntryModel BEM = new BillEntryModel();
            BEM.SrNo = id;

            if (BillType == "BENO")/*   For Payment  */
            {
                return PartialView("_PaymentBillDetails", BEM);
            }
            else /*   For Income  */
            {
                return PartialView("_IncomeBillDetails", BEM);
            }
        }

        public JsonResult GetBillDetails(string BillNo, string BillType)
        {
            try
            {
                string BillDate = "", Party = "", BillStatus = "", BillCancel = "", Acccode = "";
                decimal BillAmout = 0, PendAmount = 0;
                string Status = "0"; // Invalid Bill No.
                DataTable objPaymentDetails = new DataTable();
                objPaymentDetails = FS.GetPaymentBillDetails(BillNo, BillType);
                if (objPaymentDetails.Rows.Count > 0)
                {
                    BillDate = objPaymentDetails.Rows[0][1].ToString();
                    Party = objPaymentDetails.Rows[0][5].ToString();
                    BillAmout = Convert.ToDecimal(objPaymentDetails.Rows[0][3].ToString());
                    PendAmount = Convert.ToDecimal(objPaymentDetails.Rows[0][4].ToString());
                    BillStatus = objPaymentDetails.Rows[0][6].ToString();
                    BillCancel = objPaymentDetails.Rows[0][7].ToString();
                    Acccode = objPaymentDetails.Rows[0][9].ToString();
                    Status = "5";
                    if (PendAmount == 0)
                    {
                        Status = "1"; // Bill Has no Pending Amount.
                    }
                    else if (BillCancel.ToUpper() == "Y")
                    {
                        Status = "2"; // Bill is Cancelled
                    }
                    else if (BillStatus.ToUpper() != "BILL SUBMITTED" && BillType == "BILLNO")
                    {
                        Status = "3"; // Bill Is Not Submitted,For Adjustment Bill Must be Submitted.
                    }
                    else if (BillStatus.ToUpper() == "BILL COLLECTED" && BillType == "BILLNO")
                    {
                        Status = "4"; // Bill Already Collected.
                    }

                }
                return new JsonResult()
                {
                    Data = new
                    {
                        BillDate = BillDate,
                        Party = Party,
                        BillAmout = BillAmout,
                        PendAmount = PendAmount,
                        BillStatus = BillStatus,
                        BillCancel = BillCancel,
                        Acccode = Acccode,
                        Status = Status
                    }
                };
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult BillEntryAdjustment(BillEntryAdjustmentViewModel BEAVM, List<BillEntryModel> PaymentBillList, List<BillEntryModel> IncomeBillList)
        {
            string AdjNO = "", TranXaction = "";
            try
            {
                string Xml_AdjMst = "<root><Mst>", Xml_AdjDet = "<root>", Xml_BeAdj = "<root>", Xml_BillAdj = "<root>", BeNos = "", BillNos = "";
                Xml_AdjMst = Xml_AdjMst + "<AdjDt>" + GF.FormateDate(BEAVM.AdjustmentDate) + "</AdjDt>";
                Xml_AdjMst = Xml_AdjMst + "<AdjAmt>" + BEAVM.AdjustmentPaymentAmount + "</AdjAmt>";
                Xml_AdjMst = Xml_AdjMst + "<AdjBrcd>" + BEAVM.AdjustmentLocation.ToString() + "</AdjBrcd>";
                Xml_AdjMst = Xml_AdjMst + "<EntryBy>" + BaseUserName + "</EntryBy>";

                foreach (var item in PaymentBillList)
                {
                    if (BeNos == "")
                    {
                        BeNos = item.BIllno.ToString();
                    }
                    else
                    {
                        BeNos = BeNos + "," + item.BIllno.ToString();
                    }

                    if (String.IsNullOrEmpty(item.Acccode))
                    {
                        item.Acccode = "";
                    }
                    //Xml_AdjDet = Xml_AdjDet + "<Det>";
                    Xml_BeAdj = Xml_BeAdj + "<BeDet>";
                    Xml_BeAdj = Xml_BeAdj + "<Billno>" + item.BIllno.ToString() + "</Billno>";
                    Xml_BeAdj = Xml_BeAdj + "<BillAmt>" + item.AdjstAmt.ToString() + "</BillAmt>";
                    Xml_BeAdj = Xml_BeAdj + "<BillPenAmt>" + item.PendAmt.ToString() + "</BillPenAmt>";
                    Xml_BeAdj = Xml_BeAdj + "<Acccode>" + item.Acccode.ToString() + "</Acccode>";
                    Xml_BeAdj = Xml_BeAdj + "<BillType>BillEntry</BillType>";
                    //Xml_AdjDet = Xml_AdjDet + "<Det>";
                    Xml_BeAdj = Xml_BeAdj + "</BeDet>";
                    Xml_AdjDet = Xml_BeAdj;
                }
                Xml_AdjDet = Xml_AdjDet.Replace("BeDet", "Det");
                //Xml_AdjDet = Xml_AdjDet + Xml_BeAdj.Replace("</BeDet>", "");
                Xml_BeAdj = Xml_BeAdj + "</root>";
                foreach (var item in IncomeBillList)
                {
                    if (BillNos == "")
                    {
                        BillNos = item.BIllno.ToString();
                    }
                    else
                    {
                        BillNos = BillNos + "," + item.BIllno.ToString();
                    }
                    if (String.IsNullOrEmpty(item.Acccode))
                    {
                        item.Acccode = "";
                    }
                    //Xml_AdjDet = Xml_AdjDet + "<Det>";
                    Xml_BillAdj = Xml_BillAdj + "<BillDet>";
                    Xml_BillAdj = Xml_BillAdj + "<Billno>" + item.BIllno.ToString() + "</Billno>";
                    Xml_BillAdj = Xml_BillAdj + "<BillAmt>" + item.AdjstAmt.ToString() + "</BillAmt>";
                    Xml_BillAdj = Xml_BillAdj + "<BillPenAmt>" + item.PendAmt.ToString() + "</BillPenAmt>";
                    Xml_BillAdj = Xml_BillAdj + "<Acccode>" + item.Acccode.ToString() + "</Acccode>";
                    Xml_BillAdj = Xml_BillAdj + "<BillType>Bill</BillType>";
                    //Xml_AdjDet = Xml_AdjDet + "<Det>";
                    Xml_BillAdj = Xml_BillAdj + "</BillDet>";
                    Xml_AdjDet = Xml_AdjDet + Xml_BillAdj;
                }
                Xml_AdjDet = Xml_AdjDet.Replace("BillDet", "Det");
                Xml_AdjDet = Xml_AdjDet.Replace("</Det><root>", "</Det>");

                //Xml_AdjDet = Xml_AdjDet + Xml_BillAdj.Replace("<BillDet>", "</Det>");

                Xml_BillAdj = Xml_BillAdj + "</root>";
                Xml_AdjDet = Xml_AdjDet + "</root>";
                Xml_AdjMst = Xml_AdjMst + "<BeNos>" + BeNos + "</BeNos>";
                Xml_AdjMst = Xml_AdjMst + "<BillNos>" + BillNos + "</BillNos>";
                Xml_AdjMst = Xml_AdjMst + "</Mst></root>";

                DataTable objDT = new DataTable();
                objDT = FS.BillEntryAdjustmentSubmit(Xml_AdjMst.Replace("&", "&amp;").Trim(), Xml_AdjDet.Replace("&", "&amp;").Trim(), Xml_BeAdj.Replace("&", "&amp;").Trim(), Xml_BillAdj.Replace("&", "&amp;").Trim(), BaseFinYear.Split('-')[0].ToString(), "E", BaseCompanyCode);
                if (objDT.Rows.Count > 0)
                {
                    AdjNO = objDT.Rows[0]["AdjNO"].ToString();
                    TranXaction = objDT.Rows[0]["TranXaction"].ToString();

                }
            }
            catch (Exception)
            {
                return RedirectToAction("BillEntryAdjustmentDone", new { AdjNO = "", TranXaction = "NotDone" });
            }
            return RedirectToAction("BillEntryAdjustmentDone", new { AdjNO = AdjNO, TranXaction = TranXaction });
        }

        public ActionResult BillEntryAdjustmentDone(string AdjNO, string TranXaction)
        {
            ViewBag.AdjNO = AdjNO;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region Bill/BillEntry Adjustment Cancellation

        public ActionResult BillEntryAdjustmentCancellation()
        {
            BillEntryAdjustmentViewModel BEAVM = new BillEntryAdjustmentViewModel();
            BEAVM.PaymentBillEntry = new List<BillEntryModel>();
            BEAVM.IncomeBillEntry = new List<BillEntryModel>();
            return View(BEAVM);
        }

        public ActionResult BillAdjustmentCancellationDetails(BillEntryAdjustmentViewModel model)
        {
            BillEntryAdjustmentViewModel VM = new BillEntryAdjustmentViewModel();
            List<BillEntryModel> PaymentBillEntry = FS.GetAdjustedBillNoPaymentDetails(model);
            List<BillEntryModel> IncomeBillEntry = FS.GetAdjustedBillNoIncomeDetails(model);
            VM.PaymentBillEntry = PaymentBillEntry;
            VM.IncomeBillEntry = IncomeBillEntry;
            return View(VM);
        }
        public ActionResult AddBillAdjustmentCancellation(BillEntryAdjustmentViewModel model)
        {
            string AdjustmentBillNo = "", TranXaction = "";
            try
            {
                DataTable objDT = new DataTable();
                objDT = FS.AddBillAdjustmentCancellation(model.AdjustmentBillNo, model.CancellationDate, model.CancellationReason, BaseUserName, BaseFinYear.Split('-')[0].ToString());

                if (objDT.Rows.Count > 0)
                {
                    AdjustmentBillNo = objDT.Rows[0]["AdjustmentBillNo"].ToString();
                    TranXaction = objDT.Rows[0]["TranXaction"].ToString();
                }
            }
            catch (Exception)
            {
                return RedirectToAction("BillCancellationAdjustmentDone", new { AdjustmentBillNo = "", TranXaction = "NotDone" });
            }
            return RedirectToAction("BillCancellationAdjustmentDone", new { AdjustmentBillNo = AdjustmentBillNo, TranXaction = TranXaction });
        }

        public ActionResult BillCancellationAdjustmentDone(string AdjustmentBillNo, string TranXaction)
        {
            ViewBag.AdjustmentBillNo = AdjustmentBillNo;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region View Print

        #region Income View Print

        public ActionResult BillsViewPrintCriteria()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BillsViewPrintList(vm_InconeViewPrint vm)
        {
            List<webx_BILLMST> optrkList = FS.Get_BillViewPrintList(GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), vm.billno, vm.mbillno, vm.BillingParty, vm.billtype, vm.billstatus, BaseLocationCode);
            vm.ListBillMst = optrkList;
            return View(vm);
        }

        public ActionResult MRViewPrintCriteria()
        {
            return View();
        }

        public ActionResult MRViewPrintList(vm_InconeViewPrint vm)
        {
            List<WEBX_MR_VPLIST> optrkList = FS.Get_MRViewPrintList(GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), vm.billno, vm.mbillno, vm.billtype, BaseLocationCode, BaseFinYear);
            vm.ListMRhdr = optrkList;
            return View(vm);
        }

        public ActionResult CreditNoteViewCriteria()
        {
            CreaditCnotViewModel VM = new CreaditCnotViewModel();
            VM.ListLocation = MS.GetWorkingLocations(BaseLocationCode, MainLocCode);
            return View(VM);
        }

        public JsonResult GetSubBranch(string BRCode)
        {
            List<Webx_Master_General> DSFieldOfficerList = FS.GetSubBranchFromBranch(BRCode);
            var users = (from user in DSFieldOfficerList
                         select new
                         {
                             Value = user.CodeId,
                             Text = user.CodeDesc
                         }).Distinct().ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreditNoteViewList(CreaditCnotViewModel vm)
        {
            List<vw_CreaditCnotViewList> optrkList = FS.Get_cnote_register(GF.FormateDate(vm.CreaditCnotModel.FromDate), GF.FormateDate(vm.CreaditCnotModel.ToDate), vm.CreaditCnotModel.DockNo, vm.CreaditCnotModel.From_loc, vm.CreaditCnotModel.From_br);
            vm.ListCraditCnot = optrkList;
            return View(vm);
        }

        public ActionResult SuppBillViewCriteria()
        {
            return View();
        }

        public ActionResult SupplimentryList(vm_InconeViewPrint vm)
        {
            List<vw_SupplimentryViewList> optrkList = FS.Get_SupplimentryViewPrintList(GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), vm.billno, vm.mbillno, vm.BillingParty, vm.billtype, vm.billstatus);
            vm.ListSupplimentry = optrkList;
            return View(vm);
        }

        #endregion

        #region  Octroi Agent Bill View & Print

        public ActionResult AgentBillCriteria()
        {
            return View();
        }

        public ActionResult AgentBillViewList(OctroiAgentBillViewModel vm)
        {
            List<AgentBillList> optrkList = FS.Get_Agent_Octroi_Bill_ReViewPrint_Data(GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.PartyCode, vm.AgentBillModel.BillNo);
            vm.ListAgentBill = optrkList;
            return View(vm);
        }

        public JsonResult GetVendor_NameJson(string searchTerm)
        {
            List<AgentBillModel> ListLocations = new List<AgentBillModel>();

            ListLocations = FS.GetVendor_NameList(searchTerm);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.PartyCode,
                                  text = e.PartyName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerBillCriteria()
        {
            return View();
        }

        public ActionResult CustomerBillViewList(OctroiAgentBillViewModel vm)
        {
            List<CustomerBillList> optrkList = FS.Get_Octroi_Bill_ReViewPrint_Data(GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.PartyCode, vm.AgentBillModel.BillNo);
            vm.ListCustBill = optrkList;
            return View(vm);
        }

        public ActionResult AgentPaymentVoucher()
        {
            return View();
        }

        public ActionResult AgentPaymentVoucherList(OctroiAgentBillViewModel vm)
        {
            List<AgentVoucherBillList> optrkList = FS.Get_AgentPaymentVoucher_ReViewPrint_Data(GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.PartyCode, vm.AgentBillModel.BillNo);
            vm.ListAgntVohrBill = optrkList;
            return View(vm);
        }

        #endregion

        #endregion

        #region ChartOfAccount

        public ActionResult ChartOfAccount()
        {

            return View();
        }

        public string GetAccountTree()
        {
            DataTable obj = FS.GetListAccountTree();
            return JsonConvert.SerializeObject(obj);
        }

        #endregion

        #region Main Contract

        public ActionResult MainContract()
        {
            CustomerContractViewModel objcontractModel = new CustomerContractViewModel();
            List<webx_custcontract_hdr> objCustLits = new List<webx_custcontract_hdr>();
            webx_custcontract_hdr objContractHdr = new webx_custcontract_hdr();
            objcontractModel.CustomerContract = objContractHdr;
            objcontractModel.ListCustContract = objCustLits;

            return View(objcontractModel);
        }

        public ActionResult SecondryContract()
        {
            CustomerContractViewModel objcontractModel = new CustomerContractViewModel();
            List<webx_custcontract_hdr> objCustLits = new List<webx_custcontract_hdr>();
            webx_custcontract_hdr objContractHdr = new webx_custcontract_hdr();
            Webx_Fleet_Secondary_Contract_Hdr objSecondryHdr = new Webx_Fleet_Secondary_Contract_Hdr();

            objcontractModel.CustomerContract = objContractHdr;
            objcontractModel.ListCustContract = objCustLits;
            objcontractModel.SecondryConthdr = objSecondryHdr;

            return View(objcontractModel);
        }

        public ActionResult SecondryContractEditList(string CustCode, string ContractID)
        {
            CustomerContractViewModel objCustomerContract = new CustomerContractViewModel();
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();
            webx_custcontract_hdr objcontractHdr = new webx_custcontract_hdr();
            List<Webx_Fleet_Contract_People> ContractPepoleList = new List<Webx_Fleet_Contract_People>();
            objCustomerContract.ContractPepoleList = ContractPepoleList;

            List<Webx_Fleet_Secondary_Contract_Hdr> ContrxthdrList = new List<Webx_Fleet_Secondary_Contract_Hdr>();
            List<Webx_Fleet_Secondary_Contract_Hdr> ContrdetailList = new List<Webx_Fleet_Secondary_Contract_Hdr>();
            objCustomerContract.ListSecondryContract = ContrxthdrList;
            objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
            if (objCustHdr != null)
            {
                ViewBag.CustCode = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
            }
            else
            {
                ViewBag.CustCode = CustCode;
            }
            objcontractHdr.Custcode = CustCode;
            objcontractHdr.ContractId = ContractID;
            objCustomerContract.CustomerContract = objcontractHdr;
            return View(objCustomerContract);
        }

        public ActionResult MainContarctEditList(string CustCode, string ContractID, string ContractType)
        {
            CustomerContractViewModel objcontract = new CustomerContractViewModel();
            List<Webx_Fleet_Contract_People> ContractPepoleList = new List<Webx_Fleet_Contract_People>();
            Webx_Fleet_Contract_People objcontractPepole = new Webx_Fleet_Contract_People();
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();
            webx_custcontract_hdr CustomerContract = new webx_custcontract_hdr();
            try
            {
                ContractPepoleList = FS.GetPepoleData(ContractID).ToList();
                objcontractPepole.SrNo = 1;
                if (ContractPepoleList.Count == 0)
                {
                    ContractPepoleList.Add(objcontractPepole);
                }
                objcontract.ContractPepoleList = ContractPepoleList;
                objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
                int srno = 0;
                foreach (var item in ContractPepoleList)
                {
                    srno++;
                    item.SrNo = srno;

                }
                if (objCustHdr != null)
                {
                    ViewBag.CustCode = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
                }
                else
                {
                    ViewBag.CustCode = CustCode;
                }
                if (ContractID != "")
                    CustomerContract.ContractId = ContractID;
                if (CustCode != "")
                    CustomerContract.Custcode = CustCode;
                if (ContractType != "")
                    CustomerContract.Contract_Type = ContractType;
                objcontract.CustomerContract = CustomerContract;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View("MainContarctEditList", objcontract);
        }

        public ActionResult NewContract(string CustCode, string ContractID, string ContractType, string Flag)
        {
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();

            objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
            ViewBag.contarctCode = ContractID;
            ViewBag.Flag = Flag;
            ViewBag.Type = ContractType;
            if (objCustHdr != null)
            {
                ViewBag.Name = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
                ViewBag.CustCode = objCustHdr.CUSTCD;
            }
            else
            {
                ViewBag.CustCode = CustCode;
            }
            return View("NewContract");
        }

        public ActionResult ContractBasicInformation(string CustCode, string ContractID, string ContractType)
        {
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();
            webx_custcontract_hdr objcontractHdr = new webx_custcontract_hdr();



            objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
            if (objCustHdr != null)
            {
                ViewBag.CustCode = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
            }
            else
            {
                ViewBag.CustCode = CustCode;
            }
            objcontractHdr.Custcode = CustCode;
            objcontractHdr.ContractId = ContractID;
            if (ContractID != "")
            {
                objcontractHdr = FS.GetContractInfromation(ContractID).FirstOrDefault();
            }
            if (ContractType != "")
            {
                objcontractHdr.Contract_Type = ContractType;
            }

            return View("ContractBasicInformation", objcontractHdr);

        }

        public ActionResult SecondryContractEditNext(string CustCode, string ContractID)
        {
            CustomerContractViewModel objCustomerContract = new CustomerContractViewModel();
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();
            webx_custcontract_hdr objcontractHdr = new webx_custcontract_hdr();

            objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
            if (objCustHdr != null)
            {
                ViewBag.CustCode = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
            }
            else
            {
                ViewBag.CustCode = CustCode;
            }
            Webx_Fleet_Secondary_Contract_Hdr ObjSecContacrt = new Webx_Fleet_Secondary_Contract_Hdr();
            List<Webx_Fleet_Secondary_Contract_Hdr> ListSecondryContra = new List<Webx_Fleet_Secondary_Contract_Hdr>();
            ListSecondryContra = FS.GetSecondryContractList(ContractID).ToList();

            if (ListSecondryContra.Count == 0)
            {
                ObjSecContacrt.Id = 1;
                ListSecondryContra.Add(ObjSecContacrt);
            }
            else
            {
                int AutoId = 0;
                foreach (var item in ListSecondryContra)
                {
                    AutoId++;
                    item.Id = AutoId;
                }
            }
            ObjSecContacrt = FS.GetSingleSecondryObj(ContractID).FirstOrDefault();


            if (ObjSecContacrt.Contract_Based_On == "FTL") { ObjSecContacrt.Contract_Based_On = "FTL Type"; }
            if (ObjSecContacrt.Contract_Based_On == "Vehicle") { ObjSecContacrt.Contract_Based_On = "Vehicle Type"; }

            if (ObjSecContacrt.AC == "Yes") { ObjSecContacrt.AC = "AC"; }
            if (ObjSecContacrt.Non_AC == "Yes") { ObjSecContacrt.AC = ObjSecContacrt.Non_AC + " & Non-AC"; }
            if (ObjSecContacrt.Empty == "Yes") { ObjSecContacrt.AC = ObjSecContacrt.Empty + " & Empty"; }

            if (ObjSecContacrt.Rate_Based_On == "Hourly") { ObjSecContacrt.Rate_Based_On = "Hourly Based"; }
            if (ObjSecContacrt.Rate_Based_On == "Fixed") { ObjSecContacrt.Rate_Based_On = "Fixed"; }

            if (ObjSecContacrt.Rate_Type == "KM") { ObjSecContacrt.Rate_Type = "Per KM"; }
            if (ObjSecContacrt.Rate_Type == "KG") { ObjSecContacrt.Rate_Type = "Per KG"; }
            if (ObjSecContacrt.Rate_Type == "PKG") { ObjSecContacrt.Rate_Type = "Per Package"; }

            if (ObjSecContacrt.Fixed_Cost_Rate == "Yes")
            {

            }

            if (ObjSecContacrt.Fixed_Cost_Rate_Based_On == "NonMatrix") { ObjSecContacrt.Fixed_Cost_Rate_Based_On = "Non-Matrix"; }
            else if (ObjSecContacrt.Fixed_Cost_Rate_Based_On == "Matrix") { ObjSecContacrt.Fixed_Cost_Rate_Based_On = "Matrix"; }

            if (ObjSecContacrt.Variable_Cost_Rate_Based_On == "NonMatrixAddSecondryNext") { ObjSecContacrt.Variable_Cost_Rate_Based_On = "Non-Matrix"; }
            else if (ObjSecContacrt.Variable_Cost_Rate_Based_On == "Matrix") { ObjSecContacrt.Variable_Cost_Rate_Based_On = "Matrix"; }

            if (ObjSecContacrt.Standard_Charge_Applicable == "Yes") { ObjSecContacrt.Standard_Charge_Applicable = "Yes"; }


            objCustomerContract.SecondryConthdr = ObjSecContacrt;
            objcontractHdr.Custcode = CustCode;
            objcontractHdr.ContractId = ContractID;
            objCustomerContract.CustomerContract = objcontractHdr;
            objCustomerContract.ListSecondryContract = ListSecondryContra;
            return View(objCustomerContract);


        }

        public ActionResult GetContractDetail(string custcode, string Type)
        {
            List<webx_custcontract_hdr> CoustContList = new List<webx_custcontract_hdr>();
            CoustContList = FS.GetPeopleContactdata(custcode, Type).ToList();
            return PartialView("_Pepole_CustContract", CoustContList);
        }

        public ActionResult GetSecondryContractDetail(string custcode)
        {
            List<webx_custcontract_hdr> CoustContList = new List<webx_custcontract_hdr>();
            CoustContList = FS.GetContractdetail(custcode).ToList();
            return PartialView("_Secondry_CustContract", CoustContList);
        }

        public ActionResult Changestatus(string ContractID, string Type, string CustCode, string Flag, string ConType)
        {
            DataTable dt = FS.CustomerChnageStatus(ContractID, Type, ConType);
            List<webx_custcontract_hdr> CoustContList = new List<webx_custcontract_hdr>();

            string partialName = "";
            if (Flag == "M")
            {
                CoustContList = FS.GetPeopleContactdata(CustCode, ConType).ToList();
                partialName = "_Pepole_CustContract";
            }
            else
            {
                CoustContList = FS.GetContractdetail(CustCode).ToList();
                partialName = "_Secondry_CustContract";
            }
            return PartialView(partialName, CoustContList);
        }

        public ActionResult ContractEdit(string CustCode, string ContractID, string ContractType, string Flag)
        {
            CustomerContractViewModel objCustomerContract = new CustomerContractViewModel();
            webx_CUSTHDR objCustHdr = new webx_CUSTHDR();
            webx_custcontract_hdr objcontractHdr = new webx_custcontract_hdr();
            List<Webx_Fleet_Contract_People> ContractPepoleList = new List<Webx_Fleet_Contract_People>();
            objCustomerContract.ContractPepoleList = ContractPepoleList;

            List<Webx_Fleet_Secondary_Contract_Hdr> ContrxthdrList = new List<Webx_Fleet_Secondary_Contract_Hdr>();
            List<Webx_Fleet_Secondary_Contract_Hdr> ContrdetailList = new List<Webx_Fleet_Secondary_Contract_Hdr>();
            objCustomerContract.ListSecondryContract = ContrxthdrList;
            objCustHdr = CS.Getwebx_CUSTHDR().Where(c => c.CUSTCD == CustCode).FirstOrDefault();
            if (objCustHdr != null)
            {
                ViewBag.CustCode = objCustHdr.CUSTCD + " : " + objCustHdr.CUSTNM;
            }
            else
            {
                ViewBag.CustCode = CustCode;
            }
            objcontractHdr.Custcode = CustCode;
            objcontractHdr.ContractId = ContractID;
            if (ContractType != "")
            {
                objcontractHdr.Contract_Type = ContractType;
            }
            ViewBag.Flag = Flag;
            objCustomerContract.CustomerContract = objcontractHdr;
            return View(objCustomerContract);

        }

        public ActionResult AddContractbasicInfo(webx_custcontract_hdr Custcontract)
        {
            CustomerContractViewModel objcontract = new CustomerContractViewModel();
            List<Webx_Fleet_Contract_People> ContractPepoleList = new List<Webx_Fleet_Contract_People>();
            Webx_Fleet_Contract_People objcontractPepole = new Webx_Fleet_Contract_People();
            DataTable Dt = new DataTable();
            try
            {

                XmlDocument xmlDoc = new XmlDocument();
                if (Custcontract.stax_yn_chek == true)
                    Custcontract.stax_yn = "Y";
                else
                    Custcontract.stax_yn = "N";
                XmlSerializer xmlSerializer = new XmlSerializer(Custcontract.GetType());
                if (Custcontract != null)
                {
                    using (MemoryStream xmlStream = new MemoryStream())
                    {
                        xmlSerializer.Serialize(xmlStream, Custcontract);
                        xmlStream.Position = 0;
                        xmlDoc.Load(xmlStream);
                        Dt = FS.UpdateCustomerCode(Custcontract.Custcode, Custcontract.ContractId, Custcontract.Contract_Type, xmlDoc.InnerXml, BaseUserName);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("SecondryContractDone", new { CustCode = Custcontract.Custcode, ContractID = Dt.Rows[0]["ContractID"].ToString(), Flag = "M" });
        }

        public ActionResult ClickServiceSelectoion(CustomerContractViewModel objcontract, string Custcode, string Contracttype)
        {
            List<Webx_Fleet_Contract_People> ContractPepoleList = new List<Webx_Fleet_Contract_People>();
            Webx_Fleet_Contract_People objcontractPepole = new Webx_Fleet_Contract_People();
            webx_custcontract_hdr CustomerContract = new webx_custcontract_hdr();
            CustomerContract.Custcode = Custcode;
            CustomerContract.Contract_Type = Contracttype;
            objcontract.CustomerContract = CustomerContract;
            //objcontract.CustomerContract.ContractId = ContractId;
            ContractPepoleList = FS.GetPepoleData("").ToList();
            objcontractPepole.ID = 1;
            if (ContractPepoleList.Count == 0)
            {
                ContractPepoleList.Add(objcontractPepole);
            }
            objcontract.ContractPepoleList = ContractPepoleList;
            return PartialView("_Finance_ServiceSelection", objcontract);
        }

        public ActionResult Addpepole(int id)
        {
            Webx_Fleet_Contract_People ObjPeople = new Webx_Fleet_Contract_People();
            ObjPeople.SrNo = id;

            return PartialView("_PartialPepole", ObjPeople);
        }

        public ActionResult AddRate(int id)
        {
            Webx_Fleet_Secondary_Contract_Hdr ObjSecondry = new Webx_Fleet_Secondary_Contract_Hdr();
            ObjSecondry.Id = id;

            return PartialView("_Partial_RateBase_On", ObjSecondry);
        }

        public ActionResult AddContracthdrdetail(int id)
        {
            Webx_Fleet_Secondary_Contract_Hdr ObjSecondry = new Webx_Fleet_Secondary_Contract_Hdr();
            ObjSecondry.Id = id;

            return PartialView("_Partial_Detail_Contract_Obj", ObjSecondry);
        }

        public ActionResult AddServiceSelection(CustomerContractViewModel objcontract, List<Webx_Fleet_Contract_People> PepoleList)
        {
            XmlDocument xmlDoc = new XmlDocument();
            DataTable Dt = new DataTable();
            if (PepoleList != null)
            {
                foreach (var item in PepoleList)
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(item.GetType());
                    using (MemoryStream xmlStream = new MemoryStream())
                    {
                        xmlSerializer.Serialize(xmlStream, item);
                        xmlStream.Position = 0;
                        xmlDoc.Load(xmlStream);

                    }
                    Dt = FS.UpdateCustPepople(item.ID, objcontract.CustomerContract.Custcode, objcontract.CustomerContract.ContractId, objcontract.CustomerContract.Contract_Type, xmlDoc.InnerXml, BaseUserName, BaseLocationCode, BaseCompanyCode);
                }

            }
            return RedirectToAction("SecondryContractDone", new { CustCode = objcontract.CustomerContract.Custcode, ContractID = Dt.Rows[0]["ContractID"].ToString(), Flag = "M" });
        }

        public ActionResult AddFinanceContract(CustomerContractViewModel objcontract, DateTime Contract_From_Dt, DateTime Contract_To_Dt, List<Webx_Fleet_Secondary_Contract_Hdr> RateBaseOnList)
        {

            DataTable Dt = new DataTable();
            objcontract.SecondryConthdr.Contract_From_Dt = Contract_From_Dt;
            objcontract.SecondryConthdr.Contract_To_Dt = Contract_To_Dt;
            if (objcontract.CustomerContract.ContractId != "")
                objcontract.SecondryConthdr.Contract_Code = objcontract.CustomerContract.ContractId;
            if (objcontract.SecondryConthdr.AC1 == true)
                objcontract.SecondryConthdr.AC = "Yes";
            else
                objcontract.SecondryConthdr.AC = "No";

            if (objcontract.SecondryConthdr.Non_AC1 == true)
                objcontract.SecondryConthdr.Non_AC = "Yes";
            else
                objcontract.SecondryConthdr.Non_AC = "No";

            if (objcontract.SecondryConthdr.Empty1 == true)
                objcontract.SecondryConthdr.Empty = "Yes";
            else
                objcontract.SecondryConthdr.Empty = "No";

            if (objcontract.SecondryConthdr.Fixed_Cost_Rate_Based_On1 == true)
                objcontract.SecondryConthdr.Fixed_Cost_Rate_Based_On = "Yes";
            else
                objcontract.SecondryConthdr.Fixed_Cost_Rate_Based_On = "No";

            if (objcontract.SecondryConthdr.Standard_Charge_Applicable1 == true)
                objcontract.SecondryConthdr.Standard_Charge_Applicable = "Yes";
            else
                objcontract.SecondryConthdr.Standard_Charge_Applicable = "No";

            objcontract.SecondryConthdr.Contract_Code = objcontract.CustomerContract.ContractId;
            XmlDocument xmlDocContract = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(objcontract.SecondryConthdr.GetType());
            using (MemoryStream xmlStream1 = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream1, objcontract.SecondryConthdr);
                xmlStream1.Position = 0;
                xmlDocContract.Load(xmlStream1);
            }

            XmlDocument xmlDoc = new XmlDocument();

            if (RateBaseOnList != null)
            {
                XmlSerializer xmlSerializer = new XmlSerializer(RateBaseOnList.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, RateBaseOnList);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
            }
            Dt = FS.InsertUpdateFleetSecondrt(objcontract.CustomerContract.Custcode, objcontract.CustomerContract.ContractId, xmlDocContract.InnerXml, xmlDoc.InnerXml, BaseUserName);

            return RedirectToAction("SecondryContractEditNext", new { objcontract.CustomerContract.Custcode, ContractID = Dt.Rows[0]["ContractID"].ToString() });
        }

        public ActionResult AddSecondryNext(CustomerContractViewModel objcontract, List<Webx_Fleet_Secondary_Contract_Hdr> SecondryDetailList)
        {
            DataTable Dt = new DataTable();
            XmlDocument xmlDoc = new XmlDocument();

            foreach (var item in SecondryDetailList)
            {
                if (SecondryDetailList != null)
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(item.GetType());
                    using (MemoryStream xmlStream = new MemoryStream())
                    {
                        xmlSerializer.Serialize(xmlStream, item);
                        xmlStream.Position = 0;
                        xmlDoc.Load(xmlStream);
                    }
                }

                Dt = FS.InsertUpdateFleetSecondrtDetail(objcontract.CustomerContract.Custcode, objcontract.CustomerContract.ContractId, item.Contract_Det_Code, xmlDoc.InnerXml, BaseUserName);
            }
            return RedirectToAction("SecondryContractDone", new { CustCode = objcontract.CustomerContract.Custcode, ContractID = Dt.Rows[0]["ContractID"].ToString(), Flag = "S" });
        }

        public ActionResult SecondryContractDone(string CustCode, string ContractID, string Flag)
        {
            ViewBag.CustCode = CustCode;
            ViewBag.contarctCode = ContractID;
            ViewBag.Flag = Flag;
            return View("SecondryContractDone");
        }

        #endregion

        #region Customer/Vendor Setting

        public ActionResult SeetingCustomerVendor()
        {
            CustomerVendrosetting objCustomerSetting = new CustomerVendrosetting();

            objCustomerSetting.ListAccount = FS.GetAccountInfoList().ToList();
            objCustomerSetting.ListVendor = FS.SeetingVendorList().ToList();
            objCustomerSetting.ListDriver = FS.SeetingDriverList().ToList();
            objCustomerSetting.ListEmployee = FS.SeetingEmployeeList().ToList();

            return View("SeetingCustomerVendor", objCustomerSetting);
        }

        public ActionResult AddCustomersetting(CustomerVendrosetting objcustomer)
        {
            string EffectCode, location, SelectCode, Flag;
            if (objcustomer.CustomerType == "Customer")
            {
                EffectCode = objcustomer.Customereffect;
                location = objcustomer.Customerlocation;
                SelectCode = objcustomer.Customer;
                Flag = "Customer";
            }
            else if (objcustomer.CustomerType == "Vendor")
            {
                EffectCode = objcustomer.Vendoreffect;
                location = objcustomer.Vendorlocation;
                SelectCode = objcustomer.Vendor;
                Flag = "Vendor";
            }
            else if (objcustomer.CustomerType == "Driver")
            {
                EffectCode = objcustomer.Driveeffect;
                location = objcustomer.Drivelocation;
                SelectCode = objcustomer.Drive;
                Flag = "Driver";
            }
            else
            {
                EffectCode = objcustomer.Employeeeffect;
                location = objcustomer.Employeelocation;
                SelectCode = objcustomer.Employee;
                Flag = "Employee";
            }

            return RedirectToAction("SettingCustomerOpeingBalance", new { EffectCode = EffectCode, location = location, SelectCode = SelectCode, Flag = Flag });
        }

        public ActionResult SettingCustomerOpeingBalance(string EffectCode, string location, string SelectCode, string Flag)
        {
            //string tblStr1 = "webx_acctinfo";
            string tblStr2 = "webx_acctopening_employee_" + BaseYearVal;
            string tblStr3 = "webx_acctopening_" + BaseYearVal;

            CustomerVendrosetting objcustomer = new CustomerVendrosetting();

            DataTable dt = new DataTable();
            List<PartyOpeningBalance> ListBalance = new List<PartyOpeningBalance>();

            if (Flag == "Customer")
            {

                ListBalance = FS.SettingPartyDetails("C", EffectCode, SelectCode, "Desc", location, BaseYearVal, tblStr3, "");
                ViewBag.Name = "Account Code/Name";
                ViewBag.Code = "Customer Code/Name";

                objcustomer.Customereffect = EffectCode;
                objcustomer.Customerlocation = location;
                objcustomer.Customer = SelectCode;
                objcustomer.CustomerType = "C";

            }
            else if (Flag == "Vendor")
            {
                string[] Vend_account_arr = EffectCode.ToString().Split('~');
                string vendor_type = Vend_account_arr[0].ToString();
                string Vend_account = Vend_account_arr[1].ToString();
                string Vend_accountDesc = Vend_account_arr[2].ToString();

                ListBalance = FS.SettingPartyDetails("V", Vend_account, SelectCode, Vend_accountDesc, location, BaseYearVal, tblStr2, vendor_type);
                ViewBag.Name = "Account Code/Name";
                ViewBag.Code = "Vendor Code/Name";

                objcustomer.Customereffect = Vend_account;
                objcustomer.Customerlocation = location;
                objcustomer.Customer = Vend_accountDesc;
                objcustomer.CustomerType = "V";
            }
            else if (Flag == "Driver")
            {
                ListBalance = FS.SettingPartyDetails("D", EffectCode, SelectCode, "Desc", location, BaseYearVal, tblStr2, "");
                ViewBag.Name = "Account Code/Name";
                ViewBag.Code = "Driver Code/Name";

                objcustomer.Customereffect = EffectCode;
                objcustomer.Customerlocation = location;
                objcustomer.Customer = SelectCode;
                objcustomer.CustomerType = "D";

            }
            else if (Flag == "Employee")
            {
                ListBalance = FS.SettingPartyDetails("E", EffectCode, SelectCode, "Desc", location, BaseYearVal, tblStr3, "");
                ViewBag.Name = "Account Code/Name";
                ViewBag.Code = "Driver Code/Name";
                objcustomer.Customereffect = EffectCode;
                objcustomer.Customerlocation = location;
                objcustomer.Customer = SelectCode;
                objcustomer.CustomerType = "E";

            }
            objcustomer.ListPartyOpeing = ListBalance.ToList();
            return View(objcustomer);
        }

        public ActionResult AddPartyOpenBalnace(CustomerVendrosetting objcustomer, List<PartyOpeningBalance> PartyBalList)
        {
            try
            {
                foreach (var item in PartyBalList)
                {
                    if (item.chk == true)
                    {
                        DataTable dt = FS.SetPartyOpeningBalance(item.Opendebit, item.OpenCredit, objcustomer.CustomerType, objcustomer.Customereffect, objcustomer.Customer, objcustomer.Customerlocation, BaseUserName, BaseCompanyCode, BaseYearVal);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
                //return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("SeetingCustomerVendor");
        }

        #endregion

        #region Credit Note Generation

        public ActionResult CreditNoteCriteria()
        {
            CNoteFilterViewModel CNFVM = new CNoteFilterViewModel();
            return View(CNFVM);
        }

        public ActionResult CreditNoteGeneration(CNoteFilterViewModel CNFVM)
        {
            CNoteSubmissionViewModel CNSVM = new CNoteSubmissionViewModel();
            CNSVM.BillList = FS.GetCreditNoteGeneration(CNFVM, BaseLocationCode, BaseCompanyCode).ToList();
            DataTable dt = CS.GetCustomerName(CNFVM.CUSTCD);
            if (dt.Rows.Count > 0)
            {
                CNFVM.CUSTNM = dt.Rows[0]["CustName"].ToString();
            }
            CNSVM.CNFVM = CNFVM;
            return View(CNSVM);
        }

        public ActionResult CreditNoteDone(CNoteSubmissionViewModel CNSVM, List<webx_BILLMST> BillList)
        {
            List<webx_acctrans> itmList = new List<webx_acctrans>();
            try
            {
                string Xml_Cnotehdr = "<root><CNOTE>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<Finyear>" + BaseFinYear.Split('-')[0].ToString() + "</Finyear>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<CNOTEDT>" + CNSVM.CNoteDt + "</CNOTEDT>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<CNBRCD>" + BaseLocationCode + "</CNBRCD>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<REFNO>" + CNSVM.Ref_No + "</REFNO>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<CNAMT>" + CNSVM.TotAmt + "</CNAMT>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<CUSTCD>" + CNSVM.CNFVM.CUSTCD + "</CUSTCD>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<ENTRYBY>" + BaseUserName + "</ENTRYBY>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<TRN_MODE>" + CNSVM.CNFVM.TransMode + "</TRN_MODE>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<Stax_Y_N>" + (CNSVM.CNFVM.STax_YN ? "N" : "Y") + "</Stax_Y_N>";
                Xml_Cnotehdr = Xml_Cnotehdr + "<Stax_Rate>" + CNSVM.CNFVM.STax + "</Stax_Rate>";
                Xml_Cnotehdr = Xml_Cnotehdr + "</CNOTE></root>";

                string Xml_CnoteDet = "<root>";
                if (BillList != null)
                {
                    foreach (var Item in BillList.Where(c => c.IsBillSubmitted == true).ToList())
                    {
                        Xml_CnoteDet = Xml_CnoteDet + "<Det>";
                        Xml_CnoteDet = Xml_CnoteDet + "<BILLNO>" + Item.BILLNO + "</BILLNO>";
                        Xml_CnoteDet = Xml_CnoteDet + "<BILLDT>" + Item.BILLDT + "</BILLDT>";
                        Xml_CnoteDet = Xml_CnoteDet + "<BILLAMT>" + Item.BILLAMT + "</BILLAMT>";
                        Xml_CnoteDet = Xml_CnoteDet + "<PENDAMT>" + Item.PENDAMT + "</PENDAMT>";
                        Xml_CnoteDet = Xml_CnoteDet + "<CNAMT>" + Item.CNAMT + "</CNAMT>";
                        Xml_CnoteDet = Xml_CnoteDet + "<CNPURPOSE>" + Item.POC + "</CNPURPOSE>";
                        Xml_CnoteDet = Xml_CnoteDet + "<CNREMARK>" + Item.REMARK + "</CNREMARK>";
                        Xml_CnoteDet = Xml_CnoteDet + "</Det> ";
                    }
                    Xml_CnoteDet = Xml_CnoteDet + "</root>";
                }
                Xml_Cnotehdr = Xml_Cnotehdr.ReplaceSpecialCharacters();
                Xml_CnoteDet = Xml_CnoteDet.ReplaceSpecialCharacters();
                DataTable Dt = FS.AddCreditNoteGeneration(Xml_Cnotehdr, Xml_CnoteDet);
                if (Dt.Rows.Count > 0)
                {
                    ViewBag.Tranxaction = "DONE";
                    string table_name = "webx_acctrans_" + BaseYearVal;
                    DataTable DT = FS.GetDoneTable(Dt.Rows[0][0].ToString(), table_name);
                    itmList = DataRowToObject.CreateListFromTable<webx_acctrans>(DT);
                    ViewBag.Table = DT;
                }
            }
            catch (Exception)
            {
                ViewBag.Tranxaction = "Not DONE";
            }
            return View(itmList);
        }

        #endregion

        #region Direct Debit Note

        public ActionResult DebitNote()
        {
            DCNoteSubmissionViewModel DNVM = new DCNoteSubmissionViewModel();
            DNVM.DNoteDt = System.DateTime.Now;
            DNVM.preparedby = BaseUserName;
            DNVM.preparedfor = BaseLocationCode + ":" + BaseLocationName;
            DNVM.BillList = new List<webx_BILLMST>();
            DNVM.WBM = new webx_BILLMST();
            return View(DNVM);
        }

        public ActionResult AddDNoteInfo(int id)
        {
            webx_BILLMST WBMM = new webx_BILLMST();
            WBMM.SrNo = id;
            return PartialView("_PartialDebitNote", WBMM);
        }

        public JsonResult GetAcctsearchListJson(string searchTerm)
        {
            List<webx_acctinfo> ListAccount = new List<webx_acctinfo>();
            ListAccount = FS.GetAcctInfo().Where(c => c.Acccode.ToUpper().Contains(searchTerm.ToUpper()) || c.Accdesc.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.Accdesc).ToList();
            var SearchList = (from e in ListAccount
                              select new
                              {
                                  id = e.Acccode,
                                  text = e.Accdesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetDebitorName(string custcode, string VENCD)
        {

            string DebitorName = "", DebitorCode = "";
            string Name = "";
            DataTable dt = FS.GetDebitorName(custcode, VENCD);
            DataTable Dt = FS.GetName(custcode, VENCD);
            if (dt.Rows.Count > 0)
            {
                DebitorName = dt.Rows[0]["DrAccount"].ToString();
                DebitorCode = dt.Rows[0]["Acccode"].ToString();
            }
            if (Dt.Rows.Count > 0)
            {
                Name = Dt.Rows[0]["Name"].ToString();
            }
            var strName = DebitorCode + "," + DebitorName + "," + Name;
            return strName;
        }

        [HttpPost]
        public ActionResult DebitNote(DCNoteSubmissionViewModel DDNVM, List<webx_BILLMST> DNoteList)
        {
            var DebitNoteNo = "";
            var TranXaction = "";
            try
            {
                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear.Split('-')[0].ToString() + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + DDNVM.DNoteDt + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>Journal</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BaseLocationCode + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>DIRECT DEBIT NOTE</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + DDNVM.Ref_No + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + DDNVM.pbov_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + DDNVM.pbov_name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + DDNVM.pbov_typ + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<DebitAccount>" + DDNVM.DebitAccount + "</DebitAccount>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DDNVM.BillList = DNoteList;
                string Xml_Acccode_Details = "<root>";
                foreach (var item in DDNVM.BillList)
                {
                    Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                    Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + item.bankacccode + "</Acccode>";
                    Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + item.AccAmt + "</Credit>";
                    Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";

                }
                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                DataTable Dt = FS.AddDebitNoteDetail(Xml_Acccode_Details.Replace("&", "&amp;").Trim(), Xml_Other_Details.Replace("&", "&amp;").Trim());
                DebitNoteNo = Dt.Rows[0]["DebitNoteNo"].ToString();
                TranXaction = "Done";
                return RedirectToAction("DebitNoteDone", new { DebitNoteNo = DebitNoteNo, VoucherDate = System.DateTime.Now, TranXaction = TranXaction });
            }
            catch (Exception)
            {
                TranXaction = "Not Done";
                return RedirectToAction("DebitNoteDone", new { DebitNoteNo = DebitNoteNo, VoucherDate = System.DateTime.Now, TranXaction = TranXaction });
            }
        }

        public ActionResult DebitNoteDone(string DebitNoteNo, string VoucherDate, string TranXaction)
        {
            ViewBag.DebitNoteNo = DebitNoteNo;
            ViewBag.VoucherDate = VoucherDate;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region ServiceWise Billing

        public ActionResult ServiceWiseBilling()
        {
            return View();
        }

        public JsonResult GetCustomerListByService(string Service, string searchTerm)
        {
            List<webx_CUSTHDR> CustList = new List<webx_CUSTHDR>();

            CustList = FS.GetCustomerListByService(Service).Where(c => c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CUSTNM).ToList();
            var SearchList = (from e in CustList
                              select new
                              {
                                  id = e.CUSTCD,
                                  text = e.CUSTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region IDTFreightMemo

        public ActionResult IDTFreightMemoCriteria()
        {
            IDTFreightMemoCriteria IDTC = new IDTFreightMemoCriteria();
            IDTC.Brcd = BaseLocationCode;
            return View(IDTC);
        }

        public ActionResult IDTFreightMemo(IDTFreightMemoCriteria model)
        {
            IDTFreightMemoViewModel VM = new IDTFreightMemoViewModel();
            List<IDTDetail> IDTDET = OS.GetIDTListForFreightMemo(model);
            if (IDTDET.ToList().Count() > 0)
            {
                VM.IDTDET = IDTDET;
                VM.PartyCode = IDTDET.FirstOrDefault().PARTY_CODE;
                VM.PartyName = IDTDET.FirstOrDefault().PARTY_NAME;
            }
            VM.Brcd = model.Brcd;
            VM.EntryBy = BaseUserName;
            return View(VM);
        }

        [HttpPost]
        public ActionResult IDTFreightMemoSubmit(IDTFreightMemoViewModel VM, List<IDTDetail> IDTList)
        {
            try
            {
                XmlDocument xmlDocCha = new XmlDocument();
                XmlSerializer xmlSerializerCha = new XmlSerializer(VM.GetType());

                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, VM);
                    xmlStreamCha.Position = 0;
                    xmlDocCha.Load(xmlStreamCha);
                }

                List<IDTDetail> docketlist = new List<IDTDetail>();
                docketlist = IDTList.Where(c => c.IsChecked).ToList();

                XmlDocument xmlDocdet = new XmlDocument();
                XmlSerializer xmlSerializerdet = new XmlSerializer(docketlist.GetType());

                using (MemoryStream xmlStreamdet = new MemoryStream())
                {
                    xmlSerializerdet.Serialize(xmlStreamdet, docketlist);
                    xmlStreamdet.Position = 0;
                    xmlDocdet.Load(xmlStreamdet);
                }

                DataTable DT = OS.IDTFreightMemoSubmitProcess(xmlDocCha.InnerXml.Replace("&", "&amp;").Replace("–", "-").Trim(), xmlDocdet.InnerXml.Replace("&", "&amp;").Replace("–", "-").Trim(), BaseUserName.ToUpper(), BaseLocationCode, BaseFinYear, BaseCompanyCode);
                string FMNO = "";
                string status = DT.Rows[0]["Status"].ToString();


                if (status == "0")
                {
                    ViewBag.StrError = DT.Rows[0]["ErrMessage"].ToString();
                    throw new Exception(ViewBag.StrError);
                }
                if (status == "1")
                {
                    FMNO = DT.Rows[0]["FMNO"].ToString();
                }

                return RedirectToAction("IDTFreightMemoDone", new { FMNO = FMNO });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
        }
        public ActionResult IDTFreightMemoDone(string FMNO)
        {

            ViewBag.FMNO = FMNO;

            return View();

        }

        #region IDT Freight Memo Payment
        public ActionResult FreightMemoPaymentCriteria()
        {
            IDTFreightMemoCriteria IDTC = new IDTFreightMemoCriteria();
            IDTC.Brcd = BaseLocationCode;
            return View(IDTC);
        }

        public ActionResult IDTFreightMemoPayment(IDTFreightMemoCriteria model)
        {
            IDTFreightMemoPaymentViewModel VM = new IDTFreightMemoPaymentViewModel();
            List<IDTFreightMemoViewModel> FMDETList = OS.GetFMListForPayment(model);
            if (FMDETList.ToList().Count() > 0)
            {
                VM.FMDET = FMDETList.ToList();
                VM.PartyCode = FMDETList.FirstOrDefault().PartyCode;
                VM.PartyName = FMDETList.FirstOrDefault().PartyName;
            }
            VM.Brcd = model.Brcd;
            VM.EntryBy = BaseUserName;
            VM.CCP = new PaymentControl();
            VM.CCP.ChequeDate = System.DateTime.Now;


            return View(VM);
        }
        [HttpPost]
        public ActionResult SubmitIDTFreightMemoPayment(IDTFreightMemoPaymentViewModel IFMV, List<IDTFreightMemoViewModel> IDTList, PaymentControl PaymentControl)
        {


            try
            {
                //if (PaymentControl.PayAmount < 0)
                //{
                //    ViewBag.StrError = "Please Enter Non Negative Value.";
                //    return View("Error");
                //} 

                string Cnt = "";
                if (PaymentControl.PaymentMode != "Cash")
                {
                    Cnt = PS.Duplicate_ChqNO(PaymentControl.ChequeNo, GF.FormateDate(PaymentControl.ChequeDate));
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }


                string Xml_PAY_IDT = "<root>";

                if (IDTList != null)
                {
                    foreach (var item in IDTList.Where(M => M.IsChecked == true))
                    {
                        IFMV.PartyName = item.PartyName;
                        Xml_PAY_IDT = Xml_PAY_IDT + "<PAY_IDT_DET>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<FreightMemoNo>" + item.FMNO + "</FreightMemoNo>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<FreightMemoDate>" + item.FMDATE + "</FreightMemoDate>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<AmountPayable>" + item.NetPayable + "</AmountPayable>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<Deduction>" + item.Deductions + "</Deduction>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<NetPayable>" + item.FinalNetPayable + "</NetPayable>";
                        Xml_PAY_IDT = Xml_PAY_IDT + "<Cancel>" + "N" + "</Cancel></PAY_IDT_DET>";
                    }
                }

                Xml_PAY_IDT = Xml_PAY_IDT + "</root>";

                string Xml_PAY_MST_IDT = "<root><PAY_IDT_Mst>";

                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<CustCode>" + IFMV.PartyCode + "</CustCode>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<CustName>" + IFMV.PartyName + "</CustName>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<TotalAmtPayable>" + IFMV.TotalAmtPayable + "</TotalAmtPayable>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<TotalDeduction>" + IFMV.TotalDeduction + "</TotalDeduction>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<TotalNetPayable>" + IFMV.TotalNetPayable + "</TotalNetPayable>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<PayMode>" + PaymentControl.PaymentMode + "</PayMode>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<CashAccCode>" + PaymentControl.CashLedger + "</CashAccCode>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<ChequeNo>" + PaymentControl.ChequeNo + "</ChequeNo>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<BankAccCode>" + PaymentControl.BankLedger + "</BankAccCode>";
                if (PaymentControl.PaymentMode.ToUpper() == "CASH")
                {
                    Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<ChequeDate>" + "01 Jan 1990" + "</ChequeDate>";
                }
                else
                {
                    Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<ChequeDate>" + Convert.ToDateTime(PaymentControl.ChequeDate).ToString("dd MMM yyyy") + "</ChequeDate>";
                }

                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<ChequeNo>" + PaymentControl.ChequeNo + "</ChequeNo>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<Company_Code>" + BaseCompanyCode + "</Company_Code>";

                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "<Cancel>" + "N" + "</Cancel>";
                Xml_PAY_MST_IDT = Xml_PAY_MST_IDT + "</PAY_IDT_Mst></root>";

                DataTable Dt = FS.Insert_IDT_Payment_Data(Xml_PAY_MST_IDT, Xml_PAY_IDT, BaseLocationCode, BaseUserName, BaseFinYear);
                string VoucherNo = "";
                string TranXaction = "";
                VoucherNo = Dt.Rows[0]["VoucherNo"].ToString();
                TranXaction = Dt.Rows[0]["TranXaction"].ToString();

                if (TranXaction == "Done")
                {
                    return RedirectToAction("FreightMemoPaymentDone", new { VoucherNo = VoucherNo });
                }
                else
                {
                    throw new Exception(TranXaction);
                }
            }
            catch (Exception Ex)
            {
                @ViewBag.StrError = Ex.Message;
                return View("Error");
            }
        }

        public ActionResult FreightMemoPaymentDone(string VoucherNo)
        {

            ViewBag.VoucherNo = VoucherNo;

            return View();

        }

        #endregion IDT Freight Memo Payment
        #endregion IDTFreightMemo

        #region Crossing Challan View/ Print


        public ActionResult CrossingChallanViewCriteria()
        {
            CrossingChallanFilterViewModel Criteria = new CrossingChallanFilterViewModel();
            return View(Criteria);
        }

        [HttpPost]
        public string GetCrossingChallanViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetCrossingChallanViewList(VM, BaseLocationCode));
        }
        #endregion

        #region Crossing Voucher  View/ Print


        public ActionResult CrossingChallanVoucherViewCriteria()
        {
            CrossingChallanFilterViewModel Criteria = new CrossingChallanFilterViewModel();
            return View(Criteria);
        }

        [HttpPost]
        public string GetCrossingVoucherViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetCrossingVoucherViewList(VM, BaseLocationCode));
        }
        #endregion

        #region IDT View/ Print


        public ActionResult IDTViewCriteria()
        {
            CrossingChallanFilterViewModel Criteria = new CrossingChallanFilterViewModel();
            return View(Criteria);
        }

        [HttpPost]
        public string GetIDTViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetIDTViewList(VM, BaseLocationCode));
        }
        #endregion

        #region GatePass Reprint


        public ActionResult GatePassReprintViewCriteria()
        {
            CrossingChallanFilterViewModel Criteria = new CrossingChallanFilterViewModel();
            return View(Criteria);
        }

        [HttpPost]
        public string GetGatePassReprintViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetGatePassReprintViewList(VM, BaseLocationCode));
        }

        [HttpPost]
        public string GetGatePassReprintDKTWiseViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetGatePassReprintDKTWiseViewList(VM, BaseLocationCode));
        }



        #endregion

        #region CNote Reprint


        public ActionResult CNoteReprintViewCriteria()
        {
            CrossingChallanFilterViewModel Criteria = new CrossingChallanFilterViewModel();
            return View(Criteria);
        }

        [HttpPost]
        public string GetCnoteReprintViewList(CrossingChallanFilterViewModel VM)
        {
            return JsonConvert.SerializeObject(FS.GetCnoteReprintViewList(VM, BaseLocationCode));
        }
        #endregion

        #region IDTFreightMemoViewPrint


        public ActionResult IDTFreightMemoCriteriaViewPrint()
        {
            IDTFreightMemoCriteria Criteria = new IDTFreightMemoCriteria();
            return View(Criteria);
        }

        [HttpPost]
        public string GetFreightMemoViewList(IDTFreightMemoCriteria VM)
        {

            return JsonConvert.SerializeObject(FS.GetFreightMemoViewList(VM, BaseLocationCode));
        }
        #endregion

        #region Buget Module Implementation

        public ActionResult SelectBranch()
        {
            string BaseFinancialYear = BaseFinYear.ToString().Substring(0, 4);
            ViewBag.year = BaseFinancialYear;
            return View();
        }

        public ActionResult SelectBranchsubmitList(SetOpeningBalanceViewModel1 Viewmodel, string Changwise)
        {

            List<webx_AcctBudget> ListOfResults = new List<webx_AcctBudget>();
            if (Changwise == "Location")
            {
                if (Viewmodel.Account1.Budget_Type == "Y")
                {
                    ViewBag.Budget_Type = "Y";
                }
                else
                {
                    ViewBag.Budget_Type = "M";
                }
                string BudgetType = Changwise == "Location" ? "1" : "2";
                string AccountCode = Viewmodel.Balance.Acccode;
                if (AccountCode == "All")
                {
                    AccountCode = "All";
                }
                else
                {
                    AccountCode = Viewmodel.Balance.Acccode;
                }
                string AccountCategory = Viewmodel.Balance.Acccategory;
                string brcd = Viewmodel.Account1.brcd;
                string YearVal = Viewmodel.Account1.FinYear;
                string EntryType = Viewmodel.Account1.Budget_Type;
                string yearType = Viewmodel.Account1.YearType;
                string RO = "";
                string LO = "";
                ViewBag.Calendar_Type = Viewmodel.Account1.YearType;
                ViewBag.Finyear = Viewmodel.Account1.FinYear;
                ListOfResults = FS.SerchResultsLedger(BudgetType, AccountCode, AccountCategory, brcd, YearVal, EntryType, yearType, RO, LO);
            }

            else if (Changwise == "Ledger")
            {
                if (Viewmodel.Account1.Budget_Type == "Y")
                {
                    ViewBag.Budget_Type = "Y";
                }
                else
                {
                    ViewBag.Budget_Type = "M";
                }
                string BudgetType = Changwise == "Location" ? "1" : "2";
                string AccountCode = Viewmodel.Account1.LedgerCode;
                string AccountCategory = "";
                string brcd = "";
                string YearVal = Viewmodel.Account1.FinYear;
                string EntryType = Viewmodel.Account1.Budget_Type;
                string yearType = Viewmodel.Account1.YearType;
                string RO = Request["LOC2"].ToString();
                string LO = Request["LOC3"].ToString();
                ViewBag.Calendar_Type = Viewmodel.Account1.YearType;
                ViewBag.Finyear = Viewmodel.Account1.FinYear;
                ListOfResults = FS.SerchResultsLedger(BudgetType, AccountCode, AccountCategory, brcd, YearVal, EntryType, yearType, RO, LO);
            }
            else
            {
                return View("Error");

            }
            return View(ListOfResults);
        }

        public ActionResult SelectBranchsubmit(List<webx_AcctBudget> SelectBranchDone)
        {
            DataTable DT = new DataTable();
            try
            {
                foreach (var itm in SelectBranchDone)
                {
                    if (itm.IsCheck == true)
                    {
                        string SQRY = "";
                        string brcd = itm.LocCode;
                        string Year = itm.FinYear;
                        string YearTP = itm.YearType;
                        string AccountCode = itm.ACCCODE;
                        string BudgetType = itm.Budget_Type;

                        var Jan = itm.JanAmt;
                        var Feb = itm.FebAmt;
                        var Mar = itm.MarAmt;
                        var Apr = itm.AprAmt;
                        var May = itm.MayAmt;
                        var Jun = itm.JunAmt;
                        var JlY = itm.JulAmt;
                        var Aug = itm.AugAmt;
                        var Sep = itm.SepAmt;
                        var Oct = itm.OctAmt;
                        var Nov = itm.NovAmt;
                        var Dec = itm.DecAmt;

                        var BudgetAmount = itm.txtAmt / 12;

                        if (BudgetType == "Y")
                        {
                            string Xml_Bill_Submission = "<root>";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>1</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>2</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>3</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>4</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>5</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>6</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>7</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>8</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>9</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>10</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>11</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>12</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + BudgetAmount + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "</root> ";

                            SQRY = "EXEC usp_SetBudgetAmount_Ver2_NewPortal " + "'" + 1 + "', " + "'" + Year + "', " + "'" + YearTP + "',  " + "'" + brcd + "'," + "'" + AccountCode + "', " + "'" + Xml_Bill_Submission + "'";
                            int id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "SetBudgetAmount", "", "");

                        }

                        else
                        {
                            string Xml_Bill_Submission = "<root>";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>1</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Jan + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>2</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Feb + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>3</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Mar + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>4</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Apr + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>5</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + May + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>6</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Jun + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>7</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + JlY + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>8</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Aug + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";

                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>9</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Sep + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>10</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Oct + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>11</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Nov + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "<AccDetails>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<FinYear>" + Year + "</FinYear>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Acccode>" + itm.ACCCODE + "</Acccode>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Brcd>" + itm.LocCode + "</Brcd>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<BType>M</BType>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Month1>12</Month1>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<Amount>" + Dec + "</Amount>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                            Xml_Bill_Submission = Xml_Bill_Submission + "</AccDetails> ";


                            Xml_Bill_Submission = Xml_Bill_Submission + "</root> ";

                            SQRY = "EXEC usp_SetBudgetAmount_Ver2_NewPortal " + "'" + 2 + "', " + "'" + Year + "', " + "'" + YearTP + "',  " + "'" + brcd + "'," + "'" + AccountCode + "', " + "'" + Xml_Bill_Submission + "'";
                            int id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "SetBudgetAmount", "", "");
                        }

                        DataTable Dt = GF.GetDataTableFromSP(SQRY);

                    }

                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message :-" + ex.Message;
                return View("Error");
            }
            return RedirectToAction("SelectBranchsubmitDone");
        }

        public ActionResult SelectBranchsubmitDone()
        {

            return View();
        }

        #endregion

        #region Crossing Challen Criteria

        public ActionResult CrossingChallanBillCriteria()
        {
            CrossingChallanFilterViewModel CCFList = new CrossingChallanFilterViewModel();
            return View(CCFList);
        }

        public ActionResult CrossingChallanBillCriteriaList(CrossingChallanFilterViewModel CCF)
        {
            CrossingChallanFilterViewModel CCFList = new CrossingChallanFilterViewModel();
            CCFList.WCM = new webx_CrossingBill_MST();
            List<webx_CrossingBill_DET> BillList = new List<webx_CrossingBill_DET>();
            DataTable DT = new DataTable();
            var Accountledger = "CDA0001";
            DT = FS.GetCrossingChallanDet(CCF.VendorCode);
            if (DT.Rows.Count > 0)
            {
                CCFList.WCM.GSTNo = DT.Rows[0]["GSTNO"].ToString().Replace("\n", "");
                CCFList.WCM.PTMSNM = DT.Rows[0]["VendorName"].ToString().Replace("\n", "");
                CCFList.WCM.PTMSEMAIL = DT.Rows[0]["Email"].ToString().Replace("\n", "");
                CCFList.WCM.PTMSTEL = DT.Rows[0]["Mobileno"].ToString().Replace("\n", "");
                CCFList.WCM.PTMSADDR = DT.Rows[0]["Address"].ToString().Replace("\n", "");
            }
            else
            {
                CCFList.WCM.GSTNo = "";
                CCFList.WCM.PTMSNM = "";
                CCFList.WCM.PTMSEMAIL = "";
                CCFList.WCM.PTMSTEL = "";
                CCFList.WCM.PTMSADDR = "";
            }
            DT = FS.CrossingChallanOpeningBalance(CCF.FromDate.ToString("dd MMM yyyy"), CCF.ToDate.ToString("dd MMM yyyy"), CCF.VendorCode, BaseYearVal, Accountledger, BaseCompanyCode, BaseLocationCode);
            if (DT.Rows.Count > 0)
            {
                CCFList.WCM.OpeningBalance = Convert.ToDecimal(DT.Rows[0]["OpeningBalance"]);
            }
            else
            {
                CCFList.WCM.OpeningBalance = 0;
            }

            CCFList.WCM.Duration = Convert.ToDateTime(CCF.FromDate).ToString("dd MMM yyyy") + ":" + Convert.ToDateTime(CCF.ToDate).ToString("dd MMM yyyy");
            CCFList.WCM.VendNameStr = CCF.VendorCode + ":" + CCFList.WCM.PTMSNM;
            CCFList.WCM.BGENEMPCD = BaseUserName;
            try
            {
                BillList = FS.CrossingChallanBillList(Convert.ToDateTime(CCF.FromDate).ToString("dd MMM yyyy"), Convert.ToDateTime(CCF.ToDate).ToString("dd MMM yyyy"), Convert.ToString(CCF.VendorCode));
                CCFList.WCDB = BillList;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(CCFList);
        }

        [HttpPost]
        public ActionResult CrossingChallanBillSubmit(CrossingChallanFilterViewModel CCF, List<webx_CrossingBill_DET> CrossingChallanList)
        {
            var BillNo = "";
            var TranXaction = "";
            DataTable DT = new DataTable();
            try
            {
                string Xml_BILL_Det = "<root>";
                if (CrossingChallanList != null)
                {
                    foreach (var item in CrossingChallanList)
                    {
                        Xml_BILL_Det = Xml_BILL_Det + "<DET>";
                        Xml_BILL_Det = Xml_BILL_Det + "<CrossingChallanNo>" + item.CrossingChallanNo + "</CrossingChallanNo>";
                        Xml_BILL_Det = Xml_BILL_Det + "<CrossingChallanDT>" + Convert.ToDateTime(item.CrossingChallanDT).ToString("dd MMM yyyy") + "</CrossingChallanDT>";
                        Xml_BILL_Det = Xml_BILL_Det + "<Origin>" + item.Origin + "</Origin>";
                        Xml_BILL_Det = Xml_BILL_Det + "<DKTTOTCount>" + item.DKTTOTCount + "</DKTTOTCount>";
                        Xml_BILL_Det = Xml_BILL_Det + "<Packages>" + item.Pkgs + "</Packages>";
                        Xml_BILL_Det = Xml_BILL_Det + "<Weight>" + item.ActuWeight + "</Weight>";
                        Xml_BILL_Det = Xml_BILL_Det + "<TotalToPay>" + item.TotalTopay + "</TotalToPay>";
                        Xml_BILL_Det = Xml_BILL_Det + "<TotalCrossing>" + item.CrossingChrg + "</TotalCrossing>";
                        Xml_BILL_Det = Xml_BILL_Det + "<BulkyCharges>" + item.BulkyChrg + "</BulkyCharges>";
                        Xml_BILL_Det = Xml_BILL_Det + "<NetPayable>" + item.NetPayable + "</NetPayable>";
                        Xml_BILL_Det = Xml_BILL_Det + "</DET>";
                    }
                    Xml_BILL_Det = Xml_BILL_Det + "</root> ";
                }
                else
                {
                    return View("Error");
                }


                string Xml_BILL_Mst = "<root>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<MST>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<Manualbillno>" + CCF.WCM.Manualbillno + "</Manualbillno>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BGNDT>" + Convert.ToDateTime(CCF.WCM.BGNDT).ToString("dd MMM yyyy") + "</BGNDT>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BBRCD>" + BaseLocationCode + "</BBRCD>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BBRNM>" + BaseLocationName + "</BBRNM>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<PTMSCD>" + CCF.WCM.VendNameStr.Split(':')[0].ToString() + "</PTMSCD>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<PTMSNM>" + CCF.WCM.VendNameStr.Split(':')[1].ToString() + "</PTMSNM>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<PTMSTEL>" + CCF.WCM.PTMSTEL + "</PTMSTEL>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<PTMSEMAIL>" + CCF.WCM.PTMSEMAIL + "</PTMSEMAIL>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<PTMSADDR>" + CCF.WCM.PTMSADDR + "</PTMSADDR>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BilltypeCD>" + 15 + "</BilltypeCD>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BilltypeNM>" + "CrossingChallanBill" + "</BilltypeNM>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<REMARK>" + CCF.WCM.REMARK + "</REMARK>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BILLAMT>" + CCF.WCM.BILLAMT + "</BILLAMT>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<SUBTOTEL>" + CCF.WCM.OpeningBalance + "</SUBTOTEL>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<BGENEMPCD>" + BaseUserName + "</BGENEMPCD>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<FromDate>" + CCF.WCM.Duration.Split(':')[0] + "</FromDate>";
                Xml_BILL_Mst = Xml_BILL_Mst + "<ToDate>" + CCF.WCM.Duration.Split(':')[1] + "</ToDate>";
                Xml_BILL_Mst = Xml_BILL_Mst + "</MST>";
                Xml_BILL_Mst = Xml_BILL_Mst + "</root> ";

                DT = FS.Insert_CrossingChallan_Bill_Data(Xml_BILL_Det, Xml_BILL_Mst, BaseLocationCode, BaseUserName, BaseYearValFirst, BaseCompanyCode);
                BillNo = DT.Rows[0]["BILLNO"].ToString();
                TranXaction = DT.Rows[0]["TranXaction"].ToString();
                return RedirectToAction("CrossingChallanBillDone", new { BillNo = BillNo, TranXaction = TranXaction });

            }
            catch (Exception Ex)
            {
                ViewBag.StrError = Ex.Message;
                return View("Error");
            }

        }

        public ActionResult CrossingChallanBillDone(string BillNo, string TranXaction)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.TranXaction = TranXaction;
            return View();
        }


        #endregion
    }
}
