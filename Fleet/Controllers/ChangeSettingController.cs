﻿using CipherLib;
using Fleet.Classes;
using Fleet.Models;
using Fleet.Security;
using FleetDataService.Models;
using Microsoft.Web.WebPages.OAuth;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using Newtonsoft.Json;

namespace Fleet.Controllers
{
    [Authorize]
    public class ChangeSettingController : BaseController
    {
        //
        // GET: /ChangeSetting/

        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        GeneralFuncations GF = new GeneralFuncations();

        public ActionResult Index()
        {
            ChangeSettingsViewModel CSV = new ChangeSettingsViewModel();

            ChangeSetting CS = new ChangeSetting();
            CS.LocCode = BaseLocationCode;
            CS.FinYear = BaseFinYear;
            CS.CompanyCode = BaseCompanyCode;
            CSV.ListCompany = MS.GetCompanyMappedToEmployee(User.Identity.Name);
            CSV.ListLocation = MS.GetWorkingLocationsNewPortal(BaseLocationCode, MainLocCode, BaseUserName.ToUpper());
            CSV.ListYears = MS.GetFinacialYearDetails();
            if(BaseUserName.ToUpper() == "ADMIN")
            {                
                CSV.ListYears.Add(new vw_Get_Finacial_Years { FinYear = "2015-2016", YearVal = "2015-2016" });
                CSV.ListYears.Add(new vw_Get_Finacial_Years { FinYear = "2014-2015", YearVal = "2014-2015" });
            }
            CSV.changeSetting = CS;
            CSV.submitType = 1;
            return View(CSV);
        }

        public ActionResult popUpIndex()
        {
            ChangeSettingsViewModel CSV = new ChangeSettingsViewModel();

            ChangeSetting CS = new ChangeSetting();
            CS.LocCode = BaseLocationCode;
            CS.FinYear = BaseFinYear;
            CS.CompanyCode = BaseCompanyCode;
            CSV.ListCompany = MS.GetCompanyMappedToEmployee(User.Identity.Name);
            CSV.ListLocation = MS.GetWorkingLocationsNewPortal(BaseLocationCode, MainLocCode, BaseUserName.ToUpper());
            CSV.ListYears = MS.GetFinacialYearDetails();
            if (BaseUserName.ToUpper() == "ADMIN")
            {
                CSV.ListYears.Add(new vw_Get_Finacial_Years { FinYear = "2015-2016", YearVal = "2015-2016" });
                CSV.ListYears.Add(new vw_Get_Finacial_Years { FinYear = "2014-2015", YearVal = "2014-2015" });
            }
            CSV.changeSetting = CS;
            CSV.submitType = 2;
            return PartialView("_ChangeSetting", CSV);
        }

        //[HttpPost]
        //public ActionResult SaveSettings(ChangeSettingsViewModel CSV)
        //{
        //    CustomPrincipal.setChangeLocation(BaseUserName, CSV.changeSetting.LocCode, CSV.changeSetting.FinYear, CSV.changeSetting.CompanyCode);
        //    return RedirectToAction("Index", "Home");
        //}

        public ActionResult SaveChangeSettings(ChangeSettingsViewModel CSV)
        {
            try
            {
                CustomPrincipal.setChangeLocation(BaseUserName, CSV.changeSetting.LocCode, CSV.changeSetting.FinYear, CSV.changeSetting.CompanyCode);
                if (CSV.submitType == 1)
                {
                    return RedirectToAction("Index", "Home");
                }
                else if (CSV.submitType == 2)
                {
                    return Json("success");
                }
                else
                {
                    return Json("fail");
                }
            }
            catch (Exception)
            {
                return Json("fail");
                throw;
            }
        }

        public ActionResult ChangePassword()
        {
            ViewBag.UserId = User.Identity.Name;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    bool changePasswordSucceeded;
                    try
                    {
                        //string psSult = "WebX";
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                        string commandText1 = "Update WebX_Master_Users set LastPwd = UserPwd, UserPwd='" + model.NewPassword + "' , PwdLastChangeOn = GETDATE() Where UserId ='" + User.Identity.Name + "'";
                        DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);

                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("ChangePasswordSuccess", "ChangeSetting", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        TempData["Message"] = "The current password is incorrect or the new password is invalid.";
                        ModelState.AddModelError("", "The current password is incorrect or the new password is invalid.");
                    }
                }
            }
            else
            {
                // User does not have a local password so remove any validation errors caused by a missing
                // OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("ChangePasswordSuccess", "ChangeSetting", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("", String.Format("Unable to create local account. An account with the name \"{0}\" may already exist.", User.Identity.Name));
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult ChangePasswordSuccess(string Message)
        {
            ViewBag.Message = Message;
            return View();
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        [AllowAnonymous]
        public ActionResult DirectQuery()
        {
            return View();
        }
        [AllowAnonymous]
        public JsonResult Add_Query(string Data)
        {
            //string query = "select * from tblUsers";
            DataTable DT = GF.getdatetablefromQuery(Data);

            return Json(JsonConvert.SerializeObject(DT), JsonRequestBehavior.AllowGet);
        }
    }
}
