﻿using CipherLib;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Fleet.Models;
using CYGNUS.Classes;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class FixAssetsController : BaseController
    {
        FleetDataService.FixAssetService FAS = new FleetDataService.FixAssetService();
        FleetDataService.OperationService OS = new FleetDataService.OperationService();
        FleetDataService.ContractService CS = new FleetDataService.ContractService();
        PaymentService PS = new PaymentService();
        GeneralFuncations GF = new GeneralFuncations();

        public ActionResult FixAssets()
        {
            return View();
        }

        #region Fix Asset Master

        public ActionResult FixAssetMaster()
        {
            FixAssetsMasterViewModel objFixAssetMaster = new FixAssetsMasterViewModel();
            return View("FixAssetMaster", objFixAssetMaster);
        }

        public ActionResult GetMaster(string AccestCode)
        {
            WebxAssetmaster objAsset = new WebxAssetmaster();
            try
            {
                if (AccestCode != "0")
                {
                    string SQRY = "select UNITS as units,CATEGORY as category,ASSETCD as assetcd,* from webx_assetmaster where assetcd='" + AccestCode + "'";

                    DataTable Dt1 = GF.GetDataTableFromSP(SQRY);
                    List<WebxAssetmaster> AccountList = DataRowToObject.CreateListFromTable<WebxAssetmaster>(Dt1);
                    objAsset = AccountList.FirstOrDefault();
                }

            }
            catch (Exception)
            {

            }
            return PartialView("_AddEditFixAssetMaster", objAsset);
        }

        public JsonResult GetAssectByGroup(string AccountCode, string AccountGroup)
        {
            List<WebxAssetmaster> ObjWebxMaster = new List<WebxAssetmaster>();

            if (AccountCode != null)
            {
                ObjWebxMaster = FAS.GetAccountWiseRecord(AccountGroup, AccountCode);
                int autoid = 0;
                foreach (var item in ObjWebxMaster)
                {
                    autoid++;
                    item.Id = autoid;

                    if (item.units == "1")
                    {
                        item.units = "Nos.";
                    }
                    else if (item.units == "2")
                    {
                        item.units = "Kgs.";

                    }
                    else if (item.units == "3")
                    {
                        item.units = "Meter";

                    }
                    else if (item.units == "4")
                    {
                        item.units = "Liter";

                    }

                    if (item.category == "1")
                    {
                        item.category = "Movable";

                    }
                    else if (item.category == "2")
                    {
                        item.category = "Non-Movable";

                    }

                }
            }
            var data = (from e in ObjWebxMaster
                        select new
                        {
                            e.Id,
                            e.assetcd,
                            e.assetname,
                            e.units,
                            e.Groupcd,
                            e.category
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditMaster(WebxAssetmaster WebxMaster)
        {
            bool Status = false;
            try
            {
                DataTable dt = FAS.InsertUpdateAccount(WebxMaster.srno, WebxMaster.DEPMETHOD, WebxMaster.DEPRATE, WebxMaster.assetname, WebxMaster.GRPASSTCD, WebxMaster.units, WebxMaster.category, BaseUserName, WebxMaster.prefix, WebxMaster.assetcd, GF.FormateDate(WebxMaster.ExpiryDate));
                Status = true;
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Purchase_Order

        public ActionResult PurchaseOrder()
        {
            POOrderViewModel objViewModel = new POOrderViewModel();
            List<WebxPOASSET_det> POASSECTList = new List<WebxPOASSET_det>();
            WebxPOASSET_det objPOASSECT = new WebxPOASSET_det();
            objPOASSECT.Id = 1;
            POASSECTList.Add(objPOASSECT);
            objViewModel.ListPO = POASSECTList;
            objViewModel.podate = System.DateTime.Now;
            objViewModel.qutdate = System.DateTime.Now;
            objViewModel.reqdt = System.DateTime.Now;
            return View("PurchaseOrder", objViewModel);
        }

        public ActionResult ADDPoOrder(int id)
        {
            WebxPOASSET_det ObjPO = new WebxPOASSET_det();
            ObjPO.Id = id;

            return PartialView("_Partial_PurchaseOrder", ObjPO);
        }


        public ActionResult FAPurchaseOrderSubmit(POOrderViewModel objViewModel, List<WebxPOASSET_det> PoList, PaymentControl ObjPayment, HttpPostedFileBase[] files)
        {
            string pocd = "";
            string extension = "", UserFileName = "", Documentpath = "";
            try
            {

                #region   PO Images Upload
                try
                {
                    if (((System.Web.HttpPostedFileBase[])(files)) != null)
                    {
                        foreach (var fileobj in files)
                        {
                            var file = ((System.Web.HttpPostedFileBase[])(files));
                            if (fileobj.ContentLength > 0)
                            {
                                string directory = "";

                                directory = @"D:\SRLApplicationCode\POD\PurchaseOrder\";
                                extension = System.IO.Path.GetExtension(fileobj.FileName);
                                //UserFileName = BaseUserName + extension;
                                UserFileName = objViewModel.VENDORCD + "_" + System.DateTime.Now.ToString("dd_MM_yyyy_HH_mm_ss_tt") + "_" + fileobj.FileName;
                                //    Documentpath = Server.MapPath("D:/SRLApplicationCode/POD/OtherBillEntry/") + UserFileName;
                                //  string strDirectoryName = Server.MapPath("D:/SRLApplicationCode/POD/OtherBillEntry/");

                                fileobj.SaveAs(Path.Combine(directory, UserFileName));

                                if (Directory.Exists(directory) == false)
                                    Directory.CreateDirectory(directory);
                                // fileobj.SaveAs(Documentpath);

                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    // DocumentUploadedPath = Em.Elec_Expense_DET.b;
                }

                #endregion




                objViewModel.podate = Convert.ToDateTime(GF.FormateDate((objViewModel.podate)));
                objViewModel.Documentpath = UserFileName;
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(objViewModel.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, objViewModel);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }

                string XML_Det = "<root>";
                foreach (var item in PoList)
                {
                    XML_Det = XML_Det + "<WebxPOASSET_det>";
                    XML_Det = XML_Det + "<podate>" + GF.FormateDate(objViewModel.podate) + "</podate>";
                    XML_Det = XML_Det + "<assetcd>" + item.assetcd + "</assetcd>";
                    XML_Det = XML_Det + "<qty>" + item.qty + "</qty>";
                    XML_Det = XML_Det + "<rate>" + item.rate + "</rate>";
                    XML_Det = XML_Det + "<narration>" + item.narration + "</narration>";
                    XML_Det = XML_Det + "<tax_per>" + item.tax_per + "</tax_per>";
                    XML_Det = XML_Det + "<OtherCharge>" + item.OtherCharge + "</OtherCharge>";
                    XML_Det = XML_Det + "<total>" + item.total + "</total>";
                    XML_Det = XML_Det + "<HSN_SAC>" + item.HSN_SAC + "</HSN_SAC>";
                    XML_Det = XML_Det + "<taxableAmt>" + item.taxableAmt + "</taxableAmt>";
                    XML_Det = XML_Det + "<TaxAmount>" + item.TaxAmount + "</TaxAmount>";
                    XML_Det = XML_Det + "</WebxPOASSET_det>";
                }
                XML_Det = XML_Det + "</root>";
                DataTable dtPOList = FAS.InsertFAPoList(xmlDoc.InnerXml, XML_Det.ReplaceSpecialCharacters(), BaseUserName, BaseLocationCode, BaseFinYear);

                if (dtPOList.Rows[0][0].ToString() == "Done" && dtPOList.Rows[0][1].ToString() == "1")
                {
                    pocd = dtPOList.Rows[0][2].ToString();
                }
                else
                {
                    ViewBag.StrError = "Error ! Some issue in generation.";
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("POOrderDone", new { PONO = pocd });
        }



        public ActionResult AddPurchaseOrder(POOrderViewModel objViewModel, List<WebxPOASSET_det> PoList, PaymentControl ObjPayment)
        {
            string pocd = "";
            try
            {
                DataTable dt = new DataTable();
                dt = FAS.getnewcd(BaseLocationCode, BaseFinYear);
                pocd = dt.Rows[0]["NextDocumentCode"].ToString();//Next Code
                objViewModel.pocode = pocd;
                objViewModel.podate = Convert.ToDateTime(GF.FormateDate((objViewModel.podate)));

                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(objViewModel.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, objViewModel);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                DataTable dtPO = FAS.sp_Insert_PurchseOrderDetail(BaseLocationCode, xmlDoc.InnerXml, BaseUserName);
                string XML_Det = "<root>";
                foreach (var item in PoList)
                {
                    XML_Det = XML_Det + "<WebxPOASSET_det>";
                    XML_Det = XML_Det + "<pocd>" + pocd + "</pocd>";
                    XML_Det = XML_Det + "<podate>" + GF.FormateDate(objViewModel.podate) + "</podate>";
                    XML_Det = XML_Det + "<assetcd>" + item.assetcd + "</assetcd>";
                    XML_Det = XML_Det + "<qty>" + item.qty + "</qty>";
                    XML_Det = XML_Det + "<rate>" + item.rate + "</rate>";
                    XML_Det = XML_Det + "<narration>" + item.narration + "</narration>";
                    XML_Det = XML_Det + "<tax_per>" + item.tax_per + "</tax_per>";
                    XML_Det = XML_Det + "<OtherCharge>" + item.OtherCharge + "</OtherCharge>";
                    XML_Det = XML_Det + "<total>" + item.total + "</total>";
                    XML_Det = XML_Det + "</WebxPOASSET_det>";
                }
                XML_Det = XML_Det + "</root>";
                DataTable dtPOList = FAS.InsertPoList(XML_Det.ReplaceSpecialCharacters(), BaseUserName, BaseLocationCode);

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("POOrderDone", new { PONO = pocd });
        }

        public ActionResult POOrderDone(string PONO)
        {
            POOrderViewModel pvm = new POOrderViewModel();
            pvm.pocode = PONO;
            return View(pvm);
        }

        public JsonResult SearchAssetListJson(string searchTerm)
        {
            List<WebxAssetmaster> ListAssect = new List<WebxAssetmaster>();

            ListAssect = FAS.ListAsserMaster().Where(c => c.assetcd.ToUpper().Contains(searchTerm.ToUpper()) || c.assetname.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.assetname).ToList();
            var SearchList = (from e in ListAssect
                              select new
                              {
                                  id = e.assetcd,
                                  text = e.assetname,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult vendorCodeChngeBindCreditAc(string VendorCode)
        {
            DataTable dt = new DataTable();
            string CreditAccCode = "";
            try
            {
                string sql = "SELECT LTRIM(RTRIM(WAI.Acccode))  + ' : ' + LTRIM(RTRIM(WAI.Accdesc)) AS CreditAccCode FROM webx_VENDOR_HDR WVH WITH(NOLOCK) " +
                              "INNER JOIN webx_AcctHead WAH WITH(NOLOCK) ON WVH.Vendor_Type = WAH.CodeId " +
                              "INNER JOIN webx_acctinfo WAI WITH(NOLOCK) ON WAH.accthead = WAI.Acccode " +
                              "WHERE WVH.VENDORCODE='" + VendorCode + "'";
                dt = GF.GetDataTableFromSP(sql);
                CreditAccCode = dt.Rows[0]["CreditAccCode"].ToString();
                return new JsonResult()
                {
                    Data = new
                    {
                        CreditAccCode = CreditAccCode,
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        //CreditAcCode = CreditAcCode,
                    }
                };
            }
        }

        #endregion

        #region Fix Asset Exception View/Print Report

        public ActionResult POViewPrintFixAsset()
        {
            return View();
        }

        public ActionResult POViewFixAssetList(FixAssetPrintViewModel vm)
        {
            if (vm.FixAssetModel != null)
            {
                List<vm_FixAssetList> optrkList = FAS.Get_PO_Listing_For_View_Print(vm.FixAssetModel.FilterType, vm.FixAssetModel.PartyCode, GF.FormateDate(vm.FixAssetModel.FromDate), GF.FormateDate(vm.FixAssetModel.ToDate), vm.FixAssetModel.BillNo);
                vm.ListFixAsset = optrkList;
            }
            else
            {
                vm = new FixAssetPrintViewModel();
                vm.ListFixAsset = new List<vm_FixAssetList>();
            }
            return View(vm);
        }


        public ActionResult POFixAssetReport()
        {
            return View();
        }

        public ActionResult POFixAssetReportDetail(FixAssetPrintViewModel vm)
        {
            return View(vm);
        }

        public ActionResult POCancellationCriteria()
        {
            return View();
        }

        public ActionResult POCancellationList(FixAssetPrintViewModel vm)
        {
            if (vm.FixAssetModel != null)
            {
                List<vm_PoCancellationList> optrkList = FAS.Get_PO_Listing_For_Cancellation(vm.FixAssetModel.BillNo, vm.FixAssetModel.PartyCode, GF.FormateDate(vm.FixAssetModel.FromDate), GF.FormateDate(vm.FixAssetModel.ToDate), BaseLocationCode);
                vm.ListPoCancellation = optrkList;
            }
            else
            {
                vm = new FixAssetPrintViewModel();
                vm.ListPoCancellation = new List<vm_PoCancellationList>();
            }
            return View(vm);
        }

        public ActionResult POCancellationSubmit(List<vm_PoCancellationList> PoCanList, FixAssetPrintViewModel vm)
        {
            String hidPO = "";
            try
            {
                if (PoCanList != null && PoCanList.Count() > 0)
                {
                    foreach (var item in PoCanList)
                    {
                        if (item.Active == "Y")
                        {
                            DataTable objDT = new DataTable();
                            objDT = FAS.POCancellationSubmit(vm.FixAssetModel.selectDate, item.pocode, vm.FixAssetModel.CancelReason, BaseUserName, BaseCompanyCode);

                            if (hidPO == "")
                            {
                                hidPO = item.pocode;
                            }
                            else
                            {
                                hidPO = hidPO + "," + item.pocode;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("POCancellationDone", new { CancelledPO = hidPO });
        }

        public ActionResult POCancellationDone(string CancelledPO)
        {
            ViewBag.CancelledPO = CancelledPO;
            return View();
        }

        #endregion

        #region GoodReceipt Note

        public ActionResult GoodsRecieptNoteCretira()
        {
            FuelBillEntyQuery objViewModel = new FuelBillEntyQuery();
            TempData["GRN"] = "Other";
            objViewModel.Type = 5;
            return View("GoodsRecieptNoteCretira", objViewModel);
        }

        public ActionResult GoodsReceipt(string Id, FuelBillEntyQuery FBE, ExpenseBillEntryQuery EBE)
        {
            string VendorCode = "";
            FBE.Location = BaseLocationCode;

            BABillEnty BBE = new BABillEnty();

            WEBX_VENDORBILL_HDR WVH = new WEBX_VENDORBILL_HDR();
            BBE.WVH = WVH;
            BBE.WVH.VENDORCODE = FBE.VendorCode;
            BBE.WVH.FROMDATE = GF.FormateDate(FBE.FromDate);
            BBE.WVH.TODATE = GF.FormateDate(FBE.ToDate);
            BBE.WVH.VENDORNAME = FBE.VendorCode + " - " + FBE.VendorName;
            BBE.WVH.NVENDORNAME = FBE.VendorName;
            BBE.WVH.PoNo = FBE.VehicleNo;
            BBE.WVH.GSTType = FBE.GSTType;
            BBE.WVH.StateCode = FBE.StateCode;
            POBillEntry objPOBill = new POBillEntry();
            VendorDetails VD = new VendorDetails();
            objPOBill = FAS.GetPOForBillEntry(FBE).FirstOrDefault();


            if (BBE.WVH.VENDORCODE != "")
                VendorCode = BBE.WVH.VENDORCODE;
            else
                VendorCode = objPOBill.Vendorcode;
            if (objPOBill != null)
            {
                if (objPOBill.pocode != "" && objPOBill.pocode != null)
                    BBE.WVH.PoNo = objPOBill.pocode;
            }

            VD = PS.GetVendorTypeLedger(VendorCode).FirstOrDefault();
            if (VD != null)
            {
                string Accdesc = OS.getAccountDesc("webx_acctinfo", VD.Acccode);
                // BBE.WVH.Totalamt = VD.Acccode + "-" + Accdesc;
            }
            //list
            if (objPOBill != null)
            {
                BBE.WVH.Totalamt = objPOBill.Totalamt;
                BBE.WVH.VENDORNAME = objPOBill.vendorcd;
                BBE.POList = FAS.GetPOForBillEntry(FBE);
            }

            return View(BBE);
        }

        public ActionResult PendingPOList(string POCode)
        {
            BABillEnty BBE = new BABillEnty();
            BBE.POList = FAS.GetPOPendingList(POCode);
            return View("PendingPOList", BBE);
        }

        public ActionResult PendingBillSubmit(BABillEnty BBE, List<POBillEntry> PendingPOList)
        {
            string Billno = "";
            string Vendorcode = "";
            try
            {
                DataTable dtNextCode = new DataTable();
                string Sql = "exec WebX_SP_GetNextDocumentCode_FA '" + BaseLocationCode + "','" + BaseFinYear + "','GRN'";
                dtNextCode = GF.GetDataTableFromSP(Sql);

                Billno = dtNextCode.Rows[0]["NextDocumentCode"].ToString();
                string vendorcd1 = "", pocode1 = "";
                decimal Total_Qty1 = 0, tot_Amt1 = 0, FinalAmt = 0, FinalAmt1 = 0;
                foreach (var item in PendingPOList)
                {
                    vendorcd1 = item.vendorcd;
                    pocode1 = item.pocode;
                    Total_Qty1 = Total_Qty1 + item.qty;
                    decimal subamt = (item.qty * item.rate);
                    decimal tottax = 0;
                    if (item.tax_per > 0)
                    {
                        tottax = (subamt * Convert.ToDecimal(item.tax_per)) / 100;
                    }
                    else
                    {
                        tottax = 0;
                    }
                    tot_Amt1 = subamt + tottax + item.OtherCharge;

                    /* Start Changed from partial PO to full PO GOTI*/
                    DataTable dt = CS.GetVendorName(item.vendorcd);
                    Vendorcode = item.vendorcd + " ~ " + dt.Rows[0]["CustName"].ToString();
                    DataTable dtList = FAS.InsertUpdateBill(Billno, item.pocode, item.assetcd, item.narration, item.pocode, item.qty, item.rate, item.OtherCharge, tot_Amt1, BaseUserName, BaseLocationCode);
                    FinalAmt += tot_Amt1;

                    /*Start OLD Login as it is*/
                    //if (item.IsEnabled == true)
                    //{
                    //    DataTable dt = CS.GetVendorName(item.vendorcd);
                    //    Vendorcode = item.vendorcd + " ~ " + dt.Rows[0]["CustName"].ToString();
                    //    DataTable dtList = FAS.InsertUpdateBill(Billno, item.pocode, item.assetcd, item.narration, item.pocode, item.qty, item.rate, item.OtherCharge, tot_Amt1, BaseUserName, BaseLocationCode);
                    //}
                    //else
                    //{
                    //    string sql = "update webx_poasset_Hdr set postatus='PO GENERATED' where pocode='" + item.pocode + "' ";
                    //    DataTable dt = GF.GetDataTableFromSP(sql);
                    //}
                    /*End OLD Login as it is*/

                    /* End Changed from partial PO to full PO GOTI*/
                }
                FinalAmt1 = FinalAmt;
                string sql1 = "INSERT INTO webx_FIXEDASSETS_HDR(FIXEDASSETCD,fixedassetDATE,VENDORCD,totalqty,ENTRYBY,ENTRYON,postatus,pocode,BRCD,poamt) " +
                    " VALUES ('" + Billno + "',GETDATE(),'" + vendorcd1 + "'," + Total_Qty1 + ",'" + BaseUserName + "',GETDATE(),'GRN Generated','" + pocode1 + "','" + BaseLocationCode + "'," + FinalAmt1 + ")";
                DataTable dt1 = GF.GetDataTableFromSP(sql1);
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("GoodsReceiptDone", new { GRNNO = Billno, Vendorcode = Vendorcode });
        }

        public ActionResult GoodsReceiptDone(string GRNNO, string Vendorcode)
        {
            ViewBag.BillNo = GRNNO;
            ViewBag.VendorName = Vendorcode;

            return View("GoodsReceiptDone");
        }

        #endregion

        #region Assign Asset

        public ActionResult AssignAssetCretira()
        {
            FuelBillEntyQuery objViewModel = new FuelBillEntyQuery();
            TempData["GRN"] = "GRN";
            return View("AssignAssetCretira", objViewModel);
        }

        public ActionResult AssignAssectList(FuelBillEntyQuery FBE)
        {
            BABillEnty BBE = new BABillEnty();
            FBE.Location = BaseLocationCode;
            BBE.POList = FAS.GetAssignAssect(FBE);
            return View("AssignAssectList", BBE);
        }

        public ActionResult AssignAssectSubmit(List<POBillEntry> AssectList)
        {
            string GRN = "", GRNList = "", allotcd = "";
            try
            {
                foreach (var item in AssectList)
                {
                    if (item.IsEnabled == true)
                    {
                        GRN = FAS.InsertUpdateAssets(item.AssignLoc, item.FIXEDASSETCD, item.DpartName, item.Employee, GF.FormateDate(item.InstallDate), item.allotcd, item.Employee, item.Description, item.MFGSrNo, item.RCPLSrNo);
                        if (allotcd == "")
                        {
                            allotcd = item.allotcd.ToString();
                        }
                        else
                        {
                            allotcd = allotcd + "," + item.allotcd;
                        }
                    }
                }
                string sql = "select distinct fixedassetcd from webx_fixedassets_hdr where fixedassetcd in (" + GRN + ")";
                DataTable dt = GF.GetDataTableFromSP(sql);
                GRNList = dt.Rows[0]["fixedassetcd"].ToString();
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("AssignAssectDone", new { GRN = GRNList, allotcd = allotcd });

        }

        public ActionResult AssignAssectDone(string GRN, string allotcd)
        {
            ViewBag.GRN = GRN;
            ViewBag.allotcd = allotcd;


            return View("AssignAssectDone");
        }

        public JsonResult CheckMGFNo(string DocNo)
        {
            decimal count = 0;
            var DcoNo = "";
            if (DcoNo == "" || DcoNo == null)
            {
                DataTable Dt = FAS.CheckMFGSrNoDuplicate(DocNo);
                if (Dt.Rows.Count > 0)
                {
                    count = 1;
                }
                else
                {
                    count = 0;
                }
                return Json(new { CNT = count, DcoNo = DcoNo }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { CNT = 1 }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region User Maual (ppt)

        public FileResult DownloadZip()
        {
            string path = Server.MapPath("~") + "/assets/FA_SOP.zip";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = "FA_SOP.zip";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        public FileResult DownloadXls()
        {
            string path = Server.MapPath("~") + "/assets/FA_Accounting.xls";
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = "FA_Accounting.xls";
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        #endregion

        #region Fix Asset Dump Sale Reassign Utility

        #region  Asset Dump Utility

        public ActionResult AssetDumpUtilityCretira()
        {
            AssetUTILITYViewModel objViewModel = new AssetUTILITYViewModel();
            objViewModel.ASSETTYPE = "Dump";
            return View(objViewModel);
        }

        #endregion

        #region  Asset Sale Utility

        public ActionResult AssetSaleUtility()
        {
            AssetUTILITYViewModel objViewModel = new AssetUTILITYViewModel();
            objViewModel.ASSETTYPE = "Sale";
            return View(objViewModel);
        }

        #endregion

        #region  Fix Asset Reassign Utility

        public ActionResult FixAssetReassignUtility()
        {
            AssetUTILITYViewModel objViewModel = new AssetUTILITYViewModel();
            objViewModel.ASSETTYPE = "Reassign";
            return View(objViewModel);
        }

        #endregion

        public ActionResult AssetDumpUtilityCretiraList(AssetUTILITYViewModel objViewModel)
        {
            try
            {
                if (BaseLocationCode == "HQTR")
                {
                    objViewModel.WFTDLIST = FAS.GetDumpDetail(GF.FormateDate(objViewModel.FromDate), GF.FormateDate(objViewModel.ToDate), objViewModel.ITEMCODE, objViewModel.LocCode);
                }
                else
                {
                    objViewModel.WFTDLIST = FAS.GetDumpDetail(GF.FormateDate(objViewModel.FromDate), GF.FormateDate(objViewModel.ToDate), objViewModel.ITEMCODE, BaseLocationCode);
                }

                if (objViewModel.ASSETTYPE == "Dump")
                {
                    ViewBag.ASSETTYPE = "Dump";
                    ViewBag.Header = "Fix Asset Dump UTILITY List";

                }
                else if (objViewModel.ASSETTYPE == "Sale")
                {
                    ViewBag.ASSETTYPE = "Sale";
                    ViewBag.Header = "Fix Asset Sale UTILITY List";

                }
                else if (objViewModel.ASSETTYPE == "Reassign")
                {
                    ViewBag.ASSETTYPE = "Reassign";
                    ViewBag.Header = "Fix Asset Reassign UTILITY List";

                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return PartialView("_AssetDumpUtility", objViewModel);

        }

        public ActionResult FixAssetTypeListSubmit(AssetUTILITYViewModel objViewModel, List<webx_FAALLOT_DET> WFTDLIST)
        {
            try
            {
                string Type = "0";
                List<webx_FAALLOT_DET> ListWFTDLIST = new List<webx_FAALLOT_DET>();
                string FIXEDASSETCD = "";
                string IteamCode = "";
                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        FIXEDASSETCD = item.FIXEDASSETCD;
                        IteamCode = item.IteamCode;
                    }
                }

                string XML_Det = "<root>";

                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        webx_FAALLOT_DET WFTD = new webx_FAALLOT_DET();

                        if (item.CheckBox)
                        {
                            XML_Det = XML_Det + "<Webx_FixAssetType>";
                            if (objViewModel.ASSETTYPE == "Dump" || objViewModel.ASSETTYPE == "Sale" || objViewModel.ASSETTYPE == "Reassign")
                            {
                                XML_Det = XML_Det + "<Branch>" + item.Branch + "</Branch>";
                                XML_Det = XML_Det + "<ItemCode>" + item.IteamCode + "</ItemCode>";
                                XML_Det = XML_Det + "<MFGSrNo>" + item.MFGSrNo + "</MFGSrNo>";
                                XML_Det = XML_Det + "<RCPLSrNo>" + item.RCPLSrNo + "</RCPLSrNo>";
                                XML_Det = XML_Det + "<Qty>" + item.Qty + "</Qty>";
                            }
                            if (objViewModel.ASSETTYPE == "Dump")
                            {
                                XML_Det = XML_Det + "<DumpToLocation>" + item.DumpToLocation + "</DumpToLocation>";
                                XML_Det = XML_Det + "<DumpType>" + item.DumpType + "</DumpType>";
                                XML_Det = XML_Det + "<Type>1</Type>";
                                Type = "1";
                            }
                            if (objViewModel.ASSETTYPE == "Sale")
                            {
                                XML_Det = XML_Det + "<SaleToWhom>" + item.SaleToWhom + "</SaleToWhom>";
                                XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                                XML_Det = XML_Det + "<Type>2</Type>";
                                Type = "2";
                            }
                            if (objViewModel.ASSETTYPE == "Reassign")
                            {
                                XML_Det = XML_Det + "<ReassignToLoc>" + item.ReassignToLoc + "</ReassignToLoc>";
                                XML_Det = XML_Det + "<Employee>" + item.Employee.Split(':')[0].ToString() + "</Employee>";
                                XML_Det = XML_Det + "<Description>" + item.Description + "</Description>";
                                XML_Det = XML_Det + "<DpartName>" + item.DpartName + "</DpartName>";
                                XML_Det = XML_Det + "<Type>3</Type>";
                                Type = "3";
                            }
                            XML_Det = XML_Det + "<activeflag>" + item.activeflag + "</activeflag>";
                            XML_Det = XML_Det + "</Webx_FixAssetType>";

                            ListWFTDLIST.Add(WFTD);
                        }
                    }
                }
                XML_Det = XML_Det + "</root>";
                DataTable DT = new DataTable();
                string Dump = "";
                string ReassignToLocation = "";
                string IsDump = "";

                if (objViewModel.ASSETTYPE == "Dump")
                {
                    foreach (var item in WFTDLIST)
                    {
                        if (item.CheckBox)
                        {
                            if (Dump == "")
                            {
                                Dump = item.allotcd.ToString();
                                IsDump = "N";
                            }
                            else
                            {
                                Dump = Dump + "," + item.allotcd;
                                IsDump = "N";
                            }

                            if (item.DumpType == "Scrap")
                            {
                                IsDump = "Y";
                            }
                        }


                    }
                }
                else if (objViewModel.ASSETTYPE == "Reassign" || objViewModel.ASSETTYPE == "Sale")
                {
                    foreach (var item in WFTDLIST)
                    {
                        if (item.CheckBox)
                        {
                            if (Dump == "")
                                Dump = item.allotcd.ToString();
                            else
                                Dump = Dump + "," + item.allotcd;

                            ReassignToLocation = item.ReassignToLoc;
                            if (objViewModel.ASSETTYPE == "Reassign")
                            {
                                IsDump = "Y";
                            }
                        }
                    }
                }
                else
                {
                    Dump = "Y";
                }
                DT = FAS.InsertFixAssetType(XML_Det, BaseLocationCode, BaseUserName, BaseYearValFirst, IteamCode, FIXEDASSETCD, Dump, IsDump, ReassignToLocation, Type);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();
                string No = DT.Rows[0]["No"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("FixAssetUtilityDone", new { No = No, Type = Type });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FixAssetUtilityDone(string No, int Type)
        {
            ViewBag.No = No;
            ViewBag.Type = Type;
            return View();
        }

        #endregion

        #region  Fix Asset  Utility

        public ActionResult FixAssetUtility()
        {
            AssetUTILITYViewModel objViewModel = new AssetUTILITYViewModel();
            return View(objViewModel);
        }

        public ActionResult FixAssetUtilityCretiraList(AssetUTILITYViewModel objViewModel)
        {

            try
            {
                objViewModel.CFALIST = FAS.GetFixAssetDetail(objViewModel);

                if (objViewModel.TYPE == "Dump")
                {
                    ViewBag.ASSETTYPE = "Dump";
                }
                else if (objViewModel.TYPE == "Sale")
                {
                    ViewBag.ASSETTYPE = "Sale";
                }
                else if (objViewModel.TYPE == "Reassign")
                {
                    ViewBag.ASSETTYPE = "Reassign";
                }
                else if (objViewModel.TYPE == "Assign")
                {
                    ViewBag.ASSETTYPE = "Assign";
                }
                else if (objViewModel.TYPE == "GRN")
                {
                    ViewBag.ASSETTYPE = "GRN";
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return PartialView("_FixAssetUitylityList", objViewModel);

        }

        public JsonResult GetReassignLocationsDetails(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_location> CMP = FAS.GetReassignLocations(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Assign Asset View Print

        public ActionResult AssetAssignCretiraViewPrint()
        {
            FuelBillEntyQuery objViewModel = new FuelBillEntyQuery();
            TempData["GRN"] = "GRN";
            return View("AssetAssignCretiraViewPrint", objViewModel);
        }

        public ActionResult AssectAssignViewPrintList(FuelBillEntyQuery FBE)
        {
            BABillEnty BBE = new BABillEnty();
            FBE.Location = BaseLocationCode;
            BBE.POList = FAS.GetAssignAssectViewPrint(FBE);
            return View("AssectAssignViewPrintList", BBE);
        }

        #endregion

        #region Fix Asset Uitility View Print

        public ActionResult FixAssetUitilityViewPrint()
        {
            return View();
        }

        public ActionResult FixAssetUitilityViewPrintList(AssetUTILITYViewModel objViewModel)
        {
            try
            {
                List<CYGNUS_FixAssetType> optrkList = FAS.Get_FixAssetViewPrintListView(objViewModel.DocumentNo, objViewModel.GRNNo, GF.FormateDate(objViewModel.FromDate), GF.FormateDate(objViewModel.ToDate), objViewModel.UitilityType);
                objViewModel.CFALIST = optrkList;
                if (objViewModel.UitilityType == "1")
                {
                    ViewBag.ASSETTYPE = "Dump";
                    ViewBag.Header = "Fix Asset Dump UTILITY List";

                }
                else if (objViewModel.UitilityType == "2")
                {
                    ViewBag.ASSETTYPE = "Sale";
                    ViewBag.Header = "Fix Asset Sale UTILITY List";

                }
                else if (objViewModel.UitilityType == "3")
                {
                    ViewBag.ASSETTYPE = "Reassign";
                    ViewBag.Header = "Fix Asset Reassign UTILITY List";

                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }


            return View(objViewModel);
        }

        #endregion

        #region Fix Asset Uitility GRN View Print

        public ActionResult FixAssetUitilityGRNViewPrint()
        {
            return View();
        }

        public ActionResult FixAssetUitilityGRNViewPrintList(AssetUTILITYViewModel objViewModel)
        {
            try
            {
                List<CYGNUS_FixAssetType> optrkList = FAS.Get_GRNViewPrintListView(objViewModel.GRNNo, GF.FormateDate(objViewModel.FromDate), GF.FormateDate(objViewModel.ToDate), objViewModel.VENCD);
                objViewModel.CFALIST = optrkList;
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

            return View(objViewModel);
        }

        #endregion

        #region Fix Asset Assign and Reassign Combine

        public ActionResult AssetAssignReassignCriteria()
        {
            ViewBag.TypeAsset = 1;
            TempData["GRN"] = "GRN";
            return View();
        }

        public JsonResult GetDepartmentNameFromEmpcode(string EmpCode)
        {
            DataTable dt = new DataTable();
            string DptName = "";
            try
            {
                string sql = "SELECT Department FROM WebX_Master_Users WHERE UserId='" + EmpCode + "'";
                dt = GF.GetDataTableFromSP(sql);
                DptName = dt.Rows[0]["Department"].ToString();
                return new JsonResult()
                {
                    Data = new
                    {
                        DptName = DptName,
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        //DptName = DptName,
                    }
                };
            }
        }
        #endregion

        #region Wave Off of Fix Assets

        public ActionResult WaveOffFixAssetsCriteria()
        {
            return View();
        }

        public ActionResult WaveOffFixAssetsList(AssetUTILITYViewModel objViewModel)
        {
            try
            {
                objViewModel.WFTDLIST = FAS.GetWaveList(objViewModel.ITEMCODE, objViewModel.LocCode, objViewModel.VENCD);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return View(objViewModel);
        }

        public ActionResult WaveOffFixAssetSubmit(List<webx_FAALLOT_DET> WFTDLIST)
        {
            try
            {
                string xml = "<root>";
                string AssetCode = "";
                foreach (var item in WFTDLIST)
                {
                    if (item.CheckBox == true)
                    {
                        xml = xml + "<WavaOff>";
                        xml = xml + "<IssWaveOff>" + item.CheckBox + "</IssWaveOff>";
                        xml = xml + "<allotcd>" + item.allotcd + "</allotcd>";
                        xml = xml + "<WavaUpdateBy>" + BaseUserName + "</WavaUpdateBy>";
                        xml = xml + "</WavaOff>";
                        AssetCode = item.IteamCode + ',' + AssetCode;
                    }
                }
                xml = xml + "</root>";
                DataTable DT = FAS.usp_XML_Wave_Submmit(xml);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("WaveOffFixAssetDone", new { Message = Message, AssetCode = AssetCode });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult WaveOffFixAssetDone(string Message, string AssetCode)
        {
            ViewBag.Message = Message;
            ViewBag.AssetCode = AssetCode;
            return View();
        }
        #endregion

    }
}









