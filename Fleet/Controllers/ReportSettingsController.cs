﻿using Fleet.Classes;
using Fleet.ViewModel;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;

namespace Fleet.Controllers
{
    public class ReportSettingsController : BaseController
    {

        ReportService CL = new ReportService();
        GeneralFuncations GF = new GeneralFuncations();

        //#region Add Report

        //public ActionResult Index()
        //{
        //    MenuObj1 mc = new MenuObj1();
        //    DataTable dtMenu = CL.GetParentMenu("A");
        //    mc.ListMenu = DataRowToObject.CreateListFromTable<Master_Menu>(dtMenu);
        //    dtMenu = CL.GetParentMenu("P");
        //    mc.ListParentMenu = DataRowToObject.CreateListFromTable<Master_Menu>(dtMenu);
            
        //    mc.IsActive = true;
        //    return View(mc);
        //}

        //[HttpPost]
        //public ActionResult AddMenu(int MenuId, string DisplayName, int ParentID, int DisplayRank, string NavigationURL, bool HasAccess, bool IsActive, bool IsQuickLink, bool IsHomeDisplay, string RDLName, int MenuType, int MenuDisplayType, int ParameterSet)
        //{
        //    string query = null;
        //    query = "exec [USP_Add_NewMenu] @MenuId='" + MenuId + "', @DisplayName='" + DisplayName + "',@ParentID='" + ParentID + "' ,@DisplayRank='" + DisplayRank + "',@NavigationURL='" + NavigationURL + "',@IsUserAccess='" + HasAccess + "',@IsActive='" + IsActive + "',@IsQuickLink='" + IsQuickLink + "',@IsHomeDisplay='" + IsHomeDisplay + "',@RDLName='" + RDLName + "',@MenuLevel='1',@MenuType='" + MenuType + "',@MenuDisplayType='" + MenuDisplayType + "', @ParameterSet ='" + ParameterSet + "' ";

        //    DataTable dtEntity = GF.getdatetablefromQuery(query);
        //    List<Master_Menu> MenuList = new List<Master_Menu>();
        //    string query1 = "USP_GetParentMenu 'A'";
        //    DataTable dtMenu = GF.GetDataTableFromSP(query1);
        //    MenuList = DataRowToObject.CreateListFromTable<Master_Menu>(dtMenu);

        //    return PartialView("_MenuList", MenuList);
        //}

        //public ActionResult EditMenu(int id)
        //{
        //    MenuObj1 mc = new MenuObj1();
        //    DataTable dtMenu = CL.GetParentMenu("A");
        //    DataTable dtparent = CL.GetParentMenu("P");
        //    mc = DataRowToObject.CreateListFromTable<MenuObj1>(dtMenu).Where(c => c.MenuId == id).FirstOrDefault();
        //    mc.ListMenu = DataRowToObject.CreateListFromTable<Master_Menu>(dtMenu);
        //    mc.ListParentMenu = DataRowToObject.CreateListFromTable<Master_Menu>(dtparent);
        //    return PartialView("_PartialAdd", mc);
        //}


        //#endregion

        public class Parameters
        {
            public int Id { get; set; }
            public string ParameterName { get; set; }
            public string SourceTableName { get; set; }
            public string IdColumn { get; set; }
            public string NameColumn { get; set; }
            public bool IsMultipleChoice { get; set; }
            public int ParameterType { get; set; }
            public bool IsActive { get; set; }
            public Master_ParameterSet_Details headerParameter { get; set; }
            public IEnumerable<Master_ParameterSet_Details> ParameterSet_Details { get; set; }
            public IEnumerable<Master_ParameterSet_Details> ListPara { get; set; }
        }

        #region Add Parameter

        public ActionResult AddParameters()
        {
            Parameters prObj = new Parameters();
            string query1 = "Usp_GetAllParameter";
            DataTable dtPara = GF.GetDataTableFromSP(query1);
            prObj.ListPara = DataRowToObject.CreateListFromTable<Master_ParameterSet_Details>(dtPara);
            prObj.IsActive = true;
            return View(prObj);
        }

        [HttpPost]
        public ActionResult AddParameters(int Id, string ParameterName, string SourceTableName, string IdColumn, string NameColumn, int ParameterType, bool IsMultipleChoice, bool IsActive)
        {
            string query = null;
            query = "exec [Usp_AddParameters] @Id='" + Id + "', @ParameterName='" + ParameterName + "',@SourceTableName='" + SourceTableName + "' ,@IdColumn='" + IdColumn + "',@NameColumn='" + NameColumn + "',@IsMultipleChoice='" + IsMultipleChoice + "',@ParameterType='" + ParameterType + "',@IsActive='" + IsActive + "' ";
            DataTable dtEntity = GF.getdatetablefromQuery(query);
            List<Master_ParameterSet_Details> ParaList = new List<Master_ParameterSet_Details>();
            string query1 = "Usp_GetAllParameter";
            DataTable dtPara = GF.GetDataTableFromSP(query1);
            ParaList = DataRowToObject.CreateListFromTable<Master_ParameterSet_Details>(dtPara);

            return PartialView("_ParameterList", ParaList);
        }

        public ActionResult EditPerameter(int id)
        {
            Parameters editRecord = new Parameters();
            string qry = "Usp_GetAllParameter";
            DataTable dtPara = GF.GetDataTableFromSP(qry);
            editRecord = DataRowToObject.CreateListFromTable<Parameters>(dtPara).Where(c => c.Id == id).FirstOrDefault();
            return PartialView("_PartialAddParameter", editRecord);
        }


        #endregion


        #region ParameterSet

        public ActionResult ParameterSetList()
        {
            List<Master_ParameterSet> ListSetPara = new List<Master_ParameterSet>();
            //string query1 = "Usp_GetAllParameterSet";
            //DataTable dtPara = GF.GetDataTableFromSP(query1);
            //ListSetPara = DataRowToObject.CreateListFromTable<Master_ParameterSet>(dtPara);
            ListSetPara = CL.Usp_GetAllParameterSet();
            return View(ListSetPara);
        }

        public ActionResult AddEditParameterSet(int Id)
        {
            ParameterSetViewModel PSM = new ParameterSetViewModel();
            Master_ParameterSet objMPS = new Master_ParameterSet();
            if (Id > 0)
            {
                //string query1 = "Usp_GetAllParameterSet";
                //DataTable dtPara = GF.GetDataTableFromSP(query1);
                //PSM.ParameterSet = DataRowToObject.CreateListFromTable<Master_ParameterSet>(dtPara).Where(c => c.Id == Id).FirstOrDefault();
                PSM.ParameterSet = CL.Usp_GetAllParameterSet().Where(c => c.Id == Id).FirstOrDefault();
            }
            else
            {
                objMPS.IsActive = true;
                PSM.ParameterSet = objMPS;
            }

            PSM.ParameterSet_Details = CL.USP_GETParameterDetails_ForSet(Id);

            return View(PSM);
        }

        [HttpPost]
        public ActionResult AddEditParameterSet(ParameterSetViewModel objViewModel, List<Master_ParameterSet_Details> ParamDet)
        {
            // Header

            XmlDocument xmlDocSetHdr = new XmlDocument();
            XmlSerializer xmlSerializerHdr = new XmlSerializer(objViewModel.ParameterSet.GetType());

            if (objViewModel.ParameterSet.Id > 0)
            {
                objViewModel.ParameterSet.UpdateBy = User.Identity.Name;
            }
            else
            {
                objViewModel.ParameterSet.EntryBy = User.Identity.Name;
            }

            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializerHdr.Serialize(xmlStream, objViewModel.ParameterSet);
                xmlStream.Position = 0;
                xmlDocSetHdr.Load(xmlStream);
            }

            // Details

            XmlDocument xmlDocSetDet = new XmlDocument();
            if (ParamDet.Count() == 0)
            {
                ParamDet = new List<Master_ParameterSet_Details>();
            }
            XmlSerializer xmlSerializerDet = new XmlSerializer(ParamDet.GetType());

            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializerDet.Serialize(xmlStream, ParamDet);
                xmlStream.Position = 0;
                xmlDocSetDet.Load(xmlStream);
            }
            DataTable dataTableAdd = CL.AddEditParameterSet(objViewModel.ParameterSet.Id, xmlDocSetHdr.InnerXml, xmlDocSetDet.InnerXml);
            //string QueryStringAdd = "exec Usp_Insert_Master_ParameterSet @ID='" + objViewModel.ParameterSet.Id + "',@HeaderXML='" + xmlDocSetHdr.InnerXml + "',@DetailXML='" + xmlDocSetDet.InnerXml + "'";
            //DataTable dataTableAdd = GF.GetDataTableFromSP(QueryStringAdd);
            //string query = null;
            //query = "exec [Usp_AddParaSet] @Id='" + objViewModel.ParameterSet.Id + "', @ParameterSet='" + objViewModel.ParameterSet.ParameterSet + "',@Description='" + objViewModel.ParameterSet.Description + "' ,@IsActive='" + objViewModel.ParameterSet.IsActive + "' ";
            //DataTable dtEntity = GF.getdatetablefromQuery(query);
            //List<ParaSet> ParaSetList = new List<ParaSet>();
            //string query1 = "Usp_GetAllParameterSet";
            //DataTable dtPara = GF.GetDataTableFromSP(query1);
            //ParaSetList = DataRowToObject.CreateListFromTable<ParaSet>(dtPara);
            return RedirectToAction("ParameterSetList");
        }

        public ActionResult ParameterSetData(int Id)
        {
            ParameterSetViewModel PSM = new ParameterSetViewModel();
           
            PSM.ParameterSet = new Master_ParameterSet();
            PSM.ParameterSet_Details = CL.USP_GETParameterDetails_ForSet(Id);
            string query1 = "Usp_GetAllParameterSet";
            DataTable dtPara = GF.GetDataTableFromSP(query1);
            PSM.ListSetPara = DataRowToObject.CreateListFromTable<ParaSet>(dtPara);
            return View(PSM);

        }

        [HttpPost]
        public ActionResult ParameterSetData(int Id, string ParameterSet, string Description, bool IsActive)
        {
            string query = null;
            query = "exec [Usp_AddParaSet] @Id='" + Id + "', @ParameterSet='" + ParameterSet + "',@Description='" + Description + "' ,@IsActive='" + IsActive + "' ";
            DataTable dtEntity = GF.getdatetablefromQuery(query);
            List<ParaSet> ParaSetList = new List<ParaSet>();
            string query1 = "Usp_GetAllParameterSet";
            DataTable dtPara = GF.GetDataTableFromSP(query1);
            ParaSetList = DataRowToObject.CreateListFromTable<ParaSet>(dtPara);
            return PartialView("_ParaSetList", ParaSetList);
        }
        public ActionResult EditPeraSet(int id)
        {
            ParaSet prObj = new ParaSet();
            string query1 = "Usp_GetAllParameterSet";
            DataTable dtPara = GF.GetDataTableFromSP(query1);
            prObj = DataRowToObject.CreateListFromTable<ParaSet>(dtPara).Where(c => c.Id == id).FirstOrDefault();
            return View("ParameterSet", prObj);
        }

        #endregion

      

    }
}
