﻿using CipherLib;
using CYGNUS.Models;
using CYGNUS.ViewModels;
using CYGNUS.ViewModel;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using FleetDataService.ViewModels;
using Microsoft.ApplicationBlocks.Data;
using CYGNUS.Classes;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Drawing;
using GenCode128;
using System.Text.RegularExpressions;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class OperationController : BaseController
    {
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        OperationService OS = new OperationService();
        MasterService MS = new MasterService();
        GeneralFuncations GF = new GeneralFuncations();
        ContractService CS = new ContractService();
        ExceptionsService ES = new ExceptionsService();
        ExceptionsController EC = new ExceptionsController();
        string BasePodPath = "D:\\SRLApplicationCode\\POD\\EmailDocuemnt\\";
        public static string xmlTCHDR = "";
        public static string xmlTCTRN = "";
        public static string xmlWTDS = "";
        public static string xmlNonSelDKT = "";
        public static string xmlTCTRNIM = "";
        string ControllerName = "OperationController";

        #region LoadingSheet

        public ActionResult LoadingSheet(string Type, string TCNO, string NextLOC, string IsBCProcess)
        {
            LoadingSheetFilterViewModel LSVM = new LoadingSheetFilterViewModel();
            List<LoadingSheetDatatable> DocketListForMFGeneration = new List<LoadingSheetDatatable>();
            LSVM.DocketListForMFGeneration = DocketListForMFGeneration;
            if (Type == "ULS" && TCNO != null && TCNO != "")
            {
                LSVM.LsNO = TCNO;
                LSVM.NEXTLOC = NextLOC;
                LSVM.LsDate = System.DateTime.Now;
                LSVM.DocketListForMFGeneration = OS.DocketListFromLSForMFGeneration(TCNO);
            }
            TempData["Type"] = Type;
            LSVM.Type = Type;
            return View(LSVM);
        }

        public ActionResult DocketListForMFGeneration(LoadingSheetFilterViewModel VM)
        {
            List<LoadingSheetDatatable> DocketListForMFGeneration = new List<LoadingSheetDatatable>();
            DocketListForMFGeneration = OS.DocketListForMFGeneration(BaseLocationCode, VM.NextStopLocation, VM.TransportMode, GF.FormateDate(VM.FromDate), GF.FormateDate(VM.ToDate), VM.DestinationList, VM.DocketNoList, GF.FormateDate(VM.LsDate), VM.LoadingBy, string.IsNullOrEmpty(VM.RateType) ? "1" : VM.RateType);
            return PartialView("_LoadingSheetList", DocketListForMFGeneration);
        }

        public ActionResult InternalDocumentListForMFGeneration(LoadingSheetFilterViewModel VM)
        {
            List<CYGNUS_Internal_Movement> InternalDocumentListForMFGeneration = new List<CYGNUS_Internal_Movement>();
            InternalDocumentListForMFGeneration = OS.InternalDocumentListForMFGeneration(BaseLocationCode, VM.NextStopLocation, GF.FormateDate(VM.FromDate), GF.FormateDate(VM.ToDate));
            return PartialView("_InternalDocumentList", InternalDocumentListForMFGeneration);
        }

        public string ListLsForMF()
        {
            return JsonConvert.SerializeObject(OS.ListLsForMF(BaseLocationCode));
        }

        public ActionResult PrepareLoadingSheet(LoadingSheetFilterViewModel VM, List<LoadingSheetDatatable> DocketList, List<CYGNUS_Internal_Movement> InternalDocumentList)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            //SqlTransaction trn = con.BeginTransaction();

            DataTable Dt = new DataTable();
            string GeneratedCode = "";
            if (!VM.IsMonthly)
            {
                if ((VM.LoadingBy == "A" || VM.LoadingBy == "XX5" || VM.LoadingBy == "XX8" || VM.LoadingBy == "M") && VM.LoadingCharge == 0)
                {
                    ViewBag.StrError = "Loading Charge cannot be 0.";
                    return View("Error");
                }
            }

            try
            {
                if (VM.Type == "LS")
                {
                    if (GenerateXML(VM, DocketList, InternalDocumentList, "LS") > 0)
                    {
                        Dt = OS.MFGeneration(BaseLocationCode, GF.FormateDate(VM.LsDate), xmlTCHDR.ReplaceSpecialCharacters(), xmlTCTRN.ReplaceSpecialCharacters(), xmlWTDS.ReplaceSpecialCharacters(), xmlTCTRNIM.ReplaceSpecialCharacters(), BaseFinYear.Substring(0, 4), VM.Type);
                        GeneratedCode = Dt.Rows[0]["LS"].ToString();
                    }
                }
                else if (VM.Type == "MF")
                {
                    if (GenerateXML(VM, DocketList, InternalDocumentList, "MF") > 0)
                    {
                        Dt = OS.MFGeneration(BaseLocationCode, GF.FormateDate(VM.LsDate), xmlTCHDR.ReplaceSpecialCharacters(), xmlTCTRN.ReplaceSpecialCharacters(), xmlWTDS.ReplaceSpecialCharacters(), xmlTCTRNIM.ReplaceSpecialCharacters(), BaseFinYear.Substring(0, 4), VM.Type);
                        GeneratedCode = Dt.Rows[0]["MF"].ToString();
                    }
                }
                else
                {
                    if (GenerateXML(VM, DocketList, InternalDocumentList, "ULS") > 0)
                    {
                        string QueryStringNonSelDKT = "exec WebxNet_NonSelDKT_UpdateLS_Status '" + xmlNonSelDKT.ReplaceSpecialCharacters().Trim() + "' ,'" + VM.LsNO + "'";
                        DataTable Dt1 = GF.GetDataTableFromSP(QueryStringNonSelDKT);
                        int Id = GF.SaveRequestServices(QueryStringNonSelDKT.Replace("'", "''"), "NonSelDKT_UpdateLS_Status" + "ULS", "", "");
                        Dt = OS.MFGeneration(BaseLocationCode, System.DateTime.Now.ToString("dd MMM yyyy"), xmlTCHDR.ReplaceSpecialCharacters(), xmlTCTRN.ReplaceSpecialCharacters(), xmlWTDS.ReplaceSpecialCharacters(), xmlTCTRNIM.ReplaceSpecialCharacters(), BaseFinYear.Substring(0, 4), VM.Type);
                        GeneratedCode = Dt.Rows[0]["MF"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                //trn.Rollback();
                con.Close();
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
            return RedirectToAction("LoadingSheetResult", new { Code = GeneratedCode, Type = VM.Type });
        }

        public ActionResult LoadingSheetResult(string Code, string Type)
        {
            LoadingSheetFilterViewModel LSVM = new LoadingSheetFilterViewModel();
            LSVM.LsNO = Code;
            LSVM.Type = Type;
            return View(LSVM);
        }

        protected int GenerateXML(LoadingSheetFilterViewModel VM, List<LoadingSheetDatatable> DocketList, List<CYGNUS_Internal_Movement> InternalDocumentList, string Type)
        {
            string docDateStr = GF.FormateDate(VM.LsDate);//System.DateTime.Now.ToString("dd MMM yyyy"), 
            string IsBCProcess = "N";
            int DktCount = 0;
            int TotalPkgs = 0;
            int TotalWt = 0;
            int LoadedPkgs = 0;
            int LoadedWt = 0;
            string Nextstop = "";
            if (Type != "ULS")
                Nextstop = GF.FormateString(VM.NextStopLocation);
            else
                Nextstop = GF.FormateString(VM.NEXTLOC);

            if ((VM.LoadingBy == "A" || VM.LoadingBy == "XX5" || VM.LoadingBy == "XX8"))
            {
                VM.LoadingCharge = VM.LoadingCharge;
            }
            else if (Convert.ToDecimal(VM.LoadingCharge) > Convert.ToDecimal(VM.MaxLimit))
            {
                VM.LoadingCharge = VM.MaxLimit;
            }

            if (VM.LoadingBy == "M")
            {
                if (string.IsNullOrEmpty(VM.VendorCode))
                    VM.VendorCode = "9888";
                if (string.IsNullOrEmpty(VM.VendorName))
                    VM.VendorName = "MARKET";
            }

            string ImageName = "";
            #region Stock Update Image Upload
            try
            {
                if (Request.Files != null)
                {
                    var File = Request.Files["UploadImage"];
                    if (File.ContentLength > 0)
                    {
                        ImageName = SaveImage(File, "0", "LoadingSheetImage");
                    }
                }
            }
            catch (Exception)
            {
            }
            #endregion

            xmlTCHDR = "<root><tchdr>";
            xmlTCHDR = xmlTCHDR + "<TCNO></TCNO>" +
                                  "<TCSF>.</TCSF>" +
                                  "<TCDT>" + docDateStr + "</TCDT>" +
                                  "<TCBR>" + BaseLocationCode + "</TCBR>" +
                                  "<LsNO>" + VM.LsNO + "</LsNO>" +
                                  "<TOBH_CODE>" + Nextstop.Trim().ToUpper() + "</TOBH_CODE>" +
                                  "<TOT_DKT></TOT_DKT>" +
                                  "<TOT_PKGS></TOT_PKGS>" +
                                  "<TOT_ACTUWT></TOT_ACTUWT>" +
                                  "<TOT_CFTWT></TOT_CFTWT>" +
                                  "<TOT_LOAD_PKGS></TOT_LOAD_PKGS>" +
                                  "<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>" +
                                  "<TOT_LOAD_CFTWT></TOT_LOAD_CFTWT>" +
                                  "<TCHDRFLAG>Y</TCHDRFLAG>" +
                                  "<TFLAG_YN>N</TFLAG_YN>" +
                                  "<MANUALLSNO>" + GF.FormateString(VM.ManualLsNO) + "</MANUALLSNO>" +
                                  "<PREPAREDBY>" + BaseUserName + "</PREPAREDBY>" +
                                  "<FROMCITY></FROMCITY>" +
                                  "<TOCITY></TOCITY>" +
                                  "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>" +
                                  "<ROUTE_CODE>-</ROUTE_CODE>" +
                                  "<LOADING_STATUS>-</LOADING_STATUS>" +
                                  "<TOT_FRTAMT>00</TOT_FRTAMT>" +
                                  "<MODVATCOVER>0</MODVATCOVER>" +
                                  "<INTCOVER>0</INTCOVER>" +
                                  "<FINAL_TC>N</FINAL_TC>" +
                                  "<IsBCProcess>" + IsBCProcess + "</IsBCProcess>" +
                                  "<LoadingCharge>" + VM.LoadingCharge + "</LoadingCharge>" +
                                  "<LoadedRateType>" + VM.RateType + "</LoadedRateType>" +
                                  "<LoadedBy>" + VM.LoadingBy + "</LoadedBy>" +
                                  "<LoadingRate>" + VM.Rate + "</LoadingRate>" +
                                  "<Loading_VendorName>" + VM.VendorName + "</Loading_VendorName>" +
                                  "<Loading_VendorCode>" + VM.VendorCode + "</Loading_VendorCode>" +
                                  "<IsMathadi>" + VM.IsMathadi + "</IsMathadi>" +
                                  "<MathadiSlipNo>" + VM.MathadiSlipNo + "</MathadiSlipNo>" +
                                  "<MathadiDate>" + VM.MathadiDate + "</MathadiDate>" +
                                  "<MathadiAmt>" + VM.MathadiAmt + "</MathadiAmt>" +
                                  "<TotalInternalDocument>" + (InternalDocumentList == null ? 0 : InternalDocumentList.Where(c => c.IsChecked).Count()) + "</TotalInternalDocument>" +
                                  "<ROUTE_MODE>" + VM.MF_TransportMode + "</ROUTE_MODE>" +
                                 "<ImageName>" + ImageName + "</ImageName>";

            xmlTCHDR = xmlTCHDR + "</tchdr></root>";

            xmlTCTRN = "<root>";
            xmlWTDS = "<root>";
            xmlNonSelDKT = "<root>";
            xmlTCTRNIM = "<root>";
            foreach (var itm in DocketList)
            {
                if (itm.IsChecked)
                {
                    if (Type == "ULS")
                    {
                        xmlWTDS = xmlWTDS + "<wtds>" +
                                            "<DOCKNO>" + itm.DOCKNO + "</DOCKNO>" +
                                            "<DOCKSF>" + itm.DOCKSF + "</DOCKSF>" +
                                            "<TOBH_CODE>" + Nextstop.Trim().ToUpper() + "</TOBH_CODE>" +
                                            "<TCDT>" + docDateStr + "</TCDT>" +
                                            "<NEXTLOC>" + Nextstop.Trim().ToUpper() + "</NEXTLOC>" +
                                            "<LOADPKGSNO>" + itm.PackagesLB + "</LOADPKGSNO>" +
                                            "<LOADACTUWT>" + itm.WeightLB + "</LOADACTUWT>" +
                                            "</wtds>";
                    }
                    else
                    {
                        xmlWTDS = xmlWTDS + "<wtds>" +
                                            "<DOCKNO>" + itm.DOCKNO + "</DOCKNO>" +
                                            "<DOCKSF>" + itm.DOCKSF + "</DOCKSF>" +
                                            "<TOBH_CODE>" + Nextstop.Trim().ToUpper() + "</TOBH_CODE>" +
                                            "<TCDT>" + docDateStr + "</TCDT>" +
                                            "<LOADPKGSNO>" + itm.PackagesLB + "</LOADPKGSNO>" +
                                            "<LOADACTUWT>" + itm.WeightLB + "</LOADACTUWT>" +
                                            "</wtds>";
                    }

                    xmlTCTRN = xmlTCTRN + "<tctrn>" +
                                          "<TCNO></TCNO>" +
                                          "<TCSF>.</TCSF>" +
                                          "<DOCKNO>" + itm.DOCKNO + "</DOCKNO>" +
                                          "<DOCKSF>" + itm.DOCKSF + "</DOCKSF>" +
                                          "<LOADPKGSNO>" + itm.PackagesLB + "</LOADPKGSNO>" +
                                          "<LOADACTUWT>" + itm.WeightLB + "</LOADACTUWT>" +
                                          "<LOADCFTWT>" + itm.WeightLB + "</LOADCFTWT>" +
                                          "<ORGNCD>" + itm.OrgCode + "</ORGNCD>" +
                                          "<DESTCD>" + itm.ReDestCode + "</DESTCD>" +
                                          "<DOCKCLS>X</DOCKCLS>" +
                                          "<DOCKDT>" + itm.DocketDate + "</DOCKDT>" +
                                          "<NOPKGS>" + itm.PKGSNO + "</NOPKGS>" +
                                          "<CFTWT>" + itm.WeightLB + "</CFTWT>" +
                                          "<ACTUWT>" + itm.ACTUWT + "</ACTUWT>" +
                                          "<Rate>" + itm.NewRate + "</Rate>" +
                                          "<RateType>" + itm.ratetype + "</RateType>" +
                                          "<DKTAMT>0</DKTAMT>" +
                                          "</tctrn>";

                    DktCount += 1;
                    TotalPkgs += Convert.ToInt32(itm.PKGSNO);
                    TotalWt += Convert.ToInt32(itm.ACTUWT);
                    LoadedPkgs += Convert.ToInt32(itm.PackagesLB);
                    LoadedWt += Convert.ToInt32(itm.WeightLB);
                }
                else
                {
                    if (Type == "ULS")
                    {
                        xmlNonSelDKT = xmlNonSelDKT + "<NonSelDKT>" +
                            "<DOCKNO>" + itm.DOCKNO + "</DOCKNO>" +
                            "<DOCKSF>" + itm.DOCKSF + "</DOCKSF>" +
                            "<LOADPKGSNO>" + itm.PKGSNO + "</LOADPKGSNO>" +
                            "<LOADACTUWT>" + itm.WeightLB + "</LOADACTUWT>" +
                            "</NonSelDKT>";
                    }

                }
            }

            xmlTCTRNIM = xmlTCTRNIM + "<TCTRNIM>";
            if (InternalDocumentList != null && InternalDocumentList.Count > 0)
            {
                foreach (var itm in InternalDocumentList.Where(c => c.IsChecked))
                {
                    xmlTCTRNIM = xmlTCTRNIM + "<IMNo>" + itm.IMNo + "</IMNo>";
                    xmlTCTRNIM = xmlTCTRNIM + "<Weight>" + itm.Weight + "</Weight>";
                    xmlTCTRNIM = xmlTCTRNIM + "<Packages>" + itm.Packages + "</Packages>";
                    xmlTCTRNIM = xmlTCTRNIM + "<PREPAREDBY>" + BaseUserName + "</PREPAREDBY>";
                }
            }
            xmlTCTRNIM = xmlTCTRNIM + "</TCTRNIM>";

            if (DktCount > 0)
            {
                xmlTCHDR = xmlTCHDR.Replace("<TOT_DKT></TOT_DKT>", "<TOT_DKT>" + DktCount.ToString().Trim() + "</TOT_DKT>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_PKGS></TOT_PKGS>", "<TOT_PKGS>" + TotalPkgs.ToString().Trim() + "</TOT_PKGS>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_ACTUWT></TOT_ACTUWT>", "<TOT_ACTUWT>" + TotalWt.ToString().Trim() + "</TOT_ACTUWT>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_CFTWT></TOT_CFTWT>", "<TOT_CFTWT>" + TotalWt.ToString().Trim() + "</TOT_CFTWT>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_LOAD_PKGS></TOT_LOAD_PKGS>", "<TOT_LOAD_PKGS>" + LoadedPkgs.ToString().Trim() + "</TOT_LOAD_PKGS>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>", "<TOT_LOAD_ACTWT>" + LoadedWt.ToString().Trim() + "</TOT_LOAD_ACTWT>");
                xmlTCHDR = xmlTCHDR.Replace("<TOT_LOAD_CFTWT></TOT_LOAD_CFTWT>", "<TOT_LOAD_CFTWT>" + LoadedWt.ToString().Trim() + "</TOT_LOAD_CFTWT>");
                xmlTCTRN = xmlTCTRN + "</root>";
                xmlWTDS = xmlWTDS + "</root>";
                xmlNonSelDKT = xmlNonSelDKT + "</root>";
                xmlTCTRNIM = xmlTCTRNIM + "</root>";
            }

            return DktCount;
        }

        public JsonResult LocationConnectionMappingListJson(string Location)
        {
            List<Webx_master_LocationConnection> ListLocationConnection = OS.GetLocationConnectionObject(Location);
            var ListLocationConnectionData = (from e in ListLocationConnection
                                              select new
                                              {
                                                  Locations = e.Locations
                                              }).ToArray();
            return Json(ListLocationConnectionData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region UpdateLoading Sheet

        public ActionResult UpdateLoadingSheet(string Type)
        {
            LoadingSheetFilterViewModel LSVM = new LoadingSheetFilterViewModel();
            List<LoadingSheetDatatable> DocketListForMFGeneration = new List<LoadingSheetDatatable>();
            LSVM.DocketListForMFGeneration = DocketListForMFGeneration;
            if (BaseLocationCode != null && BaseLocationCode != "")
            {
                LSVM.DocketListForMFGeneration = OS.DocketListFromLSForMFGeneration(BaseLocationCode);
            }
            return View(LSVM);
        }

        #endregion

        #region DRSUpdate

        public ActionResult DRSUpdate()
        {
            vw_DRS_Summary DRSummry = new vw_DRS_Summary();
            return View(DRSummry);
        }

        public string DRSUpdateListJson(vw_DRS_Summary DRSummary)
        {
            return JsonConvert.SerializeObject(OS.DRSUpdateList(BaseLocationCode, GF.FormateDate(DRSummary.FromDate), GF.FormateDate(DRSummary.ToDate), DRSummary.DRSNoList));
        }

        #endregion

        #region UpdateDRS

        public ActionResult UpdateDRS(string DRSId, string Loadby, string chargeType)
        {
            UpdateDRSViewModel UDVM = new UpdateDRSViewModel();
            vw_DRS_Summary ObjDRS = new vw_DRS_Summary();
            vw_dockets_for_drs_updation_New ObjnewUpdation = new vw_dockets_for_drs_updation_New();
            List<vw_DRS_Summary> DRSSummaryList = new List<vw_DRS_Summary>();
            List<vw_DRS_Summary> DRSDeliveryList = new List<vw_DRS_Summary>();
            List<vw_dockets_for_drs_updation_New> DRSUpdateList = new List<vw_dockets_for_drs_updation_New>();
            DRSUpdateList = OS.GetUpdateDRSList(DRSId, Loadby, chargeType, BaseLocationCode);
            ObjnewUpdation = DRSUpdateList.FirstOrDefault();
            ObjDRS.Rate = ObjnewUpdation.Rate;
            ObjDRS.MaxLimit = ObjnewUpdation.MaxLimit;
            DRSDeliveryList = OS.GetDRSDevelieryList(DRSId).ToList();
            UDVM.UpdateDRSLits = DRSUpdateList;
            UDVM.DRSDeliveryList = DRSDeliveryList;
            ObjDRS.LoadingBy = Loadby;
            ObjDRS.RateType = chargeType;
            UDVM.DRSSummary = OS.GetUpdateDRSSummaryList(DRSId).FirstOrDefault();
            UDVM.DRSSummary.LoadingBy = ObjDRS.LoadingBy;
            UDVM.DRSSummary.RateType = ObjDRS.RateType;
            UDVM.DRSSummary.Rate = ObjDRS.Rate;
            UDVM.DRSSummary.MaxLimit = ObjDRS.MaxLimit;
            ViewBag.Currentdate = System.DateTime.Now;
            return View(UDVM);
        }

        [HttpPost]
        public ActionResult AddUpdateDRS(UpdateDRSViewModel ViewModel, vw_DRS_Summary DRSSummary, List<vw_dockets_for_drs_updation_New> DRSDocketsUpdateList)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                if (!DRSSummary.IsMonthly)
                {
                    /*28 Jan 2017*/
                    int mathadiCNT = DRSDocketsUpdateList.Where(c => c.ratetype == "4").Count();
                    int otherCNT = DRSDocketsUpdateList.Where(c => c.IsEnabled == true && (c.ratetype == "1" || c.ratetype == "3")).Count();
                    if (mathadiCNT != DRSDocketsUpdateList.Count())
                    {
                        if (otherCNT > 0)
                        {
                            /*28 Jan 2017*/
                            if ((DRSSummary.LoadingBy == "A" || DRSSummary.LoadingBy == "XX5" || DRSSummary.LoadingBy == "XX8" || DRSSummary.LoadingBy == "M") && ViewModel.LoadingCharge == 0)
                            {
                                ViewBag.StrError = "UnLoading Charge cannot be 0.";
                                return View("Error");
                            }
                        }
                    }
                }

                XmlDocument XDoc = new XmlDocument();
                string XML = "<root>";

                foreach (var item in DRSDocketsUpdateList)
                {
                    string DelyStatus = "F";

                    if (Convert.ToDouble(item.PKGSDELIVERED) < Convert.ToDouble(item.Pkgs_Pending) && Convert.ToDouble(item.PKGSDELIVERED) != 0)
                    {
                        DelyStatus = "P"; // Part Dely. 
                    }
                    else
                    {
                        if (Convert.ToDouble(item.PKGSDELIVERED) == 0)
                        {
                            DelyStatus = "U"; // Full Dely.
                        }
                        else
                        {
                            DelyStatus = "F";
                        }
                    }
                    if (item.COD_DOD == "N")
                    {
                        item.CODDODAmount = 0;
                        item.CODDODCOLLECTED = 0;
                        item.CODDODNO = 0;
                    }

                    if ((DRSSummary.LoadingBy == "A" || DRSSummary.LoadingBy == "XX5" || DRSSummary.LoadingBy == "XX8"))
                    {
                        ViewModel.LoadingCharge = ViewModel.LoadingCharge;
                    }
                    else if (Convert.ToDecimal(ViewModel.LoadingCharge) > Convert.ToDecimal(ViewModel.DRSSummary.MaxLimit))
                    {
                        ViewModel.LoadingCharge = ViewModel.DRSSummary.MaxLimit;
                    }
                    XML = XML + "<DRS>";
                    XML = XML + "<DRSCode>" + DRSSummary.pdcno + "</DRSCode>";
                    XML = XML + "<CLOSEKM>" + DRSSummary.CloseKM + "</CLOSEKM>";
                    //XML = XML + "<DRSCode>" + item.dockno + "</DRSCode>";
                    XML = XML + "<DOCKET>" + item.dockno + "</DOCKET>";
                    XML = XML + "<DOCKETSF>" + item.docksf + "</DOCKETSF>";
                    XML = XML + "<PKGSWASPENDING>" + item.Pkgs_Pending + "</PKGSWASPENDING>";
                    XML = XML + "<PKGSDELIVERED>" + item.PKGSDELIVERED + "</PKGSDELIVERED>";
                    XML = XML + "<DELYDATE>" + item.DELYDATE.ToString("dd MMM yyyy") + "</DELYDATE>";
                    XML = XML + "<DELYSTATUS>" + DelyStatus + "</DELYSTATUS>";
                    //XML = XML + "<DELYTIME>" + item.DELYTIME.ToString("HH:MM") + "</DELYTIME>";
                    XML = XML + "<DELYTIME>" + item.DELYDATE.ToString("HH:MM") + "</DELYTIME>";
                    XML = XML + "<DELYREASON>" + item.remark + "</DELYREASON>";
                    XML = XML + "<DELYPERSON>" + item.DELYPERSON + "</DELYPERSON>";
                    XML = XML + "<cboLateReason>" + item.cboLateReason + "</cboLateReason>";
                    XML = XML + "<UnLoadingCharge>" + ViewModel.LoadingCharge + "</UnLoadingCharge>";
                    XML = XML + "<UnLoadingBy>" + DRSSummary.LoadingBy + "</UnLoadingBy>";
                    XML = XML + "<RateType>" + DRSSummary.RateType + "</RateType>";
                    XML = XML + "<Rate>" + DRSSummary.Rate + "</Rate>";
                    XML = XML + "<URateType>" + item.ratetype + "</URateType>";
                    XML = XML + "<URate>" + item.NewRate + "</URate>";
                    XML = XML + "<VendorCode>" + DRSSummary.VendorCode + "</VendorCode>";
                    XML = XML + "<VendorName>" + DRSSummary.VendorName + "</VendorName>";
                    XML = XML + "<REMARK>" + item.remark + "</REMARK>";
                    string isMahadi = "0";
                    if (DRSSummary.IsMathadi)
                    {
                        isMahadi = "1";
                        XML = XML + "<IsMathadi>" + isMahadi + "</IsMathadi>";
                        XML = XML + "<MathadiSlipNo>" + DRSSummary.MathadiSlipNo + "</MathadiSlipNo>";
                        XML = XML + "<MathadiDate>" + DRSSummary.MathadiDate + "</MathadiDate>";
                        XML = XML + "<MathadiAmt>" + DRSSummary.MathadiAmt + "</MathadiAmt>";
                    }

                    XML = XML + "<CODDODAMOUNT>" + item.CODDODAmount + "</CODDODAMOUNT>";
                    XML = XML + "<CODDODCOLLECTED>" + item.CODDODCOLLECTED + "</CODDODCOLLECTED>";
                    XML = XML + "<CODDODNO>" + item.CODDODNO + "</CODDODNO>";

                    XML = XML + "</DRS>";

                }
                XML = XML + "</root>";


                bool Status = OS.InsertUpdateDRS(BaseUserName, XML.ReplaceSpecialCharacters());
                if (Status == false)
                {
                    ViewBag.StrError = "Server Error.";
                    return View("Error");
                }
                trn.Commit();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                trn.Rollback();
                con.Close();
                return View("Error");
            }
            return RedirectToAction("UpdateDRSResult", new { DRSNO = DRSSummary.pdcno });
        }

        private XmlElement CreateElement(XmlDocument XDoc, string name, string value)
        {
            XmlElement xa = XDoc.CreateElement(name);
            xa.InnerText = value;
            return xa;
        }

        public ActionResult UpdateDRSResult(string DRSNO)
        {
            vw_DRS_Summary DRSSummaryList = new vw_DRS_Summary();
            List<webx_BILLDET> DocketBillList = new List<webx_BILLDET>();
            DRSSummaryList = OS.GetUpdateDRSSummaryList(DRSNO).FirstOrDefault();
            DocketBillList = OS.GetUpdateDRSDocketBillList(DRSNO);
            DRSSummaryList.BillDet = DocketBillList;

            return View(DRSSummaryList);
        }

        #endregion

        #region THC ARRIVAL AND STOCK UPDATE

        #region THC ARRIVAL

        public ActionResult ArrivalUpdate(int id)
        {
            THCArrivalsFilterViewModel TAFVM = new THCArrivalsFilterViewModel();
            TAFVM.Type = id.ToString();
            return View(TAFVM);
        }

        public string THCArrivalsMain(THCArrivalsFilterViewModel TAFVM)
        {
            ViewBag.Type = TAFVM.Type.ToString();
            return JsonConvert.SerializeObject(OS.THCArrivalStockUpdateList(BaseLocationCode, GF.FormateDate(TAFVM.FromDate), GF.FormateDate(TAFVM.ToDate), TAFVM.THCNo, TAFVM.THCDepLoc, TAFVM.THCVehicleNo, TAFVM.THCCnoteNo, TAFVM.Type, TAFVM.TransportMode));
        }

        public ActionResult ArrivalDetails(string id, string Loadby, string chargeType)
        {
            vw_Incoming_THC WTHM = new vw_Incoming_THC();
            ViewBag.Type = id;
            try
            {
                if (id != "0")
                {

                    WTHM = OS.GetTHCDetailsObject(id).FirstOrDefault();
                    WTHM.RateType = chargeType;
                    WTHM.LoadingBy = Loadby;
                    WTHM.Unloder = BaseUserName;
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WTHM);
        }

        public JsonResult CheckSealNo(string thc)
        {
            try
            {
                string Count = OS.GetSealNoDetails(thc);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public ActionResult THCArrivalDetails(vw_Incoming_THC ITM)
        {
            string Status = "";
            try
            {
                var sql = "SELECT CNT=COUNT(*) FROM webx_TCHDR WITH(NOLOCK) WHERE THCNO='" + ITM.THCNO + "' and ToBH_CODE='" + BaseLocationCode + "'";
                DataTable DT = GF.getdatetablefromQuery(sql);
                int Count = Convert.ToInt32(DT.Rows[0][0]);
                if (Count > 0)
                {
                    if (!ITM.IsMonthly)
                    {
                        if ((ITM.LoadingBy == "A" || ITM.LoadingBy == "XX5" || ITM.LoadingBy == "XX8" || ITM.LoadingBy == "M") && ITM.LoadingCharge == 0)
                        {
                            ViewBag.StrError = "UnLoading Charge cannot be 0.";
                            return View("Error");
                        }
                    }
                }

                if (ITM.AD > System.DateTime.Now)
                {
                    ViewBag.StrError = ViewBag.THCCalledAs + " Arrival Date cannot be Large than Today date..!!";
                    return View("Error");
                }
                else
                {
                    if ((ITM.LoadingBy == "A" || ITM.LoadingBy == "XX5" || ITM.LoadingBy == "XX8"))
                    {
                        ITM.LoadingCharge = ITM.LoadingCharge;
                    }
                    else if (Convert.ToDecimal(ITM.LoadingCharge) > Convert.ToDecimal(ITM.MaxLimit))
                    {
                        ITM.LoadingCharge = ITM.MaxLimit;
                    }
                    DataTable Dt = OS.AddEditTHCArrivalDetails(ITM.THCNO, BaseLocationCode, ITM.AD, ITM.AT, ITM.ISN, ITM.Status, ITM.IR, BaseUserName, ITM.LAR, ITM.OPENKM.ToString(), ITM.CLOSEKM.ToString(), ITM.Seal_Reason, ITM.LoadingCharge, ITM.Rate, ITM.LoadingBy, ITM.RateType, ITM.VendorCode, ITM.VendorName, ITM.IsMathadi, ITM.MathadiSlipNo, ITM.MathadiAmt, ITM.MathadiDate);
                    if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                    {
                        Status = Dt.Rows[0]["TranXaction"].ToString();
                        ArrivalStockUpdate ASU = new ArrivalStockUpdate()
                        {
                            THCNO = ITM.THCNO,
                            UnLoadingNo = "",
                            TranXaction = Status,
                            view = "Arrival"
                        };
                        TempData["ArrivalStockUpdate"] = ASU;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
            return RedirectToAction("ArrivalUpdateDone");
        }
        public class ArrivalStockUpdate
        {
            public string THCNO { get; set; }
            public string UnLoadingNo { get; set; }
            public string TranXaction { get; set; }
            public string view { get; set; }
        }

        #endregion

        #region THC STOCK UPDATE

        public ActionResult StockUpdateDetails(string id)
        {
            vw_Stock_For_UpdateViewModel VSFUVM = new vw_Stock_For_UpdateViewModel();
            ViewBag.Type = id;
            try
            {
                if (id != "0")
                {
                    VSFUVM.listVSFUM = OS.GetStockUpdateObject(id, BaseLocationCode);
                    VSFUVM.VSFUM = VSFUVM.listVSFUM.FirstOrDefault();
                    if (VSFUVM.listVSFUM.Count == 0)
                    {
                        return RedirectToAction("ArrivalUpdate", new { id = 2 });
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }

            return View(VSFUVM);
        }

        [HttpPost]
        public ActionResult StockUpdateDetails(vw_Stock_For_UpdateViewModel VSFUVM, List<vw_Stock_For_Update> StockUpdateList, IEnumerable<HttpPostedFileBase> files)
        {
            string Status = "";

            vw_Stock_For_UpdateViewModel VSFUVMList = new vw_Stock_For_UpdateViewModel();
            VSFUVMList.listVSFUM = StockUpdateList.ToList();
            var DelyStatus = "F";
            try
            {
                string MstDetails = "<root>";
                foreach (var item in VSFUVMList.listVSFUM)
                {

                    string ImageName = "", vehicleimage = "";
                    #region Stock Update Image Upload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["UploadImage_" + item.DockNo];
                            var File1 = Request.Files["VehicleImage_" + item.DockNo];
                            if (File.ContentLength > 0)
                            {
                                ImageName = SaveImage(File, item.DockNo, "StockupdateImage");
                            }
                            if (File1.ContentLength > 0)
                            {
                                vehicleimage = SaveImage(File, item.DockNo, "StockupdateVehicleImage");
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    #endregion

                    MstDetails = MstDetails + "<SU><SUDate>" + VSFUVM.VSFUM.UpdateDate + "</SUDate>";
                    MstDetails = MstDetails + "<TCNO>" + item.TCNO + "</TCNO>";
                    MstDetails = MstDetails + "<THCNO>" + VSFUVM.VSFUM.THCNO + "</THCNO>";
                    MstDetails = MstDetails + "<DOCKET>" + item.DockNo + "</DOCKET>";
                    MstDetails = MstDetails + "<DOCKETSF>" + item.DockSF + "</DOCKETSF>";
                    MstDetails = MstDetails + "<PKGSTOBEARRIVED>" + item.BKG_PKGSNO + "</PKGSTOBEARRIVED>";
                    MstDetails = MstDetails + "<PKGSARRIVED>" + item.PKGSNO + "</PKGSARRIVED>";
                    MstDetails = MstDetails + "<WTTOBEARRIVED>" + item.BKG_ACTUWT + "</WTTOBEARRIVED>";
                    MstDetails = MstDetails + "<WTARRIVED>" + item.ACTUWT + "</WTARRIVED>";
                    MstDetails = MstDetails + "<ARRVCOND>" + item.AC + "</ARRVCOND>";
                    MstDetails = MstDetails + "<GODOWN>" + item.WI + "</GODOWN>";
                    MstDetails = MstDetails + "<IsExcess>" + (VSFUVM.VSFUM.ExcessQty == 0 ? false : true) + "</IsExcess > ";
                    MstDetails = MstDetails + "<ExcessQty>" + VSFUVM.VSFUM.ExcessQty + "</ExcessQty>";
                    MstDetails = MstDetails + "<ExcessRemarks>" + VSFUVM.VSFUM.ExcessRemarks + "</ExcessRemarks>";
                    if (item.IsCODDODChar == "1")
                    {
                        if (item.BKG_PKGSNO < item.PKGSNO && item.BKG_PKGSNO != 0)
                        {
                            DelyStatus = "P";
                        }
                        else if (item.BKG_PKGSNO == item.PKGSNO)
                        {
                            DelyStatus = "F";
                        }
                        else if (item.BKG_PKGSNO == 0)
                        {
                            DelyStatus = "U";
                        }
                        MstDetails = MstDetails + "<DELYDATE>" + item.CDELYDT + "</DELYDATE>";
                        MstDetails = MstDetails + "<DELYSTATUS>" + DelyStatus + "</DELYSTATUS>";
                        MstDetails = MstDetails + "<DELYTIME>" + item.CDELYDT + "</DELYTIME>";
                        MstDetails = MstDetails + "<DELYREASON>" + item.DELYREASON + "</DELYREASON>";
                        MstDetails = MstDetails + "<DELYPERSON>" + item.DELYPERSON + "</DELYPERSON>";
                    }
                    else
                    {
                        MstDetails = MstDetails + "<DELYDATE>" + "01/01/1900" + "</DELYDATE>";
                        MstDetails = MstDetails + "<DELYSTATUS>" + "N" + "</DELYSTATUS>";
                        MstDetails = MstDetails + "<DELYTIME>" + "00:00" + "</DELYTIME>";
                        MstDetails = MstDetails + "<DELYREASON>" + "-" + "</DELYREASON>";
                        MstDetails = MstDetails + "<DELYPERSON>" + "-" + "</DELYPERSON>";
                    }

                    MstDetails = MstDetails + "<DELY_PROCESS>" + item.DP + "</DELY_PROCESS>";
                    MstDetails = MstDetails + "<CODDODAMOUNT>" + item.CODDODAmount + "</CODDODAMOUNT>";
                    MstDetails = MstDetails + "<CODDODCOLLECTED>" + item.CODDODCOLLECTED + "</CODDODCOLLECTED>";
                    MstDetails = MstDetails + "<CODDODNO>" + item.CODDOD + "</CODDODNO>";
                    MstDetails = MstDetails + "<ISCounterDelivery>" + item.ISCounterDelivery + "</ISCounterDelivery>";

                    if (item.ShortageQty == 0)
                    {
                        MstDetails = MstDetails + "<IsShortage>N</IsShortage>";
                    }
                    else
                    {
                        MstDetails = MstDetails + "<IsShortage>Y</IsShortage>";
                    }

                    MstDetails = MstDetails + "<ShortageQty>" + item.ShortageQty + "</ShortageQty>";
                    MstDetails = MstDetails + "<ShortageWeight>" + item.ShortageWeight + "</ShortageWeight>";
                    MstDetails = MstDetails + "<ShortageReason>" + item.ShortageReason + "</ShortageReason>";
                    MstDetails = MstDetails + "<ShortageRemarks>" + item.ShortageRemarks + "</ShortageRemarks>";

                    if (item.PilferageQty == 0)
                    {
                        MstDetails = MstDetails + "<IsPilferage>N</IsPilferage>";
                    }
                    else
                    {
                        MstDetails = MstDetails + "<IsPilferage>Y</IsPilferage>";
                    }
                    MstDetails = MstDetails + "<PilferageQty>" + item.PilferageQty + "</PilferageQty>";
                    MstDetails = MstDetails + "<PilferageWeight>" + item.PilferageWeight + "</PilferageWeight>";
                    MstDetails = MstDetails + "<PilferageReason>" + item.PilferageReason + "</PilferageReason>";
                    MstDetails = MstDetails + "<PilferageRemarks>" + item.PilferageRemarks + "</PilferageRemarks>";

                    if (item.DamageQry == 0)
                    {
                        MstDetails = MstDetails + "<IsDamage>N</IsDamage>";
                    }
                    else
                    {
                        MstDetails = MstDetails + "<IsDamage>Y</IsDamage>";
                    }
                    MstDetails = MstDetails + "<DamageQry>" + item.DamageQry + "</DamageQry>";
                    MstDetails = MstDetails + "<DamageWeight>" + item.DamageWeight + "</DamageWeight>";
                    MstDetails = MstDetails + "<DamageReason>" + item.DamageReason + "</DamageReason>";
                    MstDetails = MstDetails + "<Imageupload>" + ImageName + "</Imageupload>";
                    MstDetails = MstDetails + "<vehicleimage>" + vehicleimage + "</vehicleimage>";
                    MstDetails = MstDetails + "<DamageRemarks>" + item.DamageRemarks + "</DamageRemarks></SU>";

                    //if (item.ExcessQty == 0)
                    //{
                    //    MstDetails = MstDetails + "<IsExcess>N</IsExcess>";
                    //}
                    //else
                    //{
                    //    MstDetails = MstDetails + "<IsExcess>Y</IsExcess>";
                    //}
                    //MstDetails = MstDetails + "<ExcessQty>" + item.ExcessQty + "</ExcessQty>";
                    //MstDetails = MstDetails + "<ExcessRemarks>" + item.ExcessRemarks + "</ExcessRemarks></SU>";
                }

                MstDetails = MstDetails + "</root>";
                DataTable Dt = OS.AddEditStockUpdateDetails(BaseUserName, BaseLocationCode, MstDetails);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = Dt.Rows[0]["TranXaction"].ToString();
                    ArrivalStockUpdate ASU = new ArrivalStockUpdate()
                    {
                        THCNO = VSFUVM.VSFUM.THCNO,
                        UnLoadingNo = Dt.Rows[0]["UnLoadingNo"].ToString(),
                        TranXaction = Status,
                        view = "Stock Update"
                    };
                    TempData["ArrivalStockUpdate"] = ASU;
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("ArrivalUpdateDone");
        }

        private string SaveImage(HttpPostedFileBase File, string Id, string FolderName)
        {
            string extension = "", name = "", path = "";
            string IdPath = Server.MapPath("~/Images/" + FolderName + "/" + GF.GenerateString(8));
            if (!Directory.Exists(IdPath))
                Directory.CreateDirectory(IdPath);

            string FileName = File.FileName;
            extension = System.IO.Path.GetExtension(File.FileName);
            name = GF.GenerateString(10) + extension;
            path = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);
            string path1 = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);

            if (System.IO.File.Exists(path1))
                System.IO.File.Delete(path1);

            File.SaveAs(path1);

            return name;
        }

        #endregion

        public ActionResult ArrivalUpdateDone()
        {
            ArrivalStockUpdate ASU = (ArrivalStockUpdate)TempData["ArrivalStockUpdate"];
            TempData.Keep("ArrivalStockUpdate");
            ViewBag.ThcNo = ASU.THCNO;
            ViewBag.UnLoadingNo = ASU.UnLoadingNo;
            ViewBag.TranXaction = ASU.TranXaction;
            ViewBag.view = ASU.view;
            //ViewBag.ThcNo = ThcNo;
            //ViewBag.UnLoadingNo = UnLoadingNo;
            //ViewBag.TranXaction = TranXaction;
            //ViewBag.view = view;
            return View();
        }


        #endregion

        #region Docket

        public ActionResult Docket(int Type)
        {
            string BookingAllowed = MS.GetLocationDetails().Where(c => c.LocCode == BaseLocationCode).FirstOrDefault().Op_Bkg;
            if (!string.IsNullOrEmpty(BookingAllowed) && BookingAllowed.ToUpper() == "Y")
            {
                DocketViewModel DVM = new DocketViewModel();
                WebX_Master_Docket_Charges WMDC = new WebX_Master_Docket_Charges();
                WebX_Master_Docket WMD = new WebX_Master_Docket();
                List<WebX_Master_Docket_Invoice> ListInVoice = new List<WebX_Master_Docket_Invoice>();
                List<QuickDocketBalancecharges> ListCharges = new List<QuickDocketBalancecharges>();

                if (ListInVoice.Count() == 0)
                {
                    WebX_Master_Docket_Invoice CMDI = new WebX_Master_Docket_Invoice();
                    CMDI.SrNo = 1;
                    CMDI.INVDT = System.DateTime.Now;
                    CMDI.DECLVAL = 0;
                    CMDI.PKGSNO = 0;
                    CMDI.ACTUWT = 0;
                    CMDI.VOL_L = 0;
                    CMDI.VOL_B = 0;
                    CMDI.VOL_H = 0;
                    ListInVoice.Add(CMDI);
                    CMDI = new WebX_Master_Docket_Invoice();
                }
                WMD.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
                WMD.FlagStaxBifer = MS.GetRuleObject("STAX_BIFER").defaultvalue.IndexOf(BaseCompanyCode) >= 0 ? "Y" : "N";
                WMD.FLAG_PRORATA = MS.GetRuleObject("FLAG_PRORATA").defaultvalue;
                DVM.WMD = WMD;
                DVM.WMDC = WMDC;
                DVM.ListInVoice = ListInVoice;
                ListCharges = OS.GetDocketChargesDetails("").Where(c => c.ChargeCode != "SCHG11" && c.ChargeCode != "SCHG12" && c.ChargeCode != "SCHG13").ToList();
                DVM.ListCharges = ListCharges;

                //START GST Changes Chirag D 
                //  string ChargesStr = "EXEC USP_GetDynamicChargesNew 'DKT'";

                string ChargesStr = "";
                if (Type == 1)
                {
                    ChargesStr = "EXEC USP_GetDynamicChargesNew 'DKT'";
                }
                else
                {
                    ChargesStr = "EXEC USP_GetDynamicChargesNew 'URD'";
                }
                DataTable Dt = GF.GetDataTableFromSP(ChargesStr);
                List<CygnusChargesHeader> GetCCHList = DataRowToObject.CreateListFromTable<CygnusChargesHeader>(Dt);
                DVM.ListCCH = GetCCHList;
                //END GST Changes Chirag D
                DVM.WCM = new webx_CUSTHDR();
                DVM.WMD.Moduletype = Type;
                return View(DVM);
            }
            else
            {
                ViewBag.StrError = "You do not have permission to add Docket from current branch...!!!";
                return View("Error");
                //return RedirectToAction("URLRedirect", "Home");
            }
        }

        public ActionResult DocketSubmit(DocketViewModel DVM, List<WebX_Master_Docket_Invoice> DocketInvoiceList, List<QuickDocketBalancecharges> DocketChargesList,
            List<webx_Master_DOCKET_DOCUMENT> ListDocument, List<BarCodeSerial> ListBarCode, string DOCTYP, List<CygnusChargesHeader> DynamicList)
        {
            string fromdate = Convert.ToDateTime(DVM.WMD.DOCKDT).ToString("dd MMM yyyy");
            string todate = Convert.ToDateTime(DVM.WMD.CDELDT).ToString("dd MMM yyyy");
            DateTime DocketDate = Convert.ToDateTime(fromdate);
            DateTime EDDDate = Convert.ToDateTime(todate);

            /* START GST Changes By Chirag D  */
            #region GST Charges

            //if (DVM.WMD.DOCKDT >= Convert.ToDateTime("01 Jul 2017"))
            //{
            //    DVM.WMD.fincmplbr = DVM.WMD.ORGNCD;
            //}

            XmlDocument xmlDocCha = new XmlDocument();
            XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());

            using (MemoryStream xmlStreamCha = new MemoryStream())
            {
                xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                xmlStreamCha.Position = 0;
                xmlDocCha.Load(xmlStreamCha);
            }

            WebX_Master_Docket_Charges TempWMDC = new WebX_Master_Docket_Charges();
            string DynamicChargdeXML = "EXEC USP_GetPivotDynamicCharges '" + xmlDocCha.InnerXml + "'";
            DataTable DTCharges = GF.GetDataTableFromSP(DynamicChargdeXML);
            TempWMDC = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Charges>(DTCharges).First();

            #endregion

            /* END GST Changes By Chirag D  */

            //SDD Date Changes
            #region SDD Date Changes Start

            if (DVM.WMD.SDD_Date.ToString("dd MMM yyyy") != "01 Jan 0001" && DVM.WMD.SDD_Date.ToString("dd MMM yyyy") != "01 Jan 1990")
            {
                int diff = (DVM.WMD.DOCKDT - DVM.WMD.SDD_Date).Days;
                Webx_Date_Rules WDR = MS.GetDateRule("19").FirstOrDefault();
                if (WDR.BackDate_Days < diff)
                {
                    string ErrorMsg = "SDD Date Should be Large than Docket Date";
                    ViewBag.StrError = ErrorMsg;
                    return View("Error");
                }
                //if (DVM.WMD.SDD_Date > DVM.WMD.DOCKDT)
                //{
                //    string ErrorMsg = "SDD Date Should be Large than Docket Date";
                //    ViewBag.StrError = ErrorMsg;
                //    return View("Error");
                //}
            }

            #endregion

            //EDD Date Validation Check
            #region EDD Date Validation

            string IsScheduleDelivery = MS.GetCustomerMasterObjectDocketSubmit(DVM.WMD.PARTY_CODE).FirstOrDefault().ScheduleApply;
            if ((string.IsNullOrEmpty(IsScheduleDelivery) || IsScheduleDelivery != "Y") && DVM.WMD.PAYBAS != "P02")
            {
                if (DVM.WMD.CDELDT != System.DateTime.MinValue && DVM.WMD.CDELDT != System.DateTime.MaxValue)
                {
                    DateTime SYSEDDDate = Convert.ToDateTime(GF.FormateDate(DVM.WMD.DOCKDT.AddDays(Convert.ToInt32(DVM.WMD.TRDays))));
                    if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "PU")
                    {
                        DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, DVM.WMD.REASSIGN_DESTCD, true);
                        if (edd_hday > SYSEDDDate)
                        {
                            SYSEDDDate = edd_hday;
                        }
                    }
                    else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "AD")
                    {
                        DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, DVM.WMD.REASSIGN_DESTCD, true);
                        if (edd_hday > SYSEDDDate)
                        {
                            // hdnhdayedd.Value = edd_hday.ToString("dd/MM/yyyy");
                            SYSEDDDate = edd_hday;
                        }
                    }
                    else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "N")
                    {
                        if (SYSEDDDate.DayOfWeek.ToString().ToUpper().CompareTo("SUNDAY") == 0)
                            SYSEDDDate.AddDays(1);
                    }

                    if (Convert.ToDateTime(GF.FormateDate(DVM.WMD.CDELDT)) != SYSEDDDate)
                    {
                        //string ErrorMsg = "Actual EDD Should be " + GF.FormateDate(DVM.WMD.DOCKDT.AddDays(Convert.ToInt32(DVM.WMD.TRDays))) + " TRDays = " + DVM.WMD.TRDays + " but current EDD is " + GF.FormateDate(DVM.WMD.CDELDT);
                        //ViewBag.StrError = ErrorMsg;
                        //return View("Error");
                    }
                }
            }

            #endregion

            #region DKT Entry Date Validation

            if (Convert.ToDateTime(DocketDate.Date.ToString("dd MMM yyyy")) > Convert.ToDateTime(System.DateTime.Now.ToString("dd MMM yyyy")))
            {
                ViewBag.StrError = ViewBag.DKTCalledAs + " Date cannot be Large than Today Date";
                return View("Error");
                //return RedirectToAction("ErrorTransaction", "Home", new { Message = ErrorMsg });
            }

            if (DocketDate.Date > EDDDate.Date)
            {
                ViewBag.StrError = "EDD Date Should be Large than " + ViewBag.DKTCalledAs + " Date";
                return View("Error");
            }

            int days = Convert.ToInt32((System.DateTime.Now - DocketDate).TotalDays);
            Webx_Date_Rules WDR1 = MS.GetDateRule("01").FirstOrDefault();
            if (days > WDR1.BackDate_Days)
            {
                ViewBag.StrError = "Please Enter Valid " + ViewBag.DKTCalledAs + " Date. " + ViewBag.DKTCalledAs + " date " + DocketDate.ToString("dd MMM yyy") + " not valid.";
                return View("Error");
            }

            //if (DocketDate < System.DateTime.Now.AddDays(5))
            //{
            //    ViewBag.StrError = "Please Enter Valid Cnote Date. Docket date " + DocketDate.ToString("dd MMM yyy") + " not valid.";
            //    return View("Error");
            //}

            #endregion

            #region STAX Validation

            //if ((DVM.WMD.PAYBAS.ToUpper() == "P01" || DVM.WMD.PAYBAS.ToUpper() == "P03") && DVM.WMD.stax_paidby == "T" && DVM.WMD.TRN_MOD == "4")
            //{
            //    ViewBag.StrError = "Service Tax Paid by is not belongs to Contract.";
            //    return View("Error");
            //}

            //if (DVM.WMD.PAYBAS.ToUpper() == "P02")
            //{
            //    string stax_paidby_opts = OS.GetContractServiceChargessDetails(DVM.WMD.ContractId, DVM.WMD.TRN_MOD).FirstOrDefault().stax_paidby_opts;
            //    if (stax_paidby_opts != "" && !stax_paidby_opts.Contains(DVM.WMD.stax_paidby))
            //    {
            //        ViewBag.StrError = "Service Tax Paid by is not belongs to Contract.";
            //        return View("Error");
            //    }
            //}

            //if (DVM.WMD.stax_paidby == "T" && (DVM.WMDC.SVCTAX_Rate == 0 || DVM.WMDC.SVCTAX == 0) && DVM.WMD.PAYBAS.ToUpper() != "P04")
            //{
            //    ViewBag.StrError = "Service Tax related issue.";
            //    return View("Error");
            //}

            //if (DVM.WMD.stax_paidby == "P" && DVM.WMDC.SVCTAX > 0)
            //{
            //    ViewBag.StrError = "Service Tax related issue.";
            //    return View("Error");
            //}
            //if (DVM.WMD.PAYBAS.ToUpper() == "P04" && DVM.WMDC.DKTTOT > 0)
            //{
            //    ViewBag.StrError = "Billing Type FOC but " + ViewBag.DKTCalledAS + " > 0.";
            //    return View("Error");
            //}

            #endregion

            #region GST Validation

            //if (DVM.WMD.PAYBAS.ToUpper() != "P04")
            //{
            //    string GST_paidby_opts = OS.GetContractServiceChargessDetails(DVM.WMD.ContractId, DVM.WMD.TRN_MOD).FirstOrDefault().gstpaidby;
            //    if (GST_paidby_opts != "" && !GST_paidby_opts.Contains(DVM.WMD.stax_paidby))
            //    {
            //        ViewBag.StrError = "GST Paid by is not belongs to Contract.";
            //        return View("Error");
            //    }
            //}

            if (DVM.WMD.Moduletype == 2)
            {
                if (TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.CGSTAmount + TempWMDC.UTGSTAmount == 0)
                {
                    DVM.WMD.IsStaxExemp = true;
                }
                else
                {
                    DVM.WMD.IsStaxExemp = false;
                }
            }



            if (TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.CGSTAmount + TempWMDC.UTGSTAmount == 0 && DVM.WMD.PAYBAS.ToUpper() != "P04" && !DVM.WMD.IsStaxExemp)
            {
                ViewBag.StrError = "GST Paid by Transporter but GST not Chagred.";
                return View("Error");
            }

            if (DVM.WMD.PAYBAS.ToUpper() == "P04" && DVM.WMDC.DKTTOT > 0)
            {
                ViewBag.StrError = "Billing Type FOC but " + ViewBag.DKTCalledAS + " > 0.";
                return View("Error");
            }

            #endregion

            //#region DKT Calculation Check

            //decimal Charges_SubTotal = 0;
            //foreach (var item in DocketChargesList)
            //{
            //    Charges_SubTotal = Charges_SubTotal + item.ChargeAmount;
            //}
            //Charges_SubTotal = Charges_SubTotal + Convert.ToDecimal(DVM.WMDC.FREIGHT) + Convert.ToDecimal(DVM.WMD.Codchrg) + Convert.ToDecimal(DVM.WMD.DKTDACCCharges);

            //#endregion

            #region Check DKT Charges Calculation

            decimal DKTsubTotal = 0, Charges_SubTotal = 0, serviceTax = 0, SBC = 0, KKC = 0;

            foreach (var item in DocketChargesList)
            {
                Charges_SubTotal = Charges_SubTotal + item.ChargeAmount;
            }

            DKTsubTotal = Convert.ToDecimal(DVM.WMDC.FREIGHT) + Convert.ToDecimal(Charges_SubTotal);

            if (DVM.WMDC.FOV == null)
            {
                DVM.WMDC.FOV = 0;
            }
            else if (DVM.WMDC.FOV > 0)
            {
                DVM.WMDC.FOV = Convert.ToDecimal(string.Format("{0:0.00}", Convert.ToDecimal(DVM.WMDC.FOV)));
            }
            DKTsubTotal = DKTsubTotal + Convert.ToDecimal(DVM.WMDC.FOV);
            if (DVM.WMD.IsCODDOD)
            {
                DKTsubTotal = DKTsubTotal + Convert.ToDecimal(DVM.WMD.Codchrg);
            }
            if (DVM.WMD.IsDACC)
            {
                DKTsubTotal = DKTsubTotal + Convert.ToDecimal(DVM.WMD.DKTDACCCharges);
            }

            if (DVM.WMD.stax_paidby == "T")
            {
                //serviceTax = (DKTsubTotal * DVM.WMDC.SVCTAX_Rate) / 100;
                //SBC = (DKTsubTotal * Convert.ToDecimal(DVM.WMDC.SbcRate)) / 100;
                //KKC = (DKTsubTotal * Convert.ToDecimal(DVM.WMDC.KKCRate)) / 100;
            }
            /* START GST Changes By Chirag D  */
            decimal DKTTotal = DKTsubTotal + serviceTax + SBC + KKC + TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.UTGSTAmount + TempWMDC.CGSTAmount;
            /* END GST Changes By Chirag D  */

            //decimal DKTTotal = DKTsubTotal + serviceTax + SBC + KKC;

            #endregion

            if (DVM.WMD.DOCKNO == null)
                DVM.WMD.DOCKNO = "";

            DVM.WMD.hday_booked_yn = "Y";
            DVM.WMD.cutoff_applied_yn = "Y";


            if (DOCTYP == null || DOCTYP == "")
            {
                if (DVM.WMD.GCType == "C")
                    DVM.WMD.GCType = "CMP";
                else
                    DVM.WMD.GCType = "DKT";
            }
            else
                DVM.WMD.GCType = DOCTYP;

            DVM.WMD.IsCompletion = DVM.IsCompletion;
            Docket docket = GetDocketObject(DVM.WMD, DocketInvoiceList);
            DVM.WMDC.DOCKNO = docket.DockNo;

            DocketCharges docketcharges = GetChargesObject(DocketChargesList, DVM.WMDC);
            List<webx_Master_DOCKET_DOCUMENT> docketdoc = new List<webx_Master_DOCKET_DOCUMENT>();// GetDocumentObject(ListDocument);
            List<DocketInvoice> DocketInvoice = GetInvoiceObject(DocketInvoiceList);
            List<BarCodeSerial> bcs = new List<BarCodeSerial>(); // GetBarCodeSerialObject(ListBarCode);
            DataTable dt = new DataTable();

            docketcharges.FOVAmt = Convert.ToDecimal(DVM.WMDC.FOV);

            if (DVM.WMD.IsCODDOD)
                docketcharges.CODODCAmt = Convert.ToDecimal(DVM.WMD.Codchrg);

            if (DVM.WMD.IsDACC)
                docketcharges.DACCAmt = Convert.ToDecimal(DVM.WMD.DKTDACCCharges);

            string strXMLMasterEntry = "", strXMLChargesEntry = "", strXMLDocumentEntry = "", strXMLInvoiceEntry = "", strXMLBCSerialEntry = "";

            /* START GST Changes By Chirag D  */
            docketcharges.IGSTRate = TempWMDC.IGSTRate;
            docketcharges.IGSTAmount = TempWMDC.IGSTAmount;
            docketcharges.CGSTRate = TempWMDC.CGSTRate;
            docketcharges.CGSTAmount = TempWMDC.CGSTAmount;
            docketcharges.SGSTRate = TempWMDC.SGSTRate;
            docketcharges.SGSTAmount = TempWMDC.SGSTAmount;
            docketcharges.UTGSTRate = TempWMDC.UTGSTRate;
            docketcharges.UTGSTAmount = TempWMDC.UTGSTAmount;
            /* END GST Changes Chirag D  */

            strXMLMasterEntry = GetDocketXMLString(docket);
            strXMLChargesEntry = GetDocketChargesXMLString(docketcharges);
            strXMLDocumentEntry = GetDocketDocumentXMLString(docketdoc);
            strXMLInvoiceEntry = GetDocketInvoiceXMLString(DocketInvoice);
            strXMLBCSerialEntry = GetDocketBCSerialXMLString(bcs);


            if (Convert.ToInt32(Convert.ToDouble(DKTsubTotal)) != Convert.ToInt32(Convert.ToDouble(docketcharges.SubTotal)))
            {
                string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry_ChargesError", "", "Error");

                ViewBag.StrError = "There is a problem in Docket Subtotal Calculation. Please retry and check charges properly.";
                return View("Error");
            }
            double DKtTotDiff = Convert.ToDouble(DKTTotal) - Convert.ToDouble(docketcharges.DocketTotal);
            if (DKtTotDiff >= 1 || DKtTotDiff <= -1)
            {
                string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry_ChargesError", "", "Error");
                ViewBag.StrError = "There is a problem in Docket Total Calculation. Please retry and check charges properly.";
                return View("Error");
            }
            DataTable PrimaryOutPut = new DataTable();
            DataTable SecondaryOutPut = new DataTable();

            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                PrimaryOutPut = OS.MasterDocketEntry(strXMLMasterEntry, strXMLChargesEntry, strXMLDocumentEntry, strXMLInvoiceEntry, strXMLBCSerialEntry, BaseFinYear, trn);
                if (MS.GetRuleObject("M_CMP_BKG").defaultvalue == "Y" && MS.GetRuleObject("M_CMP_BKG_PC").defaultvalue == BaseCompanyCode)
                {
                    docket.DockNo = "";
                    docket.DocType = "CMP";
                    docket.CompanyCode = MS.GetRuleObject("M_CMP_BKG_SC").defaultvalue;

                    docket.ManualDockNo = docket.ManualDockNo;
                    docket.PrimaryCompanyDockNo = docket.DockNo;
                    docketcharges.DocketTotal = docketcharges.SubTotal * 96 / 100;
                    docketcharges.SubTotal = docketcharges.SubTotal * 96 / 100;
                    docketcharges.ServiceTax = 0;
                    docketcharges.EduCess = 0;
                    docketcharges.HEduCess = 0;

                    strXMLMasterEntry = GetDocketXMLString(docket);
                    strXMLChargesEntry = GetDocketChargesXMLString(docketcharges);

                    if (Convert.ToInt32(Charges_SubTotal) != Convert.ToInt32(docketcharges.SubTotal))
                    {
                        string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                        int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry_ChargesError", "", "Error");

                        ViewBag.StrError = "Docket Calculation was Wrong";
                        return View("Error");
                    }

                    if (docketcharges.SubTotal < docketcharges.Freight)
                    {
                        string SQRY = "exec USP_DOCKET_ENTRY_NewPortal '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                        int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "MasterDocketEntry", "", "Error");
                        ViewBag.StrError = "Error in Total";
                        return View("Error");
                    }
                    SecondaryOutPut = OS.MasterDocketEntry(strXMLMasterEntry, strXMLChargesEntry, strXMLDocumentEntry, strXMLInvoiceEntry, strXMLBCSerialEntry, BaseFinYear, trn);
                }


                string DockNo = "", BillNo = "";
                try
                {
                    DockNo = PrimaryOutPut.Rows[0]["DockNo"].ToString();
                    BillNo = PrimaryOutPut.Rows[0]["BillNo"].ToString();
                }
                catch (Exception ex)
                {
                    trn.Rollback();
                    con.Close();
                    ViewBag.StrError = ex.Message;
                    return View("Error");
                }
                trn.Commit();
                if (DVM.WMD.Moduletype == 1)
                {
                    return RedirectToAction("DocketDone", new { DOCKNO = DockNo, BILLNO = BillNo, id = 1 });
                }
                else
                {
                    return RedirectToAction("URDDocketDone", new { DOCKNO = DockNo, BILLNO = BillNo, id = 2 });
                }
            }
            catch (Exception ex)
            {
                string exmess = ex.Message.ToString().Replace('\n', '_');
                trn.Rollback();
                con.Close();
                // Response.Redirect("../ErrorPage.aspx?heading=DataBase Entry Failed.&detail1=May Be or Out of Range Wrong Data  Occured&detail2=" + exmess + "&suggestion2=Re Enter " + ViewBag.DKTCalledAs + " with proper Details");
                ViewBag.StrError = "Error " + exmess;
                return View("Error");
            }
        }
        //SDD Date Changes
        public JsonResult Get_Docket_SDD_Date(string DKTDate, int days)
        {
            string DKT_Date = Convert.ToDateTime(DKTDate).ToString("dd MMM yyyy");
            string SDD_Date = Convert.ToDateTime(DKTDate).AddDays(days).ToString("dd MMM yyyy");
            return new JsonResult()
            {
                Data = new
                {
                    DKT_Date = DKT_Date,
                    SDD_Date = SDD_Date
                }
            };
        }
        public ActionResult DocketDone(string DOCKNO, string BILLNO, int id)
        {
            Docket objDocket = new Docket();
            DataTable DT = OS.GetPartyEmailIdFromDocket(DOCKNO);
            ViewBag.DOCKNO = DOCKNO;
            ViewBag.BILLNO = BILLNO;
            ViewBag.id = id;

            if (DT.Rows.Count > 0)
            {
                objDocket.ConsignorEmail = Convert.ToString(DT.Rows[0][0]);
            }
            return View(objDocket);
        }
        public ActionResult URDDocketDone(string DOCKNO, string BILLNO, int id)
        {
            ViewBag.DOCKNO = DOCKNO;
            ViewBag.BILLNO = BILLNO;
            ViewBag.id = id;
            return View();
        }
        public JsonResult SendDocketOnMail(string Dockno, string EmailId)
        {
            int status = 0;
            List<ReportParameter> parameters = new List<ReportParameter>();
            parameters.Add(new ReportParameter("DocNo", Dockno));
            ReportViewer ReportViewer1 = new ReportViewer();
            ReportViewer1.ServerReport.ReportServerUrl = ReportServerURL;
            string Name = "", FromMailId = "", Password = "", Body = "", CCMailId = "", BCCMailId = "";
            string sql = "exec Usp_GetMailidandpwdfrommailtype 'fleetcnote'";
            DataTable DT1 = GF.GetDataTableFromSP(sql);

            FromMailId = DT1.Rows[0]["MailId"].ToString();
            Password = DT1.Rows[0]["Password"].ToString();
            Name = DT1.Rows[0]["RDLName"].ToString();
            CCMailId = DT1.Rows[0]["CCMailId"].ToString();
            BCCMailId = DT1.Rows[0]["BCCMailId"].ToString();
            ReportViewer1.ServerReport.ReportPath = ViewBag.StandardReportPath + "/" + Name;
            ReportViewer1.ServerReport.ReportServerCredentials = BaseGetReportCredentials;
            ReportViewer1.ServerReport.SetParameters(parameters);

            string mimeType;
            string encoding;
            string extension;
            string[] streams;
            Warning[] warnings;
            byte[] asPdf = ReportViewer1.ServerReport.Render("PDF", string.Empty, out mimeType, out encoding, out extension, out streams, out warnings);
            string outputPath = BasePodPath + "DocketPdf\\" + Dockno.Replace("/", "_") + ".pdf";

            using (FileStream fs = new FileStream(outputPath, FileMode.Create))
            {
                fs.Write(asPdf, 0, asPdf.Length);
                fs.Close();
            }

            /********************Send mail ********************************/


            try
            {
                MailMessage mail = new MailMessage();

                try
                {
                    mail.To.Add(EmailId);
                }
                catch
                {
                    mail.To.Add(BCCMailId);
                }

                try
                {
                    mail.CC.Add(CCMailId);
                }
                catch (Exception)
                {
                    mail.CC.Add(BCCMailId);
                }
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                mail.From = new MailAddress(FromMailId);
                mail.Subject = "CNOTE AUTOMAIL";

                //string Body = Subject;
                mail.Body = Body;

                mail.IsBodyHtml = true;

                try
                {
                    if (!string.IsNullOrEmpty(outputPath))
                    {
                        Attachment att = new Attachment(outputPath);
                        mail.Attachments.Add(att);
                    }


                }
                catch (Exception ex)
                {

                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.Credentials = new System.Net.NetworkCredential
                     (FromMailId, Password);

                smtp.EnableSsl = true;

                smtp.Send(mail);
                status = 1;
            }
            catch (Exception ep)
            {
                int Id = GF.SaveRequestServices(Dockno, "Error In Mail Sending", "0", ep.Message);
                status = 0;
            }
            /********************Send mail ********************************/
            return new JsonResult()
            {
                Data = new
                {
                    status = status
                }
            };
        }
        #region Copy From OLD Code

        protected Docket GetDocketObject(WebX_Master_Docket WMD, List<WebX_Master_Docket_Invoice> DocketInvoiceList)
        {
            Docket docket = new Docket();
            docket.DockNo = WMD.DOCKNO.Trim();
            try
            {
                docket.DockDate = Convert.ToDateTime(SYSConvert.ToDateTime(WMD.DOCKDT.ToString(), "en-GB"));
            }
            catch (Exception) { }
            //docket.DockDate = docket.DockDate.AddHours(SYSConvert.ToInt32(DOCKDT.ToString("hh")));
            //docket.DockDate = docket.DockDate.AddMinutes(SYSConvert.ToInt32(DOCKDT.ToString("min")));
            docket.DockDate = WMD.DOCKDT;
            docket.ManualDockNo = WMD.manual_dockno;
            docket.PayBase = WMD.PAYBAS;
            docket.PartyCode = WMD.PARTY_CODE.Trim();
            docket.IsEwayBillPartA = WMD.IsEwayBillPartA;
            docket.PartyCode = WMD.PARTY_CODE.Trim();
            docket.PartyName = WMD.party_name;
            if (docket.PartyName == "")
            {
                docket.PartyName = WMD.party_name;
            }
            if (docket.PartyCode == "")
            {
                if (WMD.party_as == "CSGN")
                {
                    docket.PartyCode = WMD.CSGNCD == "" ? "8888" : WMD.CSGNCD;
                    docket.PartyName = WMD.CSGNNM;
                }
                else
                {
                    docket.PartyCode = WMD.CSGECD == "" ? "8888" : WMD.CSGECD;
                    docket.PartyName = WMD.CSGENM;
                }
            }

            if (WMD.TAMNo == null)
                WMD.TAMNo = "";

            docket.OrgnLoc = WMD.ORGNCD.Trim();
            docket.DelLoc = WMD.DESTCD.ToUpper().Trim();
            docket.TAMNo = WMD.TAMNo.Trim();
            docket.QuotationNo = WMD.quot_no;
            docket.AgendaNo = WMD.agnd_no;
            docket.TransMode = WMD.TRN_MOD;
            docket.ServiceType = WMD.Service_Class;
            docket.FTLType = WMD.ftl_types;
            docket.PickUpDel = WMD.Pickup_Dely.Trim();
            docket.FromCity = MS.GetCityMasterObject().Where(c => c.city_code == Convert.ToDecimal(WMD.from_loc)).FirstOrDefault().Location;
            docket.ToCity = MS.GetCityMasterObject().Where(c => c.city_code == Convert.ToDecimal(WMD.to_loc)).FirstOrDefault().Location;
            docket.PackType = WMD.PKGSTY;
            docket.ProdType = WMD.PRODCD;
            docket.Remarks = WMD.spl_svc_req;
            docket.FlagVolumetric = (WMD.IsVolumetric == true ? "Y" : "N");
            docket.FlagCODDOD = (WMD.IsCODDOD == true ? "Y" : "N");
            docket.FlagODA = (WMD.IsODA == true ? "Y" : "N");
            docket.FlagLocal = (WMD.IsLocalDocket == true ? "Y" : "N");
            docket.FlagDACC = (WMD.IsDACC == true ? "Y" : "N");
            docket.FlagPermit = (WMD.permit_yn == "Y") ? "Y" : "N";
            docket.FlagHolidayApplied = WMD.hday_appl_yn;
            docket.LoadType = WMD.loadtype;
            docket.DopiNo = WMD.dopino;
            docket.BusinessType = WMD.businesstype;
            docket.Industry = WMD.Industry;
            docket.ContractID = WMD.ContractId;
            docket.ConsignorCode = (WMD.CSGNCD.CompareTo("") == 0 ? "8888" : WMD.CSGNCD.Trim());
            if (WMD.csgnaddrcd == null)
                WMD.csgnaddrcd = "";
            docket.ConsignorAddressCode = WMD.csgnaddrcd.Trim();


            docket.ConsignorName = WMD.CSGNNM; //(WMD.CSGNCD.CompareTo("") == 0 ? WMD.CSGNNM : WMD.CSGNNM);
            docket.ConsignorAddress = WMD.CSGNADDR;
            docket.ConsignorCity = WMD.CSGNCity;
            docket.ConsignorPinCode = WMD.CSGNPinCode;
            docket.ConsignorPhone = WMD.CSGNTeleNo;
            docket.ConsignorEmail = WMD.CSGNEmail;
            docket.ConsignorTinNo = WMD.CSGNTinNo;
            docket.ConsignorCSTNo = WMD.CSGNCSTNo;
            docket.ConsignorMobile = WMD.csgnmobile;
            docket.ConsignorTinNo = WMD.CSGNTinNo;
            docket.ConsignorCSTNo = WMD.CSGNCSTNo;
            docket.EWayBillNo = WMD.EWayBillNo;
            docket.EWayBillExpiredDate = WMD.EWayBillExpiredDate;
            docket.EWayInvoicevalue = WMD.EWayInvoicevalue;
            docket.ISCounterPickUpPRS = WMD.ISCounterPickUpPRS;
            docket.ISCounterDelivery = WMD.ISCounterDelivery;
            docket.deliveryAddress = WMD.deliveryAddress;

            docket.ConsigneeCode = (WMD.CSGECD.CompareTo("") == 0 ? "8888" : WMD.CSGECD.Trim());
            if (WMD.csgeaddrcd == null)
                WMD.csgeaddrcd = "";
            docket.ConsigneeAddressCode = WMD.csgeaddrcd.Trim();
            docket.ConsigneeName = WMD.CSGENM;//(WMD.CSGECD.CompareTo("") == 0 ? WMD.CSGENM : WMD.CSGENM);
            docket.ConsigneeAddress = WMD.CSGEADDR;
            docket.ConsigneeCity = WMD.CSGECity;
            docket.ConsigneePinCode = WMD.CSGEPinCode;
            docket.ConsigneePhone = WMD.CSGETeleNo;
            docket.ConsigneeEmail = WMD.CSGEEmail;
            docket.ConsigneeTinNo = WMD.CSGETinNo;
            docket.ConsigneeCSTNo = WMD.CSGECSTNo;
            docket.ConsigneeMobile = WMD.csgemobile;

            docket.ConsigneeTinNo = WMD.CSGETinNo;
            docket.ConsigneeCSTNo = WMD.CSGECSTNo;
            docket.ConsignorGSTNo = WMD.CustGSTNo;
            docket.ConsigneeGSTNo = WMD.CSGECustGSTNo;

            docket.ConsignorPanNo = WMD.CustPanNo;
            docket.ConsignorAadharNo = WMD.CustAadharNo;
            docket.ConsigneePanNo = WMD.CSGECustPanNo;
            docket.ConsigneeAadharNo = WMD.CSGECustAadharNo;
            docket.Moduletype = WMD.Moduletype;
            docket.EntrySheetNo = WMD.entrysheetno;
            docket.OBDNo = WMD.obdno;
            docket.PermitNo = WMD.Permit_No;
            docket.SDD_Date = WMD.SDD_Date;
            docket.MailId = WMD.MailId;

            try
            {
                docket.PermitDate = Convert.ToDateTime(WMD.permitdt.ToString());// Convert.ToDateTime(SYSConvert.ToDateTime(WMD.permitdt.ToString(), "en-GB"));
            }
            catch (Exception) { }
            try
            {
                docket.PermitValidityDate = Convert.ToDateTime(WMD.permitdt.ToString());// Convert.ToDateTime(SYSConvert.ToDateTime(WMD.permitdt.ToString(), "en-GB"));
            }
            catch (Exception) { }
            try
            {
                docket.PermitRecieveDate = Convert.ToDateTime(WMD.permitdt.ToString());//Convert.ToDateTime(SYSConvert.ToDateTime(WMD.permitdt.ToString(), "en-GB"));
            }
            catch (Exception) { }


            docket.PermitRecievedAt = WMD.permit_recvd_at;

            docket.InvoiceNo = DocketInvoiceList.FirstOrDefault().INVNO;
            docket.InvoiceDate = DocketInvoiceList.FirstOrDefault().INVDT;
            docket.InvoiceAmount = SYSConvert.ToInt64(WMD.DECVAL);

            if (!WMD.IsCompletion)
            {
                docket.BACode = (WMD.GCType.CompareTo("CMP") == 0 ? BaseUserName : OS.GetBACode(WMD.DOCKNO));
            }
            else
            {
                docket.BACode = BaseUserName;
            }
            //docket.BACode = (WMD.GCType.CompareTo("C") == 1 ? BaseUserName : OS.GetBACode(WMD.DOCKNO));

            docket.RiskType = WMD.insuyn; //(optcarrierrisk.Checked == true ? "C" : "O");
            docket.PolicyNo = WMD.insupl;
            docket.PolicyDate = Convert.ToDateTime(WMD.insupldt);
            //try
            //{
            //    docket.PolicyDate = Convert.ToDateTime(SYSConvert.ToDateTime(txtpolicydate.Text, "en-GB"));
            //}
            //catch (Exception) { }

            docket.InternalCovers = SYSConvert.ToDouble(WMD.tot_covers);
            docket.ModvatCovers = SYSConvert.ToDouble(WMD.tot_modvat);
            docket.CustomerRefNo = WMD.ctr_no;
            docket.CustRefGPNo = WMD.ctr_gpno;
            docket.CustRefDelNo = WMD.ctr_delno;
            docket.EngineNo = WMD.EngineNo;
            docket.ModelNo = WMD.ModelNo;
            docket.GPSNo = WMD.GPSNo;
            docket.ChassisNo = WMD.ChassisNo;
            docket.OrderID = WMD.ContractId;
            docket.ContainerID = WMD.ContractId;
            docket.SealNo = WMD.SerialNo;
            docket.PrivateMark = WMD.privatemark;
            docket.TPNumber = WMD.tpnumber;
            docket.NoOfPkgs = SYSConvert.ToInt64(WMD.PKGSNO);
            docket.ActualWeight = SYSConvert.ToInt64(WMD.ACTUWT);
            docket.ChargedWeight = SYSConvert.ToInt64(WMD.CHRGWT);
            docket.CODDODCharged = SYSConvert.ToDouble(WMD.CODCharged);
            docket.CODDODAmount = SYSConvert.ToDouble(WMD.codamt);
            docket.OctroiAmount = SYSConvert.ToDouble(WMD.oct_amt);
            docket.EDD = Convert.ToDateTime(WMD.CDELDT);
            docket.CustomerRefDate = Convert.ToDateTime(WMD.CustomerRefDate);
            //try
            //{
            //    docket.EDD = SYSConvert.ToDateTime(txtedd.Text, "en-GB");
            //}
            //catch (Exception) { }

            docket.STaxPaidBy = WMD.stax_paidby;
            docket.STaxRegNo = WMD.stax_regno;
            docket.LocalSTaxRegNo = WMD.stax_regno;
            docket.BilledAt = WMD.fincmplbr;
            docket.SourceDockNo = WMD.sourcedockno;
            docket.FlagMultiPickUp = WMD.multipickup_yn;
            docket.FlagMultiDelivery = WMD.multidelivery_yn;
            docket.FlagStaxExmpt = WMD.stax_exmpt_yn;
            docket.PartyAs = WMD.party_as;
            docket.FlagHolidayBooked = WMD.hday_booked_yn;
            docket.FlagCutoffApplied = WMD.cutoff_applied_yn;
            docket.CompanyCode = BaseCompanyCode;

            docket.DocType = WMD.GCType;

            //if (optentrytype.SelectedValue.CompareTo("N") == 0)
            //    docket.DocType = "DKT";
            //else
            //    docket.DocType = "CMP"; 
            docket.EntryBy = BaseUserName.ToUpper();
            docket.FlagMultiDelivery = "N";
            docket.FlagMultiPickUp = "N";
            docket.LoadType = "1";
            docket.Industry = "1";
            docket.PermitRecievedAt = "0";

            //START GST Changes Chirag D
            docket.OriginStateCode = WMD.OriginStateCode;
            docket.DestStateCode = WMD.DestStateCode;
            docket.IsUnionTeritory = WMD.IsUnionTeritory;
            docket.OriginStateName = WMD.OriginStateName;
            docket.DestStateName = WMD.DestStateName;
            docket.Origin_Area = WMD.Origin_Area;
            docket.Destination_Area = WMD.Destination_Area;
            docket.CustGSTNo = WMD.CustGSTNo;
            docket.CSGECustGSTNo = WMD.CSGECustGSTNo;


            docket.FlagMultiDelivery = "N";
            docket.FlagMultiPickUp = "N";
            docket.LoadType = "1";
            docket.Industry = "1";
            docket.PermitRecievedAt = "0";
            docket.IsCompletion = WMD.IsCompletion;
            docket.BillingState = WMD.BillingState;

            //END GST Changes Chirag D

            return docket;
        }

        protected DocketCharges GetChargesObject(List<QuickDocketBalancecharges> DocketChargesList, WebX_Master_Docket_Charges WMDC)
        {
            //ChargeCodeValue[] chargeCodeValue = new ChargeCodeValue[34];

            //for (int i = 1; i < 24; i++)
            //{
            //    chargeCodeValue[i] = new ChargeCodeValue();
            //    chargeCodeValue[i].ChargeCode = "SCHG" + i.ToString().PadLeft(2, '0');
            //}
            //for (int i = 1; i < 11; i++)
            //{
            //    chargeCodeValue[i + 23] = new ChargeCodeValue();
            //    chargeCodeValue[i + 23].ChargeCode = "UCHG" + i.ToString().PadLeft(2, '0');
            //}

            //foreach (WebX_Master_Docket_Charges DKTCRGS in DocketChargesList)
            //{
            //    for (int i = 1; i < 34; i++)
            //    {
            //        if (chargeCodeValue[i].ChargeCode.CompareTo(hdnchargecode1.Value) == 0)
            //        {
            //            chargeCodeValue[i].ChargeValue = SYSConvert.ToDouble(txtcharge1.Text.Trim());
            //        }
            //        if (chargeCodeValue[i].ChargeCode.CompareTo(hdnchargecode2.Value) == 0)
            //        {
            //            chargeCodeValue[i].ChargeValue = SYSConvert.ToDouble(txtcharge2.Text.Trim());
            //        }
            //    }
            //}

            //chargeCodeValue[11].ChargeValue = SYSConvert.ToDouble(txtfovcharged.Text);
            //if (chkcoddod.Checked == true)
            //    chargeCodeValue[12].ChargeValue = SYSConvert.ToDouble(txtcoddodcharged.Text);
            //else
            //    chargeCodeValue[12].ChargeValue = 0;

            //if (chkdacc.Checked == true)
            //    chargeCodeValue[13].ChargeValue = SYSConvert.ToDouble(txtdacccharged.Text);
            //else
            //    chargeCodeValue[13].ChargeValue = 0;

            foreach (QuickDocketBalancecharges DKTCRGS in DocketChargesList)
            {



            }

            WMDC.FRT_RATE = WMDC.FREIGHT_CALC;
            DocketCharges docketcharges = new DocketCharges();
            docketcharges.DockNo = WMDC.DOCKNO;
            docketcharges.RateType = WMDC.RATE_TYPE;
            docketcharges.FreightRate = SYSConvert.ToDouble(WMDC.FRT_RATE);
            docketcharges.Freight = SYSConvert.ToDouble(WMDC.FREIGHT);
            docketcharges.Charges = DocketChargesList;
            docketcharges.SubTotal = SYSConvert.ToDouble(WMDC.SubTotal);
            docketcharges.ServiceTax = SYSConvert.ToDouble(WMDC.SVCTAX);
            docketcharges.EduCess = SYSConvert.ToDouble(WMDC.CESS);
            docketcharges.HEduCess = SYSConvert.ToDouble(WMDC.hedu_cess);
            docketcharges.DocketTotal = SYSConvert.ToDouble(WMDC.DKTTOT);

            /* SB Cess */
            docketcharges.SbcRate = SYSConvert.ToDouble(WMDC.SbcRate);
            docketcharges.SBCess = SYSConvert.ToDouble(WMDC.SBCess);

            docketcharges.KKCRate = SYSConvert.ToDouble(WMDC.KKCRate);
            docketcharges.KKCAmount = SYSConvert.ToDouble(WMDC.KKCAmount);
            /* SB Cess */

            if (docketcharges.ServiceTax > 0)
                docketcharges.ServiceTaxRate = SYSConvert.ToDouble(WMDC.SVCTAX_Rate);

            /* Start GST Changes Chirag D  */
            docketcharges.GSTType = WMDC.GSTType;
            /* End GST Changes Chirag D  */
            return docketcharges;
        }

        //protected List<webx_Master_DOCKET_DOCUMENT> GetDocumentObject(List<webx_Master_DOCKET_DOCUMENT> ListDocument)
        //{
        //    List<webx_Master_DOCKET_DOCUMENT> docketdoc = new List<webx_Master_DOCKET_DOCUMENT>();

        //    foreach (webx_Master_DOCKET_DOCUMENT WMDD in ListDocument)
        //    {
        //        webx_Master_DOCKET_DOCUMENT dktdoc = new webx_Master_DOCKET_DOCUMENT();
        //        dktdoc.DOCKNO = WMDD.DOCKNO;
        //        dktdoc.DOCKSF = ".";
        //        dktdoc.SRNO = WMDD.SRNO;
        //        dktdoc.DOCUMENTNO = WMDD.DOCUMENTNO;
        //    }

        //    return docketdoc;
        //}

        protected List<BarCodeSerial> GetBarCodeSerialObject(List<BarCodeSerial> ListBarCode)
        {
            List<BarCodeSerial> bcslst = new List<BarCodeSerial>();

            foreach (BarCodeSerial bcs in ListBarCode)
            {
                BarCodeSerial DKTBCS = new BarCodeSerial();
                DKTBCS.DockNo = bcs.DockNo;
                DKTBCS.BCFrom = bcs.BCFrom;
                DKTBCS.BCTo = bcs.BCTo;
            }
            return bcslst;
        }

        protected List<DocketInvoice> GetInvoiceObject(List<WebX_Master_Docket_Invoice> DocketInvoiceList)
        {
            List<DocketInvoice> dockinv = new List<DocketInvoice>();

            foreach (WebX_Master_Docket_Invoice DKTINV in DocketInvoiceList)
            {
                DocketInvoice DOCKINV = new DocketInvoice();
                DOCKINV.DockNo = DKTINV.DOCKNO;
                DOCKINV.InvoiceNo = DKTINV.INVNO;

                //try
                //{
                //    DOCKINV.InvoiceDate = SYSConvert.ToDateTime(DKTINV.INVDT.ToString(), "en-GB");
                //}
                //catch (Exception) { }

                DOCKINV.InvoiceDate = DKTINV.INVDT;

                DOCKINV.DeclaredValue = SYSConvert.ToInt64(DKTINV.DECLVAL.ToString());
                DOCKINV.NoOfPkgs = SYSConvert.ToInt64(DKTINV.PKGSNO.ToString());
                DOCKINV.ActualWeight = SYSConvert.ToInt64(DKTINV.ACTUWT.ToString());
                DOCKINV.Vol_Length = SYSConvert.ToInt64(DKTINV.VOL_L.ToString());
                DOCKINV.Vol_Breadth = SYSConvert.ToInt64(DKTINV.VOL_B.ToString());
                DOCKINV.Vol_Height = SYSConvert.ToInt64(DKTINV.VOL_H.ToString());
                DOCKINV.Vol_CFT = SYSConvert.ToInt64(DKTINV.vol_cft.ToString());
                DOCKINV.Part_No = DKTINV.Part_No;


                DOCKINV.ProductName = DKTINV.ProductName;
                DOCKINV.ProductDesc = DKTINV.ProductDesc;
                DOCKINV.HSNCode = DKTINV.HSNCode;
                DOCKINV.Qty = DKTINV.Qty;
                DOCKINV.UnitPrice = DKTINV.UnitPrice;
                DOCKINV.GSTRate = DKTINV.GSTRate;
                DOCKINV.PKGSTY = DKTINV.PKGSTY;
                DOCKINV.PRODCD = DKTINV.PRODCD;

                dockinv.Add(DOCKINV);
            }

            return dockinv;

        }

        public static string GetDocketXMLString(Docket dkt)
        {
            string DocketXMLString = "";
            if (dkt.DockDate < DateTime.MinValue || dkt.DockDate > DateTime.MaxValue)
                dkt.DockDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.EDD <= DateTime.MinValue || dkt.EDD >= DateTime.MaxValue)
                dkt.EDD = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.PermitDate <= DateTime.MinValue || dkt.PermitDate >= DateTime.MaxValue)
                dkt.PermitDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.PermitRecieveDate <= DateTime.MinValue || dkt.PermitRecieveDate >= DateTime.MaxValue)
                dkt.PermitRecieveDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.PermitValidityDate <= DateTime.MinValue || dkt.PermitValidityDate >= DateTime.MaxValue)
                dkt.PermitValidityDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.PolicyDate <= DateTime.MinValue || dkt.PolicyDate >= DateTime.MaxValue)
                dkt.PolicyDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            if (dkt.InvoiceDate <= DateTime.MinValue || dkt.InvoiceDate >= DateTime.MaxValue)
                dkt.InvoiceDate = SYSConvert.ToDateTime("01/01/1990", "en-GB");

            dkt.SDD_Date_str = dkt.SDD_Date.ToString("dd MMM yyyy");

            if (dkt.SDD_Date_str == "01 Jan 0001" || dkt.SDD_Date_str == "01 Jan 1990")
            {
                dkt.SDD_Date_str = null;
            }

            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(dkt.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            x.Serialize(stream, dkt);
            stream.Position = 0;
            XmlDocument xd = new XmlDocument();
            xd.Load(stream);
            DocketXMLString = xd.InnerXml;

            DocketXMLString = DocketXMLString.Replace("&", "&amp;");
            return DocketXMLString;
        }

        public static string GetDocketChargesXMLString(DocketCharges charges)
        {

            string DocketChargesXMLString = "";
            DocketChargesXMLString = "<DocketCharges>"
                   + "<DockNo>" + charges.DockNo + "</DockNo>"
                   + "<FreightRate>" + charges.FreightRate.ToString("F2") + "</FreightRate>"
                   + "<RateType>" + charges.RateType + "</RateType>"
                   + "<FOVRate>" + charges.FOVRate.ToString("F2") + "</FOVRate>"
                   + "<Freight>" + charges.Freight.ToString("F2") + "</Freight>";

            foreach (var itm in charges.Charges)
            {
                if (itm.ChargeAmount == null)
                    itm.ChargeAmount = 0;

                DocketChargesXMLString = DocketChargesXMLString + "<" + itm.ChargeCode + ">";
                DocketChargesXMLString = DocketChargesXMLString + itm.ChargeAmount.ToString("F2");
                DocketChargesXMLString = DocketChargesXMLString + "</" + itm.ChargeCode + ">";
            }

            DocketChargesXMLString = DocketChargesXMLString + "<SCHG11>";
            DocketChargesXMLString = DocketChargesXMLString + charges.FOVAmt.ToString("F2");
            DocketChargesXMLString = DocketChargesXMLString + "</SCHG11>";


            DocketChargesXMLString = DocketChargesXMLString + "<SCHG12>";
            DocketChargesXMLString = DocketChargesXMLString + charges.CODODCAmt.ToString("F2");
            DocketChargesXMLString = DocketChargesXMLString + "</SCHG12>";

            DocketChargesXMLString = DocketChargesXMLString + "<SCHG13>";
            DocketChargesXMLString = DocketChargesXMLString + charges.DACCAmt.ToString("F2");
            DocketChargesXMLString = DocketChargesXMLString + "</SCHG13>";

            DocketChargesXMLString = DocketChargesXMLString + "<SubTotal>" + charges.SubTotal.ToString("F2") + "</SubTotal>"
            + "<ServiceTax>" + charges.ServiceTax.ToString("F2") + "</ServiceTax>"
            + "<EduCess>" + charges.EduCess.ToString("F2") + "</EduCess>"
            + "<HEduCess>" + charges.HEduCess.ToString("F2") + "</HEduCess>"
            + "<DocketTotal>" + charges.DocketTotal.ToString("F2") + "</DocketTotal>"
            + "<ServiceTaxRate>" + charges.ServiceTaxRate.ToString("F2") + "</ServiceTaxRate>"

            /* SB Cess */
            + "<SbcRate>" + charges.SbcRate.ToString("F2") + "</SbcRate>"
            + "<SBCess>" + charges.SBCess.ToString("F2") + "</SBCess>"
            /* SB Cess */

            /* KKC */
            + "<KKCRate>" + charges.KKCRate.ToString("F2") + "</KKCRate>"
            + "<KKCAmount>" + charges.KKCAmount.ToString("F2") + "</KKCAmount>"
            /* KKC*/

            /*Green Tax */
            + "<GreenTax>" + charges.GreenTax.ToString("F2") + "</GreenTax>"

            /* Green Tax */

            /* START GST Changes By Chirag D  */
            + "<IGSTRate>" + charges.IGSTRate.ToString("F2") + "</IGSTRate>"
            + "<IGSTAmount>" + charges.IGSTAmount.ToString("F2") + "</IGSTAmount>"
            + "<CGSTRate>" + charges.CGSTRate.ToString("F2") + "</CGSTRate>"
            + "<CGSTAmount>" + charges.CGSTAmount.ToString("F2") + "</CGSTAmount>"
            + "<SGSTRate>" + charges.SGSTRate.ToString("F2") + "</SGSTRate>"
            + "<SGSTAmount>" + charges.SGSTAmount.ToString("F2") + "</SGSTAmount>"
            + "<UTGSTRate>" + charges.UTGSTRate.ToString("F2") + "</UTGSTRate>"
            + "<UTGSTAmount>" + charges.UTGSTAmount.ToString("F2") + "</UTGSTAmount>"
            + "<GSTType>" + charges.GSTType + "</GSTType>"
            /* END GST Changes By Chirag D */

            + "</DocketCharges>";
            DocketChargesXMLString = DocketChargesXMLString.Replace("&", "&amp;");

            return DocketChargesXMLString;
        }

        public static string GetDocketDocumentXMLString(List<webx_Master_DOCKET_DOCUMENT> dockdoc)
        {
            string DocketDocumentXMLString = "";
            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(dockdoc.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            x.Serialize(stream, dockdoc);
            stream.Position = 0;
            XmlDocument xd = new XmlDocument();
            xd.Load(stream);
            DocketDocumentXMLString = xd.InnerXml;
            DocketDocumentXMLString = DocketDocumentXMLString.Replace("&", "&amp;");
            return DocketDocumentXMLString;
        }

        public static string GetDocketInvoiceXMLString(List<DocketInvoice> dockinv)
        {
            string DocketDocumentXMLString = "";

            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(dockinv.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            x.Serialize(stream, dockinv);
            stream.Position = 0;
            XmlDocument xd = new XmlDocument();
            xd.Load(stream);
            DocketDocumentXMLString = xd.InnerXml;
            DocketDocumentXMLString = DocketDocumentXMLString.Replace("&", "&amp;");
            return DocketDocumentXMLString;
        }

        public static string GetDocketBCSerialXMLString(List<BarCodeSerial> bcs)
        {
            string DocketBCSerialXMLString = "";

            DocketBCSerialXMLString = "<root>";

            foreach (var itm in bcs)
            {
                double from = 0, to = 0;
                int numlen;
                char[] arrfrom, arrto;
                string numstrfrom = "", numstrto = "", alfafrom = "", alfato = "", bcserialno = "";
                arrfrom = itm.BCFrom.ToCharArray();
                arrto = itm.BCTo.ToCharArray();
                for (int j = 0; j < arrfrom.Length; j++)
                {
                    if (Convert.ToInt16(arrfrom[j]) > 47 && Convert.ToInt16(arrfrom[j]) < 58)
                    {
                        numstrfrom = numstrfrom + arrfrom[j];
                        numstrto = numstrto + arrto[j];
                    }
                    else
                    {
                        alfafrom = alfafrom + arrfrom[j];
                        alfato = alfato + arrto[j];
                    }
                }
                numlen = numstrfrom.Length;
                from = SYSConvert.ToDouble(numstrfrom);
                to = SYSConvert.ToDouble(numstrto);

                for (double num = from; num <= to; num++)
                {
                    bcserialno = alfafrom + num.ToString().Split('.')[0].PadLeft(numlen, '0');
                    DocketBCSerialXMLString += "<bc><bcsrno>" + bcserialno + "</bcsrno></bc>";
                }
            }
            DocketBCSerialXMLString += "</root>";
            DocketBCSerialXMLString = DocketBCSerialXMLString.Replace("&", "&amp;");
            return DocketBCSerialXMLString;
        }

        #endregion

        public JsonResult GetStep1Details()
        {

            List<Step1Model> listitm = new List<Step1Model>();
            listitm.Add(OS.GetStep1Details(BaseLocationCode));

            return new JsonResult()
            {
                Data = new
                {
                    DATE_RULE = listitm.FirstOrDefault().DATE_RULE,
                    FLAG_COMPUTERISED = listitm.FirstOrDefault().FLAG_COMPUTERISED,
                    FLAG_ROUND = listitm.FirstOrDefault().FLAG_ROUND,
                    PARTY_ENTRY_SETTING = listitm.FirstOrDefault().PARTY_ENTRY_SETTING,
                    contractsets = listitm.FirstOrDefault().contractsets,

                }
            };


        }

        public JsonResult GetRuleDetails(string Key, string Paybas)
        {

            List<webx_rules_docket> listitm = new List<webx_rules_docket>();

            if (Paybas == null || Paybas == "")
                listitm.Add(MS.GetRuleObject(Key));
            else
                listitm.Add(MS.GetRuleObjectPaybas(Key, Paybas));

            return new JsonResult()
            {
                Data = new
                {
                    DefaultValue = listitm.FirstOrDefault().defaultvalue,
                }
            };


        }

        public JsonResult GetServiceTax(string ServiceType, string ServiceTaxPaidBy, string TransMode, DateTime DOCKDT, double FinSubtotal,
            bool IsStaxExemp, string FlagRoundOff, string FlagStaxBifer)
        {
            List<DocketServiceTax> listitm = new List<DocketServiceTax>();

            double subtotal = 0.00, DocketTotal = 0.00;

            DocketServiceTax dst = new DocketServiceTax();
            dst.Date = Convert.ToDateTime(DOCKDT);
            dst.SubTotal = SYSConvert.ToDouble(FinSubtotal);
            dst.TransMode = TransMode;
            dst.ServiceType = ServiceType;
            dst.StaxPayer = ServiceTaxPaidBy;

            dst = OS.GetServiceTax(dst);

            string ServiceTaxRate = "0";
            string CollServiceTaxRate = "0";

            string CessRate = "0";
            string CollCess = "0";

            string HCessRate = "0";
            string CollHCess = "0";

            string SBRate = "0";
            string CollSBRate = "0";

            string KKCRate = "0";
            string CollKKCRate = "0";

            ServiceTaxRate = dst.STaxRate.ToString("F2");
            CessRate = dst.StdEduCessRate.ToString("F2");
            HCessRate = dst.StdHEduCessRate.ToString("F2");
            SBRate = dst.SbcRate.ToString("F2");
            KKCRate = dst.KKCRate.ToString("F2");

            if (FlagStaxBifer.CompareTo("Y") == 0)
            {
                subtotal = FinSubtotal / (1 + (dst.STaxRate / 100) + (dst.STaxRate * dst.EduCessRate / 10000) + (dst.STaxRate * dst.HEduCessRate / 10000));
                dst.ServiceTax = subtotal * dst.STaxRate / 100;
                dst.EduCess = dst.ServiceTax * dst.EduCessRate / 100;
                dst.HEduCess = dst.ServiceTax * dst.HEduCessRate / 100;
                dst.SBCess = FinSubtotal * dst.SbcRate / 100;
                dst.KKCAmount = FinSubtotal * dst.KKCRate / 100;
            }
            else
            {
                subtotal = FinSubtotal;// / (1 + (dst.STaxRate / 100) + (dst.STaxRate * dst.EduCessRate / 10000) + (dst.STaxRate * dst.HEduCessRate / 10000));
                dst.ServiceTax = subtotal * dst.STaxRate / 100;
                dst.EduCess = dst.ServiceTax * dst.EduCessRate / 100;
                dst.HEduCess = dst.ServiceTax * dst.HEduCessRate / 100;
                dst.SBCess = FinSubtotal * dst.SbcRate / 100;
                dst.KKCAmount = FinSubtotal * dst.KKCRate / 100;
            }

            if (IsStaxExemp)
            {
                CollServiceTaxRate = "0";
                CollCess = "0";
                CollHCess = "0";
                CollSBRate = "0";
                CollKKCRate = "0";
            }
            else
            {
                if (dst.SubTotal > dst.ExceedAmount && dst.StaxPayer.CompareTo("T") == 0)
                {
                    if (FlagRoundOff.CompareTo("Y") == 0)
                    {
                        CollServiceTaxRate = dst.ServiceTax.ToString("F0");
                        CollCess = dst.EduCess.ToString("F0");
                        CollHCess = dst.HEduCess.ToString("F0");
                        CollSBRate = dst.SBCess.ToString("F0");
                        CollKKCRate = dst.KKCAmount.ToString("F0");
                    }
                    else
                    {
                        CollServiceTaxRate = dst.ServiceTax.ToString("F2");
                        CollCess = dst.EduCess.ToString("F2");
                        CollHCess = dst.HEduCess.ToString("F2");
                        CollSBRate = dst.SBCess.ToString("F2");
                        CollKKCRate = dst.KKCAmount.ToString("F2");
                    }
                }
                else
                {
                    CollServiceTaxRate = "0";
                    CollCess = "0";
                    CollHCess = "0";
                    CollSBRate = "0";
                    CollKKCRate = "0";
                }
            }

            DocketTotal = FinSubtotal + SYSConvert.ToDouble(CollServiceTaxRate) + SYSConvert.ToDouble(CollCess) + SYSConvert.ToDouble(CollHCess)
          + SYSConvert.ToDouble(CollSBRate) + SYSConvert.ToDouble(CollKKCRate);

            var Diff = DocketTotal - FinSubtotal - SYSConvert.ToDouble(CollServiceTaxRate) - SYSConvert.ToDouble(CollCess) - SYSConvert.ToDouble(CollHCess)
          - SYSConvert.ToDouble(CollSBRate) - SYSConvert.ToDouble(CollKKCRate);

            string strDocketTotal = "";
            if (FlagRoundOff.CompareTo("Y") == 0)
                strDocketTotal = DocketTotal.ToString("F0");
            else
                strDocketTotal = DocketTotal.ToString("F2");

            return new JsonResult()
            {
                Data = new
                {
                    ServiceTaxRate = ServiceTaxRate,
                    CollServiceTaxRate = CollServiceTaxRate,
                    CessRate = CessRate,
                    CollCess = CollCess,
                    HCessRate = HCessRate,
                    CollHCess = CollHCess,
                    SBRate = SBRate,
                    CollSBRate = CollSBRate,
                    KKCRate = KKCRate,
                    CollKKCRate = CollKKCRate,
                    DocketTotal = strDocketTotal,
                }
            };
        }

        public JsonResult GetStep2Details(string PartyCode, string Destination, string Paybas, string Doctype, DateTime DOCKDT, decimal ToCity)
        {

            List<Step2Model> listitm = new List<Step2Model>();
            if (Convert.ToString(ToCity) == "")
            {
                ToCity = 0;
            }
            listitm.Add(OS.GetStep2Details("<root><docket><dockno>4300603</dockno><dockdt>" + DOCKDT.ToString("dd MMM yyyy") + "</dockdt><paybas>" + Paybas + "</paybas><party_code>" + PartyCode + "</party_code><orgncd>" + BaseLocationCode + "</orgncd><destcd>" + Destination + "</destcd><tamno></tamno><doctype>" + Doctype + "</doctype><ToCity>" + ToCity + "</ToCity></docket></root>"));

            return new JsonResult()
            {
                Data = new
                {
                    CONTRACTID = listitm.FirstOrDefault().CONTRACTID,
                    SEARCH = listitm.FirstOrDefault().SEARCH,
                    BEHAVE = listitm.FirstOrDefault().BEHAVE,
                    FLAG_LOT = listitm.FirstOrDefault().FLAG_LOT,
                    FLAG_MULTI_PICKUPDEL = listitm.FirstOrDefault().FLAG_MULTI_PICKUPDEL,

                    FCITY = listitm.FirstOrDefault().FCITY,
                    TCITY = listitm.FirstOrDefault().TCITY,
                    FLAG_PERMIT = listitm.FirstOrDefault().FLAG_PERMIT,
                    STAX_EXMPT_YN = listitm.FirstOrDefault().STAX_EXMPT_YN,
                    Flag_Defer = listitm.FirstOrDefault().Flag_Defer,

                    TransMode = listitm.FirstOrDefault().TransMode,
                    ServiceType = listitm.FirstOrDefault().ServiceType,
                    PKGDelyType = listitm.FirstOrDefault().PKGDelyType,
                    FTLType = listitm.FirstOrDefault().FTLType,


                    Risktype = listitm.FirstOrDefault().Risktype,
                    CODDODCharge = listitm.FirstOrDefault().CODDODCharge,
                    DACCCharge = listitm.FirstOrDefault().DACCCharge,
                    BillingLocation = listitm.FirstOrDefault().BillingLocation,

                    IsVolumentric = listitm.FirstOrDefault().IsVolumentric,
                    IsCODDOD = listitm.FirstOrDefault().IsCODDOD,
                    IsDACC = listitm.FirstOrDefault().IsDACC,
                    CFTWeightType = listitm.FirstOrDefault().CFTWeightType,

                    ChargeBas = listitm.FirstOrDefault().ChargeBas,

                    //Freight Contract 

                    MinFreightType = listitm.FirstOrDefault().MinFreightType,
                    FLAG_Freight = listitm.FirstOrDefault().FLAG_Freight,
                    FLAG_Subtotal = listitm.FirstOrDefault().FLAG_Subtotal,

                    //CODDOD
                    CODRateType = listitm.FirstOrDefault().CODRateType,
                    Min_CODCharged = listitm.FirstOrDefault().Min_CODCharged,
                    CODCharged = listitm.FirstOrDefault().CODCharged,

                    //DACC
                    DACCRateType = listitm.FirstOrDefault().DACCRateType,
                    DACCCharged = listitm.FirstOrDefault().DACCCharged,
                    Min_DACCCharged = listitm.FirstOrDefault().Min_DACCCharged,


                }
            };


        }

        public JsonResult GetContractServiceChargessDetails(string ContractId, string TransType, string Paybas)
        {
            List<webx_custcontract_servicecharges> listitm = new List<webx_custcontract_servicecharges>();
            listitm = OS.GetContractServiceChargessDetails(ContractId, TransType);
            var STAX_PAIDBY_RULE = MS.GetRuleObjectPaybas("STAX_PAIDBY_RULE", Paybas).defaultvalue;

            string stax_paidby_opts = "";
            if (STAX_PAIDBY_RULE == "CST")
            {
                stax_paidby_opts = listitm.FirstOrDefault().stax_paidby;
            }
            else
            {
                /* Changed based on Requirement from Mukeshji on 30 Nov 2016 */
                stax_paidby_opts = listitm.FirstOrDefault().stax_paidby_opts;
            }

            if (listitm.Count == 0)
                listitm.Add(new webx_custcontract_servicecharges());

            return new JsonResult()
            {
                Data = new
                {
                    cft_measure = listitm.FirstOrDefault().cft_measure,
                    cft_ratio = listitm.FirstOrDefault().cft_ratio,
                    stax_paidby_opts = stax_paidby_opts,
                    stax_paidby = listitm.FirstOrDefault().stax_paidby,
                    stax_paidby_enabled = listitm.FirstOrDefault().stax_paidby_enabled,
                    MinFreightBas = listitm.FirstOrDefault().min_frtbas,
                    MinFreightBasRate = listitm.FirstOrDefault().min_frtbasrate,
                    MinFreightRate = listitm.FirstOrDefault().min_frtrate_per,
                    MinSubtotal = listitm.FirstOrDefault().min_subtot_per,
                    FreightRateLowerLimit = listitm.FirstOrDefault().lowlim_frt,
                    FreightRateUpperLimit = listitm.FirstOrDefault().upplim_frt,
                    SubTotalUpperLimit = listitm.FirstOrDefault().lowlim_subtot,
                    SubTotalLowerLimit = listitm.FirstOrDefault().upplim_subtot,
                    FuelRateType = listitm.FirstOrDefault().fuelsurchrgbas,
                    FuelRate = listitm.FirstOrDefault().fuelsurchrg,
                    MinFuleCharge = listitm.FirstOrDefault().min_fuelsurchrg,
                    MaxFuelCharge = listitm.FirstOrDefault().max_fuelsurchrg,

                    /* START GST Changes By Chirag D */
                    gstpaidby = listitm.FirstOrDefault().gstpaidby,
                    /* END GST Changes By Chirag D */

                }
            };
        }

        public JsonResult GetProRataCharge(string FLAG_PRORATA, string RateType, string CHRGWT, string FTLType, string FREIGHT, string ServiceType, string FREIGHTRate)
        {

            if (FREIGHTRate == "" || FREIGHTRate == null)
                FREIGHTRate = "0";
            if (FREIGHT == "" || FREIGHT == null)
                FREIGHT = "0";


            string FreightCharge = FREIGHT, FreightRate = FREIGHTRate;
            if (FLAG_PRORATA.CompareTo("Y") == 0 && RateType.CompareTo("F") == 0)
            {
                double freightcharge, maxcapacity, chargedweight;
                freightcharge = SYSConvert.ToDouble(FREIGHT);
                maxcapacity = OS.GetFTLMaxCapacity(FTLType);
                chargedweight = SYSConvert.ToDouble(CHRGWT);
                //  hdnftlmaxcapacity.Value = maxcapacity.ToString("F2");
                if (ServiceType.CompareTo("2") == 0)
                {
                    freightcharge = freightcharge + OS.GetProRataCharge(freightcharge, chargedweight, maxcapacity);
                    FreightCharge = freightcharge.ToString("F2");
                    FreightRate = freightcharge.ToString("F2");
                }
            }

            return new JsonResult()
            {
                Data = new
                {

                    FreightCharge = FreightCharge,
                    FreightRate = FreightRate,

                }
            };
        }

        public JsonResult GetOtherChargesDetails(
         string ChargeRule, string BaseCode1, string ChargeSubRule, string BaseCode2,
         string ChargedWeight, string ContractID, string Destination, string Depth,
         string FlagProceed, string FromCity, string FTLType, string NoOfPkgs,
         string Origin, string PayBase, string ServiceType, string ToCity,
         string TransMode, string OrderID, string InvAmt, DateTime DOCKDT, string ProdType, string PackType, string RiskType)
        {

            string strXMLCharges = "<ContractKeys>";
            strXMLCharges += "<ChargeType>BKG</ChargeType>"
                + "<BasedOn1>" + ChargeRule + "</BasedOn1>"
                + "<BaseCode1>" + BaseCode1 + "</BaseCode1>"
                + "<BasedOn2>NONE</BasedOn2>"
                + "<BaseCode2>NONE</BaseCode2>"
                + "<ContractID>" + ContractID + "</ContractID>"
                + "<FromCity>" + FromCity + "</FromCity>"
                + "<ToCity>" + ToCity + "</ToCity>"
                + "<OrgnLoc>" + Origin + "</OrgnLoc>"
                + "<DelLoc>" + Destination + "</DelLoc>"
                + "<PayBase>" + PayBase + "</PayBase>"
                + "<ServiceType>" + ServiceType + "</ServiceType>"
                + "<FTLType>" + FTLType + "</FTLType>"
                + "<TransMode>" + TransMode + "</TransMode>"
                + "<ProdType>" + ProdType + "</ProdType>"
                + "<PackType>" + PackType + "</PackType>"
                + "<FlagMultiPickUp>" + SYSConvert.ToY_N(false) + "</FlagMultiPickUp>"
                + "<FlagMultiDelivery>" + SYSConvert.ToY_N(false) + "</FlagMultiDelivery>"
                + "<RiskType>" + RiskType + "</RiskType>"
                + "<ChargedWeight>" + ChargedWeight + "</ChargedWeight>"
                + "<NoOfPkgs>" + NoOfPkgs + "</NoOfPkgs>"
                + "<DeclaredValue>" + InvAmt + "</DeclaredValue>"
            + "<FreightCharge>0</FreightCharge></ContractKeys>";


            DataTable DT = OS.GetStandardCharges(strXMLCharges);
            List<SelectListItem> litsitm = new List<SelectListItem>();
            if (DT.Rows.Count > 0)
            {

                foreach (DataRow dtr in DT.Rows)
                {
                    litsitm.Add(new SelectListItem() { Text = dtr["Charge"].ToString(), Value = dtr["chargecode"].ToString() });

                }
            }


            return Json(litsitm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFreightContractDetails(
                  string ChargeRule, string BaseCode1, string ChargeSubRule, string BaseCode2,
                  string ChargedWeight, string ContractID, string Destination, string Depth,
                  string FlagProceed, string FromCity, string FTLType, string NoOfPkgs, string ChargedWeright,
                  string Origin, string PayBase, string ServiceType, string ToCity,
                  string TransMode, string OrderID, string InvAmt, DateTime DOCKDT)
        {

            ContractKeys ckeys = new ContractKeys();
            ckeys.BasedOn1 = ChargeRule;
            ckeys.BaseCode1 = BaseCode1;
            ckeys.BasedOn2 = ChargeSubRule;
            ckeys.BaseCode2 = BaseCode2;
            ckeys.ChargedWeight = SYSConvert.ToInt64(ChargedWeight);
            ckeys.ContractID = ContractID;
            ckeys.DelLoc = Destination;
            ckeys.Depth = Depth;
            ckeys.FlagProceed = FlagProceed;
            ckeys.FromCity = FromCity;
            ckeys.FTLType = FTLType;
            ckeys.NoOfPkgs = SYSConvert.ToInt64(NoOfPkgs);
            ckeys.ChargedWeight = SYSConvert.ToDouble(ChargedWeright);
            ckeys.OrgnLoc = Origin;
            ckeys.PayBase = PayBase;
            ckeys.ServiceType = ServiceType;
            ckeys.ToCity = ToCity;
            ckeys.TransMode = TransMode;
            ckeys.OrderID = OrderID.Trim();
            ckeys.InvAmt = SYSConvert.ToInt64(InvAmt);

            ckeys = OS.ExecuteContractLocationFreight(ckeys);



            //string .EDD_TRANSIT = MS.GetRuleObject("EDD_TRANSIT").defaultvalue;
            //WMD.EDD_NDAYS = MS.GetRuleObject("EDD_NDAYS").defaultvalue;
            //WMD.FLAG_CUTOFF = MS.GetRuleObject("FLAG_CUTOFF").defaultvalue;
            //WMD.EDD_LOCAL = MS.GetRuleObject("EDD_LOCAL").defaultvalue;

            //WMD.EDD_ADD_HDAYS = MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue;

            /*****************************************************************************************************
        *      CALCULATING EDD RULE WISE
        *****************************************************************************************************/
            string flag_transit = MS.GetRuleObject("EDD_TRANSIT").defaultvalue;
            if (flag_transit.CompareTo("C") == 0)
            {
                // BY DEFAULT TAKEN FROM CUSTOMER CONTRACT MASTER
                // SPACE LEFT FOR FUTURE CODE
            }
            else if (flag_transit.CompareTo("CT") == 0)
            {
                if (ckeys.TRDays == 0)
                {
                    ckeys.ContractID = PayBase + "8888";
                    // ckeys.TRDays = DocketController.GetEDDFromContract(ckeys);
                }
            }
            else if (flag_transit.CompareTo("T") == 0)
            {
                ckeys.ContractID = PayBase + "8888";
                // ckeys.TRDays = DocketController.GetEDDFromContract(ckeys);
            }
            //  hdntrdays.Value = ckeys.TRDays.ToString();

            int ndays = 0;
            try
            {
                ndays = SYSConvert.ToInt32(MS.GetRuleObject("EDD_NDAYS").defaultvalue);
            }
            catch (Exception) { ndays = 0; }
            if (ckeys.TRDays == null || ckeys.TRDays == 0)
            {
                if (PayBase.ToUpper() == "P02")
                {

                }
            }
            ckeys.TRDays = ckeys.TRDays + ndays;

            ndays = 0;
            string cutoffrule = MS.GetRuleObject("FLAG_CUTOFF").defaultvalue;

            if (cutoffrule.CompareTo("CL") == 0)
            {
                ndays = OS.GetContractCutoffDays(ContractID, Origin, SYSConvert.ToInt32(DOCKDT.ToString("hh")), SYSConvert.ToInt32(DOCKDT.ToString("min")), TransMode);
                if (ndays == 0)
                {
                    ndays = OS.GetLocationCutoffDays(Origin, SYSConvert.ToInt32(DOCKDT.ToString("hh")), SYSConvert.ToInt32(DOCKDT.ToString("min")), TransMode);
                }
            }
            else if (cutoffrule.CompareTo("LC") == 0)
            {
                ndays = OS.GetLocationCutoffDays(Origin, SYSConvert.ToInt32(DOCKDT.ToString("hh")), SYSConvert.ToInt32(DOCKDT.ToString("min")), TransMode);
                if (ndays == 0)
                {
                    ndays = OS.GetContractCutoffDays(ContractID, Origin, SYSConvert.ToInt32(DOCKDT.ToString("hh")), SYSConvert.ToInt32(DOCKDT.ToString("min")), TransMode);
                }
            }

            string FlagCutOff = "", Flagtblcutoff = "", EDD = "", FlgConfirmEddDiv = "", Confirmdesc = "", BtnConfirm = "", BtnCancel = "";

            if (ndays > 0)
            {
                FlagCutOff = "Y";
                Flagtblcutoff = "Y";
            }
            else
            {
                FlagCutOff = "N";
                Flagtblcutoff = "N";
            }

            ckeys.TRDays = ckeys.TRDays + ndays;
            ckeys.TRDays = ckeys.TRDays + OS.GetODADays(ContractID, ToCity, SYSConvert.ToDouble(ChargedWeight), SYSConvert.ToInt64(NoOfPkgs));

            // if (DocketRules.GetDocketRule("EDD_LOCAL").DefaultValue.IndexOf(TransMode) >= 0)
            if (MS.GetRuleObject("EDD_LOCAL").defaultvalue.IndexOf(TransMode) >= 0)
            {
                ckeys.TRDays = 0;
            }

            DateTime edd = DOCKDT;
            edd = edd.AddDays(ckeys.TRDays);
            EDD = edd.ToString("dd-MMM-yyyy");

            if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "PU")
            {
                DateTime edd_hday = OS.AddHolidaysToEDD(edd, Destination, true);
                if (edd_hday > edd)
                {

                    FlgConfirmEddDiv = "Y";
                    Confirmdesc = "Holiday comes on EDD. Do you want to add Holidays to EDD ?";
                    BtnConfirm = "Add Holidays to EDD";
                    BtnCancel = "Don't Add Holiday to EDD and apply Holiday Charges";
                    EDD = edd_hday.ToString("dd-MMM-yyyy");
                }
            }
            else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "AD")
            {
                DateTime edd_hday = OS.AddHolidaysToEDD(edd, Destination, true);
                if (edd_hday > edd)
                {
                    // hdnhdayedd.Value = edd_hday.ToString("dd/MM/yyyy");
                    EDD = edd_hday.ToString("dd-MMM-yyyy");
                }
            }
            else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "N")
            {
                if (edd.DayOfWeek.ToString().ToUpper().CompareTo("SUNDAY") == 0)
                    edd.AddDays(1);
                EDD = edd.ToString("dd-MMM-yyyy");
            }

            double minfreight = 0.00;
            string ContractMessage = "", FlgChrgWTDisabled = "N";
            if (ckeys.MinFreightType.CompareTo("B") == 0)
            {
                int cnt = 0;
                double pkgsno = 0, chargedweight = 0, ton = 0, freightrate = 0, freightcharge = 0;
                pkgsno = SYSConvert.ToDouble(NoOfPkgs);
                chargedweight = SYSConvert.ToDouble(ChargedWeright);
                ton = SYSConvert.ToDouble(ChargedWeright) / 1000;

                if (ckeys.InvoiceRateApplay.ToString() == "1" && ckeys.InvoiceRate > 0)
                {
                    ckeys.FreightCharge = SYSConvert.ToDouble(SYSConvert.ToDouble(ckeys.InvAmt) * SYSConvert.ToDouble(ckeys.InvoiceRate) / 100);
                    if (ckeys.RateType.CompareTo("F") == 0)
                    {
                        ckeys.FreightRate = ckeys.FreightCharge;
                    }
                    else if (ckeys.RateType.CompareTo("W") == 0)
                    {
                        ckeys.FreightRate = ckeys.FreightCharge / chargedweight;
                    }
                    else if (ckeys.RateType.CompareTo("P") == 0)
                    {
                        ckeys.FreightRate = ckeys.FreightCharge / pkgsno;
                    }
                    else if (ckeys.RateType.CompareTo("T") == 0)
                    {
                        ckeys.FreightRate = ckeys.FreightCharge / ton;
                    }
                    //freightrate = SYSConvert.ToDouble(ckeys.FreightRate);
                }
                else
                {
                    freightcharge = SYSConvert.ToDouble(ckeys.FreightCharge);
                    freightrate = SYSConvert.ToDouble(ckeys.FreightRate);
                }

                if (ckeys.MinFreightBase.CompareTo("F") == 0)
                {
                    //if (freightcharge > 0)
                    //{
                    if (freightcharge < SYSConvert.ToDouble(ckeys.MinFreightBaseRate))
                    {
                        cnt++;
                        freightcharge = SYSConvert.ToDouble(ckeys.MinFreightBaseRate);
                        ContractMessage = "Minimum Freight Base Wise Flat (in RS) Applied.";
                        ckeys.RateType = "F";
                    }
                    //}
                    //else
                    //{
                    //    cnt++;
                    //    freightcharge = 0;
                    //    ContractMessage = "Rate is not available in Contract for " + ckeys.OrgnLoc + " to " + ckeys.DelLoc + ".";
                    //    ckeys.RateType = "F";
                    //}
                }
                else if (ckeys.MinFreightBase.CompareTo("W") == 0)
                {
                    if (chargedweight < SYSConvert.ToDouble(ckeys.MinFreightBaseRate))
                    {
                        cnt++;
                        chargedweight = SYSConvert.ToDouble(ckeys.MinFreightBaseRate);
                        ckeys.ChargedWeight = chargedweight;
                        ContractMessage = "Minimum Freight Base Wise Min " + chargedweight.ToString("F0") + " KGs Applied.";
                        FlgChrgWTDisabled = "Y";
                    }
                }
                else if (ckeys.MinFreightBase.CompareTo("P") == 0)
                {
                    if (pkgsno < SYSConvert.ToDouble(ckeys.MinFreightBaseRate))
                    {
                        cnt++;
                        pkgsno = SYSConvert.ToDouble(ckeys.MinFreightBaseRate);
                        ckeys.NoOfPkgs = pkgsno;
                        ContractMessage = "Minimm Freight Base Wise Min " + pkgsno.ToString("F0") + " PKGs Applied.";
                    }
                }
                else if (ckeys.MinFreightBase.CompareTo("T") == 0)
                {
                    if (ton < SYSConvert.ToDouble(ckeys.MinFreightBaseRate))
                    {
                        cnt++;
                        ton = SYSConvert.ToDouble(ckeys.MinFreightBaseRate);
                        chargedweight = ton * 1000;
                        ckeys.ChargedWeight = chargedweight;
                        ContractMessage = "Minimm Freight Base Wise Min " + ton.ToString("F2") + " TONs Applied.";
                        FlgChrgWTDisabled = "Y";
                    }
                }

                if (cnt > 0)
                {
                    if (ckeys.RateType.CompareTo("F") == 0)
                    {
                        ckeys.FreightCharge = freightcharge;
                        ckeys.FreightRate = freightcharge;
                    }
                    else if (ckeys.RateType.CompareTo("W") == 0)
                    {
                        freightcharge = chargedweight * freightrate;
                        ckeys.ChargedWeight = chargedweight;
                        ckeys.FreightCharge = freightcharge;
                        ckeys.FreightRate = freightrate;
                    }
                    else if (ckeys.RateType.CompareTo("P") == 0)
                    {
                        freightcharge = pkgsno * freightrate;
                        ckeys.NoOfPkgs = pkgsno;
                        ckeys.FreightCharge = freightcharge;
                        ckeys.FreightRate = freightrate;
                    }
                    else if (ckeys.RateType.CompareTo("T") == 0)
                    {
                        freightcharge = ton * freightrate;
                        ckeys.ChargedWeight = chargedweight;
                        ckeys.FreightCharge = freightcharge;
                        ckeys.FreightRate = freightrate;
                    }
                }
            }
            else if (ckeys.MinFreightType.CompareTo("%") == 0)
            {
                minfreight = SYSConvert.ToDouble(ckeys.MinFreightRate);
                if (SYSConvert.ToDouble(ckeys.FreightCharge) < minfreight)
                {
                    ckeys.FreightCharge = minfreight;
                    if (ckeys.RateType.CompareTo("W") == 0)
                    {
                        ckeys.FreightRate = SYSConvert.ToDouble(Convert.ToString(SYSConvert.ToDouble(ckeys.FreightCharge) / SYSConvert.ToInt64(ckeys.ChargedWeight)));
                    }
                    else if (ckeys.RateType.CompareTo("P") == 0)
                    {
                        ckeys.FreightRate = SYSConvert.ToDouble(Convert.ToString(SYSConvert.ToDouble(ckeys.FreightCharge) * SYSConvert.ToInt64(ckeys.NoOfPkgs)));
                    }
                    else if (ckeys.RateType.CompareTo("T") == 0)
                    {
                        ckeys.FreightRate = SYSConvert.ToDouble(Convert.ToString(SYSConvert.ToDouble(ckeys.FreightCharge) / ((SYSConvert.ToInt64(ckeys.ChargedWeight) * 1000))));
                    }
                    else if (ckeys.RateType.CompareTo("F") == 0)
                    {
                        ckeys.FreightRate = ckeys.FreightCharge;
                    }
                    ContractMessage = "Minimm Freight Percent Wise applied. Minimum Amount " + minfreight.ToString("F2") + " RS Applied.";
                }
            }
            else
            {
                ContractMessage = "Minimum Freight Not Set Please complete Contract.................";
            }

            string strRateType = "";
            if (ckeys.RateType != null && ckeys.RateType != "0" && ckeys.RateType != "")
            {
                strRateType = MS.GetGeneralMasterObject().Where(c => c.CodeId == ckeys.RateType && c.CodeType.ToUpper() == "RATETYPE").FirstOrDefault().CodeDesc;
            }

            return new JsonResult()
            {
                Data = new
                {

                    FreightCharge = ckeys.FreightCharge.ToString("F2"),
                    FreightRate = ckeys.FreightRate.ToString("F2"),
                    RateType = ckeys.RateType,
                    TRDays = ckeys.TRDays,
                    ChargedWeight = ckeys.ChargedWeight,
                    NoOfPkgs = ckeys.NoOfPkgs,
                    strRateType = strRateType,
                    FlagCutOff = FlagCutOff,
                    Flagtblcutoff = Flagtblcutoff,
                    EDD = EDD,
                    FlgConfirmEddDiv = FlgConfirmEddDiv,
                    Confirmdesc = Confirmdesc,
                    BtnConfirm = BtnConfirm,
                    BtnCancel = BtnCancel,
                    FlgChrgWTDisabled = FlgChrgWTDisabled,
                    ContractMessage = ContractMessage,
                }
            };
        }

        public JsonResult GetFovContractDetailsDetails(string ChargeRule, string BaseCode1, string ContractID, string RiskType, string InvAmt)
        {

            FOVCharge fovchrg = new FOVCharge();
            fovchrg.ChargeRule = ChargeRule == "" ? "NONE" : ChargeRule;
            fovchrg.BaseCode = BaseCode1 == "" ? "NONE" : BaseCode1;
            fovchrg.ContractID = ContractID;
            fovchrg.RiskType = RiskType;
            fovchrg.DeclareValue = SYSConvert.ToInt64(InvAmt);
            fovchrg = OS.GetFOVCharge(fovchrg);

            if (fovchrg.FlagFOV.CompareTo("Y") == 0)
            {
                if (fovchrg.FOVRateType.CompareTo("%") == 0)
                    fovchrg.FOVRateType = "(% of Invoice)";
                else
                    fovchrg.FOVRateType = "(Flat In RS)";

                //txtfovrate.Text = fovchrg.FOVRate.ToString("F2");
                //txtfovcalculated.Text = fovchrg.FOVCharged.ToString("F2");
                //txtfovcharged.Text = fovchrg.FOVCharged.ToString("F2");
            }
            else
            {
                //trfov.Style["display"] = "none";

                //txtfovcharged.Text = "0.00";
                //txtfovcalculated.Text = "0.00";
                //txtfovrate.Text = "0.00";
                //lblfovratetype.Text = "Flat (in RS)";

                fovchrg.FOVRateType = "Flat (in RS)";
                fovchrg.FOVRate = 0;
                fovchrg.FOVCharged = 0;


            }


            return new JsonResult()
            {
                Data = new
                {

                    FOVRateType = fovchrg.FOVRateType,
                    FOVRate = fovchrg.FOVRate,
                    FOVCharged = fovchrg.FOVCharged,
                    //  TRDays = fovchrg.TRDays,
                }
            };

        }

        public JsonResult GetGCStatusDetails(string dockno)
        {
            string Flg = "N", MSG = "";
            Webx_Master_General WMG = OS.CheckDocketNo(dockno, BaseLocationCode, BaseUserName.ToUpper());
            //Webx_Master_General WMG = OS.CheckDocketNo(dockno, BaseLocationCode);
            Flg = WMG.CodeId;
            MSG = WMG.CodeDesc;
            return new JsonResult()
            {
                Data = new
                {
                    Flg = Flg,
                    MSG = MSG
                }
            };

        }

        public int ToInteger(string num)
        {
            if (num.ToString().CompareTo("") == 0 || num == null)
            {
                num = "0";
            }

            try
            {
                return Convert.ToInt16(num);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public ActionResult InvoiceDetails(int id)
        {

            WebX_Master_Docket_Invoice WMDI = new WebX_Master_Docket_Invoice();
            WMDI.SrNo = id;
            WMDI.INVDT = System.DateTime.Now;
            WMDI.DECLVAL = 0;
            WMDI.PKGSNO = 0;
            WMDI.ACTUWT = 0;

            WMDI.VOL_L = 0;
            WMDI.VOL_B = 0;
            WMDI.VOL_H = 0;
            return PartialView("_DocketInvoice", WMDI);
        }

        public JsonResult GetBookDateRule(string dbdaterule)
        {
            System.Globalization.CultureInfo enGB = new System.Globalization.CultureInfo("en-GB");

            string daterule = "N|";
            string[] arrtmp = new string[2];
            string[] arrdaterule = new string[7];
            DateTime dtcompare;
            arrtmp = dbdaterule.Split('|');
            arrdaterule = arrtmp[0].Split(',');
            DateTime dtfinstart = new DateTime(ToInteger(BaseFinYear.Substring(0, 3)), 4, 1);
            DateTime dtfinend = new DateTime(ToInteger(BaseFinYear.Substring(0, 3)) + 1, 3, 31);

            // [0]=Y MEANS RULE FOR DATE APPLIES
            if (arrdaterule[0].CompareTo("Y") == 0)
            {
                daterule = "Y|";
                if (arrdaterule[1].CompareTo("B_D") == 0)
                {
                    int days = ToInteger(arrdaterule[3]);
                    dtcompare = Convert.ToDateTime(OS.GetServerDate(), enGB);
                    dtcompare = dtcompare.AddDays(0 - days);
                    daterule = daterule + arrdaterule[1] + "," + dtcompare.ToString("dd/MM/yyyy");

                }
                else if (arrdaterule[1].CompareTo("B_T") == 0)
                {
                    dtcompare = Convert.ToDateTime(arrdaterule[5], enGB);
                    daterule = daterule + arrdaterule[1] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else
                {
                    daterule = "N|";
                }


                daterule = daterule + "|";
                // [2] MEANS WHICH RULE FOR ABOVE DATE
                if (arrdaterule[2].CompareTo("A_D") == 0)
                {
                    int days = ToInteger(arrdaterule[4]);
                    dtcompare = Convert.ToDateTime(OS.GetServerDate(), enGB);
                    dtcompare = dtcompare.AddDays(days);
                    daterule = daterule + arrdaterule[2] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else if (arrdaterule[2].CompareTo("A_T") == 0)
                {
                    dtcompare = Convert.ToDateTime(arrdaterule[6], enGB);
                    daterule = daterule + arrdaterule[2] + "," + dtcompare.ToString("dd/MM/yyyy");
                }
                else
                {
                    daterule = "N|";
                }

            }

            arrtmp = daterule.Split('|');
            string fromDate = "", todate = "";
            string[] startdtArr = new string[2];
            string[] EndDtArr = new string[2];

            DateTime FrmDate = System.DateTime.Now;
            DateTime ToDate = FrmDate;
            if (arrtmp[0].CompareTo("Y") == 0)
            {
                startdtArr = arrtmp[1].Split(',');
                EndDtArr = arrtmp[2].Split(',');

                fromDate = startdtArr[1];
                todate = EndDtArr[1];

                FrmDate = SYSConvert.ToDateTime(fromDate, "en-GB");
                ToDate = Convert.ToDateTime(SYSConvert.ToDateTime(todate, "en-GB").ToString("dd MMM yyyy") + " " + System.DateTime.Now.ToString("hh:mm:ss tt"));

            }




            return new JsonResult()
            {
                Data = new
                {
                    daterule = daterule,
                    fromDate = FrmDate.ToString(),
                    todate = ToDate.ToString()
                }
            };

        }

        public JsonResult GetStaxPaidBy(string id)
        {
            List<SelectListItem> litsitm = new List<SelectListItem>();

            if (id.IndexOf("CO") >= 0)
                litsitm.Add(new SelectListItem() { Text = "Consignor", Value = "CO" });

            if (id.IndexOf("CE") >= 0)
                litsitm.Add(new SelectListItem() { Text = "Consignee", Value = "CE" });

            if (id.IndexOf("P") >= 0)
                litsitm.Add(new SelectListItem() { Text = "Billing Party", Value = "P" });

            if (id.IndexOf("T") >= 0)
                litsitm.Add(new SelectListItem() { Text = "Transporter", Value = "T" });

            if (id == "0")
                litsitm.Add(new SelectListItem() { Text = "Consignor", Value = "CO" });


            return Json(litsitm, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPincodeFromLocation(string Location)
        {
            Location = Location.ToUpper();
            List<webx_pincode_master> List = new List<webx_pincode_master>();
            List = MS.GetPincodeMasterObject().Where(c => c.Handling_Location == Location && c.cityname != null && c.cityname != "" && c.StateCode != null && c.StateCode != "" && c.Handling_Location != "").ToList();

            var Listintg = (from e in List
                            select new
                            {
                                Value = e.pincode,
                                Text = e.PinArea,
                            }).Distinct().ToList();
            return Json(Listintg, JsonRequestBehavior.AllowGet);
        }

        #region Customer Details

        public ActionResult CustomerSubmit(string CUSTNM, string GRPCD, string CustAddress, string CUSTLOC, string MOBILENO, string GSTNO, bool Paid, bool TBB, bool ToPay, bool FOC, string State, string PANNo, string AADHARNO)
        {
            bool status = false;

            try
            {
                List<string> listCUSTCAT = new List<string>();
                if (Paid == true)
                    listCUSTCAT.Add("P01");
                if (TBB == true)
                    listCUSTCAT.Add("P02");
                if (ToPay == true)
                    listCUSTCAT.Add("P03");
                if (FOC == true)
                    listCUSTCAT.Add("P04");

                string CUSTCAT = string.Join(",", listCUSTCAT);

                DataTable dtCustDetails = OS.Customer_Details_Insert(CUSTNM, GRPCD, CustAddress, CUSTLOC, MOBILENO, GSTNO, CUSTCAT, BaseUserName, State, PANNo, AADHARNO);
                if (dtCustDetails.Rows.Count > 0 && dtCustDetails.Rows[0][1].ToString() == "1")
                {
                    status = true;
                }

                return Json(status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        #endregion

        #endregion

        #region Challan

        public ActionResult Index(int TYP)
        {
            string DeliveryAllowed = MS.GetLocationDetails().Where(c => c.LocCode == BaseLocationCode).FirstOrDefault().Op_Dly;
            if (TYP == 3 && (string.IsNullOrEmpty(DeliveryAllowed) || DeliveryAllowed.ToUpper() == "N"))
            {
                ViewBag.StrError = "You do not have permission to add DRS from current branch...!!!";
                return View("Error");
                //return RedirectToAction("URLRedirect", "Home");
            }
            GCFilter GC = new GCFilter();

            GC.TYP = TYP;

            if (TYP == 1)
                return RedirectToAction("Challan", new { TYP = TYP });
            else if (TYP == 2)
                GC.isBookedby = true;
            else if (TYP == 3)
                GC.isBookedby = false;

            return View(GC);
        }

        public ActionResult Challan(int TYP, GCFilter GC)
        {
            if (TYP == 1)
                ViewBag.ChallanType = ViewBag.THCCalledAs;
            else if (TYP == 2)
                ViewBag.ChallanType = "PRS";
            else if (TYP == 3)
                ViewBag.ChallanType = "DRS";

            GC.DOCTYP = ViewBag.ChallanType;
            ViewBag.THCType = TYP;
            ChallanViewModel CVM = new ChallanViewModel();
            List<ChallanCharges> ListCharges = new List<ChallanCharges>();
            CYGNUS_THC_Header CTH = new CYGNUS_THC_Header();
            CTH.THCType = TYP;
            CTH.THCBRCD = BaseLocationCode;
            CVM.CTH = CTH;

            ListCharges = OS.GetChargesDetails("GE");
            CVM.ListCharges = ListCharges;
            PRSDRSGC PRS = new PRSDRSGC();

            CYGNUS_THC_Financial_Details THcDetail = new CYGNUS_THC_Financial_Details();
            THcDetail.LoadedRateType = GC.ChargeType;
            THcDetail.LoadedBy = GC.LoadingBy;
            CVM.CTVD = new CYGNUS_THC_Vehicle_Details();

            if (TYP == 2)
            {
                CVM.DocketList = OS.GetDocketListfromPRSDRS(GC, BaseLocationCode);
                CVM.ListVendorType = OS.GetVendorType("B");
                PRS = CVM.DocketList.FirstOrDefault();
            }
            else if (TYP == 3)
            {
                CVM.DocketList = OS.GetDocketListfromPRSDRS(GC, BaseLocationCode);
                CVM.ListVendorType = OS.GetVendorType("D");
                PRS = CVM.DocketList.FirstOrDefault();
            }
            else
            {
                CVM.DocketList = new List<PRSDRSGC>();
                CVM.ListVendorType = new List<VendorType>();
            }

            if (GC.BookedByType == "P" || TYP == 1 || TYP == 3)
            {
                string THC_Vendor_Type_CodeList = ConfigurationManager.AppSettings["THC_Vendor_Type_CodeList"].ToString();
                string[] Vendor_Type_CodeList = THC_Vendor_Type_CodeList.Trim().Split(',').Select(n => n.Trim()).ToArray();
                CVM.ListVendorType = OS.GetVendorType("D").Where(c => Vendor_Type_CodeList.Contains(c.Vendor_Type_Code)).ToList();
            }
            if (TYP == 2)
            {
                CVM.ListVendorType = CVM.ListVendorType.Where(c => c.Vendor_Type_Code.Contains("XX")).ToList();
            }
            if (PRS != null)
            {
                THcDetail.Rate = PRS.Rate;
                THcDetail.MaxLimit = PRS.MaxLimit;
            }
            THcDetail.Is_Local_ODA_id = "Local";
            CVM.CTFD = THcDetail;
            CVM.BookedByType = GC.BookedByType;

            CVM.CPML = new CYGNUS_PDC_MaxLimit();

            return View(CVM);
        }

        [HttpPost]
        public ActionResult ChallanSubmit(ChallanViewModel CVM, List<MFDetails> MFList, List<ChallanCharges> THCCharge, List<PRSDRSGC> PRSDRSDocketList, IEnumerable<HttpPostedFileBase> files)
        {
            string docDateStr = CVM.CTH.THCDate.ToString("dd MMM yyyy");
            int TCCount = 0, TotalDkts = 0, TotalPkgs = 0, TotalPkgsArrv = 0, TotalPkgsPend = 0, TotalActWt = 0, TotalActWtArrv = 0, TotalChargeWt = 0;
            string RouteCode = "", RouteName = "", ToHubNext = "", ToHubLast = "", LateDeptReason = "", AirTrainName = "", FlightTrainName = "", AirWayBillRRNo = "", Airport = "", AIRSCH_TM = "";

            #region ODA Validation

            string Is_Local_ODA_id = "0";
            DataTable DT_Local_ODA = new DataTable();
            string checkedDockNo = ""; decimal chareges = 0;

            if (CVM.CTFD.Is_Local_ODA_id == "ODA")
            {
                foreach (var item in PRSDRSDocketList.Where(c => c.IsEnabled == true).ToList())
                {
                    if (item.IsEnabled == true)
                    {
                        if (checkedDockNo == "")
                        {
                            checkedDockNo = item.DOCKNO;
                        }
                        else
                        {
                            checkedDockNo = checkedDockNo + "," + item.DOCKNO;
                        }
                    }
                }

                DT_Local_ODA = OS.Get_ODA_Docket_Charges(checkedDockNo, Convert.ToInt32(CVM.CTH.THCType));
                chareges = System.Convert.ToDecimal(DT_Local_ODA.Rows[0]["chareges"].ToString());
                if (chareges == 0)
                {
                    ViewBag.StrError = "Can not generate ODA PRS/DRS because selected " + ViewBag.DKTCalledAS + " list does not have ODA Applied.";
                    return View("Error");
                }
                else
                {
                    Is_Local_ODA_id = "1";
                }
            }

            decimal PendPkgSum = 0;

            if ((CVM.CTH.THCType == 2 || CVM.CTH.THCType == 3) && BaseUserType == "0")
            {
                foreach (var item in PRSDRSDocketList.Where(c => c.IsEnabled == true).ToList())
                {
                    if (item.IsEnabled == true)
                    {
                        PendPkgSum = PendPkgSum + item.PendPkgQty;
                    }
                }
            }

            decimal ContractAmount = Convert.ToInt32(CVM.CTFD.ContractAmount);
            string VehicleType = CVM.CTVD.VehicleType;
            string DOCTYP = "";
            //decimal RatePerGram = 0, CalculatedContractAmount = 0;

            if (CVM.CTH.THCType == 1)
                DOCTYP = ViewBag.THCCalledAs;
            else if (CVM.CTH.THCType == 2)
                DOCTYP = "PRS";
            else if (CVM.CTH.THCType == 3)
                DOCTYP = "DRS";

            // COMMENTED : As Rate Type Changes are not Required right now.
            //if ((CVM.CTH.THCType == 2 || CVM.CTH.THCType == 3) && BaseUserType == "0")
            //{
            //    try
            //    {
            //        DataTable RateDT = OS.GetPDCRatePerGram(BaseLocationCode, VehicleType, BaseUserName, DOCTYP);
            //        RatePerGram = Convert.ToDecimal(RateDT.Rows[0]["RatePerGM"].ToString());
            //        int Counts = Convert.ToInt32(RateDT.Rows[0]["Counts"].ToString());
            //        CalculatedContractAmount = PendPkgSum * RatePerGram;

            //        if (Counts == 0)
            //        {
            //            ViewBag.StrError = "Please Enter Rate Per KG For Location :-" + BaseLocationCode + ".";
            //            return View("Error");
            //        }
            //        if (ContractAmount >= CalculatedContractAmount)
            //        {
            //            Is_Local_ODA_id = "1";
            //        }
            //        else
            //        {
            //            Is_Local_ODA_id = "0";
            //        }
            //    }
            //    catch (Exception)
            //    {
            //        ViewBag.StrError = "Please Enter Rate Per KG For Location :-" + BaseLocationCode + ".";
            //        return View("Error");
            //    }
            //}

            #endregion

            if (!CVM.CTFD.IsMonthly)
            {
                /*28 Jan 2017*/
                if (PRSDRSDocketList != null)
                {
                    if (CVM.CTH.THCType.ToString() == "2")
                    {
                        int mathadiCNT = PRSDRSDocketList.Where(c => c.ratetype == "4" && c.IsEnabled == true).Count();
                        int otherCNT = PRSDRSDocketList.Where(c => c.IsEnabled == true && (c.ratetype == "1" || c.ratetype == "3")).Count();
                        if (mathadiCNT != PRSDRSDocketList.Count())
                        {
                            if (otherCNT > 0)
                            {
                                /*28 Jan 2017*/
                                if ((CVM.CTFD.LoadedBy == "A" || CVM.CTFD.LoadedBy == "XX5" || CVM.CTFD.LoadedBy == "XX8" || CVM.CTFD.LoadedBy == "M") && CVM.CTFD.Loadingcharge == 0)
                                {
                                    ViewBag.StrError = "Loading Charge cannot be 0.";
                                    return View("Error");
                                }
                            }
                        }
                    }
                    if (CVM.CTH.THCType.ToString() == "3")
                    {
                        if ((CVM.CTFD.LoadedBy == "A" || CVM.CTFD.LoadedBy == "XX5" || CVM.CTFD.LoadedBy == "XX8" || CVM.CTFD.LoadedBy == "M") && CVM.CTFD.Loadingcharge == 0)
                        {
                            ViewBag.StrError = "Loading Charge cannot be 0.";
                            return View("Error");
                        }
                    }
                }
            }

            string ISMKTVeh = "N";
            if (CVM.CTH.VendorType == "XX")
            {
                CVM.CTH.VehicleNO = CVM.CTH.MKTVehicleNo;
                ISMKTVeh = "Y";
                CVM.CTH.VendorCode = "9888";
            }
            if (CVM.CTH.VehicleNO == "O")
            {
                CVM.CTH.VehicleNO = CVM.CTH.MKTVehicleNo;
            }
            CVM.CTVD.VehicleNO = CVM.CTH.VehicleNO;

            if (CVM.CTH.THCType.ToString() == "1")
            {
                string[] tmpString = CVM.CTH.RouteCode.Split(':');
                RouteCode = CVM.CTH.RouteCode.Split(':')[0].Trim();
                RouteName = CVM.CTH.RouteName.Trim();

                string[] tmpString2 = RouteName.Split('-');
                ToHubNext = tmpString2[1].Trim();
                ToHubLast = tmpString2[tmpString2.Length - 1].Trim();
                LateDeptReason = CVM.CTH.LateDepaturereason;
            }
            else
                CVM.CTH.RouteType = "S";

            try
            {
                if ((CVM.CTFD.LoadedBy == "A" || CVM.CTFD.LoadedBy == "XX5" || CVM.CTFD.LoadedBy == "XX8"))
                {
                    CVM.CTFD.Loadingcharge = CVM.CTFD.Loadingcharge;
                }
                else if (Convert.ToDecimal(CVM.CTFD.Loadingcharge) > Convert.ToDecimal(CVM.CTFD.MaxLimit))
                {
                    CVM.CTFD.Loadingcharge = CVM.CTFD.MaxLimit;
                }


                string ImageName = "";
                #region Stock Update Image Upload
                try
                {
                    if (Request.Files != null)
                    {
                        var File = Request.Files["UploadImage"];
                        if (File.ContentLength > 0)
                        {
                            ImageName = SaveImage(File, "0", "StockupdateImage");
                        }
                    }
                }
                catch (Exception)
                {
                }
                #endregion

                string HDR_DETAILS = "";
                HDR_DETAILS = "<root><DOCHDR>";
                HDR_DETAILS = HDR_DETAILS + "<MANDOCNO>" + CVM.CTH.ManualTHCNo + "</MANDOCNO>";
                HDR_DETAILS = HDR_DETAILS + "<DOCDT>" + CVM.CTH.THCDate.ToString("dd MMM yyyy hh:mm:ss") + "</DOCDT>";
                HDR_DETAILS = HDR_DETAILS + "<DOCDTTM>" + CVM.CTH.THCDate.ToString("hh:mm") + "</DOCDTTM>";
                HDR_DETAILS = HDR_DETAILS + "<ACTDEPTTM>" + CVM.CTH.ActualDeptDate.ToString("hh:mm") + "</ACTDEPTTM>";
                HDR_DETAILS = HDR_DETAILS + "<SCHDEPTTM>" + CVM.CTH.ScheduleTime + "</SCHDEPTTM>";
                HDR_DETAILS = HDR_DETAILS + "<ToHubLast>" + ToHubLast + "</ToHubLast>";
                HDR_DETAILS = HDR_DETAILS + "<ToHubNext>" + ToHubNext + "</ToHubNext>";
                HDR_DETAILS = HDR_DETAILS + "<Loader>" + CVM.CTH.EntryBy.ToUpper() + "</Loader>";
                HDR_DETAILS = HDR_DETAILS + "<Loadingcharge>" + CVM.CTFD.Loadingcharge + "</Loadingcharge>";
                HDR_DETAILS = HDR_DETAILS + "<LoadedRateType>" + CVM.CTFD.LoadedRateType + "</LoadedRateType>";
                HDR_DETAILS = HDR_DETAILS + "<LoadedBy>" + CVM.CTFD.LoadedBy + "</LoadedBy>";
                HDR_DETAILS = HDR_DETAILS + "<Loading_VendorCode>" + CVM.CTFD.VendorCode + "</Loading_VendorCode>";
                HDR_DETAILS = HDR_DETAILS + "<Loading_VendorName>" + CVM.CTFD.VendName + "</Loading_VendorName>";
                HDR_DETAILS = HDR_DETAILS + "<LoadingRate>" + CVM.CTFD.Rate + "</LoadingRate>";
                HDR_DETAILS = HDR_DETAILS + "<IsMathadi>" + CVM.CTFD.IsMathadi + "</IsMathadi>";
                HDR_DETAILS = HDR_DETAILS + "<MathadiSlipNo>" + CVM.CTFD.MathadiSlipNo + "</MathadiSlipNo>";
                HDR_DETAILS = HDR_DETAILS + "<MathadiDate>" + CVM.CTFD.MathadiDate + "</MathadiDate>";
                HDR_DETAILS = HDR_DETAILS + "<MathadiAmt>" + CVM.CTFD.MathadiAmt + "</MathadiAmt>";
                HDR_DETAILS = HDR_DETAILS + "<Is_Local_ODA>" + Is_Local_ODA_id + "</Is_Local_ODA>";
                HDR_DETAILS = HDR_DETAILS + "<RateType>" + CVM.RateType + "</RateType>";
                HDR_DETAILS = HDR_DETAILS + "<ImageName>" + ImageName + "</ImageName>";

                if (CVM.CTH.THCType.ToString() == "1")
                {
                    HDR_DETAILS = HDR_DETAILS + "<RouteMode>" + CVM.CTH.RouteType + "</RouteMode>";
                    HDR_DETAILS = HDR_DETAILS + "<RouteCode>" + CVM.CTH.RouteCode.Split(':')[0].Trim() + "</RouteCode>";
                    HDR_DETAILS = HDR_DETAILS + "<RouteName>" + CVM.CTH.RouteName.Split(':')[1].Trim() + "</RouteName>";
                    HDR_DETAILS = HDR_DETAILS + "<Seal>" + CVM.CTH.SealNo + "</Seal>";
                    HDR_DETAILS = HDR_DETAILS + "<OutRemark>" + CVM.CTH.THCRemarks + "</OutRemark>";
                    HDR_DETAILS = HDR_DETAILS + "<LateDeptReason></LateDeptReason>";
                    HDR_DETAILS = HDR_DETAILS + "<TSNO>" + CVM.CTH.TripSheetNo + "</TSNO>";
                    HDR_DETAILS = HDR_DETAILS + "<ROUTE_CAT>" + CVM.CTH.RouteCategory + "</ROUTE_CAT>";
                    HDR_DETAILS = HDR_DETAILS + "<EWayBillNo>" + CVM.CTH.EWayBillNo + "</EWayBillNo>";
                }
                else
                {
                    HDR_DETAILS = HDR_DETAILS + "<RouteMode></RouteMode>";
                    HDR_DETAILS = HDR_DETAILS + "<RouteCode></RouteCode>";
                    HDR_DETAILS = HDR_DETAILS + "<RouteName></RouteName>";
                    HDR_DETAILS = HDR_DETAILS + "<Seal></Seal>";
                    HDR_DETAILS = HDR_DETAILS + "<OutRemark></OutRemark>";
                    HDR_DETAILS = HDR_DETAILS + "<LateDeptReason>" + LateDeptReason + "</LateDeptReason>";
                    HDR_DETAILS = HDR_DETAILS + "<TSNO>" + CVM.CTH.TripSheetNo + "</TSNO>";
                    HDR_DETAILS = HDR_DETAILS + "<ROUTE_CAT></ROUTE_CAT>";
                    HDR_DETAILS = HDR_DETAILS + "<tabletNumber>" + CVM.CTVD.tabletNumber + "</tabletNumber>";
                    HDR_DETAILS = HDR_DETAILS + "<StaffName>" + CVM.CTVD.StaffName + "</StaffName>";
                    HDR_DETAILS = HDR_DETAILS + "<StaffMobileNo>" + CVM.CTVD.StaffMobileNo + "</StaffMobileNo>";
                    HDR_DETAILS = HDR_DETAILS + "<EWayBillNo></EWayBillNo>";
                }
                HDR_DETAILS = HDR_DETAILS + "<EWayBillExpiredDate>" + (CVM.CTH.EWayBillExpiredDate == System.DateTime.MinValue ? null : CVM.CTH.EWayBillExpiredDate) + "</EWayBillExpiredDate>";
                HDR_DETAILS = HDR_DETAILS + "<VendorCode>" + CVM.CTH.VendorCode + "</VendorCode>";
                HDR_DETAILS = HDR_DETAILS + "<VendorName>" + CVM.CTH.VendorName + "</VendorName>";
                HDR_DETAILS = HDR_DETAILS + "<Pan_No>" + CVM.CTH.LorryOwnerPanNo + "</Pan_No>";


                if (CVM.CTH.AirLine == null)
                    CVM.CTH.AirLine = "";

                if (CVM.CTH.TrainName == null)
                    CVM.CTH.TrainName = "";

                if (CVM.CTH.FlightCode == null)
                    CVM.CTH.FlightCode = "";

                if (CVM.CTH.TrainNo == null)
                    CVM.CTH.TrainNo = "";

                if (CVM.CTH.AirportCode == null)
                    CVM.CTH.AirportCode = "";

                if (CVM.CTH.FlightScheduleTime == null)
                    CVM.CTH.FlightScheduleTime = "00:00:00";

                if (CVM.CTH.AirWayBillNo == null)
                    CVM.CTH.AirWayBillNo = "";

                if (CVM.CTH.RRNo == null)
                    CVM.CTH.RRNo = "";

                AirTrainName = (CVM.CTH.RouteType == "A" ? CVM.CTH.AirLine : CVM.CTH.TrainName);
                FlightTrainName = (CVM.CTH.RouteType == "A" ? CVM.CTH.FlightCode : CVM.CTH.TrainNo);
                Airport = (CVM.CTH.RouteType == "A" ? CVM.CTH.AirportCode.ToString() : "");
                AIRSCH_TM = (CVM.CTH.RouteType == "A" ? CVM.CTH.FlightScheduleTime : "");

                string AWBNO = (CVM.CTH.RouteType == "A" ? CVM.CTH.AirWayBillNo : CVM.CTH.RRNo);

                if (CVM.CTH.RouteType != "S") //If Not Road 
                {
                    HDR_DETAILS = HDR_DETAILS + "<VehicleNo></VehicleNo>" +
                    "<VehicleType>" + CVM.CTVD.VehicleTypeSize + "</VehicleType>" +
                    "<VehicleTons>0</VehicleTons>" +
                    "<MarketVeh></MarketVeh>" +
                    "<MarketVehImage>0</MarketVehImage>" +
                    "<EngineNo></EngineNo>" +
                    "<ChasisNo></ChasisNo>" +
                    "<RCBookNo></RCBookNo>" +
                    "<VehicleRegDt></VehicleRegDt>" +
                    "<InsuranceDt></InsuranceDt>" +
                    "<FitnessCertDt></FitnessCertDt>" +
                    "<Driver1></Driver1>" +
                    "<Licence1></Licence1>" +
                    "<RTO1></RTO1>" +
                    "<ValidityDt1></ValidityDt1>" +
                    "<Driver1MOB></Driver1MOB>" +
                    "<Driver2></Driver2>" +
                    "<Licence2></Licence2>" +
                    "<RTO2></RTO2>" +
                    "<ValidityDt2></ValidityDt2>" +
                    "<Driver2MOB></Driver2MOB>" +
                    "<AirTrainName>" + AirTrainName + "</AirTrainName>" +
                    "<FlightTrainNo>" + FlightTrainName + "</FlightTrainNo>" +
                    "<AirWayBillRRNo>" + AirWayBillRRNo + "</AirWayBillRRNo>" +
                    "<Airport>" + Airport + "</Airport>" +
                    "<AIRSCH_TM>" + AIRSCH_TM + "</AIRSCH_TM>" +
                    "<AWBNO>" + AWBNO + "</AWBNO>" +
                    "<SPLRMOBNO></SPLRMOBNO>" +
                    "<SPLRNM></SPLRNM>" +
                    "<VHCAP></VHCAP>" +
                    "<WTLOADED></WTLOADED>" +
                    "<CAPUTI></CAPUTI>" +
                    "<OVLED_YN>N</OVLED_YN>" +
                    "<OVLED_REASON>-</OVLED_REASON>" +
                    "<FreeSpace>0</FreeSpace>";
                }
                else ////If Road
                {
                    HDR_DETAILS = HDR_DETAILS + "<VehicleNo>" + CVM.CTH.VehicleNO + "</VehicleNo>";
                    HDR_DETAILS = HDR_DETAILS + "<VehicleType>" + CVM.CTVD.VehicleType + "</VehicleType>";
                    HDR_DETAILS = HDR_DETAILS + "<FTLType>" + CVM.CTVD.FTLType + "</FTLType>";
                    HDR_DETAILS = HDR_DETAILS + "<VehicleTons>0</VehicleTons>";
                    HDR_DETAILS = HDR_DETAILS + "<MarketVeh>" + ISMKTVeh + "</MarketVeh>";
                    HDR_DETAILS = HDR_DETAILS + "<MarketVehImage>0</MarketVehImage>";

                    HDR_DETAILS = HDR_DETAILS + "<EngineNo>" + CVM.CTVD.ENGINENO + "</EngineNo>";
                    HDR_DETAILS = HDR_DETAILS + "<ChasisNo>" + CVM.CTVD.CHASISNO + "</ChasisNo>";
                    HDR_DETAILS = HDR_DETAILS + "<RCBookNo>" + CVM.CTVD.RCBOOKNO + "</RCBookNo>";

                    HDR_DETAILS = HDR_DETAILS + "<VehicleRegDt>" + CVM.CTVD.RegistrationDate + "</VehicleRegDt>";
                    HDR_DETAILS = HDR_DETAILS + "<VEHPRMDT>" + CVM.CTVD.PermitDate + "</VEHPRMDT>";
                    HDR_DETAILS = HDR_DETAILS + "<InsuranceDt>" + CVM.CTVD.InsuranceDate + "</InsuranceDt>";
                    HDR_DETAILS = HDR_DETAILS + "<FitnessCertDt>" + CVM.CTVD.FitnessDate + "</FitnessCertDt>";

                    HDR_DETAILS = HDR_DETAILS + "<Driver1>" + CVM.CTVD.Driver1Name + "</Driver1>";
                    HDR_DETAILS = HDR_DETAILS + "<Licence1>" + CVM.CTVD.Driver1Licence + "</Licence1>";
                    HDR_DETAILS = HDR_DETAILS + "<RTO1>" + CVM.CTVD.Driver1RTONo + "</RTO1>";
                    HDR_DETAILS = HDR_DETAILS + "<ValidityDt1>" + CVM.CTVD.Driver1LicenceValDate + "</ValidityDt1>";
                    HDR_DETAILS = HDR_DETAILS + "<Driver1MOB>" + CVM.CTVD.Driver1MobileNo + "</Driver1MOB>";

                    HDR_DETAILS = HDR_DETAILS + "<Driver2>" + CVM.CTVD.Driver2Name + "</Driver2>";
                    HDR_DETAILS = HDR_DETAILS + "<Licence2>" + CVM.CTVD.Driver2Licence + "</Licence2>";
                    HDR_DETAILS = HDR_DETAILS + "<RTO2>" + CVM.CTVD.Driver2RTONo + "</RTO2>";
                    HDR_DETAILS = HDR_DETAILS + "<ValidityDt2>" + CVM.CTVD.Driver2LicenceValDate + "</ValidityDt2>";
                    HDR_DETAILS = HDR_DETAILS + "<Driver2MOB>" + CVM.CTVD.Driver2MobileNo + "</Driver2MOB>";

                    HDR_DETAILS = HDR_DETAILS + "<AirTrainName></AirTrainName>";
                    HDR_DETAILS = HDR_DETAILS + "<FlightTrainNo></FlightTrainNo>";
                    HDR_DETAILS = HDR_DETAILS + "<AirWayBillRRNo></AirWayBillRRNo>";
                    HDR_DETAILS = HDR_DETAILS + "<SPLRMOBNO>" + CVM.CTH.SUPPLYERMOBNO + "</SPLRMOBNO>";
                    HDR_DETAILS = HDR_DETAILS + "<SPLRNM>" + CVM.CTH.SUPPLYERNAME + "</SPLRNM>";

                    string OVLED_YN = "N";
                    if (CVM.CTH.IsOverLoad)
                    {
                        OVLED_YN = "Y";
                    }

                    HDR_DETAILS = HDR_DETAILS + "<VEH_CAP>" + CVM.CTH.VehicleCapacity.ToString() + "</VEH_CAP>";
                    HDR_DETAILS = HDR_DETAILS + "<WTLOADED>" + CVM.CTH.WtLoaded.ToString() + "</WTLOADED>";
                    HDR_DETAILS = HDR_DETAILS + "<CAPUTI>" + CVM.CTH.VehicleCapacityUti + "</CAPUTI>";
                    HDR_DETAILS = HDR_DETAILS + "<OVLED_YN>" + OVLED_YN + "</OVLED_YN>";
                    HDR_DETAILS = HDR_DETAILS + "<OVLED_REASON>" + CVM.CTH.OverLoadReason + "</OVLED_REASON>";
                    HDR_DETAILS = HDR_DETAILS + "<FreeSpace>" + CVM.CTH.FreeSpace + "</FreeSpace>";
                }

                HDR_DETAILS = HDR_DETAILS + "<STKM>" + CVM.CTH.OpenKM + "</STKM>";
                HDR_DETAILS = HDR_DETAILS + "<ADVAMT>" + CVM.CTFD.AdvanceAmount + "</ADVAMT>";
                HDR_DETAILS = HDR_DETAILS + "<ADVLOC>" + CVM.CTFD.AdvanceLocation + "</ADVLOC>";
                HDR_DETAILS = HDR_DETAILS + "<BALLOC>" + CVM.CTFD.BalanceLocation + "</BALLOC>";
                HDR_DETAILS = HDR_DETAILS + "<CONTTYP>ADH</CONTTYP>";
                HDR_DETAILS = HDR_DETAILS + "<CONTAMT>" + CVM.CTFD.ContractAmount + "</CONTAMT>";
                HDR_DETAILS = HDR_DETAILS + "<ISAttechedVendor>" + CVM.ISAttechedVendor + "</ISAttechedVendor>";
                HDR_DETAILS = HDR_DETAILS + "<ISContractualVendor>" + CVM.ISContractualVendor + "</ISContractualVendor>";
                //HDR_DETAILS = HDR_DETAILS + "<WTADJ>" + txtWtAdj.Text + "</WTADJ>";
                //HDR_DETAILS = HDR_DETAILS + "<WTADJ_PM>" + DDLWTADJ.SelectedValue.ToString() + "</WTADJ_PM>";
                //HDR_DETAILS = HDR_DETAILS + "<TOTWT_ADJ>" + fintotwt.ToString() + "</TOTWT_ADJ>";

                HDR_DETAILS = HDR_DETAILS + "<WTADJ></WTADJ>";
                HDR_DETAILS = HDR_DETAILS + "<WTADJ_PM></WTADJ_PM>";
                HDR_DETAILS = HDR_DETAILS + "<TOTWT_ADJ></TOTWT_ADJ>";
                HDR_DETAILS = HDR_DETAILS + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_DKT></TOT_DKT>";
                HDR_DETAILS = HDR_DETAILS + "<TCCount></TCCount>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_LOAD_PKGS></TOT_LOAD_PKGS>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_ARRV_PKGS></TOT_ARRV_PKGS>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_ARRV_ACTWT></TOT_ARRV_ACTWT>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_PEND_PKGS></TOT_PEND_PKGS>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_CHARGE_WT></TOT_CHARGE_WT>";
                HDR_DETAILS = HDR_DETAILS + "<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>";
                HDR_DETAILS = HDR_DETAILS + "<BALAMTBRCD>" + BaseLocationCode + "</BALAMTBRCD>";

                string DET_DETAILS = "<root>";

                if (CVM.CTH.IsEmpty == false)
                {
                    if (CVM.CTH.THCType.ToString() == "1")
                    {
                        //// DET_DETAILS = "";
                        foreach (var itm in MFList.Where(c => c.IsEnabled == true).ToList())
                        {
                            DET_DETAILS = DET_DETAILS + "<TC>" +
                            "<TCNO>" + itm.TCNO + "</TCNO>" +
                            "<TOTAL_DOCKETS>" + itm.TOT_DKT + "</TOTAL_DOCKETS>" +
                            "<LOADPKGSNO>" + itm.TOT_LOAD_PKGS + "</LOADPKGSNO>" +
                            "<LOADACTUWT>" + itm.TOT_LOAD_ACTWT + "</LOADACTUWT>" +
                            "<LOADCFTWT>" + itm.TOT_LOAD_ACTWT + "</LOADCFTWT>" +
                            "<SourceHub>" + BaseLocationCode + "</SourceHub>" +
                            "<ToHubNext>" + ToHubNext + "</ToHubNext>" +
                           "<THCDT>" + docDateStr.Trim() + "</THCDT>" +
                            "<RouteCode>" + CVM.CTH.RouteCode + "</RouteCode>" +
                            "<RouteName>" + CVM.CTH.RouteName + "</RouteName>" +
                            "<VehicleNo>" + CVM.CTH.VehicleNO + "</VehicleNo>" +
                            "<ActualDeptDate>" + CVM.CTH.ActualDeptDate.ToString("dd MMM yyyy") + "</ActualDeptDate>" +
                            "<ActualDeptTime>" + CVM.CTH.ActualDeptDate.TimeOfDay.ToString() + "</ActualDeptTime>" +
                            "</TC>";
                            TCCount += 1;
                            TotalDkts += Convert.ToInt32(itm.TOT_DKT);
                            TotalPkgs += Convert.ToInt32(itm.TOT_LOAD_PKGS);
                            TotalActWt += Convert.ToInt32(itm.TOT_LOAD_ACTWT);
                        }
                    }
                    else
                    {
                        foreach (var itm in PRSDRSDocketList.Where(c => c.IsEnabled == true).ToList())
                        {
                            ////DOCKNO,DOCKSF,PKGSNO,ArrPkgQty,PendPkgQty,ACTUWT,ArrWeightQty,CHRGWT,Freight,SVCTAX,DKTTOT,PayBas,TRN_MOD,DockDT
                            DET_DETAILS = DET_DETAILS + "<DOCKETS>";
                            DET_DETAILS = DET_DETAILS + "<DOCKETNO>" + itm.DOCKNO + "</DOCKETNO>";
                            DET_DETAILS = DET_DETAILS + "<DOCKETSF>" + itm.DOCKSF + "</DOCKETSF>";
                            DET_DETAILS = DET_DETAILS + "<ORGNCD>" + itm.ORGNCD + "</ORGNCD>";
                            DET_DETAILS = DET_DETAILS + "<PKGSNO>" + itm.PKGSNO + "</PKGSNO>";
                            DET_DETAILS = DET_DETAILS + "<ARRVPKGSNO>" + itm.ArrPkgQty + "</ARRVPKGSNO>";
                            DET_DETAILS = DET_DETAILS + "<PENDPKGSNO>" + itm.PendPkgQty + "</PENDPKGSNO>";
                            DET_DETAILS = DET_DETAILS + "<PAYBAS>" + itm.PayBas + "</PAYBAS>";
                            DET_DETAILS = DET_DETAILS + "<ACTUALWT>" + itm.ACTUWT + "</ACTUALWT>";
                            DET_DETAILS = DET_DETAILS + "<ARRVACTUALWT>" + itm.ArrWeightQty + "</ARRVACTUALWT>";
                            DET_DETAILS = DET_DETAILS + "<CHRGWT>" + itm.CHRGWT + "</CHRGWT>";
                            DET_DETAILS = DET_DETAILS + "<TRN_MOD>" + itm.TRN_MOD + "</TRN_MOD>";
                            DET_DETAILS = DET_DETAILS + "<DOCKETDT>" + itm.Bkg_Date + "</DOCKETDT>";
                            DET_DETAILS = DET_DETAILS + "<DESTCD>" + itm.DEST_CD + "</DESTCD>";
                            DET_DETAILS = DET_DETAILS + "<DRSDT>" + docDateStr.Trim() + "</DRSDT>";
                            DET_DETAILS = DET_DETAILS + "<Rate>" + itm.NewRate + "</Rate>";
                            DET_DETAILS = DET_DETAILS + "<RateType>" + itm.ratetype + "</RateType>";
                            DET_DETAILS = DET_DETAILS + "<SCHG08>" + itm.SCHG08 + "</SCHG08>";
                            DET_DETAILS = DET_DETAILS + "</DOCKETS>";

                            TotalDkts += 1;
                            TotalPkgs += Convert.ToInt32(itm.PKGSNO);
                            TotalPkgsArrv += Convert.ToInt32(itm.ArrPkgQty);
                            TotalPkgsPend += itm.PendPkgQty;
                            TotalActWt += Convert.ToInt32(itm.ACTUWT);
                            TotalActWtArrv += Convert.ToInt32(itm.ArrWeightQty);
                            TotalChargeWt += Convert.ToInt32(itm.CHRGWT);
                            //TotalFreight += Convert.ToInt32(GRDDKT.DataKeys[index].Values[8]);
                            //TotalST += Convert.ToInt32(GRDDKT.DataKeys[index].Values[9]);
                            //TotalDKTTOT += Convert.ToInt32(GRDDKT.DataKeys[index].Values[10]);
                        }
                    }
                }

                if (CVM.CTH.THCType.ToString() == "1")
                {
                    if (CVM.CTH.IsCityEnabled)
                    {
                        HDR_DETAILS = HDR_DETAILS + "<FROMCITY>" + CVM.CTH.FROMCITY + "</FROMCITY>";
                        HDR_DETAILS = HDR_DETAILS + "<TOCITY>" + CVM.CTH.TOCITY + "</TOCITY>";
                    }
                    else
                    {
                        HDR_DETAILS = HDR_DETAILS + "<FROMCITY></FROMCITY>";
                        HDR_DETAILS = HDR_DETAILS + "<TOCITY></TOCITY>";
                    }
                    if (TCCount > 0)
                    {
                        HDR_DETAILS = HDR_DETAILS.Replace("<TCCount></TCCount>", "<TCCount>" + TCCount.ToString().Trim() + "</TCCount>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_DKT></TOT_DKT>", "<TOT_DKT>" + TotalDkts.ToString().Trim() + "</TOT_DKT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_PKGS></TOT_LOAD_PKGS>", "<TOT_LOAD_PKGS>" + TotalPkgs.ToString().Trim() + "</TOT_LOAD_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>", "<TOT_LOAD_ACTWT>" + TotalActWt.ToString().Trim() + "</TOT_LOAD_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_CFTWT></TOT_LOAD_CFTWT>", "<TOT_LOAD_CFTWT>" + TotalActWt.ToString().Trim() + "</TOT_LOAD_CFTWT>");
                    }
                    else
                    {
                        HDR_DETAILS = HDR_DETAILS.Replace("<TCCount></TCCount>", "<TCCount>0</TCCount>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_DKT></TOT_DKT>", "<TOT_DKT>0</TOT_DKT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_PKGS></TOT_LOAD_PKGS>", "<TOT_LOAD_PKGS>0</TOT_LOAD_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>", "<TOT_LOAD_ACTWT>0</TOT_LOAD_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_CFTWT></TOT_LOAD_CFTWT>", "<TOT_LOAD_CFTWT>0</TOT_LOAD_CFTWT>");
                    }
                }
                else
                {
                    if (TotalDkts > 0)
                    {
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_DKT></TOT_DKT>", "<TOT_DKT>" + TotalDkts.ToString().Trim() + "</TOT_DKT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_PKGS></TOT_LOAD_PKGS>", "<TOT_LOAD_PKGS>" + TotalPkgs.ToString().Trim() + "</TOT_LOAD_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_PEND_PKGS></TOT_PEND_PKGS>", "<TOT_PEND_PKGS>" + TotalPkgsPend.ToString().Trim() + "</TOT_PEND_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>", "<TOT_LOAD_ACTWT>" + TotalActWt.ToString().Trim() + "</TOT_LOAD_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_ARRV_PKGS></TOT_ARRV_PKGS>", "<TOT_ARRV_PKGS>" + TotalPkgsArrv.ToString().Trim() + "</TOT_ARRV_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_ARRV_ACTWT></TOT_ARRV_ACTWT>", "<TOT_ARRV_ACTWT>" + TotalActWtArrv.ToString().Trim() + "</TOT_ARRV_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_CHARGE_WT></TOT_CHARGE_WT>", "<TOT_CHARGE_WT>" + TotalChargeWt.ToString().Trim() + "</TOT_CHARGE_WT>");
                    }
                    else
                    {
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_DKT></TOT_DKT>", "<TOT_DKT>0</TOT_DKT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_PKGS></TOT_LOAD_PKGS>", "<TOT_LOAD_PKGS>0</TOT_LOAD_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_PEND_PKGS></TOT_PEND_PKGS>", "<TOT_PEND_PKGS>0</TOT_PEND_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_LOAD_ACTWT></TOT_LOAD_ACTWT>", "<TOT_LOAD_ACTWT>0</TOT_LOAD_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_ARRV_PKGS></TOT_ARRV_PKGS>", "<TOT_ARRV_PKGS>0</TOT_ARRV_PKGS>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_ARRV_ACTWT></TOT_ARRV_ACTWT>", "<TOT_ARRV_ACTWT>0</TOT_ARRV_ACTWT>");
                        HDR_DETAILS = HDR_DETAILS.Replace("<TOT_CHARGE_WT></TOT_CHARGE_WT>", "<TOT_CHARGE_WT>0</TOT_CHARGE_WT>");
                    }

                    if (CVM.CTH.THCType.ToString() == "2")
                    {
                        HDR_DETAILS = HDR_DETAILS + "<ENDKM>" + CVM.CTH.CloseKM + "</ENDKM>";
                        // HDR_DETAILS = HDR_DETAILS + "<BASTAFF>" + Request.QueryString["STATUS"].ToString() + "</BASTAFF>";
                        //HDR_DETAILS = HDR_DETAILS + "<DLZONE>" + Request.QueryString["STATUS"].ToString() + "</DLZONE>";
                    }
                    else
                    {
                        HDR_DETAILS = HDR_DETAILS + "<BASTAFF>" + CVM.CTH.VendorType + "</BASTAFF>";
                        HDR_DETAILS = HDR_DETAILS + "<DLZONE>" + CVM.CTH.DeliveryZone + "</DLZONE>";
                    }
                }
                string IsBCProcess = "N";
                //  IsBCProcess = (chkBCProcess.Checked) ? "Y" : "N";
                if (CVM.CTH.THCType.ToString() == "1")
                    HDR_DETAILS = HDR_DETAILS + "<IsBCProcess>" + IsBCProcess + "</IsBCProcess>";
                else
                    HDR_DETAILS = HDR_DETAILS + "<IsBCProcess>N</IsBCProcess>";

                HDR_DETAILS = HDR_DETAILS + "<Company_Code>" + BaseCompanyCode + "</Company_Code>";
                HDR_DETAILS = HDR_DETAILS.Replace("<VEH_CAP></VEH_CAP>", "<VEH_CAP>0</VEH_CAP>");
                HDR_DETAILS = HDR_DETAILS.Replace("<WTLOADED></WTLOADED>", "<WTLOADED>0</WTLOADED>");
                HDR_DETAILS = HDR_DETAILS.Replace("<CAPUTI></CAPUTI>", "<CAPUTI>0</CAPUTI>");
                HDR_DETAILS = HDR_DETAILS.Replace("<WTADJ></WTADJ>", "<WTADJ>0</WTADJ>");
                HDR_DETAILS = HDR_DETAILS.Replace("<CONTAMT></CONTAMT>", "<CONTAMT>0</CONTAMT>");
                HDR_DETAILS = HDR_DETAILS.Replace("<ADVAMT></ADVAMT>", "<ADVAMT>0</ADVAMT>");
                HDR_DETAILS = HDR_DETAILS.Replace("<STKM></STKM>", "<STKM>01</STKM>");
                HDR_DETAILS = HDR_DETAILS.Replace("<TOTWT_ADJ></TOTWT_ADJ>", "<TOTWT_ADJ>0</TOTWT_ADJ>");
                HDR_DETAILS = HDR_DETAILS.Replace("<FreeSpace></FreeSpace>", "<FreeSpace>0</FreeSpace>");

                string findetval = "";
                //  string[] columnnm_STR = hidChargeCodeAll.Value.ToString().Split(',');

                int cnt = 1;
                if (CVM.CTH.THCType != 3)
                {
                    foreach (var item in THCCharge)
                    {
                        if (item.chargecode != null)
                        {
                            if (item.ChargeAmount.ToString() == "")
                                item.ChargeAmount = 0;

                            if (findetval == "")
                                findetval = item.chargecode.ToString() + "=" + item.ChargeAmount.ToString();
                            else
                                findetval = findetval + "," + item.chargecode.ToString() + "=" + item.ChargeAmount.ToString();
                            cnt++;
                        }
                    }
                }
                DET_DETAILS = DET_DETAILS + "</root>";
                HDR_DETAILS = HDR_DETAILS + "</DOCHDR></root>";


                if (CVM.CTH.THCType == 1)
                    DOCTYP = ViewBag.THCCalledAs;
                else if (CVM.CTH.THCType == 2)
                    DOCTYP = "PRS";
                else if (CVM.CTH.THCType == 3)
                    DOCTYP = "DRS";

                DataTable DT = OS.ChallanEntry(HDR_DETAILS, DET_DETAILS, findetval, BaseLocationCode, BaseFinYear, DOCTYP);

                string DOCNO = "", sDOCTYP = "", TranXaction = "";
                bool IsError = false;

                DOCNO = DT.Rows[0]["DOCNO"].ToString();
                sDOCTYP = DT.Rows[0]["DOCTYP"].ToString();
                TranXaction = DT.Rows[0]["TranXaction"].ToString().Trim();
                IsError = Convert.ToBoolean(DT.Rows[0]["IsError"]);
                try
                {
                    //string DOCType = CVM.CTH.THCType.ToString() == "2" ? "P" : "D";
                    //string SQRY = "exec USP_InsertDistanceIn_Table '" + CVM.CTH.FromAddress + "','" + CVM.CTH.ToAddress + "','" + CVM.CTH.distanceInKM + "','" + CVM.CTH.approxAPITime + "','" + BaseUserName.ToUpper() + "','0','1','" + BaseLocationCode.ToUpper() + "' ,'" + CVM.CTH.FromAddLat.ToUpper() + "' ,'" + CVM.CTH.FromAddLng.ToUpper() + "' ,'" + CVM.CTH.ToAddLat.ToUpper() + "' ,'" + CVM.CTH.ToAddLng.ToUpper() + "' ,'" + DOCType + "','" + DOCNO + "'";
                    //DataTable Dt = GF.GetDataTableFromSP(SQRY);
                }
                catch (Exception) { }
                if (IsError)
                {
                    ViewBag.StrError = "Error ";
                    return View("Error");
                }

                return RedirectToAction("ChallanDone", new { DOCNO = DOCNO, DOCTYP = sDOCTYP, TranXaction = TranXaction, IsError = IsError });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message;
                return View("Error");
            }
        }

        public ActionResult ChallanDone(string DOCNO, string DOCTYP, string TranXaction)
        {

            ViewBag.DOCNO = DOCNO;
            ViewBag.DOCTYP = DOCTYP;
            ViewBag.TranXaction = TranXaction;

            return View();

        }
        public ActionResult GetMFList(string Route, string IsBCProcess)
        {

            List<MFDetails> itmList = OS.GeMFListFromRoute(Route, IsBCProcess);

            return PartialView("_MFListForTHC", itmList);
        }

        public ActionResult Get_RouteSchedule(RouteSchedule RouteSchedule)
        {

            return PartialView("_RouteSchedule", RouteSchedule);
        }

        public JsonResult GetVendorsFromVendorType(string Id, string documentType)
        {
            List<Vendors> listVendors = new List<Vendors>();
            listVendors = OS.GetVendorsFromVendorType(Id, BaseLocationCode, BaseUserName, documentType);
            listVendors = listVendors.Where(c => c.Vendor_Code != "").ToList();
            var VendorList = (from e in listVendors
                              select new
                              {
                                  id = e.Vendor_Code,
                                  text = e.Vendor_Name,//.Replace(": " + e.Vendor_Code, ""),
                              }).Distinct().ToList();
            return Json(VendorList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVendorsForChallanFromRouteVendType(string RouteMode, string RouteName, string VendType, string THCType)
        {
            List<Vendors> listVendors = new List<Vendors>();
            listVendors = OS.GetVendorsForChallanFromRouteVendType(RouteMode, RouteName, VendType, BaseLocationCode, THCType);
            listVendors = listVendors.Where(c => c.Vendor_Code != "").ToList();
            var VendorList = (from e in listVendors
                              select new
                              {
                                  id = e.Vendor_Code,
                                  text = e.Vendor_Name,//.Replace(": " + e.Vendor_Code, ""),
                              }).Distinct().ToList();
            return Json(VendorList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleFromVendor(string VendorType, string Vendor)
        {
            List<webx_VEHICLE_HDR> listVehicle = new List<webx_VEHICLE_HDR>();
            listVehicle = OS.GetVehicleFromVendor(VendorType, Vendor);
            listVehicle = listVehicle.Where(c => c.VEHNO != "").ToList();
            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.VEHNO,
                                   Text = e.VEHNO == "O" ? "---Other---" : e.VEHNO,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPanNoFromVendor(string Vendor)
        {
            string QueryString = "SELECT PAN_NO FROM webx_VENDOR_HDR WHERE vendorcode='" + Vendor + "'";
            DataTable DT = GF.GetDataTableFromSP(QueryString);
            string PANNO = "";
            if (DT.Rows.Count > 0)
            {
                PANNO = DT.Rows[0]["PAN_NO"].ToString();
            }
            return new JsonResult()
            {
                Data = new
                {
                    PANNO = PANNO.ToUpper()
                }
            };
        }

        public JsonResult GetVendorType(string Id, string Type)
        {
            List<VendorType> listVendors = new List<VendorType>();
            listVendors = OS.GetVendorType(Id);
            if (Type == "THC")
            {
                string THC_Vendor_Type_CodeList = ConfigurationManager.AppSettings["THC_Vendor_Type_CodeList"].ToString();
                string[] Vendor_Type_CodeList = THC_Vendor_Type_CodeList.Trim().Split(',').Select(n => n.Trim()).ToArray();
                listVendors = listVendors.Where(c => Vendor_Type_CodeList.Contains(c.Vendor_Type_Code)).ToList();
            }

            var VendorList = (from e in listVendors
                              select new
                              {
                                  Value = e.Vendor_Type_Code,
                                  Text = e.Vendor_Type
                              }).Distinct().ToList();
            return Json(VendorList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoutesFromRouteType(string RouteType, string IsEmpty)
        {
            List<Routes> listVendors = new List<Routes>();
            listVendors = OS.GetRouteFromRouteType(RouteType, BaseLocationCode, IsEmpty);

            var VendorList = (from e in listVendors
                              select new
                              {
                                  Value = e.MyRouteName,
                                  Text = e.MyRouteName
                              }).Distinct().ToList();
            return Json(VendorList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTripsheetFromVehicle(string VehicleNo)
        {
            List<Tripsheet> listTripsheet = new List<Tripsheet>();
            listTripsheet = OS.GetTripsheetFromVehicle(VehicleNo).Where(c => c.CodeSlipNo != "").ToList();

            var TripsheetList = (from e in listTripsheet
                                 select new
                                 {
                                     Value = e.VSlipNo,
                                     Text = e.VSlipNo
                                 }).Distinct().ToList();
            return Json(TripsheetList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleTypeFromVehicle(string VehicleNo)
        {
            List<VehicleType> listTripsheet = new List<VehicleType>();
            listTripsheet = OS.GetVehicleTypeFromVehicle(VehicleNo).Where(c => c.TypeCode != "").ToList();

            var TripsheetList = (from e in listTripsheet
                                 select new
                                 {
                                     Value = e.TypeCode,
                                     Text = e.Type_Name
                                 }).Distinct().ToList();
            return Json(TripsheetList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleTypesForChallanFromRouteVendType(string VehicleNo, string RouteMode, string RouteName, string vendor_type, string VendorCode, string THCType)
        {
            List<VehicleType> listTripsheet = new List<VehicleType>();
            listTripsheet = OS.GetVehicleTypesForChallanFromRouteVendType(VehicleNo, RouteMode, RouteName, vendor_type, VendorCode, BaseLocationCode, THCType);

            var TripsheetList = (from e in listTripsheet
                                 select new
                                 {
                                     Value = e.TypeCode,
                                     Text = e.Type_Name
                                 }).Distinct().ToList();
            return Json(TripsheetList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBranchWiseTablet(string type, string assetCode)
        {
            List<AssignAssetBranchWise> tabletList = new List<AssignAssetBranchWise>();
            tabletList = OS.GetBranchWiseTablet(GF.FormateDate(System.DateTime.Now), BaseLocationCode, "1", "FA00076", "");

            var listTablet = (from e in tabletList
                              select new
                              {
                                  Value = e.RCPLSrNo,
                                  Text = e.RCPLSrNo
                              }).Distinct().ToList();
            return Json(listTablet, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFTLTypeFromVehicle(string VehicleNo)
        {
            List<FTLType> listTripsheet = new List<FTLType>();
            listTripsheet = OS.GetFTLTypeFromVehicle(VehicleNo).Where(c => c.codeid != "").ToList();

            var TripsheetList = (from e in listTripsheet
                                 select new
                                 {
                                     Value = e.codeid,
                                     Text = e.codedesc
                                 }).Distinct().ToList();
            return Json(TripsheetList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVehicleDetails(string VehicleNo)
        {
            List<vw_Vehicles> listitm = new List<vw_Vehicles>();
            listitm = OS.GetVehicleDetails(VehicleNo).ToList();

            List<vw_Vehicles> listitm1 = new List<vw_Vehicles>();
            listitm1 = OS.GetVehicleStartKM(VehicleNo).ToList();

            return new JsonResult()
            {
                Data = new
                {
                    VEHNO = listitm.FirstOrDefault().VEHNO,
                    VendorCode = listitm.FirstOrDefault().VendorCode,
                    ChasisNo = listitm.FirstOrDefault().ChasisNo,
                    EngineNo = listitm.FirstOrDefault().EngineNo,
                    RCBookNo = listitm.FirstOrDefault().RCBookNo,
                    RegistrationDt = listitm.FirstOrDefault().RegistrationDt,
                    InsuranceValDt = listitm.FirstOrDefault().InsuranceValDt,
                    FitnessValDt = listitm.FirstOrDefault().FitnessValDt,
                    VEHPRMDT = listitm.FirstOrDefault().VEHPRMDT,
                    Vehicle_Type = listitm.FirstOrDefault().Vehicle_Type,
                    Type_Name = listitm.FirstOrDefault().Type_Name,
                    Capacity = listitm.FirstOrDefault().Capacity,
                    Rate_Per_KM = listitm.FirstOrDefault().Rate_Per_KM,
                    Fuel_Type = listitm.FirstOrDefault().Fuel_Type,
                    usedcapacity = listitm.FirstOrDefault().usedcapacity,
                    Payload = listitm.FirstOrDefault().Payload,
                    Vehicle_Active = listitm.FirstOrDefault().Vehicle_Active,
                    Vehicle_Type_Active = listitm.FirstOrDefault().Vehicle_Type_Active,
                    FTLTYPe = listitm.FirstOrDefault().FTLTYPe,
                    FTLTYPE_NAME = listitm.FirstOrDefault().FTLTYPE_NAME,
                    StartKM = listitm1.FirstOrDefault().StartKM,
                }
            };
        }

        public JsonResult GetDriverDetailsFromTripsheet(string TripsheetNo)
        {
            List<Drivers> listitm = new List<Drivers>();
            listitm = OS.GetDriverDetailsFromTripsheet(TripsheetNo).ToList();


            return new JsonResult()
            {
                Data = new
                {
                    VSlipNo = listitm.FirstOrDefault().VSlipNo,
                    Driver1 = listitm.FirstOrDefault().Driver1,
                    Licno1 = listitm.FirstOrDefault().Licno1,
                    Validity_Date1 = listitm.FirstOrDefault().Validity_Date1,
                    RTO1 = listitm.FirstOrDefault().RTO1,
                    MOB1 = listitm.FirstOrDefault().MOB1,
                    Driver2 = listitm.FirstOrDefault().Driver2,
                    Licno2 = listitm.FirstOrDefault().Licno2,
                    Validity_Date2 = listitm.FirstOrDefault().Validity_Date2,
                    RTO2 = listitm.FirstOrDefault().RTO2,
                    MOB2 = listitm.FirstOrDefault().MOB2,
                }
            };
        }

        public JsonResult GetBookcedBy(string id)
        {
            List<BookedBy> litsitm = new List<BookedBy>();
            litsitm = OS.GetBookcedBy(id, BaseLocationCode, BaseUserName).Where(c => c.AllotToCode != "").ToList();

            var ItemList = (from e in litsitm
                            select new
                            {
                                Value = e.AllotToCode,
                                Text = e.AllotToName
                            }).Distinct().ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetContractAmount(string THCTYPE, decimal TotalWeight, decimal WeightAdjust, bool IsAllowAdhoc, bool IsAdhoc, bool IsAllowTAM, string VendorCode, string RouteCode, string RouteMODE, string FTL_Type, string Vehicle, string From_City, string To_City)
        {

            if (THCTYPE == "1")
                THCTYPE = "THC";
            else if (THCTYPE == "2")
                THCTYPE = "PRS";
            else if (THCTYPE == "3")
                THCTYPE = "DRS";

            string ContractID = "", Matrix = "";
            bool hasContract = false, IsContractAmtEnabled = false;
            decimal ContractAmount = 0, StandardContractAmount = 0;

            if (IsAllowAdhoc && IsAdhoc)
            {
                //HidContTyp.Value = "ADH";
                //txtContamt.Text = "0.00";
                //txtContamt.Enabled = true;
                //string hh = "";
            }
            else if (IsAllowTAM)
            {
                //double ContractAMT = GetTAMContactAmt(txtTAMNo.Text.Trim());
                //HidContTyp.Value = "TAM";
                //txtContamt.Text = ContractAMT.ToString("0.00");
                //txtContamt.Enabled = false;
                //string hh = "";
            }
            else
            {
                DataSet dsCNT = OS.GetContractDetails(THCTYPE, VendorCode, RouteCode, RouteMODE, FTL_Type, Vehicle, From_City, To_City);
                double dCntAMT = 0.00;
                double totwt = 0.00;

                totwt = Convert.ToDouble(TotalWeight) + Convert.ToDouble(WeightAdjust);

                if (dsCNT.Tables.Count > 0)
                {
                    DataTable dtCNT = dsCNT.Tables[0];
                    if (dtCNT.Rows.Count > 0)
                    {
                        DataRow drCNT = dtCNT.Rows[0];
                        hasContract = true;
                        ContractID = drCNT["CONTRACTCD"].ToString();
                        Matrix = drCNT["MatrixType"].ToString().Trim();

                        #region Route Based
                        if ((Matrix == "01" || Matrix == "02" || Matrix == "03") && THCTYPE == "THC")
                        {
                            double dRate = 0.00, dMinChg = 0.00, dMaxChg = 0.00;

                            if (drCNT["Chg_Rate"] != DBNull.Value)
                                dRate = Convert.ToDouble(drCNT["Chg_Rate"]);
                            if (drCNT["Min_Charge"] != DBNull.Value)
                                dMinChg = Convert.ToDouble(drCNT["Min_Charge"]);
                            if (drCNT["Max_Charge"] != DBNull.Value)
                                dMaxChg = Convert.ToDouble(drCNT["Max_Charge"]);

                            if (totwt > 0 && dRate > 0)
                                dCntAMT = dRate * totwt;

                            //if (dCntAMT > 0)
                            //{
                            if (dCntAMT > dMaxChg)
                                dCntAMT = dMaxChg;
                            else if (dCntAMT < dMinChg)
                                dCntAMT = dMinChg;
                            //}
                            ContractAmount = Convert.ToDecimal(dCntAMT.ToString("0.00"));
                            //if (dCntAMT > 0)
                            IsContractAmtEnabled = false;

                            StandardContractAmount = OS.GetStandardContract(RouteCode);
                        }
                        #endregion

                        #region Distance Based
                        if (
                            ((Matrix == "01" || Matrix == "02" || Matrix == "03" || Matrix == "09" || Matrix == "10" || Matrix == "11") && (THCTYPE == "PRS" || THCTYPE == "DRS")) ||
                            ((Matrix == "04" || Matrix == "05" || Matrix == "06") && (THCTYPE == "PRS" || THCTYPE == "DRS" || THCTYPE == "THC"))
                          )
                        {
                            double Min_Amt_Committed = 0.00, Committed_Km = 0.00, Chg_Per_Add_Km = 0.00, Max_Amt_Committed = 0.00;
                            int Trips_PM = 0;

                            if (drCNT["Min_Amt_Committed"] != DBNull.Value)
                                Min_Amt_Committed = Convert.ToDouble(drCNT["Min_Amt_Committed"]);
                            if (drCNT["Committed_Km"] != DBNull.Value)
                                Committed_Km = Convert.ToDouble(drCNT["Committed_Km"]);
                            if (drCNT["Chg_Per_Add_Km"] != DBNull.Value)
                                Chg_Per_Add_Km = Convert.ToDouble(drCNT["Chg_Per_Add_Km"]);
                            if (drCNT["Max_Amt_Committed"] != DBNull.Value)
                                Max_Amt_Committed = Convert.ToDouble(drCNT["Max_Amt_Committed"]);
                            if (drCNT["Trips_PM"] != DBNull.Value)
                                Trips_PM = Convert.ToInt32(drCNT["Trips_PM"]);

                            ContractAmount = 0;
                            //if (dCntAMT > 0)
                            IsContractAmtEnabled = false;

                            StandardContractAmount = 0;
                        }
                        #endregion

                        #region City Based
                        if ((Matrix == "09" || Matrix == "10" || Matrix == "11") && THCTYPE == "THC")
                        {
                            double dRate = 0.00, dMinChg = 0.00, dMaxChg = 0.00;

                            if (drCNT["Chg_Rate"] != DBNull.Value)
                                dRate = Convert.ToDouble(drCNT["Chg_Rate"]);
                            if (drCNT["Min_Charge"] != DBNull.Value)
                                dMinChg = Convert.ToDouble(drCNT["Min_Charge"]);
                            if (drCNT["Max_Charge"] != DBNull.Value)
                                dMaxChg = Convert.ToDouble(drCNT["Max_Charge"]);

                            if (totwt > 0 && dRate > 0)
                                dCntAMT = dRate * totwt;

                            //if (dCntAMT > 0)
                            //{
                            if (dCntAMT > dMaxChg)
                                dCntAMT = dMaxChg;
                            else if (dCntAMT < dMinChg)
                                dCntAMT = dMinChg;
                            //}

                            ContractAmount = 0;
                            //if (dCntAMT > 0)
                            IsContractAmtEnabled = false;

                            StandardContractAmount = 0;
                        }
                        // txtAdvamt.Focus();
                        #endregion
                    }
                }
            }

            return new JsonResult()
            {
                Data = new
                {
                    ContractAmount = ContractAmount,
                    StandardContractAmount = StandardContractAmount,
                    IsContractAmtEnabled = IsContractAmtEnabled,
                    ContractID = ContractID,
                }
            };
        }

        public JsonResult GetRouteSchedule(string RouteCode, DateTime DatDay, string DatTime)
        {

            string strDatDay = DatDay.ToString("dd MMM yyyy");

            List<RouteSchedule> litsitm = new List<RouteSchedule>();
            litsitm = OS.GetRouteScheduleDetails(RouteCode, strDatDay, DatTime).ToList();

            var ItemList = (from e in litsitm
                            select new
                            {
                                srno = e.srno,
                                rutcd = e.rutcd,
                                Schdep_HR = e.Schdep_HR,
                                Schdep_min = e.Schdep_min,
                                Is_Scheduled_rut = e.Is_Scheduled_rut,
                                Schno = e.Schno,
                                Schtype = e.Schtype,
                                Day_name = e.Day_name,
                                SchDT = e.SchDT,
                                SchTime = e.SchTime

                            }).Distinct().OrderBy(c => c.srno).ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetODADocketDetails(string Dockno, int Type)
        {
            DataTable DT = new DataTable();

            DT = OS.Get_ODA_Docket_Charges(Dockno, Type);

            decimal chareges = System.Convert.ToDecimal(DT.Rows[0]["chareges"].ToString());

            return new JsonResult()
            {
                Data = new
                {
                    chareges = chareges
                }
            };
        }

        public JsonResult GetAirport()
        {
            List<Webx_Master_General> litsitm = new List<Webx_Master_General>();
            litsitm = OS.GetAirport(BaseLocationCode).ToList();

            var ItemList = (from e in litsitm
                            select new
                            {
                                Text = e.CodeDesc,
                                Value = e.CodeId
                            }).Distinct().ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFlights(string AirLine, string Airport)
        {
            List<Webx_Master_General> litsitm = new List<Webx_Master_General>();
            litsitm = OS.GetFlights(AirLine, Airport).ToList();

            var ItemList = (from e in litsitm
                            select new
                            {
                                Text = e.CodeDesc,
                                Value = e.CodeId
                            }).Distinct().ToList();
            return Json(ItemList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFlightSchTime(string Flight, string Airport)
        {
            return new JsonResult()
            {
                Data = new
                {
                    SchTime = OS.GetFlightSchTime(Flight, Airport).ToString(),

                }
            };
        }

        #endregion

        #region Common Functions

        public JsonResult GetLocationList()
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag.ToUpper() == "Y").ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationCodeNameList()
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag.ToUpper() == "Y").ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocCode + " : " + e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationSeachList(string searchTerm)
        {
            List<webx_location> ListLocations = new List<webx_location>();
            searchTerm = searchTerm.ToUpper();
            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag.ToUpper() == "Y" && (c.LocCode.ToUpper().Contains(searchTerm) || c.LocName.ToUpper().Contains(searchTerm))).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGeneralMasterDetails(string id)
        {

            id = id.ToUpper();

            List<Webx_Master_General> listVehicle = new List<Webx_Master_General>();
            listVehicle = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == id && c.StatusCode.ToUpper() == "Y").ToList();

            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.CodeId,
                                   Text = e.CodeDesc,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetGeneralMasterDetailsOnlyDocket(string id)
        {
            string ServiceType = id;
            id = "RATETYPE";
            id = id.ToUpper();
            List<Webx_Master_General> listVehicle = new List<Webx_Master_General>();
            if (ServiceType != "1")
            {
                listVehicle = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == id && c.StatusCode.ToUpper() == "Y").ToList();
            }
            else
            {
                listVehicle = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == id && c.StatusCode.ToUpper() == "Y" && c.CodeId.ToUpper() != "F").ToList();
            }



            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.CodeId,
                                   Text = e.CodeDesc,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGeneralMasterDetailsWithContract(string id, string CodeList)
        {

            id = id.ToUpper();

            string[] strTaskid = CodeList.Split(',');

            List<Webx_Master_General> listVehicle = new List<Webx_Master_General>();

            if (CodeList != "")
                listVehicle = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == id && c.StatusCode.ToUpper() == "Y" && strTaskid.Contains(c.CodeId)).ToList();
            else
                listVehicle = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == id && c.StatusCode.ToUpper() == "Y").ToList();


            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.CodeId,
                                   Text = e.CodeDesc,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetPKGSListJson(string str)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();
            ListLocations = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "PKGS" && c.StatusCode.ToUpper() == "Y").ToList();

            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPRODCDSListJson(string str)
        {
            List<Webx_Master_General> ListLocations = new List<Webx_Master_General>();
            ListLocations = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "PROD" && c.StatusCode.ToUpper() == "Y").ToList();

            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDeliveryZone()
        {
            List<webx_Delivery_Zone> listVehicle = new List<webx_Delivery_Zone>();
            listVehicle = OS.GetDeliveryZone(BaseLocationCode).ToList();

            var VehicleList = (from e in listVehicle
                               select new
                               {
                                   Value = e.ZoneID,
                                   Text = e.ZoneName,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBillingParty(string searchTerm, string Location, string Paybas)
        {
            searchTerm = searchTerm.ToUpper();
            Location = Location.ToUpper();

            var CMP = new List<webx_CUSTHDR>();

            //if (Location == null || Location == "")
            //    CMP = MS.GetCustomerMasterObject1().Where(c => (c.CUSTCD.ToUpper().Contains(searchTerm) || c.CUSTNM.ToUpper().Contains(searchTerm)) && c.CUST_ACTIVE.ToUpper() == "Y").ToList();
            //else
            //    CMP = MS.GetCustomerMasterObject1().Where(c => (c.CUSTCD.ToUpper().Contains(searchTerm) || c.CUSTNM.ToUpper().Contains(searchTerm)) && c.CUST_ACTIVE.ToUpper() == "Y" && c.CUSTLOC.ToUpper().Contains(Location)).ToList();

            //string CustHierarchy = "";

            //CustHierarchy = MS.GetRuleObject("CUST_HRCHY").defaultvalue;

            //if (CustHierarchy != "")
            //    CMP = CMP.Where(c => CustHierarchy.Contains(c.customer_hierarchy)).ToList();

            CMP = MS.GetCustomerMasterObject1(searchTerm, Location, Paybas);

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };
            //return new JsonpResult
            //{
            //    Data = CMP,
            //    JsonRequestBehavior = JsonRequestBehavior.AllowGet
            //};
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetBillingParty_OctroiBill(string searchTerm, string Location, string Paybas)
        {
            searchTerm = searchTerm.ToUpper();
            Location = Location.ToUpper();

            var CMP = new List<webx_CUSTHDR>();
            CMP = MS.GetCustomerMaster_OctroiBill(searchTerm, Location, Paybas);

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDestinationLocations(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_location> CMP = OS.GetDestinationLocations(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDestinationLocationsWithoutSomeLocation(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_location> CMP = OS.GetDestinationLocations(searchTerm);

            var users = from user in CMP.Where(c => c.LocCode.ToUpper() != "YPRD").ToList()
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerDetails(string custcd)
        {
            //var CMP = MS.GetCustomerMasterObject().Where(c => c.CUSTCD == custcd && c.CUST_ACTIVE.ToUpper() == "Y").ToList();
            var CMP = MS.GetCustomerMasterObject(custcd);

            if (CMP.Count() > 0)
                return Json(new { Address = CMP.First().CustAddress, Name = CMP.First().CUSTNM, Mobileno = CMP.First().MOBILENO, GSTNO = CMP.First().GSTNO, City = CMP.First().city, Pincode = CMP.First().pincode, Email = CMP.First().EMAILIDS, SchedualeApply = CMP.First().ScheduleApply }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { Address = "", Name = "", Mobileno = "", GSTNO = "", City = "", Pincode = "", Email = "", SchedualeApply = "", TELNO = "" }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetDestinationLocations_With_HQTR(string searchTerm)
        {
            searchTerm = !string.IsNullOrEmpty(searchTerm) ? searchTerm.ToUpper() : searchTerm;

            List<webx_location> CMP = OS.GetDestinationLocations_With_HQTR(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPincodeMatrixCharges(string PinCode)
        {
            DataTable DT = new DataTable();
            decimal KM_From_Location = 0; string Is_ODA_Apply = "";
            DT = OS.Get_Pincode_Matrix_CHarges(PinCode);
            if (DT.Rows.Count > 0)
            {
                //string StateCode = DT.Rows[0]["StateCode"].ToString();
                //string cityname = DT.Rows[0]["cityname"].ToString();
                KM_From_Location = System.Convert.ToDecimal(DT.Rows[0]["KM_From_Location"]);
                //string Handling_Location = DT.Rows[0]["Handling_Location"].ToString();
                Is_ODA_Apply = DT.Rows[0]["Is_ODA_Apply"].ToString();
            }

            return new JsonResult()
            {
                Data = new
                {
                    Is_ODA_Apply = Is_ODA_Apply,
                    KM_From_Location = KM_From_Location
                }
            };
        }

        public JsonResult GetPincodeMatrixRate(decimal KM_From_Location, decimal CHRGWT)
        {
            DataTable DT = new DataTable();
            decimal Rate = 0;
            DT = OS.Get_Pincode_Matrix_Rate(KM_From_Location, CHRGWT);
            Rate = System.Convert.ToDecimal(DT.Rows[0]["Rate"]);
            return new JsonResult()
            {
                Data = new
                {
                    Rate = Rate,
                }
            };
        }

        public JsonResult GetEDDDateAdday(string DOCKDT, string REASSIGN_DESTCD, string EDDDate, string Total_Amount, string ODAFlag, string Original_Edd, int TRN_Days, bool DACCFlag, bool CODDODFlag)
        {
            string Flag = ODAFlag;
            double defaultValue = 0;

            try
            {
                Original_Edd = Original_Edd.Replace("-", " ");
                EDDDate = EDDDate.Replace("-", " ");

                //if (Convert.ToDouble(Total_Amount) != defaultValue && ODAFlag != "Y")
                //{
                //    EDDDate = Convert.ToDateTime(EDDDate).AddDays(+3).ToString("dd-MMM-yyyy");
                //    Flag = "Y"; 
                //    TRN_Days = TRN_Days + 3;
                //}
                //if (Convert.ToDouble(Total_Amount) == defaultValue && EDDDate != Original_Edd && Original_Edd != "")
                //{
                //    EDDDate = Convert.ToDateTime(EDDDate).AddDays(-3).ToString("dd-MMM-yyyy");
                //    Flag = "N";
                //    TRN_Days = TRN_Days - 3;
                //}
                if ((DACCFlag == true || CODDODFlag == true) && ODAFlag != "Y")
                {
                    Flag = "Y";
                    TRN_Days = TRN_Days + 3;
                }
                if (DACCFlag == false && CODDODFlag == false && ODAFlag != "N" && EDDDate != Original_Edd && Original_Edd != "")
                {
                    Flag = "N";
                    TRN_Days = TRN_Days - 3;
                }

                DateTime SYSEDDDate = Convert.ToDateTime(GF.FormateDate(Convert.ToDateTime(DOCKDT).AddDays(Convert.ToInt32(TRN_Days))));
                if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "PU")
                {
                    DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, REASSIGN_DESTCD, true);
                    if (edd_hday > SYSEDDDate)
                    {
                        SYSEDDDate = edd_hday;
                    }
                }
                else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "AD")
                {
                    DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, REASSIGN_DESTCD, true);
                    if (edd_hday > SYSEDDDate)
                    {
                        // hdnhdayedd.Value = edd_hday.ToString("dd/MM/yyyy");
                        SYSEDDDate = edd_hday;
                    }
                }
                else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "N")
                {
                    if (SYSEDDDate.DayOfWeek.ToString().ToUpper().CompareTo("SUNDAY") == 0)
                        SYSEDDDate.AddDays(1);
                }

                EDDDate = SYSEDDDate.ToString("dd-MMM-yyyy");
            }
            catch (Exception)
            {
                EDDDate = "";
            }

            return new JsonResult()
            {
                Data = new
                {
                    EDDDate = EDDDate,
                    Flag = Flag,
                    TRN_Days = TRN_Days,
                }
            };

        }

        public void SendMail(WebX_Master_Docket OBjDKT)
        {
            CustomergeneratemailviewModel CGMV = new CustomergeneratemailviewModel();
            CGMV.Dkt = new WebX_Master_Docket();
            CGMV.Dkt.DOCKNO = OBjDKT.DOCKNO;
            CGMV.Dkt.DOCKDT = OBjDKT.DOCKDT;
            CGMV.Dkt.DACC = OBjDKT.DACC;
            CGMV.Dkt.COD = OBjDKT.COD;
            CGMV.Dkt.MailId = OBjDKT.MailId;
            CGMV.Dkt.MailId1 = OBjDKT.MailId1;
            CGMV.Dkt.MailId2 = OBjDKT.MailId2;
            CGMV.Dkt.DACC_YN = OBjDKT.DACC_YN;
            CGMV.Dkt.COD_DOD = OBjDKT.COD_DOD;
            if (OBjDKT.DACC_YN == "Y")
            {
                EC.SendMail(CGMV);
            }
            else if (OBjDKT.COD_DOD == "Y")
            {
                EC.SendMail(CGMV);
            }
        }

        #endregion

        #region Connection THC

        public ActionResult THCDepartures()
        {
            THCDeparturesViewModel THVVM = new THCDeparturesViewModel();
            THVVM.THCDepList = new List<webx_THC_SUMMARY>();
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult THCDepartures(THCDeparturesViewModel VM)
        {
            List<webx_THC_SUMMARY> ListTHCDep = new List<webx_THC_SUMMARY>();
            DataTable DT = OS.ListOFTHCDepartures(BaseLocationCode, VM.THCDepModel.thcnoumber, VM.THCDepModel.MFNo, VM.THCDepModel.DockNo, GF.FormateDate(VM.THCDepModel.FromDate), GF.FormateDate(VM.THCDepModel.ToDate));
            //return JsonConvert.SerializeObject(OP.ListOFTHCDepartures(BaseLocationCode, VM.THCDepModel.thcno, VM.THCDepModel.MFNo, VM.THCDepModel.DockNo, GF.FormateDate(VM.THCDepModel.FromDate), GF.FormateDate(VM.THCDepModel.ToDate)));
            ListTHCDep = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(DT);
            VM.THCDepList = ListTHCDep;
            return View(VM);

        }

        [HttpPost]
        public string GetData(string Method, string param1, string param2, string param3, string param4)
        {
            return JsonConvert.SerializeObject(MS.GetData(Method, param1, param2, param3, param4));
        }

        public ActionResult THCDepartureSubmit(string THCNO)
        {
            THCDeparturesViewModel THVVM = new THCDeparturesViewModel();
            THVVM.THCDepModel = new webx_THC_SUMMARY();
            List<webx_THC_SUMMARY> ListTHCDep = new List<webx_THC_SUMMARY>();
            DataTable DT1 = OS.Get_MF_For_OutgoingTHC(THCNO);
            ListTHCDep = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(DT1);
            THVVM.THCDepList = ListTHCDep;

            DataTable DT = OS.Get_THC_For_Departure_Summary(THCNO, BaseLocationCode);
            THVVM.THCDepModel = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(DT).FirstOrDefault();
            if (THVVM.THCDepModel == null)
                THVVM.THCDepModel = new webx_THC_SUMMARY();
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult THCDepartureSubmit(THCDeparturesViewModel THVVM, List<webx_THC_SUMMARY> ThcDeparterList)
        {
            string TCNO = "", THCNO = "";
            string xmlTC = "<root>";
            if (ThcDeparterList != null && ThcDeparterList.Count() > 0)
            {
                foreach (var item in ThcDeparterList.Where(c => c.IsChecked == true))
                {
                    xmlTC = xmlTC + "<tc>" +
                        "<TCNO>" + item.tcno + "</TCNO>" +
                        "<THCNO>" + THVVM.THCDepModel.THCNO + "</THCNO>" +
                        "</tc>";
                }
            }

            xmlTC = xmlTC + "</root>";
            DataTable DT = OS.Usp_do_THC_Departure(THVVM.THCDepModel.THCNO, THVVM.THCDepModel.ATD, DateTime.Parse(THVVM.THCDepModel.deptime_flight).ToString("HH:mm"), THVVM.THCDepModel.sealno_in, THVVM.THCDepModel.OutRemarks, BaseUserName, BaseLocationCode, xmlTC);
            if (DT.Rows.Count > 0)
            {
                TCNO = DT.Rows[0][0].ToString();
                THCNO = DT.Rows[0][1].ToString();
            }
            return RedirectToAction("THCDepartureDone", new { TCNO = TCNO, THCNO = THCNO });
        }

        public ActionResult THCDepartureDone(string TCNO, string THCNO)
        {
            ViewBag.TCNO = TCNO;
            ViewBag.THCNO = THCNO;
            return View();
        }

        #endregion

        #region Single CNote Update in DRS

        public ActionResult SingleCNoteUpdateInDRS()
        {
            SingleCNoteUpdatViewModel SCUVM = new SingleCNoteUpdatViewModel();
            SCUVM.CNoteUpdatList = new List<vw_SingleCNoteUpdatList>();
            SCUVM.ListCNoteUpdat = new List<vw_SingleCNoteUpdatList>();
            return View(SCUVM);
        }

        [HttpPost]
        public ActionResult SingleCNoteUpdateInDRS(SingleCNoteUpdatViewModel SCUVM)
        {
            List<vw_SingleCNoteUpdatList> ListCnot = new List<vw_SingleCNoteUpdatList>();
            DataTable DT = OS.get_ListOFSingleCNoteUpdat(SCUVM.SingleCnote.DOCKNO);
            ListCnot = DataRowToObject.CreateListFromTable<vw_SingleCNoteUpdatList>(DT);
            SCUVM.CNoteUpdatList = ListCnot;
            List<vw_SingleCNoteUpdatList> ListCnotList = new List<vw_SingleCNoteUpdatList>();
            DataTable DT1 = OS.get_ListOFCNoteUpdat(SCUVM.SingleCnote.DOCKNO, BaseLocationCode);
            ListCnotList = DataRowToObject.CreateListFromTable<vw_SingleCNoteUpdatList>(DT1);
            SCUVM.ListCNoteUpdat = ListCnotList;
            return View(SCUVM);
        }

        public ActionResult UpdateDRSById(string DOCKNO, string DRSCode)
        {
            UpdateDRSViewModel UDVM = new UpdateDRSViewModel();
            List<vw_DRS_Summary> DRSSummaryList = new List<vw_DRS_Summary>();
            List<vw_dockets_for_drs_updation_New> DRSUpdateList = new List<vw_dockets_for_drs_updation_New>();
            DRSUpdateList = OS.GetUpdateSingleDRSList(DOCKNO, DRSCode);
            UDVM.UpdateDRSLits = DRSUpdateList;
            UDVM.DRSSummary = OS.GetUpdateDRSSummaryList(DRSCode).FirstOrDefault();
            ViewBag.Currentdate = System.DateTime.Now;
            return View(UDVM);
        }

        [HttpPost]
        public ActionResult UpdateDRSSubmit(UpdateDRSViewModel VM, List<vw_dockets_for_drs_updation_New> DRSDocketsUpdateList)
        {

            XmlDocument XDoc = new XmlDocument();

            string XML = "<root>";

            foreach (var item in DRSDocketsUpdateList)
            {
                var delyStatus = "U";
                XML = XML + "<DRS>";
                XML = XML + "<DRSCode>" + VM.DRSSummary.pdcno + "</DRSCode>";
                XML = XML + "<CLOSEKM>" + VM.DRSSummary.CloseKM + "</CLOSEKM>";
                XML = XML + "<DOCKET>" + item.dockno + "</DOCKET>";
                XML = XML + "<DOCKETSF>" + item.docksf + "</DOCKETSF>";
                XML = XML + "<PKGSWASPENDING>" + item.Pkgs_Pending + "</PKGSWASPENDING>";
                XML = XML + "<PKGSDELIVERED>" + item.PKGSDELIVERED + "</PKGSDELIVERED>";
                if (item.PKGSDELIVERED < item.Pkgs_Pending && item.PKGSDELIVERED != 0)
                {
                    delyStatus = "P"; // Part Dely. 
                }
                else
                {
                    if (item.PKGSDELIVERED != 0)
                    {
                        delyStatus = "F"; // Full Dely.
                    }
                }
                if (item.PKGSDELIVERED == 0)
                    delyStatus = "U";

                XML = XML + "<DELYDATE>" + item.DELYDATE.ToString("dd MMM yyyy") + "</DELYDATE>";

                if (item.PKGSDELIVERED > 0 && delyStatus == "F")
                {
                    delyStatus = "L"; //Late Dely
                }
                else if (item.PKGSDELIVERED <= 0 && delyStatus == "F")
                {
                    delyStatus = "D"; //Good Dely
                }

                XML = XML + "<DELYSTATUS>" + delyStatus + "</DELYSTATUS>";// remaining
                XML = XML + "<DELYTIME>" + item.DELYTIME.ToString("HH:MM") + "</DELYTIME>";
                XML = XML + "<DELYREASON>" + item.cboReason + "</DELYREASON>";
                XML = XML + "<DELYPERSON>" + item.DELYPERSON + "</DELYPERSON>";

                XML = XML + "<CODDODAMOUNT>" + item.CODDODAmount + "</CODDODAMOUNT>";
                XML = XML + "<CODDODCOLLECTED>" + item.CODDODCOLLECTED + "</CODDODCOLLECTED>";
                XML = XML + "<CODDODNO>" + item.CODDODNO + "</CODDODNO>";
                XML = XML + "</DRS>";
            }
            XML = XML + "</root>";

            bool Status = OS.Insert_DRS_Update_Single(BaseUserName, XML.ReplaceSpecialCharacters());
            return RedirectToAction("UpdateSingleDRSDone", new { DRSNO = VM.DRSSummary.pdcno, Status = Status });
        }

        public ActionResult UpdateSingleDRSDone(string DRSNO, bool Status)
        {
            @ViewBag.DRSNO = DRSNO;
            @ViewBag.Status = Status;
            return View();
        }

        public JsonResult GetDKTStatusForSingleCNotUpdate(string dockno)
        {
            string Flg = "1", MSG = "";


            MSG = OS.GetDKTStatusForSingleCNotUpdate(dockno);


            if (MSG.ToUpper() != "DONE")
            {
                Flg = "0";
            }

            return new JsonResult()
            {
                Data = new
                {

                    Flg = Flg,
                    MSG = MSG
                }
            };

        }

        #endregion

        #region Report

        public ActionResult LodingSheetViewPrint(string LsNO)
        {
            LoadingSheetFilterViewModel EF = new LoadingSheetFilterViewModel();
            EF.LsNO = LsNO;
            return View(EF);
        }

        public ActionResult UpdateResultViewPrint(string DRSNo)
        {
            LoadingSheetFilterViewModel EF = new LoadingSheetFilterViewModel();
            EF.DRSNo = DRSNo;
            return View(EF);
        }

        public ActionResult Menifest_ViewPrint(string MFNo)
        {
            LoadingSheetFilterViewModel EF = new LoadingSheetFilterViewModel();

            EF.MFNo = MFNo;


            return View(EF);
        }

        public ActionResult ChallanViewPrint(string THCNo)
        {
            ChallanViewModel EF = new ChallanViewModel();
            EF.THCNo = THCNo;
            return View(EF);
        }

        public ActionResult VendorContractViewPrint(string ContractID)
        {
            Webx_Vendor_Contract_Summary_Ver1ViewModel EF = new Webx_Vendor_Contract_Summary_Ver1ViewModel();
            EF.ContractID = ContractID;
            return View(EF);
        }

        #endregion

        /* Chirag D */
        #region PoGeneration SKU

        public ActionResult SKUPOGeneration(string Type)
        {
            PoGenerationViewModel objGV = new PoGenerationViewModel();
            objGV.webx_GENERAL_POASSET_HDR = new webx_GENERAL_POASSET_HDR();
            objGV.webx_GENERAL_POASSET_det = new webx_GENERAL_POASSET_det();
            objGV.Listwebx_location = MS.GetLocationDetails();
            List<webx_GENERAL_POASSET_det> newCMWW = new List<webx_GENERAL_POASSET_det>();
            webx_GENERAL_POASSET_det CMU = new webx_GENERAL_POASSET_det();
            CMU.srno = 1;
            newCMWW.Add(CMU);
            objGV.Listwebx_GENERAL_POASSET_det = newCMWW;
            return View("SKUPOGeneration", objGV);
        }

        [HttpPost]
        public ActionResult SKUPOGeneration(webx_GENERAL_POASSET_HDR webx_GENERAL_POASSET_HDR, List<webx_GENERAL_POASSET_det> GENERAL_POASSET_det, PaymentControl PaymentControl)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            string sql = "exec WebX_SP_GetNextDocumentCode_FA  '" + BaseLocationCode + "','" + BaseFinYear + "','GPO'";
            DataTable Dt = GF.GetDataTableFromSP(sql);
            string POCODE = Dt.Rows[0][0].ToString();
            string Adv_VocherNo = "";
            try
            {
                webx_GENERAL_POASSET_HDR.PO_advamt = webx_GENERAL_POASSET_HDR.PAIDAMT;

                string Xml_POASSET_HDR_Detail = Xml_POASSET_HDR_Details(webx_GENERAL_POASSET_HDR, POCODE, "PO GENERATED", "UNS0004", "Sundry Creditors - Others", "N", "N", "N", PaymentControl);
                string Xml_PO_Detail = Xml_PO_Details(GENERAL_POASSET_det, POCODE, webx_GENERAL_POASSET_HDR.podate);

                DataTable dt = OS.usp_Generate_PO_Asset_Details(Xml_POASSET_HDR_Detail, Xml_PO_Detail, BaseFinYear);

                if (dt.Rows.Count > 0)
                {
                    DataRow ResultDtlRow = dt.Rows[0];
                    if (ResultDtlRow["Status"] != DBNull.Value)
                    {
                        int status = Convert.ToInt32(ResultDtlRow["Status"]);
                        string TranXaction = ResultDtlRow["Message"].ToString();

                        if (status == 0 && TranXaction != "Done")
                            throw new Exception(ResultDtlRow["Message"].ToString());
                        if (status == 1)
                        {
                            Adv_VocherNo = ResultDtlRow["Voucherno"].ToString();
                            // POCODE = ResultDtlRow["PoCode"].ToString();
                        }
                    }
                    else
                        throw new Exception("Unknown Exception");
                }
            }
            catch (Exception ex)
            {
                string exmess = ex.Message.ToString().Replace('\n', '_');
                trn.Rollback();
                con.Close();
                ViewBag.Message = exmess;
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            ViewBag.CalledController = "SKU";
            return RedirectToAction("PurchaseOrder_Done", new { POCODE = POCODE, Adv_VocherNo = Adv_VocherNo });
        }

        public ActionResult GeneralizeWOTableForSKU(int id, string SelectedValue, string IsFabricSheet)
        {
            ViewBag.SelectedValue = SelectedValue;

            //This Condition is as per old
            if (IsFabricSheet == "true")
            {
                ViewBag.IsFabricSheet = "";
            }
            else
            {
                ViewBag.IsFabricSheet = "N";
            }
            webx_GENERAL_POASSET_det GENERAL_POASSET_det = new webx_GENERAL_POASSET_det();
            GENERAL_POASSET_det.srno = id;
            return PartialView("_GeneralizeWOTableForSKU", GENERAL_POASSET_det);
        }

        public ActionResult PurchaseOrder_Done(string POCODE, string Adv_VocherNo)
        {
            string Financial_Year = "", fin_year = "";
            Financial_Year = BaseFinYear.Substring(2, 2);
            fin_year = BaseFinYear.ToString();
            double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
            fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
            // string VoucherNo = "";// get it with "POCODE"
            string str = OS.WorkOrder_Done(fin_year, POCODE);
            ViewBag.POCODE = POCODE;
            ViewBag.Adv_VocherNo = Adv_VocherNo;
            return View();
        }

        public string Xml_POASSET_HDR_Details(webx_GENERAL_POASSET_HDR webx_GENERAL_POASSET_HDR, string POCODE, string POSTATUS, string AccCode, string AccDesc, string FabricSheet_YN, string PerfectBox_YN, string LCDBOX_YN, PaymentControl PaymentControl)
        {
            string Xml_POASSET_HDR_Details = "<root>";
            string str = "";
            if (webx_GENERAL_POASSET_HDR.ValideDate.ToString("dd MMM yyyy") != "01 Jan 0001")
            {
                str = webx_GENERAL_POASSET_HDR.ValideDate.ToString("dd MMM yyyy");
            }
            else
            {
                str = null;
            }
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<POHDR>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<POCODE>" + POCODE + "</POCODE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PODATE>" + webx_GENERAL_POASSET_HDR.podate.ToString("dd MMM yyyy") + "</PODATE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<VENDORCD>" + webx_GENERAL_POASSET_HDR.VENDORCD + "</VENDORCD>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<VENDORNM>" + webx_GENERAL_POASSET_HDR.VENDORNAME + "</VENDORNM>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<TOTALQTY>" + webx_GENERAL_POASSET_HDR.totalqty + "</TOTALQTY>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<TOTALAMT>" + webx_GENERAL_POASSET_HDR.totalamt + "</TOTALAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<ATTN>" + webx_GENERAL_POASSET_HDR.attn + "</ATTN>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<POSTATUS>" + POSTATUS + "</POSTATUS>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PENDAMT>" + webx_GENERAL_POASSET_HDR.pendamt + "</PENDAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<Brcd>" + BaseLocationCode + "</Brcd>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<CHQDATE>" + GF.FormateDate(PaymentControl.ChequeDate) + "</CHQDATE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PAYMODE>" + PaymentControl.PaymentMode + "</PAYMODE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<CHQNO>" + PaymentControl.ChequeNo + "</CHQNO>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PAIDAMT>" + webx_GENERAL_POASSET_HDR.PAIDAMT + "</PAIDAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<TERMS_CONDITION>" + webx_GENERAL_POASSET_HDR.terms_condition + "</TERMS_CONDITION>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<CHQAMT>" + PaymentControl.ChequeAmount + "</CHQAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<CASHAMT>" + PaymentControl.CashAmount + "</CASHAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PO_ADVAMT>" + webx_GENERAL_POASSET_HDR.PO_advamt + "</PO_ADVAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<PO_BALAMT>" + webx_GENERAL_POASSET_HDR.PO_balamt + "</PO_BALAMT>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<BANK_ACCODE>" + PaymentControl.BankLedger + "</BANK_ACCODE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<CASH_ACCODE>" + PaymentControl.CashLedger + "</CASH_ACCODE>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<MANUAL_PO_NO>" + webx_GENERAL_POASSET_HDR.Manual_PO_No + "</MANUAL_PO_NO>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<Company_Code>" + BaseCompanyCode + "</Company_Code>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<AccCode>" + AccCode + "</AccCode>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<AccDesc>" + AccDesc + "</AccDesc>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<MatCat>" + webx_GENERAL_POASSET_HDR.MatCat + "</MatCat>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<GSTPercentage>" + webx_GENERAL_POASSET_HDR.GSTPercentage + "</GSTPercentage>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "<IsGSTApplied>1</IsGSTApplied>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "</POHDR>";
            Xml_POASSET_HDR_Details = Xml_POASSET_HDR_Details + "</root>";
            return Xml_POASSET_HDR_Details;
        }

        public string Xml_PO_Details(List<webx_GENERAL_POASSET_det> webx_GENERAL_POASSET_det, string POCODE, DateTime podate)
        {
            string Xml_PO_Details = "<root>";

            foreach (var item in webx_GENERAL_POASSET_det)
            {
                if (item.assetcd != null || item.assetcd != "")
                {
                    Xml_PO_Details = Xml_PO_Details + "<Other>";
                    Xml_PO_Details = Xml_PO_Details + "<pocode>" + POCODE + "</pocode>";
                    Xml_PO_Details = Xml_PO_Details + "<podate>" + podate.ToString("dd MMM yyyy") + "</podate>";
                    Xml_PO_Details = Xml_PO_Details + "<assetcd>" + item.assetcd.Split('~').Last() + "</assetcd>";
                    Xml_PO_Details = Xml_PO_Details + "<qty>" + item.qty + "</qty>";
                    Xml_PO_Details = Xml_PO_Details + "<rate>" + item.rate + "</rate>";
                    Xml_PO_Details = Xml_PO_Details + "<Other_Chg>" + item.Other_Chg + "</Other_Chg>";
                    Xml_PO_Details = Xml_PO_Details + "<total>" + item.total + "</total>";
                    Xml_PO_Details = Xml_PO_Details + "<entryby>" + BaseUserName.ToUpper() + "</entryby>";
                    Xml_PO_Details = Xml_PO_Details + "<location>" + BaseLocationCode + "</location>";
                    Xml_PO_Details = Xml_PO_Details + "<narration>" + item.narration + "</narration>";
                    Xml_PO_Details = Xml_PO_Details + "<tax_per>" + item.tax_per + "</tax_per>";
                    Xml_PO_Details = Xml_PO_Details + "<tax_type>" + "" + "</tax_type>";
                    Xml_PO_Details = Xml_PO_Details + "<activeflag>" + "N" + "</activeflag>";
                    Xml_PO_Details = Xml_PO_Details + "<balanceqty>" + item.balanceqty + "</balanceqty>";
                    Xml_PO_Details = Xml_PO_Details + "<Discount_Per>" + item.Discount_Per + "</Discount_Per>";
                    Xml_PO_Details = Xml_PO_Details + "<TyreSizeCode>" + item.TyreSizeCode + "</TyreSizeCode>";
                    Xml_PO_Details = Xml_PO_Details + "<TyreMFG_Code>" + item.TyreMFG_Code + "</TyreMFG_Code>";
                    Xml_PO_Details = Xml_PO_Details + "<TyreTypeCode>" + item.TyreTypeCode + "</TyreTypeCode>";
                    Xml_PO_Details = Xml_PO_Details + "<UnitType>" + item.UnitType + "</UnitType>";
                    Xml_PO_Details = Xml_PO_Details + "<TaxAmt>" + item.TaxAmt + "</TaxAmt>";
                    Xml_PO_Details = Xml_PO_Details + "<DiscountAmt>" + item.DiscountAmt + "</DiscountAmt>";
                    Xml_PO_Details = Xml_PO_Details + "<WarrantyInYear>" + item.WarrantyInYear + "</WarrantyInYear>";
                    Xml_PO_Details = Xml_PO_Details + "<HSN_SAC>" + item.HSN_SAC + "</HSN_SAC>";
                    Xml_PO_Details = Xml_PO_Details + "<taxableAmt>" + item.taxableAmt + "</taxableAmt>";
                    //Xml_PO_Details = Xml_PO_Details + "<LastRate>0</LastRate>";
                    Xml_PO_Details = Xml_PO_Details + "</Other>";
                }
            }
            Xml_PO_Details = Xml_PO_Details + "</root>";
            return Xml_PO_Details;
        }

        public string CheckExist(string mode, string code)
        {
            string Count = CheckPOGenerationExistance(mode, code);
            int i = 0;
            string val = "";
            string desc = "";
            if (Convert.ToInt16(Count) != 0)
            {
                i = 1;
            }
            if (mode.CompareTo("ManualPONo") == 0)
            {
                if (i == 1)
                    val = "true|";
                else if (i == 0)
                    val = "false|" + desc + "|";
            }
            else
            {
                if (i == 0)
                    val = "false|";
                else if (i == 1)
                    val = "true|" + desc + "|";
            }
            return val;
        }

        public string CheckPOGenerationExistance(string mode, string code)
        {
            string sql = "";
            if (mode.CompareTo("SKU") == 0)
                sql = "SELECT Cnt=Count(*) FROM Webx_PO_SKU_Master Where SKU_ID='" + code + "'";
            else if (mode.CompareTo("Spare") == 0)
                sql = "SELECT Cnt=Count(*) FROM Webx_PO_SKU_Master Where SKU_ID='" + code + "'";
            else if (mode.CompareTo("Vendor") == 0)
                sql = "SELECT Count(*) FROM Webx_Vendor_hdr Where VENDORCODE='" + code + "'";
            else if (mode.CompareTo("TyreModel") == 0)
                sql = "SELECT COUNT(*) FROM WEBX_FLEET_TYREMODELMST WHERE TYRE_MODEL_ID ='" + code + "'";
            else if (mode.CompareTo("TyreMFG") == 0)
                sql = "SELECT COUNT(*) FROM WEBX_FLEET_TYREMFG WHERE MFG_ID='" + code + "'";
            else if (mode.CompareTo("TyreSize") == 0)
                sql = "SELECT Count(*) FROM WEBX_FLEET_TYRESIZEMST Where Tyre_sizeID='" + code + "'";
            else if (mode.CompareTo("ManualPONo") == 0)
                sql = "Select COUNT(*) FROM webx_GENERAL_POASSET_HDR Where Manual_PO_No='" + code + "'";
            else if (mode.CompareTo("FixAsset") == 0)
                sql = "Select COUNT(*) FROM webx_ASSETMASTER Where ASSETCD='" + code + "'";
            return GF.executeScalerQuery(sql);
        }

        public JsonResult GetVenderCode(string searchTerm)
        {
            var CMP = OS.GetVenderListSearch(searchTerm);

            var users = from user in CMP

                        select new
                        {
                            id = user.VENDORCD,
                            text = user.vendorname
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region GRN Generation

        public ActionResult GRNGenerationCriteria()
        {
            try
            {
                FuelBillEntyQuery FBE = new FuelBillEntyQuery();
                TempData["GRN"] = "Other";
                FBE.Type = 8;
                FBE.LocationLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
                //FBE.BaseFinYear = BaseFinYear.Split('-')[0].ToString();
                //FBE.SelectionType = "0";    
                FBE.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
                webx_location objNew = new webx_location();
                if (FBE.LocationLevel == 1)
                {
                    objNew.LocCode = "All";
                    objNew.LocName = "All";
                    FBE.RegionList.Insert(0, objNew);
                    FBE.LocationList = new List<webx_location>();
                    FBE.LocationList.Insert(0, objNew);
                }
                if (FBE.LocationLevel == 2)
                {
                    FBE.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                    objNew.LocCode = "All";
                    objNew.LocName = "All";
                    FBE.LocationList.Insert(0, objNew);
                }
                if (FBE.LocationLevel == 3)
                {
                    FBE.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
                }
                return View(FBE);
            }
            catch (Exception)
            {
                return RedirectToAction("GRNGenerationCriteria");
            }
        }

        public ActionResult GRNGenerationEntryList(FuelBillEntyQuery FBE)
        {
            try
            {
                GRNGenerationViewModel GRNVM = new GRNGenerationViewModel();
                FBE.MatCat = FBE.MatCat == "" || FBE.MatCat == null ? "All" : FBE.MatCat;
                GRNVM.POList = OS.GetPOGRNGeneration(FBE, BaseCompanyCode);
                GRNVM.FromDate = GF.FormateDate(FBE.FromDate);
                GRNVM.ToDate = GF.FormateDate(FBE.ToDate);
                GRNVM.VendorName = FBE.VendorName + "~" + FBE.VendorCode;
                GRNVM.PONo = FBE.VehicleNo == "" || FBE.VehicleNo == null ? "-" : FBE.VehicleNo;
                GRNVM.ManualPONo = FBE.ManualPONo == "" || FBE.ManualPONo == null ? "-" : FBE.ManualPONo;
                GRNVM.MatCat = FBE.MatCat == "All" || FBE.MatCat == null ? "" : MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "MATCAT" && c.CodeId == FBE.MatCat).FirstOrDefault().CodeDesc;
                return View(GRNVM);
            }
            catch (Exception)
            {
                return RedirectToAction("GRNGenerationCriteria");
            }
        }

        public ActionResult GRNDetails(string PONo, string VendorName, string VendorCode)
        {
            GRNGenerationViewModel GGVM = new GRNGenerationViewModel();
            List<POGRNTyreDetail> objList = new List<POGRNTyreDetail>();
            GGVM.POList = OS.GetGRNGenerationDetails(PONo);
            GGVM.POTyreList = objList;

            DataTable DT = OS.GetPOHDRDetails(PONo);
            webx_GENERAL_POASSET_HDR POHdr = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(DT).FirstOrDefault();

            GGVM.ManualPONo = POHdr.Manual_PO_No;
            GGVM.PONo = PONo;
            GGVM.VendorCode = VendorCode;
            GGVM.VendorName = VendorName;
            GGVM.MatCat = DT.Rows[0]["PoTypeDesc"].ToString();
            GGVM.PoDate = POHdr.podate.ToString("dd MMM yyyy");
            GGVM.PoAmount = POHdr.PO_balamt;
            GGVM.GRNLocation = BaseLocationCode;
            TempData["SaveXML"] = "";
            return View(GGVM);
        }

        public ActionResult AddGRNTyreList(int SrNo, string Size, string MFG, string Model, string Category, int TotalRow, decimal Rate)
        {
            List<POGRNTyreDetail> objList = new List<POGRNTyreDetail>();
            for (int i = 1; i <= TotalRow; i++)
            {
                POGRNTyreDetail ObjGRNTyre = new POGRNTyreDetail();
                ObjGRNTyre.SrNo = i;
                ObjGRNTyre.TyreSize = Size.Trim();
                ObjGRNTyre.TyreSizeID = MS.GetTyreSizeList().Where(c => c.TYRE_SIZENAME.ToUpper().Trim() == Size.ToUpper().Trim()).FirstOrDefault().TYRE_SIZEID;
                ObjGRNTyre.TyreMFG = MFG.Trim();
                ObjGRNTyre.TyreMFGID = MS.GetTYREMFGList().Where(c => c.MFG_NAME.ToUpper().Trim() == MFG.ToUpper().Trim()).FirstOrDefault().MFG_ID;
                ObjGRNTyre.TyreModel = Model.Trim();
                ObjGRNTyre.TyreModelID = MS.GetTyreModelList().Where(c => c.MODEL_DESC.ToUpper().Trim() == Model.ToUpper().Trim() || c.MODEL_NO.ToUpper() == Model.ToUpper().Trim()).FirstOrDefault().TYRE_MODEL_ID;
                ObjGRNTyre.TyreCategory = Category;
                ObjGRNTyre.TyreCategoryID = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper().Trim() == "TYTYP" && c.CodeDesc.ToUpper() == Category.ToUpper().Trim()).FirstOrDefault().CodeId;
                ObjGRNTyre.rate = Rate;
                objList.Add(ObjGRNTyre);
            }

            return PartialView("_GRNTyreDetail_obj", objList);
        }

        public JsonResult AddTyreGeneratedXML(GRNGenerationViewModel GNRModel, List<POGRNTyreDetail> GRNTyreList)
        {
            string Xml_SKUWise = "";
            foreach (var item in GRNTyreList)
            {
                Xml_SKUWise = Xml_SKUWise + "<SKUWise_TyreNo>";
                Xml_SKUWise = Xml_SKUWise + "<TyreNo>" + item.Tyreno + "</TyreNo>";
                Xml_SKUWise = Xml_SKUWise + "<TyreSizeId>" + item.TyreSizeID.Trim() + "</TyreSizeId>";
                Xml_SKUWise = Xml_SKUWise + "<TyreMFGId>" + item.TyreMFGID.Trim() + "</TyreMFGId>";
                Xml_SKUWise = Xml_SKUWise + "<TyreModelId>" + item.TyreModelID + "</TyreModelId>";
                Xml_SKUWise = Xml_SKUWise + "<TyreCategory>" + item.TyreCategoryID.Trim() + "</TyreCategory>";
                Xml_SKUWise = Xml_SKUWise + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
                Xml_SKUWise = Xml_SKUWise + "<Vendor>" + GNRModel.VendorCode.Trim() + "</Vendor>";
                Xml_SKUWise = Xml_SKUWise + "<Location>" + BaseLocationCode.Trim() + "</Location>";
                Xml_SKUWise = Xml_SKUWise + "<Position></Position>";
                Xml_SKUWise = Xml_SKUWise + "<Rate>" + item.rate + "</Rate>";
                Xml_SKUWise = Xml_SKUWise + "</SKUWise_TyreNo>";
            }

            GenXml = GenXml + Xml_SKUWise;

            return new JsonResult()
            {
                Data = new
                {
                    Count = "SUCESS",
                }
            };
        }

        public static string GenXml = "";
        [HttpPost]
        public ActionResult GRNGenerationSubmit(GRNGenerationViewModel GGVM, List<POGRNGenerationDetails> GRNDetails)
        {
            SqlTransaction trans;
            SqlConnection conn = new SqlConnection(Connstr);
            conn.Open();
            trans = conn.BeginTransaction();

            try
            {
                string Xml_ReqMST = "<root><GRNMst>", Xml_ReqDET = "<root>", MSTID = "", Status = "Fail";

                Xml_ReqMST = Xml_ReqMST + "<BBRCD>" + BaseLocationCode + "</BBRCD>";
                Xml_ReqMST = Xml_ReqMST + "<CHALLANDT>" + GF.FormateDate(GGVM.GRNDate) + "</CHALLANDT>";
                Xml_ReqMST = Xml_ReqMST + "<RECEIVEDT>" + GF.FormateDate(GGVM.GRNDate) + "</RECEIVEDT>";
                Xml_ReqMST = Xml_ReqMST + "<REMARK>" + GGVM.Remarks + "</REMARK>";
                Xml_ReqMST = Xml_ReqMST + "<ManualGRNNO>" + GGVM.ManualGRNNo + "</ManualGRNNO>";
                Xml_ReqMST = Xml_ReqMST + "<PREPAREDBY>" + BaseUserName + "</PREPAREDBY>";
                Xml_ReqMST = Xml_ReqMST + "<GRN_LOC>" + GGVM.GRNLocation + "</GRN_LOC>";
                Xml_ReqMST = Xml_ReqMST + "<PAY_LOC>" + GGVM.GRNLocation + "</PAY_LOC>";
                Xml_ReqMST = Xml_ReqMST + "<VENDORCD>" + GGVM.VendorCode + "</VENDORCD>";
                Xml_ReqMST = Xml_ReqMST + "<VENDORNM>" + GGVM.VendorName + "</VENDORNM>";
                Xml_ReqMST = Xml_ReqMST + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
                Xml_ReqMST = Xml_ReqMST + "<COMPANYCODE>" + BaseCompanyCode + "</COMPANYCODE>";
                Xml_ReqMST = Xml_ReqMST + "</GRNMst></root>";

                foreach (var item in GRNDetails)
                {
                    string[] ICode_Arr = item.Item_Code.ToString().Split('~');
                    string Item1 = "", Item2 = "", Item3 = "", Item4 = "";
                    if (ICode_Arr.Length == 1)
                    {
                        Item1 = ICode_Arr[0].ToString();
                    }
                    else
                    {
                        Item1 = ICode_Arr[0].ToString();
                        Item2 = ICode_Arr[1].ToString();
                        Item3 = ICode_Arr[2].ToString();
                        Item4 = ICode_Arr[3].ToString();
                    }

                    decimal subamt = (item.GRNQTY * item.rate);
                    decimal tottax = 0, Disamt = 0, Totamt = 0;

                    if (item.Discount_Per > 0)
                    {
                        //Disamt = ((tottax + subamt) * item.Discount_Per) / 100;
                        Disamt = (subamt * item.Discount_Per) / 100;
                    }
                    else
                    {
                        Disamt = 0;
                    }


                    if (item.tax_per > 0)
                    {
                        tottax = ((subamt - Disamt) * item.tax_per) / 100;
                    }
                    else
                    {
                        tottax = 0;
                    }

                    item.NetAmt = (item.GRNQTY * item.rate) - (item.GRNQTY * item.rate * item.Discount_Per / 100);

                    //Totamt = subamt + tottax - Disamt + item.Other_Chg;
                    if (item.RECEIVEDQTY == 0)
                    {
                        item.OtherChargesGRN = item.Other_Chg;
                    }
                    else
                    {
                        item.OtherChargesGRN = 0;
                    }
                    //item.OtherChargesGRN = item.Other_Chg / item.OrderQTY * item.GRNQTY;
                    Totamt = subamt + tottax - Disamt + item.OtherChargesGRN;

                    Xml_ReqDET = Xml_ReqDET + "<GRNdet>";
                    Xml_ReqDET = Xml_ReqDET + "<POCODE>" + GGVM.PONo + "</POCODE>";
                    if (ICode_Arr.Length == 1)
                    {
                        Xml_ReqDET = Xml_ReqDET + "<ICODE>" + item.Item_Code + "</ICODE>";
                        Xml_ReqDET = Xml_ReqDET + "<INAME>" + item.Item_Desc + "</INAME>";
                    }
                    else
                    {
                        Xml_ReqDET = Xml_ReqDET + "<ICODE>" + Item1.Trim() + "</ICODE>";
                        Xml_ReqDET = Xml_ReqDET + "<INAME>" + Item1.Trim() + "</INAME>";
                    }
                    Xml_ReqDET = Xml_ReqDET + "<TOTALQTY>" + item.OrderQTY + "</TOTALQTY>";
                    Xml_ReqDET = Xml_ReqDET + "<RECEQTY>" + item.GRNQTY + "</RECEQTY>";
                    Xml_ReqDET = Xml_ReqDET + "<RATE>" + item.rate + "</RATE>";
                    Xml_ReqDET = Xml_ReqDET + "<TOTAL>" + Totamt + "</TOTAL>";
                    Xml_ReqDET = Xml_ReqDET + "<ITEM1>" + Item1.Trim() + "</ITEM1>";
                    Xml_ReqDET = Xml_ReqDET + "<ITEM2>" + Item2.Trim() + "</ITEM2>";
                    Xml_ReqDET = Xml_ReqDET + "<ITEM3>" + Item3.Trim() + "</ITEM3>";
                    Xml_ReqDET = Xml_ReqDET + "<ITEM4>" + Item4.Trim() + "</ITEM4>";
                    Xml_ReqDET = Xml_ReqDET + "<GRNOtherCharges>" + item.OtherChargesGRN + "</GRNOtherCharges>";
                    Xml_ReqDET = Xml_ReqDET + "<narration>" + item.narration + "</narration>";
                    Xml_ReqDET = Xml_ReqDET + "<NetAmt>" + Math.Round(item.NetAmt) + "</NetAmt>";
                    Xml_ReqDET = Xml_ReqDET + "<PoSrno>" + item.srno + "</PoSrno>";
                    Xml_ReqDET = Xml_ReqDET + "<tax_per>" + item.tax_per + "</tax_per>";
                    Xml_ReqDET = Xml_ReqDET + "<GSTAmount>" + item.GSTAmount + "</GSTAmount>";
                    Xml_ReqDET = Xml_ReqDET + "</GRNdet>";
                }

                Xml_ReqDET = Xml_ReqDET + "</root>";
                GenXml = "<root>" + GenXml + "</root>";
                string GRNNo = getnewcd();
                string Sql = "EXEC [Usp_Insert_GRN_Data_Ver1_NewPortalGST] '" + Xml_ReqMST.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim() + "','" + Xml_ReqDET.Replace("&", "&amp;").Replace("–", "-").Replace("'", " ").Replace("'", "").Trim() + "','" + GenXml.Replace("&", "&amp;").Replace("–", "-").Trim() + "','" + BaseLocationCode.ToString() + "','" + BaseFinYear.Split('-')[0].ToString() + "','" + GRNNo + "','PO'";
                int Id = GF.SaveRequestServices(Sql.Replace("'", "''"), "GRNGenerationSubmit", "", "");
                DataTable DT = GF.getdatetablefromQuery(Sql);
                if (DT.Rows.Count > 0)
                {
                    MSTID = DT.Rows[0][0].ToString();
                    Status = "Done";
                }
                trans.Commit();
                return RedirectToAction("GRNDone", new { GRNNo = MSTID, Status = Status });
            }
            catch (Exception e1)
            {
                trans.Rollback();

                ViewBag.StrError = "Error" + e1.Message;
                return View("Error");
            }
        }

        public ActionResult GRNDone(string GRNNo, string Status)
        {
            POGRNTyreDetail GRNdONE = new POGRNTyreDetail();
            GRNdONE.Tyreno = GRNNo;
            GRNdONE.TyreCategory = Status;
            return View(GRNdONE);
        }

        public string getnewcd()
        {
            SqlConnection conn = new SqlConnection(Connstr);
            conn.Open();
            string sql = "exec WebX_SP_GetNextDocumentCode_FA   '" + BaseLocationCode + "','" + BaseFinYear.Split('-')[0].ToString() + "','GGRN'";
            SqlCommand sqlcmd = new SqlCommand(sql, conn);
            string cd1 = (string)sqlcmd.ExecuteScalar();
            return cd1;
        }

        public JsonResult CheckTyreNowithManufacturer(string TyreNo, string MFGID)
        {
            string CNT = OS.CheckTyreNowithManufacturer(TyreNo, MFGID);
            return new JsonResult()
            {
                Data = new
                {
                    CNT = CNT,
                }
            };
        }

        //public string GenXml
        //{
        //    get
        //    {
        //        if (TempData["Xml_SKU_Wise_TyreNo"] != null)
        //            return TempData["Xml_SKU_Wise_TyreNo"] as string;
        //        else
        //            return "";
        //    }
        //    set
        //    {
        //        TempData["Xml_SKU_Wise_TyreNo"] = value;
        //    }
        //}

        #endregion

        #region PoGeneration With Serial No

        public ActionResult POGeneration(string Type)
        {
            PoGenerationViewModel objGV = new PoGenerationViewModel();
            objGV.webx_GENERAL_POASSET_HDR = new webx_GENERAL_POASSET_HDR();
            objGV.webx_GENERAL_POASSET_det = new webx_GENERAL_POASSET_det();
            objGV.POFROM = DataRowToObject.CreateListFromTable<Webx_Master_General>(OS.POFROMsrno()).ToList();
            objGV.Listwebx_location = MS.GetLocationDetails();
            objGV.POGenerationType = ViewBag.Type;
            return View(objGV);
        }

        [HttpPost]
        //public ActionResult POGeneration(webx_GENERAL_POASSET_HDR webx_GENERAL_POASSET_HDR, List<webx_GENERAL_POASSET_det> GENERAL_POASSET_det, PoGenerationViewModel GVM)
        //{
        //    string POCODE = "";
        //    webx_GENERAL_POASSET_HDR.Location = BaseLocationCode;

        //    string Xml_POASSET_HDR_Detail = Xml_POASSET_HDR_Details(webx_GENERAL_POASSET_HDR, "", "PO GENERATED", "UNS0004", "Sundry Creditors - Others", "N", "N", "N");
        //    string Xml_PO_Detail = Xml_PO_Details(GENERAL_POASSET_det, "", webx_GENERAL_POASSET_HDR.podate);

        //    DataTable dt = new DataTable();
        //    dt = OS.POGenerationWithSerilaNo(Xml_POASSET_HDR_Detail, Xml_PO_Detail, BaseFinYear, GVM.webx_GENERAL_POASSET_HDR.terms_condition);
        //    string Adv_VocherNo = "";
        //    if (dt.Rows.Count > 0)
        //    {
        //        DataRow ResultDtlRow = dt.Rows[0];
        //        if (ResultDtlRow["Status"] != DBNull.Value)
        //        {
        //            int status = Convert.ToInt32(ResultDtlRow["Status"]);
        //            string TranXaction = ResultDtlRow["Message"].ToString();

        //            if (status == 0 && TranXaction != "Done")
        //                throw new Exception(ResultDtlRow["Message"].ToString());
        //            if (status == 1)
        //            {
        //                Adv_VocherNo = ResultDtlRow["Voucherno"].ToString();
        //                POCODE = ResultDtlRow["PoCode"].ToString();
        //            }
        //        }
        //        else
        //            throw new Exception("Unknown Exception");
        //    }
        //    return RedirectToAction("PurchaseOrder_Done", new { POCODE = POCODE });
        //}

        public ActionResult GeneralizePOTable(int id, string SKUID)
        {
            webx_GENERAL_POASSET_det GENERAL_POASSET_det = new webx_GENERAL_POASSET_det();
            GENERAL_POASSET_det.srno = id;
            GENERAL_POASSET_det.assetcd = SKUID;
            return PartialView("_GeneralizePOTable", GENERAL_POASSET_det);
        }

        #endregion

        #region PO Edit

        public ActionResult SKUPOEditCriteria(string POCode)
        {
            SKUPOEditViewModel ViewModel = new SKUPOEditViewModel();
            ViewModel.FilterType = "0";
            ViewModel.POType = "PO";
            return View(ViewModel);
        }

        public ActionResult SKUPOListing(SKUPOEditViewModel ViewModel)
        {
            List<webx_GENERAL_POASSET_HDR> POList;
            try
            {
                string FirstDate1 = ViewModel.FromDate.ToString("dd MMM yyyy");
                string LastDate1 = ViewModel.ToDate.ToString("dd MMM yyyy");
                if (ViewModel.VENDORCD != null && ViewModel.VENDORCD != "")
                {
                    ViewModel.VENDORCD = ViewModel.VENDORCD.Split('~').Last();
                }
                string CurrentLocationCode = BaseLocationCode;
                if (BaseUserName == "10001")
                {
                    CurrentLocationCode = "";
                }
                POList = OS.GetDataForPOEdit(ViewModel.FilterType, ViewModel.VENDORCD, FirstDate1, LastDate1, ViewModel.pocode, ViewModel.GPH.Manual_PO_No, BaseLocationCode);

                ViewModel.POList = POList;
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(ViewModel);
        }

        public ActionResult AddNewRowPOEdit(int SrNo, string IsFabricSheet, string MatCat)
        {
            webx_GENERAL_POASSET_det WAD = new webx_GENERAL_POASSET_det();
            ViewBag.MatCat = MatCat;
            try
            {
                WAD.srno = SrNo;
                if (IsFabricSheet == "true")
                {
                    ViewBag.IsFabricSheet = "Y";
                }
                else
                {
                    ViewBag.IsFabricSheet = "N";
                }
            }
            catch (Exception ex)
            {
                Error_Logs("Operation", "AddNewRowPOEdit", "PartialView", "Listing", ex.Message);
            }
            return PartialView("_SKUPOEditPartial", WAD);
        }

        public string CheckExistForSKUPOEdit(string mode, string code)
        {
            string Count = CheckPOGenerationExistance(mode, code);
            int i = 0;
            string val = "";
            string desc = "";
            if (Convert.ToInt16(Count) != 0)
            {
                i = 1;
            }
            if (mode.CompareTo("ManualPONo") == 0)
            {
                if (i == 1)
                    val = "true|";
                else if (i == 0)
                    val = "false|" + desc + "|";
            }
            else
            {
                if (i == 0)
                    val = "false|";
                else if (i == 1)
                    val = "true|" + desc + "|";
            }
            return val;
        }

        public ActionResult SKUEditDone(string POCODE)
        {
            ViewBag.POCODE = POCODE;
            return View();
        }

        public ActionResult SKUPOEdit(string POCode)
        {
            SKUPOEditViewModel ViewModel = new SKUPOEditViewModel();

            if (POCode != null)
            {
                string Code = POCode;
                DataTable DT = OS.GetPODetails(Code);
                List<webx_GENERAL_POASSET_HDR> POASSET = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_HDR>(DT);
                ViewModel.GPH = POASSET.FirstOrDefault();
                ViewBag.MatCat = ViewModel.GPH.MatCat;

                if (ViewModel.GPH.FabricSheetPO_YN == "N")
                {
                    ViewBag.IsFabricSheet = "N";
                }
                else
                {
                    ViewBag.IsFabricSheet = "Y";
                }

                DataTable DT1 = OS.GetPODetDetails(Code);
                List<webx_GENERAL_POASSET_det> PODET = DataRowToObject.CreateListFromTable<webx_GENERAL_POASSET_det>(DT1);
                ViewModel.POdetList = PODET;
                ViewModel.GPD = PODET.FirstOrDefault();

                ViewModel.WGPH = DataRowToObject.CreateListFromTable<Tempwebx_GENERAL_POASSET_HDR>(DT).FirstOrDefault();

            }
            return View(ViewModel.WGPH);
        }

        public ActionResult SubmitSKUPOEdit(Tempwebx_GENERAL_POASSET_HDR Obj, List<webx_GENERAL_POASSET_det> GENERAL_POASSET_det)
        {
            string POCODE = "";
            string[] VendorName_Arr;

            VendorName_Arr = Obj.VENDORCD.Split('~');

            try
            {
                string Xml_DET_Details = "<root>";
                foreach (var item in GENERAL_POASSET_det)
                {
                    string assetcd = item.assetcd;
                    string[] Asset = assetcd.Split('~');

                    Xml_DET_Details = Xml_DET_Details + "<POASSETDET>";
                    Xml_DET_Details = Xml_DET_Details + "<assetcd>" + Asset[1] + "</assetcd>";
                    Xml_DET_Details = Xml_DET_Details + "<narration>" + item.narration + "</narration>";
                    Xml_DET_Details = Xml_DET_Details + "<qty>" + item.qty + "</qty>";
                    Xml_DET_Details = Xml_DET_Details + "<UnitType>" + item.UnitType + "</UnitType>";
                    Xml_DET_Details = Xml_DET_Details + "<rate>" + item.rate + "</rate>";
                    Xml_DET_Details = Xml_DET_Details + "<total>" + item.total + "</total>";
                    Xml_DET_Details = Xml_DET_Details + "<WarrantyInYear>" + item.WarrantyInYear + "</WarrantyInYear>";
                    Xml_DET_Details = Xml_DET_Details + "</POASSETDET>";

                }

                Xml_DET_Details = Xml_DET_Details + "</root>";
                string Xml_HDR_Details = "<root><Other>";
                Xml_HDR_Details = Xml_HDR_Details + "<MatCat>" + Obj.MatCat + "</MatCat>";
                Xml_HDR_Details = Xml_HDR_Details + "<pocode>" + Obj.pocode + "</pocode>";
                Xml_HDR_Details = Xml_HDR_Details + "<podate>" + Convert.ToDateTime(Obj.podate).ToString("dd MMM yyyy") + "</podate>";
                Xml_HDR_Details = Xml_HDR_Details + "<VENDORCD>" + VendorName_Arr[1] + "</VENDORCD>";
                Xml_HDR_Details = Xml_HDR_Details + "<SpotDelDT>" + Convert.ToDateTime(Obj.SpotDelDT).ToString("dd MMM yyyy") + "</SpotDelDT>";
                Xml_HDR_Details = Xml_HDR_Details + "<Manual_PO_No>" + Obj.Manual_PO_No + "</Manual_PO_No>";
                Xml_HDR_Details = Xml_HDR_Details + "<attn>" + Obj.attn + "</attn>";
                Xml_HDR_Details = Xml_HDR_Details + "<Location>" + Obj.Location + "</Location>";
                Xml_HDR_Details = Xml_HDR_Details + "<ValideDate>" + Convert.ToDateTime(Obj.ValideDate).ToString("dd MMM yyyy") + "</ValideDate>";
                Xml_HDR_Details = Xml_HDR_Details + "<FabricSheetPO_YN>" + Obj.FabricSheetPO_YN + "</FabricSheetPO_YN>";
                Xml_HDR_Details = Xml_HDR_Details + "<totalamt>" + Obj.totalamt + "</totalamt>";
                //Xml_HDR_Details = Xml_HDR_Details + "<terms_condition>" + Obj.terms_condition + "</terms_condition>";
                Xml_HDR_Details = Xml_HDR_Details + "<Excise_Duty>" + Obj.Excise_Duty + "</Excise_Duty>";
                Xml_HDR_Details = Xml_HDR_Details + "<Vat_S_Tax>" + Obj.Vat_S_Tax + "</Vat_S_Tax>";
                //Xml_HDR_Details = Xml_HDR_Details + "<remarks>" + Obj.remarks + "</remarks>";
                Xml_HDR_Details = Xml_HDR_Details + "<Freight_Ins>" + Obj.Freight_Ins + "</Freight_Ins>";
                Xml_HDR_Details = Xml_HDR_Details + "<Octroi>" + Obj.Octroi + "</Octroi>";
                //Xml_HDR_Details = Xml_HDR_Details + "<Specification>" + Obj.Specification + "</Specification>";
                Xml_HDR_Details = Xml_HDR_Details + "<Packing_charge>" + Obj.Packing_charge + "</Packing_charge>";
                Xml_HDR_Details = Xml_HDR_Details + "<PAIDAMT>" + Obj.PAIDAMT + "</PAIDAMT>";
                Xml_HDR_Details = Xml_HDR_Details + "<pendamt>" + Obj.pendamt + "</pendamt>";
                //Xml_HDR_Details = Xml_HDR_Details + "<PaymentTerms>" + Obj.PaymentTerms + "</PaymentTerms>";
                Xml_HDR_Details = Xml_HDR_Details + "</Other></root>";
                if (Obj.terms_condition == null)
                {
                    Obj.terms_condition = "";
                }
                if (Obj.remarks == null)
                {
                    Obj.remarks = "";
                }
                if (Obj.Specification == null)
                {
                    Obj.Specification = "";
                }
                if (Obj.PaymentTerms == null)
                {
                    Obj.PaymentTerms = "";
                }

                DataTable DT = new DataTable();
                DT = OS.GetSKUPOUpdate(Xml_HDR_Details, Xml_DET_Details, BaseUserName.ToString(), Convert.ToString(Obj.terms_condition).Replace("'", "''"), Convert.ToString(Obj.remarks).Replace("'", "''"), Convert.ToString(Obj.Specification).Replace("'", "''"), Convert.ToString(Obj.PaymentTerms).Replace("'", "''"));

                POCODE = DT.Rows[0]["Column1"].ToString();
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("SKUEditDone", new { POCODE = POCODE });
        }

        #endregion

        #region BankingOpertaion

        public ActionResult ChequeBounce()
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName == "ADMIN")
            //{
            ChequeViewModel objChequeViewModel = new ChequeViewModel();
            Cheque_det ChequeDetail = new Cheque_det();
            objChequeViewModel.ChequeDetail = ChequeDetail;
            List<Cheque_det> ListChequeDetail = new List<Cheque_det>();
            List<Cheque_det> ListChequeBounceDocument = new List<Cheque_det>();
            List<Cheque_det> ListChequeDepositDetail = new List<Cheque_det>();
            objChequeViewModel.ListChequeDetail = ListChequeDetail;
            objChequeViewModel.ListChequeBounceDocument = ListChequeBounceDocument;
            objChequeViewModel.ListChequeDepositDetail = ListChequeDepositDetail;
            return View("ChequeBounce", objChequeViewModel);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        public ActionResult ChequeReoffer()
        {
            ChequeViewModel objChequeViewModel = new ChequeViewModel();
            Cheque_det ChequeDetail = new Cheque_det();
            objChequeViewModel.ChequeDetail = ChequeDetail;

            return View("ChequeReoffer", objChequeViewModel);
        }

        public ActionResult GetChequeDetail(string ChqueNo, string ChqueDate)
        {
            ChequeViewModel objCheque = new ChequeViewModel();
            List<Cheque_det> ListChequeBounceDocument = new List<Cheque_det>();
            List<Cheque_det> ListChequeDetail = new List<Cheque_det>();
            List<Cheque_det> ListChequeDepositDetail = new List<Cheque_det>();

            ListChequeDetail = OS.ListChqDetail(ChqueNo, ChqueDate).ToList();
            if (ListChequeDetail.Count > 0)
            {
                int AutoId = 0;
                foreach (var item in ListChequeDetail)
                {
                    AutoId++;
                    item.Id = AutoId;
                }
            }
            objCheque.ListChequeDetail = ListChequeDetail;

            ListChequeBounceDocument = OS.ListChqBounceDocument(ChqueNo, ChqueDate).ToList();
            if (ListChequeBounceDocument.Count > 0)
            {
                int AutoId = 0;
                foreach (var item in ListChequeBounceDocument)
                {
                    AutoId++;
                    item.Id = AutoId;
                }
            }
            objCheque.ListChequeBounceDocument = ListChequeBounceDocument;

            ListChequeDepositDetail = OS.ListChqDeposit(ChqueNo, ChqueDate, BaseYearVal).ToList();
            if (ListChequeDepositDetail.Count > 0)
            {
                int AutoId = 0;
                foreach (var item in ListChequeDepositDetail)
                {
                    AutoId++;
                    item.Id = AutoId;
                }
            }
            objCheque.ListChequeDepositDetail = ListChequeDepositDetail;
            return PartialView("_Partial_Cheque", objCheque);
        }

        public ActionResult InsertCheckBounceDetail(ChequeViewModel chequeVM)
        {
            try
            {
                string str = OS.Chq_Bounce_Updation(chequeVM.ChequeDetail.chqno, chequeVM.ChequeDetail.chqdt, GF.FormateDate(chequeVM.ChequeDetail.chq_bounce_dt), BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                if (str == "Sucess")
                {
                    return RedirectToAction("CheckBounceDone", new { ChqueNo = chequeVM.ChequeDetail.chqno, ChqueDate = chequeVM.ChequeDetail.chqdt, ChqueBounceDate = GF.FormateDate(chequeVM.ChequeDetail.chq_bounce_dt), Flag = "Bounce" });
                }
                else
                {
                    ViewBag.StrError = str;
                    return View("Error");
                }
                //string str = OS.Chq_Bounce_Updation1(ChqueNo, ChqueDate, ChqueBounceDate, BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                //string str1 = OS.Chq_Bounce_Updation2(ChqueNo, ChqueDate, ChqueBounceDate, BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                //string str2 = OS.Chq_Bounce_Updation3(ChqueNo, ChqueDate, ChqueBounceDate, BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                //string str3 = OS.USP_Chq_Bounce_Reverese_Accounting1(ChqueNo, ChqueDate, ChqueBounceDate, BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                //string str4 = OS.USP_Chq_Bounce_Reverese_Accounting2(ChqueNo, ChqueDate, ChqueBounceDate, BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
                //return View("CheckBounceDone");
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
        }

        public ActionResult CheckBounceDone(string ChqueNo, string ChqueDate, string ChqueBounceDate, string Flag)
        {
            ViewBag.ChqueNo = ChqueNo;
            ViewBag.ChqueDate = ChqueDate;
            ViewBag.ChqueBounceDate = ChqueBounceDate;
            ViewBag.Flag = Flag;
            return View();
        }

        public string CheckValid(string ChqueName, string ChqueDate)
        {
            string CHQ_B_R = "", chq_status = "", Status = "", Flag = "", Availabe_FOR_USE_YN = "U", depoflag = "";

            DataTable dt = OS.GetChequeDetail(ChqueName, ChqueDate);
            int i = 0;
            if (dt.Rows.Count > 0)
            {
                i = i + 1;
                CHQ_B_R = dt.Rows[0]["CHQ_B_R"].ToString();
                chq_status = dt.Rows[0]["chq_status"].ToString(); ;
                depoflag = dt.Rows[0]["depoflag"].ToString();
            }
            if (i == 1)
            {
                DataTable dtBounce = OS.checkBounceDetail(ChqueName, ChqueDate);

                if (dtBounce.Rows.Count > 0)
                {
                    Availabe_FOR_USE_YN = dtBounce.Rows[0]["Availabe_FOR_USE_YN"].ToString();

                }
            }
            if (i == 0)
            {
                Status = "Cheque Is Not Present In System";
                Flag = "F";
            }
            else if (i == 1)
            {
                if (CHQ_B_R == "B")
                {
                    Status = "Cheque Is Already Bounced";
                    Flag = "F";
                }
                else if (depoflag == "N")
                {
                    Status = "Cheque Is Not Deposited";
                    Flag = "F";
                }
                else if (Availabe_FOR_USE_YN != "U")
                {
                    Status = "Cheque Is Reoffred But Not Used For Collection";
                    Flag = "F";
                }

                else
                {
                    if (Flag == "F")
                        return Status;
                    else
                        Status = "Y";

                }
            }
            return Status;
        }

        public string CheckReoffer(string ChqueName, string ChqueDate)
        {
            string CHQ_B_R = "", chq_status = "", Status = "", Flag = "", Availabe_FOR_USE_YN = "U", depoflag = "";


            DataTable dt = OS.GetChequeDetail(ChqueName, ChqueDate);
            int i = 0;
            if (dt.Rows.Count > 0)
            {
                i = i + 1;
                CHQ_B_R = dt.Rows[0]["CHQ_B_R"].ToString();
                chq_status = dt.Rows[0]["chq_status"].ToString(); ;
                depoflag = dt.Rows[0]["depoflag"].ToString();
            }
            if (i == 1)
            {
                DataTable dtBounce = OS.checkBounceDetail(ChqueName, ChqueDate);

                if (dtBounce.Rows.Count > 0)
                {
                    Availabe_FOR_USE_YN = dtBounce.Rows[0]["Availabe_FOR_USE_YN"].ToString();

                }
            }
            if (i == 0)
            {
                Status = "Cheque Is Not Present In System";
                Flag = "F";
            }
            else if (i == 1)
            {
                if (CHQ_B_R == "R")
                {
                    Status = "Cheque Is Already Reofferd/Used Not Bounced";
                    Flag = "F";
                }
                else
                {
                    if (Flag == "F")
                        return Status;
                    else
                        Status = "Y";

                }
            }
            return Status;
        }

        public ActionResult InsertCheckReofferdetail(string ChqueNo, string ChqueDate, string Flag, ChequeViewModel obj)
        {
            string str = OS.Chq_Reoffer_Updation1(ChqueNo, ChqueDate, GF.FormateDate(System.DateTime.Now), BaseUserName, BaseFinYear.Substring(0, 4), BaseYearVal);
            ViewBag.ChqueNo = ChqueNo;
            ViewBag.ChqueDate = ChqueDate;
            ViewBag.Flag = Flag;
            return View("CheckBounceDone");
            //return RedirectToAction("CheckBounceDone", new { ChqueNo = ChqueNo, ChqueDate = ChqueDate, ChqueBounceDate = "", Flag = Flag });
        }

        #endregion

        #region Bank ReConcillation

        public ActionResult BankReconlicationModule()
        {
            Nullable<decimal> loclevel = 0;
            string LOc_type = "", LocationCode = "", RO = "", LO = "";
            Webx_Master_GeneralViewModel objMasterViewModel = new Webx_Master_GeneralViewModel();
            webx_location objwebxlocation = new webx_location();
            Webx_Master_General objgenearl = new Webx_Master_General();
            objwebxlocation = MS.GetLocationDetails().Where(c => c.LocCode == BaseLocationCode).FirstOrDefault();
            List<webx_location> WexlocationROList = new List<webx_location>();
            List<webx_location> WexlocationAOList = new List<webx_location>();
            List<webx_location> WexlocationList = new List<webx_location>();
            List<webx_location> WLocationBankList = new List<webx_location>();

            ViewBag.RO = "block";
            ViewBag.AO = "block";
            ViewBag.LO = "block";
            loclevel = objwebxlocation.Loc_Level;
            if (objwebxlocation.Loc_Level >= 1)
            {
                if (objwebxlocation.Loc_Level == 1)
                {
                    WexlocationROList = OS.BankLocationList("").ToList();
                }
                else
                {
                    WexlocationROList = OS.BankLocationList(BaseLocationCode).ToList();
                }

                objwebxlocation.LocCode = "All";
                objwebxlocation.LocName = "All";
                WexlocationAOList.Insert(0, objwebxlocation);
                if (objwebxlocation.Loc_Level <= 2)
                {
                    objwebxlocation.LocCode = "All";
                    objwebxlocation.LocName = "All";
                    WexlocationList.Insert(0, objwebxlocation);

                }

            }
            if (objwebxlocation.Loc_Level >= 2)
            {
                if (objwebxlocation.Loc_Level != 2)
                {
                    ViewBag.RO = "none";
                }
                if (objwebxlocation.Loc_Level == 2)
                {
                    WexlocationAOList = OS.BankLocationList("").Where(c => c.Loc_Level == 3).OrderBy(c => c.LocName).ToList();
                    objwebxlocation.LocCode = "All";
                    objwebxlocation.LocName = "All";
                    WexlocationAOList.Insert(0, objwebxlocation);

                }
                else
                {
                    WexlocationAOList = OS.BankLocationList(BaseLocationCode).ToList();
                }

            }
            if (objwebxlocation.Loc_Level <= 3)
            {
                if (objwebxlocation.Loc_Level != 1)
                {
                    ViewBag.LO = "block";
                    if (objwebxlocation.Loc_Level == 3)
                    {
                        WexlocationList = OS.BankLocationList(BaseLocationCode).ToList();
                    }
                    if (objwebxlocation.Loc_Level == 2)
                    {
                        WexlocationList = MS.GetLocationDetails().Where(c => c.Report_Loc == BaseLocationCode).ToList();
                    }
                }

            }

            objwebxlocation = WexlocationROList.FirstOrDefault();
            RO = objwebxlocation.LocCode;
            objwebxlocation = WexlocationList.FirstOrDefault();
            LO = objwebxlocation.LocCode;

            if (RO == "All" && LO == "All")
            {
                LOc_type = "";
                LocationCode = "";
            }
            else if (RO != "All" && LO == "All")
            {
                LOc_type = "RO";
                objwebxlocation = WexlocationROList.FirstOrDefault();
                LocationCode = objwebxlocation.LocCode;
            }
            if (RO != "All" && LO != "All")
            {
                LOc_type = "LO";
                objwebxlocation = WexlocationList.FirstOrDefault();
                LocationCode = objwebxlocation.LocCode;
            }
            WLocationBankList = OS.LocationROWiseBANKList(LOc_type, LocationCode);
            ViewBag.AO = "none";
            objMasterViewModel.WLocationList = WexlocationList;
            objMasterViewModel.WLocationROList = WexlocationROList;
            objMasterViewModel.WLocationAOList = WexlocationAOList;
            objMasterViewModel.WLocationBankList = WLocationBankList;
            objgenearl.CodeType = LOc_type;
            objMasterViewModel.WMG = objgenearl;
            objMasterViewModel.LocLevel = loclevel;
            objMasterViewModel.rdioshow = true;
            return View("BankReconlicationModule", objMasterViewModel);
        }

        public ActionResult BankReconlicationReport()
        {
            Nullable<decimal> loclevel = 0;
            string LOc_type = "", LocationCode = "", RO = "", LO = "";
            Webx_Master_GeneralViewModel objMasterViewModel = new Webx_Master_GeneralViewModel();
            webx_location objwebxlocation = new webx_location();
            Webx_Master_General objgenearl = new Webx_Master_General();
            objwebxlocation = MS.GetLocationDetails().Where(c => c.LocCode == BaseLocationCode).FirstOrDefault();
            List<webx_location> WexlocationROList = new List<webx_location>();
            List<webx_location> WexlocationAOList = new List<webx_location>();
            List<webx_location> WexlocationList = new List<webx_location>();
            List<webx_location> WLocationBankList = new List<webx_location>();

            ViewBag.RO = "block";
            ViewBag.AO = "block";
            ViewBag.LO = "block";
            loclevel = objwebxlocation.Loc_Level;
            if (objwebxlocation.Loc_Level >= 1)
            {
                if (objwebxlocation.Loc_Level == 1)
                {
                    WexlocationROList = OS.BankLocationList("").ToList();
                }
                else
                {
                    WexlocationROList = OS.BankLocationList(BaseLocationCode).ToList();
                }

                objwebxlocation.LocCode = "All";
                objwebxlocation.LocName = "All";
                WexlocationAOList.Insert(0, objwebxlocation);
                if (objwebxlocation.Loc_Level <= 2)
                {
                    objwebxlocation.LocCode = "All";
                    objwebxlocation.LocName = "All";
                    WexlocationList.Insert(0, objwebxlocation);
                }
            }
            if (objwebxlocation.Loc_Level >= 2)
            {
                if (objwebxlocation.Loc_Level != 2)
                {
                    ViewBag.RO = "none";
                }
                if (objwebxlocation.Loc_Level == 2)
                {
                    WexlocationAOList = OS.BankLocationList("").Where(c => c.Loc_Level == 3).OrderBy(c => c.LocName).ToList();
                    objwebxlocation.LocCode = "All";
                    objwebxlocation.LocName = "All";
                    WexlocationAOList.Insert(0, objwebxlocation);
                }
                else
                {
                    WexlocationAOList = OS.BankLocationList(BaseLocationCode).ToList();
                }
            }
            if (objwebxlocation.Loc_Level <= 3)
            {
                if (objwebxlocation.Loc_Level != 1)
                {
                    ViewBag.LO = "block";
                    if (objwebxlocation.Loc_Level == 3)
                    {
                        WexlocationList = OS.BankLocationList(BaseLocationCode).ToList();
                    }
                    if (objwebxlocation.Loc_Level == 2)
                    {
                        WexlocationList = MS.GetLocationDetails().Where(c => c.Report_Loc == BaseLocationCode).ToList();
                    }
                }
            }

            objwebxlocation = WexlocationROList.FirstOrDefault();
            RO = objwebxlocation.LocCode;
            objwebxlocation = WexlocationList.FirstOrDefault();
            LO = objwebxlocation.LocCode;

            if (RO == "All" && LO == "All")
            {
                LOc_type = "";
                LocationCode = "";
            }
            else if (RO != "All" && LO == "All")
            {
                LOc_type = "RO";
                objwebxlocation = WexlocationROList.FirstOrDefault();
                LocationCode = objwebxlocation.LocCode;
            }
            if (RO != "All" && LO != "All")
            {
                LOc_type = "LO";
                objwebxlocation = WexlocationList.FirstOrDefault();
                LocationCode = objwebxlocation.LocCode;
            }
            WLocationBankList = OS.LocationROWiseBANKList(LOc_type, LocationCode);
            ViewBag.AO = "none";
            objMasterViewModel.WLocationList = WexlocationList;
            objMasterViewModel.WLocationROList = WexlocationROList;
            objMasterViewModel.WLocationAOList = WexlocationAOList;
            objMasterViewModel.WLocationBankList = WLocationBankList;
            objgenearl.CodeType = LOc_type;
            objMasterViewModel.WMG = objgenearl;
            objMasterViewModel.LocLevel = loclevel;
            objMasterViewModel.rdioshow = false;
            return View("BankReconlicationReport", objMasterViewModel);
        }

        public ActionResult BankReconlicationViewPrint(Webx_Master_GeneralViewModel objMasterViewModel)
        {
            string BankCode = objMasterViewModel.Bank;
            string FromDate = GF.FormateDate(objMasterViewModel.FromDate);
            string ToDate = GF.FormateDate(objMasterViewModel.ToDate);
            string Type = objMasterViewModel.rdio;
            string LO = objMasterViewModel.LO;
            string RO = objMasterViewModel.RO;
            ViewBag.Comapny = BaseCompanyCode;
            //   string ChequeNo = objMasterViewModel.ChequeNo;
            ViewBag.Finyear = BaseFinYear.Split('-')[0].ToString();
            return View(objMasterViewModel);
        }


        //public ActionResult BankReconlicationViewPrint(Webx_Master_GeneralViewModel CMGVM)
        //{
        //    ViewBag.company = BaseCompanyCode;
        //    ViewBag.transTable = "webx_acctrans__" + BaseYearVal;
        //    return View(CMGVM);
        //}


        public JsonResult GetLocationWiseBankList(string Loctype, string brcdCode)
        {
            List<webx_location> WLocationBankList = new List<webx_location>();
            if (brcdCode == "All")
            {
                brcdCode = "HQTR";
            }

            WLocationBankList = OS.LocationROWiseBANKList(Loctype, brcdCode);

            var SearchList = (from e in WLocationBankList
                              select new
                              {
                                  Value = e.Acccode,
                                  Text = e.Accdesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetRoWiseLocationList(int LocLeval, string Ro)
        {
            List<webx_location> WLocationBankList = new List<webx_location>();
            if (LocLeval <= 2)
            {
                WLocationBankList = OS.RoLocationList(Ro).ToList();
            }


            var SearchList = (from e in WLocationBankList
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddBankReconlicationModule(Webx_Master_GeneralViewModel objViewModel)
        {

            return RedirectToAction("BankReModuleResult", new { RO = objViewModel.RO, LO = objViewModel.LO, Account = objViewModel.Bank, TranType = objViewModel.rdio, FromDate = GF.FormateDateWithFullYear(objViewModel.FromDate), ToDate = GF.FormateDateWithFullYear(objViewModel.ToDate) });
        }

        public ActionResult BankReModuleResult(string RO, string LO, string Account, string FromDate, string ToDate, string TranType)
        {
            string Bankcode = Account;
            string openTable = "", transTable = "", DR_CR_type = "";
            double Total_CR_amt = 0, Total_DR_amt = 0, CR_Amount = 0, DR_Amount = 0;
            double open_balance = 0.00;
            string DRCR = "", DRCR1 = "";



            openTable = "webx_acctopening_" + BaseYearVal;
            transTable = "webx_acctrans_" + BaseYearVal;

            WebxAcctransViewModel AcctranViewModel = new WebxAcctransViewModel();
            List<webx_acctrans_Model> ListAcconttranCreadit = new List<webx_acctrans_Model>();
            List<webx_acctrans_Model> ListAcconttranDebit = new List<webx_acctrans_Model>();
            webx_acctrans_Model ObjAcconttran = new webx_acctrans_Model();

            AcctranViewModel.Bankacccode = Bankcode;
            ViewBag.RO = RO;
            ViewBag.LO = LO;
            if (Account != null)
            {
                ViewBag.BankCode = OS.getAccountDesc("webx_acctinfo", Bankcode);
            }
            ViewBag.Duration = FromDate + "-" + ToDate;
            DataTable dtTranjaction = OS.gettranjaction(transTable, Bankcode, FromDate, LO, RO, BaseCompanyCode);
            DataTable dtOpening = OS.getopening(openTable, Bankcode, LO, RO, BaseCompanyCode);
            DataTable dtTotalTransactional = OS.getTotal_transactional(transTable, Bankcode, FromDate, ToDate, LO, RO, BaseCompanyCode);

            string all_tranDr = "", all_tranCr = "", all_openDr = "", all_openCr = "", Trnas_Dr = "0", Trans_Cr = "0";
            double transactionCr = 0, transactionDr = 0, openCr = 0, openDr = 0, total_Debit = 0, total_Crebit = 0, Total_Dr = 0, Total_Cr = 0;
            //dtTranjaction
            all_tranDr = dtTranjaction.Rows[0]["Debit"].ToString();
            all_tranCr = dtTranjaction.Rows[0]["Credit"].ToString();



            if (Convert.ToDouble(all_tranDr) < Convert.ToDouble(all_tranCr))
            {
                transactionCr = Convert.ToDouble(all_tranCr) - Convert.ToDouble(all_tranDr);
            }
            else if (Convert.ToDouble(all_tranDr) > Convert.ToDouble(all_tranCr))
            {
                transactionDr = Convert.ToDouble(all_tranDr) - Convert.ToDouble(all_tranCr);
            }
            //dtOpening
            all_openDr = dtOpening.Rows[0]["opendebit_i"].ToString();
            all_openCr = dtOpening.Rows[0]["opencredit_i"].ToString();

            if (Convert.ToDouble(all_openDr) < Convert.ToDouble(all_openCr))
            {
                openCr = Convert.ToDouble(all_openCr) - Convert.ToDouble(all_openDr);
            }
            else if (Convert.ToDouble(all_openDr) > Convert.ToDouble(all_openCr))
            {
                openDr = Convert.ToDouble(all_openDr) - Convert.ToDouble(all_openCr);
            }

            //dtTotalTransactional
            Trnas_Dr = dtTotalTransactional.Rows[0]["Debit"].ToString();
            Trans_Cr = dtTotalTransactional.Rows[0]["Credit"].ToString();

            Total_Dr = 0;
            Total_Cr = 0;
            if (Convert.ToDouble(Trnas_Dr) < Convert.ToDouble(Trans_Cr))
            {
                Total_Cr = Convert.ToDouble(Trans_Cr) - Convert.ToDouble(Trnas_Dr);
            }
            else if (Convert.ToDouble(Trnas_Dr) > Convert.ToDouble(Trans_Cr))
            {
                Total_Dr = Convert.ToDouble(Trnas_Dr) - Convert.ToDouble(Trans_Cr);
            }
            //***********Transaction  Balance   from Date - to date  Open ********************

            total_Debit = transactionDr + openDr + Total_Dr;
            total_Crebit = transactionCr + openCr + Total_Cr;

            if (total_Debit > total_Crebit)
            {
                open_balance = (total_Debit - total_Crebit);
                DRCR = "DR";
                ViewBag.Chq_Issued = "Add : Cheques Issued but not presented ";
                ViewBag.Chq_Recived = "Less : Cheque Deposited But Not Cleared";
            }
            else if (total_Debit < total_Crebit)
            {
                open_balance = (total_Crebit - total_Debit);
                DRCR = "CR";
                ViewBag.Chq_Issued = "Less : Cheques Issued but not presented ";
                ViewBag.Chq_Recived = "Add : Cheque Deposited But Not Cleared";
            }
            string OpenBal = string.Format("{0:0.00}", open_balance).ToString() + DRCR;
            ViewBag.Balance_Amount = OpenBal;

            ListAcconttranCreadit = OS.GetAccountTranCreaditList(Bankcode, FromDate, ToDate, transTable, TranType, BaseCompanyCode).ToList();
            decimal TotalCreaditAmt = 0;
            int Id = 0;
            foreach (var item in ListAcconttranCreadit)
            {
                Id++;
                item.Id = Id;
                if (item.Chqcleardate.ToString("dd MMM yyyy") == "01 Jan 1900" || item.Chqcleardate.ToString("dd MMM yyyy") == "01 Jan 0001")
                {
                    // item.Chqcleardate = System.DateTime.Now;
                    item.Chqcleardate = item.Chqcleardate;

                    // item.ChqIssueddate = System.DateTime.Now;
                }
                else
                {
                    item.Chqcleardate = item.Chqcleardate;
                }

                if (item.ChqIssueddate.ToString("dd MMM yyyy") == "01 Jan 1900" || item.ChqIssueddate.ToString("dd MMM yyyy") == "01 Jan 0001")
                {
                    item.ChqIssueddate = System.DateTime.Now;
                    // item.ChqIssueddate = System.DateTime.Now;
                }
                else
                {
                    item.ChqIssueddate = item.ChqIssueddate;
                }


                TotalCreaditAmt += item.Credit;
            }
            Total_CR_amt = Convert.ToDouble(TotalCreaditAmt);
            if (DRCR == "DR")
            {
                CR_Amount = Total_CR_amt + open_balance;
                ViewBag.CR_AMT = CR_Amount.ToString("F2") + " DR";
                DRCR1 = "DR";

            }
            else
            {
                if (Total_CR_amt > open_balance)
                {
                    CR_Amount = Total_CR_amt - open_balance;
                    ViewBag.CR_AMT = CR_Amount.ToString("F2") + " DR";
                    DRCR1 = "DR";
                }
                else if (Total_CR_amt < open_balance)
                {
                    CR_Amount = Total_CR_amt - open_balance;
                    ViewBag.CR_AMT = CR_Amount.ToString("F2") + " CR";
                    DRCR1 = "CR";
                }
            }
            ViewBag.TotalCreaditAmt = TotalCreaditAmt;
            ListAcconttranDebit = OS.GetAccountTranDebitList(Bankcode, FromDate, ToDate, transTable, TranType, BaseCompanyCode).ToList();
            decimal FinalDebitAmt = 0;
            foreach (var item in ListAcconttranDebit)
            {
                if (item.Chqcleardate.ToString("dd MMM yyyy") == "01 Jan 1900" || item.Chqcleardate.ToString("dd MMM yyyy") == "01 Jan 0001")
                {
                    //item.Chqcleardate = System.DateTime.Now;
                    item.Chqcleardate = item.Chqcleardate;
                }
                else
                {
                    item.Chqcleardate = item.Chqcleardate;
                }

                if (item.ChqIssueddate.ToString("dd MMM yyyy") == "01 Jan 1900" || item.ChqIssueddate.ToString("dd MMM yyyy") == "01 Jan 0001")
                {
                    item.ChqIssueddate = System.DateTime.Now;
                    // item.ChqIssueddate = System.DateTime.Now;
                }
                else
                {
                    item.ChqIssueddate = item.ChqIssueddate;
                }

                FinalDebitAmt += item.Debit;
            }

            Total_DR_amt = Convert.ToDouble(FinalDebitAmt) + Total_DR_amt;
            if (DRCR == "DR")
            {
                if (Total_DR_amt > CR_Amount)
                {
                    DR_Amount = Total_DR_amt - CR_Amount;
                    DR_CR_type = "CR";
                }
                else
                {
                    DR_Amount = CR_Amount - Total_DR_amt;
                    DR_CR_type = "DR";
                }
            }
            else
            {
                DR_Amount = CR_Amount + Total_DR_amt;
                DR_CR_type = DRCR1;
            }

            ViewBag.Total_DR_amt = Total_DR_amt;
            ViewBag.DR_AMT = DR_Amount.ToString("F2") + " " + DR_CR_type;
            AcctranViewModel.ListAcconttranCreadit = ListAcconttranCreadit;
            AcctranViewModel.ListAcconttranDebit = ListAcconttranDebit;
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View("BankReModuleResult", AcctranViewModel);
        }

        //public ActionResult AddBankRecoResult(List<webx_acctrans_Model> BankCreaditList, List<webx_acctrans_Model> BankDebitList)
        //{
        //    string openTable = "webx_acctopening_" + BaseYearVal;
        //    string transTable = "webx_acctrans_" + BaseYearVal;
        //    if (BankCreaditList != null)
        //    {
        //        if (BankCreaditList.Count > 0 && BankCreaditList != null)
        //        {
        //            foreach (var item in BankCreaditList)
        //            {

        //                if (item.Chk == true)
        //                {
        //                    DataTable dt = OS.UpdateBankReModule(transTable, GF.FormateDate(item.Chqcleardate), item.comment, item.Srno, item.Chqno, item.Chqdate);
        //                    DataTable dt1 = OS.UpdateBankReModule_06_07(GF.FormateDate(item.Chqcleardate), item.comment, item.Chqno, item.Chqdate);
        //                    DataTable dt2 = OS.UpdateBankReModule_det(item.Chqno, item.Chqdate, GF.FormateDate(item.Chqcleardate));
        //                }
        //            }
        //        }
        //    }
        //    if (BankDebitList != null)
        //    {
        //        foreach (var item in BankDebitList)
        //        {
        //            if (item.Chk == true)
        //            {
        //                DataTable dt = OS.UpdateBankReModule(transTable, GF.FormateDate(item.Chqcleardate), item.comment, item.Srno, item.Chqno, item.Chqdate);
        //                DataTable dt2 = OS.UpdateBankReModule_det(item.Chqno, item.Chqdate, GF.FormateDate(item.Chqcleardate));
        //            }
        //        }
        //    }


        //    return RedirectToAction("BankReModuleResultDone");
        //}

        //public ActionResult AddBankRecoResult(List<webx_acctrans_Model> BankCreaditList, List<webx_acctrans_Model> BankDebitList)
        //{
        //    string FinYear = BaseYearValFirst.ToString();
        //    string FinYearNext = Convert.ToString(Convert.ToDouble(FinYear) + 1);
        //    string fin_year = FinYear.Substring(2, 2).ToString() + "_" + FinYearNext.Substring(2, 2).ToString();


        //    string openTable = "webx_acctopening_" + fin_year;
        //    string transTable = "webx_acctrans_" + fin_year;


        //    try
        //    {

        //        string Xml_BRC_Details = "<root>";
        //        if (BankCreaditList != null)
        //        {
        //            if (BankCreaditList.Count > 0 && BankCreaditList != null)
        //            {
        //                foreach (var item in BankCreaditList)
        //                {

        //                    if (item.Chk == true)
        //                    {

        //                        Xml_BRC_Details = Xml_BRC_Details + "<BRCDetail>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<comment>" + item.comment + "</comment>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Srno>" + item.Srno + "</Srno>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqno>" + item.Chqno + "</Chqno>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqdate>" + item.Chqdate + "</Chqdate>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "</BRCDetail>";
        //                    }
        //                }
        //                Xml_BRC_Details = Xml_BRC_Details + "</root>";
        //                DataTable DT = OS.UpdateBankReModuleSubmit(Xml_BRC_Details, fin_year);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        ViewBag.StrError = "Message:-" + ex.Message;
        //        Error_Logs(ControllerName, "UpdateBankReModuleSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
        //        return View("Error");
        //    }

        //    try
        //    {
        //        string Xml_BRC_Details = "<root>";
        //        if (BankDebitList != null)
        //        {
        //            if (BankDebitList.Count > 0 && BankDebitList != null)
        //            {
        //                foreach (var item in BankDebitList)
        //                {

        //                    if (item.Chk == true)
        //                    {
        //                        Xml_BRC_Details = Xml_BRC_Details + "<BRCDetail>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<comment>" + item.comment + "</comment>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Srno>" + item.Srno + "</Srno>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqno>" + item.Chqno + "</Chqno>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "<Chqdate>" + item.Chqdate + "</Chqdate>";
        //                        Xml_BRC_Details = Xml_BRC_Details + "</BRCDetail>";
        //                    }
        //                }
        //                Xml_BRC_Details = Xml_BRC_Details + "</root>";
        //                DataTable DT = OS.UpdateBankReModuleSubmit(Xml_BRC_Details, fin_year);
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        ViewBag.StrError = "Message:-" + ex.Message;
        //        Error_Logs(ControllerName, "UpdateBankReModuleSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
        //        return View("Error");
        //    }
        //    return RedirectToAction("BankReModuleResultDone");
        //}
        public ActionResult AddBankRecoResult(List<webx_acctrans_Model> BankCreaditList, List<webx_acctrans_Model> BankDebitList, WebxAcctransViewModel objViewModel)
        {
            string FinYear = BaseYearValFirst.ToString();
            string FinYearNext = Convert.ToString(Convert.ToDouble(FinYear) + 1);
            string fin_year = FinYear.Substring(2, 2).ToString() + "_" + FinYearNext.Substring(2, 2).ToString();


            string openTable = "webx_acctopening_" + fin_year;
            string transTable = "webx_acctrans_" + fin_year;
            string Xml_BRC_Details = "<root>";
            try
            {


                if (BankCreaditList != null)
                {
                    if (BankCreaditList.Count > 0 && BankCreaditList != null)
                    {
                        foreach (var item in BankCreaditList)
                        {

                            if (item.Chk == true)
                            {
                                if (GF.FormateDate(item.Chqcleardate) != "" && item.Flag == "N")
                                {
                                    Xml_BRC_Details += "<Cheque>";
                                    Xml_BRC_Details += "<Transdate>" + item.Transdate + "</Transdate>";
                                    Xml_BRC_Details += "<Voucherno>" + item.Voucherno + "</Voucherno>";
                                    Xml_BRC_Details += "<Acccode>" + objViewModel.Bankacccode + "</Acccode>";
                                    Xml_BRC_Details += "<Chqno>" + item.Chqno + "</Chqno>";
                                    Xml_BRC_Details += "<Chqdate>" + item.Chqdate + "</Chqdate>";
                                    Xml_BRC_Details += "<Debit>0.00</Debit>";
                                    Xml_BRC_Details += "<Credit>" + item.Credit + "</Credit>";
                                    Xml_BRC_Details += "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
                                    Xml_BRC_Details += "<comment>" + item.Narration + "</comment>";
                                    Xml_BRC_Details += "<comment1>" + item.comment + "</comment1>";
                                    Xml_BRC_Details += "<Transtype>" + item.transtype + "</Transtype>";
                                    Xml_BRC_Details += "<Entryby>" + BaseUserName + "</Entryby>";
                                    Xml_BRC_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                                    Xml_BRC_Details += "</Cheque>";
                                }
                                if (GF.FormateDate(item.Chqcleardate) != "" && item.Flag == "Y")
                                {
                                    Xml_BRC_Details += "<ChequeUpdate>";
                                    Xml_BRC_Details += "<Transdate>" + item.Transdate + "</Transdate>";
                                    Xml_BRC_Details += "<Voucherno>" + item.Voucherno + "</Voucherno>";
                                    Xml_BRC_Details += "<Acccode>" + objViewModel.Bankacccode + "</Acccode>";
                                    Xml_BRC_Details += "<Chqno>" + item.Chqno + "</Chqno>";
                                    Xml_BRC_Details += "<Chqdate>" + item.Chqdate + "</Chqdate>";
                                    Xml_BRC_Details += "<Debit>0.00</Debit>";
                                    Xml_BRC_Details += "<Credit>" + item.Credit + "</Credit>";
                                    Xml_BRC_Details += "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
                                    Xml_BRC_Details += "<comment>" + item.Narration + "</comment>";
                                    Xml_BRC_Details += "<comment1>" + item.comment + "</comment1>";
                                    Xml_BRC_Details += "<Transtype>" + item.transtype + "</Transtype>";
                                    Xml_BRC_Details += "<Entryby>" + BaseUserName + "</Entryby>";
                                    Xml_BRC_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                                    Xml_BRC_Details += "</ChequeUpdate>";
                                }

                            }
                        }

                    }
                }


                if (BankDebitList != null)
                {
                    if (BankDebitList.Count > 0 && BankDebitList != null)
                    {
                        foreach (var item in BankDebitList)
                        {

                            if (item.Chk == true)
                            {
                                if (GF.FormateDate(item.Chqcleardate) != "" && item.Flag == "N")
                                {
                                    Xml_BRC_Details += "<Cheque>";
                                    Xml_BRC_Details += "<Transdate>" + item.Transdate + "</Transdate>";
                                    Xml_BRC_Details += "<Voucherno>" + item.Voucherno + "</Voucherno>";
                                    Xml_BRC_Details += "<Acccode>" + objViewModel.Bankacccode + "</Acccode>";
                                    Xml_BRC_Details += "<Chqno>" + item.Chqno + "</Chqno>";
                                    Xml_BRC_Details += "<Chqdate>" + item.Chqdate + "</Chqdate>";
                                    Xml_BRC_Details += "<Debit>" + item.Credit + "</Debit>";
                                    Xml_BRC_Details += "<Credit>0.00</Credit>";
                                    Xml_BRC_Details += "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
                                    Xml_BRC_Details += "<comment>" + item.Narration + "</comment>";
                                    Xml_BRC_Details += "<comment1>" + item.comment + "</comment1>";
                                    Xml_BRC_Details += "<Transtype>" + item.transtype + "</Transtype>";
                                    Xml_BRC_Details += "<Entryby>" + BaseUserName + "</Entryby>";
                                    Xml_BRC_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                                    Xml_BRC_Details += "</Cheque>";
                                }
                                if (GF.FormateDate(item.Chqcleardate) != "" && item.Flag == "Y")
                                {
                                    Xml_BRC_Details += "<ChequeUpdate>";
                                    Xml_BRC_Details += "<Transdate>" + item.Transdate + "</Transdate>";
                                    Xml_BRC_Details += "<Voucherno>" + item.Voucherno + "</Voucherno>";
                                    Xml_BRC_Details += "<Acccode>" + objViewModel.Bankacccode + "</Acccode>";
                                    Xml_BRC_Details += "<Chqno>" + item.Chqno + "</Chqno>";
                                    Xml_BRC_Details += "<Chqdate>" + item.Chqdate + "</Chqdate>";
                                    Xml_BRC_Details += "<Debit>" + item.Credit + "</Debit>";
                                    Xml_BRC_Details += "<Credit>0.00</Credit>";
                                    Xml_BRC_Details += "<Chqcleardate>" + GF.FormateDate(item.Chqcleardate) + "</Chqcleardate>";
                                    Xml_BRC_Details += "<comment>" + item.Narration + "</comment>";
                                    Xml_BRC_Details += "<comment1>" + item.comment + "</comment1>";
                                    Xml_BRC_Details += "<Transtype>" + item.transtype + "</Transtype>";
                                    Xml_BRC_Details += "<Entryby>" + BaseUserName + "</Entryby>";
                                    Xml_BRC_Details += "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                                    Xml_BRC_Details += "</ChequeUpdate>";
                                }
                            }
                        }

                    }
                }

                Xml_BRC_Details = Xml_BRC_Details + "</root>";
                DataTable DT = OS.UpdateBankReModuleSubmit(Xml_BRC_Details.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), BaseYearValFirst);
            }

            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "UpdateBankReModuleSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
                return View("Error");
            }
            return RedirectToAction("BankReModuleResultDone");
        }
        public ActionResult BankReModuleResultDone()
        {
            return View("BankReModuleResultDone");
        }

        #endregion

        #region TDSReconlication

        public ActionResult TDSReconlication()
        {
            TDSRecolicationViewModel objTDSRecolication = new TDSRecolicationViewModel();
            return View("TDSReconlication", objTDSRecolication);
        }

        public ActionResult AddTDSReconlicationReport(TDSRecolicationViewModel objTDS)
        {

            return RedirectToAction("TDSReconlicationResult", new { FromDate = GF.FormateDate(objTDS.FromDate), ToDate = GF.FormateDate(objTDS.ToDate), VoucherNo = objTDS.VoucherNo, MRNo = objTDS.MRNo, CustomerNo = objTDS.CustomerNo });
        }

        public ActionResult TDSReconlicationResult(string FromDate, string ToDate, string VoucherNo, string MRNo, string CustomerNo)
        {
            TDSRecolicationViewModel objTDSViewModel = new TDSRecolicationViewModel();
            List<webx_acctrans_TDS_Model> ListAccountTranList = new List<webx_acctrans_TDS_Model>();
            webx_acctrans_TDS_Model obj = new webx_acctrans_TDS_Model();
            ListAccountTranList = OS.GetTDSReconlicationList(VoucherNo, MRNo, FromDate, ToDate, CustomerNo);

            //int id = 1;
            //foreach (var item in ListAccountTranList)
            //{
            //    item.Id = id;
            //}
            //obj.Id = 1;
            //ListAccountTranList.Add(obj);
            ViewBag.Date = FromDate + "-" + ToDate;
            ViewBag.VoucherNo = VoucherNo;
            ViewBag.MRNo = MRNo;
            ViewBag.CustomerNo = CustomerNo;
            objTDSViewModel.ListAccountTran = ListAccountTranList;
            return View("TDSReconlicationResult", objTDSViewModel);
        }

        public ActionResult AddTDSReconlication(List<webx_acctrans_TDS_Model> TDSList, IEnumerable<HttpPostedFileBase> fil)
        {
            string path = "", name = "", extension = "";
            //string recvdt = fn.Mydate1(txtRcvdt.Text.ToString());
            string newFName = "";
            SqlConnection Conn;
            Conn = new SqlConnection(Connstr);
            Conn.Open();
            SqlTransaction trans;
            trans = Conn.BeginTransaction();

            try
            {
                foreach (var item in TDSList)
                {
                    bool chkbox = item.Chk; //((HtmlInputCheckBox)gr.FindControl("chkbillno"));

                    //FileUpload upload_file ; ((FileUpload)gr.FindControl("fileUpload"));
                    string txtRcvdt = item.DOCDT; //((TextBox)gr.FindControl("txtRcvdt"));
                    if (chkbox)
                    {
                        string TdsSrno = item.DOCNO;
                        string strTdsno = TdsSrno.PadLeft(5, '0').ToString();
                        string Name = "file_" + item.SRNO;

                        try
                        {
                            if (Request.Files != null)
                            {

                                if (Request.Files[Name].ContentLength > 0)
                                {
                                    string FileName = Request.Files[Name].FileName;
                                    extension = System.IO.Path.GetExtension(Request.Files[Name].FileName);
                                    name = item.DOCNO.Replace("/", "_") + extension;
                                    path = string.Format("{0}/{1}", Server.MapPath("~/TDSDocuments"), name);
                                    string path1 = string.Format("{0}/{1}", Server.MapPath("~/TDSDocuments"), name);

                                    if (System.IO.File.Exists(path1))
                                        System.IO.File.Delete(path1);
                                    newFName = name;
                                    Request.Files[Name].SaveAs(path1);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //throw;
                        }
                        string sql = "UPDATE WEBX_TDS_HDR SET FORM16_RCV_STATUS='Y',FORM16_RCV_COPYNM='" + newFName + "',FORM16_RCV_DT='" + item.DOCDT1 + "' WHERE Srno='" + item.SRNO + "'";
                        SqlHelper.ExecuteNonQuery(trans, CommandType.Text, sql);
                    }
                }
                trans.Commit();
                Conn.Close();
            }
            catch (Exception e1)
            {
                Response.Write("<br><br><br><center><font class='blackboldfnt' color='red'><b>Error : " + e1.Message + "</b></font></center>");
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                trans.Rollback();

                ViewBag.StrError = ErrorMsg;
                return View("Error");
            }

            //  Response.Redirect("TDS_Reco_Done.aspx");
            //foreach (var item in TDSList)
            //{
            //    if (item.Chk == true)
            //    {
            //        DataTable Dt = OS.AddTDSReclocation("", item.DOCDT1, Convert.ToString(item.SRNO));
            //    }
            //}
            return RedirectToAction("TDSReconlicationDone");

            //foreach (var item in TDSList)
            //{
            //    if (item.Chk == true)
            //    {
            //        DataTable Dt = OS.AddTDSReclocation("", GF.FormateDate(item.DOCDT1), item.SRNO);
            //    }
            //}
            //return RedirectToAction("TDSReconlicationDone");
        }

        public ActionResult TDSReconlicationDone()
        {
            return View("TDSReconlicationDone");
        }

        #endregion

        #region Octroi_Bill_Finalization

        public ActionResult OctroiFinalizationCriteria()
        {
            OctroiFinalizationBillViewModel objOctFinalizationViewModel = new OctroiFinalizationBillViewModel();
            List<webx_location> WexlocationROList = new List<webx_location>();
            List<webx_location> WexlocationList = new List<webx_location>();
            List<OctroiFinalizationBill> ListBill = new List<OctroiFinalizationBill>();
            WexlocationROList = OS.BankLocationList("").ToList();
            objOctFinalizationViewModel.WLocationROList = WexlocationROList;
            objOctFinalizationViewModel.WLocationList = WexlocationList;
            objOctFinalizationViewModel.ListOctroiFinalizationBill = ListBill;
            return View("OctroiFinalizationCriteria", objOctFinalizationViewModel);
        }

        public ActionResult GetdOctroiBillFinalization(OctroiFinalizationBillViewModel objOctroiBill)
        {
            List<OctroiFinalizationBill> ListBill = new List<OctroiFinalizationBill>();
            ListBill = OS.GetOctrioFizaBillList(objOctroiBill.FromDate, objOctroiBill.ToDate, objOctroiBill.Vendor, objOctroiBill.OctroiBillNo, BaseFinYear.Substring(0, 4), "0", objOctroiBill.RO, objOctroiBill.LO, objOctroiBill.BillType);
            foreach (var item in ListBill)
            {
                item.Billtype = objOctroiBill.BillType;
            }
            return PartialView("_PartialOctroiFinalizationList", ListBill);
        }

        public ActionResult AddOctroiBillFinalization(OctroiFinalizationBillViewModel objOctroiBill, List<OctroiFinalizationBill> OctroiBillLoist)
        {
            string DocNo = "", OCTROI_DocNo = "";
            string Billtype = "";
            foreach (var item in OctroiBillLoist)
            {
                if (item.chk == true && item.Billtype.ToUpper() != "OCTROI")
                {
                    DataTable dt = OS.AddOctroiBill(item.Billtype, item.BillNo, BaseFinYear);
                    DocNo += item.BillNo + ",";
                }
                else if (item.chk == true && item.Billtype.ToUpper() == "OCTROI")
                {
                    OCTROI_DocNo += item.BillNo + ",";
                }
                Billtype = item.Billtype;
            }
            if (Billtype.ToUpper() == "OCTROI")
            {
                DataTable DT_Octroi = OS.AddOctroiBill(Billtype, OCTROI_DocNo, BaseFinYear);
                DocNo = OCTROI_DocNo;
            }
            return RedirectToAction("OctroiBillFinalizationDone", new { BillType = Billtype, OctroiBill = DocNo });
        }

        public ActionResult OctroiBillFinalizationDone(string BillType, string OctroiBill)
        {
            if (BillType == "1")
            {
                ViewBag.titleDone = "Following Details of Finalized Octroi Bill";
                ViewBag.Note = "Click Here to Finalized More Octroi Bill";
                ViewBag.OctroiBill = OctroiBill;
            }
            else
            {
                ViewBag.titleDone = "Following Details of Finalized Octroi Agent Bill";
                ViewBag.Note = "Click Here to Finalized More Octroi Agent Bill";
                ViewBag.OctroiBill = OctroiBill;
            }
            return View("OctroiBillFinalizationDone");

        }

        #endregion

        #region Docket Financial Edit

        public string GetCustomerName(string custcode)
        {
            string CustomerName = "";
            DataTable dt = CS.GetCustomerName(custcode);
            if (dt.Rows.Count > 0)
            {
                CustomerName = dt.Rows[0]["CustName"].ToString();
            }

            return CustomerName;
        }

        public ActionResult DocketEditCretria()
        {
            Docket objGC = new Docket();
            return View("DocketEditCretria", objGC);
        }

        //public string CheckEditDocket(Docket Dk)
        //{
        //    XmlDocument XDoc = new XmlDocument();
        //    string XML = "";
        //    XML = XML + "<Docket>";
        //    XML = XML + "<DockNo>" + Dk.DockNo + "</DockNo>";
        //    XML = XML + "<FinYear4d>" + BaseFinYear.Substring(0, 4) + "</FinYear4d>";
        //    XML = XML + "<CompanyCode>" + BaseCompanyCode + "</CompanyCode>";
        //    XML = XML + "<UserId>" + BaseUserName + "</UserId>";
        //    XML = XML + "</Docket>";


        //    string Status = OS.CheckEditDocketAvilable(XML);
        //    return Status;
        //}

        public JsonResult CheckEditDocket(string DocketNo)
        {
            XmlDocument XDoc = new XmlDocument();
            string XML = "";
            XML = XML + "<Docket>";
            XML = XML + "<DockNo>" + DocketNo + "</DockNo>";
            XML = XML + "<FinYear4d>" + BaseFinYear.Substring(0, 4) + "</FinYear4d>";
            XML = XML + "<CompanyCode>" + BaseCompanyCode + "</CompanyCode>";
            XML = XML + "<UserId>" + BaseUserName + "</UserId>";
            XML = XML + "<IsGSTApplied>1</IsGSTApplied>";
            XML = XML + "</Docket>";

            string Status = OS.CheckEditDocketAvilable(XML);
            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                    //  TRDays = fovchrg.TRDays,
                }
            };
        }

        public ActionResult DocketCretriaAdd(Docket Dk)
        {
            return RedirectToAction("DocketFinancialEdit", new { DocketNo = Dk.DockNo });
        }

        public ActionResult DocketFinancialEdit(string DocketNo)
        {
            DocketViewModel DVM = new DocketViewModel();
            WebX_Master_Docket_Charges WMDC = new WebX_Master_Docket_Charges();
            WebX_Master_Docket WMD = new WebX_Master_Docket();
            List<WebX_Master_Docket_Invoice> ListInVoice = new List<WebX_Master_Docket_Invoice>();
            List<QuickDocketBalancecharges> ListCharges = new List<QuickDocketBalancecharges>();

            DataSet DSDocket = GF.getdatasetfromQuery("EXEC USP_FinancialEditGetData '" + DocketNo + "','" + BaseUserName + "','" + BaseLocationCode + "','" + BaseCompanyCode + "'");
            ListInVoice = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Invoice>(DSDocket.Tables[0]);

            if (ListInVoice.Count > 0)
            {
                int AutoId = 0;
                foreach (var item in ListInVoice)
                {
                    AutoId++;
                    item.SrNo = AutoId;
                }
            }

            DVM.Delivered = DSDocket.Tables[1].Rows[0][0].ToString();
            DVM.PRSDONE = DSDocket.Tables[1].Rows[0][1].ToString();
            DVM.DRSDONE = DSDocket.Tables[1].Rows[0][2].ToString();
            WMD.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
            WMD.FlagStaxBifer = MS.GetRuleObject("STAX_BIFER").defaultvalue.IndexOf(BaseCompanyCode) >= 0 ? "Y" : "N";
            WMD.FLAG_PRORATA = MS.GetRuleObject("FLAG_PRORATA").defaultvalue;
            /*Fill Docket*/
            WMD.DOCKNO = DocketNo;

            WMD = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(DSDocket.Tables[2]).FirstOrDefault();
            DVM.WMD = WMD;

            WMDC = new WebX_Master_Docket_Charges();

            if (ListInVoice.Count() == 0)
            {
                WebX_Master_Docket_Invoice CMDI = new WebX_Master_Docket_Invoice();
                CMDI.SrNo = 1;
                CMDI.INVDT = System.DateTime.Now;
                CMDI.DECLVAL = 0;
                CMDI.PKGSNO = DVM.WMD.PKGSNO;
                CMDI.ACTUWT = DVM.WMD.CHRGWT;
                CMDI.VOL_L = 0;
                CMDI.VOL_B = 0;
                CMDI.VOL_H = 0;
                ListInVoice.Add(CMDI);
                CMDI = new WebX_Master_Docket_Invoice();
            }
            DVM.ListInVoice = ListInVoice;

            WMDC = OS.GetFinEditStep4DataTable(DSDocket.Tables[3], DSDocket.Tables[4]).FirstOrDefault();
            DVM.ListInVoice = ListInVoice;

            DVM.WMDC = WMDC;

            ListCharges = OS.GetDocketChargesDetails(DocketNo);
            DVM.ListCharges = ListCharges;

            //START GST Changes Chirag D 
            string ChargesStr = "EXEC USP_GetDynamicChargesNew 'DKT'";
            DataTable Dt = GF.GetDataTableFromSP(ChargesStr);
            List<CygnusChargesHeader> GetCCHList = DataRowToObject.CreateListFromTable<CygnusChargesHeader>(Dt);
            DVM.ListCCH = GetCCHList;
            //END GST Changes Chirag D

            return View("DocketFinancialEdit", DVM);
        }

        public ActionResult AddDocket(DocketViewModel DVM, List<WebX_Master_Docket_Invoice> DocketInvoiceList, List<QuickDocketBalancecharges> DocketChargesList,
            List<webx_Master_DOCKET_DOCUMENT> ListDocument, List<BarCodeSerial> ListBarCode, string DOCTYP, List<CygnusChargesHeader> DynamicList)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            int id = 0;
            /* START GST Changes By Chirag D  */

            #region GST Charges

            //if (DVM.WMD.DOCKDT >= Convert.ToDateTime("26 June 2017"))
            //{
            //    if (DVM.WMD.PAYBAS == "P01")//Paid
            //    {
            //        DVM.WMD.fincmplbr = DVM.WMD.ORGNCD;
            //    }
            //    else if (DVM.WMD.PAYBAS == "P02")//TBB
            //    {
            //        DVM.WMD.fincmplbr = DVM.WMD.ORGNCD;
            //    }
            //    else if (DVM.WMD.PAYBAS == "P03")//To Pay
            //    {
            //        DVM.WMD.fincmplbr = DVM.WMD.DESTCD;
            //    }
            //    else if (DVM.WMD.PAYBAS == "P04")//FOC
            //    {
            //    }
            //}

            XmlDocument xmlDocCha = new XmlDocument();
            XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());

            using (MemoryStream xmlStreamCha = new MemoryStream())
            {
                xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                xmlStreamCha.Position = 0;
                xmlDocCha.Load(xmlStreamCha);
            }

            WebX_Master_Docket_Charges TempWMDC = new WebX_Master_Docket_Charges();
            string DynamicChargdeXML = "EXEC USP_GetPivotDynamicCharges '" + xmlDocCha.InnerXml + "'";
            DataTable DTCharges = GF.GetDataTableFromSP(DynamicChargdeXML);
            TempWMDC = DataRowToObject.CreateListFromTable<WebX_Master_Docket_Charges>(DTCharges).First();

            #endregion

            #region Get OriginStateCode

            DataTable DTORGNCD = new DataTable();
            if (DVM.WMD.PAYBAS == "P01")/* Paid */
            {
                DTORGNCD = OS.GetOriginStateCodeDocket(DVM.WMD.PAYBAS, DVM.WMD.ORGNCD, DVM.WMD.PARTY_CODE, DVM.WMD.DESTCD, BaseUserName, BaseLocationCode);
            }
            else if (DVM.WMD.PAYBAS == "P02")/* TBB */
            {
                DTORGNCD = OS.GetOriginStateCodeDocket(DVM.WMD.PAYBAS, DVM.WMD.ORGNCD, DVM.WMD.PARTY_CODE, DVM.WMD.DESTCD, BaseUserName, BaseLocationCode);
            }
            else if (DVM.WMD.PAYBAS == "P03")/* To Pay */
            {
                DTORGNCD = OS.GetOriginStateCodeDocket(DVM.WMD.PAYBAS, DVM.WMD.ORGNCD, DVM.WMD.PARTY_CODE, DVM.WMD.DESTCD, BaseUserName, BaseLocationCode);
            }
            if (DVM.WMD.PAYBAS != "P04")
            {
                DVM.WMD.OriginStateCode = DTORGNCD.Rows[0][0].ToString();
            }

            #endregion

            /* END GST Changes By Chirag D  */
            try
            {
                #region Check DKT Charges Calculation

                decimal DKTsubTotal = 0, Charges_SubTotal = 0, IGST = 0, SGST = 0, UTGST = 0, CGST = 0;

                foreach (var item in DocketChargesList)
                {
                    Charges_SubTotal = Charges_SubTotal + item.ChargeAmount;
                }

                DKTsubTotal = Convert.ToDecimal(DVM.WMDC.FREIGHT) + Convert.ToDecimal(Charges_SubTotal);

                if (DVM.WMDC.FOV == null)
                {
                    DVM.WMDC.FOV = 0;
                }
                else if (DVM.WMDC.FOV > 0)
                {
                    DVM.WMDC.FOV = Convert.ToDecimal(string.Format("{0:0.00}", Convert.ToDecimal(DVM.WMDC.FOV)));
                }
                DKTsubTotal = DKTsubTotal + Convert.ToDecimal(DVM.WMDC.FOV);

                TempWMDC.IGSTRate = TempWMDC.IGSTAmount > 0 ? TempWMDC.IGSTRate : 0;
                TempWMDC.CGSTRate = TempWMDC.CGSTAmount > 0 ? TempWMDC.CGSTRate : 0;
                TempWMDC.SGSTRate = TempWMDC.SGSTAmount > 0 ? TempWMDC.SGSTRate : 0;
                TempWMDC.UTGSTRate = TempWMDC.UTGSTAmount > 0 ? TempWMDC.UTGSTRate : 0;

                if (DVM.WMD.stax_paidby == "T")
                {
                    //serviceTax = Convert.ToDecimal(string.Format("{0:#.##}", (DKTsubTotal * DVM.WMDC.SVCTAX_Rate) / 100));
                    //SBC = Convert.ToDecimal(string.Format("{0:#.##}", (DKTsubTotal * Convert.ToDecimal(DVM.WMDC.SbcRate) / 100)));
                    //KKC = Convert.ToDecimal(string.Format("{0:#.##}", (DKTsubTotal * Convert.ToDecimal(DVM.WMDC.KKCRate) / 100)));

                    IGST = Convert.ToDecimal((DKTsubTotal * Convert.ToDecimal(TempWMDC.IGSTRate)) / 100);
                    SGST = Convert.ToDecimal((DKTsubTotal * Convert.ToDecimal(TempWMDC.SGSTRate) / 100));
                    CGST = Convert.ToDecimal((DKTsubTotal * Convert.ToDecimal(TempWMDC.CGSTRate) / 100));
                    UTGST = Convert.ToDecimal((DKTsubTotal * Convert.ToDecimal(TempWMDC.UTGSTRate) / 100));
                }

                //decimal DKTTotal = DKTsubTotal + serviceTax + SBC + KKC;
                decimal DKTTotal = DKTsubTotal + IGST + SGST + CGST + UTGST;
                decimal DKTTotal1 = DKTsubTotal + TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.CGSTAmount + TempWMDC.UTGSTAmount;

                #endregion

                #region DKT Entry Date Validation

                string fromdate = Convert.ToDateTime(DVM.WMD.DOCKDT).ToString("dd MMM yyyy");
                string todate = Convert.ToDateTime(DVM.WMD.CDELDT).ToString("dd MMM yyyy");
                DateTime DocketDate = Convert.ToDateTime(fromdate);
                DateTime EDDDate = Convert.ToDateTime(todate);

                if (Convert.ToDateTime(DocketDate.Date.ToString("dd MMM yyyy")) > Convert.ToDateTime(System.DateTime.Now.ToString("dd MMM yyyy")))
                {
                    ViewBag.StrError = ViewBag.DKTCalledAs + " Date cannot be Large than Today Date";
                    return View("Error");
                }

                if (DocketDate.Date > EDDDate.Date)
                {
                    ViewBag.StrError = "EDD Date Should be Large than " + ViewBag.DKTCalledAs + " Date";
                    return View("Error");
                }

                if (DVM.WMD.PAYBAS.ToUpper() != "P04" && DVM.WMDC.SubTotal <= 0)
                {
                    ViewBag.StrError = ViewBag.DKTCalledAs + " Can not be Zero.";
                    return View("Error");
                }

                int days = Convert.ToInt32((System.DateTime.Now - DocketDate).TotalDays);
                //if (days > 6)
                //{
                //    ViewBag.StrError = "Please Enter Valid " + ViewBag.DKTCalledAs + " Date. " + ViewBag.DKTCalledAs + " date " + DocketDate.ToString("dd MMM yyy") + " not valid.";
                //    return View("Error");
                //}

                #endregion

                #region STAX Validation

                if (DVM.WMD.PAYBAS.ToUpper() == "P02")
                {
                    string GST_paidby_opts = OS.GetContractServiceChargessDetails(DVM.WMD.ContractId, DVM.WMD.TRN_MOD).FirstOrDefault().gstpaidby;
                    //if (GST_paidby_opts != "" && !GST_paidby_opts.Contains(DVM.WMD.stax_paidby))
                    //{
                    //    ViewBag.StrError = "GST Paid by is not belongs to Contract.";
                    //    return View("Error");
                    //}
                }
                else if (DVM.WMD.PAYBAS.ToUpper() == "P01" || DVM.WMD.PAYBAS.ToUpper() == "P03")
                {
                    string sqlQuery = "SELECT COUNT(*) AS CNT FROM WebX_Trans_Docket_Status WHERE DOCKNO='" + DVM.WMD.DOCKNO + "' AND ISNULL(Finalized,'N')='Y'";
                    DataTable DT = GF.GetDataTableFromSP(sqlQuery);
                    if (DT.Rows[0]["CNT"].ToString() == "1")
                    {
                        ViewBag.StrError = ViewBag.DKTCalledAs + " is already Finalized. You cannot Edit this " + ViewBag.DKTCalledAs + ".";
                        return View("Error");
                    }
                }

                //if (TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.CGSTAmount + TempWMDC.UTGSTAmount == 0 && DVM.WMD.PAYBAS.ToUpper() != "P04")
                if (TempWMDC.IGSTAmount + TempWMDC.SGSTAmount + TempWMDC.CGSTAmount + TempWMDC.UTGSTAmount == 0 && DVM.WMD.PAYBAS.ToUpper() != "P04" && !DVM.WMD.IsStaxExemp)
                {
                    ViewBag.StrError = "GST Paid by Transporter but GST not Chagred.";
                    return View("Error");
                }
                //if (DVM.WMD.stax_paidby == "P" && TempWMDC.IGSTRate + TempWMDC.SGSTRate + TempWMDC.CGSTRate + TempWMDC.UTGSTRate != 0)
                //{
                //    ViewBag.StrError = "GST Paid by Billing Party but GST Chagred.";
                //    return View("Error");
                //}
                if (DVM.WMD.PAYBAS.ToUpper() == "P04" && DVM.WMDC.DKTTOT > 0)
                {
                    ViewBag.StrError = "Billing Type FOC but " + ViewBag.DKTCalledAS + " Total > 0.";
                    return View("Error");
                }

                #endregion

                decimal CalculatedFREIGHT = 0;
                if (DVM.WMDC.RATE_TYPE == "P")
                {
                    CalculatedFREIGHT = Convert.ToDecimal(DVM.WMD.PKGSNO * DVM.WMDC.FREIGHT_CALC);
                }
                else if (DVM.WMDC.RATE_TYPE == "F")
                {
                    CalculatedFREIGHT = Convert.ToDecimal(DVM.WMDC.FREIGHT_CALC);
                }
                else if (DVM.WMDC.RATE_TYPE == "T")
                {
                    CalculatedFREIGHT = Convert.ToDecimal(DVM.WMD.CHRGWT * DVM.WMDC.FREIGHT_CALC);
                }
                else if (DVM.WMDC.RATE_TYPE == "W")
                {
                    CalculatedFREIGHT = Convert.ToDecimal(DVM.WMD.CHRGWT * DVM.WMDC.FREIGHT_CALC);
                }

                if (Convert.ToInt32(CalculatedFREIGHT) != Convert.ToInt32(DVM.WMDC.FREIGHT))
                {
                    ViewBag.StrError = "Freight Not Calculated Right Please Check Freight And then submit.";
                    return View("Error");
                }
                //{
                //    ViewBag.StrError = "There is a problem in Docket Calculation. Please retry and check charges properly. DKTTotal = " + Convert.ToInt32(Convert.ToDouble(DKTTotal)) + " & DKTTOT = " + Convert.ToInt32(Convert.ToDouble(DVM.WMDC.DKTTOT)) + ".";
                //    return View("Error");
                //}

                //if (Convert.ToInt32(Convert.ToDouble(DKTTotal)) == Convert.ToInt32(Convert.ToDouble(DVM.WMDC.DKTTOT)))                
                double DKtTotDiff1 = Convert.ToDouble(DKTTotal1) - Convert.ToDouble(DVM.WMDC.DKTTOT);
                if (DKtTotDiff1 >= 1 || DKtTotDiff1 <= -1)
                {
                    ViewBag.StrError = "There is a problem in Docket Calculation. Please retry and check charges properly. DKTTotal = " + Convert.ToInt32(Convert.ToDouble(DKTTotal)) + " & DKTTOT = " + Convert.ToInt32(Convert.ToDouble(DVM.WMDC.DKTTOT)) + ".";
                    return View("Error");
                }

                double DKtTotDiff = Convert.ToDouble(DKTTotal) - Convert.ToDouble(DVM.WMDC.DKTTOT);
                if (DKtTotDiff >= 1 || DKtTotDiff <= -1)
                {
                    ViewBag.StrError = "There is a problem in Docket Calculation. Please retry and check charges properly. DKTTotal = " + Convert.ToInt32(Convert.ToDouble(DKTTotal)) + " & DKTTOT = " + Convert.ToInt32(Convert.ToDouble(DVM.WMDC.DKTTOT)) + ".";
                    return View("Error");
                }
                else
                {
                    if (DVM.WMD.PAYBAS == "P02")
                    {
                        string SQRY_AtDestination = "SELECT BillingAtDestination FROM webx_CUSTHDR WHERE CUSTCD='" + DVM.WMD.PARTY_CODE + "'";
                        DataTable DTDestination = GF.GetDataTableFromSP(SQRY_AtDestination);
                        string strBillingAtDestination = string.IsNullOrEmpty(DTDestination.Rows[0][0].ToString()) ? "0" : DTDestination.Rows[0][0].ToString();
                        bool BillingAtDestination = strBillingAtDestination == "1" ? true : false;

                        //BillingAtDestination = Convert.ToBoolean(strBillingAtDestination);
                        if (BillingAtDestination)
                        {
                            string SQRY_DestState = "SELECT b.statePrefix FROM webx_location a INNER JOIN Webx_State b on a.LocState = b.stcd WHERE a.LocCode = '" + DVM.WMD.DESTCD + "'";
                            DataTable DTDestState = GF.GetDataTableFromSP(SQRY_DestState);

                            DVM.WMD.fincmplbr = DVM.WMD.DESTCD;
                            DVM.WMD.DestStateCode = DTDestState.Rows[0][0].ToString();
                        }

                        if (string.IsNullOrEmpty(DVM.WMD.DestStateCode))
                        {
                            string SQRY_DestState = "SELECT b.statePrefix,stnm FROM webx_location a INNER JOIN Webx_State b on a.LocState = b.stcd WHERE a.LocCode = '" + DVM.WMD.DESTCD + "'";
                            DataTable DTDestState = GF.GetDataTableFromSP(SQRY_DestState);
                            DVM.WMD.DestStateCode = DTDestState.Rows[0][0].ToString();
                            DVM.WMD.DestStateName = DTDestState.Rows[0][1].ToString();
                        }
                    }

                    string SQL = "EXEC USP_BACKUP_DOCKET '" + DVM.WMD.DOCKNO + "', '" + BaseUserName.ToUpper() + "'";
                    DataTable DTBackup = GF.GetDataTableFromSP(SQL);

                    #region Update WebX_Master_Docket Table

                    string sqlstr = "UPDATE WebX_Master_Docket SET edited='Y',isCPEdit='1',lasteditby='" + BaseUserName.ToUpper() + "'";
                    sqlstr = sqlstr + ",stax_paidby='" + DVM.WMD.stax_paidby + "'";
                    sqlstr = sqlstr + ",dockdt='" + GF.FormateDate(DVM.WMD.DOCKDT) + "'";
                    sqlstr = sqlstr + ",CDELDT='" + GF.FormateDate(DVM.WMD.CDELDT) + "'";
                    sqlstr = sqlstr + ",trn_mod='" + DVM.WMD.TRN_MOD + "'";
                    sqlstr = sqlstr + ",Pickup_Dely='" + DVM.WMD.Pickup_Dely + "'";
                    sqlstr = sqlstr + ",paybas='" + DVM.WMD.PAYBAS + "'";
                    sqlstr = sqlstr + ",chrgwt=" + DVM.WMD.CHRGWT + "";
                    sqlstr = sqlstr + ",actuwt=" + DVM.WMD.ACTUWT + "";
                    sqlstr = sqlstr + ",spl_svc_req='" + DVM.WMD.spl_svc_req + "'";
                    sqlstr = sqlstr + ",IsGSTApplied=1";
                    sqlstr = sqlstr + ",DestStateCode='" + DVM.WMD.DestStateCode + "'";
                    sqlstr = sqlstr + ",IsUnionTeritory='" + DVM.WMD.IsUnionTeritory + "'";
                    sqlstr = sqlstr + ",DestStateName='" + (!string.IsNullOrEmpty(DVM.WMD.DestStateName) ? DVM.WMD.DestStateName.ReplaceSpecialCharacters() : string.Empty) + "'";
                    sqlstr = sqlstr + ",Destination_Area='" + (!string.IsNullOrEmpty(DVM.WMD.Destination_Area) ? DVM.WMD.Destination_Area.ReplaceSpecialCharacters() : string.Empty) + "'";
                    if (DVM.WMD.OriginStateName != null)
                    {
                        sqlstr = sqlstr + ",OriginStateName='" + (!string.IsNullOrEmpty(DVM.WMD.OriginStateName) ? DVM.WMD.OriginStateName.ReplaceSpecialCharacters() : string.Empty) + "'";
                    }
                    if (DVM.WMD.OriginStateName != null)
                    {
                        sqlstr = sqlstr + ",Origin_Area='" + (!string.IsNullOrEmpty(DVM.WMD.Origin_Area) ? DVM.WMD.Origin_Area.ReplaceSpecialCharacters() : string.Empty) + "'";
                    }
                    sqlstr = sqlstr + ",CustGSTNo='" + DVM.WMD.CustGSTNo + "'";
                    sqlstr = sqlstr + ",CSGECustGSTNo='" + DVM.WMD.CSGECustGSTNo + "'";
                    sqlstr = sqlstr + ",OriginStateCode='" + DVM.WMD.OriginStateCode + "'";
                    sqlstr = sqlstr + ",BillingState='" + DVM.WMD.BillingState + "'";
                    sqlstr = sqlstr + ",cft_yn='" + (DVM.WMD.IsVolumetric == true ? "Y" : "N") + "'";
                    sqlstr = sqlstr + ",ISCounterPickUpPRS='" + DVM.WMD.ISCounterPickUpPRS + "'";
                    sqlstr = sqlstr + ",ISCounterDelivery='" + DVM.WMD.ISCounterDelivery + "'";

                    if (DVM.WMD.IsCODDOD == true)
                        sqlstr = sqlstr + ",cod_dod='Y'";
                    else
                        sqlstr = sqlstr + ",cod_dod='N'";

                    if (DVM.WMD.IsDACC == true)
                        sqlstr = sqlstr + ",dacc_yn='Y'";
                    else
                        sqlstr = sqlstr + ",dacc_yn='N'";

                    if (DVM.WMD.IsODA == true)
                        sqlstr = sqlstr + ",diplomat='Y'";
                    else
                        sqlstr = sqlstr + ",diplomat='N'";

                    if (DVM.WMD.IsLocalDocket == true)
                        sqlstr = sqlstr + ",localcn_yn='Y'";
                    else
                        sqlstr = sqlstr + ",localcn_yn='N'";

                    //if (DVM.WMDC.insuyn == true)
                    //    sqlstr = sqlstr + ",insuyn='O'";
                    //else
                    //    sqlstr = sqlstr + ",insuyn='C'";


                    sqlstr = sqlstr + ",reassign_destcd='" + DVM.WMD.DESTCD + "'";
                    sqlstr = sqlstr + ",from_loc='" + MS.GetCityMasterObject().Where(c => c.city_code == Convert.ToDecimal(DVM.WMD.from_loc)).FirstOrDefault().Location + "'";
                    sqlstr = sqlstr + ",to_loc='" + MS.GetCityMasterObject().Where(c => c.city_code == Convert.ToDecimal(DVM.WMD.to_loc)).FirstOrDefault().Location + "'";
                    sqlstr = sqlstr + ",service_class='" + DVM.WMD.Service_Class + "'";
                    sqlstr = sqlstr + ",ftl_types='" + DVM.WMD.ftl_types + "'";
                    sqlstr = sqlstr + ",lasteditdate='" + DateTime.Today.ToString("dd MMM yyyy") + "'";
                    sqlstr = sqlstr + ",editinfo='" + DVM.WMD.editinfo + "'";
                    sqlstr = sqlstr + ",pkgsno='" + DVM.WMD.PKGSNO + "'";

                    //Consignor
                    sqlstr = sqlstr + ",CSGNCD='" + DVM.WMD.CSGNCD + "'";
                    sqlstr = sqlstr + ",CSGNNM='" + DVM.WMD.CSGNNM + "'";
                    sqlstr = sqlstr + ",CSGNADDR='" + DVM.WMD.CSGNADDR + "'";
                    sqlstr = sqlstr + ",CSGNCity='" + DVM.WMD.CSGNCity + "'";
                    sqlstr = sqlstr + ",CSGNPinCode='" + DVM.WMD.CSGNPinCode + "'";
                    sqlstr = sqlstr + ",csgnmobile='" + DVM.WMD.csgnmobile + "'";
                    //Consignee
                    sqlstr = sqlstr + ",CSGECD='" + DVM.WMD.CSGECD + "'";
                    sqlstr = sqlstr + ",CSGENM='" + DVM.WMD.CSGENM + "'";
                    sqlstr = sqlstr + ",CSGEADDR='" + DVM.WMD.CSGEADDR + "'";
                    sqlstr = sqlstr + ",CSGECity='" + DVM.WMD.CSGECity + "'";
                    sqlstr = sqlstr + ",CSGEPinCode='" + DVM.WMD.CSGEPinCode + "'";
                    sqlstr = sqlstr + ",csgemobile='" + DVM.WMD.csgemobile + "'";
                    sqlstr = sqlstr + ",EWayBillNo='" + DVM.WMD.EWayBillNo + "'";
                    sqlstr = sqlstr + ",EWayBillExpiredDate='" + (DVM.WMD.EWayBillExpiredDate == System.DateTime.MinValue ? null : DVM.WMD.EWayBillExpiredDate) + "'";
                    sqlstr = sqlstr + ",EWayInvoiceAmount='" + DVM.WMD.EWayInvoicevalue + "'";


                    string setstr = "";

                    // TO REMOVE QUOTE IF ANY                
                    if (DVM.WMD.PARTY_CODE != "")
                    {
                        setstr = ",party_code='" + DVM.WMD.PARTY_CODE + "'";
                        setstr = setstr + ",party_name='" + DVM.WMD.party_name + "'";
                    }

                    //if (DVM.WMD.fincmplbr != "")
                    //{
                    //    if (DVM.WMD.DOCKDT >= Convert.ToDateTime("01 Jul 2017"))
                    //    {
                    //        setstr = setstr + ",fincmplbr='" + DVM.WMD.ORGNCD + "'";
                    //    }
                    //}
                    setstr = setstr + ",fincmplbr='" + DVM.WMD.fincmplbr + "'";
                    if (setstr.CompareTo("") != 0)
                    {
                        sqlstr = sqlstr + setstr;
                    }

                    sqlstr = sqlstr + " WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    id = GF.SaveRequestServices(sqlstr.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    DataTable DT = GF.GetDataTableFromSP(sqlstr);

                    #endregion

                    #region Delete and Insert WebX_Master_Docket_Invoice Table
                    /* 
                     * 
                     * Comment by Nittin on 12 Jan 2017 as added full backup process at the begining of the process starts.                 
                     * 
                    //Insert into webx_master_docket_invoice_edit
                    string sqlstrInsert = "INSERT INTO WebX_Master_Docket_Invoice_Edit (DOCKNO,INVNO,INVDT,DECLVAL,PKGSNO,ACTUWT,VOL_L,VOL_B,VOL_H,TOT_CFT,vol_cft,savedby,savingdate) ";
                    sqlstrInsert = sqlstrInsert + " SELECT DOCKNO,INVNO,INVDT,DECLVAL,PKGSNO,ACTUWT,VOL_L,VOL_B,VOL_H,TOT_CFT,vol_cft,'" + BaseUserName.ToUpper() + "',GETDATE() FROM WebX_Master_Docket_Invoice";
                    sqlstrInsert = sqlstrInsert + " WHERE DOCKNO = '" + DVM.WMD.DOCKNO + "'";
                    id = GF.SaveRequestServices(sqlstrInsert.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    DT = GF.GetDataTableFromSP(sqlstrInsert);                
                    */

                    //DELETE webx_master_docket_invoice
                    string sqlstrDelete = "DELETE FROM WebX_Master_Docket_Invoice WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    id = GF.SaveRequestServices(sqlstrDelete.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    DT = GF.GetDataTableFromSP(sqlstrDelete);

                    decimal invoiceTotal = 0;
                    string invoiceNos = "";
                    //   INSERTING INVOICE DETAILS error
                    foreach (var item in DocketInvoiceList)
                    {
                        if (item.TOT_CFT == null)
                            item.TOT_CFT = 0;
                        if (item.VOL_H == null)
                            item.VOL_H = 0;
                        if (item.VOL_B == null)
                            item.VOL_B = 0;

                        string QueryString = "Insert into WebX_Master_Docket_Invoice (DOCKNO,INVNO,INVDT,DECLVAL,PKGSNO,ACTUWT,VOL_L,VOL_B,VOL_H,TOT_CFT,Part_No"
                            + ",ProductName,ProductDescription,HSNCode,Qty,UnitPrice,GSTRate, PKGSTY, PRODCD) values ('"
                            + DVM.WMD.DOCKNO + "','" + item.INVNO.ReplaceSpecialCharacters() + "','" + GF.FormateDate(item.INVDT) + "'," + item.DECLVAL + "," + item.PKGSNO + "," + item.ACTUWT + "," + item.VOL_L + "," + item.VOL_B + "," + item.VOL_H + "," + item.TOT_CFT + ",'" + item.Part_No +
                            "','" + item.ProductName + "','" + item.ProductDesc + "','" + item.HSNCode + "','" + item.Qty + "','" + item.UnitPrice + "','" + item.GSTRate + "','" + item.PKGSTY + "','" + item.PRODCD + "')";
                        id = GF.SaveRequestServices(QueryString.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                        DT = GF.GetDataTableFromSP(QueryString);
                        invoiceTotal = invoiceTotal + Convert.ToDecimal(item.DECLVAL == null ? 0 : item.DECLVAL);
                        if (invoiceNos == "")
                        {
                            invoiceNos = string.IsNullOrEmpty(item.INVNO) ? "" : item.INVNO;
                        }
                        else
                        {
                            invoiceNos = invoiceNos + "," + (string.IsNullOrEmpty(item.INVNO) ? "" : item.INVNO);
                        }
                    }

                    #endregion

                    #region Update WebX_Trans_Docket_Status Table

                    string checkDocket = "SELECT CNT=COUNT(s.DOCKNO) FROM dbo.WebX_Master_Docket d WITH(NOLOCK) INNER JOIN  dbo.WebX_Trans_Docket_Status s WITH(NOLOCK) ON d.DOCKNO=s.DOCKNO WHERE s.Curr_Loc=d.ORGNCD AND s.DOCKNO='" + DVM.WMD.DOCKNO + "'";
                    DT = new DataTable();
                    DT = GF.GetDataTableFromSP(checkDocket);
                    if (DT.Rows.Count > 0)
                    {
                        int CNT = Convert.ToInt16(DT.Rows[0]["CNT"].ToString());
                        if (CNT == 1)
                        {
                            string updateQuery = "UPDATE dbo.WebX_Trans_Docket_Status WITH(ROWLOCK) SET ArrPkgQty='" + Convert.ToInt32(Convert.ToDouble(DVM.WMD.PKGSNO)) + "',ArrWeightQty='" + DVM.WMD.ACTUWT + "',Invamt='" + invoiceTotal + "',invno='" + invoiceNos + "' WHERE DOCKNO='" + DVM.WMD.DOCKNO + "' AND DOCKSF='.'";
                            id = GF.SaveRequestServices(updateQuery.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                            DT = new DataTable();
                            DT = GF.GetDataTableFromSP(updateQuery);
                        }
                    }

                    #endregion

                    #region Update WebX_Master_Docket_Charges Table
                    ///  CHARGE UPDATION IN CHARGES TABLE                

                    if (DVM.WMDC.FRT_RATE == null)
                        DVM.WMDC.FRT_RATE = 0;
                    string sqlstrchrage = "UPDATE WebX_Master_Docket_Charges SET ";
                    sqlstrchrage = sqlstrchrage + "rate_type='" + DVM.WMDC.RATE_TYPE + "',";
                    sqlstrchrage = sqlstrchrage + "frt_rate=" + DVM.WMDC.FREIGHT_CALC + ",";
                    sqlstrchrage = sqlstrchrage + "freight=" + DVM.WMDC.FREIGHT + ",";
                    sqlstrchrage = sqlstrchrage + "FREIGHT_CALC=" + DVM.WMDC.FREIGHT + "";

                    string[,] arrcharges = new string[33, 2];

                    foreach (var itm in DocketChargesList)
                    {
                        sqlstrchrage = sqlstrchrage + "," + itm.ChargeCode + "='" + itm.ChargeAmount + "'";
                    }

                    // Fov is Calculated and Stored Differently 
                    //sqlstrchrage = sqlstrchrage + ", fov=" + DVM.WMDC.FOV + ",";
                    if (DVM.WMD.stax_paidby == null)
                    {
                        DVM.WMD.stax_paidby = "P";
                    }
                    decimal SVCTAX_Rate = 0;
                    if (DVM.WMD.stax_paidby == "P")
                    {
                        SVCTAX_Rate = 0;
                    }
                    else
                    {
                        SVCTAX_Rate = DVM.WMDC.SVCTAX_Rate;
                    }
                    sqlstrchrage = sqlstrchrage + ", fov=" + DVM.WMDC.FOV + ",";
                    sqlstrchrage = sqlstrchrage + "SVCTAX_Rate=0,";
                    sqlstrchrage = sqlstrchrage + "SVCTAX=0,";
                    sqlstrchrage = sqlstrchrage + "CESS=0,";
                    sqlstrchrage = sqlstrchrage + "hedu_cess=0,";
                    sqlstrchrage = sqlstrchrage + "SbcRate=0,";
                    sqlstrchrage = sqlstrchrage + "SBCess=0,";
                    sqlstrchrage = sqlstrchrage + "KKCessRate=0,";
                    sqlstrchrage = sqlstrchrage + "KKCess=0,";
                    sqlstrchrage = sqlstrchrage + "IsGSTApplied=1,";
                    sqlstrchrage = sqlstrchrage + "GSTType='" + DVM.WMDC.GSTType + "',";
                    sqlstrchrage = sqlstrchrage + "IGSTRate=" + TempWMDC.IGSTRate + ",";
                    sqlstrchrage = sqlstrchrage + "IGSTAmount=" + TempWMDC.IGSTAmount + ",";
                    sqlstrchrage = sqlstrchrage + "CGSTRate=" + TempWMDC.CGSTRate + ",";
                    sqlstrchrage = sqlstrchrage + "CGSTAmount=" + TempWMDC.CGSTAmount + ",";
                    sqlstrchrage = sqlstrchrage + "SGSTRate=" + TempWMDC.SGSTRate + ",";
                    sqlstrchrage = sqlstrchrage + "SGSTAmount=" + TempWMDC.SGSTAmount + ",";
                    sqlstrchrage = sqlstrchrage + "UTGSTRate=" + TempWMDC.UTGSTRate + ",";
                    sqlstrchrage = sqlstrchrage + "UTGSTAmount=" + TempWMDC.UTGSTAmount + ",";
                    sqlstrchrage = sqlstrchrage + "subtotal=" + DVM.WMDC.SubTotal + ",";
                    sqlstrchrage = sqlstrchrage + "dkttot=" + DVM.WMDC.DKTTOT + "";
                    sqlstrchrage = sqlstrchrage + " WHERE dockno='" + DVM.WMD.DOCKNO + "'";

                    id = GF.SaveRequestServices(sqlstrchrage.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    DT = GF.GetDataTableFromSP(sqlstrchrage);

                    string sqlstrchrageSCHG11 = "UPDATE WebX_Master_Docket_Charges SET SCHG11=" + DVM.WMDC.FOV + " WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    DT = GF.GetDataTableFromSP(sqlstrchrageSCHG11);
                    #endregion

                    #region Update webx_docket_profitability Table

                    //  PROFITABILITY UPDATION
                    //string strprofitability = "UPDATE webx_docket_profitability SET ";
                    //// strprofitability = strprofitability + "destcd='" + "txtdelloc.Text.Trim()" + "'";
                    //strprofitability = strprofitability + "dockdt='" + GF.FormateDate(DVM.WMD.DOCKDT) + "'";
                    //strprofitability = strprofitability + ",trn_mod='" + DVM.WMD.TRN_MOD + "'";
                    //strprofitability = strprofitability + ",service_class='" + DVM.WMD.TrnServiceType + "'";
                    //strprofitability = strprofitability + ",paybas='" + DVM.WMD.PAYBAS + "'";
                    //strprofitability = strprofitability + ",actuwt=" + DVM.WMD.ACTUWT + "";
                    //strprofitability = strprofitability + ",chrgwt=" + DVM.WMD.CHRGWT + "";
                    //strprofitability = strprofitability + ",pkgsno=" + DVM.WMD.PKGSNO + "";
                    //strprofitability = strprofitability + ",freight=" + DVM.WMDC.FREIGHT + "";
                    //strprofitability = strprofitability + ",svctax=" + DVM.WMDC.SVCTAX + "";
                    //strprofitability = strprofitability + ",cess=" + DVM.WMDC.CESS + "";
                    //strprofitability = strprofitability + ",h_cess=" + DVM.WMDC.hedu_cess + "";
                    //strprofitability = strprofitability + ",dkttot=" + DVM.WMDC.DKTTOT + "";
                    //strprofitability = strprofitability + " WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    //id = GF.SaveRequestServices(strprofitability.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    //DT = GF.GetDataTableFromSP(strprofitability);
                    // BA Calculation UPDATION
                    if (DVM.WMD.TrnServiceType == null || DVM.WMD.TrnServiceType != "")
                        DVM.WMD.TrnServiceType = "0";

                    #endregion

                    #region Update webx_ba_calculation Table

                    //string strcal = "UPDATE webx_ba_calculation SET ";
                    //// strcal = strcal + "destcd='" + "txtdelloc.Text.Trim()" + "'";
                    //strcal = strcal + "dockdt='" + GF.FormateDate(DVM.WMD.DOCKDT) + "'";
                    //strcal = strcal + ",trn_mod='" + DVM.WMD.TRN_MOD + "'";
                    //strcal = strcal + ",service_class=" + DVM.WMD.TrnServiceType + "";
                    //strcal = strcal + ",paybas='" + DVM.WMD.PAYBAS + "'";
                    //// strcal = strcal + ",actuwt=" + DVM.WMD.ACTUWT + "";
                    //strcal = strcal + ",chrgwt=" + DVM.WMD.CHRGWT + "";
                    //strcal = strcal + ",pkgsno=" + DVM.WMD.PKGSNO + "";
                    //strcal = strcal + ",freight=" + DVM.WMDC.FREIGHT + "";
                    //strcal = strcal + ",svctax=" + DVM.WMDC.SVCTAX + "";
                    //strcal = strcal + ",cess=" + DVM.WMDC.CESS + "";
                    //strcal = strcal + ",h_cess=" + DVM.WMDC.hedu_cess + "";
                    //strcal = strcal + ",dkttot=" + DVM.WMDC.DKTTOT + "";
                    //strcal = strcal + " WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    //id = GF.SaveRequestServices(strcal.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    //DT = GF.GetDataTableFromSP(strcal);

                    #endregion

                    #region Accounting Part

                    //  ACCOUNTING PART -- NEW GENERATIONS -- START
                    switch (DVM.WMD.PAYBAS)
                    {
                        case "P01":
                            switch (DVM.WMD.PAYBAS)
                            {
                                case "P01":
                                    accP01_UpdateGeneration(DVM.WMD.DOCKNO, Convert.ToDecimal(DVM.WMD.CHRGWT), DVM.WMD.DOCKDT, Convert.ToDecimal(DVM.WMDC.DKTTOT), Convert.ToDecimal(DVM.WMDC.DKTTOT), Convert.ToDecimal(DVM.WMDC.FREIGHT), Convert.ToDecimal(DVM.WMDC.SVCTAX), Convert.ToDecimal(DVM.WMDC.CESS), Convert.ToDecimal(DVM.WMDC.hedu_cess), Convert.ToDecimal(DVM.WMDC.SVCTAX_Rate));
                                    break;
                                default:// accP01_Generation();
                                    break;
                            }
                            break;
                        case "P03":
                            switch (DVM.WMD.PAYBAS)
                            {
                                case "P03":
                                    accP03_UpdateGeneration(DVM.WMD.DOCKNO, Convert.ToDecimal(DVM.WMD.CHRGWT), DVM.WMD.DOCKDT, Convert.ToDecimal(DVM.WMDC.DKTTOT), Convert.ToDecimal(DVM.WMDC.DKTTOT), Convert.ToDecimal(DVM.WMDC.FREIGHT), Convert.ToDecimal(DVM.WMDC.SVCTAX), Convert.ToDecimal(DVM.WMDC.CESS), Convert.ToDecimal(DVM.WMDC.hedu_cess), Convert.ToDecimal(DVM.WMDC.SVCTAX_Rate));
                                    break;
                                default: //accP03_Generation();
                                    break;
                            }
                            break;
                        case "P02":
                        case "P04":
                        case "P08":
                        case "P09":
                            accP02_Generation();
                            break;
                    } // OUTER SWITCH

                    #endregion

                    string sqlstrDocket = sqlstr + "UPDATE webx_master_docket SET editinfo='" + "P02" + "' WHERE dockno='" + DVM.WMD.DOCKNO + "'";
                    id = GF.SaveRequestServices(sqlstrDocket.Replace("'", "''"), "DocketFinancialEdit_" + DVM.WMD.DOCKNO, "", "");
                    DT = GF.GetDataTableFromSP(sqlstrDocket);
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
                trn.Rollback();
                con.Close();
                return View("Error");
            }

            return RedirectToAction("DocketDone", new { DOCKNO = DVM.WMD.DOCKNO, id = 2 });
        }

        protected void accP01_UpdateGeneration(string Dockno, decimal chrgwt, DateTime dockdt, decimal mrsamt, decimal netamt, decimal col_frt, decimal col_stax, decimal col_cess, decimal col_h_cess, decimal svctax_rate)
        {

            // GENERATING PAID MR TRANSACTION - START              
            string Sql = "EXEC usp_paidmrtransaction '" + "1" + "','" + Dockno + "','" + BaseFinYear + "','" + BaseUserName + "','" + "" + "'";

            // GENERATING PAID MR TRANSACTION - END

            // UPDATION OF PAID MR - START
            string sqlstr = "UPDATE webx_mr_hdr SET ";
            sqlstr = sqlstr + "chrgwt=" + chrgwt + ",";
            sqlstr = sqlstr + "dockdt='" + GF.FormateDate(dockdt) + "',";
            sqlstr = sqlstr + "mrsamt=" + mrsamt + ",";
            sqlstr = sqlstr + "netamt=" + netamt + ",";
            sqlstr = sqlstr + "col_frt=" + col_frt + ",";
            sqlstr = sqlstr + "col_stax=" + col_stax + ",";
            sqlstr = sqlstr + "col_cess=" + col_cess + ",";
            sqlstr = sqlstr + "col_h_cess=" + col_h_cess + ",";
            sqlstr = sqlstr + "svctax_rate=" + svctax_rate;
            sqlstr = sqlstr + " WHERE dockno='" + Dockno + "'";
            // UPDATION OF PAID MR - END

        }

        protected void accP03_UpdateGeneration(string Dockno, decimal chrgwt, DateTime dockdt, decimal mrsamt, decimal netamt, decimal col_frt, decimal col_stax, decimal col_cess, decimal col_h_cess, decimal svctax_rate)
        {
            // hdneditinfo.Value = hdneditinfo.Value + "P03";

            string sqlstr = "";
            try
            {
                // UPDATION OF PAID MR - START
                sqlstr = "UPDATE webx_mr_hdr SET ";
                sqlstr = sqlstr + "chrgwt=" + chrgwt + ",";
                sqlstr = sqlstr + "dockdt='" + GF.FormateDate(dockdt) + "',";
                sqlstr = sqlstr + "mrsamt=" + mrsamt + ",";
                sqlstr = sqlstr + "netamt=" + netamt + ",";
                sqlstr = sqlstr + "col_frt=" + col_frt + ",";
                sqlstr = sqlstr + "col_stax=" + col_stax + ",";
                sqlstr = sqlstr + "col_cess=" + col_cess + ",";
                sqlstr = sqlstr + "col_h_cess=" + col_h_cess + ",";
                sqlstr = sqlstr + "svctax_rate=" + svctax_rate;
                sqlstr = sqlstr + " WHERE dockno='" + Dockno + "'";
                // UPDATION OF PAID MR - END

                // CANCELLING TO PAY MR - START
                sqlstr = "SELECT COUNT(*) FROM webx_mr_hdr WHERE dockno='" + Dockno + "' AND mr_cancel='N'";

                int mr_count = 0;
                if (mr_count > 0)
                {
                    // GENERATING PAID MR TRANSACTION - START
                    string Sql = "EXEC usp_topaymrtransaction '" + "1" + "','" + Dockno + "','" + BaseFinYear + "','" + BaseUserName + "','" + "" + "'";
                    // GENERATING PAID MR TRANSACTION - END
                }
            }
            catch (Exception)
            {
                //trn.Rollback();
                //string strex = ex.Message.Replace('\n', '_');
                //Response.Redirect("ErrorPage.aspx?heading=DataBase Updation Error&detail1=Accounting Error&detail2=" + strex);
            }
        }

        protected void accP02_Generation()
        {
            try
            {
                // GENERATING TBB TRANSACTIONS - START
                //if (hdnclient.Value.CompareTo("AGILITY") == 0 || hdnclient.Value.CompareTo("ASL") == 0 || hdnclient.Value.Trim().CompareTo("RITCO_TEST") == 0 || hdnclient.Value.CompareTo("RLL") == 0 || hdnclient.Value.CompareTo("VARUNA") == 0)
                //{
                //    sqlstr = "usp_tbb_transaction";
                //    cmd = new SqlCommand(sqlstr, con, trn);
                //    cmd.CommandType = CommandType.StoredProcedure;
                //    cmd.Parameters.AddWithValue("@transNo", "1");
                //    cmd.Parameters.AddWithValue("@docNo", hdndockno.Value.Trim());
                //    cmd.Parameters.AddWithValue("@finYear", hdnfin_year_2d.Value.Trim());
                //    cmd.Parameters.AddWithValue("@entryBy1", hdnemployee.Value.Trim());
                //    cmd.Parameters.AddWithValue("@NextVoucherno", "");
                //    cmd.ExecuteNonQuery();
                //}
                // GENERATING TBB TRANSACTIONS - END
            }
            catch (Exception)
            {
                //trn.Rollback();
                //string strex = ex.Message.Replace('\n', '_');
                //Response.Redirect("ErrorPage.aspx?heading=DataBase Updation Error&detail1=Accounting Error&detail2=" + strex);
            }
        }

        #endregion

        #region THC Financial Edit

        public ActionResult THCEditCretria(string Type)
        {
            THCDeparturesViewModel objModel = new THCDeparturesViewModel();
            List<webx_THC_SUMMARY> SummaryList = new List<webx_THC_SUMMARY>();
            webx_THC_SUMMARY THCDepModel = new webx_THC_SUMMARY();
            THCDepModel.Type = Type;
            objModel.THCDepModel = THCDepModel;
            objModel.THCDepList = SummaryList;
            ViewBag.Type = Type;
            return View("THCEditCretria", objModel);
        }

        public ActionResult THCCriteriaSubmit(THCDeparturesViewModel objModel)
        {
            string TypeOF = "";

            List<webx_THC_SUMMARY> SummaryList = new List<webx_THC_SUMMARY>();
            if (objModel.THCDepModel.THCNO == null)
                TypeOF = "0";
            else
                TypeOF = "1";


            if (objModel.THCDepModel.Type == "T")
            {
                SummaryList = OS.GetTHCEditList(GF.FormateDate(objModel.THCDepModel.FromDate), GF.FormateDate(objModel.THCDepModel.ToDate), objModel.THCDepModel.THCNO, BaseFinYear.Substring(0, 4), TypeOF);
                foreach (var item in SummaryList)
                {
                    item.Type = "T";
                }
            }
            else
            {
                SummaryList = OS.GetPDCEditList(GF.FormateDate(objModel.THCDepModel.FromDate), GF.FormateDate(objModel.THCDepModel.ToDate), objModel.THCDepModel.THCNO, BaseFinYear.Substring(0, 4), TypeOF, BaseLocationCode);
                int Id = 0;
                foreach (var item in SummaryList)
                {
                    Id++;
                    item.Id = Id;
                    item.Type = "P";
                }
            }
            return PartialView("_Partial_THCEdit", SummaryList);
        }

        public ActionResult THCFinancialEditDetail(string THCNo, string Flag)
        {
            THCViewModel objTHC = new THCViewModel();
            webx_THC_SUMMARY GetTHCEditDetail = new webx_THC_SUMMARY();
            if (Flag == "P")
            {
                GetTHCEditDetail = OS.GetPDCEditDetail(THCNo).FirstOrDefault();
            }
            else
            {
                GetTHCEditDetail = OS.GetTHCEditDetail(THCNo).FirstOrDefault();
                GetTHCEditDetail.ContractAmt = OS.StandardContractAmount(GetTHCEditDetail.routecd);
            }

            objTHC.ListTHCChargeCode = OS.GetTHCCharge(THCNo).ToList();
            int Id;
            foreach (var item in objTHC.ListTHCChargeCode)
            {
                Id = 1;
                item.Id = Id;
            }
            objTHC.ListSUMARY = OS.GetchargeCodeList().ToList();

            GetTHCEditDetail.HdnADVAMT = GetTHCEditDetail.ADVAMT;
            GetTHCEditDetail.Hdnbalamtbrcd = GetTHCEditDetail.balamtbrcd;
            GetTHCEditDetail.Hdnfincmplbr = GetTHCEditDetail.fincmplbr;
            GetTHCEditDetail.Hdnpcamt = GetTHCEditDetail.pcamt;
            GetTHCEditDetail.Type = Flag;
            objTHC.THCSUMRY = GetTHCEditDetail;

            return View("THCFinancialEditDetail", objTHC);
        }

        public ActionResult THCEditUpdate(THCViewModel ObjThc, List<webx_THC_SUMMARY> ChargeList)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            string ChargCode = "0,SCHG01,SCHG02,SCHG03";

            try
            {
                string findetval = "";
                string[] columnnm_STR = ChargCode.ToString().Split(',');
                string EditedCharges = "";
                int cnt = 1;
                foreach (var item in ChargeList)
                {
                    findetval = "SCHG01" + "=" + item.SCHG01 + "," + "SCHG02" + "=" + item.SCHG02 + "," + "SCHG03" + "=" + item.SCHG03;
                    EditedCharges = "SCHG01,SCHG02,SCHG03";
                    cnt++;
                }

                if (ObjThc.THCSUMRY.ADVAMT != ObjThc.THCSUMRY.HdnADVAMT)
                {
                    if (EditedCharges == "")
                        EditedCharges = "ADVAmt";
                    else
                        EditedCharges = EditedCharges + ",ADVAmt";
                }
                if (ObjThc.THCSUMRY.pcamt != ObjThc.THCSUMRY.Hdnpcamt)
                {
                    if (EditedCharges == "")
                        EditedCharges = "PCAmt";
                    else
                        EditedCharges = EditedCharges + ",PCAmt";
                }
                if (ObjThc.THCSUMRY.fincmplbr != ObjThc.THCSUMRY.Hdnfincmplbr)
                {
                    if (EditedCharges == "")
                        EditedCharges = "fincmplbr";
                    else
                        EditedCharges = EditedCharges + ",fincmplbr";
                }
                if (ObjThc.THCSUMRY.balamtbrcd != ObjThc.THCSUMRY.Hdnbalamtbrcd)
                {
                    if (EditedCharges == "")
                        EditedCharges = "balamtbrcd";
                    else
                        EditedCharges = EditedCharges + ",balamtbrcd";
                }
                string HidEditedFiled = " <- " + EditedCharges;
                string result = OS.UpdateFINDET(findetval, ObjThc.THCSUMRY.THCNO);
                string result2 = "";
                if (ObjThc.THCSUMRY.Type == "T")
                {
                    result2 = OS.UpdateTHCSUMMARY(ObjThc.THCSUMRY.pcamt, ObjThc.THCSUMRY.ADVAMT, ObjThc.THCSUMRY.fincmplbr, ObjThc.THCSUMRY.balamtbrcd, HidEditedFiled, ObjThc.THCSUMRY.THCNO);
                }
                else
                {
                    result2 = OS.UpdatePDCSUMMARY(ObjThc.THCSUMRY.pcamt, ObjThc.THCSUMRY.ADVAMT, ObjThc.THCSUMRY.fincmplbr, ObjThc.THCSUMRY.balamtbrcd, HidEditedFiled, ObjThc.THCSUMRY.THCNO);
                }
            }
            catch (Exception ex)
            {
                string exmess = ex.Message.ToString().Replace('\n', '_');
                trn.Rollback();
                con.Close();

                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("THCEditDone", new { Code = ObjThc.THCSUMRY.THCNO, Type = ObjThc.THCSUMRY.Type });
        }

        public ActionResult THCEditDone(string Code, string Type)
        {
            THCViewModel obj = new THCViewModel();
            webx_THC_SUMMARY THCSUMRY = new webx_THC_SUMMARY();

            THCSUMRY.Type = Type;
            THCSUMRY.THCNO = Code;
            obj.THCSUMRY = THCSUMRY;
            return View("THCEditDone", obj);
        }

        #endregion

        #region PRS ARRIVAL

        public ActionResult PRSArrival()
        {
            return View();
        }

        public string PRSArrivalsMain(PRSArrivalsFilterViewModel TAFVM)
        {
            return JsonConvert.SerializeObject(OS.PRSArrivalList(GF.FormateDate(TAFVM.FromDate), GF.FormateDate(TAFVM.ToDate), TAFVM.DocNo, TAFVM.UnloadBy, TAFVM.RateType, BaseLocationCode));
        }

        public ActionResult PRSArrivalDetails(string id, string RateType, string UnloadBy)
        {
            PRSArrivalsViewModel PRSAVM = new PRSArrivalsViewModel();
            ViewBag.Type = id;
            try
            {
                if (id != "0")
                {
                    PRSAVM.PAVM = OS.GetPDCDetailsObject(id, RateType, UnloadBy, BaseLocationCode).FirstOrDefault();
                    PRSAVM.ListPAVM = OS.GetPDCDetailsObject(id, RateType, UnloadBy, BaseLocationCode).ToList();
                    PRSAVM.PAVM.UnloadBy = UnloadBy;
                    PRSAVM.PAVM.RateType = RateType;
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }

            return View(PRSAVM);
        }

        [HttpPost]
        public ActionResult PRSArrivalDetails(PRSArrivalsViewModel PRSAVM, List<PRSArrivalsDetailViewModel> PDCDetail)
        {
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();

            DataSet Ds = new DataSet();

            var PDCNo = "";
            var Tot_Charge = "";
            var TranXaction = "";

            if (!PRSAVM.PAVM.IsMonthly)
            {
                if ((PRSAVM.PAVM.UnloadBy == "A" || PRSAVM.PAVM.UnloadBy == "XX5" || PRSAVM.PAVM.UnloadBy == "XX8" || PRSAVM.PAVM.UnloadBy == "M") && PRSAVM.PAVM.LoadingCharge == 0)
                {
                    ViewBag.StrError = "UnLoading Charge cannot be 0.";
                    return View("Error");
                    //return RedirectToAction("ErrorTransaction", "Home", new { Message = ErrorMsg });
                }
            }

            try
            {
                //Dt = OS.UpdatePRSArrival(PRSAVM);
                string PRS_DETAILS = "<root>";
                foreach (var item in PDCDetail)
                {
                    PRS_DETAILS = PRS_DETAILS + "<DOCKETS>";
                    PRS_DETAILS = PRS_DETAILS + "<PDCNO>" + PRSAVM.PAVM.PDCNO + "</PDCNO>";
                    PRS_DETAILS = PRS_DETAILS + "<DOCKNO>" + item.DOCKNO + "</DOCKNO>";
                    PRS_DETAILS = PRS_DETAILS + "<URate>" + item.NewRate + "</URate>";
                    PRS_DETAILS = PRS_DETAILS + "<URateType>" + item.ratetype1 + "</URateType>";
                    PRS_DETAILS = PRS_DETAILS + "</DOCKETS>";
                }
                PRS_DETAILS = PRS_DETAILS + "</root>";
                Ds = OS.UpdatePRSArrival(PRS_DETAILS, PRSAVM, BaseUserName.ToUpper());

                if (Ds.Tables[0] != null && Ds.Tables[0].Rows.Count > 0 && Ds.Tables[0].Rows[0]["TranXaction"].ToString().ToUpper() == "DONE")
                {
                    TranXaction = "Done";
                    PDCNo = Ds.Tables[1].Rows[0]["PDCNO"].ToString();
                    Tot_Charge = Ds.Tables[1].Rows[0]["UnLoadingCharges"].ToString();
                }


            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                trn.Rollback();
                con.Close();
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("PRSArrivalDone", new { PDCNo = PDCNo, Tot_Charge = Tot_Charge, TranXaction = TranXaction });
        }

        public ActionResult PRSArrivalDone(string PDCNo, string Tot_Charge, string TranXaction)
        {
            ViewBag.PDCNo = PDCNo;
            ViewBag.TranXaction = TranXaction;
            ViewBag.Tot_Charge = Tot_Charge;
            return View();
        }

        #endregion

        #region GRN Generation Assign

        public ActionResult GRNGenerationAssign()
        {
            FuelBillEntyQuery objViewModel = new FuelBillEntyQuery();
            ViewBag.TypeAsset = 1;
            TempData["GRN"] = "GRN";
            return View("GRNGenerationAssign", objViewModel);
        }

        public ActionResult GRNGenerationAssignList(FuelBillEntyQuery FBE)
        {
            BABillEnty BBE = new BABillEnty();
            FBE.Location = BaseLocationCode;
            BBE.POList = OS.GetGRNGenerationList(FBE);
            return View("GRNGenerationAssignList", BBE);
        }

        public ActionResult GRNGenerationAssignSubmit(FuelBillEntyQuery objViewModel, List<POBillEntry> WFTDLIST)
        {
            try
            {
                List<POBillEntry> ListWFTDLIST = new List<POBillEntry>();
                //int SKUID =0;
                string SKUID = "";


                string Baseyear = BaseFinYear.Split('-')[0].ToString();
                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        SKUID = item.id.ToString();
                    }
                }

                // int countQty = 0 ;

                string XML_Det = "<root>";

                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        POBillEntry WFTD = new POBillEntry();

                        if (item.IsEnabled)
                        {
                            XML_Det = XML_Det + "<WEBX_GRN_DET>";

                            //countQty = countQty.Count();

                            XML_Det = XML_Det + "<GRNCODE>" + item.GRNNO + "</GRNCODE>";
                            XML_Det = XML_Det + "<GRNDATE>" + item.GRNDT + "</GRNDATE>";
                            XML_Det = XML_Det + "<IteamNameNdCode>" + item.IteamName + "</IteamNameNdCode>";
                            XML_Det = XML_Det + "<QTY>" + item.qty + "</QTY>";
                            XML_Det = XML_Det + "<AssignToLocation>" + item.AssignToLoca + "</AssignToLocation>";
                            XML_Det = XML_Det + "<Description>" + item.Description + "</Description>";
                            XML_Det = XML_Det + "<DepartmentName>" + item.DpartName + "</DepartmentName>";
                            XML_Det = XML_Det + "<IssueDate>" + item.InstallDate + "</IssueDate>";
                            XML_Det = XML_Det + "<Employee>" + item.Employee + "</Employee>";

                            XML_Det = XML_Det + "</WEBX_GRN_DET>";

                            // ListWFTDLIST.Add(WFTD);
                        }
                    }
                }
                XML_Det = XML_Det + "</root>";
                DataTable DT = new DataTable();
                foreach (var item in WFTDLIST)
                {
                    if (item.IsEnabled)
                    {
                        if (SKUID == "")
                            SKUID = item.id.ToString();
                        else
                            SKUID = SKUID + "," + item.id.ToString();

                    }
                }
                DT = OS.InsertGRNGenerationAssign(XML_Det, BaseLocationCode, BaseUserName, Baseyear, SKUID);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();
                string ASSIGNNO = DT.Rows[0]["No"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("GRNGenerationAssignDone", new { ASSIGNNO = ASSIGNNO, Type = "1" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult GRNGenerationAssignDone(string ASSIGNNO, string Type)
        {
            ViewBag.ASSIGNNO = ASSIGNNO;
            ViewBag.Type = Type;

            return View();
        }

        #endregion

        #region GRN Generation ReAssign

        public ActionResult GRNGenerationReAssign()
        {
            return View();
        }

        public ActionResult GRNGenerationReAssignList(FuelBillEntyQuery FBE)
        {
            BABillEnty BBE = new BABillEnty();
            try
            {
                BBE.POList = OS.GRNGenerationReAssignList(FBE);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return PartialView("GRNGenerationReAssignList", BBE);

        }

        public ActionResult GRNGenerationReAssignSubmit(FuelBillEntyQuery objViewModel, List<POBillEntry> WFTDLIST)
        {
            try
            {
                List<POBillEntry> ListWFTDLIST = new List<POBillEntry>();
                string SKUID = "";


                string Baseyear = BaseFinYear.Split('-')[0].ToString();
                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        if (item.IsEnabled)
                        {
                            if (SKUID == "")

                                SKUID = item.id.ToString();

                            else
                            {
                                SKUID = SKUID + "," + item.id.ToString();
                            }

                        }
                    }
                }

                // int countQty = 0 ;

                string XML_Det = "<root>";

                if (WFTDLIST != null)
                {
                    foreach (var item in WFTDLIST)
                    {
                        POBillEntry WFTD = new POBillEntry();

                        if (item.IsEnabled)
                        {
                            XML_Det = XML_Det + "<WEBX_GRN_DET>";

                            //countQty = countQty.Count();

                            XML_Det = XML_Det + "<GRNCODE>" + item.GRNNO + "</GRNCODE>";
                            XML_Det = XML_Det + "<GRNDATE>" + item.GRNDT + "</GRNDATE>";
                            XML_Det = XML_Det + "<IteamNameNdCode>" + item.IteamName + "</IteamNameNdCode>";
                            XML_Det = XML_Det + "<QTY>" + item.qty + "</QTY>";
                            XML_Det = XML_Det + "<ReassignToLocation>" + item.AssignToLoca + "</ReassignToLocation>";
                            XML_Det = XML_Det + "<Description>" + item.Description + "</Description>";
                            XML_Det = XML_Det + "<DepartmentName>" + item.DpartName + "</DepartmentName>";
                            XML_Det = XML_Det + "<IssueDate>" + item.Issuedate + "</IssueDate>";
                            XML_Det = XML_Det + "<Employee>" + item.Employee + "</Employee>";

                            XML_Det = XML_Det + "</WEBX_GRN_DET>";

                            // ListWFTDLIST.Add(WFTD);
                        }
                    }
                }
                XML_Det = XML_Det + "</root>";
                DataTable DT = new DataTable();

                DT = OS.InsertGRNGenerationReAssign(XML_Det, BaseLocationCode, BaseUserName, Baseyear, SKUID);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();
                string ASSIGNNO = DT.Rows[0]["No"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("GRNGenerationAssignDone", new { ASSIGNNO = ASSIGNNO, Type = "2" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region GRN Generation ReAssign Assign

        public ActionResult GRNGenerationViewPrint()
        {
            return View();
        }

        public ActionResult GRNGenerationViewPrintList(GRNGenerationReAssignviewModel objViewModel)
        {
            try
            {
                if (objViewModel.UitilityType == "1" || objViewModel.UitilityType == "2" || objViewModel.UitilityType == "3")
                {
                    List<CYGNUS_FixAssetType> optrkList = OS.Get_GRNASSIGNREASSIGNVIEWPRINPrintListView(objViewModel.DocumentNo, GF.FormateDate(objViewModel.FromDate), GF.FormateDate(objViewModel.ToDate), objViewModel.UitilityType);
                    objViewModel.GRNLIST = optrkList;
                    if (objViewModel.UitilityType == "1")
                    {
                        ViewBag.ASSETTYPE = "ASSIGN";
                        ViewBag.Type = "1";
                        ViewBag.Header = "GRN Genration ASSIGN View Print List";

                    }
                    else if (objViewModel.UitilityType == "2")
                    {
                        ViewBag.ASSETTYPE = "REASSIGN";
                        ViewBag.Header = "GRN Genration REASSIGN View Print List";
                        ViewBag.Type = "2";

                    }
                    else if (objViewModel.UitilityType == "3")
                    {
                        ViewBag.ASSETTYPE = "BILLENTRYPAYMENT";
                        ViewBag.Header = "GRN Genration BILL ENTRY PAYMENT View Print List";
                        ViewBag.Type = "3";

                    }
                }

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }


            return View(objViewModel);
        }

        #endregion

        #region PO Advance (Fix asset Po Advance)

        public ActionResult POAdvanceCriteria()
        {
            return View();
        }

        public JsonResult GetPODetails(string PoNo)
        {
            DataTable DT = new DataTable();
            DT = OS.ChecPONo(PoNo);
            string cnt = DT.Rows[0]["cnt"].ToString();
            string MSG = DT.Rows[0]["msg"].ToString();

            return new JsonResult()
            {
                Data = new
                {
                    Flg = cnt,
                    MSG = MSG
                }
            };
        }

        public ActionResult POAdvanceDetails(FuelBillEntyQuery FBE)
        {
            POAdvancePayment PAP = new POAdvancePayment();
            PaymentControl ObjPayment = new PaymentControl();
            PAP.PC = ObjPayment;
            DataTable DT = OS.GetPOAdvancData(FBE.PoNo, GF.FormateDate(FBE.FromDate), GF.FormateDate(FBE.ToDate));
            PAP.pocode = DT.Rows[0]["pocode"].ToString();
            PAP.Vendorname = DT.Rows[0]["VendorName"].ToString();
            PAP.Podate = DT.Rows[0]["podate"].ToString();
            PAP.TotalQty = Convert.ToDecimal(DT.Rows[0]["totalqty"]);
            PAP.PO_AMOUNT = Convert.ToDecimal(DT.Rows[0]["totalamt"]);
            return View(PAP);
        }

        public ActionResult POAdvanceSubmit(POAdvancePayment objViewModel, PaymentControl ObjPayment)
        {
            string Adv_voucherno = "", Status = "";
            try
            {
                DataTable dt = new DataTable();
                decimal pendamt = objViewModel.PO_AMOUNT - objViewModel.PO_advamt;
                string XML_Chq_Det = "";
                if (ObjPayment.PaymentMode == "Bank" || ObjPayment.PaymentMode == "Both")
                {
                    XML_Chq_Det = "<root>";
                    XML_Chq_Det = XML_Chq_Det + "<Chq_Det>";
                    XML_Chq_Det = XML_Chq_Det + "<chqno>" + ObjPayment.ChequeNo + "</chqno>";
                    XML_Chq_Det = XML_Chq_Det + "<chqdt>" + GF.FormateDate(ObjPayment.ChequeDate) + "</chqdt>";
                    XML_Chq_Det = XML_Chq_Det + "<chqamt>" + ObjPayment.ChequeAmount + "</chqamt>";
                    XML_Chq_Det = XML_Chq_Det + "<banknm>" + ObjPayment.BankLedgerName + "</banknm>";
                    XML_Chq_Det = XML_Chq_Det + "<ptmsptcd>" + objViewModel.Vendorname.Split('~')[1].Trim() + "</ptmsptcd>";
                    XML_Chq_Det = XML_Chq_Det + "<ptmsptnm>" + objViewModel.Vendorname.Split('~')[0].Trim() + "</ptmsptnm>";
                    XML_Chq_Det = XML_Chq_Det + "<empcd>" + BaseUserName + "</empcd>";
                    XML_Chq_Det = XML_Chq_Det + "<empnm>" + BaseUserName + "</empnm>";
                    XML_Chq_Det = XML_Chq_Det + "<brcd>" + BaseLocationCode + "</brcd>";
                    XML_Chq_Det = XML_Chq_Det + "<brnm>" + BaseLocationCode + "</brnm>";
                    XML_Chq_Det = XML_Chq_Det + "<OwnCust>" + "O" + "</OwnCust>";
                    XML_Chq_Det = XML_Chq_Det + "<adjustamt>" + ObjPayment.ChequeAmount + "</adjustamt>";
                    XML_Chq_Det = XML_Chq_Det + "<voucherNo>" + Adv_voucherno + "</voucherNo>";
                    XML_Chq_Det = XML_Chq_Det + "<transdate>" + objViewModel.Podate + "</transdate>";
                    XML_Chq_Det = XML_Chq_Det + "<acccode>" + ObjPayment.BankLedger + "</acccode>";
                    XML_Chq_Det = XML_Chq_Det + "<DepoFlag>" + "Y" + "</DepoFlag>";
                    XML_Chq_Det = XML_Chq_Det + "<depoloccode>" + BaseLocationCode + "</depoloccode>";
                    XML_Chq_Det = XML_Chq_Det + "</Chq_Det>";
                    XML_Chq_Det = XML_Chq_Det + "</root>";
                    //DataTable dt1 = OS.usp_chq_Details(XML_Chq_Det);
                }
                else
                {
                    XML_Chq_Det = "<root>";
                    XML_Chq_Det = XML_Chq_Det + "</root>";
                }
                DataTable dtPO = OS.sp_Insert_PurchseOrderDetail_For_Advance_Payment(objViewModel.pocode, objViewModel.PO_AMOUNT, objViewModel.PO_advamt, pendamt, ObjPayment.ChequeAmount, ObjPayment.CashAmount, GF.FormateDate(ObjPayment.ChequeDate), ObjPayment.PaymentMode, ObjPayment.ChequeNo, ObjPayment.BankLedger, ObjPayment.CashLedger, BaseUserName, BaseFinYear, BaseCompanyCode, XML_Chq_Det);
                Status = dtPO.Rows[0]["MESSAGE"].ToString();
                Adv_voucherno = dtPO.Rows[0]["voucherno"].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("POAdvanceDone", new { Adv_voucherno = Adv_voucherno, PONO = objViewModel.pocode, Status = Status });
        }

        public ActionResult POAdvanceDone(string Adv_voucherno, string PONO, string Status)
        {
            ViewBag.Adv_voucherno = Adv_voucherno;
            ViewBag.PONO = PONO;
            ViewBag.Status = Status;

            return View();
        }

        #endregion

        #region PRQ

        public ActionResult PRQCriteria()
        {
            PRQViewModel PVW = new PRQViewModel();
            return View(PVW);
        }

        public ActionResult AddPRQRequest()
        {
            PRQViewModel PVW = new PRQViewModel();
            return View(PVW);
        }

        public ActionResult PRQList(PRQViewModel VW)
        {
            DataTable DT = OS.GetPRQHistoryDT(GF.FormateDate(VW.FromDate), GF.FormateDate(VW.ToDate), VW.PRQNo, VW.CustomerCode, VW.Driver, VW.SuperVisor, VW.PRQ_Status);
            VW.ListPRQHistory = DataRowToObject.CreateListFromTable<CYGNUSPRQHistory>(DT);

            return PartialView("_PRQStatusList", VW.ListPRQHistory);
        }

        public ActionResult QuicketCnoteGeneration(string No)
        {
            PRQViewModel VW = new PRQViewModel();
            DataTable DT = OS.GetPRQHistoryDT(null, null, No, null, null, null, null);
            VW.ObjPRQ = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(DT).First();
            if (!VW.ObjPRQ.IsDocketGenerated)
            {
                VW.ObjQuickDktGen = new PRQQuickDocketGeneration();
                return View(VW);
            }
            else
            {
                ViewBag.StrError = "Docket Allready Generated.";
                return View("Error");
            }
        }

        public ActionResult QuicketCnoteGenerationSubmit(PRQViewModel VW)
        {
            try
            {
                /*
                INSERT INTO WebX_Setup_Document_Nomenclature
                VALUES('EDOCK','','N',5	,'',	2	,'N',	'N',	'N',	'0')
                 */
                string DocketXML = "<quick><docket>";
                DocketXML = DocketXML + "<prqno>" + VW.ObjPRQ.PRQNo + "</prqno>";
                DocketXML = DocketXML + "<dockno>" + VW.ObjPRQ.DockNo + "</dockno>";
                DocketXML = DocketXML + "<dockdt>" + VW.ObjPRQ.PRQDate + "</dockdt>";
                DocketXML = DocketXML + "<party_code>" + VW.ObjPRQ.CustomerCode + "</party_code>";
                DocketXML = DocketXML + "<from_loc>" + VW.ObjPRQ.City + "</from_loc>";
                DocketXML = DocketXML + "<orgncd>" + VW.ObjPRQ.BRCD + "</orgncd>";
                DocketXML = DocketXML + "<destcd>" + VW.ObjPRQ.Destcd + "</destcd>";
                DocketXML = DocketXML + "<pkgsno>" + VW.ObjPRQ.ActualPKGS + "</pkgsno>";
                DocketXML = DocketXML + "<actuwt>" + VW.ObjPRQ.ActualWeight + "</actuwt>";
                DocketXML = DocketXML + "<chrgwt>0</chrgwt>";
                DocketXML = DocketXML + "<entryby>" + BaseUserName + "</entryby>";
                DocketXML = DocketXML + "<brcd>" + BaseLocationCode + "</brcd>";
                DocketXML = DocketXML + "</docket></quick>";

                DataTable DT = OS.SubmitPRQ(DocketXML, BaseFinYear, BaseUserName, BaseCompanyCode, BaseLocationCode);

                string Message = "", Status = "", DocketNo = "";

                Message = DT.Rows[0]["Message"].ToString();
                Status = DT.Rows[0]["Status"].ToString();

                if (Message.ToUpper() == "DONE" && Status.ToUpper() == "1")
                {
                    DocketNo = DT.Rows[0]["DockNo"].ToString();
                    return RedirectToAction("QuickDocketDone", new { DockNo = DocketNo, PRQNo = VW.ObjPRQ.PRQNo });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult QuickDocketDone(string DockNo, string PRQNo)
        {
            ViewBag.DockNo = DockNo;
            ViewBag.PRQNo = PRQNo;
            return View();
        }

        #endregion

        #region Quicket Docket Completion

        public ActionResult QuicketDocketCompletion(string DockNo, string PrqNo)
        {
            try
            {
                DocketViewModel DVM = new DocketViewModel();

                WebX_Master_Docket_Charges WMDC = new WebX_Master_Docket_Charges();
                WebX_Master_Docket WMD = new WebX_Master_Docket();
                List<WebX_Master_Docket_Invoice> ListInVoice = new List<WebX_Master_Docket_Invoice>();
                List<QuickDocketBalancecharges> ListCharges = new List<QuickDocketBalancecharges>();
                DVM.WMD = OS.Get_master_docket(DockNo).FirstOrDefault();
                DataTable DTPrq = OS.GetPRQHistoryDT(null, null, PrqNo, null, null, null, null);
                DVM.ObjPRQ = DataRowToObject.CreateListFromTable<CYGNUSPickupRequest>(DTPrq).First();

                if (DVM.ObjPRQ.IsDocketCompleted)
                {
                    ViewBag.StrError = "Docket Allready Completed.";
                    return View("Error");
                }

                if (ListInVoice.Count() == 0)
                {
                    WebX_Master_Docket_Invoice CMDI = new WebX_Master_Docket_Invoice();
                    CMDI.SrNo = 1;
                    CMDI.INVDT = System.DateTime.Now;
                    CMDI.DECLVAL = 0;
                    CMDI.PKGSNO = DVM.ObjPRQ.ActualPKGS;
                    CMDI.ACTUWT = DVM.ObjPRQ.ActualWeight;
                    CMDI.VOL_L = 0;
                    CMDI.VOL_B = 0;
                    CMDI.VOL_H = 0;
                    ListInVoice.Add(CMDI);
                    CMDI = new WebX_Master_Docket_Invoice();
                }

                DVM.WMD.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
                DVM.WMD.FlagStaxBifer = MS.GetRuleObject("STAX_BIFER").defaultvalue.IndexOf(BaseCompanyCode) >= 0 ? "Y" : "N";
                DVM.WMD.FLAG_PRORATA = MS.GetRuleObject("FLAG_PRORATA").defaultvalue;
                //DVM.WMD = WMD;
                DVM.WMDC = WMDC;
                DVM.ListInVoice = ListInVoice;
                ListCharges = OS.GetDocketChargesDetails("");
                DVM.ListCharges = ListCharges;
                DVM.IsCompletion = true;

                //START GST Changes Chirag D 
                string ChargesStr = "EXEC USP_GetDynamicChargesNew 'DKT'";
                DataTable Dt = GF.GetDataTableFromSP(ChargesStr);
                List<CygnusChargesHeader> GetCCHList = DataRowToObject.CreateListFromTable<CygnusChargesHeader>(Dt);
                DVM.ListCCH = GetCCHList;
                //END GST Changes Chirag D

                return View("Docket", DVM);
            }
            catch (Exception)
            {
                ViewBag.StrError = "Docket Allready Completed.";
                return View("Error");
            }
        }

        #endregion

        #region Docket HO Verification

        public ActionResult DocketHOVerificationCriteria()
        {
            return View();
        }

        public ActionResult DocketHOVerificationList(BillingFilterViewModel model)
        {
            DocketHoVerificationViewModel VM = new DocketHoVerificationViewModel();
            List<WebX_Master_Docket> DocketList = OS.GetDocketForHOVerification(model);
            VM.DocketList = DocketList;
            VM.DATEFROM = GF.FormateDate(model.FromDate);
            VM.DATETO = GF.FormateDate(model.ToDate);
            VM.PartyCode = model.BillingParty;
            VM.PartyName = MS.GetCustomerMasterObject().Where(c => c.CUSTCD == model.BillingParty).FirstOrDefault().CUSTNM;
            return View(VM);
        }

        public ActionResult DocketHOVerificationSubmit(List<WebX_Master_Docket> DocketList)
        {
            String hidDockno = "";
            try
            {
                if (DocketList != null && DocketList.Count() > 0)
                {
                    foreach (var item in DocketList.Where(c => c.IsHOVerified == true))
                    {
                        DataTable objDT = new DataTable();
                        objDT = OS.DocketHOVerificationSubmit(item.DOCKNO, BaseUserName);

                        if (hidDockno == "")
                        {
                            hidDockno = item.DOCKNO;
                        }
                        else
                        {
                            hidDockno = hidDockno + "," + item.DOCKNO;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("HOVerifyDone", new { VerifyDocno = hidDockno });
        }

        public ActionResult HOVerifyDone(string VerifyDocno)
        {
            ViewBag.VerifyDocno = VerifyDocno;
            return View();
        }

        #endregion

        #region Internal Movement

        public ActionResult InternalMovement()
        {
            InternalMovementViewModel IMVM = new InternalMovementViewModel();
            CYGNUS_Internal_Movement objIM = new CYGNUS_Internal_Movement();
            objIM.ORGNCD = BaseLocationCode;
            IMVM.objIntMovement = objIM;

            return View(IMVM);
        }

        public ActionResult InternalMovementSubmit(InternalMovementViewModel IMVM)
        {
            try
            {
                string BRCD = "", UserName = "", Message = "", IMNO = "";
                int Status = 0;
                BRCD = BaseLocationCode; UserName = BaseUserName;

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(IMVM.objIntMovement.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, IMVM.objIntMovement);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable dtIntMovement = OS.Internal_Movement_Insert(xmlDoc1.InnerXml, BRCD, UserName, basefinyear);

                Status = Convert.ToInt32(dtIntMovement.Rows[0]["Status"]);
                Message = Convert.ToString(dtIntMovement.Rows[0]["Message"]);
                IMNO = Convert.ToString(dtIntMovement.Rows[0]["No"]);

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("InternalMovementDone", new { IMNO = IMNO });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public ActionResult InternalMovementDone(string IMNO)
        {
            ViewBag.IMNO = IMNO;
            return View();
        }

        #endregion

        #region DeliveryMR

        public ActionResult DeliveryMR_Criteria()
        {
            DeliveryMR_Criteria SCUVM = new DeliveryMR_Criteria();
            return View(SCUVM);
        }

        public JsonResult IsDocketEligibleForDelMR(string dockno, string docksf)
        {
            string Flg = "1", MSG = "";
            bool flag;
            try
            {
                flag = OS.IsDocketEligibleForDelMR(dockno, docksf, BaseLocationCode, BaseYearValFirst);

                if (MSG.ToUpper() == "")
                {
                    Flg = "1";
                }
            }
            catch (Exception excp)
            {
                Flg = "0";
                MSG = excp.Message;
            }

            return new JsonResult()
            {
                Data = new
                {

                    Flg = Flg,
                    MSG = MSG
                }
            };

        }

        public ActionResult DeliveryMR_Generate(DeliveryMR_Criteria objCriteria)
        {
            DeliveryViewModel DVM = new DeliveryViewModel();
            List<vw_DeliveryMR> DtlList = new List<vw_DeliveryMR>();

            DtlList = OS.GetDeliveryMR_DocketDetails(objCriteria.DOCKNO, objCriteria.DOCKSF);
            DVM.Dtl = DtlList.FirstOrDefault();
            DVM.Dtl.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
            ViewBag.AllowPartialPayment = DVM.Dtl.AllowPartialPayment;

            if (DVM.Dtl.ChargeRule.CompareTo("NONE") == 0)
                DVM.Dtl.BaseCode1 = "NONE";
            else if (DVM.Dtl.ChargeRule.CompareTo("SVCTYP") == 0)
                DVM.Dtl.BaseCode1 = DVM.Dtl.service_class;
            else if (DVM.Dtl.ChargeRule.CompareTo("BUT") == 0)
                DVM.Dtl.BaseCode1 = DVM.Dtl.businesstype;

            var chargesubrule = OS.GetChargeSubRule("DKT", DVM.Dtl.ChargeRule, DVM.Dtl.BaseCode1);
            if (chargesubrule.CompareTo("PROD") == 0)
                DVM.Dtl.BaseCode2 = DVM.Dtl.prodcd;
            else if (chargesubrule.CompareTo("NONE") == 0)
                DVM.Dtl.BaseCode2 = "NONE";
            /******************************************************************************************************/

            DVM.Dtl.flagroundoff = MS.GetRuleObject("DELMR_FLAG_ROUND").defaultvalue;
            //hdncontractid.Value = DocketController.GetContractID(txtpartycode.Text, hdnpaybas.Value, WebXConvert.ToDateTime(txtmrdate.Text, "en-GB").ToString("dd MMM yyyy"));

            List<BookingCharges> ListBookingCharges = new List<BookingCharges>();
            ListBookingCharges = OS.GetBookingCharges(DVM.Dtl.DOCKNO, DVM.Dtl.ChargeRule, DVM.Dtl.service_class, DVM.Dtl.businesstype, DVM.Dtl.doctype, DVM.Dtl.chrgwt).ToList();
            DVM.ListBookingCharges = ListBookingCharges;

            foreach (BookingCharges obj in ListBookingCharges)
            {
                obj.paybas = DVM.Dtl.paybas;
            }

            List<DeliveryCharges> ListDeliveryCharges = new List<DeliveryCharges>();
            ListDeliveryCharges = OS.GetDeliveryCharges(DVM.Dtl.DOCKNO, DVM.Dtl.ChargeRule, DVM.Dtl.service_class, DVM.Dtl.businesstype, DVM.Dtl.doctype).ToList();
            DVM.ListDeliveryCharges = ListDeliveryCharges;

            var ChargeType = (string.IsNullOrEmpty(DVM.Dtl.Moduletype) ? "DKT" : DVM.Dtl.Moduletype);

            //string ChargesStr = "EXEC USP_GetDynamicChargesNew 'DKT'";
            string ChargesStr = "EXEC USP_GetDynamicChargesNew '" + ChargeType + "'";
            DataTable Dt = GF.GetDataTableFromSP(ChargesStr);
            List<CygnusChargesHeader> GetCCHList = DataRowToObject.CreateListFromTable<CygnusChargesHeader>(Dt);
            DVM.ListCCH = GetCCHList;

            DVM.CCP = new CYGNUS.ViewModel.CYGNUS_Collection_Payment();
            DVM.CCP.ChequeDate = System.DateTime.Now;
            //DVM.MRBranch = BaseLocationCode;
            //DVM.CCP.PayAmount = BillList.Sum(c => c.PENDAMT);

            return View(DVM);
        }

        public ActionResult DeliveryMR_Submit(DeliveryViewModel DVM, List<BookingCharges> BookingChargesList, List<DeliveryCharges> DeliveryChargesList, List<CygnusChargesHeader> DynamicList, CYGNUS_Collection_Payment CCP)
        {
            //string fromdate = Convert.ToDateTime(DVM.Dtl.DOCKDT).ToString("dd MMM yyyy");
            //string todate = Convert.ToDateTime(DVM.Dtl.CDELDT).ToString("dd MMM yyyy");
            //DateTime DocketDate = Convert.ToDateTime(fromdate);
            //DateTime EDDDate = Convert.ToDateTime(todate);
            try
            {

                #region GST Charges

                XmlDocument xmlDocCha = new XmlDocument();
                XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());

                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                    xmlStreamCha.Position = 0;
                    xmlDocCha.Load(xmlStreamCha);
                }

                DeliveryMR_GSTCharges objGSTCharges = new DeliveryMR_GSTCharges();
                string DynamicChargdeXML = "EXEC USP_GetPivotDynamicCharges '" + xmlDocCha.InnerXml + "'";
                DataTable DTCharges = GF.GetDataTableFromSP(DynamicChargdeXML);
                objGSTCharges = DataRowToObject.CreateListFromTable<DeliveryMR_GSTCharges>(DTCharges).First();

                #endregion

                #region MR_Entry
                string strMR_Entry = "";
                strMR_Entry = "<root><mr_entry>";
                strMR_Entry = strMR_Entry + "<entryby>" + BaseUserName + "</entryby>";
                strMR_Entry = strMR_Entry + "<mrssf>.</mrssf>";
                strMR_Entry = strMR_Entry + "<mrstype>" + DVM.Dtl.mrstype + "</mrstype>";
                strMR_Entry = strMR_Entry + "<mrsdt>" + GF.FormateDate(DVM.Dtl.MRDate) + "</mrsdt>";
                strMR_Entry = strMR_Entry + "<mrsbr>" + BaseLocationCode + "</mrsbr>";
                strMR_Entry = strMR_Entry + "<dockno>" + DVM.Dtl.DOCKNO + "</dockno>";
                strMR_Entry = strMR_Entry + "<docksf>" + DVM.Dtl.DOCKSF + "</docksf>";
                strMR_Entry = strMR_Entry + "<dockdt>" + DVM.Dtl.dockdate.ToString("dd MMM yyyy hh:mm:ss") + "</dockdt>";
                strMR_Entry = strMR_Entry + "<ptcd>" + DVM.Dtl.csgecd + "</ptcd>";
                strMR_Entry = strMR_Entry + "<ptname>" + DVM.Dtl.csgenm + "</ptname>";
                strMR_Entry = strMR_Entry + "<CSGEmobileno>" + DVM.Dtl.CSGEmobileno + "</CSGEmobileno>";
                strMR_Entry = strMR_Entry + "<CSGETeleNo>" + DVM.Dtl.CSGETeleNo + "</CSGETeleNo>";
                strMR_Entry = strMR_Entry + "<oldptcd>" + DVM.Dtl.oldptcd + "</oldptcd>";
                strMR_Entry = strMR_Entry + "<oldptname>" + DVM.Dtl.oldptname + "</oldptname>";
                strMR_Entry = strMR_Entry + "<destcd>" + DVM.Dtl.delloc + "</destcd>";
                strMR_Entry = strMR_Entry + "<nopkgs>" + DVM.Dtl.pkgsno + "</nopkgs>";
                strMR_Entry = strMR_Entry + "<PartPkgQty>" + DVM.Dtl.PartPkgQty + "</PartPkgQty>";
                strMR_Entry = strMR_Entry + "<chrgwt>" + DVM.Dtl.chrgwt + "</chrgwt>";
                strMR_Entry = strMR_Entry + "<paybas>" + DVM.Dtl.paybas + "</paybas>";
                strMR_Entry = strMR_Entry + "<AllowPartPayment>" + DVM.Dtl.IsAllowPartPayment + "</AllowPartPayment>";

                //strMR_Entry = strMR_Entry + "<svctax_rate>" + "" + "</svctax_rate>";
                //strMR_Entry = strMR_Entry + "<cess_rate>" + "" + "</cess_rate>";
                //strMR_Entry = strMR_Entry + "<h_cess_rate>" + "" + "</h_cess_rate>";
                //strMR_Entry = strMR_Entry + "<col_stax>" + "" + "</col_stax>";
                //strMR_Entry = strMR_Entry + "<col_cess>" + "" + "</col_cess>";
                //strMR_Entry = strMR_Entry + "<col_sbctax>" + "" + "</col_sbctax>";
                //strMR_Entry = strMR_Entry + "<sbctax_rate>" + "" + "</sbctax_rate>";
                //strMR_Entry = strMR_Entry + "<col_kkctax>" + "" + "</col_kkctax>";
                //strMR_Entry = strMR_Entry + "<kkctax_rate>" + "" + "</kkctax_rate>";
                //strMR_Entry = strMR_Entry + "<col_h_cess>" + "" + "</col_h_cess>";


                strMR_Entry = strMR_Entry + "<col_frt>" + CCP.NetPayabale + "</col_frt>";
                strMR_Entry = strMR_Entry + "<IGSTRate>" + objGSTCharges.IGSTRate + "</IGSTRate>";
                strMR_Entry = strMR_Entry + "<IGSTCOLLECTED>" + objGSTCharges.IGSTCOLLECTED + "</IGSTCOLLECTED>";
                strMR_Entry = strMR_Entry + "<CGSTRate>" + objGSTCharges.CGSTRate + "</CGSTRate>";
                strMR_Entry = strMR_Entry + "<CGSTCOLLECTED>" + objGSTCharges.CGSTCOLLECTED + "</CGSTCOLLECTED>";
                strMR_Entry = strMR_Entry + "<SGSTRate>" + objGSTCharges.SGSTRate + "</SGSTRate>";
                strMR_Entry = strMR_Entry + "<SGSTCOLLECTED>" + objGSTCharges.SGSTCOLLECTED + "</SGSTCOLLECTED>";
                strMR_Entry = strMR_Entry + "<UTGSTRate>" + objGSTCharges.UTGSTRate + "</UTGSTRate>";
                strMR_Entry = strMR_Entry + "<UTGSTCOLLECTED>" + objGSTCharges.UTGSTCOLLECTED + "</UTGSTCOLLECTED>";

                strMR_Entry = strMR_Entry + "<mrsamt>" + DVM.Dtl.MRAmount + "</mrsamt>";
                strMR_Entry = strMR_Entry + "<mrs_closed>" + "Y" + "</mrs_closed>";
                strMR_Entry = strMR_Entry + "<mr_cancel>" + "N" + "</mr_cancel>";
                strMR_Entry = strMR_Entry + "<mrcollbrcd>" + DVM.Dtl.delloc + "</mrcollbrcd>";

                strMR_Entry = strMR_Entry + "<finclosedt>" + DVM.Dtl.MRDate.ToString("dd MMM yyyy hh:mm:ss") + "</finclosedt>";
                strMR_Entry = strMR_Entry + "<company_code>" + BaseCompanyCode + "</company_code>";

                if (CCP.TransactionType.ToUpper() == "CASH")
                {
                    strMR_Entry = strMR_Entry + "<paymode>" + CCP.TransactionType + "</paymode>";
                    strMR_Entry = strMR_Entry + "<mrscash>" + CCP.CashAmount + "</mrscash>";
                    strMR_Entry = strMR_Entry + "<mrschq>0</mrschq>";
                    strMR_Entry = strMR_Entry + "<mrschqdt>" + GF.FormateDate(CCP.ChequeDate) + "</mrschqdt>";
                    strMR_Entry = strMR_Entry + "<mrschqno></mrschqno>";
                    strMR_Entry = strMR_Entry + "<mrsbank></mrsbank>";
                    strMR_Entry = strMR_Entry + "<bankacccode></bankacccode>";
                    strMR_Entry = strMR_Entry + "<flagchqdepo>N</flagchqdepo>";
                }
                else
                {
                    strMR_Entry = strMR_Entry + "<paymode>CHEQUE</paymode>";
                    strMR_Entry = strMR_Entry + "<mrscash>0</mrscash>";
                    strMR_Entry = strMR_Entry + "<mrschq>" + CCP.ChequeAmount + "</mrschq>";
                    strMR_Entry = strMR_Entry + "<mrschqdt>" + CCP.ChequeDate + "</mrschqdt>";
                    strMR_Entry = strMR_Entry + "<mrschqno>" + CCP.ChequeNo + "</mrschqno>";
                    strMR_Entry = strMR_Entry + "<mrsbank>" + CCP.ReceivedBankName + "</mrsbank>";
                    strMR_Entry = strMR_Entry + "<bankacccode>" + CCP.BankAccountCode + "</bankacccode>";
                    strMR_Entry = strMR_Entry + "<flagchqdepo>" + (CCP.IsChequeDirectDeposited == true ? "Y" : "N") + "</flagchqdepo>";
                }

                strMR_Entry = strMR_Entry + "<tdsacccode>" + CCP.TDSType + "</tdsacccode>";
                strMR_Entry = strMR_Entry + "<tdsaccdesc>" + CCP.TDSType + "</tdsaccdesc>";
                strMR_Entry = strMR_Entry + "<TanNo>" + CCP.TANNo + "</TanNo>";
                strMR_Entry = strMR_Entry + "<PANNO>" + CCP.PANNO + "</PANNO>";
                strMR_Entry = strMR_Entry + "<tds_rate>" + CCP.TDSPercentage + "</tds_rate>";
                strMR_Entry = strMR_Entry + "<DED_TDS>" + CCP.totTDSAmt + "</DED_TDS>";

                strMR_Entry = strMR_Entry + "</mr_entry></root>";
                #endregion

                #region MR_ChargeEntry
                string strMR_ChargeEntry = "";
                strMR_ChargeEntry = "<root><mr_charges_entry>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<MRNO>" + "" + "</MRNO>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<MRSF>" + "." + "</MRSF>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<DOCKNO>" + DVM.Dtl.DOCKNO + "</DOCKNO>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<FREIGHT_DIFF>" + Convert.ToString(DeliveryChargesList[0].ChargeAmount) + "</FREIGHT_DIFF>";

                if (DVM.Dtl.paybas.CompareTo("P03") == 0)
                    strMR_ChargeEntry = strMR_ChargeEntry + "<FREIGHT>" + Convert.ToString(DVM.Dtl.FreightAmount) + "</FREIGHT>";
                else
                    strMR_ChargeEntry = strMR_ChargeEntry + "<FREIGHT>0</FREIGHT>";

                foreach (DeliveryCharges obj in DeliveryChargesList)
                {
                    strMR_ChargeEntry = strMR_ChargeEntry + "<" + obj.ChargeCode + ">" + obj.ChargeAmount + "</" + obj.ChargeCode + ">";
                    //strMR_ChargeEntry = strMR_ChargeEntry + "<" + obj.ChargeCode + ">0</" + obj.ChargeCode + ">";
                }

                strMR_ChargeEntry = strMR_ChargeEntry + "<SUBTOTAL>" + DVM.Dtl.SubTotal + "</SUBTOTAL>";

                //strMR_ChargeEntry = strMR_ChargeEntry + "<SVCTAX>" + "" + "</SVCTAX>";
                //strMR_ChargeEntry = strMR_ChargeEntry + "<CESS>" + "" + "</CESS>";
                //strMR_ChargeEntry = strMR_ChargeEntry + "<HEDU_CESS>" + "" + "</HEDU_CESS>";
                //strMR_ChargeEntry = strMR_ChargeEntry + "<SBCTAX>" + "" + "</SBCTAX>";
                //strMR_ChargeEntry = strMR_ChargeEntry + "<KKCTAX>" + "" + "</KKCTAX>";

                strMR_ChargeEntry = strMR_ChargeEntry + "<IGSTAmount>" + objGSTCharges.IGSTCOLLECTED + "</IGSTAmount>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<CGSTAmount>" + objGSTCharges.CGSTCOLLECTED + "</CGSTAmount>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<SGSTAmount>" + objGSTCharges.SGSTCOLLECTED + "</SGSTAmount>";
                strMR_ChargeEntry = strMR_ChargeEntry + "<UTGSTAmount>" + objGSTCharges.UTGSTCOLLECTED + "</UTGSTAmount>";

                strMR_ChargeEntry = strMR_ChargeEntry + "<MRSAMT>" + DVM.Dtl.MRAmount + "</MRSAMT>";

                strMR_ChargeEntry = strMR_ChargeEntry + "</mr_charges_entry></root>";
                #endregion

                #region MR_DifferenceEntry
                string strMR_DifferenceEntry = "";
                strMR_DifferenceEntry = "<root><mr_diff_entry>";
                strMR_DifferenceEntry = strMR_DifferenceEntry + "<MRNO>" + "" + "</MRNO>";
                strMR_DifferenceEntry = strMR_DifferenceEntry + "<MRSF>" + "." + "</MRSF>";
                strMR_DifferenceEntry = strMR_DifferenceEntry + "<DOCKNO>" + DVM.Dtl.DOCKNO + "</DOCKNO>";

                if (DVM.Dtl.paybas.CompareTo("P03") == 0)
                    strMR_DifferenceEntry = strMR_DifferenceEntry + "<FREIGHT>" + Convert.ToString(BookingChargesList[2].ContractCharge) + "</FREIGHT>";
                else
                    strMR_DifferenceEntry = strMR_DifferenceEntry + "<FREIGHT>0</FREIGHT>";

                foreach (BookingCharges obj in BookingChargesList)
                {
                    strMR_DifferenceEntry = strMR_DifferenceEntry + "<" + obj.ChargeCode + ">" + obj.ContractCharge + "</" + obj.ChargeCode + ">";
                }

                strMR_DifferenceEntry = strMR_DifferenceEntry + "</mr_diff_entry></root>";
                #endregion

                #region MR_Gatepass
                string strMR_GatepassEntry = "<root><gatepass_entry>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<gpbrcd>" + BaseLocationCode + "</gpbrcd>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<dockno>" + DVM.Dtl.DOCKNO + "</dockno>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<gatepassdate>" + DVM.Dtl.MRDate.ToString("dd MMM yyyy hh:mm:ss") + "</gatepassdate>";
                if (DVM.Dtl.paybas.CompareTo("P02") == 0)
                {
                    if (DVM.Dtl.BillingPartyAS.CompareTo("P") == 0)
                    {
                        if (DVM.Dtl.csgecd.CompareTo("") == 0)
                        {
                            Response.Redirect("ErrorPage.aspx?heading=Party Code Missing.&detail1=&suggesion1=", false);
                        }
                        strMR_GatepassEntry = strMR_GatepassEntry + "<ptcd>" + DVM.Dtl.csgecd + "</ptcd>" + "<ptname>" + DVM.Dtl.csgenm + "</ptname>";
                    }
                    else
                    {
                        strMR_GatepassEntry = strMR_GatepassEntry + "<ptcd>" + DVM.Dtl.csgecd + "</ptcd>" + "<ptname>" + DVM.Dtl.csgenm + "</ptname>";
                    }
                }
                else
                {
                    strMR_GatepassEntry = strMR_GatepassEntry + "<ptcd>" + DVM.Dtl.csgecd + "</ptcd>" + "<ptname>" + DVM.Dtl.csgenm + "</ptname>";
                }

                strMR_GatepassEntry = strMR_GatepassEntry + "<receivercode>" + DVM.Dtl.receivercode + "</receivercode>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<receivername>" + DVM.Dtl.receivername + "</receivername>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<delvehicle>" + DVM.Dtl.DeliveryVehicle + "</delvehicle>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<remarks>" + DVM.Dtl.Remarks + "</remarks>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<activeflag>Y</activeflag>";
                strMR_GatepassEntry = strMR_GatepassEntry + "<entryby>" + BaseUserName + "</entryby>";
                strMR_GatepassEntry = strMR_GatepassEntry + "</gatepass_entry></root>";
                #endregion

                #region MR_BillObject
                double billamt = Convert.ToDouble(DVM.Dtl.MRAmount) - Convert.ToDouble(DVM.Dtl.CollectionAmount);
                string strMR_BillObject = "<root><bill_entry><billno></billno>";
                strMR_BillObject = strMR_BillObject + "<bgndt>" + GF.FormateDate(DVM.Dtl.MRDate) + "</bgndt>";
                strMR_BillObject = strMR_BillObject + "<bbrcd>" + BaseLocationCode + "</bbrcd>";
                strMR_BillObject = strMR_BillObject + "<paybas>11</paybas>";
                strMR_BillObject = strMR_BillObject + "<ptmscd>" + DVM.Dtl.party_code + "</ptmscd>";
                //strMR_BillObject = strMR_BillObject + "<ptmsnm>" + txtpartyname.Text + "</ptmsnm>";
                strMR_BillObject = strMR_BillObject + "<ptmsaddr></ptmsaddr>";
                strMR_BillObject = strMR_BillObject + "<ptmstel></ptmstel>";
                strMR_BillObject = strMR_BillObject + "<ptmsemail></ptmsemail>";
                strMR_BillObject = strMR_BillObject + "<remark >" + DVM.Dtl.Remarks + "</remark >";
                strMR_BillObject = strMR_BillObject + "<billamt>" + DVM.Dtl.MRAmount.ToString("F2") + "</billamt>";
                strMR_BillObject = strMR_BillObject + "<pendamt>" + DVM.Dtl.BillpendingAmount.ToString("F2") + "</pendamt>";
                strMR_BillObject = strMR_BillObject + "<billstatus>BILL GENERATED</billstatus>";
                strMR_BillObject = strMR_BillObject + "<closebill>N</closebill>";
                strMR_BillObject = strMR_BillObject + "<bgenempcd>" + BaseUserName + "</bgenempcd>";
                strMR_BillObject = strMR_BillObject + "<bill_cancel>N</bill_cancel>";
                strMR_BillObject = strMR_BillObject + "<billsubbrcd>" + BaseLocationCode + "</billsubbrcd>";
                strMR_BillObject = strMR_BillObject + "<billcolbrcd>" + BaseLocationCode + "</billcolbrcd>";
                strMR_BillObject = strMR_BillObject + "<GSTType>" + DVM.Dtl.GSTType + "</GSTType>";
                strMR_BillObject = strMR_BillObject + "<IGSTCOLLECTED>" + objGSTCharges.IGSTCOLLECTED + "</IGSTCOLLECTED>";
                strMR_BillObject = strMR_BillObject + "<CGSTCOLLECTED>" + objGSTCharges.CGSTCOLLECTED + "</CGSTCOLLECTED>";
                strMR_BillObject = strMR_BillObject + "<SGSTCOLLECTED>" + objGSTCharges.SGSTCOLLECTED + "</SGSTCOLLECTED>";
                strMR_BillObject = strMR_BillObject + "<UTGSTCOLLECTED>" + objGSTCharges.UTGSTCOLLECTED + "</UTGSTCOLLECTED>";
                if (Convert.ToDouble(objGSTCharges.IGSTCOLLECTED) > 0
                    || Convert.ToDouble(objGSTCharges.CGSTCOLLECTED) > 0
                    || Convert.ToDouble(objGSTCharges.SGSTCOLLECTED) > 0
                    || Convert.ToDouble(objGSTCharges.UTGSTCOLLECTED) > 0)
                {
                    strMR_BillObject = strMR_BillObject + "<IGSTRate>" + objGSTCharges.IGSTRate + "</IGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<CGSTRate>" + objGSTCharges.CGSTRate + "</CGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<SGSTRate>" + objGSTCharges.SGSTRate + "</SGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<UTGSTRate>" + objGSTCharges.UTGSTRate + "</UTGSTRate>";
                }
                else
                {
                    strMR_BillObject = strMR_BillObject + "<IGSTRate>0</IGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<CGSTRate>0</CGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<SGSTRate>0</SGSTRate>";
                    strMR_BillObject = strMR_BillObject + "<UTGSTRate>0</UTGSTRate>";
                }
                strMR_BillObject = strMR_BillObject + "<businesstype>" + DVM.Dtl.businesstype + "</businesstype>";
                strMR_BillObject = strMR_BillObject + "<dockno>" + DVM.Dtl.DOCKNO + "</dockno>";
                strMR_BillObject = strMR_BillObject + "<docksf>" + DVM.Dtl.DOCKSF + "</docksf>";
                strMR_BillObject = strMR_BillObject + "<orgncd>" + DVM.Dtl.orgncd + "</orgncd>";
                strMR_BillObject = strMR_BillObject + "<reassign_destcd>" + DVM.Dtl.delloc + "</reassign_destcd>";
                strMR_BillObject = strMR_BillObject + "<dockdt>" + DVM.Dtl.dockdate.ToString("dd MMM yyyy hh:mm:ss") + "</dockdt>";
                strMR_BillObject = strMR_BillObject + "<company_code>" + BaseCompanyCode + "</company_code>";
                strMR_BillObject = strMR_BillObject + "<chrgwt>" + DVM.Dtl.chrgwt + "</chrgwt></bill_entry></root>";
                #endregion

                //strMR_Entry = strMR_Entry.Replace("&", "&amp;");
                //strMR_ChargeEntry = strMR_ChargeEntry.Replace("&", "&amp;");
                //strMR_DifferenceEntry = strMR_DifferenceEntry.Replace("&", "&amp;");
                //strMR_GatepassEntry = strMR_GatepassEntry.Replace("&", "&amp;");
                //strMR_BillObject = strMR_BillObject.Replace("&", "&amp;");

                string flagaccounting = "";
                if (("ABCDEFGHIMXYZ".Contains(DVM.Dtl.DOCKSF) && (Convert.ToInt32(DVM.Dtl.arrpkgqty) == Convert.ToInt32(DVM.Dtl.pkgsno)) || (DVM.Dtl.DOCKSF.CompareTo(".") == 0 && Convert.ToInt32(DVM.Dtl.arrpkgqty) > 0)))
                {
                    flagaccounting = "Y";
                }

                DataTable DT = OS.DeliveryMREntry(strMR_Entry, strMR_ChargeEntry, strMR_DifferenceEntry, strMR_GatepassEntry, strMR_BillObject, flagaccounting, BaseFinYear);

                string MRNO = "", BILLNO = "", VOUCHERNO = "", TranXaction = "";
                bool IsError = false;

                MRNO = DT.Rows[0]["MRNO"].ToString();
                BILLNO = DT.Rows[0]["BILLNO"].ToString();
                VOUCHERNO = DT.Rows[0]["VOUCHERNO"].ToString();
                IsError = Convert.ToBoolean(DT.Rows[0]["IsError"]);
                TranXaction = DT.Rows[0]["TranXaction"].ToString().Trim();

                if (IsError)
                {
                    ViewBag.StrError = "Error " + TranXaction;
                    return View("Error");
                }

                return RedirectToAction("DeliveryMR_Done", new { MRNO = MRNO, BILLNO = BILLNO, VOUCHERNO = VOUCHERNO, TranXaction = TranXaction });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message;
                return View("Error");
            }
        }

        public ActionResult DeliveryMR_Done(string MRNO, string BILLNO, string VOUCHERNO, string TranXaction)
        {
            ViewBag.MRNO = MRNO;
            ViewBag.BILLNO = BILLNO;
            ViewBag.VOUCHERNO = VOUCHERNO;
            ViewBag.TranXaction = TranXaction;

            return View();

        }
        #endregion


        #region CrossingChallan

        public ActionResult CrossingChallanCriteria()
        {
            CrossingChallanFilterViewModel CRCHVM = new CrossingChallanFilterViewModel();
            CRCHVM.Orgncd = BaseLocationCode;
            return View(CRCHVM);
        }
        public ActionResult CrossingChallanDocketList(CrossingChallanFilterViewModel CCF)
        {
            CrossingChallanViewModel CHV = new CrossingChallanViewModel();
            Webx_Crossing_Docket_Master WCDM_obj = new Webx_Crossing_Docket_Master();
            DataTable Dt = new DataTable();
            Dt = OS.GetVendorName(CCF.VendorCode);
            WCDM_obj.ChallanLocCode = BaseLocationCode;
            WCDM_obj.VendorCode = CCF.VendorCode;
            WCDM_obj.VendorName = Dt.Rows[0]["vendorname"].ToString();
            WCDM_obj.ChallanDate = System.DateTime.Today;
            CHV.WCDM = WCDM_obj;
            List<Webx_Crossing_Docket_Detail> DtlList = new List<Webx_Crossing_Docket_Detail>();


            DtlList = OS.GetCrossingChallan_DocketDetails(CCF.DockNo, CCF.FromDate.ToString("dd MMM yyyy"), CCF.ToDate.ToString("dd MMM yyyy"), CCF.Orgncd, CCF.VendorCode);
            CHV.WCDD = DtlList;


            return View(CHV);
        }

        public ActionResult CrossingChallanSubmit(CrossingChallanViewModel CCVM, List<Webx_Crossing_Docket_Detail> CrossingDocketDetail)
        {
            try
            {
                List<Webx_Crossing_Docket_Detail> docketlist = new List<Webx_Crossing_Docket_Detail>();
                docketlist = CrossingDocketDetail.Where(c => c.IsChecked).ToList();

                XmlDocument xmlDocChallan = new XmlDocument();
                XmlSerializer xmlSerializerChallan = new XmlSerializer(CCVM.WCDM.GetType());

                using (MemoryStream xmlStreamChallan = new MemoryStream())
                {
                    xmlSerializerChallan.Serialize(xmlStreamChallan, CCVM.WCDM);
                    xmlStreamChallan.Position = 0;
                    xmlDocChallan.Load(xmlStreamChallan);
                }


                XmlDocument xmlDocChallanDocket = new XmlDocument();
                XmlSerializer xmlSerializerChallanDocket = new XmlSerializer(docketlist.GetType());

                using (MemoryStream xmlStreamChallanDocket = new MemoryStream())
                {
                    xmlSerializerChallanDocket.Serialize(xmlStreamChallanDocket, docketlist);
                    xmlStreamChallanDocket.Position = 0;
                    xmlDocChallanDocket.Load(xmlStreamChallanDocket);
                }


                string CrossignChallan = xmlDocChallan.InnerXml.ToString();
                string CrossignChallanDocket = xmlDocChallanDocket.InnerXml.ToString();


                DataTable DT = OS.CrossingChallanEntry(CrossignChallan.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), CrossignChallanDocket.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), BaseUserName, BaseCompanyCode);

                string Status = "", ErroMsg = "", CrossingChallanNo = "";
                Status = DT.Rows[0]["Status"].ToString();
                ErroMsg = DT.Rows[0]["ErroMsg"].ToString();
                CrossingChallanNo = DT.Rows[0]["CrossingChallanNo"].ToString();
                if (Status == "1")
                {
                    return RedirectToAction("CrossingChallanDone", new { CrossingChallanNo = CrossingChallanNo });
                }
                else
                {
                    throw new Exception(ErroMsg);
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message;
                return View("Error");
            }

        }
        public ActionResult CrossingChallanDone(string CrossingChallanNo)
        {
            CrossingChallanViewModel CCVM = new CrossingChallanViewModel();
            CCVM.THCNo = CrossingChallanNo;
            return View(CCVM);
        }
        public JsonResult GetrateforCrossingvendorContract(string vendorcode, string Origin, string ToCity, string ratetype, string Pkgs, string ActuWeight)
        {
            DataTable DT = new DataTable();
            string DDRateBase = "";
            double Rate = 0.00, DDRate = 0.00, CrossingCharge = 0.00, Weight = 0.00, DoorDeliveryCharge = 0.00, arrivedPkgs = 0.00;
            int i = 0;
            DT = OS.GetDetailCrossingvendorContract(vendorcode, Origin, ToCity, ratetype);
            if (DT.Rows.Count > 0)
            {
                ratetype = DT.Rows[0]["ratetype"].ToString();
                DDRateBase = DT.Rows[0]["DDRateType"].ToString();
                Rate = Convert.ToDouble(DT.Rows[0]["Rate"]);
                DDRate = Convert.ToDouble(DT.Rows[0]["doordeliveryCharge"]);
                Weight = Convert.ToDouble(ActuWeight);
                arrivedPkgs = Convert.ToDouble(Pkgs);
                if (ratetype == "K")
                {
                    CrossingCharge = (Weight * Rate);
                    if (DDRateBase == "K")
                        DoorDeliveryCharge = (Weight * DDRate);
                    else if (DDRateBase == "P")
                        DoorDeliveryCharge = (arrivedPkgs * DDRate);
                    else
                        DoorDeliveryCharge = DDRate;
                }
                else if (ratetype == "P")
                {
                    CrossingCharge = (arrivedPkgs * Rate);
                    if (DDRateBase == "K")
                        DoorDeliveryCharge = (Weight * DDRate);
                    else if (DDRateBase == "P")
                        DoorDeliveryCharge = (arrivedPkgs * DDRate);
                    else
                        DoorDeliveryCharge = DDRate;
                }
                else
                {
                    CrossingCharge = Rate;
                    if (DDRateBase == "K")
                        DoorDeliveryCharge = (Weight * DDRate);
                    else if (DDRateBase == "P")
                        DoorDeliveryCharge = (arrivedPkgs * DDRate);
                    else
                        DoorDeliveryCharge = DDRate;
                }
                i = 1;
            }
            return new JsonResult()
            {
                Data = new
                {
                    Status = i,
                    CrossingCharge = CrossingCharge,
                    Rate = Rate,
                    DDRateBase = DDRateBase,
                    DoorDeliveryCharge = DoorDeliveryCharge,
                    DDRate = DDRate
                }
            };
        }

        public ActionResult CrossingChallanPouchUploadCriteria()
        {
            CrossingChallanFilterViewModel CRCHVM = new CrossingChallanFilterViewModel();
            CRCHVM.Orgncd = BaseLocationCode;
            return View(CRCHVM);
        }

        public ActionResult CrossingChallanListPouchUpload(CrossingChallanFilterViewModel CCF)
        {
            CrossingChallanPouchViewModel CCPV = new CrossingChallanPouchViewModel();
            CCPV.WCDM = OS.GetCrossingChallanListForPouchUpload(CCF.FromDate.ToString("dd MMM yyyy"), CCF.ToDate.ToString("dd MMM yyyy"), CCF.THCNo, CCF.Orgncd, CCF.VendorCode);
            CCPV.CCF = CCF;
            return View(CCPV);
        }

        public ActionResult CrossingChallanPouchUpload(List<Webx_Crossing_Docket_Master> CrossingChallanPouch, IEnumerable<HttpPostedFileBase> files)
        {
            string CrossingChallanNos = "";
            var Tranxaction = "";
            var DocumentUploadedPath = "";
            try
            {
                int id = 0;

                var now = DateTime.Now;
                var yearName = now.ToString("yyyy");
                var monthName = now.ToString("MMMM");
                string docktypeName = "CrossingChallan";
                string xml_Data = "<root>";

                foreach (var item in CrossingChallanPouch)
                {
                    //var path = "";
                    var fileName = "";
                    string newFName = "";

                    if (files != null && item.IsCheck)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[id] != null)
                        {
                            var file = ((System.Web.HttpPostedFileBase[])(files))[id];
                            fileName = file.FileName;
                            double dblFileSize = Convert.ToDouble(file.ContentLength) / 1000;
                            double MaxSize = 1024; //1MB

                            if (file.ContentLength > 0 && dblFileSize < MaxSize)
                            {
                                //string PATHDONO = item.DocketNo;
                                string extension = System.IO.Path.GetExtension(file.FileName);
                                newFName = GetFileName(file.FileName, item.CrossingChallanNo.Trim(), "CRP");

                                string DB_SavedPath = docktypeName + "/" + System.DateTime.Now.ToString("yyyy/MMM");

                                string FolDerPath = "D:\\SRLApplicationCode\\POD\\FMScanDocument\\" + DB_SavedPath;
                                if (!Directory.Exists(FolDerPath))
                                    Directory.CreateDirectory(FolDerPath);

                                DocumentUploadedPath = DB_SavedPath + "/" + newFName;
                                file.SaveAs(FolDerPath + "\\" + newFName);

                                xml_Data = xml_Data + "<CrossingChallan>";
                                xml_Data = xml_Data + "<CrossingChallanNo>" + item.CrossingChallanNo + "</CrossingChallanNo>";
                                xml_Data = xml_Data + "<DocumentName>" + newFName + "</DocumentName>";
                                xml_Data = xml_Data + "<DocumentPath>" + DocumentUploadedPath + "</DocumentPath>";
                                xml_Data = xml_Data + "</CrossingChallan>";

                                CrossingChallanNos = string.IsNullOrEmpty(CrossingChallanNos) ? item.CrossingChallanNo : CrossingChallanNos + ", " + item.CrossingChallanNo;
                            }
                        }
                    }

                    id++;

                }

                xml_Data = xml_Data + "</root>";
                DataTable Dt = OS.CrossingChallanPouchUpload(xml_Data.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), BaseUserName);

                Tranxaction = Dt.Rows[0]["Status"].ToString();
                if (Tranxaction != "Done")
                {
                    throw new Exception(Dt.Rows[0]["ErrorMessage"].ToString());
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return RedirectToAction("CrossingChallanPouchUploadDone", new { CrossingChallanNos = CrossingChallanNos, Tranxaction = Tranxaction });
        }

        public ActionResult CrossingChallanPouchUploadDone(string CrossingChallanNos, string Tranxaction)
        {
            ViewBag.Tranxaction = Tranxaction;
            ViewBag.CrossingChallanNos = CrossingChallanNos;
            return View();
        }
        private string GetFileName(string fileName, string docno, string pref)
        {
            string strRet = fileName;

            string pat = @"(?:.+)(.+)\.(.+)";
            Regex r = new Regex(pat);
            //run
            Match m = r.Match(fileName);
            string file_ext = m.Groups[2].Captures[0].ToString();
            string filename = m.Groups[1].Captures[0].ToString();
            docno = docno.Replace("/", "$");

            strRet = pref + "_" + docno + "." + file_ext;

            return strRet;
        }
        #endregion


        #region IDTEntry

        public ActionResult IDTEntry()
        {
            DocketViewModel DVM = new DocketViewModel();
            WebX_Master_Docket_Charges WMDC = new WebX_Master_Docket_Charges();
            WebX_Master_Docket WMD = new WebX_Master_Docket();
            List<WebX_Master_Docket_Invoice> ListInVoice = new List<WebX_Master_Docket_Invoice>();
            List<QuickDocketBalancecharges> ListCharges = new List<QuickDocketBalancecharges>();

            if (ListInVoice.Count() == 0)
            {
                WebX_Master_Docket_Invoice CMDI = new WebX_Master_Docket_Invoice();
                CMDI.SrNo = 1;
                CMDI.INVDT = System.DateTime.Now;
                CMDI.DECLVAL = 0;
                CMDI.PKGSNO = 0;
                CMDI.ACTUWT = 0;
                CMDI.VOL_L = 0;
                CMDI.VOL_B = 0;
                CMDI.VOL_H = 0;
                ListInVoice.Add(CMDI);
                CMDI = new WebX_Master_Docket_Invoice();
            }
            WMD.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
            WMD.FlagStaxBifer = MS.GetRuleObject("STAX_BIFER").defaultvalue.IndexOf(BaseCompanyCode) >= 0 ? "Y" : "N";
            WMD.FLAG_PRORATA = MS.GetRuleObject("FLAG_PRORATA").defaultvalue;
            DVM.WMD = WMD;
            DVM.WMDC = WMDC;
            DVM.ListInVoice = ListInVoice;
            ViewBag.DKTCalledAs = "IDT";
            return View(DVM);
        }

        public ActionResult IDTDocketSubmit(DocketViewModel DVM, List<WebX_Master_Docket_Invoice> DocketInvoiceList, List<webx_Master_DOCKET_DOCUMENT> ListDocument)
        {
            string fromdate = Convert.ToDateTime(DVM.WMD.DOCKDT).ToString("dd MMM yyyy");
            string todate = Convert.ToDateTime(DVM.WMD.DOCKDT).ToString("dd MMM yyyy");
            DateTime DocketDate = Convert.ToDateTime(fromdate);
            DateTime EDDDate = Convert.ToDateTime(todate);

            #region EDD Date Validation

            string IsScheduleDelivery = "N";// MS.GetCustomerMasterObject().Where(c => c.CUSTCD == DVM.WMD.PARTY_CODE).FirstOrDefault().ScheduleApply;
            if ((string.IsNullOrEmpty(IsScheduleDelivery) || IsScheduleDelivery != "Y") && DVM.WMD.PAYBAS != "P02")
            {
                if (DVM.WMD.CDELDT != System.DateTime.MinValue && DVM.WMD.CDELDT != System.DateTime.MaxValue)
                {
                    DateTime SYSEDDDate = Convert.ToDateTime(GF.FormateDate(DVM.WMD.DOCKDT.AddDays(Convert.ToInt32(DVM.WMD.TRDays))));
                    if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "PU")
                    {
                        DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, DVM.WMD.REASSIGN_DESTCD, true);
                        if (edd_hday > SYSEDDDate)
                        {
                            SYSEDDDate = edd_hday;
                        }
                    }
                    else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "AD")
                    {
                        DateTime edd_hday = OS.AddHolidaysToEDD(SYSEDDDate, DVM.WMD.REASSIGN_DESTCD, true);
                        if (edd_hday > SYSEDDDate)
                        {
                            // hdnhdayedd.Value = edd_hday.ToString("dd/MM/yyyy");
                            SYSEDDDate = edd_hday;
                        }
                    }
                    else if (MS.GetRuleObject("EDD_ADD_HDAYS").defaultvalue == "N")
                    {
                        if (SYSEDDDate.DayOfWeek.ToString().ToUpper().CompareTo("SUNDAY") == 0)
                            SYSEDDDate.AddDays(1);
                    }

                    if (Convert.ToDateTime(GF.FormateDate(DVM.WMD.CDELDT)) != SYSEDDDate)
                    {
                        //string ErrorMsg = "Actual EDD Should be " + GF.FormateDate(DVM.WMD.DOCKDT.AddDays(Convert.ToInt32(DVM.WMD.TRDays))) + " TRDays = " + DVM.WMD.TRDays + " but current EDD is " + GF.FormateDate(DVM.WMD.CDELDT);
                        //ViewBag.StrError = ErrorMsg;
                        //return View("Error");
                    }
                }
            }

            #endregion

            #region DKT Entry Date Validation

            if (Convert.ToDateTime(DocketDate.Date.ToString("dd MMM yyyy")) > Convert.ToDateTime(System.DateTime.Now.ToString("dd MMM yyyy")))
            {
                ViewBag.StrError = ViewBag.DKTCalledAs + " Date cannot be Large than Today Date";
                return View("Error");
                //return RedirectToAction("ErrorTransaction", "Home", new { Message = ErrorMsg });
            }

            if (DocketDate.Date > EDDDate.Date)
            {
                ViewBag.StrError = "EDD Date Should be Large than " + ViewBag.DKTCalledAs + " Date";
                return View("Error");
            }

            int days = Convert.ToInt32((System.DateTime.Now - DocketDate).TotalDays);
            if (days > 6)
            {
                ViewBag.StrError = "Please Enter Valid " + ViewBag.DKTCalledAs + " Date. " + ViewBag.DKTCalledAs + " date " + DocketDate.ToString("dd MMM yyy") + " not valid.";
                return View("Error");
            }



            #endregion



            #region Check DKT Charges Calculation

            decimal DKTsubTotal = 0, Charges_SubTotal = 0, serviceTax = 0, SBC = 0, KKC = 0;

            DKTsubTotal = Convert.ToDecimal(DVM.WMDC.FREIGHT) + Convert.ToDecimal(DVM.WMDC.SCHG01) + Convert.ToDecimal(DVM.WMDC.SCHG02);

            if (DVM.WMDC.FOV == null)
            {
                DVM.WMDC.FOV = 0;
            }
            else if (DVM.WMDC.FOV > 0)
            {
                DVM.WMDC.FOV = Convert.ToDecimal(string.Format("{0:0.00}", Convert.ToDecimal(DVM.WMDC.FOV)));
            }
            decimal DKTTotal = DKTsubTotal;

            DVM.WMDC.RATE_TYPE = "F";
            DVM.WMDC.FRT_RATE = DVM.WMDC.FREIGHT;
            DVM.WMDC.FREIGHT_CALC = DVM.WMDC.FREIGHT;
            DVM.WMDC.FOV = 0;
            DVM.WMDC.SCHG03 = 0;
            DVM.WMDC.SCHG04 = 0;
            DVM.WMDC.SCHG05 = 0;
            DVM.WMDC.SCHG06 = 0;
            DVM.WMDC.SCHG07 = 0;
            DVM.WMDC.SCHG08 = 0;
            DVM.WMDC.SCHG09 = 0;
            DVM.WMDC.SCHG10 = 0;
            DVM.WMDC.SCHG11 = 0;
            DVM.WMDC.SCHG12 = 0;
            DVM.WMDC.SCHG13 = 0;
            DVM.WMDC.SCHG14 = 0;
            DVM.WMDC.SCHG15 = 0;
            DVM.WMDC.SCHG16 = 0;
            DVM.WMDC.SCHG17 = 0;
            DVM.WMDC.SCHG18 = 0;
            DVM.WMDC.SCHG19 = 0;
            DVM.WMDC.SCHG20 = 0;
            DVM.WMDC.SCHG21 = 0;
            DVM.WMDC.SCHG22 = 0;
            DVM.WMDC.SCHG23 = 0;
            DVM.WMDC.UCHG01 = 0;
            DVM.WMDC.UCHG02 = 0;
            DVM.WMDC.UCHG03 = 0;
            DVM.WMDC.UCHG04 = 0;
            DVM.WMDC.UCHG05 = 0;
            DVM.WMDC.UCHG06 = 0;
            DVM.WMDC.UCHG07 = 0;
            DVM.WMDC.UCHG08 = 0;
            DVM.WMDC.UCHG09 = 0;
            DVM.WMDC.UCHG10 = 0;
            DVM.WMDC.SVCTAX = 0;
            DVM.WMDC.CESS = 0;
            DVM.WMDC.hedu_cess = 0;
            DVM.WMDC.SVCTAX_Rate = 0;
            DVM.WMDC.SbcRate = 0;
            DVM.WMDC.SBCess = 0;
            DVM.WMDC.KKCRate = 0;
            DVM.WMDC.KKCAmount = 0;
            DVM.WMDC.BalanceAmount = DVM.WMDC.DKTTOT;




            #endregion

            if (DVM.WMD.DOCKNO == null)
                DVM.WMD.DOCKNO = "";

            DVM.WMD.hday_booked_yn = "Y";
            DVM.WMD.cutoff_applied_yn = "Y";



            DVM.WMD.doctype = "IDT";
            DVM.WMD.GCType = "IDT";
            DVM.WMD.TRN_MOD = "2";
            DVM.WMD.Service_Class = "1";
            DVM.WMD.ftltype = "0";
            DVM.WMD.IsCompletion = DVM.IsCompletion;
            DVM.WMD.Pickup_Dely = "";
            DVM.WMD.IsCompletion = true;
            DVM.WMD.stax_exmpt_yn = "Y";
            DVM.WMD.EDD_Date = DVM.WMD.DOCKDT;
            DVM.WMD.CDELDT = DVM.WMD.DOCKDT;
            DVM.WMD.permitdt = DVM.WMD.DOCKDT;
            DVM.WMD.permit_recvd_dt = DVM.WMD.DOCKDT;
            DVM.WMD.permit_validity_dt = DVM.WMD.DOCKDT;
            DVM.WMD.insupldt = DVM.WMD.DOCKDT;
            Docket docket = GetDocketObject(DVM.WMD, DocketInvoiceList);
            DVM.WMDC.DOCKNO = docket.DockNo;


            List<webx_Master_DOCKET_DOCUMENT> docketdoc = new List<webx_Master_DOCKET_DOCUMENT>();// GetDocumentObject(ListDocument);
            List<DocketInvoice> DocketInvoice = GetInvoiceObject(DocketInvoiceList);
            List<BarCodeSerial> bcs = new List<BarCodeSerial>(); // GetBarCodeSerialObject(ListBarCode);
            DataTable dt = new DataTable();


            string strXMLMasterEntry = "", strXMLChargesEntry = "", strXMLDocumentEntry = "", strXMLInvoiceEntry = "", strXMLBCSerialEntry = "";


            XmlDocument xmlDocCha = new XmlDocument();
            XmlSerializer xmlSerializerCha = new XmlSerializer(DVM.WMDC.GetType());

            using (MemoryStream xmlStreamCha = new MemoryStream())
            {
                xmlSerializerCha.Serialize(xmlStreamCha, DVM.WMDC);
                xmlStreamCha.Position = 0;
                xmlDocCha.Load(xmlStreamCha);
            }


            strXMLMasterEntry = GetDocketXMLString(docket);
            strXMLChargesEntry = xmlDocCha.InnerXml;
            strXMLDocumentEntry = GetDocketDocumentXMLString(docketdoc);
            strXMLInvoiceEntry = GetDocketInvoiceXMLString(DocketInvoice);
            strXMLBCSerialEntry = GetDocketBCSerialXMLString(bcs);


            if (Convert.ToInt32(Convert.ToDouble(DKTsubTotal)) != Convert.ToInt32(Convert.ToDouble(DVM.WMDC.SubTotal)))
            {
                string SQRY = "exec USP_IDTDOCKET_ENTRY '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "IDTDOCKETENTRY DKTsubTotal error", "", "Error");

                ViewBag.StrError = "There is a problem in Docket Subtotal Calculation. Please retry and check charges properly.";
                return View("Error");
            }


            double DKtTotDiff = Convert.ToDouble(DKTTotal) - Convert.ToDouble(DVM.WMDC.DKTTOT);

            if (DKtTotDiff >= 1 || DKtTotDiff <= -1)
            {
                string SQRY = "exec USP_IDTDOCKET_ENTRY '" + strXMLMasterEntry + "','" + strXMLChargesEntry + "','" + strXMLDocumentEntry + "','" + strXMLInvoiceEntry + "','" + strXMLBCSerialEntry + "','" + BaseFinYear + "','<root></root>'";
                int Id = GF.SaveRequestServices(SQRY.Replace("'", "''"), "IDTDOCKETENTRY DKtTotDiff error", "", "Error");
                ViewBag.StrError = "There is a problem in Docket Total Calculation. Please retry and check charges properly.";
                return View("Error");
            }


            XmlDocument PrimaryOutPut = new XmlDocument();
            SqlConnection con = new SqlConnection(Connstr);
            con.Open();
            SqlTransaction trn = con.BeginTransaction();
            try
            {
                PrimaryOutPut = OS.IDTDocketEntry(strXMLMasterEntry, strXMLChargesEntry, strXMLDocumentEntry, strXMLInvoiceEntry, strXMLBCSerialEntry, BaseFinYear, trn);


                string Dockno = "";
                try
                {
                    Dockno = ((XmlNode)((XmlNodeList)PrimaryOutPut.GetElementsByTagName("DockNo"))[0]).InnerText.ToString();
                }
                catch (Exception ex)
                {
                    trn.Rollback();
                    con.Close();
                    ViewBag.StrError = ex.Message;
                    return View("Error");
                }
                trn.Commit();
                if (docket.MailId != null && docket.MailId != "")
                {
                    try
                    {
                        List<WebX_Master_Docket> Dock_List = ES.Get_DOCKET_GETList_Mail(Dockno);
                        try
                        {
                            WebX_Master_Docket OBjDKT = Dock_List.First();
                            OBjDKT.MailId = docket.MailId;
                            SendMail(OBjDKT);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                    catch (Exception ex)
                    {
                        trn.Rollback();
                        con.Close();
                        ViewBag.StrError = ex.Message;
                        return View("Error");
                    }
                }
                return RedirectToAction("IDTDocketDone", new { DOCKNO = ((XmlNode)((XmlNodeList)PrimaryOutPut.GetElementsByTagName("DockNo"))[0]).InnerText.ToString(), BILLNO = "", id = 1 });
            }
            catch (Exception ex)
            {
                string exmess = ex.Message.ToString().Replace('\n', '_');
                trn.Rollback();
                con.Close();
                // Response.Redirect("../ErrorPage.aspx?heading=DataBase Entry Failed.&detail1=May Be or Out of Range Wrong Data  Occured&detail2=" + exmess + "&suggestion2=Re Enter " + ViewBag.DKTCalledAs + " with proper Details");
                ViewBag.StrError = "Error " + exmess;
                return View("Error");
            }
        }

        public ActionResult IDTDocketDone(string DOCKNO, string BILLNO, int id)
        {

            ViewBag.DOCKNO = DOCKNO;
            ViewBag.BILLNO = "";
            ViewBag.id = id;
            return View();

        }
        #endregion IDTEntry

        #region AutounloadingViewPrint

        public ActionResult AutoUnloadingViewPrint()
        {
            THCAutounloadingViewPrintViewModel TAFVM = new THCAutounloadingViewPrintViewModel();

            return View(TAFVM);
        }

        public string AutoUnloadingListGet(THCAutounloadingViewPrintViewModel TAFVM)
        {
            ViewBag.Type = "1";
            return JsonConvert.SerializeObject(OS.AutoUnloadingList(TAFVM.THCNo, GF.FormateDate(TAFVM.FromDate), GF.FormateDate(TAFVM.ToDate), TAFVM.UnLoadingNo, BaseLocationCode));
        }
        #endregion

        #region Cheque Register Details

        public ActionResult ChequeRegisterDetails()
        {
            var Cheq = Request.QueryString["id"].Split(',');

            CHQ_Register_Details DCDV = new CHQ_Register_Details();
            DataTable DT = OS.USPGETCHEQUEDETAILS(Cheq[0], Cheq[1]);
            if (DT.Rows.Count > 0)
            {
                DCDV.ChequeNo = DT.Rows[0]["chqno"].ToString();
                DCDV.Chequedate = DT.Rows[0]["chqdt"].ToString();
                DCDV.Issuelocation = DT.Rows[0]["brcd"].ToString();
                DCDV.Issuedate = DT.Rows[0]["entrydt"].ToString();
                DCDV.Depositlocation = DT.Rows[0]["depoloccode"].ToString();
                DCDV.Depositdate = DT.Rows[0]["transdate"].ToString();
                DCDV.Depositvoucherno = "";
                DCDV.Depositedfrom = DT.Rows[0]["banknm"].ToString();
                DCDV.ChequeAmount = Convert.ToDecimal(DT.Rows[0]["chqamt"].ToString());
                DCDV.RecivedFrom = DT.Rows[0]["ptmsptnm"].ToString();
                DCDV.AccountedAmount = Convert.ToDecimal(DT.Rows[0]["adjustamt"].ToString());
                DCDV.UnAccountedAmount = (Convert.ToDecimal(DT.Rows[0]["chqamt"]) - Convert.ToDecimal(DT.Rows[0]["adjustamt"]));
                DCDV.ListDCD = OS.GETCHEQLIST(Cheq[0], Cheq[1]).ToList();
            }
            else
            {
                DCDV.ChequeNo = "-";
                DCDV.Chequedate = "-";
                DCDV.Issuelocation = "-";
                DCDV.Issuedate = "-";
                DCDV.Depositlocation = "-";
                DCDV.Depositdate = "-";
                DCDV.Depositvoucherno = "-";
                DCDV.Depositedfrom = "-";
                DCDV.ChequeAmount = 0;
                DCDV.RecivedFrom = "-";
                DCDV.AccountedAmount = 0;
                DCDV.UnAccountedAmount = 0;
                DCDV.ListDCD = new List<CHQ_Register_Details_DET>();
            }

            return View(DCDV);
        }

        #endregion

        #region Accident/break Down
        public ActionResult Accident_BreakDown()
        {
            THCCriteariaSelectionViewModel THCCSVM = new THCCriteariaSelectionViewModel();
            try
            {
                THCCSVM.ListTHCSelection = new List<THCCriteariaSelection>();
                THCCSVM.THCSelection = new THCCriteariaSelection();
                THCCSVM.EditFlag = "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(THCCSVM);
        }
        public ActionResult GetAccident_BreakDownList(string THCNO, string TCNO, DateTime FromDate, DateTime ToDate)
        {
            THCCriteariaSelectionViewModel THCCSMV = new THCCriteariaSelectionViewModel();
            List<THCCriteariaSelection> lst = new List<THCCriteariaSelection>();
            THCCSMV.ListTHCSelection = new List<THCCriteariaSelection>();
            try
            {
                lst = OS.GetJobOrderViewList(THCNO, TCNO, FromDate, ToDate).ToList();
                THCCSMV.ListTHCSelection = lst;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return PartialView("_THCSelection", THCCSMV.ListTHCSelection);
        }
        public ActionResult THCExceptionList(THCCriteariaSelectionViewModel THCCSMV, List<THCCriteariaSelection> RutTran)
        {
            List<THCCriteariaSelection> MFSList = new List<THCCriteariaSelection>();
            try
            {
                MFSList = OS.Get_MFs_List(THCCSMV.THCSelection.selectTHCNO);
                THCCSMV.ListTHCSelection = MFSList;

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "THCExceptionList", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(THCCSMV);
        }
        public ActionResult GetExceptionExeptionList(THCCriteariaSelectionViewModel THCCSMV)
        {
            try
            {
                DataTable DT = OS.ExecuteExceptionSubmit(THCCSMV.THCSelection.selectTHCNO, BaseLocationCode);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("ExecuteExceptionDone", new { selectTHCNO = THCCSMV.THCSelection.selectTHCNO });
        }
        public ActionResult ExecuteExceptionDone(string selectTHCNO)
        {
            ViewBag.selectTHCNO = selectTHCNO;
            return View();
        }
        #endregion


        public ActionResult Prepareclaims()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Prepareclaims(claims objPCMCriteria, FormCollection fc, claims_Details CDM)
        {
            claims objPCMRequest = new claims();
            //string DocketNo = "", mDocNo = "", Suffix = "", NonDel = "", Short = "", Dam = "", Leakage = "", Pilferage = "", Other = "";
            //string COF = "", ClaimParty = "";
            string mDocNo = "", PKGType = "", ACTUWT = "", tot_frtamt = "", InvVal = "";
            try
            {
                var ND = fc["NonDelivery"];
                if (ND != null)
                {
                    objPCMRequest.NONDELY = "Y";
                }
                else
                {
                    objPCMRequest.NONDELY = "N";
                }

                var ST = fc["Shortage"];
                if (ST != null)
                {
                    objPCMRequest.Short = "Y";
                }
                else
                {
                    objPCMRequest.Short = "N";
                }

                var DM = fc["Damage"];
                if (DM != null)
                {
                    objPCMRequest.Damg = "Y";
                }
                else
                {
                    objPCMRequest.Damg = "N";
                }

                var LK = fc["Leakage"];
                if (LK != null)
                {
                    objPCMRequest.Leakg = "Y";
                }
                else
                {
                    objPCMRequest.Leakg = "N";
                }

                var PF = fc["Pilferage"];
                if (PF != null)
                {
                    objPCMRequest.pilfer = "Y";
                }
                else
                {
                    objPCMRequest.pilfer = "N";
                }


                var Ot = fc["Other"];
                if (Ot != null)
                {
                    objPCMRequest.Other = "Y";
                }
                else
                {
                    objPCMRequest.Other = "N";
                }



                var CB = fc["ClaimBasis"];
                if (CB != null)
                {
                    if (CB == "Bill")
                    {
                        objPCMRequest.COF = "B";
                    }

                    if (CB == "Cash")
                    {
                        objPCMRequest.COF = "C";
                    }

                    if (CB == "COF")
                    {
                        objPCMRequest.COF = "CF";
                    }
                }

                var PB = fc["PartyBasis"];
                if (PB != null)
                {
                    if (PB == "Consignor")
                    {
                        objPCMRequest.clmpty = "Consignor";
                    }
                    if (PB == "Consignee")
                    {
                        objPCMRequest.clmpty = "Consignee";
                    }
                    if (PB == "Other")
                    {
                        objPCMRequest.clmpty = "Other";
                    }
                }

                string DataFound = "N";
                if (objPCMCriteria.DOCKNO != null)
                {
                    DataTable DT = new DataTable();
                    DT = OS.getDocketDetail(objPCMCriteria.DOCKNO);
                    if (DT.Rows.Count > 0)
                    {
                        PKGType = Convert.ToString(DT.Rows[0]["PKGSTY"]);
                        mDocNo = Convert.ToString(DT.Rows[0]["DOCKNO"]);
                        ACTUWT = Convert.ToString(DT.Rows[0]["ACTUWT"]);
                        tot_frtamt = Convert.ToString(DT.Rows[0]["FREIGHT"]);
                        InvVal = Convert.ToString(DT.Rows[0]["DECLVAL"]);
                        DataFound = "Y";
                    }
                }

                string DataFoundFLAG = "N";
                if (DataFound == "Y")
                {
                    DataTable DT = new DataTable();
                    string SQRY1 = "select * from WEBX_CIR_HDR where DOCKNO='" + objPCMCriteria.DOCKNO + "'";
                    DT = GF.GetDataTableFromSP(SQRY1);
                    if (DT.Rows.Count > 0)
                    {
                        mDocNo = Convert.ToString(DT.Rows[0]["DOCKNO"]);
                        DataFoundFLAG = "Y";
                    }
                }
                if (DataFoundFLAG == "Y")
                {
                    return RedirectToAction("CIRAdded", "Operation");
                }

                objPCMRequest.DOCKNO = objPCMCriteria.DOCKNO;
                objPCMRequest.DOCKSF = objPCMCriteria.DOCKSF;
                objPCMRequest.PKGSTY = PKGType;
                objPCMRequest.ACTUWT = Convert.ToDecimal(ACTUWT);
                objPCMRequest.tot_frtamt = Convert.ToDecimal(tot_frtamt);
                objPCMRequest.InvVal = Convert.ToDecimal(InvVal);

                //if (CB != null)
                //{
                //    if (CB == "Bill")
                //    {
                //        objPCMRequest.COF = "B";
                //    }

                //    if (CB == "Cash")
                //    {
                //        objPCMRequest.COF = "C";
                //    }

                //    if (CB == "COF")
                //    {
                //        objPCMRequest.COF = "CF";
                //    }
                //}

                var mCIRNo = "";
                DataTable DT1 = new DataTable();
                DT1 = OS.GetCIRDocketDetail(BaseLocationCode);
                if (DT1.Rows.Count > 0)
                {
                    mCIRNo = Convert.ToString(DT1.Rows[0]["CIRNO"]);
                }
                objPCMRequest.CIRNo = mCIRNo;
                objPCMRequest.CIR_PREPBY = BaseUserName;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
            return RedirectToAction("ClaimsInvestReport", objPCMRequest);
        }

        public ActionResult ClaimsInvestReport(claims objPCMRequest1, FormCollection fc, HttpPostedFileBase file)
        {
            decimal TOAMT = 0;
            DataTable DT1 = new DataTable();
            DT1 = OS.GetDetailRecord(objPCMRequest1.DOCKNO);
            if (DT1.Rows.Count > 0)
            {

                objPCMRequest1.CIR_PREPBY = BaseUserName;
                objPCMRequest1.BR_PREPDT = objPCMRequest1.BR_PREPDT;
                objPCMRequest1.DOCKNO = Convert.ToString(DT1.Rows[0]["DOCKNO"]);
                objPCMRequest1.DOCKDT = Convert.ToString(DT1.Rows[0]["dockdt"]);
                objPCMRequest1.ORGNCD = Convert.ToString(DT1.Rows[0]["orgncd"]);
                objPCMRequest1.REASSIGN_DESTCD = Convert.ToString(DT1.Rows[0]["reassign_destcd"]);
                objPCMRequest1.CSGNNM = Convert.ToString(DT1.Rows[0]["CSGNNM"]);
                objPCMRequest1.CSGENM = Convert.ToString(DT1.Rows[0]["CSGENM"]);
                objPCMRequest1.CSGNADDR = Convert.ToString(DT1.Rows[0]["CSGNADDR"]);
                objPCMRequest1.CSGEADDR = Convert.ToString(DT1.Rows[0]["CSGEADDR"]);
                objPCMRequest1.CSGNPinCode = Convert.ToString(DT1.Rows[0]["CSGNPinCode"]);
                objPCMRequest1.CSGEPinCode = Convert.ToString(DT1.Rows[0]["CSGEPinCode"]);
                objPCMRequest1.CSGNTeleNo = Convert.ToString(DT1.Rows[0]["CSGNTeleNo"]);
                objPCMRequest1.CSGETeleNo = Convert.ToString(DT1.Rows[0]["CSGETeleNo"]);
                objPCMRequest1.Temp1 = Convert.ToString(DT1.Rows[0]["PKGSNO"].ToString());




                objPCMRequest1.ClmPrtyGrd = "-";

                if (objPCMRequest1.clmpty == "Consignor")
                {
                    objPCMRequest1.clmptynm = Convert.ToString(DT1.Rows[0]["CSGNNM"]);
                    objPCMRequest1.clmptyadd = Convert.ToString(DT1.Rows[0]["CSGNADDR"]);
                    objPCMRequest1.clmptypin = Convert.ToString(DT1.Rows[0]["CSGNPinCode"]);
                    objPCMRequest1.clmptytel = Convert.ToString(DT1.Rows[0]["CSGNTeleNo"]);
                }
                if (objPCMRequest1.clmpty == "Consignee")
                {
                    objPCMRequest1.clmptynm = Convert.ToString(DT1.Rows[0]["CSGENM"]);
                    objPCMRequest1.clmptyadd = Convert.ToString(DT1.Rows[0]["CSGEADDR"]);
                    objPCMRequest1.clmptypin = Convert.ToString(DT1.Rows[0]["CSGEPinCode"]);
                    objPCMRequest1.clmptytel = Convert.ToString(DT1.Rows[0]["CSGETeleNo"]);
                }

                if (objPCMRequest1.NONDELY == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "NonDelivery";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "NonDelivery";
                    }
                }
                if (objPCMRequest1.Short == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "Shortage";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "Shortage";
                    }
                }
                if (objPCMRequest1.Damg == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "Damage";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "Damage";
                    }
                }
                if (objPCMRequest1.Leakg == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "Leakage";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "Leakage";
                    }
                }
                if (objPCMRequest1.pilfer == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "Pilferage";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "Pilferage";
                    }
                }
                if (objPCMRequest1.Other == "Y")
                {
                    if (objPCMRequest1.Claimstr == "")
                    {
                        objPCMRequest1.Claimstr = "Other";
                    }
                    else
                    {
                        objPCMRequest1.Claimstr = objPCMRequest1.Claimstr + "," + "Other";
                    }
                }

                objPCMRequest1.insuyn = Convert.ToString(DT1.Rows[0]["insuyn"]);
                objPCMRequest1.paydesc = Convert.ToString(DT1.Rows[0]["paydesc"]);
                objPCMRequest1.proddesc = Convert.ToString(DT1.Rows[0]["proddesc"]);
                objPCMRequest1.tot_frtamt = objPCMRequest1.tot_frtamt;
                objPCMRequest1.ACTUWT = objPCMRequest1.ACTUWT;
                objPCMRequest1.InvVal = objPCMRequest1.InvVal;
                objPCMRequest1.IncomingFrom = Convert.ToString(DT1.Rows[0]["IncomingFrom"]);
                objPCMRequest1.mf = Convert.ToString(DT1.Rows[0]["mf"]);
                objPCMRequest1.thc = Convert.ToString(DT1.Rows[0]["thc"]);
                objPCMRequest1.thcdate = Convert.ToString(DT1.Rows[0]["thcdate"]);
                objPCMRequest1.vehno = Convert.ToString(DT1.Rows[0]["vehno"]);
                objPCMRequest1.route = Convert.ToString(DT1.Rows[0]["route"]);
                objPCMRequest1.invno = Convert.ToString(DT1.Rows[0]["invno"]);
                objPCMRequest1.Temp1 = Convert.ToString(DT1.Rows[0]["PKGSNO"].ToString());
                string SQRY = "select *  from WebX_Master_Users where  UserId ='" + objPCMRequest1.CIR_PREPBY + "' ";
                var dt2 = GF.GetDataTableFromSP(SQRY);
                if (dt2.Rows.Count > 0)
                    objPCMRequest1.BR_PREPNM = Convert.ToString(dt2.Rows[0]["Name"]);

            }
            return View(objPCMRequest1);
        }

        public ActionResult ClaimsReport(string DocketNo)
        {
            ViewBag.DOCN = DocketNo;
            return View();
        }

        public ActionResult cmdApprove_Click(claims PCM1, IEnumerable<HttpPostedFileBase> file, FormCollection fc)
        {

            string mCLetter = "", mICopy = "", mPOD = "", mTC = "", mFIR = "", mSRPT = "", mPhoto = "";
            string CLetter_Name = "", ICOPY_Name = "", POD_Name = "", TC_Name = "", FIR_Name = "", SRPT_Name = "", Photo_Name = "";
            string fileCLetter = "", fileICopy = "", filePOD = "", fileTC = "", fileFIR = "", fileSRPT = "", filePhoto = "", EXT1 = "";
            claims objPCMRequest1 = new claims();
            string path1 = Server.MapPath("~/Images/ClaimDocument/");
            string[] array = new string[12];
            int id = 1;
            PCM1.DOCKSF = ".";
            var allowedExtensions = new[] { ".Jpg", ".png", ".jpg", "jpeg" };
            foreach (var item in file)
            {
                string Myfile = "";
                var EXT = "";
                var filename = "";
                if (item != null)
                {

                    if (item.ContentLength > 0)
                    {

                        filename = Path.GetFileName(item.FileName);
                        EXT = Path.GetExtension(item.FileName);
                        EXT1 = EXT;
                        //String Name = Path.GetFileNameWithoutExtension(filename);
                        //string Myfile = Name + "_" + PCM1.DOCKNO+EXT;
                        if (allowedExtensions.Contains(EXT))
                        {
                            Myfile = PCM1.DOCKNO + "_" + id + EXT;
                            string strDirectoryName = Server.MapPath("~/Images/ClaimDocument/") + PCM1.DOCKNO;
                            if (Directory.Exists(strDirectoryName) == false)
                                Directory.CreateDirectory(strDirectoryName);
                            string fpath = strDirectoryName + "/" + Myfile;
                            // item.SaveAs(fpath);
                        }

                    }
                }
                array[id] = filename;
                if (id == 1)
                {
                    fileCLetter = filename;
                }
                if (id == 2)
                {
                    fileICopy = filename;
                }
                if (id == 3)
                {
                    filePOD = filename;
                }
                if (id == 4)
                {
                    fileTC = filename;
                }
                if (id == 5)
                {
                    fileFIR = filename;
                }
                if (id == 6)
                {
                    fileSRPT = filename;
                }
                if (id == 7)
                {
                    filePhoto = filename;
                }
                id++;

                string SQRY2 = "select * from vw_createFileNameCIR";
                var dt2 = GF.GetDataTableFromSP(SQRY2);
                if (dt2.Rows.Count > 0)
                {
                    mCLetter = Convert.ToString(dt2.Rows[0]["mCLetter"]);
                    mICopy = Convert.ToString(dt2.Rows[0]["mICopy"]);
                    mPOD = Convert.ToString(dt2.Rows[0]["mPOD"]);
                    mTC = Convert.ToString(dt2.Rows[0]["mTC"]);
                    mFIR = Convert.ToString(dt2.Rows[0]["mFIR"]);
                    mSRPT = Convert.ToString(dt2.Rows[0]["mSRPT"]);
                    mPhoto = Convert.ToString(dt2.Rows[0]["mPhoto"]);
                }
                if (fileCLetter != "")
                {
                    if (mCLetter == "")
                    {
                        mCLetter = "PCL00000001";
                    }
                    string CLetter = mCLetter + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + CLetter);
                    CLetter_Name = CLetter;
                    fileCLetter = "";
                }
                if (fileICopy != "")
                {
                    if (mICopy == "")
                    {
                        mICopy = "PIC00000001";
                    }
                    string ICopy = mICopy + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + ICopy);
                    ICOPY_Name = ICopy;
                    fileICopy = "";
                }
                if (filePOD != "")
                {
                    if (mPOD == "")
                    {
                        mPOD = "POD00000001";
                    }
                    string POD = mPOD + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + POD);
                    POD_Name = POD;
                    filePOD = "";
                }
                if (fileTC != "")
                {
                    if (mTC == "")
                    {
                        mTC = "THC00000001";
                    }
                    string TC = mTC + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + TC);
                    TC_Name = TC;
                    fileTC = "";
                }

                if (fileFIR != "")
                {
                    if (mFIR == "")
                    {
                        mFIR = "FIR00000001";
                    }
                    string FIR = mFIR + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + FIR);
                    FIR_Name = FIR;
                    fileFIR = "";
                }
                if (fileSRPT != "")
                {
                    if (mSRPT == "")
                    {
                        mSRPT = "SRT00000001";
                    }
                    string SRPT = mSRPT + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + SRPT);
                    SRPT_Name = SRPT;
                    fileSRPT = "";
                }
                if (filePhoto != "")
                {
                    if (mPhoto == "")
                    {
                        mPhoto = "PHT00000001";
                    }
                    string Photo = mPhoto + EXT1;
                    item.SaveAs(path1 + PCM1.DOCKNO + "/" + Photo);
                    Photo_Name = Photo;
                    filePhoto = "";
                }
            }
            objPCMRequest1.CIRBR = BaseLocationCode;
            objPCMRequest1.DOCKNO = PCM1.DOCKNO;
            objPCMRequest1.DOCKSF = PCM1.DOCKSF;
            objPCMRequest1.CIRNo = PCM1.CIRNo;
            objPCMRequest1.CIR_PREPBY = PCM1.CIR_PREPBY;
            objPCMRequest1.BR_PREPNM = PCM1.BR_PREPNM;
            objPCMRequest1.LOCEMPCD = PCM1.CIR_PREPBY;
            objPCMRequest1.LOCEMPNM = PCM1.BR_PREPNM;
            objPCMRequest1.NONDELY = PCM1.NONDELY;
            objPCMRequest1.Short = PCM1.Short;
            objPCMRequest1.Damg = PCM1.Damg;
            objPCMRequest1.Leakg = PCM1.Leakg;
            objPCMRequest1.pilfer = PCM1.pilfer;
            objPCMRequest1.Other = PCM1.Other;
            objPCMRequest1.COF = PCM1.COF;
            objPCMRequest1.clmpty = PCM1.clmpty;
            objPCMRequest1.clmptynm = PCM1.clmptynm;
            objPCMRequest1.clmptyadd = PCM1.clmptyadd;
            objPCMRequest1.clmptypin = PCM1.clmptypin;
            objPCMRequest1.clmptytel = PCM1.clmptytel;
            objPCMRequest1.CIRDT = PCM1.CIRDT;
            objPCMRequest1.CLMAMT = PCM1.CLMAMT;
            objPCMRequest1.CName = PCM1.CName;
            objPCMRequest1.SName = PCM1.SName;
            objPCMRequest1.SEName = PCM1.SEName;
            objPCMRequest1.CValue = PCM1.CValue;
            objPCMRequest1.SValue = PCM1.SValue;
            objPCMRequest1.SEValue = PCM1.SEValue;
            objPCMRequest1.IncCoName = PCM1.IncCoName;
            objPCMRequest1.IncPNo = PCM1.IncPNo;
            objPCMRequest1.ChallanNo = PCM1.ChallanNo;

            if (PCM1.MBusiness == null)
            {
                PCM1.MBusiness = "0";
            }

            if (PCM1.Booking == null)
            {
                PCM1.Booking = "0";
            }

            if (PCM1.Delivery == null)
            {
                PCM1.Delivery = "0";
            }


            objPCMRequest1.MBusiness = PCM1.MBusiness;
            objPCMRequest1.Booking = PCM1.Booking;
            objPCMRequest1.Delivery = PCM1.Delivery;
            objPCMRequest1.NBusiness = PCM1.NBusiness;
            objPCMRequest1.ApprLoc = PCM1.ApprLoc;
            objPCMRequest1.BR_FLAG = "Y";
            objPCMRequest1.AO_FLAG = "N";
            objPCMRequest1.HO_FLAG = "N";


            if (PCM1.BRATTPCL == "true")
            {
                PCM1.BRATTPCL = "YES";
            }
            else
            {
                PCM1.BRATTPCL = "NO";
            }

            if (PCM1.BRATTPIC == "true")
            {
                PCM1.BRATTPIC = "YES";
            }
            else
            {
                PCM1.BRATTPIC = "NO";
            }

            if (PCM1.BRATTPOD == "true")
            {
                PCM1.BRATTPOD = "YES";
            }
            else
            {
                PCM1.BRATTPOD = "NO";
            }

            if (PCM1.BRATTTCTHC == "true")
            {
                PCM1.BRATTTCTHC = "YES";
            }
            else
            {
                PCM1.BRATTTCTHC = "NO";
            }

            if (PCM1.BRATTFIR == "true")
            {
                PCM1.BRATTFIR = "YES";
            }
            else
            {
                PCM1.BRATTFIR = "NO";
            }

            if (PCM1.BRATTSRPT == "true")
            {
                PCM1.BRATTSRPT = "YES";
            }
            else
            {
                PCM1.BRATTSRPT = "NO";
            }

            if (PCM1.BRATTPHOTO == "true")
            {
                PCM1.BRATTPHOTO = "YES";
            }
            else
            {
                PCM1.BRATTPHOTO = "NO";
            }

            objPCMRequest1.BRATTPCL = PCM1.BRATTPCL;
            objPCMRequest1.BRATTPCL_FILENAME = CLetter_Name;

            objPCMRequest1.BRATTPIC = PCM1.BRATTPIC;
            objPCMRequest1.BRATTPIC_FILENAME = ICOPY_Name;

            objPCMRequest1.BRATTPOD = PCM1.BRATTPOD;
            objPCMRequest1.BRATTPOD_FILENAME = POD_Name;

            objPCMRequest1.BRATTTCTHC = PCM1.BRATTTCTHC;
            objPCMRequest1.BRATTTCTHC_FILENAME = TC_Name;

            objPCMRequest1.BRATTFIR = PCM1.BRATTFIR;
            objPCMRequest1.BRATTFIR_FILENAME = FIR_Name;

            objPCMRequest1.BRATTSRPT = PCM1.BRATTSRPT;
            objPCMRequest1.BRATTSRPT_FILENAME = SRPT_Name;

            objPCMRequest1.BRATTPHOTO = PCM1.BRATTPHOTO;
            objPCMRequest1.BRATTPHOTO_FILENAME = Photo_Name;


            DataTable DT1 = new DataTable();
            DT1 = OS.Insert_CIR_Claim(objPCMRequest1);

            return RedirectToAction("CIR_Successfully", objPCMRequest1);
        }
        public string CheckDockNO(string DocketNo)
        {

            string result = "";
            //string SQRY = "exec usp_getDocketNo '" + DockNo + "'";
            string SQRY2 = "exec usp_getDocketNo '" + DocketNo + "'";

            var dt2 = GF.GetDataTableFromSP(SQRY2);

            if (dt2.Rows.Count > 0)
                result = "true";
            else
                result = "false";
            return result;
        }

        public JsonResult GetLocationListJson(string searchTerm)
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag.ToUpper() == "Y" && (c.LocName.ToUpper().Contains(searchTerm.ToUpper()) || c.LocCode.ToUpper().Contains(searchTerm.ToUpper()))).ToList().OrderBy(c => c.LocName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CIR_Successfully(claims objPCMRequest2)
        {
            ViewBag.CIRNO = objPCMRequest2.CIRNo;
            ViewBag.DNO = objPCMRequest2.DOCKNO;
            return View();
        }

        public ActionResult CIRAdded()
        {
            return View();
        }

        public ActionResult ClaimList()
        {
            return View();
        }
        public ActionResult ClaimListForMFGeneration(claims CLM)
        {
            List<claims> ClaimListForMFGeneration = new List<claims>();
            ClaimListForMFGeneration = OS.ClaimListForMFGeneration(GF.FormateDate(CLM.FromDate), GF.FormateDate(CLM.ToDate), CLM.DOCKNO);
            return PartialView("_ViewClaimList", ClaimListForMFGeneration);
        }

        public ActionResult ClaimApproval()
        {
            return View();
        }

        public ActionResult ClaimApprovalForGeneration(claims CLMOBJ)
        {

            List<claims> ClaimApprovalList = new List<claims>();
            ClaimApprovalList = OS.ClaimApproval_List(GF.FormateDate(CLMOBJ.FromDate), GF.FormateDate(CLMOBJ.ToDate), CLMOBJ.CIRNo, CLMOBJ.DOCKNO);
            return PartialView("_Pending_Claims_for_Approval ", ClaimApprovalList);
        }
        #region Check SealNo Branch Wise Exist or Not In THC Generation
        public JsonResult GetSealNoExistNotJson(string SealNo, string BRCD)
        {
            webx_DCR_Header WDHM = new webx_DCR_Header();
            try
            {
                DataTable DT_DCRHDR = MS.GetExistOrNotSealNoDetail(SealNo, BRCD);
                string Count = "", Message = "", BranchCode = "";
                if (DT_DCRHDR.Rows.Count > 0)
                {
                    Count = DT_DCRHDR.Rows[0]["CNT"].ToString();
                    Message = DT_DCRHDR.Rows[0]["MSG"].ToString();
                    BranchCode = DT_DCRHDR.Rows[0]["BRCD"].ToString();
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                        MSG = Message,
                        BranchCode = BranchCode,
                    }
                };
            }
            catch (Exception EX)
            {
                Error_Logs(ControllerName, "GetSealNoExistNotJson", "Json", "SEAL NO - THC Generation", EX.Message);
                return Json(WDHM);
            }
        }
        #endregion
    }
}