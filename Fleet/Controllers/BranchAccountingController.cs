﻿using CYGNUS.Models;
using Fleet.Classes;
using Fleet.Models;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;
using FleetDataService.ViewModels;

namespace Fleet.Controllers
{
    [Authorize]
    public class BranchAccountingController : BaseController
    {
        /* usp_webx_vouchertrans_arch_updateData */

        BranchAccountingService BAS = new BranchAccountingService();
        OperationService OS = new OperationService();
        GeneralFuncations GF = new GeneralFuncations();
        MasterService MS = new MasterService();
                
        public ActionResult Index()
        {
            return View();
        }

        #region Branch Accouting Master

        public ActionResult BranchAccoutingMaster()
        {
            try
            {
                BranchAccountingMaster BMVW = new BranchAccountingMaster();
                //BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode);
                BMVW.ListCBAM = new List<CYGNUS_Branch_Accounting_Master>();
                ViewBag.ActionName = "BranchAccoutingMaster";
                return View(BMVW);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _BranchAcctEntryModule(string BRCD)
        {
            BranchAccountingMaster BMVW = new BranchAccountingMaster();
            BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BRCD);
            return PartialView("_BranchAcctEntryModule_Partial", BMVW.ListCBAM);
        }

        [HttpPost]
        public ActionResult BranchAccoutingMaster(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Accounting_Master> BAEM)
        {
            List<CYGNUS_Branch_Accounting_Master> ListBAEM = new List<CYGNUS_Branch_Accounting_Master>();
            string XML_BRCD = "";

            try
            {
                if (BMVW.BRCD == "" || BMVW.BRCD == null)
                {
                    @ViewBag.StrError = "Please Select Branch.";
                    return View("Error");
                }

                if (BAEM != null)
                {
                    foreach (var item in BAEM)
                    {
                        CYGNUS_Branch_Accounting_Master ObjBAEM = new CYGNUS_Branch_Accounting_Master();
                        ObjBAEM.ID = item.ID;
                        ObjBAEM.SrNo = item.SrNo;
                        ObjBAEM.ChargeName = item.ChargeName;
                        ObjBAEM.MaxAmt = item.MaxAmt;
                        ObjBAEM.Accode = item.Accode;
                        ObjBAEM.Narration = item.Narration;
                        ObjBAEM.BRCD = BMVW.BRCD;
                        ObjBAEM.IsActive = item.IsActive;
                        XML_BRCD = BMVW.BRCD;
                        ListBAEM.Add(ObjBAEM);
                    }
                }
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(ListBAEM.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, ListBAEM);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DT = BAS.InserBRCDAcc(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper(), XML_BRCD);

                int Status = Convert.ToInt32(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("BrcdAccDone");
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult BrcdAccDone()
        {
            return View();
        }

        #endregion

        #region BranchAccoting Module

        public ActionResult BRCDAcc_Criteria()
        {
            try
            {
                BranchAccountingMaster BMVW = new BranchAccountingMaster();
                string basefinyear = BaseYearVal;
                List<CYGNUS_Branch_Expance> optrkList = BAS.GET_BR_Dashbord_Report_Approval(BaseLocationCode);
                DataTable DT = BAS.BRStatusSummaryCashOnToday(BaseLocationCode, GF.FormateDate(System.DateTime.Now), GF.FormateDate(System.DateTime.Now), basefinyear, 0);
                BMVW.ListCBAM = BAS.GetBRCD_Acc_Master_List(BaseUserName.ToUpper(), BaseLocationCode);
                decimal NetAmt = Convert.ToDecimal(DT.Rows[0][2].ToString());
                BMVW.List_BRS = optrkList;
                CYGNUS_Branch_Expance BRSAmt = new CYGNUS_Branch_Expance();
                BRSAmt.NetAmt = NetAmt;
                BMVW.BRS = BRSAmt;
                ViewBag.ActionName = "Expenses";
                return View(BMVW);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        #endregion

        #region Electricity Expance

        #region ElectriCity Master Office Godown Details Master

        public ActionResult Office_Godown_Details()
        {
            ElectricMaster VMEM = new ElectricMaster();
            CYGNUS_Elect_Office_Godown_Details CEOGD = new CYGNUS_Elect_Office_Godown_Details();
            try
            {
                string BRCD = BaseLocationCode;
                //DataTable DTGET = BAS.GetOfficeGodown(BRCD);

                //List<CYGNUS_Elect_Office_Godown_Details> ListCEOG = DataRowToObject.CreateListFromTable<CYGNUS_Elect_Office_Godown_Details>(DTGET);
                VMEM.ListCEOGD = new List<CYGNUS_Elect_Office_Godown_Details>();
                return View(VMEM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _Add_Electric_Master_Partial(int id, int Type, string Location)
        {
            try
            {
                if (Type == 1)
                {
                    List<CYGNUS_Porter_Expense> ListCPE = new List<CYGNUS_Porter_Expense>();
                    DataTable DTGET = BAS.GetOfficeGodown(Location);
                    List<CYGNUS_Elect_Office_Godown_Details> ListCEOG = DataRowToObject.CreateListFromTable<CYGNUS_Elect_Office_Godown_Details>(DTGET);
                    return PartialView("_Electric_Master_Partial_List", ListCEOG);
                }
                else
                {
                    CYGNUS_Elect_Office_Godown_Details ObjCBTT = new CYGNUS_Elect_Office_Godown_Details();
                    ObjCBTT.ID = id;
                    return PartialView("_Electric_Master_Partial", ObjCBTT);
                }

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Office_Godown_Details(CYGNUS_Elect_Office_Godown_Details CEOGD, List<CYGNUS_Elect_Office_Godown_Details> ListCEOGD)
        {
            try
            {
                string BRCD = BaseLocationCode;
                DataTable DT = BAS.GetOfficeGodown(BRCD);

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(ListCEOGD.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, ListCEOGD);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DTADD = BAS.InserCEOGD(xmlDoc1.InnerXml, CEOGD.ID, BaseLocationCode, BaseUserName.ToUpper(), CEOGD.BRCD);
                return RedirectToAction("Master_Done", new { ID = 1 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Master_Done(int ID)
        {
            if (ID == 1)
            {
                ViewBag.Title = "Office Godown Detail Done.";
            }
            else if (ID == 2)
            {
                ViewBag.Title = "Porter Master Expense Done.";
            }
            else if (ID == 3)
            {
                ViewBag.Title = "Driver Incentive TSP Expense Master.";
            }
            return View();
        }

        #endregion

        #region Electriciy Expenses Entry

        public ActionResult Electriciy_Expenses()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 1;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Title = "Electricity Expense Module";
                ViewBag.ActionName = "Electriciy_Expenses_Submit";

                CYGNUS_Branch_Electricity_Expenses_DET Obj_CBEE = new CYGNUS_Branch_Electricity_Expenses_DET();
                Obj_CBEE.ID = 1;

                if (BaseLocationCode != "HQTR")
                {
                    PEVM.List_Elect_AddressDetail = BAS.Get_ElectriCity_Address(BaseLocationCode, BaseUserName.ToUpper(), BaseLocationCode);
                }
                List<CYGNUS_Branch_Electricity_Expenses_DET> List_CBEE = new List<CYGNUS_Branch_Electricity_Expenses_DET>();
                List_CBEE.Add(Obj_CBEE);
                PEVM.List_Elec_Expense_DET = List_CBEE;
                PEVM.Elec_Expense_HDR = new CYGNUS_Branch_Electricity_Expenses_HDR();
                PEVM.Elec_Expense_HDR.BR_Date = System.DateTime.Now;
                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Electriciy_Expenses_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Electricity_Expenses_DET> CPEEDET, HttpPostedFileBase[] files)
        {
            //string extension = "", name = "", path = "";
            try
            {
                
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 1);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Total_Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "Maximum Approved Amount For Electricity Expance was '" + MaxLimit + "'.";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "Please Enter Valid amount Forr Electricity Expance.";
                    return View("Error");
                }

                PEVM.Elec_Expense_HDR.BRCD = BaseLocationCode;
                PEVM.Elec_Expense_HDR.ExpenceType = PEVM.Type2;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Elec_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Elec_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Electricity_DET_XML(CPEEDET, 1, files);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Electricity_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "1" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public JsonResult GetElectriciy_PreviousMonth(int Month, string Month_Year, string Address)
        {
            string IsRecordFound = "";
            DataTable DT = new DataTable();
            try
            {
                string[] Month_Year_String = Month_Year.Split(new string[] { ":" }, StringSplitOptions.None);
                string Month_Name = Month_Year_String[0].Trim();
                string Year = Month_Year_String[1].Trim();

                string SQRY = "exec USP_Get_Duplicate_Month '" + BaseLocationCode + "','" + BaseUserName + "','" + Year + "','" + Month + "','"+Address+"'";

                DT = GF.GetDataTableFromSP(SQRY);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = DT.Rows[0][0].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
            catch (Exception)
            {
                IsRecordFound = "2";

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        public JsonResult GetElect_AddressDetail(string Location)
        {
            List<CYGNUS_Elect_Office_Godown_Details> List_Elect_AddressDetail = new List<CYGNUS_Elect_Office_Godown_Details>();

            List_Elect_AddressDetail = BAS.Get_ElectriCity_Address(Location, BaseUserName.ToUpper(), BaseLocationCode);


            var VehicleList = (from e in List_Elect_AddressDetail
                               select new
                               {
                                   Value = e.Text,
                                   Text = e.Text,
                               }).Distinct().ToList();
            return Json(VehicleList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetElectriciy_Detils(string Brcd, string Address)
        {
            string IsRecordFound = "";
            CYGNUS_Branch_Electricity_Expenses_DET ObjCEOGD = new CYGNUS_Branch_Electricity_Expenses_DET();
            try
            {
                ObjCEOGD = BAS.Get_ElectriCity_Detail(Brcd, BaseUserName.ToUpper(), Address).First();

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = 1,
                        Total_Area_in_sq_ft = ObjCEOGD.Total_Area_sqft,
                        PreviousReading = ObjCEOGD.PreviousReading,
                        RateAsPer_Agreement = ObjCEOGD.RateAsPer_Agreement,
                        Number_of_Rooms = ObjCEOGD.NoOf_Rooms,
                        Computers = ObjCEOGD.Computers,
                        Printers = ObjCEOGD.Printers,
                        TubeLights = ObjCEOGD.TubeLights,
                        Bulbs = ObjCEOGD.Bulbs,
                        CFLs = ObjCEOGD.CFLs,
                        Others = ObjCEOGD.Others,
                        Previous_Claimed_Amount = ObjCEOGD.Previous_Claimed_Amount,
                        Previous_Month_When_Claimed = ObjCEOGD.Previous_Month_When_Claimed,
                        Name_Of_Land_Lord = ObjCEOGD.Name_Of_Land_Lord,
                        Bank_Account_No = ObjCEOGD.Land_Lord_Bank_Account_No,
                        IFSC_Code = ObjCEOGD.Land_Lord_IFSC_Code,
                    }
                };
            }
            catch (Exception)
            {
                IsRecordFound = "2";

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        #endregion

        #region Electriciy Expenses Approval Submit

        public ActionResult Electricity_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Electricity_Expenses_DET> CPEEDET)
        {
            try
            {
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Elec_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Elec_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                int MaxLimit = GetCharge_MaxLimit(PEVM.Elec_Expense_HDR.BRCD, 1);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Total_Amount));
                int ApprovedSumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount_Passed));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "Maximum Approved Amount For Electriciy Expance was '" + MaxLimit + "'.";
                    return View("Error");
                }
                if (MaxLimit < ApprovedSumAmount)
                {
                    @ViewBag.StrError = "Maximum Approved Amount For Electriciy Expance was '" + MaxLimit + "'.";
                    return View("Error");
                }

                string XML_Det = Electricity_DET_XML(CPEEDET, 2,null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Electricity_Expence_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Elec_Expense_HDR.BillNo, PEVM.Elec_Expense_HDR.ExpenceType);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "1" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Electriciy Expenses Rejection Submit

        [HttpPost]
        public ActionResult Electricity_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Electricity_Expenses_DET> CPEEDET)
        {
            try
            {
                string XML = "<root>";
                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        XML = XML + "<CYGNUS_Branch_Electricity_Expenses_DET>";
                        XML = XML + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML = XML + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML = XML + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                        XML = XML + "<ID>" + item.ID + "</ID>";
                        XML = XML + "</CYGNUS_Branch_Electricity_Expenses_DET>";
                    }
                }
                else
                {
                    @ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }
                XML = XML + "</root>";

                DataTable DT = BAS.USP_Elec_Expance_Branch_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Elec_Expense_HDR.BillNo, PEVM.Elec_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 1, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string Electricity_DET_XML(List<CYGNUS_Branch_Electricity_Expenses_DET> CPEEDET, int Type, HttpPostedFileBase[] files)
        {
            string extension = "", UserFileName = "", path="";
            string XML_Det = "<root>";

            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Branch_Electricity_Expenses_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        #region Electriciy Expense Images Image Upload
                        try
                        {
                            if (((System.Web.HttpPostedFileBase[])(files)) != null)
                            {
                                foreach (var fileobj in files)
                                {
                                    var file = ((System.Web.HttpPostedFileBase[])(files));
                                    if (fileobj.ContentLength > 0)
                                    {
                                        extension = System.IO.Path.GetExtension(fileobj.FileName);
                                        UserFileName = BaseUserName + extension;
                                        path = Server.MapPath("~/Images/ElectriCityBillImages/") + UserFileName;
                                        string strDirectoryName = Server.MapPath("~/Images/ElectriCityBillImages/");
                                        if (Directory.Exists(strDirectoryName) == false)
                                            Directory.CreateDirectory(strDirectoryName);
                                        fileobj.SaveAs(path);
                                        //DocumentUploadedPath = AzureStorageHelper.UploadBlobFileForUser(fileobj, UserFileName, BaseUserName.ToString(), type);
                                    }

                                }
                            }
                        }
                        catch (Exception)
                        {
                            // DocumentUploadedPath = Em.Elec_Expense_DET.b;
                        }


                        #endregion
                        XML_Det = XML_Det + "<Location>" + item.Location + "</Location>";
                        XML_Det = XML_Det + "<Address>" + item.Address + "</Address>";
                        XML_Det = XML_Det + "<Total_Area_sqft>" + item.Total_Area_sqft + "</Total_Area_sqft>";
                        XML_Det = XML_Det + "<NoOf_Rooms>" + item.NoOf_Rooms + "</NoOf_Rooms>";
                        XML_Det = XML_Det + "<Computers>" + item.Computers + "</Computers>";
                        XML_Det = XML_Det + "<TubeLights>" + item.TubeLights + "</TubeLights>";
                        XML_Det = XML_Det + "<Bulbs>" + item.Bulbs + "</Bulbs>";
                        XML_Det = XML_Det + "<Printers>" + item.Printers + "</Printers>";
                        XML_Det = XML_Det + "<CFLs>" + item.CFLs + "</CFLs>";
                        XML_Det = XML_Det + "<Others_Elec_Equip_Gadgets>" + item.Others_Elec_Equip_Gadgets + "</Others_Elec_Equip_Gadgets>";
                        XML_Det = XML_Det + "<Previous_Claimed_Amount>" + item.Previous_Claimed_Amount + "</Previous_Claimed_Amount>";
                        XML_Det = XML_Det + "<Previous_Month_When_Claimed>" + item.Previous_Month_When_Claimed + "</Previous_Month_When_Claimed>";
                        XML_Det = XML_Det + "<Month_For_Which_Claim_BeingMade>" + item.Month_For_Which_Claim_BeingMade + "</Month_For_Which_Claim_BeingMade>";
                        XML_Det = XML_Det + "<PreviousReading>" + item.PreviousReading + "</PreviousReading>";
                        XML_Det = XML_Det + "<CurrentReading>" + item.CurrentReading + "</CurrentReading>";
                        XML_Det = XML_Det + "<UnitConsumption>" + item.UnitConsumption + "</UnitConsumption>";
                        XML_Det = XML_Det + "<RateAsPer_Agreement>" + item.RateAsPer_Agreement + "</RateAsPer_Agreement>";
                        XML_Det = XML_Det + "<Others>" + item.Others + "</Others>";
                        XML_Det = XML_Det + "<Total_Amount>" + item.Total_Amount + "</Total_Amount>";
                        XML_Det = XML_Det + "<RefBillNo>" + item.RefBillNo + "</RefBillNo>";
                        XML_Det = XML_Det + "<Month_Year>" + item.Month_Year + "</Month_Year>";
                        XML_Det = XML_Det + "<Bill_Year>" + item.Bill_Year + "</Bill_Year>";
                        XML_Det = XML_Det + "<Name_Of_Land_Lord>" + item.Name_Of_Land_Lord + "</Name_Of_Land_Lord>";
                        XML_Det = XML_Det + "<Land_Lord_Bank_Account_No>" + item.Land_Lord_Bank_Account_No + "</Land_Lord_Bank_Account_No>";
                        XML_Det = XML_Det + "<Land_Lord_IFSC_Code>" + item.Land_Lord_IFSC_Code + "</Land_Lord_IFSC_Code>";
                         XML_Det = XML_Det + "<ElectriCity_BillImage>" + UserFileName + "</ElectriCity_BillImage>";

                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Branch_Electricity_Expenses_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #endregion

        #region  Conveyance(Local) Expenses

        public ActionResult Conveyance_Local_Expenses()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 2;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.Title = "Conveyance(Local) Expenses";
                ViewBag.ActionName = "Conveyance_Expenses_Submit";

                CYGNUS_Conveyance_Expense_DET Obj_CBEE = new CYGNUS_Conveyance_Expense_DET();
                List<CYGNUS_Conveyance_Expense_DET> List_CBEE = new List<CYGNUS_Conveyance_Expense_DET>();
                Obj_CBEE.ID = 1;
                Obj_CBEE.Date = System.DateTime.Now;
                List_CBEE.Add(Obj_CBEE);
                PEVM.List_Conveyance_DET = List_CBEE;

                PEVM.Conveyance_Expense_HDR = new CYGNUS_Conveyance_Expense_HDR();
                PEVM.Conveyance_Expense_HDR.ExpenceType = 2;
                PEVM.Conveyance_Expense_HDR.BR_Date = System.DateTime.Now;

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Conveyance(Local) Expenses Entry

        public ActionResult Conveyance_Expenses_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Conveyance_Expense_DET> CPEEDET, IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 2);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "Maximum Approved Amount For Conveyance Expance was '" + MaxLimit + "'.";
                    return View("Error");
                }

                List<CYGNUS_Conveyance_Expense_DET> ListCPEEDet = new List<CYGNUS_Conveyance_Expense_DET>();

                PEVM.Conveyance_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Conveyance_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Conveyance_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Conveyance_DET_XML(CPEEDET, 1, files);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Conveyance_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "2" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Conveyance(Local) Expenses Approval

        public ActionResult Conveyance_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Conveyance_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Conveyance_Expense_DET> ListCPEEDet = new List<CYGNUS_Conveyance_Expense_DET>();

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Conveyance_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Conveyance_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                //int MaxLimit = GetCharge_MaxLimit(PEVM.Conveyance_Expense_HDR.BRCD, 2);
                //int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 2);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));
                int ApprovedSumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount_Passed));

                //if (MaxLimit < SumAmount)
                //{
                //    @ViewBag.StrError = "Maximum Approved Amount For Conveyance Expance was '" + MaxLimit + "'.";
                //    return View("Error");
                //}
                //if (MaxLimit < ApprovedSumAmount)
                //{
                //    @ViewBag.StrError = "Maximum Approved Amount For Conveyance Expance was '" + MaxLimit + "'.";
                //    return View("Error");
                //}
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "Please Enter Valid amount Forr Conveyance Expance.";
                    return View("Error");
                }


                string XML_Det = Conveyance_DET_XML(CPEEDET, 2, null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_Conveyance_Expence_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Conveyance_Expense_HDR.BillNo, PEVM.Conveyance_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "2" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string Conveyance_DET_XML(List<CYGNUS_Conveyance_Expense_DET> CPEEDET, int Type, IEnumerable<HttpPostedFileBase> files)
        {
            string XML_Det = "<root>";
            //int id = 0;
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    CYGNUS_Conveyance_Expense_DET ObjCPEEDet = new CYGNUS_Conveyance_Expense_DET();
                    XML_Det = XML_Det + "<CYGNUS_Conveyance_Expense_DET>";
                    if (Type == 1)
                    {
                        string ImageName = "";
                        #region ConveyanceExpenseImages Image Upload
                        try
                        {
                            if (Request.Files != null)
                            {
                                var File = Request.Files["Conve_" + item.ID];
                                if (File.ContentLength > 0)
                                {
                                    ImageName = SaveImage(File, item.ID, "ConveyanceExpenseImages");
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        #endregion

                        XML_Det = XML_Det + "<Employee>" + item.Employee + "</Employee>";
                        XML_Det = XML_Det + "<Date>" + item.Date.ToString("dd MMM yyyy") + "</Date>";
                        XML_Det = XML_Det + "<Client_Party_Name_Visiting_Place>" + item.Client_Party_Name_Visiting_Place + "</Client_Party_Name_Visiting_Place>";
                        XML_Det = XML_Det + "<Contract_Person>" + item.Contract_Person + "</Contract_Person>";
                        XML_Det = XML_Det + "<Contract_Num_Concerned>" + item.Contract_Num_Concerned + "</Contract_Num_Concerned>";
                        XML_Det = XML_Det + "<DocumentNo>" + item.DocumentNo + "</DocumentNo>";
                        XML_Det = XML_Det + "<Travel_Mode>" + item.Travel_Mode + "</Travel_Mode>";
                        XML_Det = XML_Det + "<MFrom>" + item.MFrom + "</MFrom>";
                        XML_Det = XML_Det + "<MTo>" + item.MTo + "</MTo>";
                        XML_Det = XML_Det + "<Opening>" + item.Opening + "</Opening>";
                        XML_Det = XML_Det + "<Closing>" + item.Closing + "</Closing>";
                        XML_Det = XML_Det + "<Reason_Purpose_Visting>" + item.Reason_Purpose_Visting + "</Reason_Purpose_Visting>";
                        XML_Det = XML_Det + "<Distance_Covered>" + item.Distance_Covered + "</Distance_Covered>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<ConveyanceExp_Image>" + ImageName + "</ConveyanceExp_Image>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Conveyance_Expense_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #region Conveyance(Local) Expenses Rejection

        [HttpPost]
        public ActionResult Conveyance_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Conveyance_Expense_DET> CPEEDET)
        {
            try
            {
                string XML = "";
                if (CPEEDET != null)
                {
                    XML = Conveyance_DET_XML(CPEEDET, 2, null);
                }
                else
                {
                    @ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }

                DataTable DT = BAS.USP_Conveyance_Expance_Branch_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Conveyance_Expense_HDR.BillNo, PEVM.Conveyance_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 2, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Conveyance(Local) Validation

        public JsonResult GetDocumentNoStatusDetails(string DocumentNo, string Type, string OppNonOpp_Type)
        {
            string CNT = "0", TripSheetNo = "";
            DataTable DT = new DataTable();
            if ((OppNonOpp_Type == "OPERATING" && (Type == "1" || Type == "2" || Type == "3")) || (OppNonOpp_Type == "NON_OPERATING" && (Type == "1" || Type == "2")))
            {
                DT = BAS.CheckDocumentNoStatusDetails(DocumentNo, Type, OppNonOpp_Type, BaseLocationCode, BaseUserName);
                CNT = DT.Rows[0][0].ToString();
                TripSheetNo = DT.Rows[0][1].ToString();
            }
            else
            {
                CNT = "1";
            }

            return new JsonResult()
            {
                Data = new
                {
                    CNT = CNT,
                    TripSheetNo = TripSheetNo
                }
            };
        }

        public JsonResult MonthlyPassValidation(string EmpCode, string Month)
        {
            return new JsonResult()
            {
                Data = new
                {
                    CNT = BAS.MonthlyPassValidation(EmpCode, Month)
                }
            };
        }

        #endregion

        #region Conveyance(Local) Expenses Approval Reject Criteria

        public ActionResult ConveyanceExpenseApprovalReject(int TYP)
        {
            try
            {
                BranchAccountingMaster BAM = new BranchAccountingMaster();
                BAM.ConveyanceExpense = "Conveyance Expenses";
                @ViewBag.ActionName = "AccotingApproval_Reject_Conveyance";
                BAM.SubType = 2;
                if (TYP == 1)
                {
                    if (BaseLocationLevel == 1)
                    {
                        BAM.SubmitType = "1";
                        ViewBag.Title = "Conveyance(Local) Expenses Approval Reject Criteria";
                    }
                    else
                    {
                        return RedirectToAction("URLRedirect", "Home");
                    }
                }
                else
                {
                    ViewBag.Title = "Conveyance(Local) Expenses Payment Criteria";
                }
                return View(BAM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        public JsonResult Get_Staff_ListFor_Conveyance(string Branch, string SubmitType)
        {
            List<CYGNUS_Branch_Expance> ListPorter = new List<CYGNUS_Branch_Expance>();
            ListPorter = BAS.GetStaffListFor_Conveyance(Branch, SubmitType);

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.Value,
                                  Text = e.Text,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_Month_For_Conveyance(string Staff, string SubmitType)
        {
            List<CYGNUS_Branch_Expance> ListPorter = new List<CYGNUS_Branch_Expance>();
            ListPorter = BAS.GetMonth_For_Conveyance(Staff, SubmitType);

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.Value,
                                  Text = e.Text,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccotingApproval_Conveyance(string Location, string Staff, int Month, string SubmitType)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            PEEM.ListCPEE_HDR = new List<CYGNUS_Branch_Expenses_EntryHDR>();
            PEEM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();
            PEEM.CPEE_HDR.Approve_Account_Type = 1;
            PEEM.ListCPEE_HDR = BAS.GetConveyanceAprroval_List(Location, Staff, Month, SubmitType);
            return PartialView("_AccotingApproval_Conveyance", PEEM);
        }

        public ActionResult AccotingApproval_Reject_Conveyance(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Expenses_EntryHDR> CPEEApprov)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                CYGNUS_Branch_Expenses_EntryHDR CBE = new CYGNUS_Branch_Expenses_EntryHDR();
                string Nos = "";
                foreach (var item in CPEEApprov.Where(c => c.IsChecked == true).ToList())
                {

                    if (item.IsChecked)
                    {
                        if (Nos == "")
                            Nos = item.BillNo.ToString();
                        else
                            Nos = Nos + "," + item.BillNo;
                    }

                    CBE.Bill_Status_ID = item.Bill_Status_ID;
                }
                DataSet DS = BAS.GetBrancAcct_Aprroval_Reject_Conveyance(Nos, CBE.Bill_Status_ID);
                PEVM.Conveyance_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                PEVM.List_Conveyance_DET = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_DET>(DS.Tables[1]);

                PEVM.Conveyance_Expense_HDR.ExpenceType = 2;

                DataTable DT_Get_Name = new DataTable();
                if (CBE.Bill_Status_ID == 2)
                {
                    DataTable DT_Tax = OS.GetServiceTaxRate();
                    decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
                    decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
                    decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());
                    Webx_Account_Details OBjWAD = new Webx_Account_Details();
                    OBjWAD.SRNo = 1;
                    OBjWAD.Vouchertype = "";
                    //PEVM.WAD.SRNo = 1;
                    //PEVM.WAD.ServiceTax = HdnServiceTaxRate;
                    PEVM.HdnEduCessRate = HdnEduCessRate;
                    PEVM.HdnHEduCessRate = HdnHEduCessRate;
                    PEVM.HdnServiceTaxRate = HdnServiceTaxRate;
                    PEVM.SubType = 3;
                    ViewBag.SubType = 3;
                    PEVM.Type2 = 2;

                    List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                    ListWAD1.Add(OBjWAD);
                    PEVM.WADListing = ListWAD1;
                }
                else
                {
                    PEVM.SubType = 2;
                    ViewBag.SubType = 2;
                }
                PEVM.ExpanceType = 2;
                ViewBag.Type = "Conveyance";
                ViewBag.Title = "Conveyance_Local_Expenses";
                PEVM.Conveyance_Expense_HDR.BillNo = Nos;
                return View("Conveyance_Local_Expenses", PEVM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Conveyance Expance Master

        public ActionResult ConveyanceExpanceMaster()
        {
            List<CYGNUS_Conveyance_Expance_Master> List_CCEM = new List<CYGNUS_Conveyance_Expance_Master>();
            List_CCEM = new List<CYGNUS_Conveyance_Expance_Master>();
            List_CCEM = BAS.Get_Conveyance_Expances(BaseLocationCode, BaseUserName);
            if (List_CCEM.Count() == 0)
            {
                CYGNUS_Conveyance_Expance_Master OBJ_List_CCEM = new CYGNUS_Conveyance_Expance_Master();
                OBJ_List_CCEM.ID = 1;
                List_CCEM.Add(OBJ_List_CCEM);
            }
            return View(List_CCEM);
        }

        public ActionResult _ConveyanceExpance_Partial(int id)
        {
            List<CYGNUS_Conveyance_Expance_Master> List_CCEM = new List<CYGNUS_Conveyance_Expance_Master>();
            try
            {
                CYGNUS_Conveyance_Expance_Master OBJ_List_CCEM = new CYGNUS_Conveyance_Expance_Master();
                OBJ_List_CCEM.ID = id;
                List_CCEM.Add(OBJ_List_CCEM);
                return PartialView("_ConveyanceExpance_Partial", OBJ_List_CCEM);

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Conveyance_Master_Submit(List<CYGNUS_Conveyance_Expance_Master> ListConveyance)
        {
            List<CYGNUS_Conveyance_Expance_Master> List_CCEM = new List<CYGNUS_Conveyance_Expance_Master>();
            try
            {
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(ListConveyance.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, ListConveyance);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DTADD = BAS.Insert_Conveyance_Master(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper());
                return View("Conveyance_Master_Done");

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Conveyance_Master_Done()
        {
            return View();
        }

        public string GetData(string Method, IDictionary<string, string> parameters)
        {
            DataSet DS = GF.getdatasetFromParams("USP_" + Method, parameters);
            if (DS.Tables.Count == 1)
            {
                return JsonConvert.SerializeObject(DS.Tables[0]);
            }
            else
            {
                return JsonConvert.SerializeObject(DS);
            }
        }

        #endregion

        #endregion

        #region Travelling Tour Expenses

        #region Travelling Expenses Open

        public ActionResult Travelling_Expenses_Open()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                CYGNUS_Branch_Travelling_Tour_HDR ObjTrave_Tour_Expense_HDR = new CYGNUS_Branch_Travelling_Tour_HDR();

                PEVM.Type2 = 3;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Title = "Travelling Expenses Open";
                ViewBag.ActionName = "Travelling_Expenses_Open_Submit";

                ObjTrave_Tour_Expense_HDR.ExpenceType = 3;
                ObjTrave_Tour_Expense_HDR.Date = System.DateTime.Now;
                ObjTrave_Tour_Expense_HDR.PeriodFrom = System.DateTime.Now.ToString("dd MMM yyyy");
                PEVM.Trave_Tour_Expense_HDR = ObjTrave_Tour_Expense_HDR;
                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Travelling_Expenses_Open_Submit(ExpensesEntryModule PEVM)
        {
            try
            {
                List<CYGNUS_Branch_Office_Expenses_Fixed_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Fixed_DET>();

                PEVM.Trave_Tour_Expense_HDR.BRCD = BaseLocationCode;
                PEVM.Trave_Tour_Expense_HDR.EntryBy = BaseUserName.ToUpper();
                PEVM.Trave_Tour_Expense_HDR.IsOpen = true;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Trave_Tour_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Trave_Tour_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Travelling_Expenses_Open(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("Travelling_Expanses_Open_Done", new { No = No, Type = "3" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Travelling_Expanses_Open_Done(string No, int Type)
        {
            ViewBag.No = No;
            ViewBag.Type = Type;
            return View();
        }

        #endregion

        #region Travelling Expense  Advance

        public ActionResult Travelling_Advance_Criteria()
        {
            Expenses_Advance_EntryModule VW = new Expenses_Advance_EntryModule();
            VW.FromDate = System.DateTime.Now;
            VW.ToDate = System.DateTime.Now;
            VW.Type = "3";
            return View(VW);
        }

        public ActionResult Travelling_Advance(Expenses_Advance_EntryModule VW)
        {
            VW.ListBR_Travel_Hdr = BAS.Get_Travelling_Tour_Advance_Details(VW.BRCD, VW.DocumentNo, VW.FromDate, VW.ToDate, VW.Staff);
            PaymentControl objpayment = new PaymentControl();
            VW.PC = objpayment;
            VW.BR_ADV = new CYGNUS_BR_Advance_Entry();
            return View("_Travelling_Expenses_Advance", VW.ListBR_Travel_Hdr);
        }

        public ActionResult Travelling_Expenses_Advanced(string BillNo)
        {
            Expenses_Advance_EntryModule PEEM = new Expenses_Advance_EntryModule();
            PEEM.BR_Travel_Hdr = BAS.GetTravelling_Expense_Advance_BillList(BillNo).Where(c => c.BillNo == BillNo).FirstOrDefault();
            PaymentControl objpayment = new PaymentControl();
            PEEM.PC = objpayment;
            return View(PEEM);
        }

        public ActionResult Travelling_Advance_Submit(Expenses_Advance_EntryModule VW, PaymentControl PaymentControl)
        {
            try
            {
                PaymentControl ObjPC = new PaymentControl();
                if (PaymentControl != null)
                {
                    ObjPC = PaymentControl;
                }
                else
                {
                    ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }

                #region Travelling Advance Entry

                string ADV_XML = "<root>";

                string Ledger = "", ChequeNo = "", ChequeDate = "", Name = "", HDR_Table = "", DET_Table = "";
                if (ObjPC.PaymentMode.ToUpper() == "CASH" || ObjPC.PaymentMode.ToUpper() == "BOTH")
                {
                    Ledger = "CAS0002";
                    ChequeNo = null; ChequeDate = "01 Jan 1990";
                    if (ObjPC.PaymentMode.ToUpper() == "CASH")
                    {
                        ObjPC.ChequeAmount = 0;
                        ObjPC.BankLedger = null;
                    }
                }
                if (ObjPC.PaymentMode.ToUpper() == "BANK" || ObjPC.PaymentMode.ToUpper() == "BOTH")
                {
                    Ledger = ObjPC.BankLedger;
                    ChequeNo = ObjPC.ChequeNo;
                    ChequeDate = ObjPC.ChequeDate.ToString("dd MMM yyyy");
                    if (ObjPC.PaymentMode.ToUpper() == "BANK")
                    {
                        ObjPC.CashAmount = 0;
                        ObjPC.CashLedger = null;
                    }
                }
                if (ObjPC.PaymentMode.ToUpper() == "")
                {
                    ViewBag.StrError = "Please Select Proper Pay Mode For Advance.";
                    return View("Error");
                }

                if (VW.BR_Travel_Hdr.ExpenceType == 3)
                {
                    HDR_Table = "CYGNUS_Branch_Travelling_Tour_HDR";
                    DET_Table = "CYGNUS_Branch_Travelling_Tour_DET";
                }

                Name = MS.GetUserDetails().Where(c => c.UserId == VW.BR_Travel_Hdr.EmpCode).First().Name;

                /*
                 Type = 1 - User
                 Type = 2 - Labour 
                 Type = 3 - Vendor 
                 Type = 4 - Driver 
                 */

                ADV_XML = ADV_XML + "<BR_Advance>";
                ADV_XML = ADV_XML + "<Type>1</Type>";
                ADV_XML = ADV_XML + "<BillNo>" + VW.BR_Travel_Hdr.BillNo + "</BillNo>";
                ADV_XML = ADV_XML + "<Name>" + Name + "</Name>";
                ADV_XML = ADV_XML + "<Vendor_Employee>" + VW.BR_Travel_Hdr.EmpCode + "</Vendor_Employee>";
                ADV_XML = ADV_XML + "<PaymentMode>" + ObjPC.PaymentMode + "</PaymentMode>";
                ADV_XML = ADV_XML + "<Advance_Paid_Code>" + VW.BR_Travel_Hdr.EmpCode + "</Advance_Paid_Code>";
                ADV_XML = ADV_XML + "<Advance_Paid_By>" + VW.BR_ADV.Advance_Paid_By + "</Advance_Paid_By>";
                ADV_XML = ADV_XML + "<PayAmount>" + ObjPC.PayAmount + "</PayAmount>";
                ADV_XML = ADV_XML + "<Selected_Ledger>" + Ledger + "</Selected_Ledger>";
                ADV_XML = ADV_XML + "<CashAmount>" + ObjPC.CashAmount + "</CashAmount>";
                ADV_XML = ADV_XML + "<CashLedger>" + ObjPC.CashLedger + "</CashLedger>";
                ADV_XML = ADV_XML + "<ChequeAmount>" + ObjPC.ChequeAmount + "</ChequeAmount>";
                ADV_XML = ADV_XML + "<BankLedger>" + ObjPC.BankLedger + "</BankLedger>";
                ADV_XML = ADV_XML + "<ChequeNo>" + ChequeNo + "</ChequeNo>";
                ADV_XML = ADV_XML + "<ChequeDate>" + ChequeDate + "</ChequeDate>";
                ADV_XML = ADV_XML + "<HDR_Table_Name>" + HDR_Table + "</HDR_Table_Name>";
                ADV_XML = ADV_XML + "<DET_Table_Name>" + DET_Table + "</DET_Table_Name>";
                ADV_XML = ADV_XML + "</BR_Advance>";

                ADV_XML = ADV_XML + "</root>";

                #endregion

                DataTable DT = BAS.BR_Advance(BaseLocationCode, BaseFinYear, ADV_XML, BaseCompanyCode, BaseYearValFirst, VW.BR_Travel_Hdr.ExpenceType.ToString(), VW.BR_Travel_Hdr.BillNo, BaseUserName);

                string VoucherNo = "", Message = "", Status = "";
                Message = DT.Rows[0]["Message"].ToString();
                Status = DT.Rows[0]["Status"].ToString();
                VoucherNo = DT.Rows[0]["VoucherNo"].ToString();
                if (Message.ToUpper() == "DONE" && Status == "1")
                {
                    return RedirectToAction("Travelling_Advance_Done", new { VoucherNo = VoucherNo, BillNo = VW.BR_Travel_Hdr.BillNo });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Travelling_Advance_Done(string VoucherNo, string BillNo)
        {
            try
            {
                ViewBag.VoucherNo = VoucherNo;
                ViewBag.BillNo = BillNo;
                return View();
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Travelling Expenses Close

        public ActionResult Travelling_Expenses_Close_Criteria()
        {
            try
            {
                return View();
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Travelling_Expenses_Close_Criteria_List(string BillNo, DateTime FromDate, DateTime ToDate, string Branch, string Staff)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            PEEM.ListCPEE_HDR = BAS.Get_Travelling_Expenses_Close_List(BillNo, FromDate.ToString("dd MMM yyyy"), ToDate.ToString("dd MMM yyyy"), Branch, Staff);
            PEEM.SubType = 1;
            return PartialView("_Travelling_Expenses_Closed", PEEM);
        }

        public ActionResult Travelling_Tour_Expenses(string Id)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                CYGNUS_Branch_Travelling_Tour_HDR ObjTrave_Tour_Expense_HDR = new CYGNUS_Branch_Travelling_Tour_HDR();
                CYGNUS_Branch_Travelling_Tour_DET ObjTrave_Tour_Expense_DET = new CYGNUS_Branch_Travelling_Tour_DET();

                #region Get Expance Data

                DataSet DS = BAS.GetBrancAcct_Aprroval_Reject(Id, BaseLocationCode, BaseUserName.ToUpper(), 3);

                #region Trave_Tour

                ObjTrave_Tour_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_HDR>(DS.Tables[0]).FirstOrDefault();

                PEVM.List_Fare_Trave_Tour_Expense_DET = new List<CYGNUS_Branch_Travelling_Tour_DET>();
                PEVM.List_Conveyance_Trave_Tour_Expense_DET = new List<CYGNUS_Branch_Travelling_Tour_DET>();
                PEVM.List_Bo_Lo_Trave_Tour_Expense_DET = new List<CYGNUS_Branch_Travelling_Tour_DET>();
                PEVM.List_Food_Trave_Tour_Expense_DET = new List<CYGNUS_Branch_Travelling_Tour_DET>();
                PEVM.List_Other_Trave_Tour_Expense_DET = new List<CYGNUS_Branch_Travelling_Tour_DET>();

                PEVM.Trave_Tour_Expense_DET = new CYGNUS_Branch_Travelling_Tour_DET();
                PEVM.Trave_Tour_Expense_DET.GrandTotal_SubTotal_Amount = 0;
                #endregion

                #endregion

                PEVM.List_BR_Adv = BAS.Get_BR_AdvanceList(ObjTrave_Tour_Expense_HDR.BillNo, "3", BaseLocationCode, BaseUserName);

                PEVM.Type2 = 3;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Title = "Travelling (Tour) Expenses";
                ViewBag.ActionName = "Travelling_Tour_Submit";

                ObjTrave_Tour_Expense_HDR.ExpenceType = 3;
                //ObjTrave_Tour_Expense_HDR.Date = System.DateTime.Now;
                PEVM.Trave_Tour_Expense_HDR = ObjTrave_Tour_Expense_HDR;
                PEVM.Trave_Tour_Expense_DET = ObjTrave_Tour_Expense_DET;
                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Travelling Tour Expenses Entry

        private string SaveImage(HttpPostedFileBase File, int Id, string FolderName)
        {
            string extension = "", name = "", path = "";
            string IdPath = Server.MapPath("~/Images/" + FolderName + "/" + GF.GenerateString(8));
            if (!Directory.Exists(IdPath))
                Directory.CreateDirectory(IdPath);

            string FileName = File.FileName;
            extension = System.IO.Path.GetExtension(File.FileName);
            name = GF.GenerateString(10) + extension;
            path = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);
            string path1 = string.Format("{0}/{1}", Server.MapPath("~/Images/" + FolderName + "/"), name);

            if (System.IO.File.Exists(path1))
                System.IO.File.Delete(path1);

            File.SaveAs(path1);

            return name;
        }

        public ActionResult _Add_Travelling_Tour_Details(int id, string Type)
        {
            try
            {
                CYGNUS_Branch_Travelling_Tour_DET ObjCBTT = new CYGNUS_Branch_Travelling_Tour_DET();
                ObjCBTT.ID = id;

                if (Type == "FARE")
                {
                    ObjCBTT.Date = System.DateTime.Now;
                    return PartialView("_Fare_Trave_Tour_Expense_Partial", ObjCBTT);
                }
                else if (Type == "CONVEYANCE")
                {
                    ObjCBTT.Date = System.DateTime.Now;
                    return PartialView("_Conveyance_Trave_Tour_Expense_Partial", ObjCBTT);
                }
                else if (Type == "BO_LO")
                {
                    ObjCBTT.DateOfCheckIn = System.DateTime.Now;
                    ObjCBTT.DateOfCheckOut = System.DateTime.Now;
                    return PartialView("_Bo_Lo_Trave_Tour_Expense_Partial", ObjCBTT);
                }
                else if (Type == "FOOD")
                {
                    ObjCBTT.Date = System.DateTime.Now;
                    return PartialView("_Food_Trave_Tour_Expense_Partial", ObjCBTT);
                }
                else if (Type == "OTHER")
                {
                    ObjCBTT.Date = System.DateTime.Now;
                    return PartialView("_Other_Trave_Tour_Expense_Partial", ObjCBTT);
                }
                else
                    return PartialView("_Fare_Trave_Tour_Expense_Partial", ObjCBTT);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Travelling_Tour_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET, List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET,
             List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET)
        {
            try
            {
                List<CYGNUS_Branch_Office_Expenses_Fixed_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Fixed_DET>();

                PEVM.Trave_Tour_Expense_HDR.BRCD = BaseLocationCode;
                PEVM.Trave_Tour_Expense_HDR.EntryBy = BaseUserName.ToUpper();
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Trave_Tour_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Trave_Tour_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Travelling_DET_XML(FAREDET, ConveyanceDET, Bo_Lo_DET, FoodDET, OtherDET, 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Travelling_Tour_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "3" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public string Travelling_DET_XML(List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET, List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET,
            List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET, int Type)
        {
            string XML_Det = "";
            XML_Det = XML_Det + "<root>";

            #region Fare

            if (FAREDET != null)
            {
                foreach (var item in FAREDET)
                {
                    string FareImageName = "";
                    #region FARE Image Upload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["fare_" + item.ID];
                            if (File.ContentLength > 0)
                            {
                                FareImageName = SaveImage(File, item.ID, "Travelling_Tour");
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    #endregion

                    XML_Det = XML_Det + "<Fare>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Date>" + item.Date.ToString("dd MMM yyyy") + "</Date>";
                        XML_Det = XML_Det + "<LocationFrom>" + item.LocationFrom + "</LocationFrom>";
                        XML_Det = XML_Det + "<LocationTo>" + item.LocationTo + "</LocationTo>";
                        XML_Det = XML_Det + "<TravellingMode>" + item.TravellingMode + "</TravellingMode>";
                        XML_Det = XML_Det + "<Class>" + item.Class + "</Class>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Image>" + FareImageName + "</Image>";
                        XML_Det = XML_Det + "<SubExpense_Type>" + item.SubExpense_Type + "</SubExpense_Type>";
                    }
                    if (Type == 2 || Type == 3)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</Fare>";
                }
            }

            #endregion

            #region Conveyance

            if (ConveyanceDET != null)
            {
                foreach (var item in ConveyanceDET)
                {
                    string ConveyanceImage = "";
                    #region Conveyance Image Upload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["conveyancefiles_" + item.ID];
                            if (File.ContentLength > 0)
                            {
                                ConveyanceImage = SaveImage(File, item.ID, "Travelling_Tour"); ;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    #endregion

                    XML_Det = XML_Det + "<Conveyance>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Date>" + item.Date.ToString("dd MMM yyyy") + "</Date>";
                        XML_Det = XML_Det + "<LocationFrom>" + item.LocationFrom + "</LocationFrom>";
                        XML_Det = XML_Det + "<LocationTo>" + item.LocationTo + "</LocationTo>";
                        XML_Det = XML_Det + "<TravellingMode>" + item.TravellingMode + "</TravellingMode>";
                        XML_Det = XML_Det + "<Distance>" + item.Distance + "</Distance>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Image>" + ConveyanceImage + "</Image>";
                        XML_Det = XML_Det + "<SubExpense_Type>" + item.SubExpense_Type + "</SubExpense_Type>";
                    }
                    if (Type == 2 || Type == 3)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</Conveyance>";
                }
            }

            #endregion

            #region Boarding Loading

            if (Bo_Lo_DET != null)
            {
                foreach (var item in Bo_Lo_DET)
                {
                    string Bo_LO_Image = "";
                    #region Conveyance Image Upload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["Bo_Lo_" + item.ID];
                            if (File.ContentLength > 0)
                            {
                                Bo_LO_Image = SaveImage(File, item.ID, "Travelling_Tour");
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    #endregion

                    XML_Det = XML_Det + "<Boarding_Loading>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Name_Of_City>" + item.Name_Of_City + "</Name_Of_City>";
                        XML_Det = XML_Det + "<Grade_Of_City>" + item.Grade_Of_City + "</Grade_Of_City>";
                        XML_Det = XML_Det + "<Name_Of_Hotel>" + item.Name_Of_Hotel + "</Name_Of_Hotel>";
                        XML_Det = XML_Det + "<DateOfCheckIn>" + item.DateOfCheckIn.ToString("dd MMM yyyy h:mm:ss") + "</DateOfCheckIn>";
                        XML_Det = XML_Det + "<DateOfCheckOut>" + item.DateOfCheckOut.ToString("dd MMM yyyy h:mm:ss") + "</DateOfCheckOut>";
                        XML_Det = XML_Det + "<DaysOfStay>" + item.DaysOfStay + "</DaysOfStay>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Image>" + Bo_LO_Image + "</Image>";
                        XML_Det = XML_Det + "<SubExpense_Type>" + item.SubExpense_Type + "</SubExpense_Type>";
                    }
                    if (Type == 2 || Type == 3)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</Boarding_Loading>";
                }
            }

            #endregion

            #region Food

            if (FoodDET != null)
            {
                foreach (var item in FoodDET)
                {
                    string FoodImage = "";
                    #region Food Image Upload
                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["food_" + item.ID];
                            if (File.ContentLength > 0)
                            {
                                FoodImage = SaveImage(File, item.ID, "Travelling_Tour");
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }
                    #endregion

                    XML_Det = XML_Det + "<Food>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Date>" + item.Date.ToString("dd MMM yyyy") + "</Date>";
                        XML_Det = XML_Det + "<Particular>" + item.Particular + "</Particular>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Image>" + FoodImage + "</Image>";
                        XML_Det = XML_Det + "<SubExpense_Type>" + item.SubExpense_Type + "</SubExpense_Type>";
                    }
                    if (Type == 2 || Type == 3)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</Food>";
                }
            }

            #endregion

            #region Other

            if (OtherDET != null)
            {
                foreach (var item in OtherDET)
                {
                    string OtherImage = "";
                    #region Other Image Upload

                    try
                    {
                        if (Request.Files != null)
                        {
                            var File = Request.Files["other_" + item.ID];
                            if (File.ContentLength > 0)
                            {
                                OtherImage = SaveImage(File, item.ID, "Travelling_Tour"); ;
                            }
                        }
                    }
                    catch (Exception)
                    {
                    }

                    #endregion

                    XML_Det = XML_Det + "<Other>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Date>" + item.Date.ToString("dd MMM yyyy") + "</Date>";
                        XML_Det = XML_Det + "<Particular>" + item.Particular + "</Particular>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Image>" + OtherImage + "</Image>";
                        XML_Det = XML_Det + "<SubExpense_Type>" + item.SubExpense_Type + "</SubExpense_Type>";
                    }
                    if (Type == 2 || Type == 3)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</Other>";
                }
            }

            #endregion

            XML_Det = XML_Det + "</root>";

            return XML_Det;
        }

        public ActionResult Travel_Accounting_Advance_Payment_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET, List<Webx_Account_Details> PAYMENTMODE, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET,
        List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET, List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET,
       List<CYGNUS_BR_Advance_Entry> BR_Adv)
        {
            string BillNo = "", IDS = "";

            try
            {
                #region Get IDs

                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        if (IDS == "")
                            IDS = item.ID.ToString();
                        else
                            IDS = IDS + "," + item.ID;
                    }
                }
                else
                {
                    #region Travelling Expances

                    if (FAREDET != null)
                    {
                        foreach (var item in FAREDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (ConveyanceDET != null)
                    {
                        foreach (var item in ConveyanceDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (Bo_Lo_DET != null)
                    {
                        foreach (var item in Bo_Lo_DET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;



                        }
                    }
                    if (FoodDET != null)
                    {
                        foreach (var item in FoodDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (OtherDET != null)
                    {
                        foreach (var item in OtherDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }

                    #endregion
                }

                #endregion

                string Year = BaseFinYear.ToString();
                string fin_year = "", PBOV_TYP = "";
                string Financial_Year = BaseYearVal;
                fin_year = Financial_Year;
                string EntryBy = BaseUserName.ToUpper();
                string Transtype = "";
                string Opertitile = "", Voucherno = "";

                Webx_Account_Details ObjWAD = new Webx_Account_Details();

                #region Validations

                if (PAYMENTMODE == null)
                {
                    @ViewBag.StrError = "This Transaction was Some Issue. Please Check.";
                    return View("Error");
                }

                if (PAYMENTMODE != null)
                    ObjWAD = PAYMENTMODE.First();

                if (ObjWAD.PaymentMode.ToUpper() == "BANK" || ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    DataTable DT = OS.Duplicate_ChqNO(ObjWAD.ChequeNo, ObjWAD.Chequedate.ToString("dd MMM yyyy"));
                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }
                if (ObjWAD.PaymentMode.ToUpper() == "CASH" || ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    string FIN_Start = "", Curr_Year = "", Finyear = "";
                    fin_year = BaseFinYear.ToString();
                    Curr_Year = DateTime.Now.ToString("yyyy");
                    Finyear = BaseFinYear.ToString();
                    if (Finyear == Curr_Year)
                        FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
                    else
                        FIN_Start = "01 Apr " + Financial_Year;

                    DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, GF.FormateDate(System.DateTime.Now), GF.FormateDate(Convert.ToDateTime(FinYearStartDate)), BaseYearVal, ObjWAD.NETAMOUNT.ToString());

                    //string Cnt = DT.Rows[0][0].ToString();
                    //if (Cnt == "F")
                    //{
                    //    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    //    return View("Error");
                    //}
                    //if (Cnt != "T")
                    //{
                    //    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    //    return View("Error");
                    //}
                }

                Opertitile = "Branch Accouting"/*ObjCBAM.ChargeName*/;
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH PAYMENT";
                }

                #endregion

                #region Get PBOV_code and PBOV_Name

                string PBOV_code = "", PBOV_Name = "";


                if (PEVM.Type2 == 3)
                {
                    PBOV_code = PEVM.Trave_Tour_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Trave_Tour_Expense_HDR.EmpName;
                }
                else
                {
                    ViewBag.StrError = "Please select Proper Employee.";
                    return View("Error");
                }

                #endregion

                string svrcheck = "N", tdscheck = "N", DepoFlag = "N", Str_Onaccount = "N";
                string BankCode = "";
                double adjustamt = 0;
                string TdsCalForLedgerAmt = "N";

                //if (PEVM.WAD.IsServicetaxapply)
                //{
                //    svrcheck = "Y";
                //}
                //if (PEVM.WAD.IsTDStaxapply)
                //{
                //    tdscheck = "Y";
                //}
                //if (PEVM.WAD.IsTDSWithoutSvcTax)
                //{
                //    TdsCalForLedgerAmt = "Y";
                //}

                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    DepoFlag = "Y";
                    BankCode = ObjWAD.DepositedInBank;
                    adjustamt = Convert.ToDouble(ObjWAD.ChequeAmount);
                    if (ObjWAD.IsonAccount)
                    {
                        Str_Onaccount = "Y";
                        adjustamt = 0;
                    }
                }

                string CompanyCode = "", Acccode = "", Accdesc = "", Debit = "";
                string Credit = "", Narration = "", Description = "", Stax_App = "N";

                string CostCenterId = "", CostCenter = "";

                string hdnroundoff = "N";
                string hdneditablsvctaxrate = "N";
                string HdnCompanyDropDownRule = "N";
                string HdnRowwiseSvcTaxRule = "N";
                string HdnTransactionWiseAccountRule = "N";
                string HdnCostCenterRule = "N";

                hdnroundoff = OS.ManualVouchersRule("RoundOff");
                hdneditablsvctaxrate = OS.ManualVouchersRule("EditableServiceTax");
                HdnCompanyDropDownRule = OS.ManualVouchersRule("CompanyDropDown");
                HdnRowwiseSvcTaxRule = OS.ManualVouchersRule("RowwiseSvcTax");
                HdnTransactionWiseAccountRule = OS.ManualVouchersRule("Transwiserule");
                HdnCostCenterRule = OS.ManualVouchersRule("CostCenterRule");

                string Xml_Acccode_Details = "<root>";

                if (CPEEDET != null || PEVM.Type2 == 3)
                {
                    CYGNUS_Branch_Accounting_Master ObjCBSM = new CYGNUS_Branch_Accounting_Master();

                    #region GET BillNo And Expance Account Code

                    if (PEVM.Type2 == 3)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Trave_Tour_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Trave_Tour_Expense_HDR.BillNo;
                    }
                    else
                    {
                        ViewBag.StrError = "Proper Bill not available Please select Proper Bill.";
                        return View("Error");
                    }
                    CompanyCode = ObjCBSM.Accode;
                    string ChargeName = ObjCBSM.ChargeName;

                    #endregion

                    if (HdnCostCenterRule == "Y")
                    {
                    }

                    Credit = "0.00";
                    Debit = ObjWAD.AmountApplicable.ToString();

                    Narration = "BEING AMOUNT PAID IN " + ObjWAD.PaymentMode.ToUpper() + " AGAINST " + ChargeName.ToUpper() + " FOR BR NO." + BillNo.ToUpper();
                    Description = "";

                    bool chkstax = false; ;

                    if (HdnRowwiseSvcTaxRule == "Y")
                    {
                        if (chkstax)
                            Stax_App = "Y";
                        else
                            Stax_App = "N";
                    }

                    if (CompanyCode != "")
                    {
                        DataTable dt = OS.FindOutSystemAccountCode(CompanyCode, BaseLocationCode);
                        if (dt.Rows.Count == 0)
                        {
                            ViewBag.StrError = "Error : Not Valid Account Code : " + BaseCompanyCode + ". So Please Enter The Valid Account Details";
                            return View("Error");
                        }
                        Acccode = dt.Rows[0]["Acccode"].ToString().Trim();
                        Accdesc = dt.Rows[0]["Accdesc"].ToString().Trim();
                        Accdesc = Accdesc.Replace("<", "&lt;");
                        Accdesc = Accdesc.Replace(">", "&gt;");
                        Accdesc = Accdesc.Replace("\"", "&quot;");
                        Accdesc = Accdesc.Replace("'", "&apos;");

                        if (Str_Onaccount == "Y" && Acccode != "CDA0001")
                        {
                            ViewBag.StrError = "Error : On Account Cheque Facility is not Availble for : " + Accdesc + ". This Facility Available only for ledger : Billed Debtors";
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Acccode + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Stax_App>" + Stax_App + "</Stax_App>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenterId>" + CostCenterId + "</CostCenterId>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenter>" + BillNo + "</CostCenter>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                    else
                    {
                        ViewBag.StrError = "AproPriate Account not Found..";
                        return View("Error");
                    }
                }
                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                string Company_code = "";
                Company_code = BaseCompanyCode;
                string CommonCostCenter = "N";
                string BranchCode = "";

                BranchCode = BaseLocationCode;

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + System.DateTime.Now.ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BranchCode + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.ToUpper() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString() + "</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ObjWAD.ManualNo + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ObjWAD.BusinessDivision + "</BusinessType>";
                Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
                Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + BaseLocationCode/*PEVM.WAD.AccountingLocation*/ + "</Acclocation>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ObjWAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ObjWAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + Narration/*PEVM.WAD.Narration*/ + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<Panno>" + ObjWAD.PANNumber + "</Panno>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ObjWAD.ServiceTaxRegNo + "</Svrtaxno>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ObjWAD.ChequeNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqamt>" + ObjWAD.ChequeAmount + "</chqamt>";
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Xml_Other_Details = Xml_Other_Details + "<chqdt>" + "01 Jan 1990" + "</chqdt>";
                }
                else
                {
                    Xml_Other_Details = Xml_Other_Details + "<chqdt>" + Convert.ToDateTime(ObjWAD.Chequedate).ToString("dd MMM yyyy") + "</chqdt>";
                }
                Xml_Other_Details = Xml_Other_Details + "<CashAmt>" + ObjWAD.CashAmount + "</CashAmt>";
                Xml_Other_Details = Xml_Other_Details + "<CashAcccode>" + ObjWAD.CashAccount + "</CashAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<BankAcccode>" + ObjWAD.DepositedInBank + "</BankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + PBOV_TYP + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + svrcheck + "</Svrtax_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + tdscheck + "</Tds_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + ObjWAD.TDSSection + "</Tds_Acccode>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + ObjWAD.TDSRate + "</Tds_rate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsAmount>" + ObjWAD.TDSAmount + "</TdsAmount>";
                Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ObjWAD.ReceivedFromBank + "</recbanknm>";
                Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + Str_Onaccount + "</Onaccount_YN>";
                Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag + "</Deposited>";
                Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ObjWAD.PaymentMode + "</Paymode>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ObjWAD.ServiceTaxRate /*HdnServiceTaxRate*/ + "</SvrTaxRate>";
                Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ObjWAD.EducationCess /*HdnEduCessRate*/ + "</EduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ObjWAD.HEducationCess /*HdnHEduCessRate*/ + "</HEduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsOption>" + TdsCalForLedgerAmt + "</TdsOption>";
                Xml_Other_Details = Xml_Other_Details + "<CommonCostCenter>" + CommonCostCenter + "</CommonCostCenter>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + Company_code + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "<AmountPaidToEmployee>" + PEVM.Trave_Tour_Expense_HDR.AmountPaidToEmployee + "</AmountPaidToEmployee>";
                Xml_Other_Details = Xml_Other_Details + "<AmountPaidFromEmployee>" + PEVM.Trave_Tour_Expense_HDR.AmountPaidFromEmployee + "</AmountPaidFromEmployee>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                #region Advacne XML

                string Xml_ADV = "<root><Advance>";

                if (BR_Adv != null)
                {
                    foreach (var item in BR_Adv)
                    {
                        Xml_ADV = Xml_ADV + "<Advance_Amount>" + item.Advance_Amount + "</Advance_Amount>";
                        Xml_ADV = Xml_ADV + "<DocumentNo>" + item.DocumentNo + "</DocumentNo>";
                        Xml_ADV = Xml_ADV + "<VoucherNo>" + item.VoucherNo + "</VoucherNo>";
                        Xml_ADV = Xml_ADV + "<Advance_Paid_Code>" + item.Advance_Paid_Code + "</Advance_Paid_Code>";
                        Xml_ADV = Xml_ADV + "<Advance_Paid_Name>" + item.Advance_Paid_Name + "</Advance_Paid_Name>";
                        Xml_ADV = Xml_ADV + "<Against_Ledger>" + item.Against_Ledger + "</Against_Ledger>";
                        Xml_ADV = Xml_ADV + "<Selected_Ledger>" + item.Selected_Ledger + "</Selected_Ledger>";
                        Xml_ADV = Xml_ADV + "<Reverse_Accode>" + item.Reverse_Accode + "</Reverse_Accode>";
                    }
                }
                Xml_ADV = Xml_ADV + "</Advance></root>";

                #endregion

                try
                {
                    Voucherno = BAS.TravellingTour_Exp_payments(Xml_Acccode_Details, Xml_Other_Details, Xml_ADV);
                    //Voucherno = BAS.Insert_Branch_Voucher_Details(Xml_Acccode_Details, Xml_Other_Details, IDS, PEVM.Type2, Xml_ADV);
                    return RedirectToAction("Accounting_Payment_Done", new { Type = PEVM.Type2, No = Voucherno, BillNo = BillNo });
                }
                catch (Exception ex)
                {
                    @ViewBag.StrError = ex.Message;
                    return View("Error");
                }
                //return RedirectToAction("Accounting_Payment_Done", new { Type = PEVM.Type2, No = Voucherno, BillNo = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }
        #region Travelling Tour Expenses Rejection

        public ActionResult Travelling_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET, List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET,
            List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET)
        {
            try
            {
                string XML = Travelling_DET_XML(FAREDET, ConveyanceDET, Bo_Lo_DET, FoodDET, OtherDET, 2);

                DataTable DT = BAS.Travelling_Tour_Expence_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Trave_Tour_Expense_HDR.BillNo, PEVM.Trave_Tour_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 3, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion


        #endregion

        #region Travelling Tour Expenses Approval

        public ActionResult Travelling_Tour_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET, List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET,
            List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET)
        {
            try
            {
                List<CYGNUS_Branch_Travelling_Tour_DET> ListCPEEDet = new List<CYGNUS_Branch_Travelling_Tour_DET>();

                PEVM.Trave_Tour_Expense_HDR.BRCD = BaseLocationCode;
                PEVM.Trave_Tour_Expense_HDR.EntryBy = BaseUserName.ToUpper();
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Trave_Tour_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Trave_Tour_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Travelling_DET_XML(FAREDET, ConveyanceDET, Bo_Lo_DET, FoodDET, OtherDET, 2);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Travelling_Tour_Expence_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Trave_Tour_Expense_HDR.BillNo, PEVM.Trave_Tour_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "3" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #endregion

        #endregion

        #region Driver Incentive TSP Expenses Submit

        public ActionResult DriverIncentiveTSPCharge(int Type, string No)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = Type;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Title = "Driver Incentive ";
                ViewBag.ActionName = "DriverIncentiveTSPExpensesSubmit";
                PEVM.Driver_TSP_Expense_HDR = new CYGNUS_DriverIncentiveTSP_Expense_HDR();
                PEVM.Driver_TSP_Expense_HDR.DocumentNo = No;
                //PEVM.Driver_TSP_Expense_HDR.BR_Date = System.DateTime.Now;

                PEVM.List_Driver_TSP_Expense_DET = BAS.GetBRCD_DriverIncentiveTSP_Detail(Type, No, BaseLocationCode);

                if (PEVM.List_Driver_TSP_Expense_DET.Count() == 0)
                {
                    ViewBag.StrError = "Please Enter Valid THC No";
                    return View("Error");
                }

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Driver Incentive TSP Expenses Entry

        public ActionResult DriverIncentiveTSPExpensesSubmit(ExpensesEntryModule PEVM, List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET)
        {
            try
            {
                PEVM.Driver_TSP_Expense_HDR.Brcd = BaseLocationCode;
                PEVM.Driver_TSP_Expense_HDR.ExpenceType = PEVM.Type2;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Driver_TSP_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Driver_TSP_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = DriverIncentiveTSP_DET_XML(CPEEDET.Where(c => c.ID == PEVM.ID).ToList(), 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_DriverIncentiveTSP_Unloading_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "4" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Driver Incentive TSP Expenses Branch Approval

        public ActionResult DriverIncetive_Approval()
        {
            try
            {
                BranchAccountingMaster BMVW = new BranchAccountingMaster();
                BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode);
                BMVW.SubType = 2;
                BMVW.ExpenceType = 4;
                BMVW.FromDate = System.DateTime.Now;
                BMVW.ToDate = System.DateTime.Now;
                @ViewBag.ActionName = "AccotingApproval_Reject";
                return View(BMVW);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult DriverIncetiveApproval(DateTime FromDate, DateTime ToDate, string Location, string ExpenceType)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            string ToLocation = BaseLocationCode;
            if (Location == "HQTR")
            {
                ToLocation = Location;
            }

            PEEM.ListCPEE_HDR = BAS.Get_DriverIncetive_List(FromDate.ToString("dd MMM yyyy"), ToDate.ToString("dd MMM yyyy"), BaseUserName.ToUpper(), ExpenceType.ToString(), ToLocation, BaseLocationCode);
            return PartialView("_DriverIncetiveApproval", PEEM);
        }

        public ActionResult DriverIncentiveTSP_BranchApproval(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Expenses_EntryHDR> CPEEApprov)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();

                PEVM.Type2 = BMVW.ExpenceType;
                PEVM.SubType = 4;
                ViewBag.Title = "Head Office Accounting Approval/Reject Module";
                ViewBag.SubType = 4;
                ViewBag.ActionName = "Accounting_Approval";

                PEVM.WAD = new Webx_Account_Details();
                string Nos = BMVW.Id.ToString();

                DataSet DS = BAS.GetBrancAcct_Aprroval_Reject(Nos, BaseLocationCode, BaseUserName.ToUpper(), BMVW.ExpenceType);
                PEVM.List_Driver_TSP_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_DET>(DS.Tables[1]).Where(c => c.MTo == BaseLocationCode).ToList();
                PEVM.Driver_TSP_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_HDR>(DS.Tables[0]).FirstOrDefault();

                return View("DriverIncentiveTSPCharge", PEVM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult DriverIncentiveTSP_Approval_At_Destination_Branch(ExpensesEntryModule PEVM, List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_DriverIncentiveTSP_Expense_DET> ListCPEEDet = new List<CYGNUS_DriverIncentiveTSP_Expense_DET>();
                PEVM.Driver_TSP_Expense_HDR.Brcd = BaseLocationCode;

                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 4);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.AmountPaid_Per_Branch));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR DRIVER INCENTIVE FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Driver_TSP_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Driver_TSP_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = DriverIncentiveTSP_DET_XML(CPEEDET, 3);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_DriverIncentiveTSP_Branch_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Driver_TSP_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "4" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Driver Incentive TSP Expenses Branch Reject

        public ActionResult DriverIncentiveTSP_BranchApproval_BranchRejection(ExpensesEntryModule PEVM, List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_DriverIncentiveTSP_Expense_DET> ListCPEEDet = new List<CYGNUS_DriverIncentiveTSP_Expense_DET>();
                PEVM.Driver_TSP_Expense_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Driver_TSP_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Driver_TSP_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = DriverIncentiveTSP_DET_XML(CPEEDET, 3);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_DriverIncentiveTSP_Branch_Reject(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Driver_TSP_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "4" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Driver Incentive TSP Expenses  Branch Approval Screen Submit

        public ActionResult DriverIncentiveTSPCharge_Approval(ExpensesEntryModule PEVM, List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_DriverIncentiveTSP_Expense_DET> ListCPEEDet = new List<CYGNUS_DriverIncentiveTSP_Expense_DET>();
                PEVM.Driver_TSP_Expense_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Driver_TSP_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Driver_TSP_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = DriverIncentiveTSP_DET_XML(CPEEDET, 2);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_DriverIncentiveTSP_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Driver_TSP_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "4" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Driver Incentive TSP Expenses Branch Reject Screen Submit

        [HttpPost]
        public ActionResult DriverIncentiveTSPCharge_Reject(ExpensesEntryModule PEVM, List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET)
        {
            try
            {
                string XML = "";
                if (CPEEDET != null)
                {
                    XML = DriverIncentiveTSP_DET_XML(CPEEDET, 2);
                }
                else
                {
                    @ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }

                DataTable DT = BAS.USP_DriverIncentiveTSP_Branch_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Driver_TSP_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 1, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string DriverIncentiveTSP_DET_XML(List<CYGNUS_DriverIncentiveTSP_Expense_DET> CPEEDET, int Type)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    //if (item.CheckBox)
                    //{
                    XML_Det = XML_Det + "<CYGNUS_DriverIncentiveTSP_Expense_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<ManifestNumber>" + item.ManifestNumber + "</ManifestNumber>";
                        XML_Det = XML_Det + "<DocketNo>" + item.DocketNo + "</DocketNo>";
                        XML_Det = XML_Det + "<MFrom>" + item.MFrom + "</MFrom>";
                        XML_Det = XML_Det + "<MTo>" + item.MTo + "</MTo>";
                        XML_Det = XML_Det + "<Distance>" + item.Distance + "</Distance>";
                        XML_Det = XML_Det + "<Weight>" + item.Weight + "</Weight>";
                        XML_Det = XML_Det + "<GoogleDistance>" + item.GoogleDistance + "</GoogleDistance>";
                        XML_Det = XML_Det + "<Hours_AsPer_Erp>" + item.Hours_AsPer_Erp + "</Hours_AsPer_Erp>";
                        XML_Det = XML_Det + "<GoogleHours>" + item.GoogleHours + "</GoogleHours>";
                        XML_Det = XML_Det + "<Name_Of_Driver>" + item.Name_Of_Driver + "</Name_Of_Driver>";
                        XML_Det = XML_Det + "<Driver_Contract_Number>" + item.Driver_Contract_Number + "</Driver_Contract_Number>";
                        XML_Det = XML_Det + "<One_ST_Prize>" + item.One_ST_Prize + "</One_ST_Prize>";
                        XML_Det = XML_Det + "<One_ST_Prize_Amount_Hours>" + item.One_ST_Prize_Amount_Hours + "</One_ST_Prize_Amount_Hours>";
                        XML_Det = XML_Det + "<Second_ST_Prize>" + item.Second_ST_Prize + "</Second_ST_Prize>";
                        XML_Det = XML_Det + "<Second_ST_Prize_Amount_Hours>" + item.Second_ST_Prize_Amount_Hours + "</Second_ST_Prize_Amount_Hours>";
                        XML_Det = XML_Det + "<Third_ST_Prize>" + item.Third_ST_Prize + "</Third_ST_Prize>";
                        XML_Det = XML_Det + "<Third_ST_Prize_Amount_Hours>" + item.Third_ST_Prize_Amount_Hours + "</Third_ST_Prize_Amount_Hours>";
                        XML_Det = XML_Det + "<Driver_Id>" + item.Driver_Id + "</Driver_Id>";
                    }
                    if (Type == 3)
                    {
                        XML_Det = XML_Det + "<DestinationApproveBY>" + BaseUserName.ToUpper() + "</DestinationApproveBY>";
                        XML_Det = XML_Det + "<Hours_Per_Dest_BRCD>" + item.Hours_Per_Dest_BRCD + "</Hours_Per_Dest_BRCD>";
                        XML_Det = XML_Det + "<AmountPaid_Per_Branch>" + item.AmountPaid_Per_Branch + "</AmountPaid_Per_Branch>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_DriverIncentiveTSP_Expense_DET>";
                    // }
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #endregion

        #region Porter Expenses

        public ActionResult PorterExpenses(int Type, string No)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = Type;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                int counts = 0;

                CYGNUS_Porter_Expense ObjCPE = new CYGNUS_Porter_Expense();

                try
                {
                    DataTable DT = BAS.Check_ValidNo(Type, No, BaseLocationCode);
                    counts = Convert.ToInt32(DT.Rows[0][0].ToString());
                }
                catch (Exception ex)
                {
                    @ViewBag.StrError = ex.Message;
                    return View("Error");
                }
                if (counts == 0)
                {
                    @ViewBag.StrError = "This THC Is Not Valid.";
                    return View("Error");
                }

                ViewBag.Title = "Porter Expense Entry";
                ViewBag.ActionName = "PorterExpensesSubmit";
                string BRCD = BaseLocationCode;

                PEVM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();
                PEVM.CPEE_HDR.DocumentNo = No;
                PEVM.CPEE_HDR.ExpenceType = Type;
                PEVM.CPEE_HDR.BR_Date = System.DateTime.Now;
                PEVM.CPEE_HDR.BR_Date = System.DateTime.Now;
                PEVM.ListCPEE_DET = BAS.GetBRCD_ACCT_HDR_Detail(PEVM.Type2, No, BaseLocationCode);

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Porter Expenses Entry

        [HttpPost]
        public ActionResult PorterExpensesSubmit(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET, IEnumerable<HttpPostedFileBase> file)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 5);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.TotalClaimMade_ExlHandling));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR PORTER EXPENSES FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN PORTER EXPENSES ";
                    return View("Error");
                }

                List<CYGNUS_Branch_Expenses_EntryDET> ListCPEEDet = new List<CYGNUS_Branch_Expenses_EntryDET>();

                PEVM.CPEE_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.CPEE_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.CPEE_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = PorterExpenses_DET_XML(CPEEDET.Where(c => c.CheckBox == true).ToList(), 1, file);

                DataTable DT = BAS.InsertPortelExpence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), BaseYearValFirst);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = 5 });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Porter Expenses Approval

        public ActionResult Porter_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Branch_Expenses_EntryDET> ListCPEEDet = new List<CYGNUS_Branch_Expenses_EntryDET>();

                PEVM.CPEE_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.CPEE_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.CPEE_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = PorterExpenses_DET_XML(CPEEDET, 2,null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_PortelExpence_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.CPEE_HDR.BillNo, PEVM.CPEE_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = 5 });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        #endregion

        #region Porter Expenses Rejection

        [HttpPost]
        public ActionResult Porter_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET)
        {
            try
            {
                string XML = "";
                if (CPEEDET != null)
                {
                    XML = PorterExpenses_DET_XML(CPEEDET, 2,null);
                }
                else
                {
                    @ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }

                DataTable DT = BAS.USP_PortelExpence_Branch_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.CPEE_HDR.BillNo, PEVM.CPEE_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 5, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string PorterExpenses_DET_XML(List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET, int Type, IEnumerable<HttpPostedFileBase> files)
        {
            string XML_Det = "<root>";
            //int id = 0;
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Branch_Expenses_EntryDET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";

                    if (Type == 1)
                    {
                        if (item.CheckBox)
                        {
                            string ImageName = "";
                            #region Porter Image Upload

                            try
                            {
                                if (Request.Files != null)
                                {
                                    var File = Request.Files["PorterExpense_" + item.ID];
                                    if (File.ContentLength > 0)
                                    {
                                        ImageName = SaveImage(File, item.ID, "PorterExpense");
                                    }
                                }
                            }
                            catch (Exception)
                            {
                            }

                            #endregion

                            XML_Det = XML_Det + "<ManifestNumber>" + item.ManifestNumber + "</ManifestNumber>";
                            XML_Det = XML_Det + "<DocketNo>" + item.DocketNo + "</DocketNo>";
                            XML_Det = XML_Det + "<ActualWeight>" + item.ActualWeight + "</ActualWeight>";
                            XML_Det = XML_Det + "<NoOfPKGS>" + item.NoOfPKGS + "</NoOfPKGS>";
                            XML_Det = XML_Det + "<PorterType>" + item.PorterType + "</PorterType>";
                            XML_Det = XML_Det + "<ExpensIncurredDate>" + item.ExpensIncurredDate + "</ExpensIncurredDate>";
                            XML_Det = XML_Det + "<PerKGS>" + item.PerKGS + "</PerKGS>";
                            XML_Det = XML_Det + "<PerPackage>" + item.PerPackage + "</PerPackage>";
                            XML_Det = XML_Det + "<StdRate>" + item.StdRate + "</StdRate>";
                            XML_Det = XML_Det + "<RateCharged>" + item.RateCharged + "</RateCharged>";
                            XML_Det = XML_Det + "<PortelPaidPerBranch>" + item.PortelPaidPerBranch + "</PortelPaidPerBranch>";
                            XML_Det = XML_Det + "<OtherTSPEtc>" + item.OtherTSPEtc + "</OtherTSPEtc>";
                            XML_Det = XML_Det + "<Marker>" + item.Marker + "</Marker>";
                            XML_Det = XML_Det + "<HandlingInsideThePlatForm>" + item.HandlingInsideThePlatForm + "</HandlingInsideThePlatForm>";
                            XML_Det = XML_Det + "<TotalClaimMade_ExlHandling>" + item.TotalClaimMade_ExlHandling + "</TotalClaimMade_ExlHandling>";
                            XML_Det = XML_Det + "<TotalClaim_IncHandling>" + item.TotalClaim_IncHandling + "</TotalClaim_IncHandling>";
                            XML_Det = XML_Det + "<PerKGCost_PKG>" + item.PerKGCost_PKG + "</PerKGCost_PKG>";
                            XML_Det = XML_Det + "<PerKGCost_Weight>" + item.PerKGCost_Weight + "</PerKGCost_Weight>";
                            XML_Det = XML_Det + "<NameOfStation>" + item.NameOfStation + "</NameOfStation>";
                            XML_Det = XML_Det + "<ImageName>" + ImageName + "</ImageName>";
                        }
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Branch_Expenses_EntryDET>";

                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #region Porter Expense Master

        public JsonResult GetPorterExpense_Detail(string BRCD)
        {
            string IsRecordFound = "", NameOfStation = "", Porter_Type = "", Rate_Charged = "";
            bool Marker = false, Handling = false, Other = false;
            DataTable DT = new DataTable();
            CYGNUS_Porter_Expense OBJCPE = new CYGNUS_Porter_Expense();

            try
            {
                OBJCPE = BAS.GetPorterExpenseMasterDetails(BRCD).First();
                if (OBJCPE != null)
                {
                    IsRecordFound = "1";

                    NameOfStation = OBJCPE.NameOfStation;
                    Porter_Type = OBJCPE.Porter_Type;
                    Rate_Charged = OBJCPE.Rate_Charged;
                    Marker = OBJCPE.Marker;
                    Handling = OBJCPE.Handling;
                    Other = OBJCPE.Other;
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        NameOfStation = OBJCPE.NameOfStation,
                        Porter_Type = OBJCPE.Porter_Type,
                        Rate_Charged = OBJCPE.Rate_Charged,
                        Marker = OBJCPE.Marker,
                        Handling = OBJCPE.Handling,
                        Other = OBJCPE.Other
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        NameOfStation = OBJCPE.NameOfStation,
                        Porter_Type = OBJCPE.Porter_Type,
                        Rate_Charged = OBJCPE.Rate_Charged,
                        Marker = OBJCPE.Marker,
                        Handling = OBJCPE.Handling,
                        Other = OBJCPE.Other
                    }
                };
            }
        }

        public ActionResult PorterExpenseMaster()
        {
            ExpensesMasters PEVM = new ExpensesMasters();
            string BRCD = BaseLocationCode;
            PEVM.CPE = new CYGNUS_Porter_Expense();//BAS.GetPorterExpenseMasterDetails(BRCD).FirstOrDefault();
            PEVM.CPEListing = new List<CYGNUS_Porter_Expense>();
            return View(PEVM);
        }

        public ActionResult PorterExpenseMasterSubmit(ExpensesMasters PEVM, List<CYGNUS_Porter_Expense> CPE)
        {
            try
            {
                DataTable DT = new DataTable();
                string BRCD = BaseLocationCode;
                string Location = CPE.First().Location;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(CPE.GetType());
                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, CPE);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DT = BAS.Porter_Master_Expense(xmlDoc1.InnerXml, BaseUserName, BaseLocationCode, Location);

                return RedirectToAction("Master_Done", new { ID = 2 });
            }

            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _Add_Porter_Expenses_Details(int id, string BRCD, int Type)
        {
            try
            {
                if (Type == 1)
                {
                    List<CYGNUS_Porter_Expense> ListCPE = new List<CYGNUS_Porter_Expense>();
                    ListCPE = BAS.GetPorterExpenseMasterDetails(BRCD);
                    return PartialView("_Porter_Expenses_Master_Partial_List", ListCPE);
                }
                else
                {
                    CYGNUS_Porter_Expense ObjCPE = new CYGNUS_Porter_Expense();
                    ObjCPE.ID = id;
                    ObjCPE.Location = BRCD;
                    return PartialView("_Porter_Expenses_Master_Partial", ObjCPE);
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public JsonResult Get_PorterMaster_Detailes(string PorterType, string NameOfStation, string BRCD)
        {
            string IsRecordFound = "", Rate_Charged = "", Marker = "", Other = "", Handling = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.Get_PorterData_Details(PorterType, NameOfStation, BaseUserName.ToUpper(), BRCD);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";
                    Rate_Charged = DT.Rows[0]["Rate_Charged"].ToString();
                    Marker = DT.Rows[0]["Marker"].ToString();
                    Other = DT.Rows[0]["Other"].ToString();
                    Handling = DT.Rows[0]["Handling"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Rate_Charged = Rate_Charged,
                        Marker = Marker,
                        Other = Other,
                        Handling = Handling
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Rate_Charged = Rate_Charged,
                        Marker = Marker,
                        Other = Other,
                        Handling = Handling
                    }
                };
            }
        }

        public JsonResult Get_Porter_Name_Of_Station(string Type, string BRCD)
        {
            List<CYGNUS_Porter_Expense> ListPorter = new List<CYGNUS_Porter_Expense>();
            ListPorter = BAS.GetPorterExpenseMasterDetails(BRCD).Where(c => c.Porter_Type == Type).ToList();

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.NameOfStation,
                                  Text = e.NameOfStation,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Porter Expenses Approval Reject Criteria

        public ActionResult PorterExpenseApprovalReject(int TYP)
        {
            try
            {
                BranchAccountingMaster BAM = new BranchAccountingMaster();
                BAM.ConveyanceExpense = "Porter Expenses";
                @ViewBag.ActionName = "AccotingApproval_Reject_Porter";
                BAM.SubType = 2;
                if (TYP == 1)
                {
                    if (BaseLocationLevel == 1)
                    {
                        BAM.SubmitType = "1";
                        ViewBag.Title = "Porter Expenses Approval Reject Criteria";
                    }
                    else
                    {
                        return RedirectToAction("URLRedirect", "Home");
                    }
                }
                else
                {
                    ViewBag.Title = "Porter Expenses Payment Criteria";
                }
                return View(BAM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        public JsonResult Get_Staff_ListFor_Porter(string Branch, string SubmitType)
        {
            List<CYGNUS_Branch_Expance> ListPorter = new List<CYGNUS_Branch_Expance>();
            ListPorter = BAS.GetStaffListFor_Porter(Branch, SubmitType);

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.Value,
                                  Text = e.Text,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_Month_For_Porter(string Staff, string SubmitType)
        {
            List<CYGNUS_Branch_Expance> ListPorter = new List<CYGNUS_Branch_Expance>();
            ListPorter = BAS.GetMonth_For_Porter(Staff, SubmitType);

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.Value,
                                  Text = e.Text,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccotingApproval_Porter(string Location, string Staff, int Month, string SubmitType)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            PEEM.ListCPEE_HDR = new List<CYGNUS_Branch_Expenses_EntryHDR>();
            PEEM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();
            PEEM.CPEE_HDR.Approve_Account_Type = 1;
            PEEM.ListCPEE_HDR = BAS.GetPorterAprroval_List(Location, Staff, Month, SubmitType);
            return PartialView("_AccotingApproval_Porter", PEEM);
        }

        public ActionResult AccotingApproval_Reject_Porter(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Expenses_EntryHDR> CPEEApprov)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();
                PEVM.ListCPEE_DET = new List<CYGNUS_Branch_Expenses_EntryDET>();
                string Nos = "";
                int Bill_Status_ID = 0;
                foreach (var item in CPEEApprov.Where(c => c.IsChecked == true).ToList())
                {

                    if (item.IsChecked)
                    {
                        if (Nos == "")
                            Nos = item.BillNo.ToString();
                        else
                            Nos = Nos + "," + item.BillNo;
                    }

                    PEVM.CPEE_HDR.Bill_Status_ID = item.Bill_Status_ID;
                }
                Bill_Status_ID = PEVM.CPEE_HDR.Bill_Status_ID;
                DataSet DS = BAS.GetBrancAcct_Aprroval_Reject_Porter(Nos, Bill_Status_ID);
                PEVM.CPEE_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(DS.Tables[0]).FirstOrDefault();
                PEVM.ListCPEE_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(DS.Tables[1]);

                PEVM.CPEE_HDR.ExpenceType = 5;

                DataTable DT_Get_Name = new DataTable();
                if (Bill_Status_ID == 2)
                {
                    DataTable DT_Tax = OS.GetServiceTaxRate();
                    decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
                    decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
                    decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());
                    Webx_Account_Details OBjWAD = new Webx_Account_Details();
                    OBjWAD.SRNo = 1;
                    OBjWAD.Vouchertype = "";
                    //PEVM.WAD.SRNo = 1;
                    //PEVM.WAD.ServiceTax = HdnServiceTaxRate;
                    PEVM.HdnEduCessRate = HdnEduCessRate;
                    PEVM.HdnHEduCessRate = HdnHEduCessRate;
                    PEVM.HdnServiceTaxRate = HdnServiceTaxRate;
                    PEVM.SubType = 3;
                    ViewBag.SubType = 3;
                    PEVM.Type2 = 5;

                    List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                    ListWAD1.Add(OBjWAD);
                    PEVM.WADListing = ListWAD1;
                }
                else
                {
                    PEVM.SubType = 2;
                    ViewBag.SubType = 2;
                }
                PEVM.ExpanceType = 5;
                ViewBag.Type = "Porter";
                ViewBag.Title = "PorterExpenses";
                PEVM.CPEE_HDR.BillNo = Nos;
                return View("PorterExpenses", PEVM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #endregion

        #region Market Vehicle Charges Expenses

        public ActionResult MarketVehicleCharges(int Type, string No)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = Type;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Type = Type;
                ViewBag.Title = "Market Vehicle Charges";
                ViewBag.ActionName = "MarketVehicleChargesExpensesSubmit";
                PEVM.MarketVehicleCharges_Expence_HDR = new CYGNUS_MarketVehicleCharges_Expence_HDR();
                PEVM.MarketVehicleCharges_Expence_HDR.DocumentNo = No;
                PEVM.MarketVehicleCharges_Expence_HDR.BR_Date = System.DateTime.Now;

                PEVM.List_MarketVehicle_Expence_DET = BAS.GetBRCD_MarketVehicleCharges_Detail(Type, No, BaseLocationCode);
                if (PEVM.List_MarketVehicle_Expence_DET.Count() == 0)
                {
                    ViewBag.StrError = "Please Enter Valid THC No.";
                    return View("Error");
                }

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Market Vehicle Charges Expenses Entry

        public ActionResult MarketVehicleChargesExpensesSubmit(ExpensesEntryModule PEVM, List<CYGNUS_MarketVehicleCharges_Expence_DET> CPEEDET)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 6);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "Maximum Approved Amount For Market Vehicle Expance was '" + MaxLimit + "'.";
                    return View("Error");
                }

                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN PORTER EXPENSES ";
                    return View("Error");
                }

                PEVM.MarketVehicleCharges_Expence_HDR.Brcd = BaseLocationCode;
                PEVM.MarketVehicleCharges_Expence_HDR.ExpenceType = PEVM.Type2;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.MarketVehicleCharges_Expence_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.MarketVehicleCharges_Expence_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = MarketVehicleCharges_DET_XML(CPEEDET, 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_MarketVehicleCharges_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "6" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Market Vehicle Charges Expenses Approval

        public ActionResult MarketVehicleCharges_Approval(ExpensesEntryModule PEVM, List<CYGNUS_MarketVehicleCharges_Expence_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_MarketVehicleCharges_Expence_DET> ListCPEEDet = new List<CYGNUS_MarketVehicleCharges_Expence_DET>();
                PEVM.MarketVehicleCharges_Expence_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.MarketVehicleCharges_Expence_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.MarketVehicleCharges_Expence_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = MarketVehicleCharges_DET_XML(CPEEDET, 2);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_MarketVehicleCharges_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.MarketVehicleCharges_Expence_HDR.BillNo, PEVM.MarketVehicleCharges_Expence_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "6" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Market Vehicle Charges Expenses Rejection

        [HttpPost]
        public ActionResult MarketVehicleCharges_Reject(ExpensesEntryModule PEVM, List<CYGNUS_MarketVehicleCharges_Expence_DET> CPEEDET)
        {
            try
            {
                string XML = "";
                if (CPEEDET != null)
                {
                    XML = MarketVehicleCharges_DET_XML(CPEEDET, 2);
                }
                else
                {
                    @ViewBag.StrError = "Please Make Proper Selection.";
                    return View("Error");
                }

                DataTable DT = BAS.Insert_MarketVehicleCharges_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.MarketVehicleCharges_Expence_HDR.BillNo, PEVM.MarketVehicleCharges_Expence_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 2, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string MarketVehicleCharges_DET_XML(List<CYGNUS_MarketVehicleCharges_Expence_DET> CPEEDET, int Type)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {

                    if (item.CheckBox)
                    {
                        XML_Det = XML_Det + "<CYGNUS_MarketVehicleCharges_Expence_DET>";
                        XML_Det = XML_Det + "<Date>" + item.Date + "</Date>";
                        XML_Det = XML_Det + "<Vendor_Place_Name>" + item.Vendor_Place_Name + "</Vendor_Place_Name>";
                        XML_Det = XML_Det + "<Docket_PRS_DRS_THC_No>" + item.Docket_PRS_DRS_THC_No + "</Docket_PRS_DRS_THC_No>";
                        XML_Det = XML_Det + "<MFrom>" + item.MFrom + "</MFrom>";
                        XML_Det = XML_Det + "<MTo>" + item.MTo + "</MTo>";
                        XML_Det = XML_Det + "<Distance>" + item.Distance + "</Distance>";
                        XML_Det = XML_Det + "<Vehicle_Number>" + item.Vehicle_Number + "</Vehicle_Number>";
                        XML_Det = XML_Det + "<Contract_Number_of_Vehicle_Owner>" + item.Contract_Number_of_Vehicle_Owner + "</Contract_Number_of_Vehicle_Owner>";
                        XML_Det = XML_Det + "<Actual_Weight>" + item.Actual_Weight + "</Actual_Weight>";
                        XML_Det = XML_Det + "<Number_Of_Packages>" + item.Number_Of_Packages + "</Number_Of_Packages>";
                        XML_Det = XML_Det + "<Rate_Total_Amount_Weight>" + item.Rate_Total_Amount_Weight + "</Rate_Total_Amount_Weight>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<DocumentNo>" + item.DocumentNo + "</DocumentNo>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                        XML_Det = XML_Det + "</CYGNUS_MarketVehicleCharges_Expence_DET>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<CYGNUS_MarketVehicleCharges_Expence_DET>";
                        XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                        XML_Det = XML_Det + "</CYGNUS_MarketVehicleCharges_Expence_DET>";
                    }
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }


        #endregion

        #region Office Fixed Expenses

        #region Office Expense Fixed Master

        public ActionResult Office_Expense_Fixed()
        {
            List<CYGNUS_Master_Office_Fixed_Expenses> ListCMOOE = new List<CYGNUS_Master_Office_Fixed_Expenses>();
            try
            {
                string BRCD = BaseLocationCode;
                ListCMOOE = BAS.List_Fixed_Office_Expense(BaseLocationCode, BaseUserName.ToUpper());
                return View(ListCMOOE);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Submit_Office_Expense_Fixed(List<CYGNUS_Master_Office_Fixed_Expenses> CMOFE)
        {
            try
            {
                List<CYGNUS_Master_Office_Fixed_Expenses> ListCMOFE = new List<CYGNUS_Master_Office_Fixed_Expenses>();

                if (CMOFE != null)
                {
                    foreach (var item in CMOFE)
                    {
                        CYGNUS_Master_Office_Fixed_Expenses ObjCMOFE = new CYGNUS_Master_Office_Fixed_Expenses();
                        ObjCMOFE.BRCD = item.BRCD;
                        ObjCMOFE.TEA = item.TEA;
                        ObjCMOFE.Water = item.Water;
                        ObjCMOFE.Pooja = item.Pooja;
                        ObjCMOFE.Sweeper = item.Sweeper;
                        ObjCMOFE.NewsPaper = item.NewsPaper;
                        ObjCMOFE.Watchmen = item.Watchmen;
                        ListCMOFE.Add(ObjCMOFE);
                    }
                }

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(CMOFE.GetType());
                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, ListCMOFE);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DT = BAS.Insert_Office_Expense_Fixed(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper());

                string Message = DT.Rows[0]["Message"].ToString();
                string Status = DT.Rows[0]["Status"].ToString();
                if (Message == "Done" && Status == "1")
                {
                    return RedirectToAction("Office_Expense_Done", new { id = 1 });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public ActionResult Office_Expenses_Fixed()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 7;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.ActionName = "OfficeExpensesSubmitFixed";
                PEVM.List_Office_Fixed_Expense_DET = BAS.GetBRCD_ACCT_FixedExp_Det_Detail(BaseLocationCode, BaseUserName.ToUpper());
                CYGNUS_Branch_Office_Expenses_Fixed_HDR OBJ_HDR = new CYGNUS_Branch_Office_Expenses_Fixed_HDR();

                DataTable DT = BAS.Get_OfficeExpance_HDR_Details(BaseLocationCode, BaseUserName.ToUpper(), 7);
                OBJ_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_HDR>(DT).First();

                PEVM.Office_Fixed_Expense_HDR = OBJ_HDR;
                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Office Fixed Expenses Entry

        public ActionResult OfficeExpensesSubmitFixed(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Fixed_DET> CPEEDET, IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 7);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR OFFICE EXPENSES FIXED FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN OFFICE EXPENSES FIXED ";
                    return View("Error");
                }

                List<CYGNUS_Branch_Office_Expenses_Fixed_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Fixed_DET>();

                PEVM.Office_Fixed_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Office_Fixed_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Office_Fixed_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Office_Fixed_Expenses_DET_XML(CPEEDET, 1, files);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Office_Fixed_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "7" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public string Office_Fixed_Expenses_DET_XML(List<CYGNUS_Branch_Office_Expenses_Fixed_DET> CPEEDET, int Type, IEnumerable<HttpPostedFileBase> files)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    

                    XML_Det = XML_Det + "<CYGNUS_Branch_Office_Expenses_Fixed_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {

                        string ImageName = "";
                        #region OfficeExpenseFixed Image Upload
                        try
                        {
                            if (Request.Files != null)
                            {
                                var File = Request.Files["OfficeFixedExpense_" + item.ID];
                                if (File.ContentLength > 0)
                                {
                                    ImageName = SaveImage(File, item.ID, "OfficeExpenseFixedImage");
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        #endregion

                        XML_Det = XML_Det + "<ExpenseType>" + item.ExpenseType + "</ExpenseType>";
                        XML_Det = XML_Det + "<LegderCode>" + item.LegderCode + "</LegderCode>";
                        XML_Det = XML_Det + "<FromDate>" + item.FromDate.ToString("dd MMM yyyy") + "</FromDate>";
                        XML_Det = XML_Det + "<ToDate>" + item.ToDate.ToString("dd MMM yyyy") + "</ToDate>";
                        XML_Det = XML_Det + "<For_the_Period>" + item.For_the_Period + "</For_the_Period>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Particular>" + item.Particular + "</Particular>";
                        XML_Det = XML_Det + "<OfficeExpenseFixedImage>" + ImageName + "</OfficeExpenseFixedImage>";

                    }
                    else if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Branch_Office_Expenses_Fixed_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }


        #endregion

        #region Office Fixed Expenses Approval

        public ActionResult Officer_FixedExpenses_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Fixed_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Branch_Office_Expenses_Fixed_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Fixed_DET>();

                PEVM.Office_Fixed_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Office_Fixed_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Office_Fixed_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Office_Fixed_Expenses_DET_XML(CPEEDET, 2,null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Office_Fixed_Expenses_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Office_Fixed_Expense_HDR.BillNo, PEVM.Office_Fixed_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = 7 });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        #endregion

        #region Office Fixed Expenses Reject

        [HttpPost]
        public ActionResult OfficeExpence_Fixed_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Fixed_DET> CPEEDET)
        {
            try
            {
                string XML = Office_Fixed_Expenses_DET_XML(CPEEDET, 2,null);

                DataTable DT = BAS.Office_Fixed_Expenses_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Office_Fixed_Expense_HDR.BillNo, PEVM.Office_Fixed_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 7, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Tea Expense Branch & User Wise

        public ActionResult TeaExpenseBranchUserWise()
        {
            TeaExpBranchUserWise cyopblac = new TeaExpBranchUserWise();
            return View(cyopblac);
        }

        public ActionResult GetBranchWiseUser(string BRCD)
        {
            TeaExpBranchUserWise PEEM = new TeaExpBranchUserWise();
            CYGNUS_Tea_Expense_Master CBE = new CYGNUS_Tea_Expense_Master();
            PEEM.ListCPEE_HDR = BAS.GetBranchWiseUser(BRCD);
            ViewBag.Type = "BranchWiseUsers";
            return PartialView("_BranchWiseUser_partial", PEEM);
        }

        public ActionResult GetBranchWiseUserIndividule(string BRCD)
        {
            TeaExpBranchUserWise PEEM = new TeaExpBranchUserWise();
            CYGNUS_Tea_Expense_Master CBE = new CYGNUS_Tea_Expense_Master();
            PEEM.ListCPEE_HDR = BAS.GetBranchWiseUserEdit(BRCD);
            ViewBag.Type = "123";
            return PartialView("_BranchWiseUser_partial", PEEM);
        }

        public JsonResult GetBranchWiseUserEdit(string BRCD)
        {
            string IsRecordFound = "";
            List<CYGNUS_Tea_Expense_Master> ObjCEOGDList = new List<CYGNUS_Tea_Expense_Master>();
            CYGNUS_Tea_Expense_Master ObjCEOGD = new CYGNUS_Tea_Expense_Master();
            try
            {
                ObjCEOGDList = BAS.GetBranchWiseUserEdit(BRCD).ToList();
                ObjCEOGD = ObjCEOGDList.FirstOrDefault();

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = 1,
                        Amount = ObjCEOGD.Amount,
                        Mapped = ObjCEOGD.Mapped,
                    }
                };
            }
            catch (Exception)
            {
                IsRecordFound = "2";

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        public ActionResult TeaExpenseBranchUserWise_Submit(TeaExpBranchUserWise PEEM, List<CYGNUS_Tea_Expense_Master> CPEEDET)
        {
            string XML_BRCD = "", XML_Mapped = "";
            DataTable DT = new DataTable();
            List<CYGNUS_Tea_Expense_Master> ListCPEEDET = new List<CYGNUS_Tea_Expense_Master>();

            try
            {
                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        CYGNUS_Tea_Expense_Master ObjBAEM = new CYGNUS_Tea_Expense_Master();
                        XML_BRCD = PEEM.BRCD;
                        ObjBAEM.Amount = item.Amount;
                        ObjBAEM.UserId = item.UserId;
                        ObjBAEM.Name = item.Name;
                        XML_Mapped = PEEM.Mapped;
                        ListCPEEDET.Add(ObjBAEM);
                    }
                }
                if (PEEM.Mapped == "Individual")
                {
                    XmlDocument xmlDoc1 = new XmlDocument();
                    XmlSerializer xmlSerializer1 = new XmlSerializer(ListCPEEDET.GetType());

                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, ListCPEEDET);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }
                    DT = BAS.InsertTeaExpenseBranchUserWise(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper(), XML_BRCD, XML_Mapped, 0);
                }
                else
                {
                    DT = BAS.InsertTeaExpenseBranchUserWise(PEEM.CPEE_HDR.Name, BaseLocationCode, BaseUserName.ToUpper(), PEEM.BRCD, PEEM.Mapped, PEEM.CPEE_HDR.Amount);
                }

                int Status = Convert.ToInt32(DT.Rows[0]["Status"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("BrcdAccDone");
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public JsonResult check_Tea_Expense(string EmpCode, string Month)
        {
            string IsRecordFound = "";
            DataTable DT = new DataTable();
            CYGNUS_Tea_Expense_Master ObjCEOGD = new CYGNUS_Tea_Expense_Master();

            try
            {
                string SQRY = "exec USP_Check_Tea_Expense_Details '" + EmpCode + "','" + BaseLocationCode + "','" + Month + "'";

                DT = GF.GetDataTableFromSP(SQRY);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = DT.Rows[0][0].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = DT.Rows[0][0].ToString(),
                    }
                };

            }
            catch (Exception)
            {
                IsRecordFound = "2";

                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        #endregion


        #endregion

        #region Office Other Expenses

        #region Office Expense Other Master

        public ActionResult Office_Expense_Other()
        {
            List<CYGNUS_Master_Office_Other_Expenses> ListCMOOE = new List<CYGNUS_Master_Office_Other_Expenses>();
            try
            {
                string BRCD = BaseLocationCode;
                ListCMOOE = BAS.List_Other_Office_Expense(BaseLocationCode, BaseUserName.ToUpper());
                return View(ListCMOOE);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Submit_Office_Expense_Other(List<CYGNUS_Master_Office_Other_Expenses> CMOOE)
        {
            try
            {
                List<CYGNUS_Master_Office_Other_Expenses> ListCMOOE = new List<CYGNUS_Master_Office_Other_Expenses>();

                if (CMOOE != null)
                {
                    foreach (var item in CMOOE)
                    {
                        CYGNUS_Master_Office_Other_Expenses ObjCMOOE = new CYGNUS_Master_Office_Other_Expenses();
                        ObjCMOOE.BRCD = item.BRCD;
                        ObjCMOOE.Printing_Stationary = item.Printing_Stationary;
                        ObjCMOOE.Data_Entry_Computer = item.Data_Entry_Computer;
                        ObjCMOOE.RPF_GRP_Police = item.RPF_GRP_Police;
                        ObjCMOOE.Sales_Promotion = item.Sales_Promotion;
                        ObjCMOOE.Festival = item.Festival;
                        ObjCMOOE.Donation = item.Donation;
                        ObjCMOOE.Generator_Running_Diesel = item.Generator_Running_Diesel;
                        ObjCMOOE.Any_Purchase = item.Any_Purchase;
                        ListCMOOE.Add(ObjCMOOE);
                    }
                }

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(ListCMOOE.GetType());
                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, ListCMOOE);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                DataTable DT = BAS.Insert_Office_Expense_Other(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName.ToUpper());

                string Message = DT.Rows[0]["Message"].ToString();
                string Status = DT.Rows[0]["Status"].ToString();

                return RedirectToAction("Office_Expense_Done", new { id = 2 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public ActionResult Office_Expenses_Other()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 8;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.ActionName = "OfficeExpensesSubmitFixedOthers";
                PEVM.List_Office_Other_Expense_DET = BAS.GetBRCD_ACCT_OtherExp_Det_Detail(BaseLocationCode, BaseUserName.ToUpper());
                CYGNUS_Branch_Office_Expenses_Other_HDR OBJ_HDR = new CYGNUS_Branch_Office_Expenses_Other_HDR();
                DataTable DT = BAS.Get_OfficeExpance_HDR_Details(BaseLocationCode, BaseUserName.ToUpper(), 8);
                OBJ_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_HDR>(DT).First();
                PEVM.Office_Other_Expense_HDR = OBJ_HDR;

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Office Others Expenses Entry

        public ActionResult OfficeExpensesSubmitFixedOthers(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Other_DET> CPEEDET, IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 8);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR OFFICE EXPENSES OTHERS FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN OFFICE EXPENSES OTHERS ";
                    return View("Error");
                }

                List<CYGNUS_Branch_Office_Expenses_Other_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Other_DET>();

                PEVM.Office_Other_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Office_Other_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Office_Other_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Office_Other_Expenses_DET_XML(CPEEDET, 1, files);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Office_Other_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "8" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Office Others Expenses Approval

        public ActionResult Officer_OtherExpenses_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Other_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Branch_Office_Expenses_Other_DET> ListCPEEDet = new List<CYGNUS_Branch_Office_Expenses_Other_DET>();

                PEVM.Office_Other_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Office_Other_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Office_Other_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Office_Other_Expenses_DET_XML(CPEEDET, 2, null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Office_Other_Expenses_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Office_Other_Expense_HDR.BillNo, PEVM.Office_Other_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "8" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Office Others Expenses Reject

        [HttpPost]
        public ActionResult OfficeExpence_Other_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Office_Expenses_Other_DET> CPEEDET)
        {
            try
            {
                string XML = Office_Other_Expenses_DET_XML(CPEEDET, 2, null);

                DataTable DT = BAS.Office_Other_Expenses_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Office_Other_Expense_HDR.BillNo, PEVM.Office_Other_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 8, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string Office_Other_Expenses_DET_XML(List<CYGNUS_Branch_Office_Expenses_Other_DET> CPEEDET, int Type, IEnumerable<HttpPostedFileBase> files)
        {
            string XML_Det = "<root>";//, extension = "", name = "", path = "";
            //int id = 0;
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Branch_Office_Expenses_Other_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        #region OfficeExpenseOther Image Upload
                        string ImageName = "";
                        try
                        {
                            if (Request.Files != null)
                            {
                                var File = Request.Files["OfficeOtherExpense_" + item.ID];
                                if (File.ContentLength > 0)
                                {
                                    ImageName = SaveImage(File, item.ID, "OfficeExpOtherImages");
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        #endregion

                        XML_Det = XML_Det + "<ExpenseType>" + item.ExpenseType + "</ExpenseType>";
                        XML_Det = XML_Det + "<LegderCode>" + item.LegderCode + "</LegderCode>";
                        XML_Det = XML_Det + "<FromDate>" + item.FromDate + "</FromDate>";
                        XML_Det = XML_Det + "<ToDate>" + item.ToDate + "</ToDate>";
                        XML_Det = XML_Det + "<For_the_Period>" + item.For_the_Period + "</For_the_Period>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<Particular>" + item.Particular + "</Particular>";
                        XML_Det = XML_Det + "<OfficeExpOtherImg>" + ImageName + "</OfficeExpOtherImg>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Branch_Office_Expenses_Other_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #endregion

        #region Office Expance Common Process

        public JsonResult Is_Office_Exepance_Available(int Type, int Month, string ExpenseType, string BRCD, int id)
        {
            string IsRecordFound = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.GetIs_Office_Exepance_Available(Type, Month, System.DateTime.Now.Year, BRCD, BaseUserName.ToUpper(), ExpenseType, id);

                if (DT.Rows.Count > 1)
                {
                    IsRecordFound = "1";
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound
                    }
                };
            }
        }

        public JsonResult GetVendor_Wise(string searchTerm, string Type)
        {
            var CMP = BAS.Get_Vendor_BranchWise(searchTerm, BaseLocationCode, BaseUserName, Type);
            var users = from user in CMP
                        select new
                        {
                            id = user.VENDORCODE,
                            text = user.VENDORNAME
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Food Expenses

        public ActionResult Food_Expense()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 9;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.Title = "Food Expenses";
                ViewBag.ActionName = "Food_Expenses_Submit";

                CYGNUS_Food_Expense_DET Obj_CBEE = new CYGNUS_Food_Expense_DET();
                List<CYGNUS_Food_Expense_DET> List_CBEE = new List<CYGNUS_Food_Expense_DET>();
                Obj_CBEE.ID = 1;
                Obj_CBEE.Date = System.DateTime.Now;
                List_CBEE.Add(Obj_CBEE);
                PEVM.List_Food_DET = List_CBEE;
                PEVM.Food_Expense_HDR = new CYGNUS_Food_Expense_HDR();
                PEVM.Food_Expense_HDR.BR_Date = System.DateTime.Now;

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Food Expenses Entry

        public ActionResult Food_Expenses_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Food_Expense_DET> CPEEDET, IEnumerable<HttpPostedFileBase> files)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 9);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR FOOD EXPENSES FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN FOOD EXPENSES.";
                    return View("Error");
                }

                List<CYGNUS_Food_Expense_DET> ListCPEEDet = new List<CYGNUS_Food_Expense_DET>();

                PEVM.Food_Expense_HDR.BRCD = BaseLocationCode;
                PEVM.Food_Expense_HDR.ExpenceType = 9;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Food_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Food_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Food_Expenses_DET_XML(CPEEDET, 1,null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Food_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "9" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Food Expenses Approval

        public ActionResult FoodExpenses_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Food_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Food_Expense_DET> ListCPEEDet = new List<CYGNUS_Food_Expense_DET>();

                PEVM.Food_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Food_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Food_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Food_Expenses_DET_XML(CPEEDET, 2,null);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Food_Expenses_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Food_Expense_HDR.BillNo, PEVM.Food_Expense_HDR.ExpenceType);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "9" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        #endregion

        #region Food Reject

        [HttpPost]
        public ActionResult Food_Expence_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Food_Expense_DET> CPEEDET)
        {
            try
            {
                string XML = Food_Expenses_DET_XML(CPEEDET, 2,null);

                DataTable DT = BAS.Food_Expenses_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Food_Expense_HDR.BillNo, PEVM.Food_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 9, No = BillNo });

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string Food_Expenses_DET_XML(List<CYGNUS_Food_Expense_DET> CPEEDET, int Type, IEnumerable<HttpPostedFileBase> files)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Food_Expense_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {

                        string ImageName = "";
                        #region Food Expenses Image Upload

                        try
                        {
                            if (Request.Files != null)
                            {
                                var File = Request.Files["FoodExpImg_" + item.ID];
                                if (File.ContentLength > 0)
                                {
                                    ImageName = SaveImage(File, item.ID, "FoodExpenseImges");
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                        #endregion

                        XML_Det = XML_Det + "<Date>" + item.Date + "</Date>";
                        XML_Det = XML_Det + "<Number_Of_Staff>" + item.Number_Of_Staff + "</Number_Of_Staff>";
                        XML_Det = XML_Det + "<Staff_Member_Name>" + item.Staff_Member_Name + "</Staff_Member_Name>";
                        XML_Det = XML_Det + "<Reason>" + item.Reason + "</Reason>";
                        XML_Det = XML_Det + "<Concerning_Docket_PRS_DRS_THC_No>" + item.Concerning_Docket_PRS_DRS_THC_No + "</Concerning_Docket_PRS_DRS_THC_No>";
                        XML_Det = XML_Det + "<DocketNo>" + item.DocketNo + "</DocketNo>";
                        XML_Det = XML_Det + "<Party_Location_Name>" + item.Party_Location_Name + "</Party_Location_Name>";
                        XML_Det = XML_Det + "<Rate_Per_Day>" + item.Rate_Per_Day + "</Rate_Per_Day>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<FoodImageName>" + ImageName + "</FoodImageName>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Food_Expense_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #endregion

        #region Detension Expenses Expenses

        public ActionResult DetensionExpenses()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = 10;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.Title = "Detension Expenses";
                ViewBag.ActionName = "DetensionExpensesSubmit";

                CYGNUS_Detention_Expense_DET Obj_CBEE = new CYGNUS_Detention_Expense_DET();
                List<CYGNUS_Detention_Expense_DET> List_CBEE = new List<CYGNUS_Detention_Expense_DET>();
                Obj_CBEE.ID = 1;
                Obj_CBEE.Date_From = System.DateTime.Now;
                Obj_CBEE.Date_To = System.DateTime.Now;
                List_CBEE.Add(Obj_CBEE);
                PEVM.List_Detension_Expense_DET = List_CBEE;

                PEVM.Detension_Expense_HDR = new CYGNUS_Detention_Expense_HDR();
                PEVM.Detension_Expense_HDR.ExpenceType = 10;
                PEVM.Detension_Expense_HDR.BR_Date = System.DateTime.Now;

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Detension Expenses Entry

        public ActionResult DetensionExpensesSubmit(ExpensesEntryModule PEVM, List<CYGNUS_Detention_Expense_DET> CPEEDET)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 10);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR Detension EXPENSES FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN Detension EXPENSES.";
                    return View("Error");
                }

                List<CYGNUS_Detention_Expense_DET> ListCPEEDet = new List<CYGNUS_Detention_Expense_DET>();
                PEVM.Detension_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Detension_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Detension_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Detension_Expenses_DET_XML(CPEEDET, 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_Detension_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "10" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Detension Expenses Approval

        public ActionResult Detension_Expenses_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Detention_Expense_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Detention_Expense_DET> ListCPEEDet = new List<CYGNUS_Detention_Expense_DET>();
                PEVM.Detension_Expense_HDR.BRCD = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Detension_Expense_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Detension_Expense_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = Detension_Expenses_DET_XML(CPEEDET, 2);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_Detension_Expenses_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Detension_Expense_HDR.BillNo, PEVM.Detension_Expense_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "6" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Detension Reject

        [HttpPost]
        public ActionResult Detension_Expence_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Detention_Expense_DET> CPEEDET)
        {
            try
            {
                string XML = Detension_Expenses_DET_XML(CPEEDET, 2);

                DataTable DT = BAS.Detension_Expenses_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Detension_Expense_HDR.BillNo, PEVM.Detension_Expense_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 9, No = BillNo });

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string Detension_Expenses_DET_XML(List<CYGNUS_Detention_Expense_DET> CPEEDET, int Type)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Detention_Expense_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Vehicle_Number>" + item.Vehicle_Number + "</Vehicle_Number>";
                        XML_Det = XML_Det + "<Date_From>" + item.Date_From.ToString("dd MMM yyyy") + "</Date_From>";
                        XML_Det = XML_Det + "<Time_From>" + item.Date_From.ToString("h:mm:ss") + "</Time_From>";
                        XML_Det = XML_Det + "<Date_To>" + item.Date_To.ToString("dd MMM yyyy") + "</Date_To>";
                        XML_Det = XML_Det + "<Time_To>" + item.Date_To.ToString("h:mm:ss") + "</Time_To>";
                        XML_Det = XML_Det + "<TripSheetNo>" + item.TripSheetNo + "</TripSheetNo>";
                        XML_Det = XML_Det + "<Party_Location_Name>" + item.Party_Location_Name + "</Party_Location_Name>";
                        XML_Det = XML_Det + "<Number_Of_Days>" + item.Number_Of_Days + "</Number_Of_Days>";
                        XML_Det = XML_Det + "<Rate_Per_Day>" + item.Rate_Per_Day + "</Rate_Per_Day>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                        if (item.CheckBox)
                        {
                            XML_Det = XML_Det + "<IsApprove>1</IsApprove>";
                            XML_Det = XML_Det + "<ApproveBy>" + BaseUserName.ToUpper() + "</ApproveBy>";
                            XML_Det = XML_Det + "<ApproveDate>" + System.DateTime.Now.ToString("dd MMM yyyy") + "</ApproveDate>";
                        }
                        else
                        {
                            XML_Det = XML_Det + "<IsApprove>0</IsApprove>";
                            XML_Det = XML_Det + "<ApproveBy></ApproveBy>";
                            XML_Det = XML_Det + "<ApproveDate>01 Jan 1990</ApproveDate>";
                        }
                    }
                    XML_Det = XML_Det + "</CYGNUS_Detention_Expense_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #region Vehicle No Fiil Onther Fild

        public JsonResult Get_Vehicle_No_StatusDetails(string No, string For_The_Month_Of)
        {
            string CNT = "0", TripsheetNo = "", DateTimeFrom = "", DateTimeTo = "", TripSheet_StartLoc = "", NoOfDay = "";

            DataTable DT = new DataTable();

            //CYGNUS_Detention_Expense_HDR Obj_CBEE = new CYGNUS_Detention_Expense_HDR();

            DT = BAS.Check_Vehicle_StatusDetails(No, For_The_Month_Of);
            CNT = DT.Rows[0][0].ToString();

            TripsheetNo = DT.Rows[0][1].ToString();
            DateTimeFrom = DT.Rows[0][2].ToString();
            DateTimeTo = DT.Rows[0][3].ToString();
            TripSheet_StartLoc = DT.Rows[0][4].ToString();
            NoOfDay = DT.Rows[0][5].ToString();



            return new JsonResult()
            {
                Data = new
                {
                    CNT = CNT,
                    TripsheetNo = TripsheetNo,
                    DateTimeFrom = DateTimeFrom,
                    DateTimeTo = DateTimeTo,
                    TripSheet_StartLoc = TripSheet_StartLoc,
                    NoOfDay = NoOfDay
                }
            };
        }

        #endregion


        #endregion

        #region Tripsheet Disputed Amount

        public ActionResult TripsheetDisputedAmount()
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                CYGNUS_Tripsheet_Disputed_Amount_HDR CTDAH = new CYGNUS_Tripsheet_Disputed_Amount_HDR();
                PEVM.SubType = 1;
                ViewBag.SubType = 1;

                ViewBag.Title = "Tripsheet Disputed Amount";
                ViewBag.ActionName = "TripsheetDisputedAmountSubmit";
                CTDAH.DRIVERSETTLEMENTDATE = System.DateTime.Now;
                CTDAH.FINANCIALLYCLOSUREDATE = System.DateTime.Now;
                CTDAH.OPERATIONLLYCLOSUREDATE = System.DateTime.Now;
                CTDAH.TRIPSHEETSTARTDATE = System.DateTime.Now;
                CTDAH.BR_Date = System.DateTime.Now;

                PEVM.Tripsheet_HDR = CTDAH;

                List<CYGNUS_Tripsheet_Disputed_Amount_DET> List_CBEE = new List<CYGNUS_Tripsheet_Disputed_Amount_DET>();
                List_CBEE = BAS.GetPARTICULAR_TripsheetDisputedAmountDetail();
                PEVM.List_Tripsheet_DET = List_CBEE;

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Tripsheet Disputed Amount Entry

        public ActionResult TripsheetDisputedAmountSubmit(ExpensesEntryModule TSDA, List<CYGNUS_Tripsheet_Disputed_Amount_DET> CPEEDET)
        {
            try
            {
                int MaxLimit = GetCharge_MaxLimit(BaseLocationCode, 12);
                int SumAmount = Convert.ToInt32(CPEEDET.Sum(c => c.Amount));

                if (MaxLimit < SumAmount)
                {
                    @ViewBag.StrError = "MAXIMUM APPROVED AMOUNT FOR TRIPSHEET DISPUTED AMOUNT FOR BRANCH :- " + BaseLocationCode.ToUpper() + " WAS " + MaxLimit + ". AND YOU ENTER AMOUNT IS " + SumAmount + ".";
                    return View("Error");
                }
                if (SumAmount == 0)
                {
                    @ViewBag.StrError = "PLEASE ENTER VALID VALUE IN TRIPSHEET DISPUTED AMOUNT.";
                    return View("Error");
                }

                TSDA.Tripsheet_HDR.Brcd = BaseLocationCode;
                TSDA.Tripsheet_HDR.ExpenceType = 12;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(TSDA.Tripsheet_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, TSDA.Tripsheet_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = TripsheetDisputedAmount_DET_XML(CPEEDET, 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_TripsheetDisputedAmount(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName, basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "12" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Tripsheet Disputed Amount Approval

        public ActionResult TripsheetDisputedAmount_Approval(ExpensesEntryModule PEVM, List<CYGNUS_Tripsheet_Disputed_Amount_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_Tripsheet_Disputed_Amount_DET> ListCPEEDet = new List<CYGNUS_Tripsheet_Disputed_Amount_DET>();

                PEVM.Tripsheet_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Tripsheet_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Tripsheet_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = TripsheetDisputedAmount_DET_XML(CPEEDET, 2);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_TripsheetDisputedAmount_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName, basefinyear, PEVM.Tripsheet_HDR.BillNo, PEVM.Tripsheet_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "12" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }

        }

        #endregion

        #region Tripsheet Disputed Amount Reject

        public ActionResult TripsheetDisputedAmount_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Tripsheet_Disputed_Amount_DET> CPEEDET)
        {
            try
            {
                string XML = TripsheetDisputedAmount_DET_XML(CPEEDET, 2);
                DataTable DT = BAS.Tripsheet_Disputed_Amount_Expenses_Reject(BaseLocationCode, BaseUserName.ToUpper(), PEVM.Tripsheet_HDR.BillNo, PEVM.Tripsheet_HDR.ExpenceType, XML);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 12, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        public string TripsheetDisputedAmount_DET_XML(List<CYGNUS_Tripsheet_Disputed_Amount_DET> CPEEDET, int Type)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    XML_Det = XML_Det + "<CYGNUS_Tripsheet_Disputed_Amount_DET>";
                    XML_Det = XML_Det + "<ID>" + item.ID + "</ID>";
                    if (Type == 1)
                    {
                        XML_Det = XML_Det + "<Particular>" + item.Particular + "</Particular>";
                        XML_Det = XML_Det + "<KMASPERGPS>" + item.KMASPERGPS + "</KMASPERGPS>";
                        XML_Det = XML_Det + "<KMASDRIVERCLAIMED>" + item.KMASDRIVERCLAIMED + "</KMASDRIVERCLAIMED>";
                        XML_Det = XML_Det + "<AMOUNTPAIDAPPROVED>" + item.AMOUNTPAIDAPPROVED + "</AMOUNTPAIDAPPROVED>";
                        XML_Det = XML_Det + "<AMOUNTCLAIMEDBYDRIVER>" + item.AMOUNTCLAIMEDBYDRIVER + "</AMOUNTCLAIMEDBYDRIVER>";
                        XML_Det = XML_Det + "<DIFFEXCES>" + item.DIFFEXCES + "</DIFFEXCES>";
                        XML_Det = XML_Det + "<Amount>" + item.Amount + "</Amount>";
                        XML_Det = XML_Det + "<REMARK>" + item.REMARK + "</REMARK>";
                    }
                    if (Type == 2)
                    {
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                    }
                    XML_Det = XML_Det + "</CYGNUS_Tripsheet_Disputed_Amount_DET>";
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        public JsonResult GetDriverList(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<WEBX_FLEET_DRIVERMST> CMP = BAS.GetDriverList(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.Driver_Id,
                            text = user.Driver_Name

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #region Tripsheet No Validation

        //public JsonResult Get_Tripsheet_No_StatusDetails(string No)
        //{            
        //    DataTable DT = new DataTable();
        //    DT = BAS.Check_Tripsheet_No_StatusDetails(No, BaseLocationCode, BaseUserName);

        //    return new JsonResult()
        //    {
        //        Data = new
        //        {
        //            CNT = DT.Rows[0][0].ToString(),
        //            VEHICLENO = DT.Rows[0][1].ToString(),
        //            START_DATE = DT.Rows[0][2].ToString(),
        //            OPERATIONLLY_CLOSURE_DATE = DT.Rows[0][3].ToString(),
        //            FINANCIALLY_CLOSURE_DATE = DT.Rows[0][4].ToString(),
        //            DRIVER_SETTLEMENT_DATE = DT.Rows[0][5].ToString(),
        //            Driver1_ID = DT.Rows[0][6].ToString(),
        //            Driver1_Name = DT.Rows[0][7].ToString(),
        //            Vehicle_Mode = DT.Rows[0][8].ToString(),
        //            RUTCAT = DT.Rows[0][9].ToString(),
        //            RUTCD = DT.Rows[0][10].ToString(),
        //            Vehicle_Mode_ID = DT.Rows[0][11].ToString(),
        //            RUTCAT_ID = DT.Rows[0][12].ToString(),
        //            RUTCD_ID = DT.Rows[0][13].ToString()
        //        }
        //    };
        //}

        #endregion

        #endregion

        #region Payment Part

        #region Payment Criteria

        public ActionResult BRCDAcc_Payment_Criteria()
        {
            BranchAccountingMaster BMVW = new BranchAccountingMaster();
            string basefinyear = BaseYearVal;
            List<CYGNUS_Branch_Expance> optrkList = BAS.GET_BR_Dashbord_Report_Approval(BaseLocationCode);
            DataTable DT = BAS.BRStatusSummaryCashOnToday(BaseLocationCode, GF.FormateDate(System.DateTime.Now), GF.FormateDate(System.DateTime.Now), basefinyear, 0);
            BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode);
            BMVW.List_BRS = optrkList;
            BMVW.ApprovePaidType = 2;
            BMVW.SubType = 3;
            BMVW.FromDate = System.DateTime.Now;
            BMVW.ToDate = System.DateTime.Now;
            foreach (var item in optrkList)
            {
                item.type = BMVW.ApprovePaidType;
            }
            decimal NetAmt = Convert.ToDecimal(DT.Rows[0][2].ToString());
            BMVW.List_BRS = optrkList;
            CYGNUS_Branch_Expance BRSAmt = new CYGNUS_Branch_Expance();
            BRSAmt.NetAmt = NetAmt;
            BMVW.BRS = BRSAmt;
            ViewBag.ActionName = "Accounting_Payment";
            return View(BMVW);
        }

        public ActionResult AccotingApprovaledtime(DateTime FromDate, DateTime ToDate, string Location, string ExpenceType)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            PEEM.Approve_Account_Type = 1;
            string ToLocation = BaseLocationCode;
            if (BaseLocationCode == "HQTR")
            {
                ToLocation = Location;
            }

            PEEM.ListCPEE_HDR = BAS.GetBrancAcct_AprrovalComplite_List(ExpenceType.ToString(), ToLocation, FromDate.ToString("dd MMM yyyy"), ToDate.ToString("dd MMM yyyy"), BaseLocationCode);
            return PartialView("_AccotingApproval", PEEM);
        }

        #endregion

        #region Payment Screen Redirection

        public ActionResult Accounting_Payment(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Expenses_EntryHDR> CPEEApprov)
        {
            ExpensesEntryModule PEVM = new ExpensesEntryModule();

            DataTable DT_Tax = OS.GetServiceTaxRate();
            decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
            decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
            decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());

            PEVM.SubType = 3;
            PEVM.Type2 = BMVW.ExpenceType;
            ViewBag.Title = "Head Office Accoting Approval/Reject Module";
            PEVM.Type2 = BMVW.ExpenceType;
            ViewBag.SubType = 3;
            ViewBag.ActionName = "Accounting_ApprovalTIME";

            string Nos = "", BR_ViewName = "", EmpCode = "", BillNo = "", NameofDriver = ""; ;
            decimal DriverID = 0;

            PEVM.WAD = new Webx_Account_Details();

            Nos = BMVW.Id.ToString();

            #region Get Expance HDR Det Data
            DataSet DS = new DataSet();

            if (BMVW.ExpenceType == 4)
            {
                //CYGNUS_DriverIncentiveTSP_Expense_HDR Driver_TSP_Expense_HDR = new CYGNUS_DriverIncentiveTSP_Expense_HDR();
                PEVM.Driver_TSP_Expense_HDR = new CYGNUS_DriverIncentiveTSP_Expense_HDR();
                if (CPEEApprov != null)
                {
                    foreach (var item in CPEEApprov)
                    {
                        PEVM.Driver_TSP_Expense_HDR.Brcd = item.Brcd;
                    }
                }
                DS = BAS.GetBrancAcct(Nos, PEVM.Driver_TSP_Expense_HDR.Brcd, BaseUserName.ToUpper(), BMVW.ExpenceType);
            }
            else
            {
                DS = BAS.GetBrancAcct(Nos, BaseLocationCode, BaseUserName.ToUpper(), BMVW.ExpenceType);
            }

            if (BMVW.ExpenceType == 1)
            {
                PEVM.List_Elec_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Electricity_Expenses_DET>(DS.Tables[1]);
                PEVM.Elec_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Electricity_Expenses_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Elec_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 2)
            {
                PEVM.List_Conveyance_DET = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_DET>(DS.Tables[1]);
                PEVM.Conveyance_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Conveyance_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 3)
            {
                #region Trave_Tour

                PEVM.List_Trave_Tour_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_DET>(DS.Tables[1]);
                PEVM.Trave_Tour_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_HDR>(DS.Tables[0]).FirstOrDefault();

                PEVM.List_Fare_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "FARE").ToList();
                PEVM.List_Conveyance_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "CONVEYANCE").ToList();
                PEVM.List_Bo_Lo_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "BOARDING_LOADING").ToList();
                PEVM.List_Food_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "FOOD").ToList();
                PEVM.List_Other_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "OTHER").ToList();

                PEVM.Trave_Tour_Expense_DET = new CYGNUS_Branch_Travelling_Tour_DET();
                PEVM.Trave_Tour_Expense_DET.GrandTotal_SubTotal_Amount = PEVM.List_Trave_Tour_Expense_DET.Sum(c => c.Amount);

                EmpCode = PEVM.Trave_Tour_Expense_HDR.EmpCode;
                BillNo = PEVM.Trave_Tour_Expense_HDR.BillNo;

                #endregion
            }
            else if (BMVW.ExpenceType == 4)
            {
                PEVM.List_Driver_TSP_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_DET>(DS.Tables[1]);
                PEVM.Driver_TSP_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Driver_TSP_Expense_HDR.EmpCode;
                //NameofDriver = PEVM.Driver_TSP_Expense_HDR.DriverName;
                //DriverID = PEVM.List_Driver_TSP_Expense_DET.ToList();

                foreach (var item in PEVM.List_Driver_TSP_Expense_DET)
                {
                    DriverID = item.Driver_Id;
                    NameofDriver = item.Name_Of_Driver;
                }
                PEVM.NameofDriver = NameofDriver;
                PEVM.DriverID = DriverID;
                BillNo = PEVM.Driver_TSP_Expense_HDR.BillNo;
            }
            else if (BMVW.ExpenceType == 5)
            {
                PEVM.ListCPEE_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(DS.Tables[1]);
                PEVM.CPEE_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.CPEE_HDR.EmpCode;
                if (PEVM.CPEE_HDR.NameOfVendor != null && PEVM.CPEE_HDR.NameOfVendor != "")
                {
                    webx_VENDOR_HDR Obj_VEND = MS.GetVendorObject().Where(c => c.VENDORCODE == PEVM.CPEE_HDR.NameOfVendor).First();
                    PEVM.CPEE_HDR.Display_Name_Of_Vendor = Obj_VEND.VENDORNAME;
                    PEVM.WAD.ServiceTaxRegNo = Obj_VEND.SERVTAXNO;
                    PEVM.WAD.PANNumber = Obj_VEND.PAN_NO;
                }
            }
            else if (BMVW.ExpenceType == 6)
            {
                PEVM.List_MarketVehicle_Expence_DET = DataRowToObject.CreateListFromTable<CYGNUS_MarketVehicleCharges_Expence_DET>(DS.Tables[1]);
                PEVM.MarketVehicleCharges_Expence_HDR = DataRowToObject.CreateListFromTable<CYGNUS_MarketVehicleCharges_Expence_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.MarketVehicleCharges_Expence_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 7)
            {
                PEVM.List_Office_Fixed_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_DET>(DS.Tables[1]);
                PEVM.Office_Fixed_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Office_Fixed_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 8)
            {
                PEVM.List_Office_Other_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_DET>(DS.Tables[1]);
                PEVM.Office_Other_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Office_Other_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 9)
            {
                PEVM.List_Food_DET = DataRowToObject.CreateListFromTable<CYGNUS_Food_Expense_DET>(DS.Tables[1]);
                PEVM.Food_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Food_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Food_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 10)
            {
                PEVM.List_Detension_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Detention_Expense_DET>(DS.Tables[1]);
                PEVM.Detension_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Detention_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Detension_Expense_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 11)
            {
                PEVM.List_Handling_Expence_DET = DataRowToObject.CreateListFromTable<CYGNUS_HandlingLoading_Unloading_Expence_DET>(DS.Tables[1]);
                PEVM.Handling_Expence_HDR = DataRowToObject.CreateListFromTable<CYGNUS_HandlingLoading_Unloading_Expence_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Handling_Expence_HDR.EmpCode;
            }
            else if (BMVW.ExpenceType == 12)
            {
                PEVM.List_Tripsheet_DET = DataRowToObject.CreateListFromTable<CYGNUS_Tripsheet_Disputed_Amount_DET>(DS.Tables[1]);
                PEVM.Tripsheet_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Tripsheet_Disputed_Amount_HDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.Tripsheet_HDR.EmpCode;
            }
            else
            {
                PEVM.ListCPEE_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(DS.Tables[1]);
                PEVM.CPEE_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(DS.Tables[0]).FirstOrDefault();
                EmpCode = PEVM.CPEE_HDR.EmpCode;
            }

            if (PEVM.CPEE_HDR == null)
                PEVM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();

            #endregion

            Webx_Account_Details OBjWAD = new Webx_Account_Details();
            OBjWAD.SRNo = 1;
            OBjWAD.Vouchertype = "";
            PEVM.WAD.SRNo = 1;
            PEVM.WAD.ServiceTax = HdnServiceTaxRate;
            PEVM.HdnEduCessRate = HdnEduCessRate;
            PEVM.HdnHEduCessRate = HdnHEduCessRate;
            PEVM.HdnServiceTaxRate = HdnServiceTaxRate;

            DataTable DT_ISBank_Details = new DataTable();
            DT_ISBank_Details = BAS.Get_ISBank_Details_Available(BaseLocationCode, BaseUserName, BMVW.ExpenceType, EmpCode, 1, 0, 0, "");
            ViewBag.IS_EMP_BankDetails = DT_ISBank_Details.Rows[0][0].ToString();

            List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
            ListWAD1.Add(OBjWAD);
            PEVM.WADListing = ListWAD1;
            PEVM.List_BR_Adv = BAS.Get_BR_AdvanceList(BillNo, BMVW.ExpenceType.ToString(), BaseLocationCode, BaseUserName);
            DataTable DT_Get_Name = new DataTable();
            DT_Get_Name = BAS.Get_BranchAccounting_Title(BaseLocationCode, BaseUserName, BMVW.ExpenceType, "1,2,3", BaseYearVal);
            ViewBag.Title = DT_Get_Name.Rows[0]["BR_Request_Table_Name"].ToString();
            ViewBag.PaymentTitle = DT_Get_Name.Rows[0]["BR_Payment_Table_Name"].ToString();
            BR_ViewName = DT_Get_Name.Rows[0]["BR_ViewName"].ToString();

            return View(BR_ViewName, PEVM);

            #region

            #endregion
        }

        #endregion

        #region Branch Accouting Payment Submit Process

        public ActionResult Accounting_Payment_Submit(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET, List<Webx_Account_Details> PAYMENTMODE, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET,
             List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET, List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET)
        {
            string BillNo = "", IDS = "";

            try
            {

                #region Get IDs

                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        if (IDS == "")
                            IDS = item.ID.ToString();
                        else
                            IDS = IDS + "," + item.ID;
                    }
                }
                else
                {
                    #region Travelling Expances

                    if (FAREDET != null)
                    {
                        foreach (var item in FAREDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (ConveyanceDET != null)
                    {
                        foreach (var item in ConveyanceDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (Bo_Lo_DET != null)
                    {
                        foreach (var item in Bo_Lo_DET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (FoodDET != null)
                    {
                        foreach (var item in FoodDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (OtherDET != null)
                    {
                        foreach (var item in OtherDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }

                    #endregion
                }

                #endregion

                string Year = BaseFinYear.ToString();
                string fin_year = "", PBOV_TYP = "";
                string Financial_Year = BaseYearVal;
                fin_year = Financial_Year;
                string EntryBy = BaseUserName.ToUpper();
                string Transtype = "";
                string Opertitile = "", Voucherno = "";

                Webx_Account_Details ObjWAD = new Webx_Account_Details();

                #region Validations

                if (PAYMENTMODE == null)
                {
                    @ViewBag.StrError = "This Transaction was Some Issue. Please Check.";
                    return View("Error");
                }

                if (PAYMENTMODE != null)
                    ObjWAD = PAYMENTMODE.First();

                if (ObjWAD.PaymentMode.ToUpper() != "CASH")
                {
                    DataTable DT = OS.Duplicate_ChqNO(ObjWAD.ChequeNo, ObjWAD.Chequedate.ToString("dd MMM yyyy"));
                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }
                if (ObjWAD.PaymentMode.ToUpper() != "BANK" && ObjWAD.PaymentMode.ToUpper() == "PAYMENT")
                {
                    string FIN_Start = "", Curr_Year = "", Finyear = "";
                    fin_year = BaseFinYear.ToString();
                    double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
                    Curr_Year = DateTime.Now.ToString("yyyy");
                    Finyear = BaseFinYear.ToString();
                    if (Finyear == Curr_Year)
                        FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
                    else
                        FIN_Start = "01 Apr " + Financial_Year;

                    DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, System.DateTime.Now.ToString("dd MMM yyyy"), FIN_Start, fin_year, ObjWAD.NETAMOUNT.ToString());

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                }

                //CYGNUS_Branch_Accounting_Master ObjCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode).Where(c => c.ID == PEVM.Type).First();
                Opertitile = "Branch Accouting"/*ObjCBAM.ChargeName*/;
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH PAYMENT";
                }

                #endregion

                #region Get PBOV_code and PBOV_Name

                string PBOV_code = "", PBOV_Name = "";

                if (PEVM.Type2 == 1)
                {
                    PBOV_code = PEVM.Elec_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Elec_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 2)
                {
                    PBOV_code = PEVM.Conveyance_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Conveyance_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 3)
                {
                    PBOV_code = PEVM.Trave_Tour_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Trave_Tour_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 4)
                {
                    PBOV_code = PEVM.Driver_TSP_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Driver_TSP_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 5)
                {
                    PBOV_code = PEVM.CPEE_HDR.EmpCode;
                    PBOV_Name = PEVM.CPEE_HDR.EmpName;
                }
                else if (PEVM.Type2 == 6)
                {
                    PBOV_code = PEVM.MarketVehicleCharges_Expence_HDR.EmpCode;
                    PBOV_Name = PEVM.MarketVehicleCharges_Expence_HDR.EmpName;
                }
                else if (PEVM.Type2 == 7)
                {
                    PBOV_code = PEVM.Office_Fixed_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Office_Fixed_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 8)
                {
                    PBOV_code = PEVM.Office_Other_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Office_Other_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 9)
                {
                    PBOV_code = PEVM.Food_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Food_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 10)
                {
                    PBOV_code = PEVM.Detension_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Detension_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 11)
                {
                    PBOV_code = PEVM.Handling_Expence_HDR.EmpCode;
                    PBOV_Name = PEVM.Handling_Expence_HDR.EmpName;
                }
                else if (PEVM.Type2 == 12)
                {
                    PBOV_code = PEVM.Tripsheet_HDR.EmpCode;
                    PBOV_Name = PEVM.Tripsheet_HDR.EmpName;
                }
                else
                {
                    ViewBag.StrError = "Please select Proper Employee.";
                    return View("Error");
                }

                #endregion

                string svrcheck = "N", tdscheck = "N", DepoFlag = "N", Str_Onaccount = "N";
                string BankCode = "";
                double adjustamt = 0;
                string TdsCalForLedgerAmt = "N";

                if (PEVM.WAD.IsServicetaxapply)
                {
                    svrcheck = "Y";
                }
                if (PEVM.WAD.IsTDStaxapply)
                {
                    tdscheck = "Y";
                }
                if (PEVM.WAD.IsTDSWithoutSvcTax)
                {
                    TdsCalForLedgerAmt = "Y";
                }

                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    //BankCode = "ACA0002";

                    //if (ObjWAD.Deposited)
                    //{
                    DepoFlag = "Y";
                    BankCode = ObjWAD.DepositedInBank;
                    // }
                    adjustamt = Convert.ToDouble(ObjWAD.ChequeAmount);
                    if (ObjWAD.IsonAccount)
                    {
                        Str_Onaccount = "Y";
                        adjustamt = 0;
                    }
                }

                string CompanyCode = "", Acccode = "", Accdesc = "", Debit = "";
                string Credit = "", Narration = "", Stax_App = "N";

                string CostCenterId = "";

                string hdnroundoff = "N";
                string hdneditablsvctaxrate = "N";
                string HdnCompanyDropDownRule = "N";
                string HdnRowwiseSvcTaxRule = "N";
                string HdnTransactionWiseAccountRule = "N";
                string HdnCostCenterRule = "N";

                hdnroundoff = OS.ManualVouchersRule("RoundOff");
                hdneditablsvctaxrate = OS.ManualVouchersRule("EditableServiceTax");
                HdnCompanyDropDownRule = OS.ManualVouchersRule("CompanyDropDown");
                HdnRowwiseSvcTaxRule = OS.ManualVouchersRule("RowwiseSvcTax");
                HdnTransactionWiseAccountRule = OS.ManualVouchersRule("Transwiserule");
                HdnCostCenterRule = OS.ManualVouchersRule("CostCenterRule");

                string Xml_Acccode_Details = "<root>";

                if (CPEEDET != null || PEVM.Type2 == 3)
                {
                    CYGNUS_Branch_Accounting_Master ObjCBSM = new CYGNUS_Branch_Accounting_Master();

                    #region GET BillNo And Expance Account Code

                    if (PEVM.Type2 == 1)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Elec_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Elec_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 2)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Conveyance_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Conveyance_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 3)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Trave_Tour_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Trave_Tour_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 4)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Driver_TSP_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 5)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.CPEE_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.CPEE_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 6)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.MarketVehicleCharges_Expence_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.MarketVehicleCharges_Expence_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 7)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Office_Fixed_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Office_Fixed_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 8)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Office_Other_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Office_Other_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 9)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Food_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Food_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 10)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Detension_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Detension_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 11)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Handling_Expence_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Handling_Expence_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 12)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Tripsheet_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Tripsheet_HDR.BillNo;
                    }
                    else
                    {
                        ViewBag.StrError = "Proper Bill not available Please select Proper Bill.";
                        return View("Error");
                    }
                    CompanyCode = ObjCBSM.Accode;
                    string ChargeName = ObjCBSM.ChargeName;

                    #endregion

                    //if (HdnCostCenterRule == "Y")
                    //{
                    //}

                    //if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                    //{
                    //    Debit = ObjWAD.AmountApplicable.ToString();
                    //    Credit = "0.00";
                    //}
                    //else
                    //{
                    Credit = "0.00";
                    Debit = PEVM.WAD.AmountApplicable.ToString();
                    //}

                    Narration = "BEING AMOUNT PAID IN " + ObjWAD.PaymentMode.ToUpper() + " AGAINST " + ChargeName.ToUpper() + " FOR BR NO." + BillNo.ToUpper();
                    //Description = "";

                    bool chkstax = false; ;

                    if (HdnRowwiseSvcTaxRule == "Y")
                    {
                        if (chkstax)
                            Stax_App = "Y";
                        else
                            Stax_App = "N";
                    }

                    if (CompanyCode != "")
                    {
                        DataTable dt = OS.FindOutSystemAccountCode(CompanyCode, BaseLocationCode);
                        if (dt.Rows.Count == 0)
                        {
                            ViewBag.StrError = "Error : Not Valid Account Code : " + BaseCompanyCode + ". So Please Enter The Valid Account Details";
                            return View("Error");
                        }
                        Acccode = dt.Rows[0]["Acccode"].ToString().Trim();
                        Accdesc = dt.Rows[0]["Accdesc"].ToString().Trim();
                        Accdesc = Accdesc.Replace("<", "&lt;");
                        Accdesc = Accdesc.Replace(">", "&gt;");
                        Accdesc = Accdesc.Replace("\"", "&quot;");
                        Accdesc = Accdesc.Replace("'", "&apos;");

                        if (Str_Onaccount == "Y" && Acccode != "CDA0001")
                        {
                            ViewBag.StrError = "Error : On Account Cheque Facility is not Availble for : " + Accdesc + ". This Facility Available only for ledger : Billed Debtors";
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Acccode + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Stax_App>" + Stax_App + "</Stax_App>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenterId>" + CostCenterId + "</CostCenterId>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenter>" + BillNo + "</CostCenter>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                    else
                    {
                        ViewBag.StrError = "AproPriate Account not Found..";
                        return View("Error");
                    }
                }
                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                string Company_code = "";
                Company_code = BaseCompanyCode;
                string CommonCostCenter = "N";
                string BranchCode = "";

                BranchCode = BaseLocationCode;

                string Transdate = System.DateTime.Now.ToString("dd MMM yyyy");
                if (BaseYearValFirst == "2016")
                {
                    Transdate = "31 Mar 2017";
                }

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Transdate + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BranchCode + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.ToUpper() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString() + "</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + PEVM.WAD.ManualNo + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + PEVM.WAD.BusinessDivision + "</BusinessType>";
                Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
                Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + BaseLocationCode/*PEVM.WAD.AccountingLocation*/ + "</Acclocation>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + PEVM.WAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + PEVM.WAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + Narration/*PEVM.WAD.Narration*/ + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<Panno>" + PEVM.WAD.PANNumber + "</Panno>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + PEVM.WAD.ServiceTaxRegNo + "</Svrtaxno>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ObjWAD.ChequeNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqamt>" + ObjWAD.ChequeAmount + "</chqamt>";
                Xml_Other_Details = Xml_Other_Details + "<chqdt>" + Convert.ToDateTime(ObjWAD.Chequedate).ToString("dd MMM yyyy") + "</chqdt>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + PBOV_TYP + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + svrcheck + "</Svrtax_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + tdscheck + "</Tds_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + PEVM.WAD.TDSSection + "</Tds_Acccode>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + PEVM.WAD.TDSRate + "</Tds_rate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsAmount>" + PEVM.WAD.TDSAmount + "</TdsAmount>";
                Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ObjWAD.ReceivedFromBank + "</recbanknm>";
                Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + Str_Onaccount + "</Onaccount_YN>";
                Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag + "</Deposited>";
                Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ObjWAD.PaymentMode + "</Paymode>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + PEVM.WAD.ServiceTaxRate /*HdnServiceTaxRate*/ + "</SvrTaxRate>";
                Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + PEVM.WAD.EducationCess /*HdnEduCessRate*/ + "</EduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + PEVM.WAD.HEducationCess /*HdnHEduCessRate*/ + "</HEduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsOption>" + TdsCalForLedgerAmt + "</TdsOption>";
                Xml_Other_Details = Xml_Other_Details + "<CommonCostCenter>" + CommonCostCenter + "</CommonCostCenter>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + Company_code + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                try
                {
                    Voucherno = BAS.Insert_Branch_Voucher_Details(Xml_Acccode_Details, Xml_Other_Details, IDS, PEVM.Type2);
                    return RedirectToAction("Accounting_Payment_Done", new { Type = PEVM.Type2, No = Voucherno, BillNo = BillNo });
                }
                catch (Exception ex)
                {
                    @ViewBag.StrError = ex.Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Accounting_Payment_Submit_New_Process(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET, List<Webx_Account_Details> PAYMENTMODE, List<CYGNUS_Branch_Travelling_Tour_DET> FAREDET,
            List<CYGNUS_Branch_Travelling_Tour_DET> ConveyanceDET, List<CYGNUS_Branch_Travelling_Tour_DET> Bo_Lo_DET, List<CYGNUS_Branch_Travelling_Tour_DET> FoodDET, List<CYGNUS_Branch_Travelling_Tour_DET> OtherDET)
        {
            string BillNo = "", IDS = "";

            try
            {

                #region Get IDs

                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        if (IDS == "")
                            IDS = item.ID.ToString();
                        else
                            IDS = IDS + "," + item.ID;
                    }
                }
                else
                {
                    #region Travelling Expances

                    if (FAREDET != null)
                    {
                        foreach (var item in FAREDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (ConveyanceDET != null)
                    {
                        foreach (var item in ConveyanceDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (Bo_Lo_DET != null)
                    {
                        foreach (var item in Bo_Lo_DET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (FoodDET != null)
                    {
                        foreach (var item in FoodDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }
                    if (OtherDET != null)
                    {
                        foreach (var item in OtherDET)
                        {
                            if (IDS == "")
                                IDS = item.ID.ToString();
                            else
                                IDS = IDS + "," + item.ID;
                        }
                    }

                    #endregion
                }

                #endregion

                string Year = BaseFinYear.ToString();
                string fin_year = "", PBOV_TYP = "";
                string Financial_Year = BaseYearVal;
                fin_year = Financial_Year;
                string EntryBy = BaseUserName.ToUpper();
                string Transtype = "";
                string Opertitile = "", Voucherno = "";

                Webx_Account_Details ObjWAD = new Webx_Account_Details();

                #region Validations

                if (PAYMENTMODE == null)
                {
                    @ViewBag.StrError = "This Transaction was Some Issue. Please Check.";
                    return View("Error");
                }

                if (PAYMENTMODE != null)
                    ObjWAD = PAYMENTMODE.First();

                if (ObjWAD.PaymentMode.ToUpper() != "CASH" && ObjWAD.PaymentMode.ToUpper() != "CONTRA" && ObjWAD.PaymentMode.ToUpper() != "OTHER")
                {
                    DataTable DT = OS.Duplicate_ChqNO(ObjWAD.ChequeNo, ObjWAD.Chequedate.ToString("dd MMM yyyy"));
                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }
                if (ObjWAD.PaymentMode.ToUpper() != "BANK" && ObjWAD.PaymentMode.ToUpper() != "CONTRA" && ObjWAD.PaymentMode.ToUpper() != "OTHER")
                {
                    //string FIN_Start = "", Curr_Year = "", Finyear = "";
                    //fin_year = BaseFinYear.ToString();
                    //double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
                    //Curr_Year = DateTime.Now.ToString("yyyy");
                    //Finyear = BaseFinYear.ToString();
                    //if (Finyear == Curr_Year)
                    //    FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
                    //else
                    //    FIN_Start = "01 Apr " + Financial_Year;

                    DataTable DT = OS.DR_VR_Trn_Halt(BaseLocationCode, GF.FormateDate(System.DateTime.Now), GF.FormateDate(Convert.ToDateTime(FinYearStartDate)), BaseYearVal, ObjWAD.NETAMOUNT.ToString());

                    string Cnt = DT.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                        return View("Error");
                    }
                }

                //CYGNUS_Branch_Accounting_Master ObjCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode).Where(c => c.ID == PEVM.Type).First();
                Opertitile = "Branch Accouting"/*ObjCBAM.ChargeName*/;
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "CONTRA")
                {
                    Transtype = "CONTRA PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "OTHER")
                {
                    Transtype = "Conveyance Clear PAYMENT";
                }

                #endregion

                #region Get PBOV_code and PBOV_Name

                string PBOV_code = "", PBOV_Name = "", BRGenerationBranchCode = "";

                if (PEVM.Type2 == 1)
                {
                    PBOV_code = PEVM.Elec_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Elec_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 2)
                {
                    PBOV_code = PEVM.Conveyance_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Conveyance_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 3)
                {
                    PBOV_code = PEVM.Trave_Tour_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Trave_Tour_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 4)
                {
                    BRGenerationBranchCode = PEVM.Driver_TSP_Expense_HDR.Brcd;
                    PBOV_Name = PEVM.NameofDriver;
                }
                else if (PEVM.Type2 == 5)
                {
                    PBOV_code = PEVM.CPEE_HDR.EmpCode;
                    PBOV_Name = PEVM.CPEE_HDR.EmpName;
                }
                else if (PEVM.Type2 == 6)
                {
                    PBOV_code = PEVM.MarketVehicleCharges_Expence_HDR.EmpCode;
                    PBOV_Name = PEVM.MarketVehicleCharges_Expence_HDR.EmpName;
                }
                else if (PEVM.Type2 == 7)
                {
                    PBOV_code = PEVM.Office_Fixed_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Office_Fixed_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 8)
                {
                    PBOV_code = PEVM.Office_Other_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Office_Other_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 9)
                {
                    PBOV_code = PEVM.Food_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Food_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 10)
                {
                    PBOV_code = PEVM.Detension_Expense_HDR.EmpCode;
                    PBOV_Name = PEVM.Detension_Expense_HDR.EmpName;
                }
                else if (PEVM.Type2 == 11)
                {
                    PBOV_code = PEVM.Handling_Expence_HDR.EmpCode;
                    PBOV_Name = PEVM.Handling_Expence_HDR.EmpName;
                }
                else if (PEVM.Type2 == 12)
                {
                    PBOV_code = PEVM.Tripsheet_HDR.EmpCode;
                    PBOV_Name = PEVM.Tripsheet_HDR.EmpName;
                }
                else
                {
                    ViewBag.StrError = "Please select Proper Employee.";
                    return View("Error");
                }

                #endregion

                string svrcheck = "N", tdscheck = "N", DepoFlag = "N", Str_Onaccount = "N";
                string BankCode = "";
                double adjustamt = 0;
                string TdsCalForLedgerAmt = "N";

                //if (PEVM.WAD.IsServicetaxapply)
                //{
                //    svrcheck = "Y";
                //}
                //if (PEVM.WAD.IsTDStaxapply)
                //{
                //    tdscheck = "Y";
                //}
                //if (PEVM.WAD.IsTDSWithoutSvcTax)
                //{
                //    TdsCalForLedgerAmt = "Y";
                //}

                if (ObjWAD.PaymentMode.ToUpper() != "Cash" && ObjWAD.PaymentMode.ToUpper() != "CONTRA" && ObjWAD.PaymentMode.ToUpper() == "OTHER")
                {
                    //BankCode = "ACA0002";

                    //if (ObjWAD.Deposited)
                    //{
                    DepoFlag = "Y";
                    BankCode = ObjWAD.DepositedInBank;
                    // }
                    adjustamt = Convert.ToDouble(ObjWAD.ChequeAmount);
                    if (ObjWAD.IsonAccount)
                    {
                        Str_Onaccount = "Y";
                        adjustamt = 0;
                    }
                }

                string CompanyCode = "", Acccode = "", Accdesc = "", Debit = "";
                string Credit = "", Narration = "", Stax_App = "N";

                string CostCenterId = "";

                string hdnroundoff = "N";
                string hdneditablsvctaxrate = "N";
                string HdnCompanyDropDownRule = "N";
                string HdnRowwiseSvcTaxRule = "N";
                string HdnTransactionWiseAccountRule = "N";
                string HdnCostCenterRule = "N";

                hdnroundoff = OS.ManualVouchersRule("RoundOff");
                hdneditablsvctaxrate = OS.ManualVouchersRule("EditableServiceTax");
                HdnCompanyDropDownRule = OS.ManualVouchersRule("CompanyDropDown");
                HdnRowwiseSvcTaxRule = OS.ManualVouchersRule("RowwiseSvcTax");
                HdnTransactionWiseAccountRule = OS.ManualVouchersRule("Transwiserule");
                HdnCostCenterRule = OS.ManualVouchersRule("CostCenterRule");

                string Xml_Acccode_Details = "<root>";

                if (CPEEDET != null || PEVM.Type2 == 3)
                {
                    CYGNUS_Branch_Accounting_Master ObjCBSM = new CYGNUS_Branch_Accounting_Master();

                    #region GET BillNo And Expance Account Code

                    if (PEVM.Type2 == 1)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Elec_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Elec_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 2)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Conveyance_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Conveyance_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 3)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Trave_Tour_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Trave_Tour_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 4)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Driver_TSP_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Driver_TSP_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 5)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.CPEE_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.CPEE_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 6)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.MarketVehicleCharges_Expence_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.MarketVehicleCharges_Expence_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 7)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Office_Fixed_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Office_Fixed_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 8)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Office_Other_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Office_Other_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 9)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Food_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Food_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 10)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Detension_Expense_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Detension_Expense_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 11)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Handling_Expence_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Handling_Expence_HDR.BillNo;
                    }
                    else if (PEVM.Type2 == 12)
                    {
                        ObjCBSM = BAS.GetBRCDAcc_FormBill(PEVM.Tripsheet_HDR.BillNo, PEVM.Type2, BaseLocationCode).First();
                        BillNo = PEVM.Tripsheet_HDR.BillNo;
                    }
                    else
                    {
                        ViewBag.StrError = "Proper Bill not available Please select Proper Bill.";
                        return View("Error");
                    }
                    CompanyCode = ObjCBSM.Accode;
                    string ChargeName = ObjCBSM.ChargeName;

                    #endregion

                    //if (HdnCostCenterRule == "Y")
                    //{
                    //}

                    //if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                    //{
                    //    Debit = ObjWAD.AmountApplicable.ToString();
                    //    Credit = "0.00";
                    //}
                    //else
                    //{
                    Credit = "0.00";
                    Debit = ObjWAD.AmountApplicable.ToString();
                    //}

                    if (PEVM.Type2 == 3)
                    {
                        Narration = "BEING AMOUNT PAID IN " + PBOV_Name + " AGAINST " + ChargeName.ToUpper() + " FOR BR NO" + BillNo.ToUpper() + "for conveyance expenses in the" + System.DateTime.Now.Month + "Month";

                    }
                    else
                    {
                        Narration = "BEING AMOUNT PAID IN " + ObjWAD.PaymentMode.ToUpper() + " AGAINST " + ChargeName.ToUpper() + " FOR BR NO." + BillNo.ToUpper();
                    }

                    //Narration = "BEING AMOUNT PAID IN " + ObjWAD.PaymentMode.ToUpper() + " AGAINST " + ChargeName.ToUpper() + " FOR BR NO." + BillNo.ToUpper();
                    //Description = "";

                    bool chkstax = false; ;

                    if (HdnRowwiseSvcTaxRule == "Y")
                    {
                        if (chkstax)
                            Stax_App = "Y";
                        else
                            Stax_App = "N";
                    }

                    if (CompanyCode != "")
                    {
                        DataTable dt = OS.FindOutSystemAccountCode(CompanyCode, BaseLocationCode);
                        if (dt.Rows.Count == 0)
                        {
                            ViewBag.StrError = "Error : Not Valid Account Code : " + BaseCompanyCode + ". So Please Enter The Valid Account Details";
                            return View("Error");
                        }
                        Acccode = dt.Rows[0]["Acccode"].ToString().Trim();
                        Accdesc = dt.Rows[0]["Accdesc"].ToString().Trim();
                        Accdesc = Accdesc.Replace("<", "&lt;");
                        Accdesc = Accdesc.Replace(">", "&gt;");
                        Accdesc = Accdesc.Replace("\"", "&quot;");
                        Accdesc = Accdesc.Replace("'", "&apos;");

                        if (Str_Onaccount == "Y" && Acccode != "CDA0001")
                        {
                            ViewBag.StrError = "Error : On Account Cheque Facility is not Availble for : " + Accdesc + ". This Facility Available only for ledger : Billed Debtors";
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Acccode + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Stax_App>" + Stax_App + "</Stax_App>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenterId>" + CostCenterId + "</CostCenterId>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenter>" + BillNo + "</CostCenter>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                    else
                    {
                        ViewBag.StrError = "AproPriate Account not Found..";
                        return View("Error");
                    }
                }
                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                string Company_code = "";
                Company_code = BaseCompanyCode;
                string CommonCostCenter = "N";
                string BranchCode = "";

                BranchCode = BaseLocationCode;

                string Transdate = System.DateTime.Now.ToString("dd MMM yyyy");
                if (BaseYearValFirst == "2016")
                {
                    Transdate = "31 Mar 2017";
                }

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Transdate + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BranchCode + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.ToUpper() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString() + "</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>NA</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ObjWAD.BusinessDivision + "</BusinessType>";
                Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
                Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + BaseLocationCode/*PEVM.WAD.AccountingLocation*/ + "</Acclocation>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ObjWAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ObjWAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + Narration/*PEVM.WAD.Narration*/ + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<Panno></Panno>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ObjWAD.ServiceTaxRegNo + "</Svrtaxno>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ObjWAD.ChequeNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqamt>" + ObjWAD.ChequeAmount + "</chqamt>";
                if (ObjWAD.PaymentMode.ToUpper() == "CASH" && ObjWAD.PaymentMode.ToUpper() == "CONTRA" && ObjWAD.PaymentMode.ToUpper() == "OTHER")
                {
                    Xml_Other_Details = Xml_Other_Details + "<chqdt>" + "01 Jan 1990" + "</chqdt>";
                }
                else
                {
                    Xml_Other_Details = Xml_Other_Details + "<chqdt>" + Convert.ToDateTime(ObjWAD.Chequedate).ToString("dd MMM yyyy") + "</chqdt>";
                }
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + PBOV_TYP + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + svrcheck + "</Svrtax_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + tdscheck + "</Tds_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + ObjWAD.TDSSection + "</Tds_Acccode>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + ObjWAD.TDSRate + "</Tds_rate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsAmount>" + ObjWAD.TDSAmount + "</TdsAmount>";
                Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ObjWAD.ReceivedFromBank + "</recbanknm>";
                Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + Str_Onaccount + "</Onaccount_YN>";
                Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag + "</Deposited>";
                Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ObjWAD.PaymentMode + "</Paymode>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ObjWAD.ServiceTaxRate /*HdnServiceTaxRate*/ + "</SvrTaxRate>";
                Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ObjWAD.EducationCess /*HdnEduCessRate*/ + "</EduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ObjWAD.HEducationCess /*HdnHEduCessRate*/ + "</HEduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<TdsOption>" + TdsCalForLedgerAmt + "</TdsOption>";
                Xml_Other_Details = Xml_Other_Details + "<CommonCostCenter>" + CommonCostCenter + "</CommonCostCenter>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + Company_code + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                try
                {
                    Voucherno = BAS.Insert_Branch_Voucher_Details(Xml_Acccode_Details, Xml_Other_Details, IDS, PEVM.Type2);
                    return RedirectToAction("Accounting_Payment_Done", new { Type = PEVM.Type2, No = Voucherno, BillNo = BillNo });
                }
                catch (Exception ex)
                {
                    @ViewBag.StrError = ex.Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult Accounting_Payment_Done(int Type, string No, string BillNo)
        {
            try
            {
                ViewBag.No = No;
                ViewBag.BillNo = BillNo;
                ViewBag.Type = Type;
                return View();
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #endregion

        #region Expenses Entry

        public ActionResult Expenses(BranchAccountingMaster BMVW)
        {
            try
            {
                string BR_ViewName = "";
                DataTable DT_Get_Name = new DataTable();
                DT_Get_Name = BAS.Get_BranchAccounting_Title(BaseLocationCode, BaseUserName, BMVW.ExpenceType, "1,2,3", BaseYearVal);
                ViewBag.Title = DT_Get_Name.Rows[0]["BR_Request_Table_Name"].ToString();
                BR_ViewName = DT_Get_Name.Rows[0]["BR_ViewName"].ToString();

                if (BMVW.ExpenceType == 1 || BMVW.ExpenceType == 2 || BMVW.ExpenceType == 3 || BMVW.ExpenceType == 7 || BMVW.ExpenceType == 8 || BMVW.ExpenceType == 9 || BMVW.ExpenceType == 10 || BMVW.ExpenceType == 12)
                {
                    return RedirectToAction(BR_ViewName);
                }
                else
                {
                    return RedirectToAction(BR_ViewName, new { Type = BMVW.ExpenceType, No = BMVW.DocumentNo });
                }

                #region

                #endregion
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult ExpensesDone(string No, int Type)
        {
            ViewBag.No = No;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult Getexpensebill()
        {
            try
            {
                BranchAccountingMaster BMVW = new BranchAccountingMaster();
                BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode);
                return View(BMVW);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _Add_Expenses_Details(int id, int Type, int Subtype)
        {
            try
            {
                CYGNUS_Branch_Expenses_EntryDET ObjCBEE = new CYGNUS_Branch_Expenses_EntryDET();
                ObjCBEE.ID = id;
                if (Type == 1)
                {
                    return PartialView("_Electriciy_Expenses_Partial", ObjCBEE);
                }
                else if (Type == 2)
                {
                    CYGNUS_Conveyance_Expense_DET CCED = new CYGNUS_Conveyance_Expense_DET();
                    CCED.ID = id;
                    CCED.Date = System.DateTime.Now;
                    CCED.Subtype = Subtype;
                    return PartialView("_Conveyance_Local_Expenses_Partial", CCED);
                }
                else if (Type == 9)
                {
                    CYGNUS_Food_Expense_DET CCED = new CYGNUS_Food_Expense_DET();
                    CCED.ID = id;
                    CCED.Date = System.DateTime.Now;
                    ViewBag.SubType = 1;
                    CCED.Subtype = Subtype;
                    return PartialView("_Food_Expenses_Partial", CCED);
                }
                else
                    return PartialView("", ObjCBEE);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public JsonResult Get_Travelling_Mode_Class_(string Type)
        {
            var ListTrain = new List<SelectListItem>
                       {
                       new SelectListItem{Text="1st AC",Value="1st AC"},
                       new SelectListItem{Text="2st AC",Value="2st AC"},
                       new SelectListItem{Text="3st AC",Value="3st AC"},
                       new SelectListItem{Text="SLEEPAR CLASS",Value="SLEEPAR CLASS"},
                       new SelectListItem{Text="CHAIR CAR",Value="CHAIR CAR"},
                       new SelectListItem{Text="2nd SEETING",Value="2nd SEETING"},
                       };

            var ListBus = new List<SelectListItem>
                       {
                       new SelectListItem{Text="GENERAL",Value="GENERAL"},
                       new SelectListItem{Text="AC",Value="AC"},                       
                       new SelectListItem{Text="TAXI",Value="TAXI"},
                       new SelectListItem{Text="BUS",Value="BUS"}
                       };

            var ListAir = new List<SelectListItem>
                       {
                       new SelectListItem{Text="BUSINESS",Value="BUSINESS"},
                       new SelectListItem{Text="ECONOMY",Value="ECONOMY"},
                       };

            if (Type == "R")
            {
                return Json(ListTrain, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "S")
            {
                return Json(ListBus, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(ListAir, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Accoting Aprrovel Rejection

        public ActionResult BRCDAcc_APP_RejectCriteria()
        {
            if (BaseLocationLevel == 1)
            {
                try
                {
                    BranchAccountingMaster BMVW = new BranchAccountingMaster();
                    string basefinyear = BaseFinYear.Replace("20", "").Replace("-", "_");
                    List<CYGNUS_Branch_Expance> optrkList = BAS.GET_BR_Dashbord_Report_Approval(BaseLocationCode);
                    DataTable DT = BAS.BRStatusSummaryCashOnToday(BaseLocationCode, GF.FormateDate(System.DateTime.Now), GF.FormateDate(System.DateTime.Now), basefinyear, 0);
                    BMVW.ListCBAM = BAS.GetBRCDAccList(BaseUserName.ToUpper(), BaseLocationCode);
                    BMVW.SubType = 2;
                    BMVW.ApprovePaidType = 1;
                    BMVW.List_BRS = optrkList;
                    BMVW.FromDate = System.DateTime.Now;
                    BMVW.ToDate = System.DateTime.Now;
                    foreach (var item in optrkList)
                    {
                        item.type = BMVW.ApprovePaidType;
                    }
                    decimal NetAmt = Convert.ToDecimal(DT.Rows[0][2].ToString());
                    BMVW.List_BRS = optrkList;
                    CYGNUS_Branch_Expance BRSAmt = new CYGNUS_Branch_Expance();
                    BRSAmt.NetAmt = NetAmt;
                    BMVW.BRS = BRSAmt;
                    @ViewBag.ActionName = "AccotingApproval_Reject";
                    return View(BMVW);
                }
                catch (Exception ex)
                {
                    @ViewBag.StrError = ex.Message;
                    return View("Error");
                }
            }
            else
            {
                return RedirectToAction("URLRedirect", "Home");
            }
        }

        public ActionResult AccotingApproval(DateTime FromDate, DateTime ToDate, string Location, string ExpenceType, int Type, string Value)
        {
            ExpensesEntryModule PEEM = new ExpensesEntryModule();
            PEEM.ListCPEE_HDR = new List<CYGNUS_Branch_Expenses_EntryHDR>();
            PEEM.ListCPEE_HDR = BAS.GetBrancAcct_Aprroval_List(FromDate.ToString("dd MMM yyyy"), ToDate.ToString("dd MMM yyyy"), BaseUserName.ToUpper(), ExpenceType.ToString(), Location, BaseLocationCode);
            if (Type == 1)
            {
                PEEM.ListCPEE_HDR = PEEM.ListCPEE_HDR.Where(c => c.EmpCode == Value).ToList();
            }
            else if (Type == 2)
            {
                PEEM.ListCPEE_HDR = PEEM.ListCPEE_HDR.Where(c => c.Labour == Value).ToList();
            }
            else if (Type == 3)
            {
                PEEM.ListCPEE_HDR = PEEM.ListCPEE_HDR.Where(c => c.NameOfVendor == Value).ToList();
            }
            else if (Type == 4)
            {
                PEEM.ListCPEE_HDR = PEEM.ListCPEE_HDR.Where(c => c.Driver == Value).ToList();
            }
            return PartialView("_AccotingApproval", PEEM);
        }

        public ActionResult AccotingApproval_Reject(BranchAccountingMaster BMVW, List<CYGNUS_Branch_Expenses_EntryHDR> CPEEApprov)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();

                DataTable DT_Tax = OS.GetServiceTaxRate();
                decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
                decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
                decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());

                PEVM.Type2 = BMVW.ExpenceType;
                PEVM.SubType = 2;
                ViewBag.Title = "Head Office Accoting Approval/Reject Module";
                ViewBag.Type = BMVW.ExpenceType;
                ViewBag.SubType = 2;
                ViewBag.ActionName = "Accounting_Approval";

                string Nos = "", BR_ViewName = "";

                PEVM.WAD = new Webx_Account_Details();

                PEVM.WAD = new Webx_Account_Details();
                Nos = BMVW.Id.ToString();

                #region Get Expance Data

                DataSet DS = BAS.GetBrancAcct_Aprroval_Reject(Nos, BaseLocationCode, BaseUserName.ToUpper(), BMVW.ExpenceType);

                if (BMVW.ExpenceType == 1)
                {
                    PEVM.List_Elec_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Electricity_Expenses_DET>(DS.Tables[1]);
                    PEVM.Elec_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Electricity_Expenses_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 2)
                {
                    PEVM.List_Conveyance_DET = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_DET>(DS.Tables[1]);
                    PEVM.Conveyance_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Conveyance_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 3)
                {
                    #region Trave_Tour

                    PEVM.List_Trave_Tour_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_DET>(DS.Tables[1]);
                    PEVM.Trave_Tour_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Travelling_Tour_HDR>(DS.Tables[0]).FirstOrDefault();

                    PEVM.List_Fare_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "FARE").ToList();
                    PEVM.List_Conveyance_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "CONVEYANCE").ToList();
                    PEVM.List_Bo_Lo_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "BOARDING_LOADING").ToList();
                    PEVM.List_Food_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "FOOD").ToList();
                    PEVM.List_Other_Trave_Tour_Expense_DET = PEVM.List_Trave_Tour_Expense_DET.Where(c => c.SubExpense_Type == "OTHER").ToList();

                    PEVM.Trave_Tour_Expense_DET = new CYGNUS_Branch_Travelling_Tour_DET();
                    PEVM.Trave_Tour_Expense_DET.GrandTotal_SubTotal_Amount = PEVM.List_Trave_Tour_Expense_DET.Sum(c => c.Amount);
                    #endregion
                }
                else if (BMVW.ExpenceType == 4)
                {
                    PEVM.List_Driver_TSP_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_DET>(DS.Tables[1]);
                    PEVM.Driver_TSP_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 5)
                {
                    PEVM.ListCPEE_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(DS.Tables[1]);
                    PEVM.CPEE_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(DS.Tables[0]).FirstOrDefault();

                    if (PEVM.CPEE_HDR.NameOfVendor != null && PEVM.CPEE_HDR.NameOfVendor != "")
                    {
                        PEVM.CPEE_HDR.Display_Name_Of_Vendor = MS.GetVendorObject().Where(c => c.VENDORCODE == PEVM.CPEE_HDR.NameOfVendor).First().VENDORNAME;
                    }
                }
                else if (BMVW.ExpenceType == 6)
                {
                    PEVM.List_MarketVehicle_Expence_DET = DataRowToObject.CreateListFromTable<CYGNUS_MarketVehicleCharges_Expence_DET>(DS.Tables[1]);
                    PEVM.MarketVehicleCharges_Expence_HDR = DataRowToObject.CreateListFromTable<CYGNUS_MarketVehicleCharges_Expence_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 7)
                {
                    PEVM.List_Office_Fixed_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_DET>(DS.Tables[1]);
                    PEVM.Office_Fixed_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Fixed_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 8)
                {
                    PEVM.List_Office_Other_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_DET>(DS.Tables[1]);
                    PEVM.Office_Other_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Office_Expenses_Other_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 9)
                {
                    PEVM.List_Food_DET = DataRowToObject.CreateListFromTable<CYGNUS_Food_Expense_DET>(DS.Tables[1]);
                    PEVM.Food_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Food_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 10)
                {
                    PEVM.List_Detension_Expense_DET = DataRowToObject.CreateListFromTable<CYGNUS_Detention_Expense_DET>(DS.Tables[1]);
                    PEVM.Detension_Expense_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Detention_Expense_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 11)
                {
                    PEVM.List_Handling_Expence_DET = DataRowToObject.CreateListFromTable<CYGNUS_HandlingLoading_Unloading_Expence_DET>(DS.Tables[1]);
                    PEVM.Handling_Expence_HDR = DataRowToObject.CreateListFromTable<CYGNUS_HandlingLoading_Unloading_Expence_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else if (BMVW.ExpenceType == 12)
                {
                    PEVM.List_Tripsheet_DET = DataRowToObject.CreateListFromTable<CYGNUS_Tripsheet_Disputed_Amount_DET>(DS.Tables[1]);
                    PEVM.Tripsheet_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Tripsheet_Disputed_Amount_HDR>(DS.Tables[0]).FirstOrDefault();
                }
                else
                {
                    PEVM.ListCPEE_DET = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryDET>(DS.Tables[1]);
                    PEVM.CPEE_HDR = DataRowToObject.CreateListFromTable<CYGNUS_Branch_Expenses_EntryHDR>(DS.Tables[0]).FirstOrDefault();
                }
                if (PEVM.CPEE_HDR == null)
                    PEVM.CPEE_HDR = new CYGNUS_Branch_Expenses_EntryHDR();

                #endregion

                Webx_Account_Details OBjWAD = new Webx_Account_Details();
                OBjWAD.SRNo = 1;
                OBjWAD.Vouchertype = "";
                PEVM.WAD.SRNo = 1;
                PEVM.WAD.ServiceTax = HdnServiceTaxRate;
                PEVM.HdnEduCessRate = HdnEduCessRate;
                PEVM.HdnHEduCessRate = HdnHEduCessRate;
                PEVM.HdnServiceTaxRate = HdnServiceTaxRate;

                List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                ListWAD1.Add(OBjWAD);
                PEVM.WADListing = ListWAD1;
                if (BMVW.ExpenceType == 3)
                {
                    PEVM.List_BR_Adv = BAS.Get_BR_AdvanceList(PEVM.Trave_Tour_Expense_HDR.BillNo, "3", BaseLocationCode, BaseUserName);
                }
                DataTable DT_Get_Name = new DataTable();
                DT_Get_Name = BAS.Get_BranchAccounting_Title(BaseLocationCode, BaseUserName, BMVW.ExpenceType, "1,2,3", BaseYearVal);
                ViewBag.Title = DT_Get_Name.Rows[0]["BR_Request_Table_Name"].ToString();
                BR_ViewName = DT_Get_Name.Rows[0]["BR_ViewName"].ToString();
                return View(BR_ViewName, PEVM);
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult Accounting_Reject(ExpensesEntryModule PEVM, List<CYGNUS_Branch_Expenses_EntryDET> CPEEDET)
        {
            try
            {
                string Nos = "";
                if (CPEEDET != null)
                {
                    foreach (var ite in CPEEDET)
                    {
                        if (ite.CheckBox)
                        {
                            if (Nos == "")
                                Nos = ite.ID.ToString();
                            else
                                Nos = Nos + "," + ite.ID;
                        }
                    }
                }

                DataTable DT = BAS.USP_Branch_Reject(Nos, BaseLocationCode, BaseUserName.ToUpper(), PEVM.Elec_Expense_HDR.ExpenceType);
                string BillNo = DT.Rows[0][0].ToString();
                return RedirectToAction("Accounting_Approval_Reject_Done", new { Type = 1, No = BillNo });
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public JsonResult Get_Staff_List(string ExpenseType, string Branch, int DllType)
        {
            List<CYGNUS_Branch_Expance> ListPorter = new List<CYGNUS_Branch_Expance>();
            ListPorter = BAS.GetStaffList(ExpenseType, Branch, DllType);

            var PorterList = (from e in ListPorter
                              select new
                              {
                                  Value = e.Value,
                                  Text = e.Text,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Bill Status Details

        public ActionResult BillStatusDetails()
        {
            return View();
        }

        public JsonResult GetBill_Detailes(string No)
        {
            string IsRecordFound = "", BillStatus = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.GetBill_Detailes(No);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";

                    BillStatus = DT.Rows[0]["Bill_Status"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        BillStatus = BillStatus,
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        BillStatus = BillStatus,
                    }
                };
            }
        }

        #endregion

        public ActionResult Accounting_Approval_Reject_Done(int Type, string No)
        {
            ViewBag.BillNo = No.ToString();
            return View();
        }

        public ActionResult HandlingLoading_Unloading_Expenses_Approval(ExpensesEntryModule PEVM, List<CYGNUS_HandlingLoading_Unloading_Expence_DET> CPEEDET)
        {
            try
            {
                List<CYGNUS_HandlingLoading_Unloading_Expence_DET> ListCPEEDet = new List<CYGNUS_HandlingLoading_Unloading_Expence_DET>();

                PEVM.Handling_Expence_HDR.Brcd = BaseLocationCode;
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Handling_Expence_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Handling_Expence_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = "<root>";

                if (CPEEDET != null)
                {
                    foreach (var item in CPEEDET)
                    {
                        XML_Det = XML_Det + "<HandlingLoading_Unloading_Expence_DET>";
                        XML_Det = XML_Det + "<ManifestNumber>" + item.ManifestNumber + "</ManifestNumber>";
                        XML_Det = XML_Det + "<DocketNo>" + item.DocketNo + "</DocketNo>";
                        XML_Det = XML_Det + "<ActualWeight>" + item.ActualWeight + "</ActualWeight>";
                        XML_Det = XML_Det + "<NoOfPKGS>" + item.NoOfPKGS + "</NoOfPKGS>";
                        XML_Det = XML_Det + "<PorterType>" + item.PorterType + "</PorterType>";
                        XML_Det = XML_Det + "<LodingUnlodingType>" + item.LodingUnlodingType + "</LodingUnlodingType>";
                        XML_Det = XML_Det + "<ExpensIncurredDate>" + item.ExpensIncurredDate + "</ExpensIncurredDate>";
                        XML_Det = XML_Det + "<PerKGS>" + item.PerKGS + "</PerKGS>";
                        XML_Det = XML_Det + "<PerPackage>" + item.PerPackage + "</PerPackage>";
                        XML_Det = XML_Det + "<StdRate>" + item.StdRate + "</StdRate>";
                        XML_Det = XML_Det + "<RateCharged>" + item.RateCharged + "</RateCharged>";
                        XML_Det = XML_Det + "<AmountPaid_Per_Branch>" + item.AmountPaid_Per_Branch + "</AmountPaid_Per_Branch>";
                        XML_Det = XML_Det + "<PerKG_AmountPaid_ActualWeight>" + item.PerKG_AmountPaid_ActualWeight + "</PerKG_AmountPaid_ActualWeight>";
                        XML_Det = XML_Det + "<Amount_Passed>" + item.Amount_Passed + "</Amount_Passed>";
                        XML_Det = XML_Det + "<Amount_Rejected>" + item.Amount_Rejected + "</Amount_Rejected>";
                        XML_Det = XML_Det + "<Reason_For_Rejection>" + item.Reason_For_Rejection + "</Reason_For_Rejection>";
                        if (item.CheckBox)
                        {
                            XML_Det = XML_Det + "<IsApprove>1</IsApprove>";
                            XML_Det = XML_Det + "<ApproveBy>" + BaseUserName.ToUpper() + "</ApproveBy>";
                            XML_Det = XML_Det + "<ApproveDate>" + System.DateTime.Now.ToString("dd MMM yyyy") + "</ApproveDate>";
                        }
                        else
                        {
                            XML_Det = XML_Det + "<IsApprove>0</IsApprove>";
                            XML_Det = XML_Det + "<ApproveBy></ApproveBy>";
                            XML_Det = XML_Det + "<ApproveDate>01 Jan 1990</ApproveDate>";
                        }
                        XML_Det = XML_Det + "</HandlingLoading_Unloading_Expence_DET>";
                    }
                }

                XML_Det = XML_Det + "</root>";

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];

                DataTable DT = BAS.Insert_HandlingLoading_Unloading_Expenses_Approval(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear, PEVM.Handling_Expence_HDR.BillNo, PEVM.Handling_Expence_HDR.ExpenceType);

                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "9" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #region Handling (Loading / Unloading) Expensse

        public ActionResult HandlingLoadingUnloadingExpenses(int Type, string No)
        {
            try
            {
                ExpensesEntryModule PEVM = new ExpensesEntryModule();
                PEVM.Type2 = Type;
                PEVM.SubType = 1;
                ViewBag.SubType = 1;
                ViewBag.Title = "Handling (Loading / Unloading) Expensese ";
                ViewBag.ActionName = "HandlingLoading_UnloadingExpensesSubmit";
                PEVM.Handling_Expence_HDR = new CYGNUS_HandlingLoading_Unloading_Expence_HDR();
                PEVM.Handling_Expence_HDR.DocumentNo = No;
                PEVM.Handling_Expence_HDR.BR_Date = System.DateTime.Now;
                PEVM.List_Handling_Expence_DET = BAS.GetBRCD_HandlingLoading_Detail(Type, No, BaseLocationCode);

                return View(PEVM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult HandlingLoading_UnloadingExpensesSubmit(ExpensesEntryModule PEVM, List<CYGNUS_HandlingLoading_Unloading_Expence_DET> CPEEDET)
        {
            try
            {
                PEVM.Handling_Expence_HDR.Brcd = BaseLocationCode;
                PEVM.Handling_Expence_HDR.ExpenceType = PEVM.Type2;

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PEVM.Handling_Expence_HDR.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PEVM.Handling_Expence_HDR);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }

                string XML_Det = HandlingLoading_Unloading_Expenses_DET_XML(CPEEDET, 1);

                var FinYear = BaseFinYear.Split('-');
                var basefinyear = FinYear[0];
                DataTable DT = BAS.Insert_HandlingLoading_Unloading_Expence(xmlDoc1.InnerXml, XML_Det, BaseLocationCode, BaseUserName.ToUpper(), basefinyear);
                int Status = Convert.ToInt16(DT.Rows[0]["Status"].ToString());
                string No = DT.Rows[0]["No"].ToString();
                int ID = Convert.ToInt16(DT.Rows[0]["ID"].ToString());
                string Message = DT.Rows[0]["Message"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("ExpensesDone", new { No = No, Type = "11" });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public string HandlingLoading_Unloading_Expenses_DET_XML(List<CYGNUS_HandlingLoading_Unloading_Expence_DET> CPEEDET, int Type)
        {
            string XML_Det = "<root>";
            if (CPEEDET != null)
            {
                foreach (var item in CPEEDET)
                {
                    if (item.CheckBox)
                    {
                        XML_Det = XML_Det + "<CYGNUS_HandlingLoading_Unloading_Expence_DET>";
                        XML_Det = XML_Det + "<ManifestNumber>" + item.ManifestNumber + "</ManifestNumber>";
                        XML_Det = XML_Det + "<DocketNo>" + item.DocketNo + "</DocketNo>";
                        XML_Det = XML_Det + "<ActualWeight>" + item.ActualWeight + "</ActualWeight>";
                        XML_Det = XML_Det + "<NoOfPKGS>" + item.NoOfPKGS + "</NoOfPKGS>";
                        XML_Det = XML_Det + "<PorterType>" + item.PorterType + "</PorterType>";
                        XML_Det = XML_Det + "<LodingUnlodingType>" + item.LodingUnlodingType + "</LodingUnlodingType>";
                        XML_Det = XML_Det + "<ExpensIncurredDate>" + item.ExpensIncurredDate + "</ExpensIncurredDate>";
                        XML_Det = XML_Det + "<PerKGS>" + item.PerKGS + "</PerKGS>";
                        XML_Det = XML_Det + "<PerPackage>" + item.PerPackage + "</PerPackage>";
                        XML_Det = XML_Det + "<StdRate>" + item.StdRate + "</StdRate>";
                        XML_Det = XML_Det + "<RateCharged>" + item.RateCharged + "</RateCharged>";
                        XML_Det = XML_Det + "<AmountPaid_Per_Branch>" + item.AmountPaid_Per_Branch + "</AmountPaid_Per_Branch>";
                        XML_Det = XML_Det + "<PerKG_AmountPaid_ActualWeight>" + item.PerKG_AmountPaid_ActualWeight + "</PerKG_AmountPaid_ActualWeight>";
                        XML_Det = XML_Det + "</CYGNUS_HandlingLoading_Unloading_Expence_DET>";
                    }
                }
            }
            XML_Det = XML_Det + "</root>";
            return XML_Det;
        }

        #endregion

        #region Get Charge Type Maximum Limit

        public int GetCharge_MaxLimit(string BRCD, int ChargeID)
        {
            int MaxLimit = 0;

            DataTable DT_MaxLimit = BAS.GetCharge_Max_Limit(BRCD, ChargeID, BaseUserName.ToUpper());
            string DT_MaxLimit_str = DT_MaxLimit.Rows[0][0].ToString();
            MaxLimit = Convert.ToInt32(DT_MaxLimit_str);

            return MaxLimit;
        }

        #endregion

        #region Get and Insert Distance From Table

        public JsonResult GetDistanceFromTable(string FromCity, string ToCity, int IsCity, int LocationName, int LocationCode)
        {
            int TotalKm = 0;
            string TotalTime = "0";
            try
            {
                string SQRY = "exec USP_GetDistanceFrom_Table '" + FromCity + "','" + ToCity + "','" + IsCity + "','" + LocationName + "','" + LocationCode + "' ";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);

                try
                {
                    string Val = Dt.Rows[0][0].ToString();
                    TotalTime = Dt.Rows[0]["Hours"].ToString();
                    TotalKm = Convert.ToInt32(Convert.ToDouble(Val));
                }
                catch (Exception)
                {
                    TotalKm = 0;
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        TotalKm = TotalKm,
                        TotalTime = TotalTime
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        TotalKm = 0,
                        TotalTime = 0
                    }
                };
            }
        }

        public JsonResult InsertDistanceFromTable(string FromCity, string ToCity, string TotalKM, string Duration, int IsCity, int LocationName, int LocationCode)
        {
            string Done = "";
            try
            {
                string SQRY = "exec USP_InsertDistanceIn_Table '" + FromCity + "','" + ToCity + "','" + TotalKM + "','" + Duration + "','" + BaseUserName.ToUpper() + "','" + IsCity + "','" + LocationName + "','" + LocationCode + "' ";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);

                try
                {
                    Done = Dt.Rows[0][0].ToString();
                }
                catch (Exception)
                {
                    Done = "";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Done = ""
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        Done = ""
                    }
                };
            }
        }

        #endregion

        #region Common In Use DRiver List Get,Driver Bank Details, User Bank Details, Vendor Bank Details
        
        public JsonResult GetVehicleEmployeeUserName_Type_Wise(string searchTerm, string Type, string SubType)
        {
            var CMP = BAS.Get_CustomerList_TypeWise(searchTerm, Type, BaseLocationCode, SubType);
            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_Driver_BranchWise(string searchTerm, int Type)
        {
            var CMP = BAS.Get_Driver_BranchWise(searchTerm, BaseLocationCode, BaseUserName, Type);
            var users = from user in CMP
                        select new
                        {
                            id = user.id,
                            text = user.text
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Get_Bank_Details(string No, int Type)
        {
            string IsRecordFound = "", Id = "", Name = "", Name_Of_bank = "", Bank_AC_Number = "", IFSC_Code = "", MobileNo = "", Department = "", HOD_Name = "", Grade = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.Get_Bank_Details(No, BaseLocationCode, BaseUserName, Type);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";
                    Id = DT.Rows[0]["ID"].ToString();
                    Name = DT.Rows[0]["Name"].ToString();
                    Name_Of_bank = DT.Rows[0]["BankName"].ToString();
                    Bank_AC_Number = DT.Rows[0]["AC_Number"].ToString();
                    IFSC_Code = DT.Rows[0]["IFSC_Code"].ToString();
                    MobileNo = DT.Rows[0]["MobileNo"].ToString();
                    Department = DT.Rows[0]["Department"].ToString();
                    HOD_Name = DT.Rows[0]["HOD"].ToString();
                    Grade = DT.Rows[0]["Grade"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Id = Id,
                        Name = Name,
                        Name_Of_bank = Name_Of_bank,
                        Bank_AC_Number = Bank_AC_Number,
                        IFSC_Code = IFSC_Code,
                        MobileNo = MobileNo,
                        Department = Department,
                        HOD_Name = HOD_Name,
                        Grade = Grade
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Id = Id,
                        Name = Name,
                        Name_Of_bank = Name_Of_bank,
                        Bank_AC_Number = Bank_AC_Number,
                        IFSC_Code = IFSC_Code,
                        MobileNo = MobileNo,
                        Department = Department,
                        HOD_Name = HOD_Name,
                        Grade = Grade
                    }
                };
            }
        }

        public JsonResult GetUser_Detailes(string No)
        {
            string IsRecordFound = "", UserId = "", Name = "", Name_Of_bank = "", Bank_AC_Number = "", IFSC_Code = "", MobileNo = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.Get_Bank_Details(No, BaseLocationCode, BaseUserName, 1);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";

                    UserId = DT.Rows[0]["UserId"].ToString();
                    Name = DT.Rows[0]["Name"].ToString();
                    Name_Of_bank = DT.Rows[0]["Name_Of_bank"].ToString();
                    Bank_AC_Number = DT.Rows[0]["Bank_AC_Number"].ToString();
                    IFSC_Code = DT.Rows[0]["IFSC_Code"].ToString();
                    MobileNo = DT.Rows[0]["mobileno"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        UserId = UserId,
                        Name = Name,
                        Name_Of_bank = Name_Of_bank,
                        Bank_AC_Number = Bank_AC_Number,
                        IFSC_Code = IFSC_Code,
                        MobileNo = MobileNo
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        UserId = UserId,
                        Name = Name,
                        Name_Of_bank = Name_Of_bank,
                        Bank_AC_Number = Bank_AC_Number,
                        IFSC_Code = IFSC_Code,
                        MobileNo = MobileNo
                    }
                };
            }
        }

        public JsonResult Get_Vendor_Details(string No)
        {
            string IsRecordFound = "", Bank_AC_Number = "";
            DataTable DT = new DataTable();

            try
            {
                DT = BAS.Get_Vendor_Details(No);

                if (DT.Rows.Count > 0)
                {
                    IsRecordFound = "1";
                    Bank_AC_Number = DT.Rows[0]["BankAccountNo"].ToString();
                }
                else
                {
                    IsRecordFound = "0";
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Bank_AC_Number = Bank_AC_Number
                    }
                };
            }
            catch (Exception)
            {
                return new JsonResult()
                {
                    Data = new
                    {
                        IsRecordFound = IsRecordFound,
                        Bank_AC_Number = Bank_AC_Number
                    }
                };
            }
        }

        #endregion


        #region Branch Accounting Modual Approve

        public ActionResult BRStatusSummaryCriteria(int type)
        {
            BR_StatusSummary Criteria = new BR_StatusSummary();
            Criteria.type = type;
            return View(Criteria);
        }

        public ActionResult BRStatusSummaryCriteriaList(BR_StatusSummary BRS)
        {
            try
            {
                DateTime FromDate = System.DateTime.Now;
                DateTime ToDate = System.DateTime.Now;

                string basefinyear = BaseFinYear.Replace("20", "").Replace("-", "_");

                List<CYGNUS_Branch_Expance> optrkList = BAS.BRStatusSummary(BRS.ExpenseType, BRS.BRCD, GF.FormateDate(BRS.FromDate), GF.FormateDate(BRS.ToDate));

                //foreach (var item in optrkList)
                //{
                //    item.type = BRS.type;
                //}

                DataTable DT = BAS.BRStatusSummaryCashOnToday(BRS.BRCD, GF.FormateDate(FromDate), GF.FormateDate(ToDate), basefinyear, 0);

                decimal NetAmt = Convert.ToDecimal(DT.Rows[0][2].ToString());
                BRS.List_BRS = optrkList;

                CYGNUS_Branch_Expance BRSAmt = new CYGNUS_Branch_Expance();
                BRSAmt.NetAmt = NetAmt;
                BRS.BRS = BRSAmt;

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return View(BRS);
        }

        #endregion

        #region BranchAccounting View Print

        public ActionResult BRViewPrint_Criteria()
        {
            BR_ViewPrint_Filter VW = new BR_ViewPrint_Filter();
            VW.FromDate = System.DateTime.Now;
            VW.ToDate = System.DateTime.Now;
            VW.ListCBAM = BAS.GetBRCD_Acc_Master_List(BaseUserName.ToUpper(), "");
            return View(VW);
        }

        public ActionResult BRViewPrint_List(BR_ViewPrint_Filter VW)
        {
            VW.ListBR_Expance = BAS.Get_BR_ViewPrint_List(VW.Type, VW.BillNo, VW.FromDate.ToString("dd MMM yyyy"), VW.ToDate.ToString("dd MMM yyyy"));
            return View(VW);
        }

        #endregion

        #region Driver Incentive TSP Expense Master

        public ActionResult DriverIncentiveTSP_Expense_Master()
        {
            ExpensesEntryModule EEM = new ExpensesEntryModule();
            CYGNUS_DriverIncentiveTSP_Expense_Master CDITM = new CYGNUS_DriverIncentiveTSP_Expense_Master();
            try
            {
                string BRCD = BaseLocationCode;
                EEM.List_CDITEM = new List<CYGNUS_DriverIncentiveTSP_Expense_Master>();
                return View(EEM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _DriverIncentiveTSP_Expense_Master(int Type, int id)
        {
            if (Type == 1)
            {
                List<CYGNUS_DriverIncentiveTSP_Expense_Master> ListCPM = new List<CYGNUS_DriverIncentiveTSP_Expense_Master>();
                DataTable DTGET = BAS.GetDriverIncentiveTSP_Expense();
                List<CYGNUS_DriverIncentiveTSP_Expense_Master> ListCEOG = DataRowToObject.CreateListFromTable<CYGNUS_DriverIncentiveTSP_Expense_Master>(DTGET);
                return PartialView("_DriverIncentiveTSP_Expense_MasterList", ListCEOG);
            }
            else
            {
                CYGNUS_DriverIncentiveTSP_Expense_Master ObjCBTT = new CYGNUS_DriverIncentiveTSP_Expense_Master();
                ObjCBTT.Id = id;
                return PartialView("_DriverIncentiveTSP_Expense_Master", ObjCBTT);
            }

        }

        public ActionResult DriverIncentiveTSP_Expense_Master_Submit(ExpensesEntryModule EEM, List<CYGNUS_DriverIncentiveTSP_Expense_Master> CPM)
        {
            CYGNUS_DriverIncentiveTSP_Expense_Master CDTIM = new CYGNUS_DriverIncentiveTSP_Expense_Master();
            try
            {

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(CPM.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, CPM.Where(c => c.IsChecked == true).ToList());
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }


                DataTable DTADD = BAS.AddEDITDriverIncentiveTSP_Expense(xmlDoc1.InnerXml, BaseLocationCode, BaseUserName);
                return RedirectToAction("Master_Done", new { ID = 3 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        /*
         
         * insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','1','BREE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','2','BRCN','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','3','BRTE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','4','BRDI','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','5','BRPE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','6','BRMV','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','7','BROF','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','8','BROO','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','9','BRFE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','10','BRDE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','11','BRHE','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
insert into WEBX_MASTER_GENERAL values('BRANCHACCOUNTING_CODE','12','BRTD','U','Y',GETDATE(),'CYGNUSTEAM',null,null,null,null,null)
         
         */
    }
}
