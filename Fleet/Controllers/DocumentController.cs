﻿using CipherLib;
using CYGNUS.Classes;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization; 
using WebMatrix.WebData;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class DocumentController : BaseController
    {
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        GeneralFuncations GF = new GeneralFuncations();
        FleetDataService.DocumentService DS = new FleetDataService.DocumentService();

        #region Scan FM Documents

        public ActionResult ScanFMDocuments()
        {
            Webx_FM_Scan_DocumentsViewModel WFSDVM = new Webx_FM_Scan_DocumentsViewModel
            {
                WFSDM = new Webx_FM_Scan_Documents(),
                ListWFSDM = new List<Webx_FM_Scan_Documents>()
            };
            return View(WFSDVM);
        }

        public ActionResult ADDFMScanInfo(int id)
        {
            Webx_FM_Scan_Documents WMFSM = new Webx_FM_Scan_Documents
            {
                Srno = id,
                ScanStatus = 1,
                Status = "Not Scanned"
            };
            return PartialView("_PartialFMScanDocument", WMFSM);
        }

        public JsonResult CheckScanFMDocno(string DocNo, string DocType, string DocumentNo)
        {
            decimal count = 0;
            var status = "";
            var DcoNo = "";
            var DocketNo = "";
            string DocumentName = "", DocumentHref = "";
            string FileMsg = "";
            byte ScanStatus = 1;
            if (DocumentNo == "" || DocumentNo == null || DocumentNo == "N/A")
            {
                DataTable Dt = DS.CheckScanFMDocno(DocNo, DocType, BaseLocationCode, HeadOfficeCode);
                if (Dt.Rows.Count > 0)
                {
                    count = 1;
                    DcoNo = Dt.Rows[0][3].ToString();
                    DocketNo = Dt.Rows[0]["DocketNo"].ToString();
                    if (DcoNo == "")
                    {
                        DcoNo = "N/A";
                    }
                    status = "Not Scanned";
                    ScanStatus = 1;
                    if (Dt.Rows[0]["DocType"].ToString() != "")
                    {
                        if (HeadOfficeCode != BaseLocationCode)
                        {
                            status = "Scanned";
                            ScanStatus = 2;
                        }
                        else
                        {
                            status = "POD Already Uploaded, you can overwrite";
                            ScanStatus = 3;
                        }
                    }
                    try
                    {
                        DocumentName = Dt.Rows[0]["DocumentName"].ToString();
                        DocumentHref = Dt.Rows[0]["DocumentHref"].ToString();
                    }
                    catch (Exception)
                    {
                        DocumentName = "";
                    }
                }
                else
                {
                    count = 0;
                    status = "Not Scanned";
                }
                return Json(new { CNT = count, Status = status, DcoNo = DcoNo, DocketNo = DocketNo, ScanStatus = ScanStatus, DocumentName = DocumentName, FileMsg = FileMsg, CurrLoc = BaseLocationCode, DocumentHref = DocumentHref }, JsonRequestBehavior.AllowGet);
            }
            return Json(new { CNT = 1, Status = "Not Scanned", DcoNo = DocumentNo, DocketNo = DocNo, ScanStatus = ScanStatus, DocumentName = DocumentName, CurrLoc = BaseLocationCode, DocumentHref = DocumentHref }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddScanDoucment(List<Webx_FM_Scan_Documents> FMScan, IEnumerable<HttpPostedFileBase> files)
        {
            string DocketNo = "";//, PODFileName = "";
            var Tranxaction = "";
            var DocumentUploadedPath = "";
            try
            {
                int id = 0;
                Webx_FM_Scan_DocumentsViewModel WFSDVM = new Webx_FM_Scan_DocumentsViewModel();
                WFSDVM.ListWFSDM = FMScan;

                var now = DateTime.Now;
                var yearName = now.ToString("yyyy");
                var monthName = now.ToString("MMMM");

                foreach (var item in WFSDVM.ListWFSDM)
                {
                    //var path = "";
                    var fileName = "";
                    string newFName = "";


                    var sql = "SELECT CodeDesc FROM Webx_Master_General WHERE CodeType='FMDOC' AND StatusCode='Y' AND CodeId='" + item.DocType + "'";
                    DataTable DT = GF.getdatetablefromQuery(sql);
                    string docktypeName = DT.Rows[0][0].ToString();

                    if (files != null)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[id] != null)
                        {
                            var file = ((System.Web.HttpPostedFileBase[])(files))[id];
                            fileName = file.FileName;
                            double dblFileSize = Convert.ToDouble(file.ContentLength) / 1000;
                            double MaxSize = 1024; //1MB

                            if (file.ContentLength > 0 && dblFileSize < MaxSize)
                            {
                                //string PATHDONO = item.DocketNo;
                                string extension = System.IO.Path.GetExtension(file.FileName);
                                newFName = GetFileName(file.FileName, item.DocketNo.Trim(), BasicUtility.GetFMDocketTypeSuffix(item.DocType.ToString()));

                                string DB_SavedPath = docktypeName + "/" + System.DateTime.Now.ToString("yyyy/MMM");

                                string FolDerPath = "D:\\SRLApplicationCode\\POD\\FMScanDocument\\" + DB_SavedPath;
                                if (!Directory.Exists(FolDerPath))
                                    Directory.CreateDirectory(FolDerPath);

                                DocumentUploadedPath = DB_SavedPath + "/" + newFName;
                                file.SaveAs(FolDerPath + "\\" + newFName);

                            }
                        }
                    }

                    id++;
                    DataTable Dt = DS.AddEditScanDocument(item.DocType, item.DocketNo, (item.DocumentNo == "N/A" ? item.DocumentNo = "" : item.DocumentNo), item.ScanStatus, fileName, DocumentUploadedPath, System.DateTime.Now, BaseUserName, BaseLocationCode);

                    DocketNo = DocketNo == "" ? Dt.Rows[0]["DocketNo"].ToString() : DocketNo + "," + Dt.Rows[0]["DocketNo"].ToString();
                    Tranxaction = "Done";
                }
            }
            catch (Exception ex)
            {
                Tranxaction = "Not Done";
            }
            return RedirectToAction("ScanFMDocumentsDone", new { DocketNo = DocketNo, Tranxaction = Tranxaction });
        }

        public ActionResult ScanFMDocumentsDone(string DocketNo, string Tranxaction)
        {
            List<Webx_FM_Scan_Documents> FMScan = new List<Webx_FM_Scan_Documents>();
            FMScan = DS.GetFMDetailByDocNo(DocketNo);
            ViewBag.Tranxaction = Tranxaction;
            return View(FMScan);
        }

        private string GetFileName(string fileName, string docno, string pref)
        {
            string strRet = fileName;

            string pat = @"(?:.+)(.+)\.(.+)";
            Regex r = new Regex(pat);
            //run
            Match m = r.Match(fileName);
            string file_ext = m.Groups[2].Captures[0].ToString();
            string filename = m.Groups[1].Captures[0].ToString();
            docno = docno.Replace("/", "$");

            strRet = pref + "_" + docno + "." + file_ext;

            return strRet;
        }

        private bool CheckDocumentExistance(string FileName)
        {
            try
            {
                return true;//File.Exists(Server.MapPath("~") + @"\GUI\Scan_Document\Vendor_Invoice_Scan" + FileName);
            }
            catch (Exception)
            {
                return true;
            }
        }

        #endregion

        #region Forward FM Document

        public ActionResult ForwardFMDocumentsQuery()
        {
            WebX_FM_FWD_DOC_MasterViewModel WFFDMVM = new WebX_FM_FWD_DOC_MasterViewModel();
            WFFDMVM.WFFDM = new WebX_FM_FWD_DOC_Master();
            WFFDMVM.WFFDM.FM_Entry_Date = System.DateTime.Now;
            WFFDMVM.FFDFM = new FM_FWD_DOCFilter();
            WFFDMVM.ListWFFDM = new List<WebX_FM_FWD_DOC_Master>();
            return View(WFFDMVM);
        }

        public JsonResult GetPaybseByDoctype(string CodeType, string searchTerm)
        {
            List<Webx_Master_General> ListPaybase = new List<Webx_Master_General>();
            ListPaybase = MS.GetGeneralMasterObject().Where(c => c.CodeType == CodeType && c.StatusCode == "Y").ToList();//.Where(c => c.CodeId.ToUpper().Contains(searchTerm.ToUpper()) || c.CodeType.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CodeType)
            if (CodeType != "BILLTYP")
            {
                Webx_Master_General objTax = new Webx_Master_General();
                objTax.CodeId = "All";
                objTax.CodeDesc = "ALL";
                ListPaybase.Insert(0, objTax);
            }
            var SearchList = (from e in ListPaybase
                              select new
                              {
                                  Value = e.CodeId,
                                  Text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ForwardFMDocuments(WebX_FM_FWD_DOC_MasterViewModel WFFDMVM)
        {
            //WFFDMVM.ListWFFDM = new List<WebX_FM_FWD_DOC_Master>();
            WFFDMVM.WFFDM = new WebX_FM_FWD_DOC_Master();
            WFFDMVM.VWDFFM = new vw_dockets_For_FWD();
            WFFDMVM.ListWFFDM = new List<WebX_FM_FWD_DOC_Master>();
            WFFDMVM.ListVWDFFM = new List<vw_dockets_For_FWD>();
            WFFDMVM.ListVWBFF = new List<vw_Bill_For_FWD>();
            WFFDMVM.WFFDM.FM_Entry_Date = System.DateTime.Now;
            WFFDMVM.WFFDM.FM_Date = System.DateTime.Now;
            WFFDMVM.WFFDM.Courier_Way_Bill_Date = System.DateTime.Now;
            WFFDMVM.FFDFM.loccode = BaseLocationCode.ToString();
            WFFDMVM.WFFDM.FM_Doc_Type = Convert.ToByte(WFFDMVM.FFDFM.DocType);
            if (WFFDMVM.FFDFM.DocType == "2")
            {
                WFFDMVM.ListVWBFF = DS.GetDoc_FW_BillList(WFFDMVM).ToList();
            }
            else
            {
                WFFDMVM.ListVWDFFM = DS.GetDoc_FW_COD_DOD_PODList(WFFDMVM).ToList();
            }
            return View(WFFDMVM);
        }

        public ActionResult ForwardFMDocumentsDone(WebX_FM_FWD_DOC_MasterViewModel WFFDMVM, List<vw_Bill_For_FWD> BillList, List<vw_dockets_For_FWD> CODDODPODList)
        {
            string type = "", detailXML = "";//DocketNo = "",
            try
            {
                var suffix = "";
                if (WFFDMVM.FFDFM.DocType == "1")
                {
                    suffix = "P";
                }
                else if (WFFDMVM.FFDFM.DocType == "2")
                {
                    suffix = "B";
                }
                else
                {
                    suffix = "C";
                }
                var finYear = BaseFinYear.Split('-')[0].ToString().Substring(2, 2);
                DataTable Maxcode = DS.GetFmNo(BaseLocationCode, finYear, suffix, Convert.ToByte(WFFDMVM.FFDFM.DocType));
                WFFDMVM.WFFDM.FM_No = Convert.ToString(Maxcode.Rows[0]["Column1"]).ToString();
                WFFDMVM.WFFDM.FM_FWD_LocCode = BaseLocationCode;
                WFFDMVM.WFFDM.FM_FWD_CurrYear = finYear;
                WFFDMVM.WFFDM.FM_Date = Convert.ToDateTime(WFFDMVM.WFFDM.FM_Date);
                WFFDMVM.WFFDM.Courier_Way_Bill_Date = Convert.ToDateTime(WFFDMVM.WFFDM.Courier_Way_Bill_Date);
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WFFDMVM.WFFDM.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, WFFDMVM.WFFDM);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }

                if (BillList != null)
                {
                    WFFDMVM.ListVWBFF = BillList;
                    detailXML = "<root>";
                    foreach (var row in WFFDMVM.ListVWBFF.Where(c => c.isChecked == true))
                    {
                        detailXML = detailXML + "<BillDetail><Bill_No>" + row.billno + "</Bill_No>";
                        detailXML = detailXML + "<Manual_Bill_No>" + row.manualbillno + "</Manual_Bill_No>";
                        detailXML = detailXML + "<Bill_date>" + GF.FormateDate(row.bgndt) + "</Bill_date>";
                        detailXML = detailXML + "<Billing_party>" + row.ptmsnm + "</Billing_party>";
                        detailXML = detailXML + "<Bill_Amount>" + row.billamt + "</Bill_Amount>";
                        detailXML = detailXML + "<Submission_Location>" + row.PFM_LOC + "</Submission_Location>";
                        detailXML = detailXML + "<Scan_Status_New>" + row.Scaned + "</Scan_Status_New>";
                        detailXML = detailXML + "<CurrLoc>" + BaseLocationCode + "</CurrLoc></BillDetail>";
                    }
                    detailXML = detailXML + "</root>";
                    type = "BILL";
                }
                else
                {
                    WFFDMVM.ListVWDFFM = CODDODPODList;
                    detailXML = "<root>";
                    foreach (var row in WFFDMVM.ListVWDFFM.Where(c => c.isChecked == true))
                    {
                        detailXML = detailXML + "<CODDODPODDetail><DockNo>" + row.dockno + "</DockNo>";
                        detailXML = detailXML + "<DockDt>" + GF.FormateDate(row.dockdt) + "</DockDt>";
                        if (WFFDMVM.FFDFM.DocType == "1")
                        {
                            detailXML = detailXML + "<DocumentNo>N/A</DocumentNo>";
                            detailXML = detailXML + "<DocumentDate></DocumentDate>";
                        }
                        else
                        {
                            detailXML = detailXML + "<DocumentNo>" + row.dockno + "</DocumentNo>";
                            detailXML = detailXML + "<DocumentDate>" + GF.FormateDate(row.dockdt) + "</DocumentDate>";
                        }
                        detailXML = detailXML + "<Amount>" + row.dkttot + "</Amount>";
                        detailXML = detailXML + "<Orgn_Dest>" + row.loc + "</Orgn_Dest>";
                        detailXML = detailXML + "<From_To>" + row.from_to + "</From_To>";
                        detailXML = detailXML + "<Dely_Date>" + row.dely_date + "</Dely_Date>";
                        detailXML = detailXML + "<Scan_Status_New>" + row.Scaned + "</Scan_Status_New>";
                        detailXML = detailXML + "<CurrLoc>" + BaseLocationCode + "</CurrLoc></CODDODPODDetail>";
                    }
                    detailXML = detailXML + "</root>";
                    type = "CODDODPOD";
                }
                DataTable Dt = DS.AddEditDocuments(xmlDoc.InnerXml, detailXML.ReplaceSpecialCharacters(), type);
                if (Dt.Rows[0][0].ToString() == "Done" && Dt.Rows[0][1].ToString() == "1")
                {
                    ViewBag.FmNo = Dt.Rows[0][2].ToString();
                    ViewBag.Tranxaction = "DONE";
                }
                else
                {
                    ViewBag.Tranxaction = "NOT DONE";
                }
                ViewBag.Type = type;
            }
            catch (Exception)
            {
                ViewBag.Tranxaction = "NOT DONE";
            }
            return View();
        }

        #endregion

        public ActionResult AcknowledgeFMDocumentsQuery()
        {
            vw_FM_DOC_For_AckViewModel WFFDMVM = new vw_FM_DOC_For_AckViewModel();
            WFFDMVM.FFDAF = new FM_FWD_DOC_AckFilter();
            return View(WFFDMVM);
        }

        public ActionResult ForwardFMAckDocuments(vw_FM_DOC_For_AckViewModel WFFDMVM)
        {
            WFFDMVM.VWFDFAM = new vw_FM_DOC_For_Ack();
            WFFDMVM.ListVWFDFAM = new List<vw_FM_DOC_For_Ack>();
            WFFDMVM.FFDAF.BaseLoccode = BaseLocationCode;
            WFFDMVM.ListVWFDFAM = DS.GetDoc_FW_AckList(WFFDMVM);
            return View(WFFDMVM);
        }

        public ActionResult ForwardFMAckDocumentsSubmit(vw_FM_DOC_For_AckViewModel WFFDMVM, List<vw_FM_DOC_For_Ack> AckList)
        {
            string FMNo = "", Status = "";
            DataSet DSdt = new DataSet();
            DataTable Dt = new DataTable();
            DataTable DtFmDetail = new DataTable();
            try
            {

                List<vw_FM_DOC_For_Ack> AckDoclist = new List<vw_FM_DOC_For_Ack>();
                AckDoclist = AckList.Where(c => c.active).ToList();


                XmlDocument xmlDocAccDoc = new XmlDocument();
                XmlSerializer xmlSerializerAckDoc = new XmlSerializer(AckDoclist.GetType());

                using (MemoryStream xmlStreamAckDoc = new MemoryStream())
                {
                    xmlSerializerAckDoc.Serialize(xmlStreamAckDoc, AckDoclist);
                    xmlStreamAckDoc.Position = 0;
                    xmlDocAccDoc.Load(xmlStreamAckDoc);
                }


                string xmlackdocklist = xmlDocAccDoc.InnerXml;
                DSdt = DS.UpdateForwardFMAckDocumentDetail(xmlackdocklist.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), BaseUserName, BaseLocationCode);
                Dt = DSdt.Tables[0];
                DtFmDetail = DSdt.Tables[1];
                FMNo = Dt.Rows[0]["FMNo"].ToString();
                Status = Dt.Rows[0]["Status"].ToString();
                TempData["vw_FM_DOC_For_Ack"] = DtFmDetail;

            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
            return RedirectToAction("ForwardFMAckDocumentsDone", new { Status = Status });
        }


        public ActionResult ForwardFMAckDocumentsDone(string Status)
        {
            ViewBag.Status = Status;
            vw_FM_DOC_For_AckViewModel WFFDMVM = new vw_FM_DOC_For_AckViewModel();
            DataTable DTList = (DataTable)TempData["vw_FM_DOC_For_Ack"];
            List<vw_FM_DOC_For_Ack> ListDoc = DataRowToObject.CreateListFromTable<vw_FM_DOC_For_Ack>(DTList);
            WFFDMVM.ListVWFDFAM = ListDoc;
            TempData.Keep("vw_FM_DOC_For_Ack");
            return View(WFFDMVM);
        }

        public JsonResult SearchLocationListJsonByRO(string Id)
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.Report_Loc == Id).ToList().OrderBy(c => c.LocName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocCode + ":" + e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #region FM View/Print

        public ActionResult FMReportQuery()
        {
            FMFilter FMF = new FMFilter();
            FMF.BaseLoccode = BaseLocationCode;
            FMF.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            FMF.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (FMF.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                FMF.RegionList.Insert(0, objNew);
                FMF.LocationList = new List<webx_location>();
                FMF.LocationList.Insert(0, objNew);
            }
            if (FMF.LocLevel == 2)
            {
                FMF.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                FMF.LocationList.Insert(0, objNew);
            }
            if (FMF.LocLevel == 3)
            {
                FMF.LocationList = FMF.RegionList;// MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(FMF);
        }

        public ActionResult FMReport(FMFilter FMFilter)
        {
            ViewPrintViewModel VPVM = new ViewPrintViewModel();
            VPVM.FMF = FMFilter;
            VPVM.ListVWPFMVP = DS.GetFMReportList(FMFilter).ToList();
            return View(VPVM);
        }

        public JsonResult CheckDocno(string DocNo, string DocType)
        {
            string strResult = DS.CheckDocno(DocNo, DocType);
            return Json(new { CNT = strResult }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Documents Track

        public ActionResult DocumentsTrack()
        {
            DocumentTrackViewModel DTVM = new DocumentTrackViewModel();
            DTVM.DTF = new DocTrackFilter();
            DTVM.ListVWFTR = new List<VW_FM_TRACK_Report>();
            return View(DTVM);
        }

        public ActionResult TrackDocList(string Doctype, string DocNo)
        {
            DocumentTrackViewModel DTVM = new DocumentTrackViewModel();
            DTVM.ListVWFTR = DS.GetDocumentTrackList(Doctype, DocNo).ToList();
            return PartialView("_DocumentsTrackList", DTVM.ListVWFTR);
        }

        #endregion

        #region Pending Scan Report

        public ActionResult PendingScanReportQuery()
        {
            FMFilter FMF = new FMFilter();
            FMF.BaseLoccode = BaseLocationCode;
            FMF.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            FMF.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (FMF.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                FMF.RegionList.Insert(0, objNew);
                FMF.LocationList = new List<webx_location>();
                FMF.LocationList.Insert(0, objNew);
            }
            if (FMF.LocLevel == 2)
            {
                FMF.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                FMF.LocationList.Insert(0, objNew);
            }
            if (FMF.LocLevel == 3)
            {
                FMF.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(FMF);
        }

        public ActionResult PendingScanReport(FMFilter FMFilter)
        {
            return View(FMFilter);
        }

        #endregion

        #region POD Upload API

        public void POD_Uploaded_Push_API(string DocumentNo, string PODFileName)
        {
            int IDS = 0;
            try
            {
                string SQRY = "exec USP_Get_Document '" + DocumentNo + "' ";
                DataTable DT = GF.GetDataTableFromSP(SQRY);
                string documentname = DT.Rows[0][0].ToString();
                string PODLink = "https://rcplstorageaccount.blob.core.windows.net/rcpl/" + documentname;

                string postData = "{\"UserName\":\"madura\",\"Password\":\"mdu@123\",\"CustomerId\":\"C00002\",\"LspId\":\"RCPL\",\"DocumentNo\":\"" + DocumentNo + "\",\"POD\":{\"PODFileName\":\"" + PODFileName + "\",\"PODLink\":\"" + PODLink + "\",\"EventDateTime\":\"" + System.DateTime.Now.ToString("dd MMM yyyy HH:mm") + "\",\"Latitude\":'',\"Longitude\":''}}";

                string Push_Table_Entry = "exec USP_API_POD_Uploaded_Insert '" + DocumentNo + "','" + PODFileName + "','" + PODLink + "','" + User.Identity.Name + "','" + postData + "','" + false + "','ResponseID','Message','0','0'";
                DataTable Push_DT = GF.GetDataTableFromSP(Push_Table_Entry);
                IDS = Convert.ToInt32(Push_DT.Rows[0][0].ToString());

                string ApplicationID = "";
                //ApplicationID = "AIzaSyA34tNPQu8bcri1I5RP1ZbUrLlocGTgGFQ";
                WebRequest tRequest;
                tRequest = WebRequest.Create("http://orders-beta.logicloud.in/api/PushTracking/PushPOD");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", ApplicationID));
                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;
                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();
                WebResponse tResponse = tRequest.GetResponse();
                dataStream = tResponse.GetResponseStream();
                StreamReader tReader = new StreamReader(dataStream);
                string sResponseFromServer = tReader.ReadToEnd();

                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                PODUploadClass objRE = jsonSerializer.Deserialize<PODUploadClass>(sResponseFromServer.Replace("$", ""));

                string Push_Table_Entry1 = "exec USP_API_POD_Uploaded_Insert '','','','','','" + objRE.IsSuccess + "','" + objRE.id + "','" + objRE.Message + "','" + IDS + "','1'";
                DataTable Push_DT1 = GF.GetDataTableFromSP(Push_Table_Entry1);
                int IDS1 = Convert.ToInt32(Push_DT1.Rows[0][0].ToString());

                tReader.Close(); dataStream.Close();
                tResponse.Close();
            }
            catch (Exception ex)
            {
                string Push_Table_Entry1 = "exec USP_API_POD_Uploaded_Insert '','','','','','" + false + "','0','" + ex.Message + "','" + IDS + "','1'";
                DataTable Push_DT1 = GF.GetDataTableFromSP(Push_Table_Entry1);
                int IDS1 = Convert.ToInt32(Push_DT1.Rows[0][0].ToString());
            }
        }

        public partial class PODUploadClass
        {
            public string id { get; set; }
            public bool IsSuccess { get; set; }
            public string Message { get; set; }
        }

        #endregion

        #region View / Print POD PFM

        public ActionResult PODPFMCriteria()
        {
            PODPFMViewModel Criteria = new PODPFMViewModel();
            return View(Criteria);
        }

        public ActionResult PODPFMViewPrintList(PODPFMViewModel PPV)
        {
            string FromDate = GF.FormateDate(PPV.FromDate);
            string ToDate = GF.FormateDate(PPV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(PPV);
        }

        #endregion

    }
}
