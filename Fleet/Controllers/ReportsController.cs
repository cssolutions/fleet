﻿using Fleet.Classes;
using FleetDataService;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Fleet.ViewModel;
using MvcReportViewer;

namespace Fleet.Controllers
{

    [Authorize]
    //[NoDirectAccess]
    public class ReportsController : BaseController
    {
        GeneralFuncations GF = new GeneralFuncations();
        ReportService CL = new ReportService();

        public ActionResult ReportList(string ReportType, string ReportSubType)
        {
            ViewBag.ReportType = ReportType;
            ViewBag.ReportSubType = ReportSubType;
            var ReportList = CL.GetReportList(ReportType, ReportSubType, BaseUserName.ToUpper(), 1);

            return View(ReportList);
        }

        public ActionResult Index(int Id)
        {
            StandardReportFilter RF = new StandardReportFilter();
            RF.ReportDetails = CL.GetReportDetails(Id);
            RF.ReportId = Id;
            RF.ParameterSet_Details = CL.GETParameterDetails(RF.ReportDetails.ParameterSetId);
            RF.ReportParameterSetId = RF.ReportDetails.ParameterSetId;
            RF.RDLName = RF.ReportDetails.RDLName;
            return View(RF);
        }

        public ActionResult ReportResult(StandardReportFilter RF, FormCollection FormData, bool isDownFormat)
        {
            //RF.ClientName = BaseClientName;
            RF.ParameterSet_Details = CL.GETParameterDetails(RF.ReportParameterSetId);
            StandardReportFilter[] StandardReportFilterList = new StandardReportFilter[1];
            StandardReportFilterList[0] = RF;
            DataTable DT = GF.GetDataTableFromObjects(StandardReportFilterList);

            var ReportParameterList = CL.GetReportParameters(RF.ReportId);

            List<ReportParameter> parameters = new List<ReportParameter>();

            foreach (var itm in RF.ParameterSet_Details.Where(c => c.IsRequiredInReport == true).ToList())
            {
                var ParameterNameList = ReportParameterList.Where(c => c.ReplaceParameterName == itm.ParameterName && c.IsDefaultParameter == false).ToList();

                string ParameterName = itm.ParameterName;

                if (ParameterNameList.Count() > 0)
                {
                    ParameterName = ParameterNameList.FirstOrDefault().ParameterName;
                }

                if (itm.ParameterType == 1)
                {
                    ReportParameter RPT = new ReportParameter("StartDate", GF.FormateDate(Convert.ToDateTime(FormData["StartDate"])));
                    parameters.Add(RPT);
                    ReportParameter RPT1 = new ReportParameter("EndDate", GF.FormateDate(Convert.ToDateTime(FormData["EndDate"])));
                    parameters.Add(RPT1);
                }
                if (itm.ParameterType == 7)
                {
                    ReportParameter RPT = new ReportParameter("Location", FormData["Location"]);
                    parameters.Add(RPT);
                    //ReportParameter RPT1 = new ReportParameter("LocationLevel", FormData["LocationLevel"]);
                    //parameters.Add(RPT1);
                    ReportParameter RPT2 = new ReportParameter("RO", FormData["RO"]);
                    parameters.Add(RPT2);   
                }
                if (itm.ParameterType == 12)
                {
                    ReportParameter RPT = new ReportParameter("LocationTo", FormData["LocationTo"]);
                    parameters.Add(RPT);
                    ReportParameter RPT1 = new ReportParameter("LocationLevelTo", FormData["LocationLevelTo"]);
                    parameters.Add(RPT1);
                }
                if (itm.ParameterType == 8 || itm.ParameterType == 9 || itm.ParameterType == 10 || itm.ParameterType == 11 || itm.ParameterType == 5 || itm.ParameterType == 13 || itm.ParameterType == 3)
                {
                    var ControlVal = FormData[itm.ParameterName].ToString();

                    if (ControlVal == "")
                        ControlVal = itm.BlankValue;

                    ReportParameter RPT1 = new ReportParameter(ParameterName, ControlVal);
                    parameters.Add(RPT1);
                }

                if (itm.ParameterType == 2) 
                {
                    if (itm.IsMultipleChoice)
                    {
                        //string strVal = FormData[itm.ParameterName].ToString();
                        //string[] strArray = null;
                        //if (strVal != null && strVal.Split(',').Count() > 0)
                        //{
                        //    strArray = strVal.Split(',');
                        //}

                        //ReportParameter RPT = new ReportParameter(ParameterName, strArray);
                        //parameters.Add(RPT);

                        ReportParameter RPT = new ReportParameter(ParameterName, FormData[itm.ParameterName].ToString());
                        parameters.Add(RPT);
                    }
                    else
                    {
                        ReportParameter RPT = new ReportParameter(ParameterName, FormData[itm.ParameterName].ToString());
                        parameters.Add(RPT);
                    }
                }
            }

            foreach (var itm in ReportParameterList.Where(c => c.IsDefaultParameter).ToList())
            {
                ReportParameter RPT = new ReportParameter(itm.ParameterName, itm.DefaultValue.ToString());
                parameters.Add(RPT);
            }

            RF.RDLparameters = parameters;

            if (RF.ReportId != 25 && RF.ReportId != 28 && RF.ReportId != 83 && RF.ReportId != 84 && RF.ReportId != 85 && RF.ReportId != 158)
                RF.RDLparameters = RF.RDLparameters.Where(c => c.Name != "ReportCategory").ToList();

            foreach (var item in parameters)
            {
                CL.InsertReportHistory(FormData["ReportId"], item.Name, item.Values[0], BaseUserName);
            }

            ViewBag.ReportId = RF.ReportId.ToString();


            if (RF == null)
            {
                return RedirectToAction("Index");
            }

            //if (isDownFormat)
            //{
            //    return DownloadReport(ReportFormat.Excel, RF.RDLparameters, FormData["RDLPATH"].ToString(), FormData["ReportDetails.DisplayName"]);
            //}

            return View(RF);
        }


        private ActionResult DownloadReport(ReportFormat format, List<ReportParameter> parameters, string RDLPath, string filename = null)
        {
            List<KeyValuePair<string, object>> keyValuePair = new List<KeyValuePair<string, object>>();

            foreach (ReportParameter RP in parameters)
            {
                string _parameterValues = "";
                foreach (var _values in RP.Values)
                {
                    if (_parameterValues == "")
                        _parameterValues = _values;
                    else
                        _parameterValues = _parameterValues + "," + _values;
                }
                KeyValuePair<string, object> newParam = new KeyValuePair<string, object>(RP.Name, _parameterValues);
                keyValuePair.Add(newParam);
            }

            filename = filename + " " + DateTime.Now.ToString("dd MMM yyyy hh mm ss");
            filename = filename.Replace(" ", "_") + ".csv";

            return this.Report(
                format,
                RDLPath,
                keyValuePair,
                filename: filename);
        }

        public JsonResult GetDynamicParameterValue(string SQRY)
        {
            List<ReportService.ReportParameterValue> list = CL.GetDynamicParameterValue(SQRY);

            return Json(list, JsonRequestBehavior.AllowGet);

        }

        #region Required Methods



        public ActionResult GetReportFields(string SQRY)
        {
            List<ReportService.ReportField> CMP = CL.GetReportFields(SQRY);

            return PartialView("_ReportFields", CMP);
        }


        public JsonResult GetEmployeeUserDriverVehicleList(string searchTerm, string SearchType)
        {
            var CMP = CL.GetCustomerListFromSearch(searchTerm, SearchType);

            var users = from user in CMP

                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }


        #endregion
    }
}
