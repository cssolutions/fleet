﻿using System;
using System.Web.Mvc;
using FleetDataService;
using System.Data;
using Fleet;

namespace RCPLTest.Controllers
{
    [Authorize]    
    public class CustomerPortalController : BaseController
    {
        OperationService OS = new OperationService();

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCustomerGCStatusDetails(string dockno)
        {
            string Flg = "0";
            try
            {
                DataTable WMG = OS.CheckCustomerDocketNo(dockno, BaseUserName.ToUpper());
                Flg = WMG.Rows[0][0].ToString();
            }
            catch (Exception)
            {
                Flg = "0";
            }
            return new JsonResult()
            {
                Data = new
                {
                    Flg = Flg
                }
            };
        }
    }
}