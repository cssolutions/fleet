﻿using System.Linq;
using System.Web.Mvc;
using Fleet.Controllers;
using System.Web;
using System.Web.Security;
using System;
using Fleet.Classes;
using FleetDataService;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Collections.Generic;
using System.Data;
using Fleet.Security;
using FleetDataService.Models;
using System.Configuration;
using CYGNUS.Classes;
using System.Collections;

namespace Fleet
{
    [NoCache]
    public class BaseController : Controller
    {
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        APIService AS = new APIService();
        string _BaseUserName = "";
        public string BaseUserName
        {
            get { return _BaseUserName; }
        }

        string _BaseUserType = "";
        public string BaseUserType
        {
            get { return _BaseUserType; }
        }

        string _BaseCompanyCode = "";
        public string BaseCompanyCode
        {
            get { return _BaseCompanyCode; }
        }

        string _FinYearStartDate = "";
        public string FinYearStartDate
        {
            get { return _FinYearStartDate; }
        }

        string _FinYearEndDate = "";
        public string FinYearEndDate
        {
            get { return _FinYearEndDate; }
        }

        string _BaseCompanyName = "";
        public string BaseCompanyName
        {
            get { return _BaseCompanyName; }
        }

        string _BaseLocationCode = "";
        public string BaseLocationCode
        {
            get { return _BaseLocationCode; }
        }

        string _MainLocCode = "";
        public string MainLocCode
        {
            get { return _MainLocCode; }
        }

        string _BaseLocationName = "";
        public string BaseLocationName
        {
            get { return _BaseLocationName; }
        }

        Nullable<decimal> _BaseLocationLevel;
        public Nullable<decimal> BaseLocationLevel
        {
            get { return _BaseLocationLevel; }
        }

        string _BaseFinYear = "";
        public string BaseFinYear
        {
            get { return _BaseFinYear; }
        }

        string _BaseYearVal = "";
        public string BaseYearVal
        {
            get { return _BaseYearVal; }
        }

        string _BaseYearValFirst = "";
        public string BaseYearValFirst
        {
            get { return _BaseYearValFirst; }
        }

        string _HeadOfficeCode = "";
        public string HeadOfficeCode
        {
            get { return _HeadOfficeCode; }
        }

        string _DomainName = "";
        public string DomainName
        {
            get { return _DomainName; }
        }

        string _User_Image = "";
        public string User_Image
        {
            get { return _User_Image; }
        }

        string _CurrFinYear = "";
        public string CurrFinYear
        {
            get { return _CurrFinYear; }
        }

        string _CurrYearVal = "";
        public string CurrYearVal
        {
            get { return _CurrYearVal; }
        }

        string _BaseUserReadWrite = "";
        public string BaseUserReadWrite
        {
            get { return _BaseUserReadWrite; }
        }

        string _ChangeMenuRights = "";
        public string ChangeMenuRights
        {
            get { return _ChangeMenuRights; }
        }

        string _ChangeReportsRights = "";
        public string ChangeReportsRights
        {
            get { return _ChangeReportsRights; }
        }
        
        
        string _ResetPassword = "";
        public string ResetPassword
        {
            get { return _ResetPassword; }
        }

        string _BlockUser = "";
        public string BlockUser
        {
            get { return _BlockUser; }
        }

        string _ActionUrl = "";
        public string ActionUrl
        {
            get { return _ActionUrl; }
        }

        string _BaseEmpType = "";
        public string BaseEmpType
        {
            get { return _BaseEmpType; }
        }

        //GetListObjects GLO = new GetListObjects();

        #region GSTIN Verification
        string _GSTToken = "";
        public string GSTToken
        {
            get { return _GSTToken; }
        }
        DateTime _GSTExpiryTime = DateTime.Now;
        public DateTime GSTExpiryTime
        {
            get { return _GSTExpiryTime; }
        }
        #endregion

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            GeneralFuncations GF = new GeneralFuncations();
            var siteMenuManager = new SiteMenuManager();
            ViewBag.MenuList = MS.GetMenusList(false,BaseUserName);
            ViewBag.LocationList = MS.GetLocationDetails();
            ViewBag.SiteLinks = siteMenuManager.GetSitemMenuItems(User.Identity.Name).ToList();
            //ViewBag.MenuRightsLinks = siteMenuManager.GetSitemMenuItems().ToList();
            base.OnActionExecuting(filterContext);
            string MobileVerification_SQRY = "exec USP_LoginTimeMobileVerification '" + User.Identity.Name + "'";
            DataTable DT_Verification = GF.getdatetablefromQuery(MobileVerification_SQRY);
            _GSTToken = DT_Verification.Rows.Count > 0 ? DT_Verification.Rows[0]["GSTNToken"].ToString() : string.Empty;
            _GSTExpiryTime = DT_Verification.Rows.Count > 0 ? Convert.ToDateTime(DT_Verification.Rows[0]["GSTNTokenExpiry"].ToString()): System.DateTime.Now;
            ViewBag.GSTNToken = _GSTToken;
            ViewBag.GSTNTokenExpiry = _GSTExpiryTime;
           
            HttpCookie cookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                var newTicket = FormsAuthentication.RenewTicketIfOld(ticket);
                if (newTicket.Expiration != ticket.Expiration)
                {
                    string encryptedTicket = FormsAuthentication.Encrypt(newTicket);

                    cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);
                    cookie.Path = FormsAuthentication.FormsCookiePath;
                    Response.Cookies.Add(cookie);
                }
                //if (ticket.UserData != "")
                CustomPrincipal.Login(ticket.UserData);
            }

            /////////////////Define Default 
            if (User.Identity.IsAuthenticated == true)
            {
                ViewBag.BaseUserName = (((CustomIdentity)User.Identity).Name.ToUpper());
                ViewBag.BaseUserType = (((CustomIdentity)User.Identity).UserType);
                ViewBag.BaseLocationLevel = (((CustomIdentity)User.Identity).LocationLevel);
                ViewBag.BaseLocationCode = (((CustomIdentity)User.Identity).LocationCode);
                ViewBag.BaseLocationName = (((CustomIdentity)User.Identity).LocationName);
                ViewBag.MainLocCode = (((CustomIdentity)User.Identity).MainLocCode);
                ViewBag.HeadOfficeCode = (((CustomIdentity)User.Identity).HeadOfficeCode);
                ViewBag.BaseYearVal = (((CustomIdentity)User.Identity).YearVal);
                ViewBag.BaseFinYear = (((CustomIdentity)User.Identity).FinYear);
                ViewBag.BaseCompanyCode = (((CustomIdentity)User.Identity).CompanyCode);
                ViewBag.BaseCompanyName = (((CustomIdentity)User.Identity).CompanyName);
                ViewBag.User_Image = (((CustomIdentity)User.Identity).User_Image);
                ViewBag.CurrYearVal = (((CustomIdentity)User.Identity).CurrYearVal);
                ViewBag.CurrFinYear = (((CustomIdentity)User.Identity).CurrFinYear);
                ViewBag.BaseUserReadWrite = (((CustomIdentity)User.Identity).UserReadWrite);
                ViewBag.ChangeMenuRights = (((CustomIdentity)User.Identity).ChangeMenuRights);
                ViewBag.ChangeReportsRights = (((CustomIdentity)User.Identity).ChangeReportsRights);
                ViewBag.ResetPassword = (((CustomIdentity)User.Identity).ResetPassword);
                ViewBag.BlockUser = (((CustomIdentity)User.Identity).BlockUser);
                ViewBag.ActionUrl = (((CustomIdentity)User.Identity).ActionUrl);

                _BaseUserName = (((CustomIdentity)User.Identity).Name.ToUpper());
                _BaseUserType = (((CustomIdentity)User.Identity).UserType);
                _BaseLocationCode = (((CustomIdentity)User.Identity).LocationCode);
                _BaseLocationName = (((CustomIdentity)User.Identity).LocationName);
                _BaseLocationLevel = (((CustomIdentity)User.Identity).LocationLevel);
                _HeadOfficeCode = (((CustomIdentity)User.Identity).HeadOfficeCode);
                _MainLocCode = (((CustomIdentity)User.Identity).MainLocCode);
                _BaseYearVal = (((CustomIdentity)User.Identity).YearVal);
                _BaseFinYear = (((CustomIdentity)User.Identity).FinYear);
                _BaseCompanyCode = (((CustomIdentity)User.Identity).CompanyCode);
                _BaseCompanyName = (((CustomIdentity)User.Identity).CompanyName);
                _User_Image = (((CustomIdentity)User.Identity).User_Image);
                _CurrYearVal = (((CustomIdentity)User.Identity).CurrYearVal);
                _CurrFinYear = (((CustomIdentity)User.Identity).CurrFinYear);
                _BaseUserReadWrite = (((CustomIdentity)User.Identity).UserReadWrite);
                _ChangeMenuRights = (((CustomIdentity)User.Identity).ChangeMenuRights);
                _ChangeReportsRights = (((CustomIdentity)User.Identity).ChangeReportsRights);
                _ResetPassword = (((CustomIdentity)User.Identity).ResetPassword);
                _BlockUser = (((CustomIdentity)User.Identity).BlockUser);
                _ActionUrl = (((CustomIdentity)User.Identity).ActionUrl);
                _BaseEmpType = (((CustomIdentity)User.Identity).emptype);

                string[] FinYerArray = _BaseFinYear.Split('-');
                ViewBag.FinYearStart = FinYerArray[0];
                ViewBag.FinYearEnd = FinYerArray[1];
                _BaseYearValFirst = FinYerArray[0];
                ViewBag._BaseYearValFirst = FinYerArray[0];
                ViewBag.FinYearStartDate = "01 Apr " + FinYerArray[0];
                ViewBag.FinYearEndDate = "31 Mar " + FinYerArray[1];

                _FinYearStartDate = Convert.ToDateTime("01 Apr " + FinYerArray[0] + " 00:00:00").ToString();
                _FinYearEndDate = Convert.ToDateTime("31 Mar " + FinYerArray[1] + " 23:59:59").ToString();


                ViewBag.FinYearStartDate_1 = _FinYearStartDate;
                ViewBag.FinYearEndDate_1 = _FinYearEndDate;

                ViewBag.THCCalledAs = "THC";
                ViewBag.DKTCalledAs = "CNote";
                ViewBag.TableBoxColor = "blue";
                ViewBag.DefaultDateFormat = "{0:dd/MM/yyyy}";
                ViewBag.DefaultJqueryDateFormat = "dd/MM/yyyy";
                ViewBag.DefaultDateTimeFormat = "{0:dd/MM/yyyy hh:mm tt}";

                ViewBag.GSTStartDate = ConfigurationManager.AppSettings["GSTStartDate"].ToString();
                ViewBag.StaxEndDate = ConfigurationManager.AppSettings["StaxEndDate"].ToString();

                ViewBag.StandardReportPath = ConfigurationManager.AppSettings["MvcReportViewer.ReportPathPrefix"].ToString();
                ViewBag.ReportServerUrl = ConfigurationManager.AppSettings["MvcReportViewer.ReportServerUrl"].ToString();
                //_DomainName = "/"+ MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "DOMIANNAME" && c.CodeId == "1").FirstOrDefault().CodeDesc.ToUpper();
                //ViewBag.DomainName = _DomainName;
                ViewBag.DomainName = ConfigurationManager.AppSettings["DomainName"].ToString();
                string IsDomainNameRequired = ConfigurationManager.AppSettings["IsDomainNameRequired"].ToString();
                if (IsDomainNameRequired == "false")
                {
                    _DomainName = "";
                    ViewBag.DomainName = "";
                }
                try
                {
                    if (string.IsNullOrEmpty(_GSTToken) || _GSTExpiryTime <= DateTime.Now)
                    {
                        ArrayList a = AS.CreateToken();
                        _GSTToken = a[0].ToString();
                        _GSTExpiryTime = Convert.ToDateTime(a[1].ToString());
                        ViewBag.GSTNToken = _GSTToken;
                        ViewBag.GSTNTokenExpiry = _GSTExpiryTime;
                    }
                  
                }
                catch (Exception)
                {
                }
                string ReportURL = Request.Url.AbsoluteUri;
                string[] ReportURL_Arr = ReportURL.Split('/');
                ViewBag.URLPrefix = ReportURL_Arr[0] + "//" + ReportURL_Arr[2];
            }
            else
            {
                RedirectToAction("Login", "Account");
            }
        }

        #region Save Error

        public void Error_Logs(string Controller, string ModuleName, string View_Partial, string Transaction, string Error)
        {
            //try
            //{
            //    var Errors = CL.ErrorEntry(Controller, ModuleName, View_Partial, Transaction, Error, User.Identity.Name.ToString());
            //}
            //catch (Exception ex)
            //{
            //    var Errors = CL.ErrorEntry(Controller, "Error_Logs" + ModuleName, "Error Save Methode", "Insert", "Error in Error Save", User.Identity.Name.ToString());
            //}
        }

        #endregion

        //Report Server Credential
        public static string ReportServer
        {
            get { return ConfigurationManager.AppSettings["ReportServer"]; }
        }
        public static string ReportServerUser
        {
            get { return ConfigurationManager.AppSettings["ReportServerUser"]; }
        }
        public static string ReportServerPass
        {
            get { return ConfigurationManager.AppSettings["ReportServerPass"]; }
        }

        public static Uri ReportServerURL
        {
            get { return new Uri(ConfigurationManager.AppSettings["ReportServerURL"]); }
        }
       
        public static ReportCredentials BaseGetReportCredentials
        {
            get { return new ReportCredentials(ReportServerUser, ReportServerPass, ReportServer); }
        }
        
        public class ReportCredentials : Microsoft.Reporting.WebForms.IReportServerCredentials
        {
            string _userName, _password, _domain;

            public ReportCredentials(string userName, string password, string domain)
            {
                _userName = userName;
                _password = password;
                _domain = domain;
            }

            public System.Security.Principal.WindowsIdentity ImpersonationUser
            {
                get { return null; }
            }

            public System.Net.ICredentials NetworkCredentials
            {
                get
                { return new System.Net.NetworkCredential(_userName, _password, _domain); }
            }

            public bool GetFormsCredentials(out System.Net.Cookie authCoki, out string userName, out string password, out string authority)
            {
                userName = _userName;
                password = _password;
                authority = _domain;

                authCoki = new System.Net.Cookie(".ASPXAUTH", ".ASPXAUTH", "/", "Domain");
                return true;
            }
        }
    }
}