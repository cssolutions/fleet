﻿using CipherLib;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using CYGNUS.Models;
using CYGNUS.Classes;
using ClosedXML.Excel;
using System.Net;
using Fleet.Models;
using System.Drawing;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class MasterController : BaseController
    {
        string ControllerName = "MasterController";
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        FleetDataService.ReportService CL = new FleetDataService.ReportService();
        GeneralFuncations GF = new GeneralFuncations();
        string psSult = "WebX";

        #region Get Distance From Two Position

        public string getDistance(double fromLat, double fromLon, double toLat, double toLon)
        {
            string HtmlResult = "";
            string URI = System.Configuration.ConfigurationManager.AppSettings["GoogledistanceAPILink"].ToString();
            string Mode = System.Configuration.ConfigurationManager.AppSettings["GoogledistanceAPIMode"].ToString();
            string myParameters = "origin=" + fromLat + "," + fromLon + "&destination=" + toLat + "," + toLon + "&sensor=false&units=metric&mode=" + Mode;
            Uri mapURL = new Uri(URI + "?" + myParameters);
            using (WebClient wc = new WebClient())
            {
                HtmlResult = wc.DownloadString(mapURL);
            }
            return HtmlResult;
        }

        //public JsonResult getDistance(double fromLat, double fromLon, double toLat, double toLon, char unit)
        //{
        //    string distanceInKM = "";
        //    try
        //    {
        //        distanceInKM = distance(fromLat, fromLon, toLat, toLon, unit).ToString();

        //        return new JsonResult()
        //        {
        //            Data = new
        //            {
        //                distanceInKM = distanceInKM,
        //            }
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(distanceInKM, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //private double distance(double fromLat, double fromLon, double toLat, double toLon, char unit)
        //{
        //    double theta = fromLon - toLon;
        //    double dist = Math.Sin(deg2rad(fromLat)) * Math.Sin(deg2rad(toLat)) + Math.Cos(deg2rad(fromLat)) * Math.Cos(deg2rad(toLat)) * Math.Cos(deg2rad(theta));
        //    dist = Math.Acos(dist);
        //    dist = rad2deg(dist);
        //    dist = dist * 60 * 1.1515;
        //    if (unit == 'K')
        //    {
        //        dist = dist * 1.609344;
        //    }
        //    else if (unit == 'N')
        //    {
        //        dist = dist * 0.8684;
        //    }
        //    return (dist);
        //}

        //private double deg2rad(double deg)
        //{
        //    return (deg * Math.PI / 180.0);
        //}

        //private double rad2deg(double rad)
        //{
        //    return (rad / Math.PI * 180.0);
        //}

        #endregion

        #region Export Data

        public void ExportData(string MethodName, string FileName)
        {
            DataTable dt = MS.GetExcelData(MethodName, BaseUserName, BaseCompanyCode, BaseLocationCode, BaseYearVal);
            dt.TableName = FileName;
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        #endregion

        #region GeneralMaster Master

        // GeneralMaster List
        public ActionResult ListGeneralMaster()
        {
            return View(MS.GetCodetypesMasterList().Distinct().ToList());//.Where(c => c.HeaderAccess.ToUpper() == "U")
        }
        // Insert/Update GeneralMaster
        public ActionResult GeneralMaster(string id)
        {
            Webx_Master_GeneralViewModel WVM = new Webx_Master_GeneralViewModel();
            try
            {
                List<Webx_Master_General> GeneralMasterList = MS.GetGeneralMasterObject().Where(c => c.CodeType == id.ToUpper().Trim()).ToList();

                WVM.listWMG = GeneralMasterList;
                WVM.WMG = new Webx_Master_General();
                WVM.WMCT = MS.GetCodetypesMasterList().FirstOrDefault(c => c.HeaderCode.ToUpper().Trim() == id.ToUpper().Trim());// change 18/7
                ViewBag.CodeDec = WVM.WMCT.HeaderDesc;
                WVM.WMG.CodeType = WVM.WMCT.HeaderCode;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WVM);
        }

        // Insert/Update GeneralMaster
    //    [HttpPost]
        public ActionResult AddEditGeneralMaster(string CodeId, string CodeDesc, string CodeType, bool StatusCode)
        {
            bool Status = false;
            try
            {

                string MstDetails = "<root><MST>";
                MstDetails = MstDetails + "<CodeType>" + CodeType + "</CodeType>";
                MstDetails = MstDetails + "<CodeId>" + CodeId + "</CodeId>";
                MstDetails = MstDetails + "<CodeDesc>" + CodeDesc.Trim().ToUpper() + "</CodeDesc>";
                MstDetails = MstDetails + "<StatusCode>" + (StatusCode ? "Y" : "N") + "</StatusCode>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName + "</EntryBy></MST></root>";

                DataTable Dt = MS.AddEditGeneralMaster(MstDetails, "U", BaseFinYear.Split('-')[0].ToString());
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        // Duplicate Check
        public JsonResult CheckDuplicateGeneralMaster(string CodeType, string CodeDesc)
        {
            try
            {
                string Count = MS.CheckDuplicateGeneralMaster(CodeType, CodeDesc);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        // Update Status for the Record
        public JsonResult UpdateStatusCodeforGeneralMaster(string CodeId, string CodeType, bool StatusCode)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><MST>";
                MstDetails = MstDetails + "<CodeType>" + CodeType + "</CodeType>";
                MstDetails = MstDetails + "<CodeId>" + CodeId + "</CodeId>";
                MstDetails = MstDetails + "<CodeDesc></CodeDesc>";
                MstDetails = MstDetails + "<StatusCode>" + (StatusCode ? "Y" : "N") + "</StatusCode>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName + "</EntryBy></MST></root>";

                DataTable Dt = MS.AddEditGeneralMaster(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        // Get Manager From Designation and Location 
        public JsonResult GetManagerFromDesignationandLocation(string Location)
        {
            List<WebX_Master_Users> ManagerList = MS.GetManagerFromDesignationandLocation(Location).ToList();

            var SearchList = (from e in ManagerList
                              select new
                              {
                                  Value = e.UserId,
                                  Text = e.Name,
                              }).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        // Get Designation From Category 
        public JsonResult GetDesignationFromCategory(string Category)
        {
            List<Webx_Master_General> DesignationList = MS.GetDesignationFromCategory(Category).ToList();

            var SearchList = (from e in DesignationList
                              select new
                              {
                                  Value = e.CodeId,
                                  Text = e.CodeDesc,
                              }).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region State Master

        // StateMaster List
        public ActionResult State()
        {
            Webx_StateViewModel WSVM = new Webx_StateViewModel();
            try
            {
                WSVM.listWS = new List<Webx_State>();
                WSVM.listWS = MS.GetStateMasterObject().ToList();
                WSVM.WST = new Webx_State();
                WSVM.listWSD = new List<Webx_State_Document>();
                WSVM.WSD = new Webx_State_Document();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WSVM);
        }

        public ActionResult DocumentList(int id)
        {
            Webx_State_Document WSD = new Webx_State_Document();
            WSD.srno = id;
            return PartialView("_State_Documentlist", WSD);
        }

        public ActionResult StateMasters(int id)
        {
            Webx_StateViewModel WSVM = new Webx_StateViewModel();
            WSVM.WST = new Webx_State();
            WSVM.listWSD = new List<Webx_State_Document>();
            WSVM.WSD = new Webx_State_Document();
            try
            {
                if (id > 0)
                {
                    WSVM.WST = MS.GetStateMasterObject().FirstOrDefault(c => c.srno == id);
                    WSVM.listWSD = MS.GetStateMasterDocumentObject().ToList();
                    WSVM.listWSD = WSVM.listWSD.Where(c => c.STCD == WSVM.WST.stcd).ToList();
                }
            }
            catch (Exception)
            {

            }
            return PartialView("_AddEditState", WSVM);
        }

        public ActionResult StateMastersList()
        {
            Webx_StateViewModel WSVM = new Webx_StateViewModel();
            //List<Webx_State> listState = new List<Webx_State>();
            try
            {
                //listState = MS.GetStateMasterObject();
                WSVM.listWS = MS.GetStateMasterObject();
            }
            catch (Exception)
            {

            }
            return PartialView("_StateList", WSVM);
        }

        public ActionResult AddEditStateMaster(Webx_StateViewModel WSVM, List<Webx_State_Document> DocumentList)
        {
            bool Status = false;
            try
            {
                if (WSVM.WST.activeflag2 == true)
                    WSVM.WST.activeflag = "Y";
                else
                    WSVM.WST.activeflag = "N";
                if (WSVM.WST.stax_exmpt_yn2 == true)
                    WSVM.WST.stax_exmpt_yn = "Y";
                else
                    WSVM.WST.stax_exmpt_yn = "N";
                string MstDetails = "<State>", detail_XML = "";
                MstDetails = MstDetails + "<Code>" + WSVM.WST.srno + "</Code>";
                MstDetails = MstDetails + "<Name>" + WSVM.WST.stnm + "</Name>";
                MstDetails = MstDetails + "<FreightRate>" + WSVM.WST.frt_rate + "</FreightRate>";
                MstDetails = MstDetails + "<FreightRateType>" + WSVM.WST.rate_type + "</FreightRateType>";
                MstDetails = MstDetails + "<Category>" + WSVM.WST.Category + "</Category>";
                MstDetails = MstDetails + "<ServiceTaxFlag>" + WSVM.WST.stax_exmpt_yn + "</ServiceTaxFlag>";
                MstDetails = MstDetails + "<ActiveFlag>" + WSVM.WST.activeflag + "</ActiveFlag>";
                MstDetails = MstDetails + "<entryby>" + BaseUserName.ToUpper() + "</entryby>";
                MstDetails = MstDetails + "<IsUnionTerritory>" + WSVM.WST.IsUnionTerritory + "</IsUnionTerritory>";
                MstDetails = MstDetails + "<GSTRegNo>" + WSVM.WST.GSTRegNo + "</GSTRegNo>";
                MstDetails = MstDetails + "<gstStateCode>" + WSVM.WST.gstStateCode + "</gstStateCode>";
                MstDetails = MstDetails + "<statePrefix>" + WSVM.WST.statePrefix + "</statePrefix>";
                MstDetails = MstDetails + "<AccountNo>" + WSVM.WST.AccountNo + "</AccountNo>";
                MstDetails = MstDetails + "<IFSCCode>" + WSVM.WST.IFSCCode + "</IFSCCode>";
                MstDetails = MstDetails + "<BankName>" + WSVM.WST.BankName + "</BankName>";
                MstDetails = MstDetails + "</State>";
                detail_XML = "<ArrayOfStateDocument>";
                if (DocumentList != null)
                {

                    foreach (var Doc in DocumentList)
                    {
                        detail_XML = detail_XML + "<StateDocument>";
                        detail_XML = detail_XML + "<Require></Require>";
                        detail_XML = detail_XML + "<Srno>" + Doc.srno + "</Srno>";
                        detail_XML = detail_XML + "<Form>" + Doc.STFORM + "</Form>";
                        detail_XML = detail_XML + "<Permit></Permit>";
                        detail_XML = detail_XML + "<Strem></Strem>";
                        detail_XML = detail_XML + "<InOutBound>" + Doc.inoutbound + "</InOutBound>";
                        detail_XML = detail_XML + "</StateDocument>";
                    }

                }
                detail_XML = detail_XML + "</ArrayOfStateDocument>";
                DataTable Dt = MS.AddEditStateMaster(MstDetails, detail_XML);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckDuplicateStateMaster(string Statename)
        {
            string Count = "";
            try
            {
                List<Webx_State> Webx_StateList = MS.GetStateMasterObject().Where(c => c.stnm.ToUpper() == Statename.ToUpper()).ToList();
                Count = Webx_StateList.Count.ToString();
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetStateListFromGSTType(string GSTType)
        {
            List<Webx_State> stateList = MS.GetStateListFromGSTType(BaseLocationLevel.ToString(), BaseLocationCode).ToList();
            if (GSTType == "CU")
            {
                stateList = stateList.Where(m => m.IsUnionTerritory == true).ToList();
            }
            else if (GSTType == "CS")
            {
                stateList = stateList.Where(m => m.IsUnionTerritory == false).ToList();
            }


            var SearchList = (from e in stateList
                              select new
                              {
                                  Value = e.statePrefix,
                                  Text = e.stnm,
                              }).Distinct().OrderBy(m => m.Text).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetGSTTypeFromStateandVendor(string statePrefix, string VendorCode)
        {
            List<Webx_Master_General> GSTTypeList = MS.GetGSTTypeFromStateandVendor(statePrefix, VendorCode).ToList();

            var SearchList = (from e in GSTTypeList
                              select new
                              {
                                  Value = e.CodeId,
                                  Text = e.CodeDesc,
                              }).Distinct().OrderBy(m => m.Text).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region City Master

        // City Master List
        public ActionResult City()
        {
            webx_citymasterViewModel WCMVM = new webx_citymasterViewModel();
            try
            {
                WCMVM.listWCM = new List<webx_citymaster>();
                WCMVM.WCM = new webx_citymaster();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WCMVM);
        }

        public ActionResult GetCityDetails(int city_code)
        {
            webx_citymaster WCM = new webx_citymaster();
            try
            {
                if (city_code > 0)
                {
                    WCM = MS.GetCityMasterObject().FirstOrDefault(c => c.city_code == city_code);
                }
            }
            catch (Exception)
            {
            }

            return PartialView("_AddEditCity", WCM);
        }

        public ActionResult GetCityByState(string Id)
        {
            List<webx_citymaster> listCity = new List<webx_citymaster>();
            try
            {
                listCity = MS.GetCityMasterObject();
                if (Id != null && Id.Length > 0)
                {
                    listCity = listCity.Where(c => c.state == Id).ToList();
                }
            }
            catch (Exception)
            {
            }
            return PartialView("_CityList", listCity);
        }

        public ActionResult AddEditCity(webx_citymaster WCM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><MST>";
                MstDetails = MstDetails + "<city_code>" + WCM.city_code + "</city_code>";
                MstDetails = MstDetails + "<Location>" + WCM.Location.Trim().ToUpper() + "</Location>";
                MstDetails = MstDetails + "<state>" + WCM.state + "</state>";
                MstDetails = MstDetails + "<Region>" + WCM.Region + "</Region>";
                MstDetails = MstDetails + "<oda_yn>" + (WCM.oda_yn == "True" ? "Y" : "N") + "</oda_yn>";
                MstDetails = MstDetails + "<ODAkm>" + WCM.ODAkm + "</ODAkm>";
                MstDetails = MstDetails + "<permit_yn>" + (WCM.permit_yn == "True" ? "Y" : "N") + "</permit_yn>";
                MstDetails = MstDetails + "<Destination>" + WCM.Destination + "</Destination>";
                MstDetails = MstDetails + "<Pincode>" + WCM.Pincode + "</Pincode>";
                MstDetails = MstDetails + "<DelieryAddress>" + WCM.DelieryAddress + "</DelieryAddress>";
                MstDetails = MstDetails + "<activeflag>" + (WCM.activeflag == "True" ? "Y" : "N") + "</activeflag></MST></root>";

                DataTable Dt = MS.AddEditCityMaster(MstDetails.Replace("–", "-"));
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult CheckDuplicateCity(string City)
        {
            try
            {
                int Count = MS.GetCityMasterObject().Where(c => c.Location.ToUpper() == City.ToUpper()).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [AllowAnonymous]
        public JsonResult GetAllCityJson(string searchTerm)
        {
            List<webx_citymaster> listCity = new List<webx_citymaster>();
            listCity = MS.GetCityMasterObject().Where(c => c.Location == null ? c.Location == "" : (c.Location.ToUpper().Contains(searchTerm.ToUpper()))).ToList().OrderBy(c => c.Location).ToList();
            var SearchList = (from e in listCity
                              select new
                              {
                                  id = e.Location,
                                  text = e.Location,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Location Master

        public JsonResult GetCityByStateJson(string Id)
        {
            List<webx_citymaster> ListCity = MS.GetCityMasterObject();

            ListCity = ListCity.Where(c => c.state == Id).OrderBy(m => m.Location).ToList();
            var SearchList = (from e in ListCity
                              select new
                              {
                                  Value = e.city_code,
                                  Text = e.Location,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetReportingLocationByReportingTo(string Id)
        {
            List<webx_location> ListLocations = MS.GetLocationDetails();
            ListLocations = ListLocations.Where(c => c.Loc_Level == Convert.ToDecimal(Id) && c.ActiveFlag == "Y").OrderBy(m => m.LocName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public bool CheckDuplicateLocation(string Id)
        {
            List<webx_location> ListLocations = MS.GetLocationDetails();
            ListLocations = ListLocations.Where(c => c.LocCode.ToUpper() == Id.ToUpper()).OrderBy(m => m.LocName).ToList();
            return ListLocations.Count > 0 ? false : true;
        }

        public ActionResult LocationMaster(string Id)
        {
            LocationViewModel WSVM = new LocationViewModel();
            try
            {
                WSVM.EditFlag = "false";
                List<webx_location> ListLocations = MS.GetLocationDetails();
                WSVM.WL = new webx_location();
                if (Id != null && Id != "")
                {
                    WSVM.EditFlag = "true";
                    WSVM.WL = ListLocations.Where(c => c.LocCode.ToUpper() == Id.ToUpper()).FirstOrDefault();
                }
                WSVM.ListGnMST = MS.GetGeneralMasterObject();
                WSVM.listWS = ListLocations;
                WSVM.ListLocState = MS.GetStateMasterObject();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WSVM);
        }

        [HttpPost]
        public string LocationMasterSubmit(LocationViewModel VM)
        {
            VM.WL.LocAddr = VM.WL.LocAddr.Replace("–", " ").Trim();
            VM.WL.loc_startdt_str = Convert.ToDateTime(VM.WL.loc_startdt).ToString("dd MMM yyyy");
            VM.WL.loc_enddt_str = Convert.ToDateTime(VM.WL.loc_enddt).ToString("dd MMM yyyy");
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.WL.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.WL);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            bool Status = MS.InsertLocation(xmlDoc.InnerXml.ReplaceSpecialCharacters(), VM.EditFlag, BaseUserName.ToUpper());
            return Status.ToString();
        }

        public ActionResult LocationList()
        {
            List<webx_location> ListLocations = new List<webx_location>();
            try
            {
                ListLocations = MS.GetLocationDetails();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(ListLocations);
        }

        public JsonResult GetLocationListJson(string searchTerm)
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag.ToUpper() == "Y" && (c.LocName.ToUpper().Contains(searchTerm.ToUpper()) || c.LocCode.ToUpper().Contains(searchTerm.ToUpper()))).ToList().OrderBy(c => c.LocName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult GetCustomerLocationListJson(string str)
        //{
        //    List<webx_location> ListLocations = new List<webx_location>();
        //    ListLocations = MS.GetLocationDetails().ToList();

        //    var SearchList = (from e in ListLocations
        //                      select new
        //                      {
        //                          id = e.LocCode,
        //                          text = e.LocName,
        //                      }).Distinct().ToList();
        //    return Json(SearchList, JsonRequestBehavior.AllowGet);
        //}

        public JsonResult GetCustomerLocationListJson(string str)
        {
            List<webx_location> ListLocations = new List<webx_location>();
            ListLocations = MS.GetLocationDetails().Where(c => c.ActiveFlag == "Y").ToList().OrderBy(c => c.LocName).ToList();

            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocCode + "~" + e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region User Master

        public new ActionResult User()
        {
            return View();
        }

        private static string UserId = ""; private static bool EditFlag = false;

        public ActionResult _AddEditUser(string id)
        {
            //WebX_Master_UsersViewModel WMUVM = new WebX_Master_UsersViewModel();
            //WMUVM.WMU = new WebX_Master_Users();
            UserId = "";
            EditFlag = false;
            if (id != null && id != "")
            {
                EditFlag = true;
                UserId = id.Trim().ToString();
            }
            else
            {
                EditFlag = false;
                UserId = "0";
            }
            return RedirectToAction("AddEditUser");
            //return RedirectToAction("AddEditUser", WMUVM);
        }

        public ActionResult AddEditUser()
        {
            WebX_Master_UsersViewModel WMUVM = new WebX_Master_UsersViewModel();
            WMUVM.WMU = new WebX_Master_Users();
            WMUVM.EditFlag = EditFlag;
            if (EditFlag == true)
            {
                WMUVM.WMU = MS.GetUserDetails().Where(c => c.UserId == UserId).FirstOrDefault();

                if (WMUVM.WMU == null)
                {
                    WMUVM.WMU = new WebX_Master_Users();
                    WMUVM.EditFlag = false;
                }
            }
            return View(WMUVM);
        }

        public static byte[] ImageToByte(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        [HttpPost]
        [InitializeSimpleMembership]
        [ValidateAntiForgeryToken]
        public ActionResult AddEditUser(WebX_Master_UsersViewModel VM, HttpPostedFileBase[] files)
        {
            string extension = "", TranXaction = "", UserFileName = "", UserName = "", type = "user";
            var DocumentUploadedPath = "";
            DataTable Dt_Name = new DataTable();
            try
            {
                try
                {
                    if (((System.Web.HttpPostedFileBase[])(files)) != null)
                    {
                        foreach (var fileobj in files)
                        {
                            var file = ((System.Web.HttpPostedFileBase[])(files));
                            if (fileobj.ContentLength > 0)
                            {
                                extension = System.IO.Path.GetExtension(fileobj.FileName);
                                UserFileName = BaseUserName + extension;
                                DocumentUploadedPath = AzureStorageHelper.UploadBlobFileForUser(fileobj, UserFileName, BaseUserName.ToString(), type);
                            }
                            //var bmp1 = Image.FromFile(fileobj);
                            //bmp1.Save("E:\\Cygnus Projects\\RCPL\\Fleet\\Fleet\\Controllers", System.Drawing.Imaging.ImageFormat.Bmp);

                            //ImageToByte(file);
                            //ImageConverter converter = new ImageConverter();
                            //return (byte[])converter.ConvertTo(file, typeof(byte[]));

                        }
                    }
                }
                catch (Exception)
                {
                    DocumentUploadedPath = VM.WMU.User_Image;
                }

                //VM.WMU.EntryBy = BaseUserName.ToUpper();
                string MstDetails = "<UserMaster>";
                MstDetails = MstDetails + "<UserId>" + VM.WMU.UserId.ToUpper() + "</UserId>";
                MstDetails = MstDetails + "<EmpId>" + VM.WMU.EmpId.ToUpper() + "</EmpId>";
                MstDetails = MstDetails + "<UserPwd>" + (VM.EditFlag == true ? VM.WMU.UserPwd : GF.Encrypt(VM.WMU.UserPwd, psSult)) + "</UserPwd>";
                MstDetails = MstDetails + "<Name>" + VM.WMU.Name + "</Name>";
                MstDetails = MstDetails + "<PasswordQues>" + VM.WMU.PasswordQues + "</PasswordQues>";
                MstDetails = MstDetails + "<PasswordAns>" + VM.WMU.PasswordAns + "</PasswordAns>";
                MstDetails = MstDetails + "<gender>" + VM.WMU.gender + "</gender>";
                MstDetails = MstDetails + "<Category>" + VM.WMU.Category + "</Category>";
                MstDetails = MstDetails + "<DOB>" + VM.WMU.DOB + "</DOB>";
                MstDetails = MstDetails + "<User_Type>" + VM.WMU.User_Type + "</User_Type>";
                MstDetails = MstDetails + "<ManagerId>" + VM.WMU.ManagerId + "</ManagerId>";
                MstDetails = MstDetails + "<BranchCode>" + VM.WMU.BranchCode + "</BranchCode>";
                MstDetails = MstDetails + "<DOJ_ORG>" + VM.WMU.DOJ_ORG + "</DOJ_ORG>";
                MstDetails = MstDetails + "<emptype>" + VM.WMU.emptype + "</emptype>";
                MstDetails = MstDetails + "<EmailId>" + VM.WMU.EmailId + "</EmailId>";
                MstDetails = MstDetails + "<mobileno>" + VM.WMU.mobileno + "</mobileno>";
                MstDetails = MstDetails + "<resi_addr>" + VM.WMU.resi_addr + "</resi_addr>";
                MstDetails = MstDetails + "<Name_Of_bank>" + VM.WMU.Name_Of_bank + "</Name_Of_bank>";
                MstDetails = MstDetails + "<Bank_AC_Number>" + VM.WMU.Bank_AC_Number + "</Bank_AC_Number>";
                MstDetails = MstDetails + "<IFSC_Code>" + VM.WMU.IFSC_Code + "</IFSC_Code>";
                MstDetails = MstDetails + "<Designation>" + VM.WMU.Designation + "</Designation>";
                MstDetails = MstDetails + "<Status>" + (VM.WMU.Status == "Y" ? "100" : "200") + "</Status>";
                //MstDetails = MstDetails + "<IsFieldOfficer>" + VM.WMU.IsFieldOfficer + "</IsFieldOfficer>";
                //MstDetails = MstDetails + "<IsDistancesurveyor>" + VM.WMU.IsDistancesurveyor + "</IsDistancesurveyor>";
                MstDetails = MstDetails + "<User_Image>" + DocumentUploadedPath + "</User_Image>";
                MstDetails = MstDetails + "<Department>" + VM.WMU.Department + "</Department>";
                MstDetails = MstDetails + "<HOD>" + VM.WMU.HOD + "</HOD>";
                MstDetails = MstDetails + "<Grade>" + VM.WMU.Grade + "</Grade>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName.ToUpper() + "</EntryBy>";
                MstDetails = MstDetails + "<ConveyanceExpance>" + VM.WMU.ConveyanceExpance + "</ConveyanceExpance>";
                MstDetails = MstDetails + "<Read_Witre>" + VM.WMU.Read_Witre + "</Read_Witre>";

                MstDetails = MstDetails + "</UserMaster>";

                Dt_Name = MS.InsertUser(MstDetails, VM.EditFlag == true ? "U" : "I");
                TranXaction = Dt_Name.Rows[0][0].ToString();
                UserName = (VM.WMU.UserId + ':' + VM.WMU.UserId);

                if (TranXaction == "Done")
                {
                    if (VM.EditFlag == false)
                    {
                        WebSecurity.CreateUserAndAccount(VM.WMU.UserId, VM.WMU.UserPwd);
                    }
                    if (VM.WMU.Status == "N")
                    {
                        string oldPassword = GF.Decrypt(VM.WMU.UserPwd, psSult);
                        string newPassword = GF.GenerateString(6);
                        bool changePasswordSucceeded = WebSecurity.ChangePassword(VM.WMU.UserId, oldPassword, newPassword);
                        string commandText = "Update WebX_Master_Users set LastPwd = UserPwd, UserPwd='" + GF.Encrypt(newPassword, psSult) + "',IsPasswordExpired = 0 , PwdLastChangeOn = GETDATE() Where UserId ='" + VM.WMU.UserId + "'";
                        DataTable dataTable = GF.getdatetablefromQuery(commandText);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("UserDone", new { UserName = UserName, TranXaction = TranXaction });
        }

        public ActionResult UserDone(string UserName, string TranXaction)
        {
            ViewBag.UserName = UserName;
            ViewBag.TranXaction = TranXaction;

            return View();
        }

        public ActionResult InsertOLDUser()
        {

            List<WebX_Master_Users> ListUsers = MS.GetUserDetails().Where(c => c.UserId != "CYGNUSTEAM").ToList();

            foreach (var itm in ListUsers)
            {
                WebSecurity.CreateUserAndAccount(itm.UserId, itm.UserPwd);
            }


            return View();
        }

        public JsonResult GetUserListJson()
        {
            //List<WebX_Master_Users> ListUsers = MS.GetUserDetailsForUserMasterList(BaseUserName);
            List<WebX_Master_Users> ListUsers = MS.GetUserDetails();
            var data = (from e in ListUsers
                        select new
                        {
                            e.UserId,
                            e.EmpId,
                            e.Name,
                            e.Status,
                            e.User_Type
                        }).ToArray();
            return Json(data.OrderBy(c => c.UserId), JsonRequestBehavior.AllowGet);
        }

        public bool CheckDuplicateUser(string Id, string Flag)
        {
            List<WebX_Master_Users> listUsers = MS.GetUserDetails();
            if (Flag == "U")
                listUsers = listUsers.Where(c => c.UserId.ToUpper() == Id.ToUpper()).ToList();
            else
                listUsers = listUsers.Where(c => c.EmpId == null ? c.EmpId == "" : c.EmpId.ToUpper() == Id.ToUpper()).ToList();

            return listUsers.Count > 0 ? false : true;
        }

        public ActionResult PasswordReset(string id, string newPassword, int type)
        {
            bool status = false;
            try
            {
                if (type == 1)
                {
                    //string getEncryptedPassword = "SELECT UserPwd FROM WebX_Master_Users Where UserId ='" + id + "'";
                    //string oldPassword = GF.Decrypt(GF.getdatetablefromQuery(getEncryptedPassword).Rows[0][0].ToString(), psSult);
                    //bool changePasswordSucceeded = WebSecurity.ChangePassword(id, oldPassword, newPassword);
                    //string commandText = "Update WebX_Master_Users set LastPwd = UserPwd, UserPwd='" + GF.Encrypt(newPassword, psSult) + "',IsPasswordExpired = 0 , PwdLastChangeOn = GETDATE() Where UserId ='" + id + "'";
                    //DataTable dataTable = GF.getdatetablefromQuery(commandText);


                    string commandText1 = "Update WebX_Master_Users set LastPwd = UserPwd, UserPwd='" + newPassword + "' , PwdLastChangeOn = GETDATE() Where UserId ='" + id + "'";
                    System.Data.DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);

                    bool Valid = MS.CheckUserDetailsExistInUserprofile(id);
                    if (!Valid)
                    {
                        WebSecurity.CreateUserAndAccount(id, newPassword);
                    }
                    else
                    {

                        string tk = WebSecurity.GeneratePasswordResetToken(id);
                        int tk1 = WebSecurity.GetUserIdFromPasswordResetToken(tk);
                        bool ResetPassword = WebSecurity.ResetPassword(tk, newPassword);
                    }

                }
                else if (type == 2)
                {
                    string commandText = "Update WebX_Master_Users set LastLoginDate = GETDATE() Where UserId ='" + id + "'";
                    DataTable dataTable = GF.getdatetablefromQuery(commandText);
                }

                //string commandText = "update webpages_Membership set Password='APStWn5hQwY9ghAz5J9+nj4AGeGRRVqvYmaYpo2WmHbsOvK8iQSjW6JMScYoKmLnsA==' where UserId='" + id + "'";
                //string commandText = "Update webpages_Membership  set Password='AG9lZ7+dwTrn9TtrJGvUjC/EOt2hzSrYBQ1AmgrABEt7jGaGmTu4UdcyUkmipFdsjg==' from  UserProfile a  inner join webpages_Membership b on a.UserId=b.UserId and a.UserName='" + id + "'";
                //DataTable dataTable = GF.getdatetablefromQuery(commandText);

                //string commandText1 = "Update WebX_Master_Users  set UserPwd='" + GF.Encrypt("123456", psSult) + "' Where UserId ='" + id + "'";
                //DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);
                status = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllUserJson(string searchTerm)
        {
            List<WebX_Master_Users> listUser = new List<WebX_Master_Users>();
            listUser = MS.GetUserDetails().Where(c => c.Status == "100" && (c.Name.ToUpper().Contains(searchTerm.ToUpper()) || c.UserId.ToUpper().Contains(searchTerm.ToUpper()))).ToList();
            var SearchList = (from e in listUser
                              select new
                              {
                                  id = e.UserId,
                                  text = e.Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDestinationLocationsWithHQTR(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_location> CMP = MS.GetDestinationLocationsWithHQTR(searchTerm).Take(5).ToList();

            var users = from user in CMP
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #region Change Reports Rights

        public ViewResult ChangeRights(string Id)
        {
            ViewBag.UserId = Id;
            ViewBag.BaseUserName = BaseUserName;
            
            return View(MS.GetMenusList(false,BaseUserName).Where(c => c.IsActive == true).ToList());
        }

        [HttpPost]
        public ActionResult ChangeRights(string UserId, List<CYGNUS_Master_Menu_Access> model)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(model.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, model);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);

                    DataTable Dt = MS.InsertMenuRights(xmlDoc.InnerXml, UserId, "1", BaseUserName);
                    string Status = Dt.Rows[0][0].ToString();
                    if (Status == "Done")
                    {
                        ViewBag.Status = Status;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("User", "Master");
        }

        #endregion

        #region Change Reports Rights

        public ViewResult ChangeReportRights(string Id)
        {
            ViewBag.UserId = Id;
            List<CYGNUS_Master_Reports> List = new List<CYGNUS_Master_Reports>();
            //List = MS.GetReportList("", "", BaseUserName.ToUpper(), 2).ToList();
            List = MS.GetReportList("", "", Id.ToUpper(), 2).ToList();
            return View(List);
        }

        [HttpPost]
        public ActionResult ChangeReportRights_Submit(string Id, List<CYGNUS_Master_Reports> Report)
        {
            CYGNUS_Master_Reports ObjCMR = new CYGNUS_Master_Reports();
            try
            {
                string XML = "<root>";
                if (Report != null)
                {
                    ObjCMR = Report.First();
                    foreach (var item in Report)
                    {
                        XML = XML + "<Reports>";
                        XML = XML + "<UserId>" + item.UserName + "</UserId>";
                        XML = XML + "<ReportId>" + item.ID + "</ReportId>";
                        XML = XML + "<HasAccess>" + item.HasAccess + "</HasAccess>";
                        XML = XML + "</Reports>";
                    }
                }
                else
                {
                    @ViewBag.StrError = "Please Enter Valid UserName";
                    return View("Error");
                }
                XML = XML + "</root>";

                DataTable DT = new DataTable();
                DT = MS.Add_Report_Rights(XML, BaseLocationCode, ObjCMR.UserName, BaseCompanyCode);

                return RedirectToAction("ChangeReportRights_Done", new { Id = ObjCMR.UserName });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public ActionResult ChangeReportRights_Done(string Id)
        {
            ViewBag.UserId = Id;
            return View();
        }

        #endregion

        #endregion

        #region Warehouse Master

        public ActionResult Warehouse()
        {
            webx_GODOWNMSTViewModel WGMVM = new webx_GODOWNMSTViewModel();
            try
            {
                WGMVM.listWGMM = new List<webx_GODOWNMST>();
                WGMVM.WGMM = new webx_GODOWNMST();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WGMVM);
        }

        public ActionResult GetWarehouseDetails(int srno)
        {
            webx_GODOWNMST WGMM = new webx_GODOWNMST();
            try
            {
                if (srno > 0)
                {
                    WGMM = MS.GetWarehouseObject().FirstOrDefault(c => c.godown_srno == srno);
                }
            }
            catch (Exception)
            {
            }

            return PartialView("_AddEditWarehouse", WGMM);
        }

        public JsonResult WarehouseListJson(string LocCode)
        {
            List<webx_GODOWNMST> listWarehouse = MS.GetWarehouseObject().Where(c => c.SPDBRCD == LocCode).ToList();
            //List<webx_location> locList = MS.GetLocationDetails().Where(c => c.ActiveFlag == null ? c.ActiveFlag == null : c.ActiveFlag == "Y").OrderBy(c => c.LocName).ToList();
            var ListWarehousedata = (from e in listWarehouse
                                     select new
                                     {
                                         //SPDBRCD = locList.Where(c => c.LocCode.ToUpper().Trim() == e.SPDBRCD.ToUpper().Trim()).FirstOrDefault().LocName,
                                         e.SPDBRCD,
                                         e.godown_name,
                                         e.godown_desc,
                                         e.ActiveFlag,
                                         e.godown_srno,
                                     }).ToArray();

            return Json(ListWarehousedata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditWarehouse(webx_GODOWNMST WGMM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<godown_srno>" + WGMM.godown_srno + "</godown_srno>";
                MstDetails = MstDetails + "<SPDBRCD>" + WGMM.SPDBRCD + "</SPDBRCD>";
                MstDetails = MstDetails + "<godown_name>" + WGMM.godown_name + "</godown_name>";
                MstDetails = MstDetails + "<godown_desc>" + WGMM.godown_desc + "</godown_desc>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WGMM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag>";
                MstDetails = MstDetails + "<UPDTBY> " + BaseUserName.ToUpper() + " </UPDTBY></DocumentElement>";
                DataTable Dt = new DataTable();
                if (WGMM.godown_srno > 0)
                {
                    Dt = MS.AddEditWarehouse(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditWarehouse(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult CheckDuplicateWarehouse(string loccode, string Name)
        {
            try
            {
                string Count = MS.CheckDuplicateWarehouse(loccode, Name);
                return Json(new { Count = Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        #endregion

        #region Pincode Master

        public ActionResult Pincode()
        {
            webx_pincodemasterViewModel WPVM = new webx_pincodemasterViewModel();
            try
            {
                WPVM.listWPM = new List<webx_pincode_master>();
                WPVM.WPM = new webx_pincode_master();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WPVM);
        }

        public JsonResult GetCitylist(string Id)
        {
            List<webx_citymaster> listCity = new List<webx_citymaster>();
            listCity = MS.GetCityMasterObject();
            listCity = listCity.Where(c => c.state == Id.ToString()).ToList();
            var PincodeList = (from e in listCity
                               select new
                               {
                                   Value = e.city_code,
                                   Text = e.Location,
                               }).Distinct().ToList();
            return Json(PincodeList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPincodeByCity(string Id)
        {
            List<webx_pincode_master> listPincode = new List<webx_pincode_master>();
            try
            {
                listPincode = MS.GetPincodeMasterObject();
                if (Id != null && Id.Length > 0)
                {
                    listPincode = listPincode.Where(c => c.cityname == Id).ToList();
                }
            }
            catch (Exception) { }
            return PartialView("_PincodeList", listPincode);
        }

        public ActionResult PincodeMasters(int id)
        {
            webx_pincodemasterViewModel WPVM = new webx_pincodemasterViewModel();
            WPVM.WPM = new webx_pincode_master();
            try
            {
                if (id > 0)
                {
                    WPVM.WPM = MS.GetPincodeMasterObject().FirstOrDefault(c => c.ID == id);
                }
            }
            catch (Exception)
            {

            }
            return PartialView("_AddEditPincode", WPVM.WPM);
        }

        public ActionResult PincodeMastersList()
        {
            webx_pincodemasterViewModel WPVM = new webx_pincodemasterViewModel();
            try
            {
                WPVM.listWPM = MS.GetPincodeMasterObject();
            }
            catch (Exception) { }
            return PartialView("_PincodeList", WPVM);
        }

        public ActionResult AddEditPincode(webx_pincode_master WPM)
        {
            bool Status = false;
            try
            {
                if (WPM.ActiveFlag.ToString() == "true")
                {
                    WPM.ActiveFlag = "Y";
                }
                else
                {
                    WPM.ActiveFlag = "N";
                }
                if (WPM.Is_ODA_Apply.ToString() == "true")
                {
                    WPM.Is_ODA_Apply = "Y";
                }
                else
                {
                    WPM.Is_ODA_Apply = "N";
                }

                string MstDetails = "<PincodeMaster>";
                MstDetails = MstDetails + "<pincode>" + WPM.pincode + "</pincode>";
                MstDetails = MstDetails + "<ID>" + WPM.ID + "</ID>";
                MstDetails = MstDetails + "<StateCode>" + WPM.StateCode + "</StateCode>";
                MstDetails = MstDetails + "<cityname>" + WPM.cityname + "</cityname>";
                MstDetails = MstDetails + "<Area>" + WPM.Area.ToUpper() + "</Area>";
                MstDetails = MstDetails + "<ActiveFlag>" + WPM.ActiveFlag + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                MstDetails = MstDetails + "<LocCode>" + WPM.LocCode + "</LocCode>";
                MstDetails = MstDetails + "<District>" + WPM.District + "</District>";
                MstDetails = MstDetails + "<Region>" + WPM.Region + "</Region>";
                MstDetails = MstDetails + "<Service_Type>" + WPM.Service_Type + "</Service_Type>";

                /* PinCode Base ODA Changes*/
                MstDetails = MstDetails + "<KM_From_Location>" + WPM.KM_From_Location + "</KM_From_Location>";
                MstDetails = MstDetails + "<Handling_Location>" + WPM.Handling_Location + "</Handling_Location>";
                MstDetails = MstDetails + "<Is_ODA_Apply>" + WPM.Is_ODA_Apply + "</Is_ODA_Apply>";
                MstDetails = MstDetails + "</PincodeMaster>";

                DataTable Dt = MS.AddEditPincodeMaster(MstDetails);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult CheckDuplicatePincodeMaster(string AreaName)
        {
            string Count = "";
            try
            {
                List<webx_pincode_master> webx_pincode_masterList = MS.GetPincodeMasterObject().Where(c => c.Area.ToUpper() == AreaName.ToUpper()).ToList();
                Count = webx_pincode_masterList.Count.ToString();
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Count, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Customer Group Master

        public ActionResult CustomerGroup()
        {
            webx_GRPMSTViewModel WGVM = new webx_GRPMSTViewModel();
            try
            {
                WGVM.listWGM = new List<webx_GRPMST>();
                WGVM.WGM = new webx_GRPMST();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WGVM);
        }

        public ActionResult CustomerGroupMasters(string GroupCode)
        {
            webx_GRPMSTViewModel WGVM = new webx_GRPMSTViewModel();
            WGVM.WGM = new webx_GRPMST();
            try
            {
                if (GroupCode != "0")
                {
                    WGVM.WGM = MS.GetCustomerGroupMasterObject().FirstOrDefault(c => c.GRPCD == GroupCode);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditCustomerGroup", WGVM.WGM);
        }

        public ActionResult CustomerGroupMastersList()
        {
            webx_GRPMSTViewModel WGVM = new webx_GRPMSTViewModel();
            try
            {
                WGVM.listWGM = MS.GetCustomerGroupMasterObject();
            }
            catch (Exception) { }
            return PartialView("_CustomerGroupList", WGVM.listWGM);
        }

        public ActionResult AddEditCustomerGroup(webx_GRPMST WGM)
        {
            bool Status = false;
            try
            {
                if (WGM.ActiveFlag.ToString() == "true")
                    WGM.ActiveFlag = "Y";
                else
                    WGM.ActiveFlag = "N";
                string MstDetails = "<CustomerGroupMaster>";
                MstDetails = MstDetails + "<GRPCD>" + WGM.GRPCD + "</GRPCD>";
                MstDetails = MstDetails + "<GRPNM>" + WGM.GRPNM + "</GRPNM>";
                MstDetails = MstDetails + "<GRP_Pwd>" + WGM.GRP_Pwd + "</GRP_Pwd>";
                MstDetails = MstDetails + "<locregion>" + WGM.locregion + "</locregion>";
                MstDetails = MstDetails + "<ActiveFlag>" + WGM.ActiveFlag + "</ActiveFlag>";
                MstDetails = MstDetails + "<OLD_GRPCD>" + WGM.OLD_GRPCD + "</OLD_GRPCD>";
                MstDetails = MstDetails + "<UPDTBY>" + BaseUserName + "</UPDTBY>";
                MstDetails = MstDetails + "<UPDTON>" + System.DateTime.Now + "</UPDTON >";
                MstDetails = MstDetails + "<isSysGenerated>Y</isSysGenerated>";
                MstDetails = MstDetails + "</CustomerGroupMaster>";
                DataTable Dt = MS.AddEditCustomerGroupMaster(MstDetails);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCustomerList(string Type, string searchTerm)
        {
            List<webx_CUSTHDR> CUSTDHR = new List<webx_CUSTHDR>();
            //CUSTDHR = MS.GetCustomerMasterObject().Where(c => (c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper())) && c.CUST_ACTIVE.ToUpper() == "Y").ToList();
            //if (Type == "0")
            //    CUSTDHR = CUSTDHR.Take(30).ToList();
            //else
            //    CUSTDHR = CUSTDHR.Where(c => c.CUSTCAT.ToUpper() == Type.ToUpper()).Take(30).ToList();
            CUSTDHR = MS.GetCustomerList(Type, searchTerm).Take(30).ToList();

            var users = from user in CUSTDHR
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Customer Master

        public ActionResult Customer()
        {
            webx_CUSTHDRViewModel WCVM = new webx_CUSTHDRViewModel();
            try
            {
                WCVM.listWCH = new List<webx_CUSTHDR>();
                WCVM.WCH = new webx_CUSTHDR();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WCVM);
        }

        public ActionResult CustomerMasters(string CustomerCode)
        {
            webx_CUSTHDRViewModel WCVM = new webx_CUSTHDRViewModel();
            WCVM.WCH = new webx_CUSTHDR();
            try
            {
                if (CustomerCode != "0")
                {
                    //WCVM.WCH = MS.GetCustomerMasterObject().FirstOrDefault(c => c.CUSTCD == CustomerCode);
                    WCVM.WCH = MS.GetCustomerMasterObject(CustomerCode).FirstOrDefault();
                    //List<webx_CUSTHDR> newList = (List<webx_CUSTHDR>)TempData["listWCH"];
                    //WCVM.WCH = newList.FirstOrDefault();
                    //TempData.Keep("listWCH");
                    List<string> CUSTCAT = WCVM.WCH.CUSTCAT.Split(',').ToList<string>();
                    //WCVM.WCH.listCCBCS = new List<Cygnus_CustomerBillCycleSetting>();

                    foreach (var item in CUSTCAT)
                    {
                        if (item == "P01")
                            WCVM.WCH.Paid = true;
                        if (item == "P02")
                            WCVM.WCH.TBB = true;
                        if (item == "P03")
                            WCVM.WCH.ToPay = true;
                        if (item == "P04")
                            WCVM.WCH.FOC = true;
                    }
                    List<string> ServiceOptFor = WCVM.WCH.ServiceOptFor.Split(',').ToList<string>();
                    foreach (var item in ServiceOptFor)
                    {
                        if (item == "1" || item == "01")
                            WCVM.WCH.Transportation = true;
                        if (item == "2" || item == "02")
                            WCVM.WCH.Fleet = true;
                        if (item == "3" || item == "03")
                            WCVM.WCH.Warehouse = true;
                    }
                }
                WCVM.WCH.listCCGSTD = DataRowToObject.CreateListFromTable<CygnusCustomerGSTDetails>(MS.GetStateWiseGSTDetails("C", CustomerCode));
            }
            catch (Exception)
            {
                return RedirectToAction("Customer");
            }
            //return PartialView("_AddEditCustomer", WCVM.WCH);
            return View("_AddEditCustomer", WCVM.WCH);
        }

        public JsonResult GetCustomerByGroup(string CustCode, string GRPCD, string State)
        {
            webx_CUSTHDRViewModel WCVM = new webx_CUSTHDRViewModel();
            //WCVM.listWCH = MS.GetCustomerMasterObject();
            //if (GroupCode != null && GroupCode.Length > 0)
            //{
            //    WCVM.listWCH = WCVM.listWCH.Where(c => c.GRPCD == GroupCode).ToList();
            //}

            WCVM.listWCH = MS.GetCustomerListingNew(CustCode, GRPCD, State);
            TempData["listWCH"] = WCVM.listWCH;
            var data = (from e in WCVM.listWCH
                        select new
                        {
                            e.CUSTCD,
                            e.CUSTNM,
                            e.Paybas,
                            e.MOBILENO,
                            e.CUSTPASS,
                            e.CUST_ACTIVE
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPrimaryCustomer(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();
            var CMP = new List<webx_CUSTHDR>();
            
            CMP = MS.GetPrimaryCustomerObject(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationsJson()
        {
            List<webx_location> ListLocations = MS.GetLocationDetails();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddEditCustomer(webx_CUSTHDR WCH, List<CygnusCustomerGSTDetails> ListCustomerGSTDetails, List<Cygnus_CustomerBillCycleSetting> _CustomerBillCycleSettingDetails)
        {
            string CustCode = "false,-", Message = "", CustomerCode = "", Status = "0", CustomerName = "";
            try
            {
                List<string> CUSTCAT = new List<string>();
                List<string> ServiceOptFor = new List<string>();

                if (WCH.Paid == true)
                    CUSTCAT.Add("P01");
                if (WCH.TBB == true)
                    CUSTCAT.Add("P02");
                if (WCH.ToPay == true)
                    CUSTCAT.Add("P03");
                if (WCH.FOC == true)
                    CUSTCAT.Add("P04");

                WCH.CUSTCAT = string.Join(",", CUSTCAT);

                if (WCH.Transportation == true)
                    ServiceOptFor.Add("01");
                if (WCH.Fleet == true)
                    ServiceOptFor.Add("02");
                if (WCH.Warehouse == true)
                    ServiceOptFor.Add("03");

                WCH.ServiceOptFor = string.Join(",", ServiceOptFor);

                if (WCH.CUST_ACTIVE.ToString().ToUpper() == "TRUE")
                    WCH.CUST_ACTIVE = "Y";
                else
                    WCH.CUST_ACTIVE = "N";
                if (WCH.MOBSERV_ENABLED.ToString().ToUpper() == "TRUE")
                    WCH.MOBSERV_ENABLED = "Y";
                else
                    WCH.MOBSERV_ENABLED = "N";
                if (WCH.IsSpecialBill_YN.ToString().ToUpper() == "TRUE")
                    WCH.IsSpecialBill_YN = "Y";
                else
                    WCH.IsSpecialBill_YN = "N";
                if (WCH.Consignee.ToString().ToUpper() == "TRUE")
                    WCH.Consignee = "Y";
                else
                    WCH.Consignee = "N";

                if (WCH.Consignnor.ToString().ToUpper() == "TRUE")
                    WCH.Consignnor = "Y";
                else
                    WCH.Consignnor = "N";

                if (WCH.ScheduleApply.ToString().ToUpper() == "TRUE")
                    WCH.ScheduleApply = "Y";
                else
                    WCH.ScheduleApply = "N";

                WCH.CUSTNM = WCH.CUSTNM.ToUpper();
                XmlDocument MstDetails = new XmlDocument();
                XmlSerializer xmlSerializerHeader = new XmlSerializer(WCH.GetType());
                using (MemoryStream xmlStreamHeader = new MemoryStream())
                {
                    xmlSerializerHeader.Serialize(xmlStreamHeader, WCH);
                    xmlStreamHeader.Position = 0;
                    MstDetails.Load(xmlStreamHeader);
                }

                WCH.listCCGSTD = ListCustomerGSTDetails.Where(c => c.gst_registration_no != null).ToList();
                XmlDocument GSTDetails = new XmlDocument();
                XmlSerializer xmlSerializerGSTDetails = new XmlSerializer(WCH.listCCGSTD.GetType());
                using (MemoryStream xmlStreamGSTDetails = new MemoryStream())
                {
                    xmlSerializerGSTDetails.Serialize(xmlStreamGSTDetails, WCH.listCCGSTD);
                    xmlStreamGSTDetails.Position = 0;
                    GSTDetails.Load(xmlStreamGSTDetails);
                }

                if (WCH.AutoBillAllowed == false)
                {
                    _CustomerBillCycleSettingDetails = new List<Cygnus_CustomerBillCycleSetting>();
                }

                XmlDocument AutoBillGenerationDetails = new XmlDocument();
                XmlSerializer xmlSerializerAutoBillGenerationDetails = new XmlSerializer(_CustomerBillCycleSettingDetails.GetType());

                using (MemoryStream xmlStreamAutoBillGenerationDetails = new MemoryStream())
                {
                    xmlSerializerAutoBillGenerationDetails.Serialize(xmlStreamAutoBillGenerationDetails, _CustomerBillCycleSettingDetails);
                    xmlStreamAutoBillGenerationDetails.Position = 0;
                    AutoBillGenerationDetails.Load(xmlStreamAutoBillGenerationDetails);
                }

                DataTable Dt = MS.AddEditCustomerMaster(MstDetails.InnerXml, GSTDetails.InnerXml, AutoBillGenerationDetails.InnerXml, BaseUserName.ToUpper());
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    CustCode = "true," + Dt.Rows[0][0].ToString();
                    Message = Dt.Rows[0]["TranXaction"].ToString();
                    CustomerCode = Dt.Rows[0][0].ToString();
                    Status = "1";
                    CustomerName = WCH.CUSTNM.ToUpper();
                }
                else
                {
                    CustCode = "false,-";
                    Message = Dt.Rows[0]["TranXaction"].ToString();
                    CustomerCode = "0";
                    Status = "0";
                    CustomerName = "";
                }
                return RedirectToAction("CustomerDone", new { CustomerCode = CustomerCode, CustomerName = CustomerName, Status = Status });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public JsonResult GetCustomerListJson(string searchTerm)
        {
            List<webx_CUSTHDR> Listcustomer = new List<webx_CUSTHDR>();
            //Listcustomer = MS.GetCustomerMasterObject().Where(c => c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CUSTNM).ToList();
            Listcustomer = MS.GetCustomerListingNew(searchTerm, "", "").ToList();
            var SearchList = (from e in Listcustomer
                              select new
                              {
                                  id = e.CUSTCD,
                                  text = e.CUSTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerListing(string searchTerm, string GRPCD)
        {
            var CMP = new List<webx_CUSTHDR>();
            //CMP = MS.GetCustomerMasterObject().Where(c => c.GRPCD.ToUpper() == GRPCD.ToUpper() && (c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper()))).Take(30).ToList();
            CMP = MS.GetCustomerListingNew(searchTerm, "", "").ToList();
            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustomerListingNew(string searchTerm, string GRPCD, string State)
        {
            var CMP = new List<webx_CUSTHDR>();
            CMP = MS.GetCustomerListingNew(searchTerm, GRPCD, State);

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerDone(string CustomerCode, string Status)
        {
            if (Status == "1")
            {
                ViewBag.CustomerCode = CustomerCode.ToUpper() + " : " + MS.GetCustomerMasterObject(CustomerCode).FirstOrDefault().CUSTNM.ToUpper();
            }
            ViewBag.Status = Status;
            return View();
        }

        public ActionResult PasswordResetCustomer(string id)
        {
            bool status = false;
            try
            {

                string commandText1 = "Update webx_CUSTHDR  set CUSTPASS='" + "123456" + "' Where CUSTCD ='" + id + "'";
                DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);
                status = true;
            }
            catch (Exception)
            {
                throw;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDetailsFromCycleType(string id, string CustomerCode)
        {
            List<Cygnus_CustomerBillCycleSetting> CCBCS = new List<Cygnus_CustomerBillCycleSetting>();
            try
            {
                if (id != "0")
                {
                    CCBCS = MS.GetDetailsFromCycleType(id, CustomerCode);
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Customer");
            }
            return PartialView("_CustomerBillCycleSetting", CCBCS);
        }

        public ActionResult CheckGSTNOValid(string GSTNO)
        {
            bool status = false;
            try
            {

                string commandText1 = "Usp_CheckValidGSTNO '" + GSTNO + "'";
                DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);
                status = Convert.ToBoolean(dataTable1.Rows[0]["Status"].ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckAadharNOValid(string AADHARNO)
        {
            bool status = false;
            try
            {

                string commandText1 = "Usp_CheckValidAADHARNO '" + AADHARNO + "'";
                DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);
                status = Convert.ToBoolean(dataTable1.Rows[0]["Status"].ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckPANNOValid(string PANNO)
        {
            bool status = false;
            try
            {

                string commandText1 = "Usp_CheckValidPANNO '" + PANNO + "'";
                DataTable dataTable1 = GF.getdatetablefromQuery(commandText1);
                status = Convert.ToBoolean(dataTable1.Rows[0]["Status"].ToString());
            }
            catch (Exception)
            {
                throw;
            }
            return Json(status, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Address Master

        public ActionResult Address()
        {
            webx_master_CustAddressesViewModel WMCAVM = new webx_master_CustAddressesViewModel();
            try
            {
                WMCAVM.listCustAddress = new List<webx_master_CustAddresses>();
                WMCAVM.CustAddress = new webx_master_CustAddresses();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMCAVM);
        }

        public ActionResult GetAddressDetails(int srno)
        {
            webx_master_CustAddresses WMCAM = new webx_master_CustAddresses();
            try
            {
                if (srno > 0)
                {
                    WMCAM = MS.GetAddressObject().FirstOrDefault(c => c.srno == srno);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditAddress", WMCAM);
        }

        public JsonResult CheckDuplicateAddressCode(string AddID)
        {
            try
            {
                string Count = MS.CheckDuplicateAddressCode(AddID);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        public JsonResult GetCityPincodeDetails(string id)
        {
            List<webx_pincode_master> listPincode = new List<webx_pincode_master>();

            listPincode = MS.GetPincodeMasterObject().Where(c => c.cityname == id).ToList();


            var SearchList = (from e in listPincode
                              select new
                              {
                                  Value = e.pincode,
                                  Text = e.pincode + ":" + e.Area,
                              }).Distinct().ToList();
            // return Json(SearchList, JsonRequestBehavior.AllowGet);


            return Json(SearchList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddressListJson()
        {
            List<webx_master_CustAddresses> listAddress = MS.GetAddressObject();
            List<webx_citymaster> cityList = MS.GetCityMasterObject().Where(c => c.activeflag == null ? c.activeflag == null : c.activeflag == "Y").ToList();
            var ListAddressdata = (from e in listAddress
                                   select new
                                   {
                                       e.AddID,
                                       e.Address,
                                       City = cityList.Where(c => c.city_code.ToString() == e.City).FirstOrDefault().Location,
                                       e.Email,
                                       e.ActiveFlag,
                                       e.srno,
                                   }).ToArray();

            return Json(ListAddressdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditAddress(webx_master_CustAddresses WMCAM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<srno>" + WMCAM.srno + "</srno>";
                MstDetails = MstDetails + "<AddID>" + WMCAM.AddID + "</AddID>";
                MstDetails = MstDetails + "<City>" + WMCAM.City + "</City>";
                MstDetails = MstDetails + "<Address>" + WMCAM.Address + "</Address>";
                MstDetails = MstDetails + "<Pincode>" + WMCAM.Pincode + "</Pincode>";
                MstDetails = MstDetails + "<PhoneNo>" + WMCAM.PhoneNo + "</PhoneNo>";
                MstDetails = MstDetails + "<Email>" + WMCAM.Email + "</Email>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WMCAM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag></DocumentElement>";
                DataTable Dt = new DataTable();
                if (WMCAM.srno > 0)
                {
                    Dt = MS.AddEditAddress(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditAddress(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Customer Address Mapping

        public ActionResult CustomerAddressMapping()
        {
            webx_master_CustAddr_MappingViewModel WMCAMVM = new webx_master_CustAddr_MappingViewModel();
            try
            {
                WMCAMVM.listWMCAMM = new List<webx_master_CustAddr_Mapping>();
                WMCAMVM.WMCAMM = new webx_master_CustAddr_Mapping();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMCAMVM);
        }

        public ActionResult GetCustAddressMappingDetails(int srno)
        {
            webx_master_CustAddr_Mapping WMCAMM = new webx_master_CustAddr_Mapping();
            try
            {
                if (srno > 0)
                {
                    WMCAMM = MS.GetCustAddrressMappingObject().FirstOrDefault(c => c.SrNo == srno);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditCustomerAddress", WMCAMM);
        }

        public JsonResult GetAddressCityJson(string AddID)
        {
            List<webx_master_CustAddresses> listAddress = MS.GetAddressObject().Where(c => c.AddID == AddID).ToList();
            List<webx_citymaster> cityList = MS.GetCityMasterObject().Where(c => c.activeflag == null ? c.activeflag == null : c.activeflag == "Y").ToList();
            var ListAddressdata = (from e in listAddress
                                   select new
                                   {
                                       City = cityList.Where(c => c.city_code.ToString() == e.AddID).FirstOrDefault().Location,
                                       e.Address,
                                   }).ToArray();

            return Json(ListAddressdata, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CustAddressMappingListJson()
        {
            List<webx_master_CustAddr_Mapping> listAddress = MS.GetCustAddrressMappingObject();
            List<webx_citymaster> cityList = MS.GetCityMasterObject().Where(c => c.activeflag == null ? c.activeflag == null : c.activeflag == "Y").ToList();
            var ListAddressdata = (from e in listAddress
                                   select new
                                   {
                                       e.CustCD,
                                       e.AddID,
                                       e.ActiveFlag,
                                       e.SrNo,
                                   }).ToArray();

            return Json(ListAddressdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditCustAddressMapping(webx_master_CustAddr_Mapping WMCAMM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<srno>" + WMCAMM.SrNo + "</srno>";
                MstDetails = MstDetails + "<CustCD>" + WMCAMM.CustCD + "</CustCD>";
                MstDetails = MstDetails + "<AddID>" + WMCAMM.AddID + "</AddID>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WMCAMM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag></DocumentElement>";
                DataTable Dt = new DataTable();
                if (WMCAMM.SrNo > 0)
                {
                    Dt = MS.AddEditCustAddressMapping(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditCustAddressMapping(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Vendor Master

        public ActionResult Vendor(string Id)
        {
            VendorViewModel VVM = new VendorViewModel();
            try
            {
                VVM.EditFlag = "false";
                VVM.VendorHDR = new webx_VENDOR_HDR();
                VVM.VendorDET = new webx_VENDOR_DET();
                VVM.ListVendorDoc = new List<Cygnus_Vendor_Document>();
                VVM.VendorDoc = new Cygnus_Vendor_Document();
                VVM.VendorDoc.Id = 1;
                VVM.locationList = MS.GetLocationDetails().Where(c => c.ActiveFlag == null ? c.ActiveFlag == "" : c.ActiveFlag.ToUpper() == "Y").ToList();
                if (Id != null && Id != "")
                {
                    List<webx_VENDOR_HDR> ListVendor = MS.GetVendorObject();
                    VVM.EditFlag = "true";
                    VVM.VendorHDR = MS.GetVendorObject().Where(c => c.VENDORCODE == null ? c.VENDORCODE == "" : c.VENDORCODE.Trim().ToUpper() == Id.ToUpper()).FirstOrDefault();
                    VVM.VendorDET = MS.GetVendorDetObject().Where(c => c.VENDORCODE == null ? c.VENDORCODE == "" : c.VENDORCODE.Trim().ToUpper() == Id.ToUpper()).FirstOrDefault();
                    VVM.ListVendorDoc = MS.GetVendorDoc(Id);
                }
                VVM.stateGSTList = DataRowToObject.CreateListFromTable<CygnusVendorGSTDetails>(MS.GetStateWiseGSTDetails("V", VVM.VendorHDR.VENDORCODE));
                VVM.ListGnMST = MS.GetGeneralMasterObject();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(VVM);
        }

        [HttpPost]
        public ActionResult VendorMasterSubmit(VendorViewModel VM, List<CygnusVendorGSTDetails> ListVendorGSTDetails, List<Cygnus_Vendor_Document> UploadDoc)
        {

            foreach (var item in UploadDoc)
            {
                if (Request.Files["files_" + item.Id] != null)
                {
                    var file = Request.Files["files_" + item.Id];
                    string extension = System.IO.Path.GetExtension(file.FileName), FolderPath = "";
                    FolderPath = "/Images/VendorDocument/" + BaseLocationCode + "/" + BaseFinYear + "/" + System.DateTime.Now.Month + "/";
                    string path1 = FolderPath + DateTime.Now.ToFileTime() + "_" + item.Id + extension;
                    if (item.ImagePath == null)
                    {
                        item.ImagePath = path1;
                        path1 = Server.MapPath("~/" + path1);
                        string strDirectoryName = Server.MapPath("~/" + FolderPath);
                        if (Directory.Exists(strDirectoryName) == false)
                            Directory.CreateDirectory(strDirectoryName);
                        file.SaveAs(path1);
                    }
                }
            }

            XmlDocument xmlUploadDoc = new XmlDocument();
            XmlSerializer xmlUploadSerializer = new XmlSerializer(UploadDoc.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlUploadSerializer.Serialize(xmlStream, UploadDoc);
                xmlStream.Position = 0;
                xmlUploadDoc.Load(xmlStream);
            }

            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.VendorHDR.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.VendorHDR);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }
            XmlDocument xmlDoc1 = new XmlDocument();
            XmlSerializer xmlSerializer1 = new XmlSerializer(VM.VendorDET.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer1.Serialize(xmlStream, VM.VendorDET);
                xmlStream.Position = 0;
                xmlDoc1.Load(xmlStream);
            }

            XmlDocument xmlDocGST = new XmlDocument();

            ListVendorGSTDetails = ListVendorGSTDetails.Where(m => m.gst_registration_no != null && m.gst_registration_no != "").ToList();

            XmlSerializer xmlSerializerGST = new XmlSerializer(ListVendorGSTDetails.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializerGST.Serialize(xmlStream, ListVendorGSTDetails);
                xmlStream.Position = 0;
                xmlDocGST.Load(xmlStream);
            }

            //bool Status = MS.InsertVendor(xmlDoc.InnerXml, xmlDoc1.InnerXml, VM.EditFlag, BaseUserName);
            DataTable DT = MS.InsertVendor(xmlDoc.InnerXml, xmlDoc1.InnerXml, xmlDocGST.InnerXml, xmlUploadDoc.InnerXml, VM.EditFlag, BaseUserName);
            //DataTable DT = new DataTable();
            bool Status = false;

            string Statusstr = DT.Rows[0]["Status"].ToString();

            if (Statusstr == "1")
            {
                Status = true;
            }

            string VendorCode = DT.Rows[0]["VendorCode"].ToString();
            string ReturnStr = Status + "," + VendorCode;
            return RedirectToAction("VendorDone", new { VendorCode = VendorCode });
        }

        public ActionResult VendorList()
        {
            return View();
        }

        public JsonResult GetVendorListJson()
        {
            List<webx_VENDOR_HDR> ListVendors = MS.GetVendorObject();
            List<webx_VENDOR_DET> ListVendorsDet = MS.GetVendorDetObject();
            List<Webx_Master_General> ListGnMST = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "VENDTY").OrderBy(c => c.CodeDesc).ToList(); ;

            var ListVendorsdata = (from e in ListVendors
                                   select new
                                   {
                                       VENDORCODE = e.VENDORCODE,
                                       VENDORNAME = e.VENDORNAME,
                                       Vendor_Type = ListGnMST.Where(m => m.CodeId.ToUpper() == e.Vendor_Type.ToUpper()).FirstOrDefault() != null ? ListGnMST.Where(m => m.CodeId.ToUpper() == e.Vendor_Type.ToUpper()).FirstOrDefault().CodeDesc : "",
                                       e.Active,
                                       VENDORCITY = ListVendorsDet.Where(m => m.VENDORCODE == e.VENDORCODE).FirstOrDefault() != null ? ListVendorsDet.Where(m => m.VENDORCODE == e.VENDORCODE).FirstOrDefault().VENDORCITY : "",
                                   }).ToArray();
            return Json(ListVendorsdata, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchVendorListJson(string searchTerm)
        {
            List<webx_VENDOR_HDR> ListVendor = new List<webx_VENDOR_HDR>();
            ListVendor = MS.GetVendorObject().Where(c => c.Active == "Y" && (c.VENDORCODE.ToUpper().Contains(searchTerm.ToUpper()) || c.VENDORNAME.ToUpper().Contains(searchTerm.ToUpper()))).OrderBy(c => c.VENDORNAME).ToList();
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  id = e.VENDORCODE,
                                  text = e.VENDORNAME,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchDriverListJson(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<WEBX_FLEET_DRIVERMST> CMP = MS.GetDriverObject(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.Driver_Id,
                            text = user.Driver_Name

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmplyoee_NameJson(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<WebX_Master_Users> CMP = MS.GetEmployeeObject(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.UserId,
                            text = user.Name

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchVendorListJsonWithoutBlacklisted(string searchTerm)
        {
            List<webx_VENDOR_HDR> ListVendor = new List<webx_VENDOR_HDR>();
            ListVendor = MS.GetVendorObjectWithoutBlacklisted(BaseLocationCode).Where(c => c.Active == "Y" && (c.VENDORCODE.ToUpper().Contains(searchTerm.ToUpper()) || c.VENDORNAME.ToUpper().Contains(searchTerm.ToUpper()))).OrderBy(c => c.VENDORNAME).ToList();
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  id = e.VENDORCODE,
                                  text = e.VENDORNAME,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SearchVendorListByStateJson(string searchTerm, string State)
        {
            List<webx_VENDOR_HDR> VendorList_XML = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(MS.GetVendorByState(searchTerm, State));
            var SearchList = (from e in VendorList_XML
                              select new
                              {
                                  id = e.VENDORCODE,
                                  text = e.VENDORNAME,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getBranchWiseLoadingUnloadingVendorListJsonForBill(string searchTerm, string Branch)
        {
            List<webx_VENDOR_HDR> ListVendor = new List<webx_VENDOR_HDR>();
            string SQRY = "SELECT DISTINCT VENDORCODE,VENDORNAME FROM VW_getBranchWiseLoadingUnloadingVendorList WHERE BranchCode='" + Branch + "' AND (VENDORNAME LIKE '%" + searchTerm + "%' OR VENDORCODE LIKE '%" + searchTerm + "%') AND Active='Y'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            ListVendor = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  Value = e.VENDORCODE,
                                  Text = e.VENDORNAME,
                                  id = e.VENDORCODE,
                                  text = e.VENDORNAME,
                              }).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorDone(string VendorCode)
        {
            ViewBag.VendorCode = VendorCode.ToUpper() + " : " + MS.GetVendorObject().FirstOrDefault(c => c.VENDORCODE.ToUpper() == VendorCode.ToUpper()).VENDORNAME.ToUpper();
            return View();
        }

        [HttpPost]
        public ActionResult UploadDoc(int Id)
        {
            Cygnus_Vendor_Document CVD = new Cygnus_Vendor_Document();
            CVD.Id = Id;
            return PartialView("_VendorDocumentDetails", CVD);
        }

        #endregion

        #region Receiver Master

        public ActionResult Receiver()
        {
            WebX_Master_ReceiverViewModel WMRVM = new WebX_Master_ReceiverViewModel();
            try
            {
                WMRVM.listWMR = new List<WebX_Master_Receiver>();
                WMRVM.WMR = new WebX_Master_Receiver();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMRVM);
        }

        public ActionResult GetReceiverDetails(String ReceiverCode)
        {
            WebX_Master_Receiver WMRM = new WebX_Master_Receiver();
            try
            {
                if (ReceiverCode != "0")
                {
                    WMRM = MS.GetReceiverObject().FirstOrDefault(c => c.ReceiverCode == ReceiverCode);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditReceiver", WMRM);
        }

        public JsonResult ReceiverListJson()
        {
            List<WebX_Master_Receiver> listReceiver = MS.GetReceiverObject();
            var ListReceiverdata = (from e in listReceiver
                                    select new
                                    {
                                        e.ReceiverCode,
                                        e.ReceiverName,
                                        e.LocCode,
                                        e.ActiveFlag,
                                    }).ToArray();

            return Json(ListReceiverdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditReceiver(WebX_Master_Receiver WMRM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<ReceiverCode>" + WMRM.ReceiverCode + "</ReceiverCode>";
                MstDetails = MstDetails + "<ReceiverName>" + WMRM.ReceiverName + "</ReceiverName>";
                MstDetails = MstDetails + "<LocCode>" + WMRM.LocCode + "</LocCode>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WMRM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy>"+ BaseUserName+" </EntryBy></DocumentElement>";
                DataTable Dt = new DataTable();
                if (WMRM.ReceiverCode != "0")
                {
                    Dt = MS.AddEditReceiver(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditReceiver(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region OptimumRoute

        public ActionResult OptimumRoute(string Id)
        {
            OptimumRouteViewModel ORVM = new OptimumRouteViewModel();
            try
            {
                ORVM.EditFlag = "false";
                ORVM.locationList = MS.GetLocationDetails().Where(c => c.ActiveFlag == null ? c.ActiveFlag == "" : c.ActiveFlag.ToUpper() == "Y").ToList();
                ORVM.ListGnMST = MS.GetGeneralMasterObject();
                if (Id != null && Id != "")
                {
                    List<webx_VENDOR_HDR> ListVendor = MS.GetVendorObject();
                    ORVM.EditFlag = "true";
                    ORVM.OR = MS.GetOptimumRouteObject().Where(c => c.SrNo == Convert.ToInt32(Id)).FirstOrDefault();
                }
                if (ORVM.OR == null)
                {
                    ORVM.OR = new Webx_master_OptimumRouteMaster();
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(ORVM);
        }

        [HttpPost]
        public string OptimumRouteSubmit(OptimumRouteViewModel VM)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.OR.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.OR);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            bool Status = MS.InsertOptimalRoute(xmlDoc.InnerXml, VM.EditFlag, BaseUserName);
            return Status.ToString();
        }

        public ActionResult OptimumRouteList()
        {
            return View();
        }

        public JsonResult OptimumRouteListJson()
        {
            List<Webx_master_OptimumRouteMaster> ListOptimumRoute = MS.GetOptimumRouteObject();

            var ListVendorsdata = (from e in ListOptimumRoute
                                   select new
                                   {
                                       OptimumRouteCode = e.OptimumRouteCode,
                                       OriginLocation = e.OriginLocation,
                                       DeliveryLocation = e.DeliveryLocation,
                                       ActiveFlag = e.ActiveFlag,
                                       ID = e.SrNo
                                   }).ToArray();
            return Json(ListVendorsdata, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Location Connection

        public ActionResult LocationConnection(string Id)
        {
            LocationConnectionViewModel LCVM = new LocationConnectionViewModel();
            try
            {
                LCVM.EditFlag = "false";
                LCVM.locationList = MS.GetLocationDetails().Where(c => c.ActiveFlag == null ? c.ActiveFlag == "" : c.ActiveFlag.ToUpper() == "Y").ToList();
                LCVM.ListGnMST = MS.GetGeneralMasterObject();
                if (Id != null && Id != "")
                {
                    List<webx_VENDOR_HDR> ListVendor = MS.GetVendorObject();
                    LCVM.EditFlag = "true";
                    LCVM.LC = MS.GetLocationConnectionObject().Where(c => c.Code == Convert.ToInt32(Id)).FirstOrDefault();
                }
                if (LCVM.LC == null)
                {
                    LCVM.LC = new Webx_master_LocationConnection();
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(LCVM);
        }

        [HttpPost]
        public string LocationConnectionSubmit(LocationConnectionViewModel VM)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.LC.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.LC);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            bool Status = MS.InsertLocationConnection(xmlDoc.InnerXml, VM.EditFlag, BaseUserName);
            return Status.ToString();
        }

        public ActionResult LocationConnectionList()
        {
            return View();
        }

        public JsonResult LocationConnectionListJson()
        {
            List<Webx_master_LocationConnection> ListLocationConnection = MS.GetLocationConnectionObject();

            var ListLocationConnectionData = (from e in ListLocationConnection
                                              select new
                                              {
                                                  ManifestLocation = e.ManifestLocation,
                                                  Locations = e.Locations,
                                                  ActiveFlag = e.ActiveFlag,
                                                  Code = e.Code
                                              }).ToArray();
            return Json(ListLocationConnectionData, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckDuplicateLocationConnection(string Id)
        {
            int count = MS.GetLocationConnectionObject().Where(m => m.ManifestLocation == null ? m.ManifestLocation == "" : m.ManifestLocation.Trim().ToUpper() == (Id == null ? "" : Id.Trim().ToUpper())).Count();
            return Json(count);
        }

        #endregion

        #region Vehicle Type Master

        public ActionResult VehicleType()
        {
            return View(MS.GetVehicleTypeDetails());
        }

        public ActionResult AddEditVehicleType(int id)
        {
            webx_Vehicle_TypeViewModel WVTVM = new webx_Vehicle_TypeViewModel();
            WVTVM.WVT = new webx_Vehicle_Type();
            if (id > 0)
            {
                WVTVM.EditFlag = true;
                WVTVM.WVT = MS.GetVehicleTypeDetails().Where(c => c.Type_Code == id).FirstOrDefault();
            }
            return View("AddEditVehicleType", WVTVM);
        }

        [HttpPost]
        public ActionResult AddEditVehicleType(webx_Vehicle_TypeViewModel WVTVM)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WVTVM.WVT.GetType());
                WVTVM.WVT.UPDTBY = BaseUserName;
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, WVTVM.WVT);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                    var flag = WVTVM.WVT.Type_Code;
                    if (flag > 0)
                    {
                        DataTable Dt = MS.AddEditVehicleTypeMaster(xmlDoc.InnerXml, "US", BaseFinYear.Split('-')[0].ToString());
                    }
                    else
                    {
                        DataTable Dt = MS.AddEditVehicleTypeMaster(xmlDoc.InnerXml, "E", BaseFinYear.Split('-')[0].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("VehicleType");
        }

        public JsonResult GetVehicleTypeListJson()
        {
            List<webx_Vehicle_Type> listVehicleType = MS.GetVehicleTypeDetails();
            var data = (from e in listVehicleType
                        select new
                        {
                            e.Type_Code,
                            e.Type_Name,
                            e.Made_By,
                            e.ActiveFlag
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Delivery Zone Master

        // Delivery Zone List
        public ActionResult DeliveryZone()
        {
            webx_Delivery_ZoneViewModel WDZVM = new webx_Delivery_ZoneViewModel();
            try
            {
                WDZVM.listWDZM = new List<webx_Delivery_Zone>();
                WDZVM.listWDZM = MS.GetDeliveryZoneObject();
                WDZVM.WDZM = new webx_Delivery_Zone();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WDZVM);
        }

        public JsonResult GetPincodeDetails(string id)
        {
            List<webx_Pincode_Loc_Master> listPincode = new List<webx_Pincode_Loc_Master>();

            listPincode = MS.GetPincodeLocationObject().Where(c => c.LocCode == id).ToList();


            var SearchList = (from e in listPincode
                              select new
                              {
                                  Value = e.Pincode,
                                  Text = e.Pincode + ":" + e.Area,
                              }).Distinct().ToList();
            // return Json(SearchList, JsonRequestBehavior.AllowGet);


            return Json(SearchList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDeliveryZoneDetails(int ZoneID)
        {
            webx_Delivery_Zone WDZM = new webx_Delivery_Zone();
            try
            {
                if (ZoneID > 0)
                {
                    WDZM = MS.GetDeliveryZoneObject().FirstOrDefault(c => c.ZoneID == ZoneID);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditDeliveryZone", WDZM);
        }

        public ActionResult GetDeliveryZoneList()
        {
            List<webx_Delivery_Zone> listZone = new List<webx_Delivery_Zone>();
            try
            {
                listZone = MS.GetDeliveryZoneObject();
            }
            catch (Exception) { }
            return PartialView("_DeliveryZoneList", listZone);
        }

        public ActionResult AddEditDeliveryZone(webx_Delivery_Zone WDZM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<ZoneID>" + WDZM.ZoneID + "</ZoneID>";
                MstDetails = MstDetails + "<ZoneName>" + WDZM.ZoneName.Trim().ToUpper() + "</ZoneName>";
                MstDetails = MstDetails + "<Location>" + WDZM.Location + "</Location>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WDZM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy> " + BaseUserName + " </EntryBy>";
                MstDetails = MstDetails + "<ZoneDescription>" + WDZM.ZoneDescription + "</ZoneDescription>";
                MstDetails = MstDetails + "<Pincodes>" + WDZM.Pincodes + "</Pincodes></DocumentElement>";
                var flag = WDZM.ZoneID;
                DataTable Dt = new DataTable();
                if (flag > 0)
                {
                    Dt = MS.AddEditDeliveryZone(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditDeliveryZone(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());

                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult CheckDuplicateDeliveryZone(string ZoneName, string Location)
        {
            try
            {
                string Count = MS.CheckDuplicateGeneralMaster(ZoneName, Location);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        #endregion

        #region Airport Master

        public ActionResult Airport()
        {
            webx_Master_AirportViewModel WMAVM = new webx_Master_AirportViewModel();
            try
            {
                WMAVM.listWMAM = new List<webx_Master_Airport>();
                WMAVM.listWMAM = MS.GetAirportMasterObject();
                WMAVM.WMAM = new webx_Master_Airport();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMAVM);
        }

        public ActionResult GetAirportMasterDetails(int SrNo)
        {
            webx_Master_Airport WMAM = new webx_Master_Airport();
            try
            {
                if (SrNo > 0)
                {
                    WMAM = MS.GetAirportMasterObject().FirstOrDefault(c => c.SrNo == SrNo);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditAirportMaster", WMAM);
        }

        public ActionResult GetAirportMasterList()
        {
            List<webx_Master_Airport> listAirport = new List<webx_Master_Airport>();
            try
            {
                listAirport = MS.GetAirportMasterObject();
            }
            catch (Exception) { }
            return PartialView("_AirportMasterList", listAirport);
        }

        public ActionResult AddEditAirportMaster(webx_Master_Airport WMAM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<SrNo>" + WMAM.SrNo + "</SrNo>";
                MstDetails = MstDetails + "<AirportCode>" + WMAM.AirportCode + "</AirportCode>";
                MstDetails = MstDetails + "<AirportName>" + WMAM.AirportName.Trim().ToUpper() + "</AirportName>";
                MstDetails = MstDetails + "<City>" + WMAM.City + "</City>";
                MstDetails = MstDetails + "<AlternateCity>" + WMAM.AlternateCity + "</AlternateCity>";
                MstDetails = MstDetails + "<CountryCode>" + WMAM.CountryCode + "</CountryCode>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WMAM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy>"+ BaseUserName +" </EntryBy></DocumentElement>";
                var flag = WMAM.SrNo;
                DataTable Dt = new DataTable();
                if (flag > 0)
                {
                    Dt = MS.AddEditAirportMaster(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditAirportMaster(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());

                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Flight_Schedule Master

        public ActionResult FlightSchedule()
        {

            return View(MS.GetFlight_ScheduleDetails());
        }

        public ActionResult AddEditFlightScheduleMaster(string id)
        {
            webx_Master_Flight_Schedule_DetViewModel WMFSDVM = new webx_Master_Flight_Schedule_DetViewModel();
            List<webx_Master_Flight_Schedule_Det> VRM = new List<webx_Master_Flight_Schedule_Det>();
            WMFSDVM.listWMFSDT = VRM;
            WMFSDVM.WMFSDT = new webx_Master_Flight_Schedule_Det();
            WMFSDVM.WMFSHT = new webx_Master_Flight_Schedule_Hdr();
            if (id != null && id != "0")
            {
                //WMFSDVM.EditFlag = true;
                WMFSDVM.WMFSDT = MS.GetFlight_ScheduleDetails().Where(c => c.FlightCode == id).FirstOrDefault();
                WMFSDVM.WMFSHT = MS.GetFlightDetails().Where(c => c.FlightCode == id).FirstOrDefault();
                WMFSDVM.listWMFSDT = MS.GetFlight_ScheduleDetails().Where(c => c.FlightCode == id).ToList();
                WMFSDVM.listWMFSHT = MS.GetFlightDetails().Where(c => c.FlightCode == id).ToList();
            }

            return View("AddEditFlightScheduleMaster", WMFSDVM);
        }

        [HttpPost]
        public string AddEditFlightScheduleMaster(webx_Master_Flight_Schedule_DetViewModel WMFSDVM, List<webx_Master_Flight_Schedule_Det> Flight_Schedule)
        {
            bool Status = false;
            webx_Master_Flight_Schedule_DetViewModel Flight_ScheduleList = new webx_Master_Flight_Schedule_DetViewModel();
            Flight_ScheduleList.listWMFSDT = Flight_Schedule.ToList();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WMFSDVM.WMFSDT.GetType());
                string MstHdrDetails = "<DocumentElementHdr>";
                MstHdrDetails = MstHdrDetails + "<SrNo>" + WMFSDVM.WMFSDT.SrNo + "</SrNo>";
                MstHdrDetails = MstHdrDetails + "<FlightCode>" + WMFSDVM.WMFSHT.FlightCode + "</FlightCode>";
                MstHdrDetails = MstHdrDetails + "<AirlineCode>" + WMFSDVM.WMFSHT.AirlineCode + "</AirlineCode>";
                MstHdrDetails = MstHdrDetails + "<FlightNumber>" + WMFSDVM.WMFSHT.FlightNumber + "</FlightNumber>";
                MstHdrDetails = MstHdrDetails + "<ActiveFlag>" + WMFSDVM.WMFSHT.ActiveFlag + "</ActiveFlag>";
                MstHdrDetails = MstHdrDetails + "</DocumentElementHdr>";
                string MstDetails = "<root>";
                foreach (var row in Flight_ScheduleList.listWMFSDT)
                {
                    //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                    MstDetails = MstDetails + "<DocumentElement>";
                    MstDetails = MstDetails + "<FromAirportCode>" + row.FromAirportCode + "</FromAirportCode>";
                    MstDetails = MstDetails + "<ToAirportCode>" + row.ToAirportCode + "</ToAirportCode>";
                    MstDetails = MstDetails + "<Dep_Time>" + row.Dep_Time + "</Dep_Time>";
                    MstDetails = MstDetails + "<Arr_Time>" + row.Arr_Time + "</Arr_Time>";
                    MstDetails = MstDetails + "<Days>" + row.Days + "</Days>";
                    MstDetails = MstDetails + "<Stoppage_Min>" + row.Stoppage_Min + "</Stoppage_Min>";
                    MstDetails = MstDetails + "</DocumentElement>";
                }
                MstDetails = MstDetails + "</root>";
                if (WMFSDVM.WMFSDT.SrNo > 0)
                {
                    Status = MS.AddEditFlight_Schedule(MstHdrDetails, MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Status = MS.AddEditFlight_Schedule(MstHdrDetails, MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
            }
            catch (Exception) { }
            return Status.ToString();
        }

        public JsonResult GetFlightScheduleListJson()
        {
            List<Webx_Master_General> airlineList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "ALN" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<webx_Master_Flight_Schedule_Hdr> listFlightSchedule = MS.GetFlightDetails();
            var data = (from e in listFlightSchedule
                        select new
                        {
                            e.FlightCode,
                            AirlineCode = e.AirlineCode == null || e.AirlineCode == "" ? "" : (airlineList.Where(c => c.CodeId == e.AirlineCode).FirstOrDefault().CodeDesc),
                            e.FlightNumber,
                            e.ActiveFlag,
                            e.SrNo
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ADDFlightSchedualInfo(int id)
        {
            webx_Master_Flight_Schedule_Det WMFSM = new webx_Master_Flight_Schedule_Det();
            WMFSM.SrNo = id;
            return PartialView("_PartialFlightSchedual", WMFSM);
        }

        #endregion

        #region City Location Mapping

        public ActionResult CityLocationMapping()
        {
            Webx_master_CityLocationMappingViewModel WMCLMVM = new Webx_master_CityLocationMappingViewModel();
            try
            {
                WMCLMVM.listWMCLMM = new List<Webx_master_CityLocationMapping>();
                WMCLMVM.listWMCLMM = MS.GetCityMappingLocationObject();
                WMCLMVM.WMCLMM = new Webx_master_CityLocationMapping();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WMCLMVM);
        }

        public ActionResult GetCityLocationMappingDetails(int Code)
        {
            Webx_master_CityLocationMapping WMCLMM = new Webx_master_CityLocationMapping();
            try
            {
                if (Code > 0)
                {
                    WMCLMM = MS.GetCityMappingLocationObject().FirstOrDefault(c => c.Code == Code);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditCityLocationMapping", WMCLMM);
        }

        public ActionResult GetCityLocationMappingList()
        {
            List<Webx_master_CityLocationMapping> listCityLocation = new List<Webx_master_CityLocationMapping>();
            try
            {
                listCityLocation = MS.GetCityMappingLocationObject();
            }
            catch (Exception) { }
            return PartialView("_CityLocationMappingList", listCityLocation);
        }

        public ActionResult AddEditCityLocationMapping(Webx_master_CityLocationMapping WMCLMM)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<DocumentElement>";
                MstDetails = MstDetails + "<Code>" + WMCLMM.Code + "</Code>";
                MstDetails = MstDetails + "<City>" + WMCLMM.city_code + "</City>";
                MstDetails = MstDetails + "<Locations>" + WMCLMM.Locations + "</Locations>";
                MstDetails = MstDetails + "<ActiveFlag>" + (WMCLMM.ActiveFlag == "True" ? "Y" : "N") + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName + "</EntryBy></DocumentElement>";
                DataTable Dt = new DataTable();
                if (WMCLMM.Code > 0)
                {
                    Dt = MS.AddEditCityLocationMapping(MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditCityLocationMapping(MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult CityLocationMappingListJson()
        {
            List<Webx_master_CityLocationMapping> listCityLocation = MS.GetCityMappingLocationObject();

            var ListVehiclesdata = (from e in listCityLocation
                                    select new
                                    {
                                        e.City,
                                        e.Locations,
                                        e.ActiveFlag,
                                        e.Code
                                    }).ToArray();

            return Json(ListVehiclesdata, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCityFromLocation(string id)
        {
            List<Webx_master_CityLocationMapping> listCityLocation = MS.GetCityFromLocation(id);

            var ListCity = (from e in listCityLocation
                                    select new
                                    {
                                        Value = e.city_code,
                                        Text = e.City,
                                    }).Distinct().ToList();
            return Json(ListCity, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Route City Master

        public ActionResult RouteCity()
        {

            return View(MS.GetRutTranCityDetails());
        }

        public ActionResult AddEditRouteCity(string id)
        {
            webx_rutMST_TRAN_CityViewModel WRMTCVM = new webx_rutMST_TRAN_CityViewModel();
            List<webx_ruttran_City> WRTCM = new List<webx_ruttran_City>();
            WRMTCVM.listWRTCT = WRTCM;
            WRMTCVM.WRMCT = new webx_rutmas_City();
            WRMTCVM.WRTCT = new webx_ruttran_City();
            if (id != null && id != "0")
            {
                //WMFSDVM.EditFlag = true;
                //WRMTCVM.WRTCT = MS.GetRutTranCityDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTCVM.listWRTCT = MS.GetRutTranCityDetails().Where(c => c.RUTCD == id).ToList();

                WRMTCVM.WRMCT = MS.GetRutMstCityDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTCVM.listWRMCT = MS.GetRutMstCityDetails().Where(c => c.RUTCD == id).ToList();
            }

            return View("AddEditRouteCity", WRMTCVM);
        }

        [HttpPost]
        public ActionResult AddEditRouteCity(webx_rutMST_TRAN_CityViewModel WRMTCVM, List<webx_ruttran_City> RutTran)
        {
            webx_rutMST_TRAN_CityViewModel RutCityList = new webx_rutMST_TRAN_CityViewModel();
            RutCityList.listWRTCT = RutTran.ToList();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WRMTCVM.WRMCT.GetType());
                string MstHdrDetails = "<DocumentElementHdr>";
                MstHdrDetails = MstHdrDetails + "<RUTCD>" + WRMTCVM.WRMCT.RUTCD + "</RUTCD>";
                MstHdrDetails = MstHdrDetails + "<RUTSTBR>" + WRMTCVM.WRMCT.RUTSTBR + "</RUTSTBR>";
                MstHdrDetails = MstHdrDetails + "<RUTENDBR>" + WRMTCVM.WRMCT.RUTENDBR + "</RUTENDBR>";
                MstHdrDetails = MstHdrDetails + "<RUTCAT>" + WRMTCVM.WRMCT.RUTCAT + "</RUTCAT>";
                MstHdrDetails = MstHdrDetails + "<RUTKM>" + WRMTCVM.WRMCT.RUTKM + "</RUTKM>";
                //MstHdrDetails = MstHdrDetails + "<RUTSTDT>" + WRMTCVM.WRMCT.RUTSTDT + "</RUTSTDT>";
                //MstHdrDetails = MstHdrDetails + "<RUTENDDT>" + WRMTCVM.WRMCT.RUTENDDT + "</RUTENDDT>";
                MstHdrDetails = MstHdrDetails + "<ACTIVEFLAG>" + WRMTCVM.WRMCT.ACTIVEFLAG + "</ACTIVEFLAG>";
                MstHdrDetails = MstHdrDetails + "<RUTNM>" + WRMTCVM.WRMCT.RUTNM + "</RUTNM>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_HR>" + WRMTCVM.WRMCT.SCHDEP_HR + "</SCHDEP_HR>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_MIN>" + WRMTCVM.WRMCT.SCHDEP_MIN + "</SCHDEP_MIN>";
                MstHdrDetails = MstHdrDetails + "<STD_CONTAMT>" + WRMTCVM.WRMCT.STD_CONTAMT + "</STD_CONTAMT>";
                MstHdrDetails = MstHdrDetails + "<RUTMOD>" + WRMTCVM.WRMCT.RUTMOD + "</RUTMOD>";
                MstHdrDetails = MstHdrDetails + "<ControlLoc>" + WRMTCVM.WRMCT.ControlLoc + "</ControlLoc>";
                MstHdrDetails = MstHdrDetails + "<TransHrs>" + WRMTCVM.WRMCT.TransHrs + "</TransHrs>";
                MstHdrDetails = MstHdrDetails + "</DocumentElementHdr>";
                string MstDetails = "<root>";
                foreach (var row in RutCityList.listWRTCT)
                {
                    //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                    MstDetails = MstDetails + "<DocumentElement>";
                    MstDetails = MstDetails + "<RUTNO>" + row.RUTNO + "</RUTNO>";
                    MstDetails = MstDetails + "<LOCCD>" + row.LOCCD + "</LOCCD>";
                    MstDetails = MstDetails + "<DIST_KM>" + row.DIST_KM + "</DIST_KM>";
                    MstDetails = MstDetails + "<TRTIME_HR>" + row.TRTIME_HR + "</TRTIME_HR>";
                    MstDetails = MstDetails + "<TRTIME_MIN>" + row.TRTIME_MIN + "</TRTIME_MIN>";
                    MstDetails = MstDetails + "<STTIME_HR>" + row.STTIME_HR + "</STTIME_HR>";
                    MstDetails = MstDetails + "<STTIME_MIN>" + row.STTIME_MIN + "</STTIME_MIN>";
                    MstDetails = MstDetails + "</DocumentElement>";
                }
                MstDetails = MstDetails + "</root>";
                if (WRMTCVM.WRMCT.RUTCD != null)
                {
                    DataTable Dt = MS.AddEditRutCity(MstHdrDetails, MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    DataTable Dt = MS.AddEditRutCity(MstHdrDetails, MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
            }
            catch (Exception) { }
            return RedirectToAction("RouteCity");
        }

        public JsonResult GetRouteCityListJson()
        {
            List<Webx_Master_General> rutmodeList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTMD" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<Webx_Master_General> rutcatList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTCT" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<webx_citymaster> cityList = MS.GetCityMasterObject().Where(c => c.activeflag == "Y").ToList().OrderBy(c => c.Location).ToList();
            List<webx_rutmas_City> listRutCity = MS.GetRutMstCityDetails();
            var data = (from e in listRutCity
                        select new
                        {
                            e.RUTCD,
                            //AirlineCode = airlineList.Where(c => c.CodeType == "ALN" && c.StatusCode == "Y").FirstOrDefault().CodeDesc,
                            e.RUTNM,
                            RUTMOD = rutmodeList.Where(c => c.CodeType == "RTMD" && c.CodeId == e.RUTMOD).FirstOrDefault().CodeDesc,
                            e.RUTKM,
                           // e.ControlLoc,//= cityList.Where(c => c.city_code.ToString() == e.ControlLoc).FirstOrDefault().Location,
                            e.ControlLocation,
                            e.ACTIVEFLAG,
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ADDRouteCityInfo(string id)
        {
            webx_ruttran_City WRTCM = new webx_ruttran_City();
            WRTCM.RUTCD = id;
            return PartialView("_PartialRouteCity", WRTCM);
        }

        #endregion

        #region Route Location Master

        public ActionResult RouteLocation()
        {
            return View(MS.GetRutTranDetails());
        }

        public ActionResult AddEditRouteLocation(string id)
        {
            webx_rutMST_TRANViewModel WRMTVM = new webx_rutMST_TRANViewModel();
            List<webx_ruttran> WRTM = new List<webx_ruttran>();
            WRMTVM.listWRTT = WRTM;
            WRMTVM.WRMT = new webx_rutmas();
            WRMTVM.WRTT = new webx_ruttran();
            if (id != null && id != "0")
            {
                //WMFSDVM.EditFlag = true;
                WRMTVM.WRTT = MS.GetRutTranDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTVM.listWRTT = MS.GetRutTranDetails().Where(c => c.RUTCD == id).ToList();

                WRMTVM.WRMT = MS.GetRutMstDetails().Where(c => c.RUTCD == id).FirstOrDefault();
                WRMTVM.listWRMT = MS.GetRutMstDetails().Where(c => c.RUTCD == id).ToList();
            }

            return View(WRMTVM);
        }

        [HttpPost]
        public ActionResult AddEditRouteLocation(webx_rutMST_TRANViewModel WRMTVM, List<webx_ruttran> RutTran)
        {
            webx_rutMST_TRANViewModel RutList = new webx_rutMST_TRANViewModel();
            RutList.listWRTT = RutTran.ToList();
            bool Status = false;
            string RutName = "";
            string RouteCode = "";
            WRMTVM.WRMT.RUTSTBR = WRMTVM.WRMT.ControlLoc;
            try
            {
                if (RutTran != null)
                {
                    foreach (var item in RutTran)
                    {
                        if (RutName == "" || RutName == null)
                            RutName = item.LOCCD;
                        else
                            RutName = RutName + "-" + item.LOCCD;
                    }
                }
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WRMTVM.WRMT.GetType());
                string MstHdrDetails = "<DocumentElementHdr>";
                MstHdrDetails = MstHdrDetails + "<RUTCD>" + WRMTVM.WRMT.RUTCD + "</RUTCD>";
                MstHdrDetails = MstHdrDetails + "<RUTSTBR>" + WRMTVM.WRMT.RUTSTBR + "</RUTSTBR>";
                MstHdrDetails = MstHdrDetails + "<RUTENDBR>" + WRMTVM.WRMT.RUTENDBR + "</RUTENDBR>";
                MstHdrDetails = MstHdrDetails + "<RUTCAT>" + WRMTVM.WRMT.RUTCAT + "</RUTCAT>";
                MstHdrDetails = MstHdrDetails + "<RUTKM>" + WRMTVM.WRMT.RUTKM + "</RUTKM>";
                //MstHdrDetails = MstHdrDetails + "<RUTSTDT>" + WRMTVM.WRMT.RUTSTDT + "</RUTSTDT>";
                //MstHdrDetails = MstHdrDetails + "<RUTENDDT>" + WRMTVM.WRMT.RUTENDDT + "</RUTENDDT>";
                MstHdrDetails = MstHdrDetails + "<ACTIVEFLAG>" + WRMTVM.WRMT.ACTIVEFLAG + "</ACTIVEFLAG>";
                MstHdrDetails = MstHdrDetails + "<RUTNM>" + RutName + "</RUTNM>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_HR>" + WRMTVM.WRMT.SCHDEP_HR + "</SCHDEP_HR>";
                MstHdrDetails = MstHdrDetails + "<SCHDEP_MIN>" + WRMTVM.WRMT.SCHDEP_MIN + "</SCHDEP_MIN>";
                MstHdrDetails = MstHdrDetails + "<STD_CONTAMT>" + WRMTVM.WRMT.STD_CONTAMT + "</STD_CONTAMT>";
                MstHdrDetails = MstHdrDetails + "<RUTMOD>" + WRMTVM.WRMT.RUTMOD + "</RUTMOD>";
                MstHdrDetails = MstHdrDetails + "<ControlLoc>" + WRMTVM.WRMT.ControlLoc + "</ControlLoc>";
                MstHdrDetails = MstHdrDetails + "<TransHrs>" + WRMTVM.WRMT.TransHrs + "</TransHrs>";
                MstHdrDetails = MstHdrDetails + "<OnTime>" + WRMTVM.WRMT.OnTime + "</OnTime>";
                MstHdrDetails = MstHdrDetails + "<Early>" + WRMTVM.WRMT.Early + "</Early>";
                MstHdrDetails = MstHdrDetails + "<Late>" + WRMTVM.WRMT.Late + "</Late>";
                MstHdrDetails = MstHdrDetails + "</DocumentElementHdr>";
                string MstDetails = "<root>";
                foreach (var row in RutList.listWRTT)
                {
                    //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                    MstDetails = MstDetails + "<DocumentElement>";
                    MstDetails = MstDetails + "<RUTNO>" + row.RUTNO + "</RUTNO>";
                    MstDetails = MstDetails + "<LOCCD>" + row.LOCCD + "</LOCCD>";
                    MstDetails = MstDetails + "<DIST_KM>" + row.DIST_KM + "</DIST_KM>";
                    MstDetails = MstDetails + "<TRTIME_HR>" + row.TRTIME_HR + "</TRTIME_HR>";
                    MstDetails = MstDetails + "<TRTIME_MIN>" + row.TRTIME_MIN + "</TRTIME_MIN>";
                    MstDetails = MstDetails + "<STTIME_HR>" + row.STTIME_HR + "</STTIME_HR>";
                    MstDetails = MstDetails + "<STTIME_MIN>" + row.STTIME_MIN + "</STTIME_MIN>";
                    MstDetails = MstDetails + "</DocumentElement>";
                }
                MstDetails = MstDetails + "</root>";
                DataTable Dt = new DataTable();
                if (WRMTVM.WRMT.RUTCD != null)
                {
                    Dt = MS.AddEditRut(MstHdrDetails, MstDetails, "US", BaseFinYear.Split('-')[0].ToString());
                }
                else
                {
                    Dt = MS.AddEditRut(MstHdrDetails, MstDetails, "E", BaseFinYear.Split('-')[0].ToString());
                }
                RouteCode = Dt.Rows[0][0].ToString();
                //Department = MS.GetDepartmentObject_XML();
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                        RouteCode = RouteCode,
                    }
                };

            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult GetRouteListJson()
        {
            List<Webx_Master_General> rutmodeList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTMD" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<Webx_Master_General> rutcatList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "RTCT" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();
            List<webx_location> locList = MS.GetLocationDetails().Where(c => c.ActiveFlag == "Y").ToList().OrderBy(c => c.LocName).ToList();
            List<webx_rutmas> listRut = MS.GetRutMstDetails();
            var data = (from e in listRut
                        select new
                        {
                            e.RUTCD,
                            //AirlineCode = airlineList.Where(c => c.CodeType == "ALN" && c.StatusCode == "Y").FirstOrDefault().CodeDesc,
                            e.RUTNM,
                            RUTMOD = rutmodeList.Where(c => c.CodeType == "RTMD" /*&& c.CodeId == e.RUTMOD*/ && c.CodeId == "S").FirstOrDefault().CodeDesc,
                            e.RUTKM,
                            e.ControlLoc,// = locList.Where(c => c.LocCode == "" ? c.LocCode == "" : c.LocCode.ToString() == e.ControlLoc).FirstOrDefault().LocName,
                            e.ACTIVEFLAG,
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }

          
        public ActionResult ADDRouteInfo(int id)
        {
            webx_ruttran WRTM = new webx_ruttran();
            if (id == 1)
            {
                WRTM.DIST_KM = 0;
                WRTM.TRTIME_HR = "0";
                WRTM.TRTIME_MIN = "0";
                WRTM.STTIME_HR = "0";
                WRTM.STTIME_MIN = "0";
            }
            WRTM.RUTNO = id;
            return PartialView("_PartialRouteLocation", WRTM);
        }

        public JsonResult CheckDuplicateRouteName(string RouteName, string RouteCode)
        {
            try
            {
                string Count = MS.GetRutMstDetails().Where(c => c.RUTNM.ToUpper() == RouteName.ToUpper() && c.RUTCD.ToUpper() != RouteCode.ToUpper()).Count().ToString();
                return Json(new { Count = Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        public ActionResult AddEditRouteLocationDone(string RouteCode, string TranXaction)
        {
            ViewBag.RouteCode = RouteCode;
            return View();
        }

        #endregion

        #region Vehicle Master

        public ActionResult VehicleList()
        {
            return View();
        }

        public ActionResult Vehicle(string Id)
        {
            VehicleViewModel VVM = new VehicleViewModel();
            try
            {
                VVM.EditFlag = "false";
                VVM.VehicleHDR = new webx_VEHICLE_HDR();
                VVM.listVehicleType = MS.GetVehicleTypeObject();
                VVM.locationList = MS.GetLocationDetails().Where(c => c.ActiveFlag == null ? c.ActiveFlag == "" : c.ActiveFlag.ToUpper() == "Y").ToList();
                VVM.StatePermitList = MS.GetStateMasterObject();
                if (Id != null && Id != "")
                {
                    List<webx_VEHICLE_HDR> ListVehicle = MS.GetVehicleObject();
                    VVM.EditFlag = "true";
                    VVM.VehicleHDR = MS.GetVehicleObject().Where(c => c.VEHNO == null ? c.VEHNO == "" : c.VEHNO.ToUpper().Trim() == Id.ToUpper()).FirstOrDefault();
                    //== Id.ToUpper()).FirstOrDefault();
                    List<webx_location> WLList = MS.GetLocationDetails().Where(c => c.LocCode == VVM.VehicleHDR.Conrtl_branch).ToList();
                    webx_location WL = new webx_location();
                    if (WLList.Count != 0)
                    {
                        WL = WLList.First();
                        VVM.VehicleHDR.Conrtl_branch_Name = WL.LocName;
                    }

                    List<WebX_Master_Users> WMUList = MS.GetUserDetails().Where(c => c.UserId == VVM.VehicleHDR.Coordinator && c.Status == "100").ToList();
                    WebX_Master_Users WMU = new WebX_Master_Users();
                    if (WMUList.Count != 0)
                    {
                        WMU = WMUList.First();
                        VVM.VehicleHDR.Coordinator_Name = WMU.Name;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(VVM);
        }

        [HttpPost]
        public string VehicleMasterSubmit(VehicleViewModel VM)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.VehicleHDR.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.VehicleHDR);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            bool Status = MS.InsertVehicle(xmlDoc.InnerXml, VM.EditFlag, BaseUserName);
            return Status.ToString();
        }

        public JsonResult GetVehicleListJson()
        {
            List<webx_VEHICLE_HDR> ListVehicles = MS.GetVehicleObject();
            List<webx_Vehicle_Type> listVehicleType = MS.GetVehicleTypeObject();
            List<WEBX_FLEET_TYRESIZEMST> TyreSizeList = MS.GetTyreSizeList();

            var ListVehiclesdata = (from e in ListVehicles
                                    select new
                                    {
                                        VEHNO = e.VEHNO,
                                        TyreSize = string.IsNullOrEmpty(e.TyreSize) ? null : TyreSizeList.Where(m => m.TYRE_SIZEID.ToUpper() == e.TyreSize.ToUpper()).FirstOrDefault().TYRE_SIZENAME,
                                        ACTIVEFLAG = e.ACTIVEFLAG,
                                        Vehicle_Type = string.IsNullOrEmpty(e.Vehicle_Type) ? null : listVehicleType.Where(c => c.Type_Code == Convert.ToInt32(e.Vehicle_Type)).FirstOrDefault().Type_Name,
                                    }).ToArray();

            return Json(ListVehiclesdata, JsonRequestBehavior.AllowGet);
        }

        //Fill Dropdown
        public JsonResult GetFillVendorListJson(string Verndortype)
        {
            List<webx_VENDOR_HDR> ListVendorHDR = MS.GetVendorObject().Where(c => c.Vendor_Type == Verndortype).OrderBy(c => c.VENDORNAME).ToList();

            var ListVendor = (from e in ListVendorHDR
                              select new
                              {
                                  Value = e.VENDORCODE,
                                  Text = e.VENDORNAME,
                              }).ToArray();
            return Json(ListVendor, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckDuplicateVehicle(string Id)
        {
            int count = MS.GetVehicleObject().Where(m => m.VEHNO == null ? m.VEHNO == "" : m.VEHNO.Trim().ToUpper() == (Id == null ? "" : Id.Trim().ToUpper())).Count();
            return Json(count);
        }

        #endregion

        #region Tyre Masters

        #region Tyre Pattern

        public ActionResult TyrePattern()
        {
            return View();
        }

        public JsonResult GetTyrePatternListJson()
        {
            List<WEBX_FLEET_TYREPATTERN> ListTyrePattern = MS.GetTyrePatternList();

            var ListTyrePatterndata = (from e in ListTyrePattern
                                       select new
                                       {
                                           e.TYREPAT_CODE,
                                           e.TYRE_PATTERN_DESC,
                                           e.TyrePosition,
                                           e.ACTIVE_FLAG,
                                           e.TYRE_PATTERNID,
                                       }).ToArray();

            return Json(ListTyrePatterndata.OrderBy(c => c.TYREPAT_CODE), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTyrePatternDetails(int id)
        {
            WEBX_FLEET_TYREPATTERN WFTP = new WEBX_FLEET_TYREPATTERN();
            try
            {
                if (id > 0)
                {
                    WFTP = MS.GetTyrePatternList().FirstOrDefault(c => c.TYRE_PATTERNID == id);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTyrePattern", WFTP);
        }

        public JsonResult CheckDuplicateTyrePatternCode(int id, string code)
        {
            try
            {
                int Count = MS.GetTyrePatternList().Where(c => c.TYREPAT_CODE.ToUpper() == code.ToUpper() && c.TYRE_PATTERNID != id).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string AddEditTyrePattern(WEBX_FLEET_TYREPATTERN WFTP)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><MST>";
                MstDetails = MstDetails + "<TYRE_PATTERN_DESC>" + WFTP.TYRE_PATTERN_DESC + "</TYRE_PATTERN_DESC>";
                MstDetails = MstDetails + "<POS_ALLOWED>" + WFTP.POS_ALLOWED + "</POS_ALLOWED>";
                MstDetails = MstDetails + "<TYREPAT_CODE>" + WFTP.TYREPAT_CODE.ToUpper() + "</TYREPAT_CODE>";
                MstDetails = MstDetails + "<ACTIVE_FLAG>" + (WFTP.ACTIVE_FLAG == "True" ? "Y" : "N") + "</ACTIVE_FLAG></MST></root>";

                Status = MS.InsertUpdateTyrePattern(MstDetails, WFTP.TYRE_PATTERNID);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        #endregion

        #region Tyre Size

        public ActionResult TyreSize()
        {
            return View();
        }

        public JsonResult GetTyreSizeListJson()
        {
            List<WEBX_FLEET_TYRESIZEMST> ListTyreSize = MS.GetTyreSizeList();

            var ListTyreSizedata = (from e in ListTyreSize
                                    select new
                                    {
                                        e.TYRE_SIZEID,
                                        e.TYRE_SIZENAME,
                                        e.TYRETYPE,
                                        e.SIZE_ACTIVEFLAG
                                    }).ToArray();

            return Json(ListTyreSizedata.OrderBy(c => c.TYRE_SIZENAME), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTyreSizeDetails(string id)
        {
            WEBX_FLEET_TYRESIZEMST WFTS = new WEBX_FLEET_TYRESIZEMST();
            try
            {
                if (id != null)
                {
                    WFTS = MS.GetTyreSizeList().FirstOrDefault(c => c.TYRE_SIZEID == id);
                }
                else
                {
                    WFTS.TYRE_SIZEID = "0";
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTyreSize", WFTS);
        }

        public JsonResult CheckDuplicateTyreSizeCode(string TYRE_SIZEID, string TYRE_SIZENAME)
        {
            try
            {
                int Count = MS.GetTyreSizeList().Where(c => c.TYRE_SIZENAME.ToUpper() == TYRE_SIZENAME.ToUpper() && c.TYRE_SIZEID.ToUpper() != TYRE_SIZEID.ToUpper()).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string AddEditTyreSize(WEBX_FLEET_TYRESIZEMST WFTS)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><MST>";
                MstDetails = MstDetails + "<TYRE_SIZENAME>" + WFTS.TYRE_SIZENAME.ToUpper() + "</TYRE_SIZENAME>";
                MstDetails = MstDetails + "<TYRE_TYPEID>" + WFTS.TYRE_TYPEID + "</TYRE_TYPEID>";
                MstDetails = MstDetails + "<SIZE_ENTRYBY>" + BaseUserName + "</SIZE_ENTRYBY>";
                MstDetails = MstDetails + "<SIZE_ACTIVEFLAG>" + (WFTS.SIZE_ACTIVEFLAG == "True" ? "Y" : "N") + "</SIZE_ACTIVEFLAG></MST></root>";

                Status = MS.InsertUpdateTyreSize(MstDetails, WFTS.TYRE_SIZEID);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        #endregion

        #region Tyre Manufacture

        public ActionResult TyreManufacture()
        {
            return View();
        }

        public JsonResult GetTYREMFGListJson()
        {
            List<WEBX_FLEET_TYREMFG> ListTYREMFG = MS.GetTYREMFGList();

            var ListTYREMFGdata = (from e in ListTYREMFG
                                   select new
                                   {
                                       e.MFG_NAME,
                                       e.MFG_TELNO,
                                       e.MFG_ADDR,
                                       e.MFG_ACTIVEFLAG,
                                       e.MFG_ID,
                                   }).ToArray();

            return Json(ListTYREMFG, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTYREMFGDetails(string id)
        {
            WEBX_FLEET_TYREMFG WFTMFG = new WEBX_FLEET_TYREMFG();
            try
            {
                if (id != null && id != "" && id != "0")
                {
                    WFTMFG = MS.GetTYREMFGList().FirstOrDefault(c => c.MFG_ID == id);
                }
                else
                {
                    WFTMFG = new WEBX_FLEET_TYREMFG();
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTYREMFG", WFTMFG);
        }

        public JsonResult CheckDuplicateTYREMFGCode(string code, string id)
        {
            try
            {
                int Count = MS.GetTYREMFGList().Where(c => c.MFG_NAME == null ? c.MFG_NAME == "" : c.MFG_NAME.ToUpper() == code && c.MFG_ID.ToUpper() != id.ToUpper()).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string AddEditTYREMFG(WEBX_FLEET_TYREMFG WFTP)
        {
            bool Status = false;
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WFTP.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, WFTP);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                Status = MS.InsertUpdateTYREMFG(xmlDoc.InnerXml);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        #endregion

        #region Tyre Model

        public ActionResult TyreModel()
        {
            return View();
        }

        public JsonResult GetTyreModelListJson()
        {
            List<WEBX_FLEET_TYREMODELMST> ListTyreModel = MS.GetTyreModelList();

            var ListTyreModeldata = (from e in ListTyreModel
                                     select new
                                     {
                                         e.TYRE_MODEL_ID,
                                         e.MODEL_NO,
                                         e.MODEL_DESC,
                                         e.MFG_NAME,
                                         e.TYREPAT_CODE,
                                         e.TYRE_SIZENAME,
                                         e.ACTIVE_FLAG
                                     }).ToArray();

            return Json(ListTyreModeldata.OrderBy(c => c.MODEL_NO), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTyreModelDetails(int? id)
        {
            WEBX_FLEET_TYREMODELMST WFTM = new WEBX_FLEET_TYREMODELMST();
            try
            {
                if (id != null && id > 0)
                {
                    WFTM = MS.GetTyreModelList().FirstOrDefault(c => c.TYRE_MODEL_ID == id);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTyreModel", WFTM);
        }

        public JsonResult CheckDuplicateTyreModelCode(int TYRE_MODEL_ID, string MODEL_NO)
        {
            try
            {
                int Count = MS.GetTyreModelList().Where(c => c.MODEL_NO.ToUpper() == MODEL_NO.ToUpper() && c.TYRE_MODEL_ID != TYRE_MODEL_ID).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string AddEditTyreModel(WEBX_FLEET_TYREMODELMST WFTS)
        {
            bool Status = false;
            try
            {
                WFTS.MODEL_NO = WFTS.MODEL_NO.ToUpper();
                WFTS.ACTIVE_FLAG = WFTS.ACTIVE_FLAG == "true" ? "Y" : "N";

                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WFTS.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, WFTS);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                Status = MS.InsertUpdateTyreModel(xmlDoc.InnerXml, WFTS.TYRE_MODEL_ID);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        #endregion

        #region Tyre Position

        public ActionResult TyrePosition()
        {
            return View();
        }

        public JsonResult GetTYREPOSListJson()
        {
            List<WEBX_FLEET_TYREPOS> ListTYREPOS = MS.GetTYREPOSList();

            List<Webx_Master_General> MSTGenList = MS.GetGeneralMasterObject();
            var vehCatList = MSTGenList.Where(m => m.CodeType == "VHCAT").ToList();
            var vehTYPOS = MSTGenList.Where(m => m.CodeType == "TYPOS").ToList();
            var ListTYREPOSdata = (from e in ListTYREPOS
                                   select new
                                   {
                                       e.TYREPOS_CODE,
                                       e.TYREPOS_DESC,
                                       POS_ALLOWED = vehTYPOS.Where(m => m.CodeId.ToUpper() == e.POS_ALLOWED.ToUpper()).FirstOrDefault() != null ? vehTYPOS.Where(m => m.CodeId.ToUpper() == e.POS_ALLOWED.ToUpper()).FirstOrDefault().CodeDesc : "",
                                       TRUCK_TRAILER = vehCatList.Where(m => m.CodeId.ToUpper() == e.TRUCK_TRAILER.ToUpper()).FirstOrDefault() != null ? vehCatList.Where(m => m.CodeId.ToUpper() == e.TRUCK_TRAILER.ToUpper()).FirstOrDefault().CodeDesc : "",
                                       e.ACTIVE_FLAG,
                                       e.TYREPOS_ID,
                                   }).ToArray();

            return Json(ListTYREPOSdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTYREPOSDetails(string id)
        {
            WEBX_FLEET_TYREPOS WFTPOS = new WEBX_FLEET_TYREPOS();
            try
            {
                if (id != null && id != "" && id != "0")
                {
                    WFTPOS = MS.GetTYREPOSList().FirstOrDefault(c => c.TYREPOS_ID == Convert.ToInt32(id));
                }
                else
                {
                    WFTPOS = new WEBX_FLEET_TYREPOS();
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTYREPOS", WFTPOS);
        }

        public JsonResult CheckDuplicateTYREPOSCode(string VehCat, string POSCode)
        {
            try
            {
                int Count = MS.GetTYREPOSList().Where(c => c.TRUCK_TRAILER == null ? c.TRUCK_TRAILER == "" : c.TRUCK_TRAILER.ToUpper() == VehCat.ToUpper() && c.TYREPOS_CODE.ToUpper() == POSCode.ToUpper()).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string AddEditTYREPOS(WEBX_FLEET_TYREPOS WFTP)
        {
            bool Status = false;
            try
            {
                WFTP.COMPANY_CODE = BaseCompanyCode;
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WFTP.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, WFTP);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }
                Status = MS.InsertUpdateTYREPOS(xmlDoc.InnerXml);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        #endregion

        [HttpPost]
        public string GetData(string Method, string param1, string param2, string param3, string param4)
        {
            return JsonConvert.SerializeObject(MS.GetData(Method, param1, param2, param3, param4));
        }

        #region Tyre Masters

        public ActionResult Tyre()
        {
            return View();
        }

        public ActionResult AddEditTyre(string TyreId)
        {
            TyreViewModel VVM = new TyreViewModel();
            try
            {
                VVM.EditFlag = "false";
                VVM.tyreMST = new WEBX_FLEET_TYREMST();
                VVM.ListMFG = MS.GetTYREMFGList().Where(m => m.MFG_ACTIVEFLAG == "Y").ToList();
                VVM.ListTyreModel = new List<WEBX_FLEET_TYREMODELMST>();
                VVM.ListTyreSize = MS.GetTyreSizeList().Where(m => m.SIZE_ACTIVEFLAG == "Y").ToList();
                VVM.ListTyrePattern = MS.GetTyrePatternList().Where(m => m.ACTIVE_FLAG == "Y").ToList();
                VVM.ListVehicle = MS.GetVehicleObject().Where(m => m.ACTIVEFLAG == "Y").ToList();
                VVM.ListGnMST = MS.GetGeneralMasterObject().Where(m => m.StatusCode == "Y").ToList();

                if (TyreId != null && TyreId != "" && TyreId.Trim() != "")
                {
                    VVM.tyreMST = MS.GetTyreMSTList().Where(m => m.TYRE_ID == TyreId).FirstOrDefault();
                    if (VVM.tyreMST == null)
                    {
                        VVM.tyreMST = new WEBX_FLEET_TYREMST();
                    }
                    List<webx_VENDOR_HDR> ListVendor = MS.GetVendorObject();
                    VVM.EditFlag = "true";
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(VVM);
        }

        public JsonResult GetPostionFromPOSCategory(string Id)
        {
            Id = (Id == null ? "" : Convert.ToString(Id).ToUpper());
            List<WEBX_FLEET_TYREPOS> POSCategoryList = MS.GetTYREPOSList().Where(m => m.POS_ALLOWED.ToUpper() == Id).ToList();
            var ListCat = (from e in POSCategoryList
                           select new
                           {
                               Value = e.TYREPOS_ID,
                               Text = e.TYREPOS_DESC
                           });
            return Json(ListCat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTyreModelFromMFG(string Id)
        {
            Id = (Id == null ? "" : Id.ToUpper());
            List<WEBX_FLEET_TYREMODELMST> TyreModelList = MS.GetTyreModelList().Where(m => m.MFG_ID.ToUpper() == Id).ToList();
            var ListCat = (from e in TyreModelList
                           select new
                           {
                               Value = e.TYRE_MODEL_ID,
                               Text = e.MODEL_NO
                           });
            return Json(ListCat, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTyreSizeFromModel(string Id)
        {
            Id = (Id == null ? "" : Id.ToUpper());
            List<WEBX_FLEET_TYREMODELMST> TyreModelList = MS.GetTyreModelList().Where(m => m.ACTIVE_FLAG.ToUpper() == "Y" && m.TYRE_MODEL_ID == Convert.ToInt32(Id)).ToList();
            List<WEBX_FLEET_TYRESIZEMST> TyreSizeList = MS.GetTyreSizeList().Where(m => m.SIZE_ACTIVEFLAG.ToUpper() == "Y").ToList();
            var ListSize = (from model in TyreModelList
                            join size in TyreSizeList on model.TYRE_SIZEID equals size.TYRE_SIZEID
                            select new
                            {
                                Value = size.TYRE_SIZEID,
                                Text = size.TYRE_SIZENAME
                            });
            return Json(ListSize, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string TyreMasterSubmit(TyreViewModel VM)
        {
            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(VM.tyreMST.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, VM.tyreMST);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }
            bool Status = MS.InsertTyreMst(xmlDoc.InnerXml, VM.EditFlag, BaseUserName);
            return Status.ToString();// Status.ToString();
        }

        public JsonResult CheckDuplicateTYRECode(string code, string id)
        {
            try
            {
                int Count = MS.GetTyreMSTList().Where(c => c.TYRE_NO == null ? c.TYRE_NO == "" : c.TYRE_NO.ToUpper() == code && c.TYRE_ID.ToUpper() != id.ToUpper()).Count();

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        #endregion

        #endregion

        #region Company Employee Master

        public ActionResult CompanyEmployee()
        {
            WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel WECMVM = new WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel();
            try
            {
                WECMVM.listWECM = new List<Webx_Emp_Company>();
                WECMVM.WECMM = new WEBX_EMPLOYEE_COMPANY_MAPPING();
                WECMVM.EditFlag = "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WECMVM);
        }

        public JsonResult GetAllEmpCompanyJson(string searchTerm)
        {
            List<WebX_Master_Users> listEmpCompany = new List<WebX_Master_Users>();
            listEmpCompany = MS.GetUserDetails().Where(c => c.Name == null ? c.Name == "" : (c.Name.ToUpper().Contains(searchTerm.ToUpper()) || c.UserId.ToUpper().Contains(searchTerm.ToUpper()))).ToList().OrderBy(c => c.Name).ToList();
            var SearchList = (from e in listEmpCompany
                              select new
                              {
                                  id = e.UserId,
                                  text = e.Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetEmployeeBranchWise(string searchTerm, string BRCD)
        {
            List<WebX_Master_Users> listEmpCompany = new List<WebX_Master_Users>();
            listEmpCompany = MS.GetUserDetails().Where(c => c.Name == null ? c.Name == "" : (c.Name.ToUpper().Contains(searchTerm.ToUpper()) || c.UserId.ToUpper().Contains(searchTerm.ToUpper())) && (c.Status == "100") && (c.BranchCode == BRCD)).ToList().OrderBy(c => c.Name).ToList();
            var SearchList = (from e in listEmpCompany
                              select new
                              {
                                  id = e.UserId,
                                  text = e.Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetEmpCompanyMasterList(string id)
        {
            List<Webx_Emp_Company> listEmpCompany = new List<Webx_Emp_Company>();
            try
            {
                if (id != null)
                {
                    listEmpCompany = MS.GetEmpCompanyObject(id).ToList();
                }
                else
                {
                    listEmpCompany = new List<Webx_Emp_Company>();
                }
            }
            catch (Exception) { }
            return PartialView("_EmpCompanyList", listEmpCompany);
        }

        [HttpPost]
        public string AddEditCompanyEmployee(WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel WECMVM)
        {
            WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel EmpCompanyList = new WEBX_EMPLOYEE_COMPANY_MAPPINGViewModel();
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WECMVM.WECMM.EMP_CODE.GetType());
                string MstDetails = "<root><EmpMapping>";
                MstDetails = MstDetails + "<EMP_CODE>" + WECMVM.WECMM.EMP_CODE + "</EMP_CODE>";
                MstDetails = MstDetails + "<COMPANY_LIST>" + WECMVM.WECMM.COMPANY_LIST + "</COMPANY_LIST>";
                MstDetails = MstDetails + "<DEFAULT_COMPANY>" + WECMVM.WECMM.DEFAULT_COMPANY + "</DEFAULT_COMPANY>";
                MstDetails = MstDetails + "<ACTIVEFLAG>" + (WECMVM.WECMM.ACTIVEFLAG == "true" ? "Y" : "N") + "</ACTIVEFLAG>";
                MstDetails = MstDetails + "<INSUPFLAG>" + (WECMVM.EditFlag == "true" ? "2" : "1") + "</INSUPFLAG>";
                MstDetails = MstDetails + "<ENTRYBY>" + BaseUserName + "</ENTRYBY>";
                //MstHdrDetails = MstHdrDetails + "<INSUPFLAG>" + WECMVM.WECMM.INSUPFLAG + "</INSUPFLAG>";
                MstDetails = MstDetails + "</EmpMapping></root>";
                DataTable Dt = MS.AddEditEmployeeCompany(MstDetails);
                return "true";
            }
            catch (Exception)
            {
                return "false";
            }

        }

        #endregion

        #region Company Location Master

        public ActionResult CompanyLocation()
        {
            WEBX_LOCATION_COMPANY_MAPPINGViewModel WLCMVM = new WEBX_LOCATION_COMPANY_MAPPINGViewModel();
            try
            {
                WLCMVM.listWLCM = new List<Webx_Loc_Company>();
                WLCMVM.WLCMM = new WEBX_LOCATION_COMPANY_MAPPING();
                WLCMVM.EditFlag = "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WLCMVM);
        }

        public JsonResult GetAllLocCompanyJson(string searchTerm)
        {
            List<webx_location> listLocCompany = new List<webx_location>();
            listLocCompany = MS.GetLocationDetails().Where(c => c.LocName == null ? c.LocName == "" : (c.LocName.ToUpper().Contains(searchTerm.ToUpper()) || c.LocCode.ToUpper().Contains(searchTerm.ToUpper()))).ToList().OrderBy(c => c.LocName).ToList();
            var SearchList = (from e in listLocCompany
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetLocCompanyMasterList(string id)
        {
           // WEBX_LOCATION_COMPANY_MAPPINGViewModel WLCMVM = new WEBX_LOCATION_COMPANY_MAPPINGViewModel();
          //  WLCMVM.listWLCM = new List<Webx_Loc_Company>();
          //  WLCMVM.WLCMM = new WEBX_LOCATION_COMPANY_MAPPING();
            List<Webx_Loc_Company> listLocCompany = new List<Webx_Loc_Company>();
            //try
            //{
            //    if (id != null)
            //    {
            //        WLCMVM.listWLCM = MS.GetLocCompanyObject(id).ToList();
            //        DataTable Dt = MS.EmpLocCheckExists(id, "Location");
            //        if (Dt.Rows.Count > 0)
            //        {
            //            WLCMVM.WLCMM.ACTIVEFLAG = Dt.Rows[0]["ACTIVEFLAG"].ToString();
            //            if (Convert.ToDouble(Dt.Rows[0]["COUNT"].ToString()) > 0)
            //            {
            //                WLCMVM.EditFlag = "true";
            //            }
            //            else
            //            {
            //                WLCMVM.EditFlag = "false";
            //            }
            //        }
            //        else
            //        {
            //            WLCMVM.EditFlag = "false";
            //            WLCMVM.WLCMM.ACTIVEFLAG = "N";
            //        }

            //    }
            //    else
            //    {
            //        WLCMVM.listWLCM = new List<Webx_Loc_Company>();
            //        WLCMVM.EditFlag = "false";
            //        WLCMVM.WLCMM.ACTIVEFLAG = "N";
            //    }
            //}
            //catch (Exception)  {
            
            //}
              // return PartialView("_LocCompany", WLCMVM);


          
            try
            {
                if (id != null)
                {
                    listLocCompany = MS.GetLocCompanyObject(id).ToList();
                }
                else
                {
                    listLocCompany = new List<Webx_Loc_Company>();
                }
            }
            catch (Exception) { }
            return PartialView("_LocCompanyList", listLocCompany);
        }


        [HttpPost]
        public string AddEditCompanyLocation(WEBX_LOCATION_COMPANY_MAPPINGViewModel WLCMVM)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(WLCMVM.WLCMM.LOCCODE.GetType());
                string MstDetails = "<root><LocMapping>";
                MstDetails = MstDetails + "<LOCCODE>" + WLCMVM.WLCMM.LOCCODE + "</LOCCODE>";
                MstDetails = MstDetails + "<COMPANY_LIST>" + WLCMVM.WLCMM.COMPANY_LIST + "</COMPANY_LIST>";
                MstDetails = MstDetails + "<DEFAULT_COMPANY>" + WLCMVM.WLCMM.DEFAULT_COMPANY + "</DEFAULT_COMPANY>";
                MstDetails = MstDetails + "<ACTIVEFLAG>" + (WLCMVM.WLCMM.ACTIVEFLAG == "true" ? "Y" : "N") + "</ACTIVEFLAG>";
                MstDetails = MstDetails + "<INSUPFLAG>" + (WLCMVM.EditFlag == "true" ? "2" : "1") + "</INSUPFLAG>";
                MstDetails = MstDetails + "<ENTRYBY>" + BaseUserName + "</ENTRYBY>";
                //MstHdrDetails = MstHdrDetails + "<INSUPFLAG>" + WECMVM.WECMM.INSUPFLAG + "</INSUPFLAG>";
                MstDetails = MstDetails + "</LocMapping></root>";
                DataTable Dt = MS.AddEditLocationCompany(MstDetails);
                return "true";
            }
            catch (Exception)
            {
                return "false";
            }

        }

        #endregion

        #region Finance & Accounts

        #region Vendor Contract

        public ActionResult VendorCriteria()
        {
            Webx_Vendor_Contract_Summary_Ver1ViewModel WVCSV1VM = new Webx_Vendor_Contract_Summary_Ver1ViewModel();
            WVCSV1VM.WVCSV1 = new Webx_Vendor_Contract_Summary_Ver1();
            return View(WVCSV1VM);
        }

        public ActionResult VendorContract(string Type, string Text, string flag, string VendorCode, string contracttype, string Matrix)
        {

            VendorCode = VendorCode.ToUpper();

            Webx_Vendor_Contract_Summary_Ver1ViewModel WVCSV1VM = new Webx_Vendor_Contract_Summary_Ver1ViewModel();
            WVCSV1VM.listWVCRM = new List<Webx_Vendor_Contract_RouteBased>().ToList();
            WVCSV1VM.listWVCCM = new List<Webx_Vendor_Contract_CityBased>().ToList();
            WVCSV1VM.listWVCDM = new List<Webx_Vendor_Contract_DistanceBased>().ToList();
            WVCSV1VM.listWVCDoBCM = new List<Webx_Vendor_Contract_DocketBased>().ToList();
            WVCSV1VM.listWVCDoDCM = new List<Webx_Vendor_Contract_DocketBased>().ToList();
            WVCSV1VM.WVCSV1 = new Webx_Vendor_Contract_Summary_Ver1();
            if (flag == "Add")
            {
                // WVCSV1VM.WVCSV1 = MS.GetVendorNameInVendorContract("VENDOR").Where(c => c.contract_YN == flag && c.Vendor_Type == Type).FirstOrDefault();
                WVCSV1VM.WVCSV1.VendorCode = VendorCode;
                WVCSV1VM.WVCSV1.VendorName = MS.GetVendorObject().Where(c => c.VENDORCODE.ToUpper() == VendorCode).FirstOrDefault().VENDORNAME;
                WVCSV1VM.WVCSV1.Vendor_Type = Type;
                WVCSV1VM.WVCSV1.ContractDt = System.DateTime.Now;
                WVCSV1VM.WVCSV1.Valid_uptodt = System.DateTime.Now;
                WVCSV1VM.WVCSV1.Start_Dt = System.DateTime.Now;
                WVCSV1VM.WVCSV1.Security_deposit_date = System.DateTime.Now;
            }
            if (flag == "Edit")
            {
                WVCSV1VM.WVCSV1 = MS.GetVendorContractObject(VendorCode).Where(c => c.MetrixType == Matrix).FirstOrDefault();
                WVCSV1VM.listWVCRM = MS.GetVendorContractRouteObject(VendorCode).ToList();
                WVCSV1VM.listWVCCM = MS.GetVendorContractCityObject(VendorCode).ToList();
                WVCSV1VM.listWVCDM = MS.GetVendorContractDistanceObject(VendorCode).ToList();
                if (WVCSV1VM.WVCSV1 == null)
                {
                    WVCSV1VM.WVCSV1 = new Webx_Vendor_Contract_Summary_Ver1();
                    WVCSV1VM.WVCSV1.VendorCode = VendorCode;
                    WVCSV1VM.WVCSV1.VendorName = MS.GetVendorObject().Where(c => c.VENDORCODE.ToUpper() == VendorCode).FirstOrDefault().VENDORNAME;
                    WVCSV1VM.WVCSV1.Vendor_Type = Type;
                    WVCSV1VM.WVCSV1.ContractDt = System.DateTime.Now;
                    WVCSV1VM.WVCSV1.Valid_uptodt = System.DateTime.Now;
                    WVCSV1VM.WVCSV1.Start_Dt = System.DateTime.Now;
                    WVCSV1VM.WVCSV1.Security_deposit_date = System.DateTime.Now;
                    WVCSV1VM.listWVCDoBCM = new List<Webx_Vendor_Contract_DocketBased>().ToList();
                    WVCSV1VM.listWVCDoDCM = new List<Webx_Vendor_Contract_DocketBased>().ToList();
                }
                else
                {
                    WVCSV1VM.listWVCDoBCM = MS.GetVendorContractDocketChargesObject(WVCSV1VM.WVCSV1.CONTRACTCD, "B").ToList();
                    WVCSV1VM.listWVCDoDCM = MS.GetVendorContractDocketChargesObject(WVCSV1VM.WVCSV1.CONTRACTCD, "D").ToList();
                }
            }
            WVCSV1VM.WVCSV1.VendorTypeName = Text;
            WVCSV1VM.WVCSV1.ContractType = contracttype;
            WVCSV1VM.WVCSV1.Flag = flag;
            WVCSV1VM.WVCSV1.MetrixType = Matrix;
            return View("VendorContract", WVCSV1VM);
        }

        public JsonResult SearchVendorListJsonByFlag(string Flag, string VendorType, string searchTerm)
        {
            List<Webx_Vendor_Contract_Summary_Ver1> ListVendor = new List<Webx_Vendor_Contract_Summary_Ver1>();
            ListVendor = MS.GetVendorNameInVendorContract(Flag, VendorType).Where(c => c.VendorName.ToUpper().Contains(searchTerm.ToUpper()) || c.VendorCode.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.VendorName).ToList();
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  id = e.VendorCode,
                                  text = e.VendorName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ADDContractRouteInfo(int id)
        {
            Webx_Vendor_Contract_RouteBased WVCRBM = new Webx_Vendor_Contract_RouteBased();
            WVCRBM.ID = id;
            return PartialView("_RouteBasedContract", WVCRBM);
        }

        public ActionResult ADDContractCityInfo(int id)
        {
            Webx_Vendor_Contract_CityBased WVCCBM = new Webx_Vendor_Contract_CityBased();
            WVCCBM.ID = id;
            return PartialView("_CityBasedContract", WVCCBM);
        }

        public ActionResult ADDContractDistanceInfo(int id)
        {
            Webx_Vendor_Contract_DistanceBased WVCDBM = new Webx_Vendor_Contract_DistanceBased();
            WVCDBM.ID = id;
            return PartialView("_DistanceBasedContract", WVCDBM);
        }

        public ActionResult ADDContractDocketBCInfo(int id)
        {
            Webx_Vendor_Contract_DocketBased WVCDoBM = new Webx_Vendor_Contract_DocketBased();
            WVCDoBM.ID = id;
            return PartialView("_DocketBasedContractBC", WVCDoBM);
        }

        public ActionResult ADDContractDocketDCInfo(int id)
        {
            Webx_Vendor_Contract_DocketBased WVCDoBM = new Webx_Vendor_Contract_DocketBased();
            WVCDoBM.ID = id;
            return PartialView("_DocketBasedContractDC", WVCDoBM);
        }

        public JsonResult GetRouteByMode(string Id, string searchTerm)
        {
            List<webx_rutmas> ListRoute = new List<webx_rutmas>();
            List<webx_rutmas> ListRoute1 = MS.GetRutMstDetails().Where(c => c.RUTMOD == Id && c.ACTIVEFLAG == "Y").OrderBy(m => m.RUTNM).ToList();
            ListRoute = ListRoute1.Where(c => c.RUTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.RUTCD.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.RUTNM).ToList(); ;
            var SearchList = (from e in ListRoute
                              select new
                              {
                                  id = e.RUTCD,
                                  text = e.RUTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationByCity(string Id)
        {
            List<webx_location> listLocation = new List<webx_location>();

            listLocation = MS.GetLocationDetails().Where(c => c.LocCity == Id).ToList();

            var SearchList = (from e in listLocation
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocCode + ":" + e.LocName,
                              }).Distinct().ToList();
            // return Json(SearchList, JsonRequestBehavior.AllowGet);
            return Json(SearchList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationByState(string searchTerm, string Id)
        {
            DataTable DT = MS.GetLocationListByState(Id, searchTerm);
            List<webx_location> listLocation = new List<webx_location>();
            listLocation = DataRowToObject.CreateListFromTable<webx_location>(DT);
            var SearchList = (from e in listLocation
                              select new
                              {
                                  id = e.LocCode,
                                  text = e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList.OrderBy(c => c.text), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddEditVendorContract(Webx_Vendor_Contract_Summary_Ver1ViewModel WVCSV1VM, List<Webx_Vendor_Contract_RouteBased> RouteBasedContract, List<Webx_Vendor_Contract_CityBased> CityBasedContract, List<Webx_Vendor_Contract_DistanceBased> DistanceBasedContract, List<Webx_Vendor_Contract_DocketBased> DocketBasedContractBC, List<Webx_Vendor_Contract_DocketBased> DocketBasedContractDC, List<Webx_Vendor_Contract_DocketBased> DocketBasedContractBCfranchise, List<Webx_Vendor_Contract_DocketBased> DocketBasedContractDCfranchise)
        {
            try
            {
                string VendorContractDetails = "<root>";
                VendorContractDetails = VendorContractDetails + "<VendorContract>";
                VendorContractDetails = VendorContractDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                VendorContractDetails = VendorContractDetails + "<VendorName>" + WVCSV1VM.WVCSV1.VendorName + "</VendorName>";
                VendorContractDetails = VendorContractDetails + "<Vendor_Type>" + WVCSV1VM.WVCSV1.Vendor_Type + "</Vendor_Type>";
                VendorContractDetails = VendorContractDetails + "<CONTRACTCD>" + WVCSV1VM.WVCSV1.CONTRACTCD + "</CONTRACTCD>";
                VendorContractDetails = VendorContractDetails + "<ContractDt>" + WVCSV1VM.WVCSV1.ContractDt + "</ContractDt>";
                VendorContractDetails = VendorContractDetails + "<Contract_loccode>" + WVCSV1VM.WVCSV1.Contract_loccode + "</Contract_loccode>";
                VendorContractDetails = VendorContractDetails + "<Valid_uptodt>" + WVCSV1VM.WVCSV1.Valid_uptodt + "</Valid_uptodt>";
                VendorContractDetails = VendorContractDetails + "<Start_Dt>" + WVCSV1VM.WVCSV1.Start_Dt + "</Start_Dt>";
                VendorContractDetails = VendorContractDetails + "<VendorPerName>" + WVCSV1VM.WVCSV1.VendorPerName + "</VendorPerName>";
                VendorContractDetails = VendorContractDetails + "<VendorPerDesg>" + WVCSV1VM.WVCSV1.VendorPerDesg + "</VendorPerDesg>";
                VendorContractDetails = VendorContractDetails + "<VendorWitness>" + WVCSV1VM.WVCSV1.VendorWitness + "</VendorWitness>";
                VendorContractDetails = VendorContractDetails + "<CompEmpName>" + WVCSV1VM.WVCSV1.CompEmpName + "</CompEmpName>";
                VendorContractDetails = VendorContractDetails + "<CompEmpDesg>" + WVCSV1VM.WVCSV1.CompEmpDesg + "</CompEmpDesg>";
                VendorContractDetails = VendorContractDetails + "<CompWitness>" + WVCSV1VM.WVCSV1.CompWitness + "</CompWitness>";
                VendorContractDetails = VendorContractDetails + "<Vendor_Address>" + WVCSV1VM.WVCSV1.Vendor_Address + "</Vendor_Address>";
                VendorContractDetails = VendorContractDetails + "<VendorCity>" + WVCSV1VM.WVCSV1.VendorCity + "</VendorCity>";
                VendorContractDetails = VendorContractDetails + "<VendorPin>" + WVCSV1VM.WVCSV1.VendorPin + "</VendorPin>";
                VendorContractDetails = VendorContractDetails + "<VendorCategory>" + WVCSV1VM.WVCSV1.VendorCategory + "</VendorCategory>";
                VendorContractDetails = VendorContractDetails + "<VendorContractCat>" + WVCSV1VM.WVCSV1.VendorContractCat + "</VendorContractCat>";
                VendorContractDetails = VendorContractDetails + "<TDSAppl_YN>" + WVCSV1VM.WVCSV1.TDSAppl_YN + "</TDSAppl_YN>";
                VendorContractDetails = VendorContractDetails + "<TDS_Rate>" + WVCSV1VM.WVCSV1.TDS_Rate + "</TDS_Rate>";
                VendorContractDetails = VendorContractDetails + "<Security_deposit_chq>" + WVCSV1VM.WVCSV1.Security_deposit_chq + "</Security_deposit_chq>";
                VendorContractDetails = VendorContractDetails + "<Security_deposit_date>" + WVCSV1VM.WVCSV1.Security_deposit_date + "</Security_deposit_date>";
                VendorContractDetails = VendorContractDetails + "<Security_deposit_Amt>" + WVCSV1VM.WVCSV1.Security_deposit_Amt + "</Security_deposit_Amt>";
                VendorContractDetails = VendorContractDetails + "<Payment_interval>" + WVCSV1VM.WVCSV1.Payment_interval + "</Payment_interval>";
                VendorContractDetails = VendorContractDetails + "<Payment_Basis>" + WVCSV1VM.WVCSV1.Payment_Basis + "</Payment_Basis>";
                VendorContractDetails = VendorContractDetails + "<Payment_loc>" + WVCSV1VM.WVCSV1.Payment_loc + "</Payment_loc>";
                VendorContractDetails = VendorContractDetails + "<Monthly_Phone_Charges>" + WVCSV1VM.WVCSV1.Monthly_Phone_Charges + "</Monthly_Phone_Charges>";
                VendorContractDetails = VendorContractDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                VendorContractDetails = VendorContractDetails + "<MetrixType>" + WVCSV1VM.WVCSV1.MetrixType + "</MetrixType>";
                VendorContractDetails = VendorContractDetails + "<Status>" + "Y" + "</Status>";
                if (WVCSV1VM.WVCSV1.Vendor_Type == "08" || WVCSV1VM.WVCSV1.Vendor_Type == "XX5")
                {
                    VendorContractDetails = VendorContractDetails + "<Default_Charge>" + WVCSV1VM.WVCSV1.Default_Charge + "</Default_Charge>";
                }
                else
                {
                    VendorContractDetails = VendorContractDetails + "<Default_Charge>" + "0" + "</Default_Charge>";
                }

                if (WVCSV1VM.WVCSV1.Vendor_Type == "XX5")
                {
                    VendorContractDetails = VendorContractDetails + "<FTLFixAmount>" + WVCSV1VM.WVCSV1.FTLFixAmount + "</FTLFixAmount>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate_Type>" + WVCSV1VM.WVCSV1.Local_Feeder_Rate_Type + "</Local_Feeder_Rate_Type>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate>" + WVCSV1VM.WVCSV1.Local_Feeder_Rate + "</Local_Feeder_Rate>";
                    //VendorContractDetails = VendorContractDetails + "<CommissionUptoKM>" + WVCSV1VM.WVCSV1.CommissionUptoKM + "</CommissionUptoKM>";
                    //VendorContractDetails = VendorContractDetails + "<ExtraKMCharegeStart>" + WVCSV1VM.WVCSV1.ExtraKMCharegeStart + "</ExtraKMCharegeStart>";
                    //VendorContractDetails = VendorContractDetails + "<Rate_ExtraKM>" + WVCSV1VM.WVCSV1.Rate_ExtraKM + "</Rate_ExtraKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupApply>" + WVCSV1VM.WVCSV1.ODAPickupApply + "</ODAPickupApply>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupStartKM>" + WVCSV1VM.WVCSV1.ODAPickupStartKM + "</ODAPickupStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRateType>" + WVCSV1VM.WVCSV1.ODAPickupRateType + "</ODAPickupRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRate>" + WVCSV1VM.WVCSV1.ODAPickupRate + "</ODAPickupRate>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryApply>" + WVCSV1VM.WVCSV1.ODADeliveryApply + "</ODADeliveryApply>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryStartKM>" + WVCSV1VM.WVCSV1.ODADeliveryStartKM + "</ODADeliveryStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRateType>" + WVCSV1VM.WVCSV1.ODADeliveryRateType + "</ODADeliveryRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRate>" + WVCSV1VM.WVCSV1.ODADeliveryRate + "</ODADeliveryRate>";
                }
                else if (WVCSV1VM.WVCSV1.Vendor_Type == "08")
                {
                    VendorContractDetails = VendorContractDetails + "<FTLFixAmount>0</FTLFixAmount>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate_Type></Local_Feeder_Rate_Type>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate>0</Local_Feeder_Rate>";
                    //VendorContractDetails = VendorContractDetails + "<CommissionUptoKM>" + WVCSV1VM.WVCSV1.CommissionUptoKM + "</CommissionUptoKM>";
                    //VendorContractDetails = VendorContractDetails + "<ExtraKMCharegeStart>" + WVCSV1VM.WVCSV1.ExtraKMCharegeStart + "</ExtraKMCharegeStart>";
                    //VendorContractDetails = VendorContractDetails + "<Rate_ExtraKM>" + WVCSV1VM.WVCSV1.Rate_ExtraKM + "</Rate_ExtraKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupApply>" + WVCSV1VM.WVCSV1.ODAPickupApply + "</ODAPickupApply>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupStartKM>" + WVCSV1VM.WVCSV1.ODAPickupStartKM + "</ODAPickupStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRateType>" + WVCSV1VM.WVCSV1.ODAPickupRateType + "</ODAPickupRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRate>" + WVCSV1VM.WVCSV1.ODAPickupRate + "</ODAPickupRate>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryApply>" + WVCSV1VM.WVCSV1.ODADeliveryApply + "</ODADeliveryApply>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryStartKM>" + WVCSV1VM.WVCSV1.ODADeliveryStartKM + "</ODADeliveryStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRateType>" + WVCSV1VM.WVCSV1.ODADeliveryRateType + "</ODADeliveryRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRate>" + WVCSV1VM.WVCSV1.ODADeliveryRate + "</ODADeliveryRate>";
                    VendorContractDetails = VendorContractDetails + "<BookingService>" + WVCSV1VM.WVCSV1.BookingService + "</BookingService>";
                    VendorContractDetails = VendorContractDetails + "<DeliveryService>" + WVCSV1VM.WVCSV1.DeliveryService + "</DeliveryService>";
                    VendorContractDetails = VendorContractDetails + "<BookingRateType>" + WVCSV1VM.WVCSV1.BookingRateType + "</BookingRateType>";
                    VendorContractDetails = VendorContractDetails + "<DeliveryRateType>" + WVCSV1VM.WVCSV1.DeliveryRateType + "</DeliveryRateType>";
                    VendorContractDetails = VendorContractDetails + "<BookingRate>" + WVCSV1VM.WVCSV1.BookingRate + "</BookingRate>";
                    VendorContractDetails = VendorContractDetails + "<DeliveryRate>" + WVCSV1VM.WVCSV1.DeliveryRate + "</DeliveryRate>";
                }
                else
                {
                    VendorContractDetails = VendorContractDetails + "<FTLFixAmount>0</FTLFixAmount>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate_Type></Local_Feeder_Rate_Type>";
                    VendorContractDetails = VendorContractDetails + "<Local_Feeder_Rate>0</Local_Feeder_Rate>";
                    //VendorContractDetails = VendorContractDetails + "<CommissionUptoKM>0</CommissionUptoKM>";
                    //VendorContractDetails = VendorContractDetails + "<ExtraKMCharegeStart>0</ExtraKMCharegeStart>";
                    //VendorContractDetails = VendorContractDetails + "<Rate_ExtraKM>0</Rate_ExtraKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupApply>0</ODAPickupApply>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupStartKM>0</ODAPickupStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRateType></ODAPickupRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODAPickupRate>0</ODAPickupRate>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryApply>0</ODADeliveryApply>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryStartKM>0</ODADeliveryStartKM>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRateType></ODADeliveryRateType>";
                    VendorContractDetails = VendorContractDetails + "<ODADeliveryRate>0</ODADeliveryRate>";
                }
                VendorContractDetails = VendorContractDetails + "</VendorContract>";
                VendorContractDetails = VendorContractDetails + "</root>";
                VendorContractDetails = VendorContractDetails.ReplaceSpecialCharacters();

                DataTable Dt1 = MS.AddEditVendorContract(VendorContractDetails, WVCSV1VM.WVCSV1.Flag);
                string ContractCode = Dt1.Rows[0][0].ToString();
                DataTable Dt = new DataTable();
                if (WVCSV1VM.WVCSV1.Vendor_Type == "01")
                {
                    if (WVCSV1VM.WVCSV1.ContractType == "RB")
                    {
                        WVCSV1VM.listWVCRM = RouteBasedContract.ToList();
                        string RouteBaseDetails = "<root>";
                        foreach (var row in WVCSV1VM.listWVCRM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            RouteBaseDetails = RouteBaseDetails + "<RouteBase>";
                            RouteBaseDetails = RouteBaseDetails + "<ID>" + row.ID + "</ID>";
                            RouteBaseDetails = RouteBaseDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            RouteBaseDetails = RouteBaseDetails + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            RouteBaseDetails = RouteBaseDetails + "<TransMode>" + row.TransMode + "</TransMode>";
                            RouteBaseDetails = RouteBaseDetails + "<RouteCode>" + row.RouteCode + "</RouteCode>";
                            RouteBaseDetails = RouteBaseDetails + "<FTL_Type>" + row.FTL_Type + "</FTL_Type>";
                            RouteBaseDetails = RouteBaseDetails + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            RouteBaseDetails = RouteBaseDetails + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            RouteBaseDetails = RouteBaseDetails + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            RouteBaseDetails = RouteBaseDetails + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            RouteBaseDetails = RouteBaseDetails + "</RouteBase>";
                        }
                        RouteBaseDetails = RouteBaseDetails + "</root>";
                        Dt = MS.AddEditVendorContractBased(RouteBaseDetails, WVCSV1VM.WVCSV1.Flag, "RB", "", WVCSV1VM.WVCSV1.VendorCode);
                    }
                    if (WVCSV1VM.WVCSV1.ContractType == "CB")
                    {
                        WVCSV1VM.listWVCCM = CityBasedContract.ToList();
                        string CityBaseDetails = "<root>";
                        foreach (var row in WVCSV1VM.listWVCCM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            CityBaseDetails = CityBaseDetails + "<CityBase>";
                            CityBaseDetails = CityBaseDetails + "<ID>" + row.ID + "</ID>";
                            CityBaseDetails = CityBaseDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            CityBaseDetails = CityBaseDetails + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            CityBaseDetails = CityBaseDetails + "<FROM_CITY>" + row.FROM_CITY + "</FROM_CITY>";
                            CityBaseDetails = CityBaseDetails + "<TO_CITY>" + row.TO_CITY + "</TO_CITY>";
                            CityBaseDetails = CityBaseDetails + "<TransMode>" + row.TransMode + "</TransMode>";
                            CityBaseDetails = CityBaseDetails + "<FTL_Type>" + row.FTL_Type + "</FTL_Type>";
                            CityBaseDetails = CityBaseDetails + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            CityBaseDetails = CityBaseDetails + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            CityBaseDetails = CityBaseDetails + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            CityBaseDetails = CityBaseDetails + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            CityBaseDetails = CityBaseDetails + "</CityBase>";
                        }
                        CityBaseDetails = CityBaseDetails + "</root>";
                        Dt = MS.AddEditVendorContractBased(CityBaseDetails, WVCSV1VM.WVCSV1.Flag, "CB", "", WVCSV1VM.WVCSV1.VendorCode);
                    }
                    if (WVCSV1VM.WVCSV1.ContractType == "DB" || WVCSV1VM.WVCSV1.ContractType == "CB" || WVCSV1VM.WVCSV1.ContractType == "RB")
                    {
                        WVCSV1VM.listWVCDM = DistanceBasedContract.ToList();
                        string DistanceBaseDetails = "<root>";
                        foreach (var row in WVCSV1VM.listWVCDM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            DistanceBaseDetails = DistanceBaseDetails + "<DistanceBase>";
                            DistanceBaseDetails = DistanceBaseDetails + "<ID>" + row.ID + "</ID>";
                            DistanceBaseDetails = DistanceBaseDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            DistanceBaseDetails = DistanceBaseDetails + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            DistanceBaseDetails = DistanceBaseDetails + "<FTL_Type>" + row.FTL_Type + "</FTL_Type>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Vehicle_Type>" + row.Vehicle_Type + "</Vehicle_Type>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Vehicle_Number>" + row.Vehicle_Number + "</Vehicle_Number>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Min_Amt_Committed>" + row.Min_Amt_Committed + "</Min_Amt_Committed>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Committed_Km>" + row.Committed_Km + "</Committed_Km>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Chg_Per_Add_Km>" + row.Chg_Per_Add_Km + "</Chg_Per_Add_Km>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Max_Amt_Committed>" + row.Max_Amt_Committed + "</Max_Amt_Committed>";
                            DistanceBaseDetails = DistanceBaseDetails + "<Trips_PM>" + row.Trips_PM + "</Trips_PM>";
                            DistanceBaseDetails = DistanceBaseDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            DistanceBaseDetails = DistanceBaseDetails + "</DistanceBase>";
                        }
                        DistanceBaseDetails = DistanceBaseDetails + "</root>";
                        Dt = MS.AddEditVendorContractBased(DistanceBaseDetails, WVCSV1VM.WVCSV1.Flag, "DB", "", WVCSV1VM.WVCSV1.VendorCode);
                    }
                }

                if (WVCSV1VM.WVCSV1.Vendor_Type == "08")
                {
                    string DocketBaseDetails = "";
                    string DocketBaseDetailsDC = "";
                    if (DocketBasedContractBC != null)
                    {
                        WVCSV1VM.listWVCDoBCM = DocketBasedContractBC.ToList();
                        DocketBaseDetails = "<root>";
                        foreach (var row in WVCSV1VM.listWVCDoBCM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            DocketBaseDetails = DocketBaseDetails + "<DocketBase>";
                            DocketBaseDetails = DocketBaseDetails + "<ID>" + row.ID + "</ID>";
                            DocketBaseDetails = DocketBaseDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            DocketBaseDetails = DocketBaseDetails + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            DocketBaseDetails = DocketBaseDetails + "<PayBas>" + row.PayBas + "</PayBas>";
                            DocketBaseDetails = DocketBaseDetails + "<TransMode>" + row.TransMode + "</TransMode>";
                            DocketBaseDetails = DocketBaseDetails + "<ChargeType>" + "B" + "</ChargeType>";
                            DocketBaseDetails = DocketBaseDetails + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            DocketBaseDetails = DocketBaseDetails + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            DocketBaseDetails = DocketBaseDetails + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            DocketBaseDetails = DocketBaseDetails + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            DocketBaseDetails = DocketBaseDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            DocketBaseDetails = DocketBaseDetails + "<Location>" + row.Location + "</Location>";
                            DocketBaseDetails = DocketBaseDetails + "<City>" + row.City + "</City>";
                            DocketBaseDetails = DocketBaseDetails + "</DocketBase>";
                        }
                        DocketBaseDetails = DocketBaseDetails + "</root>";
                    }

                    if (DocketBasedContractDC != null)
                    {
                        WVCSV1VM.listWVCDoDCM = DocketBasedContractDC.ToList();
                        DocketBaseDetailsDC = "<root>";
                        foreach (var row in WVCSV1VM.listWVCDoDCM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<DocketBaseDC>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<ID>" + row.ID + "</ID>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<PayBas>" + row.PayBas + "</PayBas>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<TransMode>" + row.TransMode + "</TransMode>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<ChargeType>" + "D" + "</ChargeType>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<City>" + row.City + "</City>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Location>" + row.Location + "</Location>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "</DocketBaseDC>";
                        }
                        DocketBaseDetailsDC = DocketBaseDetailsDC + "</root>";
                    }

                    Dt = MS.AddEditVendorContractBased(DocketBaseDetails, WVCSV1VM.WVCSV1.Flag, WVCSV1VM.WVCSV1.Vendor_Type, DocketBaseDetailsDC, WVCSV1VM.WVCSV1.VendorCode);
                }

                if (WVCSV1VM.WVCSV1.Vendor_Type == "XX5")
                {
                    string DocketBaseDetails = "";
                    string DocketBaseDetailsDC = "";
                    if (DocketBasedContractBCfranchise != null)
                    {
                        WVCSV1VM.listWVCDoBCM = DocketBasedContractBCfranchise.ToList();
                        DocketBaseDetails = "<root>";
                        foreach (var row in WVCSV1VM.listWVCDoBCM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            DocketBaseDetails = DocketBaseDetails + "<DocketBase>";
                            DocketBaseDetails = DocketBaseDetails + "<ID>" + row.ID + "</ID>";
                            DocketBaseDetails = DocketBaseDetails + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            DocketBaseDetails = DocketBaseDetails + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            DocketBaseDetails = DocketBaseDetails + "<PayBas>" + row.PayBas + "</PayBas>";
                            DocketBaseDetails = DocketBaseDetails + "<TransMode>" + row.TransMode + "</TransMode>";
                            DocketBaseDetails = DocketBaseDetails + "<ChargeType>" + "B" + "</ChargeType>";
                            DocketBaseDetails = DocketBaseDetails + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            DocketBaseDetails = DocketBaseDetails + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            DocketBaseDetails = DocketBaseDetails + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            DocketBaseDetails = DocketBaseDetails + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            DocketBaseDetails = DocketBaseDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            DocketBaseDetails = DocketBaseDetails + "<Location>" + row.Location + "</Location>";
                            DocketBaseDetails = DocketBaseDetails + "<City>" + row.City + "</City>";
                            DocketBaseDetails = DocketBaseDetails + "</DocketBase>";
                        }
                        DocketBaseDetails = DocketBaseDetails + "</root>";
                    }

                    if (DocketBasedContractDCfranchise != null)
                    {
                        WVCSV1VM.listWVCDoDCM = DocketBasedContractDCfranchise.ToList();
                        DocketBaseDetailsDC = "<root>";
                        foreach (var row in WVCSV1VM.listWVCDoDCM)
                        {
                            //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<DocketBaseDC>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<ID>" + row.ID + "</ID>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<VendorCode>" + WVCSV1VM.WVCSV1.VendorCode + "</VendorCode>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<CONTRACTCD>" + ContractCode + "</CONTRACTCD>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<PayBas>" + row.PayBas + "</PayBas>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<TransMode>" + row.TransMode + "</TransMode>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<ChargeType>" + "D" + "</ChargeType>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Min_Charge>" + row.Min_Charge + "</Min_Charge>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Chg_Rate>" + row.Chg_Rate + "</Chg_Rate>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Rate_Type>" + row.Rate_Type + "</Rate_Type>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Max_Charge>" + row.Max_Charge + "</Max_Charge>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<EntryBy>" + BaseUserName + "</EntryBy>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<City>" + row.City + "</City>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "<Location>" + row.Location + "</Location>";
                            DocketBaseDetailsDC = DocketBaseDetailsDC + "</DocketBaseDC>";
                        }
                        DocketBaseDetailsDC = DocketBaseDetailsDC + "</root>";
                    }

                    Dt = MS.AddEditVendorContractBased(DocketBaseDetails, WVCSV1VM.WVCSV1.Flag, WVCSV1VM.WVCSV1.Vendor_Type, DocketBaseDetailsDC, WVCSV1VM.WVCSV1.VendorCode);
                }

                string TranXaction = "";
                TranXaction = Dt.Rows[0]["TranXaction"].ToString();
                return RedirectToAction("VendorContractDone", new { ContractCode = ContractCode, TranXaction = TranXaction });
            }
            catch (Exception)
            {
                return View("VendorCriteria");
            }
        }

        public ActionResult VendorContractDone(string ContractCode, string TranXaction)
        {

            ViewBag.ContractCode = ContractCode;
            ViewBag.TranXaction = TranXaction;

            return View();

        }

        #endregion

        #endregion

        #region DCR Add

        public ActionResult DCRAdd()
        {
            webx_DCR_HeaderViewModel WDHVM = new webx_DCR_HeaderViewModel();
            WDHVM.listWDHM = new List<webx_DCR_Header>().ToList();
            WDHVM.LocLevel = BaseLocationLevel;
            return View(WDHVM);
        }

        public ActionResult ADDDCRInfo(int id)
        {
            webx_DCR_Header WDHM = new webx_DCR_Header();
            WDHM.DOC_KEY = id;
            return PartialView("_PartialDCRAdd", WDHM);
        }

        public JsonResult CheckBookCode(string DocType, string BookCode)
        {
            string strResult = MS.CheckBookCode(DocType, BookCode);
            string[] ResultArr = strResult.Split('|');
            return Json(new { CNT = ResultArr[0], Message = ResultArr[1] }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckValidDCRSeries(string DocType, string DRSStartFrom, string TotalLeaf)
        {
            string strResult = MS.CheckValidDCRSeries(DocType, DRSStartFrom, TotalLeaf);
            string[] ResultArr = strResult.Split('|');
            return Json(new { CNT = ResultArr[0], Message = ResultArr[1] }, JsonRequestBehavior.AllowGet);
        }

        protected string GenerateArgument(List<webx_DCR_Header> DCRAdd)
        {
            string cboDocType, txtBookCode, txtSeriesFrom, txtAlloteTo, txtSplitTo, arg = "";
            int cboLeafs, txtTotalLeafs;

            foreach (var dcr in DCRAdd)
            {

                cboDocType = dcr.doc_type; //(DropDownList)dgGeneral.Rows[index].FindControl("cboDocType");
                txtBookCode = dcr.bookcode; //(TextBox)dgGeneral.Rows[index].FindControl("txtBookCode");
                txtSeriesFrom = dcr.DOC_SR_FROM;//(TextBox)dgGeneral.Rows[index].FindControl("txtSeriesFrom");
                cboLeafs = dcr.TOT_LEAF;//(DropDownList)dgGeneral.Rows[index].FindControl("cboTotalLeafs");
                txtAlloteTo = dcr.Locatoin == null ? "" : dcr.Locatoin;//(TextBox)dgGeneral.Rows[index].FindControl("txtAlloteTo");
                txtSplitTo = dcr.Page;//(TextBox)dgGeneral.Rows[index].FindControl("txtSplitTo");
                txtTotalLeafs = dcr.TotalLeaf;
                //  DropDownList cboBusType = (DropDownList)dgGeneral.Rows[index].FindControl("cboBusType");

                int TotalLeafs = (cboDocType.Trim() == "DKTBC") ? txtTotalLeafs : cboLeafs;

                int iTLeaf = Convert.ToInt32(TotalLeafs);
                int iPageSize = Convert.ToInt32(txtSplitTo.Trim());

                string FirstNo = "", LastNo = "", CurrentFrom = "", CurrentTo = "", BookSuffix = ".";
                FirstNo = txtSeriesFrom.Trim();
                LastNo = GF.NextKeyCode(FirstNo, iTLeaf - 1);

                CurrentFrom = FirstNo;

                for (int iRem = iTLeaf; iRem > 0; iRem = iRem - iPageSize)
                {
                    if (iRem < iPageSize)
                    {
                        iPageSize = iRem;
                    }
                    CurrentTo = GF.NextKeyCode(CurrentFrom, iPageSize - 1);
                    BookSuffix = ((FirstNo == CurrentFrom) ? "." : GF.getNextSuffix(BookSuffix));
                    // Generate String                
                    if (arg.Trim() == "")
                    {
                        arg = txtBookCode.Trim() + "^" + cboDocType.Trim() + "^" + CurrentFrom + "^" + iPageSize.ToString() + "^" + txtAlloteTo.Trim();
                        if (cboDocType == "DKT")
                            //arg = arg + "^" + "BUT" + "^" + BookSuffix + "^";
                            arg = arg + "^" + "^" + BookSuffix + "^";
                        else
                            arg = arg + "^-1^" + BookSuffix + "^";
                    }
                    else
                    {
                        arg = arg + "," + txtBookCode.Trim() + "^" + cboDocType.Trim() + "^" + CurrentFrom + "^" + iPageSize.ToString() + "^" + txtAlloteTo.Trim();
                        if (cboDocType == "DKT")
                            // arg = arg + "^" + "BUT" + "^" + BookSuffix + "^";
                            arg = arg + "^" + "^" + BookSuffix + "^";
                        else
                            arg = arg + "^-1^" + BookSuffix + "^";
                    }

                    // Reset From 
                    CurrentFrom = GF.NextKeyCode(CurrentTo);
                }
            }
            return arg;
        }

        [HttpPost]
        public ActionResult AddUpdateDCR(List<webx_DCR_Header> DCRAdd)
        {
            DataTable dt = MS.AddUpdateDCR(GenerateArgument(DCRAdd), BaseUserName);
            List<webx_DCR_Header> HeaderList = DataRowToObject.CreateListFromTable<webx_DCR_Header>(dt);
            return PartialView("_DCRList", HeaderList);
        }

        public JsonResult GetLocationLevel(string Id)
        {
            Id = Id.ToUpper();
            List<webx_location> ListLocations = MS.GetLocationDetails();

            ListLocations = ListLocations.Where(c => c.LocCode.ToUpper() == Id).ToList();

            return Json(new { LocationLevel = ListLocations.FirstOrDefault().Loc_Level, Name = "", Mobileno = "", City = "", Pincode = "", Email = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationZoneLevel(string Id)
        {
            Id = Id.ToUpper();
            List<webx_location> ListLocations = MS.GetLocationAsPerZoneDetails();

            ListLocations = ListLocations.Where(c => c.LocCode.ToUpper() == Id).ToList();

            return Json(new { LocationLevel = ListLocations.FirstOrDefault().Loc_Level, Name = "", Mobileno = "", City = "", Pincode = "", Email = "" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocationFromRegion(string Id)
        {
            Id = Id.ToUpper();
            List<webx_location> ListLocations = MS.GetLocationAsPerZoneDetails();

            ListLocations = ListLocations.Where(c => c.Report_Loc.ToUpper() == Id).OrderBy(m => m.LocName).ToList();
            List<SelectListItem> LocationList = new List<SelectListItem>();
            LocationList.Add(new SelectListItem() { Text = "All", Value = "All" });

            foreach (var itm1 in ListLocations)
            {
                LocationList.Add(new SelectListItem() { Text = itm1.LocCode + " : " + itm1.LocName, Value = itm1.LocCode });
            }

            //var SearchList = (from e in ListLocations
            //                  select new
            //                  {
            //                      Value = e.LocCode,
            //                      Text = e.LocName,
            //                  }).Distinct().ToList();
            return Json(LocationList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region DCR Allote

        public ActionResult DCRAllote()
        {
            vw_Series_ToBe_Alloted2ViewModel VWSTBA2VM = new vw_Series_ToBe_Alloted2ViewModel();
            VWSTBA2VM.VWSTBA2M = new vw_Series_ToBe_Alloted2();
            return View(VWSTBA2VM);
        }

        public string DCRAlloteList(vw_Series_ToBe_Alloted2ViewModel VWSTBA2VM)
        {
            return JsonConvert.SerializeObject(MS.DCRAlloteList(GF.FormateDate(VWSTBA2VM.VWSTBA2M.FromDate), GF.FormateDate(VWSTBA2VM.VWSTBA2M.ToDate), BaseLocationCode, VWSTBA2VM.VWSTBA2M.DOC_TYPE));
        }

        public JsonResult DCRAlloteToListJson(string searchTerm)
        {
            List<AlloteLocation> ListLocations = new List<AlloteLocation>();

            ListLocations = MS.GetAlloteLocationDetails(BaseLocationCode).Where(c => c.AllotToName.ToUpper().Contains(searchTerm.ToUpper()) || c.AllotToName.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.AllotToName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.AllotToCode,
                                  text = e.AllotToName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DCRLocationAllote(int DOC_KEY, string DOC_SR_FROM, string DOC_SR_TO)
        {
            vw_Series_ToBe_Alloted2 VWSTBA2M = new vw_Series_ToBe_Alloted2();
            VWSTBA2M.DOC_KEY = DOC_KEY;
            VWSTBA2M.DOC_SR_FROM = DOC_SR_FROM;
            VWSTBA2M.DOC_SR_TO = DOC_SR_TO;
            return PartialView("_DCRLocation", VWSTBA2M);
        }

        public ActionResult DCRAlloteDone(int DOC_KEY, string Alloted_To)
        {
            vw_Series_ToBe_Alloted2ViewModel VWSTBA2VM = new vw_Series_ToBe_Alloted2ViewModel();
            DataTable dt = MS.DCRAllote(DOC_KEY, BaseLocationCode, Alloted_To, BaseUserName);
            ViewBag.series = dt.Rows[0]["Series"].ToString();
            ViewBag.AlloteTo = dt.Rows[0]["AllotedTo"].ToString();
            ViewBag.Message = dt.Rows[0]["IsSucessful"].ToString();
            return View();
        }

        #endregion

        #region Manage DCR
        
        public ActionResult ReDeAllocateSeries()
        {
            vw_Series_ToBe_Alloted2ViewModel THCCSVM = new vw_Series_ToBe_Alloted2ViewModel();
            THCCSVM.listVWSTBA2M = new List<vw_Series_ToBe_Alloted2>();
            return View(THCCSVM);
        }

        public ActionResult ReallocateDeList(vw_Series_ToBe_Alloted2ViewModel VWseries)
        {
            vw_Series_ToBe_Alloted2ViewModel THCCSVM = new vw_Series_ToBe_Alloted2ViewModel();
            List<vw_Series_ToBe_Alloted2> listWDHM = new List<vw_Series_ToBe_Alloted2>();
            THCCSVM.listVWSTBA2M = new List<vw_Series_ToBe_Alloted2>();
            try
            {

                listWDHM = MS.AlloteDeAllote(Convert.ToDateTime(VWseries.VWSTBA2M.FromDate).ToString("dd MMM yyyy"), Convert.ToDateTime(VWseries.VWSTBA2M.ToDate).ToString("dd MMM yyyy"), VWseries.VWSTBA2M.DOC_SR_FROM, VWseries.VWSTBA2M.DOC_SR_TO, VWseries.VWSTBA2M.ConNo, VWseries.VWSTBA2M.BookCode, VWseries.VWSTBA2M.BUSINESS_TYPE_ID, VWseries.VWSTBA2M.DOC_TYPE).ToList();
                THCCSVM.listVWSTBA2M = listWDHM;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(THCCSVM);
        }

       
        [HttpGet]
        public ActionResult UpdateRealloCation(int Doc_Key)
        {
            vw_Series_ToBe_Alloted2ViewModel THCCSVM = new vw_Series_ToBe_Alloted2ViewModel();
            try
            {
                THCCSVM.listVWSTBA2M = MS.GetDCR(Doc_Key);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(THCCSVM);
        }

        [HttpPost]
        public ActionResult UpdateRealloCationSubmit(List<vw_Series_ToBe_Alloted2> UpdateRealloCation)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    vw_Series_ToBe_Alloted2ViewModel THCCSVM = new vw_Series_ToBe_Alloted2ViewModel();
                    vw_Series_ToBe_Alloted2 WDHM = UpdateRealloCation.FirstOrDefault();
                    THCCSVM.listVWSTBA2M = MS.UpdateAlloteDeallote(WDHM.DOC_KEY, WDHM.Alloted_To,BaseUserName);
                }
                
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("UpdateRealloteDealloteDone");
        }
        
        public ActionResult DeleteRealloCation(int Doc_Key)
        {
            int dcr = Doc_Key;

            if (Doc_Key > 0)
            {
                MS.DeleteAlloteDeallote(Doc_Key);
            }
            return RedirectToAction("RealloteDealloteDone", new { dcr = dcr });
        }

        public ActionResult RealloteDealloteDone(int dcr)
        {
            ViewBag.DCR = dcr;
            return View();
        }


        public ActionResult UpdateRealloteDealloteDone()
        {
            return View();
        }


        public ActionResult ManageDCR()
        {
            vw_DCR_RegisterViewModel VWDRVM = new vw_DCR_RegisterViewModel();
            VWDRVM.VWDRM = new vw_DCR_Register();
            VWDRVM.VWDMH = new vw_DCR_Management_History();
            VWDRVM.listVWDMH = new List<vw_DCR_Management_History>();
            return View(VWDRVM);
        }

        public ActionResult GetManageDCR(string Type, string Number)
        {
            vw_DCR_Register VWDRM = new vw_DCR_Register();
            try
            {
                VWDRM = MS.GetDCRNameObject(Type, Number).FirstOrDefault();
                VWDRM.DOC_Number = Number;
            }
            catch (Exception) { }
            return PartialView("_ManageDCRPartial", VWDRM);
        }

        public JsonResult GetpersonListJsonByLocation(string Loccode, string Category, string searchTerm)
        {
            List<vw_DCR_Register> ListNewPersonAssigned = new List<vw_DCR_Register>();
            ListNewPersonAssigned = MS.GetNewPersonAssignedObject(Loccode, Category).ToList();
            if (!string.IsNullOrEmpty(searchTerm))
            {
                ListNewPersonAssigned = ListNewPersonAssigned.Where(c => c.AlloteToName.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.AlloteToName).ToList();
            }
            var SearchList = (from e in ListNewPersonAssigned
                              select new
                              {
                                  id = e.AlloteTo,
                                  text = e.AlloteToName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetSplitDCR(string DocKey, string docno)
        {
            vw_DCR_Register VWDRM = new vw_DCR_Register();
            try
            {
                VWDRM = MS.GetDCRSplitObject(DocKey).FirstOrDefault();
                VWDRM.DOC_Number = docno;
                VWDRM.DifLEAF = Convert.ToDecimal("");
            }
            catch (Exception) { }
            return PartialView("_DCRSplitPartial", VWDRM);
        }

        public ActionResult GetDCRHistory(string DocKey)
        {
            List<vw_DCR_Management_History> listVWDMHM = new List<vw_DCR_Management_History>();
            try
            {
                listVWDMHM = MS.GetDCRHistoryObject(DocKey).ToList();
            }
            catch (Exception) { }
            return PartialView("_ManageDCRHistory", listVWDMHM);
        }

        public string NextCode(string KeyCode)
        {
            byte[] ASCIIValues = ASCIIEncoding.ASCII.GetBytes(KeyCode);
            int StringLength = ASCIIValues.Length;
            bool isAllZed = true;
            bool isAllNine = true;
            //Check if all has ZZZ.... then do nothing just return empty string.

            for (int i = 0; i < StringLength - 1; i++)
            {
                if (ASCIIValues[i] != 90)
                {
                    isAllZed = false;
                    break;
                }
            }
            if (isAllZed && ASCIIValues[StringLength - 1] == 57)
            {
                ASCIIValues[StringLength - 1] = 64;
            }

            // Check if all has 999... then mak/e it A0
            for (int i = 0; i < StringLength; i++)
            {
                if (ASCIIValues[i] != 57)
                {
                    isAllNine = false;
                    break;
                }
            }
            if (isAllNine)
            {
                ASCIIValues[StringLength - 1] = 47;
                ASCIIValues[0] = 65;
                for (int i = 1; i < StringLength - 1; i++)
                {
                    ASCIIValues[i] = 48;
                }
            }


            for (int i = StringLength; i > 0; i--)
            {
                if (i - StringLength == 0)
                {
                    ASCIIValues[i - 1] += 1;
                }
                if (ASCIIValues[i - 1] == 58)
                {
                    ASCIIValues[i - 1] = 48;
                    if (i - 2 == -1)
                    {
                        break;
                    }
                    ASCIIValues[i - 2] += 1;
                }
                else if (ASCIIValues[i - 1] == 91)
                {
                    ASCIIValues[i - 1] = 65;
                    if (i - 2 == -1)
                    {
                        break;
                    }
                    ASCIIValues[i - 2] += 1;

                }
                else
                {
                    break;
                }

            }
            KeyCode = ASCIIEncoding.ASCII.GetString(ASCIIValues, 0, ASCIIValues.Length);
            return KeyCode;
        }

        [HttpPost]
        public ActionResult SubmitSplitDetail(vw_DCR_Register VWDRM)
        {
            try
            {
                DataTable dt = MS.DCRSplitDetail(VWDRM.DOC_KEY, VWDRM.BookCode, VWDRM.NewSrTo, (Convert.ToInt32(VWDRM.TOT_LEAF) - Convert.ToInt32(VWDRM.DifLEAF)), VWDRM.Suffix, VWDRM.DOC_NEW_SR_FROM, Convert.ToInt32(VWDRM.DifLEAF), VWDRM.AllotedBy, VWDRM.Alloted_To, VWDRM.Alloted_Type_ID, VWDRM.Business_Type_ID, BaseUserName);
                return RedirectToAction("ManageDCRDone", new { BookCode = VWDRM.BookCode, FromTo = VWDRM.DOC_NEW_SR_FROM + "-" + VWDRM.DOC_SR_TO, Name = "Split Series", Status = "Done" });
            }
            catch (Exception)
            {
                return RedirectToAction("ManageDCRDone", new { BookCode = VWDRM.BookCode, FromTo = VWDRM.DOC_NEW_SR_FROM + "-" + VWDRM.DOC_SR_TO, Name = "Split Series", Status = "Not Done" });
            }
        }

        public ActionResult SubmitReallocationDetail(string DOC_KEY, string Alloted_By, string Alloted_Type_ID, string Alloted_To, string Series, string BookCD)
        {
            try
            {
                DataTable dt = MS.DCRReallocationDetail(DOC_KEY, Alloted_By, Alloted_Type_ID, Alloted_To, BaseUserName);
                return RedirectToAction("ManageDCRDone", new { BookCode = BookCD, FromTo = Series, Name = "Re-Allocated", Status = "Done" });
            }
            catch (Exception)
            {
                return RedirectToAction("ManageDCRDone", new { BookCode = BookCD, FromTo = Series, Name = "Re-Allocated", Status = "Not Done" });
            }
        }

        public ActionResult ManageDCRDone(string BookCode, string FromTo, string Name, string Status)
        {
            ViewBag.BookCode = BookCode;
            ViewBag.FromTo = FromTo;
            ViewBag.Name = Name;
            ViewBag.Status = Status;
            return View();
        }

        #endregion

        #region  View Allotted Series

        public ActionResult AllottedSeries()
        {
            AllottedSeries AS = new AllottedSeries();
            AS.BaseLoccode = BaseLocationCode;
            AS.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            AS.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (AS.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                AS.RegionList.Insert(0, objNew);
                AS.LocationList = new List<webx_location>();
                AS.LocationList.Insert(0, objNew);
            }
            if (AS.LocLevel == 2)
            {
                AS.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                AS.LocationList.Insert(0, objNew);
            }
            if (AS.LocLevel == 3)
            {
                AS.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(AS);
        }

        public ActionResult AllottedSeriesList(AllottedSeries AS)
        {
            return View(AS);
        }

        #endregion

        #region Rate+

        [AllowAnonymous]
        public ActionResult Rate()
        {
            webx_RateMatrix WRMM = new webx_RateMatrix();
            return View(WRMM);
        }

        #region Rate Matrix

        [AllowAnonymous]
        public ActionResult RateMatrix()
        {

            try
            {

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View();
        }

        [AllowAnonymous]
        public JsonResult GetRate(string FromCity, string ToCity, string Kg)
        {
            List<webx_RateMatrix> RateMatrixList = MS.GetRateMatrixObject(FromCity, ToCity, Kg).ToList();
            var RateMatrixListdata = (from e in RateMatrixList
                                      select new
                                      {
                                          e.Type,
                                          e.Rate,
                                          Weight = Convert.ToInt16(e.Rate) * Convert.ToInt16(Kg),
                                          e.TransitDays,
                                      }).ToArray();

            return Json(RateMatrixListdata, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public string RateList(webx_RateMatrix WRMM)
        {
            //return JsonConvert.SerializeObject(MS.DCRAlloteList(GF.FormateDate(VWSTBA2VM.VWSTBA2M.FromDate), GF.FormateDate(VWSTBA2VM.VWSTBA2M.ToDate), BaseLocationCode, VWSTBA2VM.VWSTBA2M.BUSINESS_TYPE = null, VWSTBA2VM.VWSTBA2M.DOC_TYPE));
            return "";
        }

        #endregion

        #region Job Order

        #region Work Group Master

        public ActionResult WorkGroup()
        {
            WEBX_FLEET_WORKGROUPMSTViewModel WFWGMVM = new WEBX_FLEET_WORKGROUPMSTViewModel();
            try
            {
                WFWGMVM.listWFWGM = new List<WEBX_FLEET_WORKGROUPMST>();
                WFWGMVM.WFWGM = new WEBX_FLEET_WORKGROUPMST();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WFWGMVM);
        }

        public ActionResult GetWorkGroupDetails(int GRPCD)
        {
            WEBX_FLEET_WORKGROUPMST WFWGMM = new WEBX_FLEET_WORKGROUPMST();
            try
            {
                if (GRPCD > 0)
                {
                    WFWGMM = MS.GetWorkGroupObject().FirstOrDefault(c => c.W_GRPCD == GRPCD);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditWorkGroup", WFWGMM);
        }

        public JsonResult WorkGroupListJson()
        {
            List<WEBX_FLEET_WORKGROUPMST> listWorkGroup = MS.GetWorkGroupObject().ToList();
            //List<Webx_Master_General> listAsset = MS.GetGeneralMasterObject().Where(c => c.CodeType == "VEHASSTYP" && c.CodeId =="01").ToList();
            var ListWorkGroupdata = (from e in listWorkGroup
                                     select new
                                     {
                                         //SPDBRCD = locList.Where(c => c.LocCode.ToUpper().Trim() == e.SPDBRCD.ToUpper().Trim()).FirstOrDefault().LocName,
                                         e.W_GRPCD,
                                         e.W_GRPDESC,
                                         Asset_Type = e.Asset_Type + ":" + e.Asset_Type_desc,//= listAsset.Where(c=>c.CodeId==e.Asset_Type.Trim()),
                                         e.ACTIVE_FLAG,
                                     }).ToArray();

            return Json(ListWorkGroupdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditWorkGroup(WEBX_FLEET_WORKGROUPMST WFWGM)
        {
            bool Status = false;
            try
            {
                DataTable Dt = MS.AddEditWorkGroup(WFWGM.W_GRPCD, WFWGM.W_GRPDESC, Convert.ToBoolean(WFWGM.ACTIVE_FLAG), BaseUserName, WFWGM.Asset_Type.Trim());
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        public JsonResult CheckDuplicateWorkGroup(string GRPDESC)
        {
            try
            {
                string Count = MS.CheckDuplicateWorkGroup(GRPDESC);
                return Json(new { CNT = Count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        #endregion

        #region Task Type

        public ActionResult TaskType()
        {
            Webx_Fleet_TaskTypeMstViewModel WFTTMVM = new Webx_Fleet_TaskTypeMstViewModel();
            try
            {
                WFTTMVM.listWFTTMM = new List<Webx_Fleet_TaskTypeMst>();
                WFTTMVM.WFTTMM = new Webx_Fleet_TaskTypeMst();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WFTTMVM);
        }

        public ActionResult GetTaskTypeDetails(int TaskTypeId)
        {
            Webx_Fleet_TaskTypeMst WFTTMM = new Webx_Fleet_TaskTypeMst();
            try
            {
                if (TaskTypeId > 0)
                {
                    WFTTMM = MS.GetTaskTypeObject().FirstOrDefault(c => c.TaskTypeID == TaskTypeId);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditTaskType", WFTTMM);
        }

        public JsonResult TaskTypeListJson()
        {
            List<Webx_Fleet_TaskTypeMst> listTaskType = MS.GetTaskTypeObject().ToList();
            //List<Webx_Master_General> listAsset = MS.GetGeneralMasterObject().Where(c => c.CodeType == "VEHASSTYP" && c.CodeId =="01").ToList();
            var ListTaskTypedata = (from e in listTaskType
                                    select new
                                    {
                                        //SPDBRCD = locList.Where(c => c.LocCode.ToUpper().Trim() == e.SPDBRCD.ToUpper().Trim()).FirstOrDefault().LocName,
                                        e.TaskType,
                                        e.AccCode,
                                        e.AccDesc,//= listAsset.Where(c=>c.CodeId==e.Asset_Type.Trim()),
                                        e.ActiveFlag,
                                        e.TaskTypeID,
                                    }).ToArray();

            return Json(ListTaskTypedata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditTaskType(Webx_Fleet_TaskTypeMst WFTTM)
        {
            bool Status = false;
            try
            {
                DataTable Dt = MS.AddEditTaskType(WFTTM.TaskTypeID, WFTTM.TaskType, WFTTM.AccCode, Convert.ToBoolean(WFTTM.ActiveFlag), BaseUserName);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Update Vehicle Km Master

        // Insert/Update GeneralMaster
        public ActionResult VehicleKM()
        {
            UpdateVehicleKmViewModel WVVM = new UpdateVehicleKmViewModel();
            try
            {
                WVVM.listVehicleKm = MS.GetVehicleKmObject().ToList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WVVM);
        }

        // Insert/Update GeneralMaster
        [HttpPost]
        public ActionResult AddEditVehicleKM(int VEH_INTERNAL_NO, decimal Tmp_Current_KM_Read)
        {
            bool Status = false;
            try
            {
                DataTable Dt = MS.AddEditVehicleKmMaster(Tmp_Current_KM_Read, VEH_INTERNAL_NO);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Mail Setting

        public ActionResult MailSettings()
        {
            MailSettingViewModel MSVM = new MailSettingViewModel();
            try
            {
                MSVM.WFJMSM = MS.GetMailSettingObject().FirstOrDefault();
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(MSVM);
        }

        [HttpPost]
        public ActionResult MailSettings(Webx_Fleet_JobApprovalMailSettings WFJMSM)
        {
            bool Status = false;
            try
            {
                WFJMSM.Update_By = BaseUserName;
                DataTable Dt = MS.AddEditMailSettingMaster(WFJMSM.SMTP_Server, WFJMSM.From_Address, WFJMSM.From_To, WFJMSM.Update_By);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region Jobsheet Approved Amount Master

        // Insert/Update GeneralMaster
        public ActionResult JobsheetApprovedAmount()
        {
            JSApproval_AmtViewModel JSAAVM = new JSApproval_AmtViewModel();
            try
            {
                JSAAVM.listWFJSAHAM = MS.GetJobsheetApprovedAmountObject().ToList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(JSAAVM);
        }

        // Insert/Update GeneralMaster
        [HttpPost]
        public ActionResult AddEditJobsheetApprovedAmount(int Id, string LOC_HRCHY_CODE, decimal LOC_APPROVE_AMT, string JS_TYPE, decimal LOC_APPROVE_AMT_BANK)
        {
            bool Status = false;
            try
            {
                DataTable Dt = MS.AddEditJobsheetApprovedAmountMaster(Id, LOC_HRCHY_CODE, LOC_APPROVE_AMT, JS_TYPE, BaseUserName, LOC_APPROVE_AMT_BANK);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception ex)
            {
                return Json(Status);
            }
        }

        #endregion

        #region General Task
        public ActionResult GeneralTask()
        {
            WEBX_FLEET_GENERALTASKMSTViewModel WFGTVM = new WEBX_FLEET_GENERALTASKMSTViewModel();
            WFGTVM.listWFGTM = new List<WEBX_FLEET_GENERALTASKMST>().ToList();
            return View(WFGTVM);
        }

        public ActionResult GetGroupList(string Id, string Text)
        {
            WEBX_FLEET_GENERALTASKMSTViewModel WFGTVM = new WEBX_FLEET_GENERALTASKMSTViewModel();
            WFGTVM.listWFGTM = MS.GetGeneralTaskListObject(Id).ToList();
            ViewBag.Text = Text.ToString();
            return PartialView("_GeneralTaskList", WFGTVM.listWFGTM);
        }

        public ActionResult ADDTaskInfo(int id, int Code)
        {
            WEBX_FLEET_GENERALTASKMST WFGT = new WEBX_FLEET_GENERALTASKMST();
            WFGT.W_GRPCD = Code;
            return PartialView("_PartialGeneralTask", WFGT);
        }

        public ActionResult AddEditGeneralTask(string Id, string Text)
        {
            WEBX_FLEET_GENERALTASKMSTViewModel WFGTVM = new WEBX_FLEET_GENERALTASKMSTViewModel();
            WFGTVM.listWFGTM = MS.GetGeneralTaskListObject(Id).ToList();
            ViewBag.ID = Id;
            ViewBag.Text = Text;
            //return PartialView("_AddEditGeneralTask",WFGTVM);
            return View(WFGTVM);
        }

        public ActionResult AddGeneralTask(List<WEBX_FLEET_GENERALTASKMST> GeneralTask1)
        {
            WEBX_FLEET_GENERALTASKMSTViewModel WFGTVM = new WEBX_FLEET_GENERALTASKMSTViewModel();
            WFGTVM.listWFGTM = GeneralTask1;
            foreach (var row in WFGTVM.listWFGTM)
            {
                DataTable Dt = MS.AddEditGeneralTask(row.W_GRPCD, row.TASKCD, row.TASKDESC, row.LABOUR_HRS, row.ACTIVE_FLAG, BaseUserName, row.TASKTYP);
            }
            return RedirectToAction("GeneralTask");
        }
        #endregion

        #region Spare Part

        public ActionResult SparePart()
        {
            SparePartViewModel SVM = new SparePartViewModel();
            SVM.listWFSHM = new List<Webx_Fleet_spareParthdr>();
            return View(SVM);
        }

        public ActionResult GetSparePartByDate(DateTime Fromdate, DateTime Todate)
        {
            List<Webx_Fleet_spareParthdr> listSparePart = new List<Webx_Fleet_spareParthdr>();
            try
            {
                listSparePart = MS.GetSparePartObject(GF.FormateDate(Fromdate), GF.FormateDate(Todate), "");
            }
            catch (Exception) { }
            return PartialView("_SparePartList", listSparePart);
        }

        public ActionResult AddEditSparePart(string Id)
        {
            SparePartViewModel SVM = new SparePartViewModel();
            List<WEBX_FLEET_SPAREPARTDET> ListWFSD = new List<WEBX_FLEET_SPAREPARTDET>();
            SVM.listWFSDM = ListWFSD;
            SVM.WFSHM = new Webx_Fleet_spareParthdr();
            SVM.WFSHM.ExpDt = System.DateTime.Now;
            SVM.WFSDM = new WEBX_FLEET_SPAREPARTDET();
            if (Id != null && Id != "0")
            {
                SVM.WFSHM = MS.GetSparePartObject("", "", Id).FirstOrDefault();
                SVM.WFSDM = MS.GetSparePartDETObject(Id).FirstOrDefault();
                SVM.listWFSHM = MS.GetSparePartObject("", "", Id).ToList();
                SVM.listWFSDM = MS.GetSparePartDETObject(Id).ToList();
            }

            return View("AddEditSparePart", SVM);
        }

        public ActionResult ADDSparePartInfo(int id)
        {
            WEBX_FLEET_SPAREPARTDET WMFSM = new WEBX_FLEET_SPAREPARTDET();
            WMFSM.Srno = id;
            return PartialView("_PartialSparePart", WMFSM);
        }

        //[HttpPost]
        //public ActionResult AddEditSparePart(SparePartViewModel SVM, List<WEBX_FLEET_SPAREPARTDET> SparePart)
        //{
        //    SVM.listWFSDM = SparePart;
        //    try
        //    {
        //        if (SVM.WFSHM.Part_Id == null)
        //        {
        //            DataTable Dt = MS.GetNewPartId();
        //            SVM.WFSHM.Part_Id = Dt.Rows[0]["PartID"].ToString();
        //            SVM.WFSDM.Part_ID = Dt.Rows[0]["PartID"].ToString();
        //            DataTable DT = MS.AddEditSparePart(SVM.WFSHM, "I");
        //            foreach (var row in SVM.listWFSDM)
        //            {
        //                DataTable DT1 = MS.AddEditSparePartDet(SVM.WFSHM.Part_Id, row.Vendorcode, row.Vendor_Part_No, row.Manufacturer, row.Cost, row.Leadtime, "I");
        //            }
        //        }
        //        else
        //        {
        //            DataTable DT = MS.AddEditSparePart(SVM.WFSHM, "U");
        //            foreach (var row in SVM.listWFSDM)
        //            {
        //                DataTable DT1 = MS.AddEditSparePartDet(SVM.WFSHM.Part_Id, row.Vendorcode, row.Vendor_Part_No, row.Manufacturer, row.Cost, row.Leadtime, "U");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
        //        return View("Error");
        //    }
        //    return RedirectToAction("SparePart");

        //}




          [HttpPost]
        public ActionResult AddEditSparePart(SparePartViewModel SVM, List<WEBX_FLEET_SPAREPARTDET> SparePart)
        {
            SVM.listWFSDM = SparePart;
            try
            {
                if (SVM.WFSHM.Part_Id == null)
                {
                    DataTable Dt = MS.GetNewPartId();
                    SVM.WFSHM.Part_Id = Dt.Rows[0]["PartID"].ToString();
                    SVM.WFSDM.Part_ID = Dt.Rows[0]["PartID"].ToString();
                    DataTable DT = MS.AddEditSparePart(SVM.WFSHM, "I");
                    string DETDetails = "<root>";
                    foreach (var row in SVM.listWFSDM)
                    {
                       
                        DETDetails = DETDetails + "<DET><Part_Id>" + SVM.WFSHM.Part_Id + "</Part_Id>";
                        DETDetails = DETDetails + "<Vendorcode>" + row.Vendorcode + "</Vendorcode>";
                        DETDetails = DETDetails + "<Vendor_Part_No>" + row.Vendor_Part_No + "</Vendor_Part_No>";
                        DETDetails = DETDetails + "<Manufacturer>" + row.Manufacturer + "</Manufacturer>";
                        DETDetails = DETDetails + "<Cost>" + row.Cost + "</Cost>";
                        DETDetails = DETDetails + "<Leadtime>" + row.Leadtime + "</Leadtime>";
                        DETDetails = DETDetails + "</DET>";
                        //DataTable DT1 = MS.AddEditSparePartDet(SVM.WFSHM.Part_Id, row.Vendorcode, row.Vendor_Part_No, row.Manufacturer, row.Cost, row.Leadtime, "I");
                    }
                    DETDetails = DETDetails + "</root>";
                    DataTable DT1 = MS.AddEditSparePartDet(DETDetails.Replace("–", "-"),"I");
                    //DataTable DT1 = MS.AddEditSparePartDet(SVM.WFSHM.Part_Id, row.Vendorcode, row.Vendor_Part_No, row.Manufacturer, row.Cost, row.Leadtime, "I");
                }
               else
                {
                DataTable DT = MS.AddEditSparePart(SVM.WFSHM, "U");
                    string DETDetails = "<root>";
                    foreach (var row in SVM.listWFSDM)
                    {

                        DETDetails = DETDetails + "<DET><Part_Id>" + SVM.WFSHM.Part_Id + "</Part_Id>";
                        DETDetails = DETDetails + "<Vendorcode>" + row.Vendorcode + "</Vendorcode>";
                        DETDetails = DETDetails + "<Vendor_Part_No>" + row.Vendor_Part_No + "</Vendor_Part_No>";
                        DETDetails = DETDetails + "<Manufacturer>" + row.Manufacturer + "</Manufacturer>";
                        DETDetails = DETDetails + "<Cost>" + row.Cost + "</Cost>";
                        DETDetails = DETDetails + "<Leadtime>" + row.Leadtime + "</Leadtime>";
                        DETDetails = DETDetails + "</DET>";
                         
                        //DataTable DT1 = MS.AddEditSparePartDet(SVM.WFSHM.Part_Id, row.Vendorcode, row.Vendor_Part_No, row.Manufacturer, row.Cost, row.Leadtime, "I");
                    }
                    DETDetails = DETDetails + "</root>";
                    DataTable DT1 = MS.AddEditSparePartDet(DETDetails.Replace("–", "-"), "U");
                }
        }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("SparePart");
        }
        #endregion


        #region Jobsheet Approved Amount Matrix Master

        public ActionResult JobsheetApprovedAmountMatrix()
        {
            JSApproval_Amt_MatrixViewModel JSAAVM = new JSApproval_Amt_MatrixViewModel();
            try
            {
                JSAAVM.listWFJSAHAM = MS.GetJobsheetApprovedAmountMatrixObject().ToList();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(JSAAVM);
        }

        public ActionResult GetJSApprovedDetail(int Id)
        {
            VW_FLEET_JS_APPROVAL_AMT_MATRIX WCM = new VW_FLEET_JS_APPROVAL_AMT_MATRIX();
            try
            {
                if (Id > 0)
                {
                    WCM = MS.GetJobsheetApprovedAmountMatrixObject().FirstOrDefault(c => c.Approved_Id == Id);
                    if (WCM.Approver_UserId == "Enter Multiple Users Separated By Comma")
                    {
                        WCM.Approver_UserId = "";
                    }
                    if (WCM.CC_UserId == "Enter Multiple Users Separated By Comma")
                    {
                        WCM.CC_UserId = "";
                    }
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditJSApprovedMatrix", WCM);
        }

        // Insert/Update GeneralMaster
        public ActionResult AddEditJobsheetApprovedAmountMatrix(VW_FLEET_JS_APPROVAL_AMT_MATRIX WCM)
        {
            bool Status = false;
            try
            {
                DataTable Dt = MS.AddEditJobsheetApprovedAmountMatrixMaster(WCM.Approved_Id, WCM.Approver_UserId, WCM.CC_UserId, BaseUserName);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                return Json(Status);
            }
        }

        #endregion

        #region SM Task

        public ActionResult SMTask()
        {
            SMTaskViewModel SVM = new SMTaskViewModel();
            SVM.listSMTASK = MS.GetSmTaskList().ToList();
            return View(SVM);
        }

        public JsonResult GetVehicleCodeJson(int Id, string searchTerm)
        {
            List<SMTask> ListItem = new List<SMTask>();
            if (Id != 0)
            {
                ListItem = MS.GetVehicleType().ToList();
            }
            else
            {
                ListItem = MS.GetVehiclaDetforSmTask(Id).ToList();
            }
            var SearchList = (from e in ListItem
                              select new
                              {
                                  id = e.TYPE_CODE,
                                  text = e.TYPE_NAME,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditSMTask(int Id)
        {
            SMTaskViewModel SVM = new SMTaskViewModel();
            List<Webx_Fleet_SM_Task_Det> ListWFSD = new List<Webx_Fleet_SM_Task_Det>();
            SVM.listWFSDM = ListWFSD;
            SVM.WFSHM = new Webx_Fleet_SM_Task_Hdr();
            SVM.WFSDM = new Webx_Fleet_SM_Task_Det();
            SVM.SFM = new ViewModels.SMTask();

            if (Id > 0)
            {
                SVM.SFM = MS.GetVehiclaDetforSmTask(Id).FirstOrDefault();
                SVM.WFSDM = MS.GetSmTaskDet(Id).FirstOrDefault();
                SVM.WFSHM = MS.GetSmTask(Id).FirstOrDefault();
                //SVM.SFM = MS.GetVehiclaDetforSmTask(Id).ToList();
                SVM.listWFSDM = MS.GetSmTaskDet(Id).ToList();
            }
            SVM.SFM.id = Id;
            return View("AddEditSMTask", SVM);
        }

        public ActionResult ADDSMTaskInfo(int id)
        {
            Webx_Fleet_SM_Task_Det WMFSM = new Webx_Fleet_SM_Task_Det();
            WMFSM.Srno = id;
            return PartialView("_PartialSMTask", WMFSM);
        }

        [HttpPost]
        public ActionResult AddEditSMTask(SMTaskViewModel SVM, List<Webx_Fleet_SM_Task_Det> SMTask)
        {
            SVM.listWFSDM = SMTask;
            try
            {

                SVM.WFSHM.Entry_By = BaseUserName;
                //SVM.WFSDM.ENTRY_BY = GF.FormateDate(System.DateTime.Now);
                if (SVM.WFSHM.SMTask_Id == 0)
                {
                    DataTable Dt = MS.AddEditSMTask(SVM.WFSHM, "Add");
                    //DataTable DT = MS.AddEditSparePart(SVM.WFSHM, "I");
                    foreach (var row in SVM.listWFSDM)
                    {
                        DataTable DT1 = MS.AddEditSMTaskDet(row.W_GRPCD, row.TASKDESC, row.TASKTYP, row.T_UOM, row.SERVICE_INTERVALS_DAY, row.SERVICE_INTERVALS_KMS, row.ADV_NOTIFICATION_DAY, row.ADV_NOTIFACATION_KMS, row.ACTIVE_FLAG, BaseUserName, row.SMTask_Id, row.Task_Id, row.Estimated_Hrs);
                    }
                }
                else
                {
                    DataTable Dt = MS.AddEditSMTask(SVM.WFSHM, "Edit");
                    foreach (var row in SVM.listWFSDM)
                    {
                        DataTable DT1 = MS.AddEditSMTaskDet(row.W_GRPCD, row.TASKDESC, row.TASKTYP, row.T_UOM, row.SERVICE_INTERVALS_DAY, row.SERVICE_INTERVALS_KMS, row.ADV_NOTIFICATION_DAY, row.ADV_NOTIFACATION_KMS, row.ACTIVE_FLAG, BaseUserName, row.SMTask_Id, row.Task_Id, row.Estimated_Hrs);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("SMTask");
        }

        #endregion

        #region Set Opening Balance

        public ActionResult SetOpeningBalance()
        {
            SetOpeningBalanceViewModel cyopblac = new SetOpeningBalanceViewModel();
            return View(cyopblac);
        }

        public ActionResult OpeningBalanceCriteria(SetOpeningBalanceViewModel cyopblac)
        {
            try
            {
                List<webx_acctinfo> List = MS.OpeningBalanceCriteria(cyopblac.Balance.Brcd, cyopblac.Balance.Acccategory, cyopblac.Balance.Acccode, BaseFinYear);
                cyopblac.Balancelist = List;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(cyopblac);
        }

        [HttpPost]
        public ActionResult OpeningBalanceSubmit(webx_acctinfo Aco, List<webx_acctinfo> Master)
        {
            string Message = "";
            string Status = "";
            DataTable DT = new DataTable();
            try
            {
                string xmlDoc1 = "<root>";

                foreach (var item in Master)
                {
                    item.FinYear = BaseFinYear.ToString();
                    string[] strArray = item.FinYear.Split('-');

                    xmlDoc1 = xmlDoc1 + "<AccDetails>";
                    xmlDoc1 = xmlDoc1 + "<FinYear>" + strArray[0] + "</FinYear>";
                    xmlDoc1 = xmlDoc1 + "<Acccode>" + item.Acccode + "</Acccode>";
                    xmlDoc1 = xmlDoc1 + "<OpenCredit>" + item.OpenCredit + "</OpenCredit>";
                    xmlDoc1 = xmlDoc1 + "<OpenDebit>" + item.OpenDebit + "</OpenDebit>";
                    xmlDoc1 = xmlDoc1 + "<Brcd>" + item.Brcd + "</Brcd>";
                    xmlDoc1 = xmlDoc1 + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";

                    xmlDoc1 = xmlDoc1 + "</AccDetails>";
                }
                xmlDoc1 = xmlDoc1 + "</root>";
                DT = MS.InsertSetOpeningBalnace(xmlDoc1);

                Message = DT.Rows[0][0].ToString();
                Status = DT.Rows[0][1].ToString();
                if (Message != "Done" || Status == "0")
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("OpeningBalanceDone");
        }

        public ActionResult OpeningBalanceDone()
        {
            return View();
        }

        public JsonResult GetSetOpeningBalance(string SerchGroup)
        {
            List<webx_acctinfo> ListGroups = new List<webx_acctinfo> { };
            try
            {
                DataTable Dt_City = MS.GetsetopnigBalancecategory(SerchGroup);
                List<webx_acctinfo> GetLocationList = DataRowToObject.CreateListFromTable<webx_acctinfo>(Dt_City);

                return Json(GetLocationList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);

            }
            catch (Exception)
            {
                return Json(ListGroups);
            }
        }

        #endregion

        #region Cheque Print Voucher

        public ActionResult ChqPrint()
        {
            ChqDepositVoucherViewModel chq = new ChqDepositVoucherViewModel();
            chq.ChqdetList = new List<WEBX_chq_det>();
            return View(chq);
        }

        public ActionResult ChqPrintDetail(ChqDepositVoucherViewModel chq)
        {
            //chq.ChqdetList = MS.ChqPrintDetailLIST(chq.chqdet.Chqno, GF.FormateDate(chq.chqdet.DATEFROM), GF.FormateDate(chq.chqdet.DATETO));
            chq.ChqdetList = MS.ChqPrintDetailLIST(chq.chqdet.chqno, GF.FormateDate(chq.chqdet.DATEFROM), GF.FormateDate(chq.chqdet.DATETO), chq.transdate, chq.Voucherno);
            return PartialView("_ChqPrintDetailList", chq.ChqdetList);

        }

        #endregion


        #endregion

        #region Customer Group - Business Type Mapping

        public ActionResult CustomerGroupBusinessTypeMapping()
        {
            WebX_master_grpbus_mappingViewModel WMGMVM = new WebX_master_grpbus_mappingViewModel();
            WMGMVM.WMGBM = new WebX_master_grpbus_mapping();
            WMGMVM.ListWMGBM = new List<WebX_master_grpbus_mapping>();
            WMGMVM.ListWMGBM = MS.CustomerGroupBusinessTypeMappingList().ToList();
            return View(WMGMVM);
        }

        public ActionResult ADDCustGroupBusTypeInfo(int id)
        {
            WebX_master_grpbus_mapping WMGBM = new WebX_master_grpbus_mapping();
            WMGBM.srno = id;
            WMGBM.activeflag = "Y";
            return PartialView("_PartialCGBUSTYPE", WMGBM);
        }
        [HttpPost]
        public ActionResult CustomerGroupBusinessTypeMapping(WebX_master_grpbus_mappingViewModel WMGBMVM, List<WebX_master_grpbus_mapping> CGBUSTYPE)
        {
            try
            {
                string strxml;
                string activeflag;
                strxml = "<root>";
                foreach (var row in CGBUSTYPE)
                {
                    if (row.activeflag == "Y")
                    {
                        activeflag = "Y";
                    }
                    else
                    {
                        activeflag = "N";
                    }
                    strxml = strxml + "<record>";
                    strxml = strxml + "<grpcd>" + row.grpcd + "</grpcd>";
                    strxml = strxml + "<businesstype>" + row.businesstype + "</businesstype>";
                    strxml = strxml + "<activeflag>" + activeflag + "</activeflag>";
                    strxml = strxml + "<entryby>" + BaseUserName + "</entryby>";
                    strxml = strxml + "</record>";
                }
                strxml = strxml + "</root>";
                DataTable Dt = MS.AddCustomerGroupBusinessTypeMapping(strxml);
                return RedirectToAction("CustomerGroupBusinessTypeMapping");
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        #endregion

        #region Vendor Service Mapping

        public ActionResult VendorServiceMapping()
        {
            VendorServiceMappingViewModel VSMVM = new VendorServiceMappingViewModel();
            VSMVM.WVSMM = new Webx_Vendor_Service_Mapping_master();
            VSMVM.ListVendor = new List<webx_VENDOR_HDR>();
            VSMVM.Listservice = new List<Webx_Master_General>();
            VSMVM.ListWVSMM = new List<Webx_Vendor_Service_Mapping_master>();
            return View(VSMVM);
        }


        public JsonResult GetServiceByUser(string Id)
        {
            List<vw_ServiceList_Mapping> ListService = MS.GetServiceListForUserModule().Where(c => c.UserCode == Id).OrderBy(c => c.Name).ToList();

            var ListServicedata = (from e in ListService
                                   select new
                                   {
                                       Value = e.Code,
                                       Text = e.Name,
                                   }).ToArray();
            return Json(ListServicedata, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModuleByService(string Id)
        {
            List<vw_ModuleList_Mapping> ListModule = MS.GetModuleList().Where(c => c.Service_Code == Id).OrderBy(c => c.Name).ToList();

            var ListModuledata = (from e in ListModule
                                  select new
                                  {
                                      Value = e.Code,
                                      Text = e.Name,
                                  }).ToArray();
            return Json(ListModuledata, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetVendorserviceList(string Id, string Vendorcode, string ServiceCode)
        {
            VendorServiceMappingViewModel VSMVM = new VendorServiceMappingViewModel();
            if (Id == "1")
            {
                VSMVM.Listservice = MS.GetGeneralMasterObject().Where(c => c.CodeType == "VENDSERV" && c.StatusCode == "Y").OrderBy(c => c.CodeDesc).ToList();
                VSMVM.ListWVSMM = MS.GetVendorServiceList(Vendorcode, ServiceCode).ToList();
                foreach (var item1 in VSMVM.Listservice)
                {
                    foreach (var item2 in VSMVM.ListWVSMM)
                    {
                        if (item1.CodeId == item2.Vendor_Service_Code)
                        {
                            item1.activeservice = true;
                        }
                    }
                }
                ViewBag.id = 1;
                return PartialView("_ListVendorServiceByVendor", VSMVM);
            }
            else
            {
                VSMVM.ListVendor = MS.GetVendorObject().Where(c => c.Active == "Y").ToList();
                VSMVM.ListWVSMM = MS.GetVendorServiceList(Vendorcode, ServiceCode).ToList();
                foreach (var item1 in VSMVM.ListVendor)
                {
                    foreach (var item2 in VSMVM.ListWVSMM)
                    {
                        if (item1.VENDORCODE == item2.Vendorcode)
                        {
                            item1.activeservice = true;
                        }
                    }
                }
                ViewBag.id = 2;
                return PartialView("_ListVendorServiceByService", VSMVM);
            }
        }
        [HttpPost]
        public ActionResult VendorServiceMapping(VendorServiceMappingViewModel VSMVM, List<Webx_Master_General> ListService, List<webx_VENDOR_HDR> ListVendor)
        {
            string Code = "";
            string VenCode = "";
            if (ListService != null)
            {
                VSMVM.Listservice = ListService;
                VSMVM.ListWVSMM = MS.GetVendorServiceList(VSMVM.WVSMM.Vendorcode, VSMVM.WVSMM.Vendor_Service_Code).ToList();
                DataTable Dt = MS.DeleteVendoerServiceMapping(VSMVM.WVSMM.mapmode, VSMVM.WVSMM.Vendor_Service_Code, VSMVM.WVSMM.Vendorcode);
                foreach (var item in VSMVM.Listservice)
                {
                    if (item.activeservice == true)
                    {
                        Code = item.CodeId;
                        foreach (var item2 in VSMVM.ListWVSMM)
                        {
                            VenCode = item2.Vendorcode;
                            DataTable Dt1 = MS.AddVendoerServiceMapping(VSMVM.WVSMM.mapmode, Code, VenCode);
                        }
                    }
                }
            }
            else
            {
                VSMVM.ListVendor = ListVendor;
                VSMVM.ListWVSMM = MS.GetVendorServiceList(VSMVM.WVSMM.Vendorcode, VSMVM.WVSMM.Vendor_Service_Code).ToList();
                DataTable Dt = MS.DeleteVendoerServiceMapping(VSMVM.WVSMM.mapmode, VSMVM.WVSMM.Vendor_Service_Code, VSMVM.WVSMM.Vendorcode);
                foreach (var item in VSMVM.ListVendor)
                {
                    if (item.activeservice == true)
                    {
                        VenCode = item.VENDORCODE;
                        foreach (var item2 in VSMVM.ListWVSMM)
                        {
                            Code = item2.Vendor_Service_Code;
                            DataTable Dt1 = MS.AddVendoerServiceMapping(VSMVM.WVSMM.mapmode, Code, VenCode);
                        }
                    }
                }
            }

            return RedirectToAction("VendorServiceMapping");
        }
        #endregion

        #region Mapping Master

        public ActionResult Mapping()
        {
            MappingViewModel MVM = new MappingViewModel();
            MVM.ListMappingService = new List<MappingService>();
            return View(MVM);
        }

        public ActionResult MappingService(string MapType, string Mode, string Code, string Name, string scode)
        {
            MappingViewModel MVM = new MappingViewModel();
            if (MapType == "1")
            {
                MVM.MS = new MappingService();
                MVM.ListMappingService = MS.GetMappingServiceList(MapType, Mode).ToList();
                if (Mode == "1")
                {
                    ViewBag.Name = "Service";
                    MVM.ListCustServiceMap = MS.GetCustServiceList().Where(c => c.CUSTCD == Code).ToList();
                    foreach (var row1 in MVM.ListCustServiceMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.Service_Code)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    ViewBag.Name = "Customer";
                    MVM.ListCustServiceMap = MS.GetCustServiceList().Where(c => c.Service_Code == Code).ToList();
                    foreach (var row1 in MVM.ListCustServiceMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.CUSTCD)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
            }
            else if (MapType == "2")
            {
                MVM.MS = new MappingService();
                MVM.ListMappingService = MS.GetMappingServiceList(MapType, Mode).ToList();
                if (Mode == "1")
                {
                    MVM.ListCustLocationMap = MS.GetCustLocationList().Where(c => c.CUSTCD == Code).ToList();
                    ViewBag.Name = "Location";
                    foreach (var row1 in MVM.ListCustLocationMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.LocCode)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    ViewBag.Name = "Customer";
                    MVM.ListCustLocationMap = MS.GetCustLocationList().Where(c => c.LocCode == Code).ToList();
                    foreach (var row1 in MVM.ListCustLocationMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.CUSTCD)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
            }
            else if (MapType == "3")
            {
                MVM.MS = new MappingService();
                MVM.ListMappingService = MS.GetMappingServiceList(MapType, Mode).ToList();
                if (Mode == "1")
                {
                    MVM.ListServiceLocationMap = MS.GetServiceLocationList().Where(c => c.Service_Code == Code).ToList();
                    ViewBag.Name = "Location";
                    foreach (var row1 in MVM.ListServiceLocationMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.LocCode)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    MVM.ListServiceLocationMap = MS.GetServiceLocationList().Where(c => c.LocCode == Code).ToList();
                    ViewBag.Name = "Service";
                    foreach (var row1 in MVM.ListServiceLocationMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.Service_Code)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
            }
            else if (MapType == "4")
            {
                MVM.MS = new MappingService();
                MVM.ListMappingService = MS.GetMappingServiceList(MapType, Mode).ToList();
                if (Mode == "1")
                {
                    MVM.ListUserServiceMap = MS.GetUserServiceList().Where(c => c.UserCode == Code).ToList();
                    ViewBag.Name = "Service";
                    foreach (var row1 in MVM.ListUserServiceMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.Service_Code)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
                else
                {
                    MVM.ListUserServiceMap = MS.GetUserServiceList().Where(c => c.Service_Code == Code).ToList();
                    ViewBag.Name = "User";
                    foreach (var row1 in MVM.ListUserServiceMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.UserCode)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                MVM.MS = new MappingService();
                MVM.ListMappingService = MS.GetMappingServiceList(MapType, Mode).Where(c => c.Scode == scode).ToList();
                if (Mode == "1")
                {
                    MVM.ListUserModuleMap = MS.GetUserModuleList().Where(c => c.UserCode == Code && c.Service_Code == scode).ToList();
                    ViewBag.Name = "Module";
                    foreach (var row1 in MVM.ListUserModuleMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.Module_Code)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                    ViewBag.ServiceName = Name;
                }
                else
                {
                    MVM.ListUserModuleMap = MS.GetUserModuleList().Where(c => c.Module_Code == Code).ToList();
                    ViewBag.Name = "User";
                    foreach (var row1 in MVM.ListUserModuleMap)
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            if (row.Code == row1.UserCode)
                            {
                                if (row1.Active == "Y")
                                {
                                    row.Status = true;
                                }
                            }
                        }
                    }
                }
            }
            MVM.MS.Mode = Mode;
            MVM.MS.OppCode = Code;
            MVM.MS.StringName = Name;
            MVM.MS.ModeType = MapType;
            MVM.MS.Scode = scode;
            return View("MappingService", MVM);
        }

        public ActionResult MappingServiceDone(MappingViewModel MVM, List<MappingService> MappingService)
        {
            try
            {
                if (MVM.MS.ModeType == "1")
                {
                    MVM.ListMappingService = MappingService;
                    if (MVM.MS.Mode == "1")
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code, row.Name, BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Service Successfully Assign To Customer";
                    }
                    else
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode, MVM.MS.StringName, BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Customer Successfully Assign To Service";
                    }
                }
                else if (MVM.MS.ModeType == "2")
                {
                    MVM.ListMappingService = MappingService;
                    if (MVM.MS.Mode == "1")
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Location Successfully Assign To Customer";
                    }
                    else
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Customer Successfully Assign To Location";
                    }
                }
                else if (MVM.MS.ModeType == "3")
                {
                    MVM.ListMappingService = MappingService;
                    if (MVM.MS.Mode == "1")
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Location Successfully Assign To Service";
                    }
                    else
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Service Successfully Assign To Location";
                    }
                }
                else if (MVM.MS.ModeType == "4")
                {
                    MVM.ListMappingService = MappingService;
                    if (MVM.MS.Mode == "1")
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "Service Successfully Assign To User";
                    }
                    else
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode, "", BaseUserName, "");
                            }
                        }
                        ViewBag.message = "User Successfully Assign To Service";
                    }
                }
                else if (MVM.MS.ModeType == "5")
                {
                    MVM.ListMappingService = MappingService;
                    if (MVM.MS.Mode == "1")
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, MVM.MS.OppCode, row.Code, "", BaseUserName, row.Scode);
                            }
                        }
                        ViewBag.message = "Service Successfully Assign To User";
                    }
                    else
                    {
                        foreach (var row in MVM.ListMappingService)
                        {
                            DataTable Dt = MS.DeleteMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode);
                            if (row.Status == true)
                            {
                                DataTable Dt1 = MS.AddMappingService(MVM.MS.ModeType, row.Code, MVM.MS.OppCode, "", BaseUserName, MVM.MS.Scode);
                            }
                        }
                        ViewBag.message = "User Successfully Assign To Service";
                    }
                }
                ViewBag.TranXaction = "Done";
            }

            catch (Exception)
            {
                ViewBag.TranXaction = "Not Done";
            }
            return View();
        }
        #endregion

        #region Reports View Track Rights

        public ActionResult ReportsViewRights(string user)
        {
            Webx_View_TrackViewModel VMWVT = new Webx_View_TrackViewModel();
            List<Webx_View_Track> optrkList = MS.Get_ListOfReportsView(user);
            VMWVT.listWVTVM = optrkList;
            return View(VMWVT);
        }

        public ActionResult ListViewRight(string user)
        {
            Webx_View_TrackViewModel VMWVT = new Webx_View_TrackViewModel();
            List<Webx_View_Track> optrkList = MS.Get_ListOfReportsView(user);
            VMWVT.listWVTVM = optrkList;
            foreach (var item in optrkList)
            {
                if (item.chacked == "true")
                    item.New_chacked = true;
                else
                    item.New_chacked = false;
            }
            return PartialView("_ReportsViewRightsPartial", optrkList);
        }

        public ActionResult ReportsRightSubmit(List<Webx_View_Track> ReoprtRights, Webx_View_TrackViewModel wvtm)
        {
            String Str = "EXEC usp_Reset_View_TrackRights '" + wvtm.WVTVM.UserId + "'";
            DataTable Dt = GF.GetDataTableFromSP(Str);

            foreach (var item in ReoprtRights)
            {
                if (item.New_chacked == true)
                {
                    DataTable objDT = new DataTable();
                    objDT = MS.Assign_View_TrackRights(wvtm.WVTVM.UserId, item.L0_App_Module, item.L1_App_Module, item.App_Module);
                }
            }
            return RedirectToAction("ReportsViewRights");
        }

        #endregion

        #region Module Wise Acess Rights

        public ActionResult ModuleAcessRights(string user)
        {
            Webx_View_TrackViewModel VMWVT = new Webx_View_TrackViewModel();
            List<ModuleAcessModel> optrkList = MS.Get_ListOfModulRights(user).Where(c => c.Chk == "true").ToList();
            VMWVT.listWVTVM1 = optrkList;
            return View(VMWVT);
        }

        public JsonResult Getlvl2(string ParentCode)
        {
            List<ModuleAcessModel> DSFieldOfficerList = MS.GetLevel1("2", ParentCode, "").ToList();
            var users = (from user in DSFieldOfficerList
                         select new
                         {
                             Value = user.App_Module,
                             Text = user.Text
                         }).Distinct().ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Getlvl3(string ParentCode)
        {
            List<ModuleAcessModel> DSFieldOfficerList = MS.GetLevel1("3", ParentCode, "").ToList();
            var users = (from user in DSFieldOfficerList
                         select new
                         {
                             Value = user.App_Module,
                             Text = user.Text
                         }).Distinct().ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListModuleRight(Webx_View_TrackViewModel VMWVT)
        {
            List<ModuleAcessModel> optrkList = new List<ModuleAcessModel>();
            if (VMWVT.WVTVM1.Level3Text != "0" && VMWVT.WVTVM1.Level3Text != "" && VMWVT.WVTVM1.Level3Text != null)
                optrkList = MS.Get_ListOfModulRights(VMWVT.WVTVM1.Level3Text).Where(c => c.Chk == "true").ToList();

            else if (VMWVT.WVTVM1.Level2Text != "0" && VMWVT.WVTVM1.Level2Text != "" && VMWVT.WVTVM1.Level2Text != null)
                optrkList = MS.Get_ListOfModulRights(VMWVT.WVTVM1.Level2Text).Where(c => c.Chk == "true").ToList();

            else if (VMWVT.WVTVM1.Level1Text != "0" && VMWVT.WVTVM1.Level1Text != "" && VMWVT.WVTVM1.Level1Text != null)
                optrkList = MS.Get_ListOfModulRights(VMWVT.WVTVM1.Level1Text).Where(c => c.Chk == "true").ToList();

            VMWVT.listWVTVM1 = optrkList;
            foreach (var item in optrkList)
            {
                if (item.Chk == "true")
                    item.New_chacked = true;
                else
                    item.New_chacked = false;
            }
            return PartialView("_ModuleAcessRights", optrkList);
        }

        public ActionResult ModuleAcessSubmit(List<ModuleAcessModel> ReoprtRights, Webx_View_TrackViewModel wvtm)
        {
            foreach (var item in ReoprtRights)
            {

                DataTable objDT = new DataTable();
                objDT = MS.AssignModuleRight(wvtm.WVTVM1.Level1Text, wvtm.WVTVM1.Level2Text, wvtm.WVTVM1.Level3Text, item.UserID, (item.New_chacked ? "Y" : "N"));

            }
            return RedirectToAction("ModuleAcessRights");
        }

        #endregion

        #region HandlingCharge_Branch

        public ActionResult BranchHandlingCharge()
        {
            HandlingCharge ViewModelobjcharges = new HandlingCharge();
            return View(ViewModelobjcharges);
        }

        public ActionResult BranchHandlingChargeList(HandlingCharge ViewModelobjcharges)
        {
            List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
            ListLoading = MS.GetHandlingChrgeLoad().Where(c => c.Type == ViewModelobjcharges.Type && c.BranchCode == ViewModelobjcharges.Branch && c.CheckCharge == true).ToList();

            if (ListLoading.Count == 0)
            {
                for (int i = 1; i < 4; i++)
                {
                    webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();
                    objHandlingCharge.Id = i;

                    if (i == 1)
                        objHandlingCharge.LoadingBy = "O";
                    if (i == 2)
                        objHandlingCharge.LoadingBy = "M";
                    if (i == 3)
                        objHandlingCharge.LoadingBy = "B";

                    ListLoading.Add(objHandlingCharge);
                }
            }
            else
            {
                int ID = 0;
                foreach (var item in ListLoading)
                {
                    ID++;
                    item.Id = ID;
                }
            }
            ViewModelobjcharges.objChargesLoad = ListLoading;
            return View(ViewModelobjcharges);
        }

        public ActionResult AddListHandlingload(int Id)
        {
            webx_HandlingCharge objcharge = new webx_HandlingCharge();
            objcharge.Id = Id;
            return PartialView("_PartialHandlingchrge_load", objcharge);
        }

        public JsonResult GetType(string str)
        {
            List<Webx_Master_General> ListGenaral = new List<Webx_Master_General>();

            for (int i = 0; i < 4; i++)
            {
                Webx_Master_General objgeneral = new Webx_Master_General();
                if (i == 0)
                {
                    objgeneral.CodeDesc = "MF";
                    objgeneral.CodeId = "M";
                    ListGenaral.Add(objgeneral);
                }
                else if (i == 1)
                {
                    objgeneral.CodeDesc = "PRS";
                    objgeneral.CodeId = "P";
                    ListGenaral.Add(objgeneral);
                }
                else
                {
                    objgeneral.CodeDesc = "DRS";
                    objgeneral.CodeId = "D";
                    ListGenaral.Add(objgeneral);
                }
            }

            var SearchList = (from e in ListGenaral
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddHandlingCharge(HandlingCharge VW, List<webx_HandlingCharge> LoadingList)
        {
            string Status = "";
            try
            {
                DataTable Dt = new DataTable();
                //Dt = MS.DeleteHandlingChrge(VW.Branch, VW.Type,BaseUserName);
                if (LoadingList != null)
                {
                    string detail_XML = "<XMLLoadingList>";
                    foreach (var item in LoadingList)
                    {
                        string s = item.ContractType;
                        string[] values = s.Split(',');
                        for (int i = 0; i < values.Length; i++)
                        {
                            detail_XML = detail_XML + "<LoadingData>";
                            detail_XML = detail_XML + "<BranchCode>" + item.BranchCode + "</BranchCode>";
                            detail_XML = detail_XML + "<Type>" + VW.Type + "</Type>";
                            detail_XML = detail_XML + "<ContractType>" + values[i].Trim() + "</ContractType>";
                            detail_XML = detail_XML + "<LoadingBy>" + item.LoadingBy + "</LoadingBy>";
                            detail_XML = detail_XML + "<RateType>" + item.RateType + "</RateType>";
                            detail_XML = detail_XML + "<Rate>" + item.Rate + "</Rate>";
                            detail_XML = detail_XML + "<MaxAmount>" + item.MaxAmount + "</MaxAmount>";
                            detail_XML = detail_XML + "</LoadingData>";
                        }
                    }
                    //string sqlstrg = "INSERT INTO CYGNUS_HandlingCharge_Branch(BranchCode,Type,ContractType,LoadingBy,RateType,Rate,MaxAmount,CheckCharge,EntryBy,EntryDate)";
                    detail_XML = detail_XML + "</XMLLoadingList>";
                    Dt = MS.InsertHandlingChrge(detail_XML, VW.Branch, VW.Type, BaseUserName);
                    if (Dt.Rows[0]["MESSAGE"].ToString() == "SUCESS")
                    {
                        Status = "SUCESS";
                    }
                    else
                    {
                        Status = "FAILED";
                    }
                }
            }
            catch (Exception)
            {
                Status = "FAILED";
            }
            return RedirectToAction("BranchHandlingChargeDone", new { Status = Status });
        }

        public ActionResult BranchHandlingChargeDone(string Status)
        {
            ViewBag.Status = Status;
            return View();
        }

        #endregion

        #region Vendor Loading / Unloading

        public ActionResult LoadingUnloading()
        {
            HandlingCharge ViewModelobjcharges = new HandlingCharge();
            List<Webx_Master_General> ListLoadBy = new List<Webx_Master_General>();
            //ViewModelobjcharges.ListLocation = MS.GetWorkingLocationsNewPortal(BaseLocationCode, MainLocCode, BaseUserName.ToUpper());
            ListLoadBy = MS.GetGeneralMasterObject();
            ViewModelobjcharges.ListLodingBy = ListLoadBy.Where(c => c.CodeType == "LOADBY" && (c.CodeId.ToUpper() == "XX5" || c.CodeId.ToUpper() == "XX8") && c.StatusCode.ToUpper() == "Y").ToList();
            return View(ViewModelobjcharges);
        }

        public ActionResult LoadingChargeList(HandlingCharge ViewModelobjcharges)
        {
            List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
            ListLoading = MS.GetHandlingChrgeLoad().Where(c => c.Type == ViewModelobjcharges.Type && c.CheckCharge == false && c.BranchCode == ViewModelobjcharges.Branch && c.LoadingBy == ViewModelobjcharges.LoadingBy).Take(5).ToList();

            if (ListLoading.Count == 0)
            {
                for (int i = 1; i < 2; i++)
                {
                    webx_HandlingCharge objHandlingCharge = new webx_HandlingCharge();
                    objHandlingCharge.Id = i;
                    if (i == 1)
                        objHandlingCharge.LoadingBy = "M";
                    ListLoading.Add(objHandlingCharge);
                }
            }
            else
            {
                int ID = 0;
                foreach (var item in ListLoading)
                {
                    ID++;
                    item.Id = ID;
                }
            }
            ViewModelobjcharges.objChargesLoad = ListLoading;
            return View(ViewModelobjcharges);
        }

        [HttpPost]
        public ActionResult AddLoadingCharge(HandlingCharge VW, List<webx_HandlingCharge> LoadingList)
        {
            string Status = "";
            try
            {
                DataTable Dt = new DataTable();
                //Dt = MS.DeleteLoadingChrge(VW.Branch, VW.LoadingBy, VW.Type);

                if (LoadingList != null)
                {
                    string detail_XML = "<XMLLoadingList>";
                    foreach (var item in LoadingList)
                    {
                        detail_XML = detail_XML + "<LoadingData>";
                        detail_XML = detail_XML + "<BranchCode>" + item.BranchCode + "</BranchCode>";
                        detail_XML = detail_XML + "<Type>" + VW.Type + "</Type>";
                        detail_XML = detail_XML + "<LoadingBy>" + item.LoadingBy + "</LoadingBy>";
                        detail_XML = detail_XML + "<RateType>" + item.RateType + "</RateType>";
                        detail_XML = detail_XML + "<MonthlyAmount>" + item.MonthlyAmount + "</MonthlyAmount>";
                        detail_XML = detail_XML + "<VendorCode>" + item.VendorCode + "</VendorCode>";
                        detail_XML = detail_XML + "<IsMonthlyFixed>" + item.IsMonthlyFixed + "</IsMonthlyFixed>";
                        detail_XML = detail_XML + "</LoadingData>";
                    }
                    detail_XML = detail_XML + "</XMLLoadingList>";
                    Dt = MS.InsertLoadingChrge(detail_XML, VW.Type, VW.Branch, VW.LoadingBy, BaseUserName);

                    if (Dt.Rows[0]["MESSAGE"].ToString() == "SUCESS")
                    {
                        Status = "SUCESS";
                    }
                    else
                    {
                        Status = "FAILED";
                    }
                    //Dt = MS.InsertLoadingChrge(item.BranchCode, VW.Type, "", item.LoadingBy, item.RateType, item.MonthlyAmount, item.VendorCode, item.IsMonthlyFixed, BaseUserName);
                    //Status = MS.InsertLoadingUnLoadingVendorTypeMapping(detail_XML.ReplaceSpecialCharacters());
                }
            }
            catch (Exception)
            {

                Status = "FAILED";
            }
            //return Status;
            return RedirectToAction("LoadingUnloadingDone", new { Status = Status });
        }

        public ActionResult LoadingUnloadingDone(string Status)
        {
            ViewBag.Status = Status;
            return View();
        }

        public ActionResult AddListChargeload(int Id)
        {
            webx_HandlingCharge objcharge = new webx_HandlingCharge();
            objcharge.Id = Id;
            return PartialView("_PartialCharges_load", objcharge);
        }

        #endregion

        #region Customer Loading / Unloading

        public ActionResult CustomerLoadUnloadList()
        {
            webx_HandlingCharge obj = new webx_HandlingCharge();
            return View(obj);
        }

        public JsonResult GetCustomerchargeListJson(string Type)
        {
            List<webx_HandlingCharge> ListLoading = new List<webx_HandlingCharge>();
            ListLoading = MS.GetHandlingChrgeLoad().Where(c => c.IsCustomer == true && c.Type == Type).ToList();
            List<Webx_Master_General> RateList = MS.GetGeneralMasterObject().Where(c => c.CodeType == "HANDCHRG" && c.StatusCode == "Y").OrderBy(c => c.CodeId).ToList();
            var ListVendorsdata = (from e in ListLoading
                                   select new
                                   {
                                       CUSTCode = e.CustomerCode,
                                       CUSTBRANCHCODE = e.BranchCode,
                                       CUSTLOADBY = e.LoadingBy,
                                       CUSTRATETYPE = RateList.Where(c => c.CodeId.ToString() == e.RateType).FirstOrDefault().CodeDesc,
                                       CUSTRATE = e.Rate,
                                       CUSTAMOUNT = e.CustomerCharges,

                                       ID = e.Id
                                   }).ToArray();
            return Json(ListVendorsdata, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCustomerLoadUnload(int Id)
        {
            webx_HandlingCharge obj = new webx_HandlingCharge();
            if (Id > 0)
            {
                List<webx_HandlingCharge> ListUnLoading = new List<webx_HandlingCharge>();
                obj = MS.GetHandlingChrgeLoad().Where(c => c.Id == Id).FirstOrDefault();
            }

            return View("AddCustomerLoadUnload", obj);
        }

        public ActionResult DeleteCustomerLoadUnload(int Id)
        {
            if (Id > 0)
            {
                MS.DeleteHandlingChrgeLoad(Id);
            }

            return RedirectToAction("CustomerLoadUnloadList");
        }

        public ActionResult AddCustomerChargesSubmit(webx_HandlingCharge obj)
        {
            DataTable Dt = new DataTable();
            if (obj.Id > 0)
            {
                Dt = MS.UpdatecustomerCharges(obj.Id, obj.BranchCode, obj.Type, obj.LoadingBy, obj.RateType, obj.Rate, obj.CustomerCharges, obj.CustomerCode);
            }
            else
            {
                Dt = MS.InsertCustomerLoadingChrge(obj.BranchCode, obj.Type, obj.LoadingBy, obj.RateType, obj.Rate, obj.CustomerCharges, obj.CustomerCode, "1", BaseUserName, System.DateTime.Now.ToString("dd MMM yyyy"));
            }

            return RedirectToAction("CustomerLoadUnloadList");
        }

        #endregion

        #region Get_LoadingCharge

        public JsonResult getBranchWiseLoadingUnloadingVendorListJson(string Type, string VendorType)
        {
            List<webx_VENDOR_HDR> ListVendor = new List<webx_VENDOR_HDR>();
            if (string.IsNullOrEmpty(VendorType))
            {
                ListVendor = MS.getBranchWiseLoadingUnloadingVendorListJson().Where(c => c.Active == "Y" && c.BranchCode.ToUpper().Trim() == BaseLocationCode.ToUpper() && c.Type.ToUpper() == Type.ToUpper()).OrderBy(c => c.VENDORNAME).ToList();
            }
            else
            {
                ListVendor = MS.getBranchWiseLoadingUnloadingVendorListJson().Where(c => c.Active == "Y" && c.BranchCode.ToUpper().Trim() == BaseLocationCode.ToUpper() && c.Type.ToUpper() == Type.ToUpper() && c.Vendor_Type.ToUpper() == VendorType.ToUpper()).OrderBy(c => c.VENDORNAME).ToList();
            }
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  Value = e.VENDORCODE,
                                  Text = e.VENDORNAME,
                              }).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get_LoadingCharge(string brdc, string loadingby, string chargeType, string TypeModule, string Vendorcode, string loadunloadType)
        {

            CYGNUS_THC_Financial_Details ListRate = MS.GetLoadingCharge(BaseLocationCode, loadingby, chargeType, TypeModule, Vendorcode, loadunloadType).FirstOrDefault();
            decimal Rate = ListRate.Rate;
            decimal Montly = ListRate.MaxLimit;
            bool IsMonthly = ListRate.IsMonthly;
            return new JsonResult()
            {
                Data = new
                {
                    Rate = Rate,
                    MaxLimit = Montly,
                    IsMonthly = IsMonthly
                }
            };
        }

        public ActionResult Get_BrachWiseMathadiCharge(string LoadingBy, string ContractType, string Type)
        {

            CYGNUS_THC_Financial_Details ListRate = MS.GetBrachWiseMathadiCharge(BaseLocationCode.ToUpper(), LoadingBy, ContractType, Type).FirstOrDefault();
            decimal Rate = ListRate.Rate;
            decimal Montly = ListRate.MaxLimit;
            return new JsonResult()
            {
                Data = new
                {
                    Rate = Rate,
                    MaxLimit = Montly
                }
            };
        }

        public ActionResult GetChargesFromCNoteNo(string DockNo, string RateType, string LoadType, string LoadingBy, string TypeModule, string Vendorcode)
        {

            CYGNUS_THC_Financial_Details ListRate = MS.GetChargesFromCNoteNo(DockNo.ToString(), RateType, LoadType, LoadingBy, TypeModule, Vendorcode, BaseLocationCode.ToUpper()).FirstOrDefault();
            decimal Rate = ListRate.Rate;
            return new JsonResult()
            {
                Data = new
                {
                    Rate = Rate,
                }
            };
        }

        #endregion

        #region  Kilometer Change

        public ActionResult kilometerChange()
        {
            VehicleKm WVVM = new VehicleKm();
            return View(WVVM);
        }

        public JsonResult GetKMList_NameJson(string SerchGroup, string type)
        {
            List<VehicleKm> ListGroups = new List<VehicleKm> { };
            try
            {
                DataTable Dt_Location = MS.GetKMListDetail(SerchGroup, type);
                string Current_Km_Read = "";
                string PRS_DRS_THC_KM = "";
                string VEH_INTERNAL_NO = "";
                string VEHNO = "";
                string VehicleNo = "";
                string f_issue_startkm = "";

                if (Dt_Location.Rows.Count > 0)
                {
                    if (type == "V")
                    {
                        VEHNO = Dt_Location.Rows[0]["VEHNO"].ToString();
                        VEH_INTERNAL_NO = Dt_Location.Rows[0]["VEH_INTERNAL_NO"].ToString();
                        Current_Km_Read = Dt_Location.Rows[0]["Current_Km_Read"].ToString();
                        PRS_DRS_THC_KM = Dt_Location.Rows[0]["PRS_DRS_THC_KM"].ToString();
                    }
                    else
                    {
                        VehicleNo = Dt_Location.Rows[0]["VehicleNo"].ToString();
                        f_issue_startkm = Dt_Location.Rows[0]["f_issue_startkm"].ToString();
                    }
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        VEHNO = VEHNO,
                        VEH_INTERNAL_NO = VEH_INTERNAL_NO,
                        Current_Km_Read = Current_Km_Read,
                        PRS_DRS_THC_KM = PRS_DRS_THC_KM,
                        f_issue_startkm = f_issue_startkm,
                        VehicleNo = VehicleNo,
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetKMList_NameJson", "Json", "Listing", ex.Message);
                return Json(ListGroups);
            }
        }

        [HttpPost]
        public ActionResult kilometerChangeSubmit(VehicleKm vk)
        {
            string VhNO = "", TranXaction = "Done";
            DataTable Dt_Name = new DataTable();
            try
            {
                Dt_Name = MS.ChangeKM(vk.VEHNO, vk.Current_KM_Read, vk.PRS_DRS_THC_KM, vk.VEH_INTERNAL_NO, vk.f_issue_startkm, vk.KMChgTyp, vk.VehicleNo, vk.VSlipNo);
                if (vk.KMChgTyp == "V")
                {
                    VhNO = Dt_Name.Rows[0]["VEHNO"].ToString();
                }
                else
                {
                    VhNO = Dt_Name.Rows[0]["VSlipNo"].ToString();
                }
            }
            catch
            {
                TranXaction = "Not Done";
            }
            return RedirectToAction("KMUpdateDone", new { VhNO = VhNO, TranXaction = TranXaction });
        }

        public ActionResult KMUpdateDone(string VhNO, string TranXaction)
        {
            ViewBag.VhNO = VhNO;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region Add Edit Accounts Masters

        public ActionResult AddAccount(AddAccountModel AAM)
        {
            try
            {
                if (AAM.Type == "1")
                {
                    string sql = "select * from webx_acctInfo where Acccode='" + AAM.AccGroup + "'";
                    DataTable sqlDT = GF.GetDataTableFromSP(sql);
                    AAM.AccountCode = sqlDT.Rows[0]["Acccode"].ToString();
                    //AAM.AccountCategory = sqlDT.Rows[0]["Acccategory"].ToString();
                    AAM.AccGroup = sqlDT.Rows[0]["Groupcode"].ToString();
                    AAM.ManualAccountCode = sqlDT.Rows[0]["Company_Acccode"].ToString();
                    AAM.AccountDescription = sqlDT.Rows[0]["Accdesc"].ToString();
                    AAM.Category = sqlDT.Rows[0]["Acccategory"].ToString();
                    AAM.LocationCode = sqlDT.Rows[0]["Brcd"].ToString();
                    if (AAM.LocationCode == "All")
                    { AAM.IsAllBranch = "Y"; }
                    else
                    { AAM.IsAllBranch = "N"; }
                    AAM.ACTIVEFLAG = sqlDT.Rows[0]["ACTIVEFLAG"].ToString();
                    AAM.AccountNo = sqlDT.Rows[0]["bkAcctNo"].ToString();
                    AAM.BankLocationCode = sqlDT.Rows[0]["bkloccode"].ToString();
                }
                return View(AAM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public JsonResult Get_AccountGroup(string Id)
        {
            Id = (Id == null ? "" : Id.ToUpper());
            List<WEBX_GROUPS> TyreModelList = MS.Get_AccountGroupList(Id).ToList();
            var ListCat = (from e in TyreModelList
                           select new
                           {
                               Value = e.Groupcode,
                               Text = e.Groupdesc
                           });
            return Json(ListCat, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddAccountSubmit(AddAccountModel AAM)
        {
            try
            {
                string hdnmancoderule = "N";
                string hdnbrcdrule = "N";
                double total = 0.0;
                string company_acccode = "";
                string BRCD = "";
                string acct_prefix = "";
                string Maxcode = "";
                string NewAccountCode = "";

                string total1 = "select count(*) as Total FROM webx_modules_rules where Module_Name='Account Master'";
                DataTable totalDT = GF.GetDataTableFromSP(total1);
                total = Convert.ToDouble(totalDT.Rows[0]["Total"].ToString());

                if (total > 0)
                {
                    string hdnmancoderule1 = ("select RULE_Y_N FROM webx_modules_rules where Module_Name='Account Master' and RULE_DESC='Set Manual Account Code As System Generated Account Code'").ToString();
                    DataTable hdnmancoderuleDT = GF.GetDataTableFromSP(hdnmancoderule1);
                    hdnmancoderule = hdnmancoderuleDT.Rows[0]["RULE_Y_N"].ToString();

                    string hdnbrcdrule1 = ("select RULE_Y_N FROM webx_modules_rules where Module_Name='Account Master' and RULE_DESC='Set Branch Code For Account Head'").ToString();
                    DataTable hdnbrcdruleDT = GF.GetDataTableFromSP(hdnbrcdrule1);
                    hdnbrcdrule = hdnbrcdruleDT.Rows[0]["RULE_Y_N"].ToString();
                }
                if (AAM.Type == "0")
                {
                    string sql = "select acct_prefix from webx_groups where groupcode='" + AAM.AccGroup + "'";
                    DataTable sqlDT = GF.GetDataTableFromSP(sql);
                    if (sqlDT.Rows[0]["acct_prefix"].ToString() != "Null" || sqlDT.Rows[0]["acct_prefix"].ToString() != "")
                    {
                        acct_prefix = sqlDT.Rows[0]["acct_prefix"].ToString();
                    }

                    if (acct_prefix == null || acct_prefix == "")
                    {
                        acct_prefix = AAM.AccGroup.Substring(0, 3);
                    }
                    string whrcls = "substring(acccode,1," + acct_prefix.Length + ")='" + acct_prefix + "'";
                    string selcls = "isNull(Max(Right(acccode, Len(acccode) - " + acct_prefix.Length + ") + 1),0)";
                    sql = "select " + selcls + " as Maxcode from webx_acctInfo where " + whrcls;

                    DataTable sqlDT1 = GF.GetDataTableFromSP(sql);

                    if (sqlDT1.Rows.Count > 0)
                    {
                        Maxcode = sqlDT1.Rows[0]["Maxcode"].ToString();
                    }
                    if (Maxcode == null || Maxcode == "")
                    {
                        Maxcode = "1";
                    }
                    int totallenght = 4 + acct_prefix.Length - Maxcode.Length;
                    if (Maxcode == null || Maxcode == "")
                    {
                        NewAccountCode = acct_prefix.PadRight(totallenght, '0') + "1";
                    }
                    else
                    {
                        NewAccountCode = acct_prefix.PadRight(totallenght, '0') + Maxcode;
                    }
                    NewAccountCode = NewAccountCode.ToUpper();

                    if (hdnmancoderule == "N")// company_acccode = new_sys_Group_Code;   if "Y" then new_sys_Group_Code
                        company_acccode = AAM.ManualAccountCode;
                    else
                        company_acccode = NewAccountCode;

                    if (AAM.IsAllBranch == "Y")
                    {
                        BRCD = "All";
                    }
                    else
                    {
                        BRCD = AAM.LocationCode;
                    }
                    DataTable objDT = new DataTable();
                    objDT = MS.Insert_Account_Master(NewAccountCode, AAM.AccountDescription, AAM.AccGroup, AAM.Category, BRCD, BaseUserName, AAM.ACTIVEFLAG, AAM.AccountNo, AAM.BankLocationCode, company_acccode,BaseCompanyCode);
                }
                else
                {
                    if (hdnmancoderule == "Y")
                        company_acccode = AAM.AccGroup;
                    else
                        company_acccode = AAM.ManualAccountCode;

                    if (AAM.IsAllBranch == "Y")
                    {
                        BRCD = "All";
                    }
                    else
                    {
                        BRCD = AAM.LocationCode;
                    }
                    NewAccountCode = AAM.AccountCode;
                    DataTable objDT = new DataTable();
                    objDT = MS.usp_Update_Account_Master(AAM.AccountCode, AAM.AccountDescription, AAM.AccGroup, AAM.Category, BRCD, BaseUserName, AAM.ACTIVEFLAG, AAM.AccountNo, AAM.BankLocationCode, company_acccode,BaseCompanyCode);
                }
                return RedirectToAction("AccountAdd_Edit_Done", new { AccountCode = NewAccountCode, Type = AAM.Type, AccGroup = AAM.AccGroup });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
        }

        public ActionResult AccountAdd_Edit_Done(string AccountCode, string Type, string AccGroup)
        {
            ViewBag.AccountCode = AccountCode;
            ViewBag.Type = Type;
            ViewBag.AccGroup = AccGroup;
            return View();
        }

        public ActionResult EditAccount()
        {
            AddAccountModel AAM = new AddAccountModel();
            AAM.Type = "1";
            return View(AAM);
        }

        public JsonResult Get_AccountGroupAll(string Id)
        {
            Id = (Id == null ? "" : Id.ToUpper());
            List<WEBX_GROUPS> TyreModelList = MS.Get_AccountGroupListAll(Id).ToList();
            var ListCat = (from e in TyreModelList
                           select new
                           {
                               Value = e.Groupcode,
                               Text = e.Groupdesc
                           });
            return Json(ListCat, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Add/Edit Account Groups

        public ActionResult AddAccountGroup()
        {
            AddEditAccountGroupViewModel AGVM = new AddEditAccountGroupViewModel();
            AGVM.Group = new webx_Groups();
            return View(AGVM);
        }

        public ActionResult AddAccountGroupSubmit(AddEditAccountGroupViewModel AGVM)
        {
            int Status = 0;
            string Msg = "Not Done";
            string Id = "0";
            DataTable dt = MS.AddGroupEntry(AGVM.Group.Groupdesc, AGVM.Group.Parentcode, AGVM.Group.Company_Groupcode, AGVM.Group.main_category, BaseUserName, BaseCompanyCode);
            if (dt != null)
            {
                Status = Convert.ToInt32(dt.Rows[0][0]);
                Msg = dt.Rows[0][1].ToString();
                Id = dt.Rows[0][2].ToString();
            }

            return RedirectToAction("AddAccountGroupDone", new { GrpCode = Id, Msg = Msg });
        }

        public ActionResult AddAccountGroupDone(string GrpCode, string Msg)
        {
            ViewBag.Message = Msg;
            ViewBag.GId = GrpCode;
            return View();
        }

        public ActionResult EditAccountGroup()
        {
            AddEditAccountGroupViewModel AGVM = new AddEditAccountGroupViewModel();
            return View(AGVM);
        }

        public string GETGroupListDDL(string searchTerm, string MainCategory)
        {
            return JsonConvert.SerializeObject(MS.GetGroupListObject(searchTerm, MainCategory, BaseLocationCode));
        }

        public ActionResult GetUpdateAccountGroup(AddEditAccountGroupViewModel AGVM)
        {
            DataTable dt = new DataTable();
            dt = MS.GetGroupEntrytoEdit(AGVM.Group.main_category, AGVM.Group.Groupdesc, AGVM.Group.Groupcode, AGVM.Group.Company_Groupcode);
            AGVM.GroupsList = DataRowToObject.CreateListFromTable<webx_Groups>(dt);
            if (AGVM.GroupsList != null)
            {
                AGVM.EditGroup = AGVM.GroupsList.FirstOrDefault();
            }
            else
            {
                AGVM.EditGroup = new webx_Groups();
            }
            return View(AGVM);
        }

        public ActionResult GetUpdateAccountGroupSubmit(AddEditAccountGroupViewModel AGVM)
        {
            DataTable dt = new DataTable();
            dt = MS.EditGroupEntry(AGVM.EditGroup.Groupcode, AGVM.EditGroup.Groupdesc, AGVM.EditGroup.Company_Groupcode, BaseUserName, BaseCompanyCode);
            int Status = 0;
            string Msg = "Not Done";
            if (dt != null)
            {
                Status = Convert.ToInt32(dt.Rows[0][0]);
                Msg = dt.Rows[0][1].ToString();
            }
            return RedirectToAction("UpdateAccountGroupDone", new { GCode = AGVM.EditGroup.Groupcode, Msg = Msg });
        }

        public ActionResult UpdateAccountGroupDone(string GCode, string Msg)
        {
            ViewBag.GId = GCode;
            ViewBag.Message = Msg;
            return View();
        }

        #endregion

        #region Customer Service Mapping

        public ActionResult CustomerServiceMapping()
        {
            CustomerServiceMappingViewModel cyopblac = new CustomerServiceMappingViewModel();
            cyopblac.Custlist = new List<webx_CUSTHDR>();

            return View(cyopblac);
        }

        public JsonResult GetCustomerJson(string searchTerm)
        {
            List<webx_CUSTHDR> ListCustomer = new List<webx_CUSTHDR>();

            ListCustomer = MS.GetByCustomerList().Where(c => c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTCD.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CUSTNM).ToList();
            var SearchList = (from e in ListCustomer
                              select new
                              {
                                  Value = e.CUSTCD,
                                  Text = e.CUSTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CustomerServiceMappingNext(string id, string MapType, string Code, string Mode, string Name)
        {
            CustomerServiceMappingViewModel CSMV = new CustomerServiceMappingViewModel();
            CSMV.WCM = new webx_CUSTHDR();
            CSMV.Custlist = MS.MappingServiceList(MapType, Mode).ToList();
            if (Mode == "1")
            {
                ViewBag.Name = "Service";
                CSMV.Custlist1 = MS.CustServiceList().Where(c => c.CUSTCD == Code).ToList();

                foreach (var row1 in CSMV.Custlist1)
                {
                    foreach (var row in CSMV.Custlist)
                    {
                        if (row.Code == row1.Service_Code)
                        {
                            if (row1.Active == "Y")
                            {
                                row.Status = true;
                            }
                        }
                    }
                }
            }
            else
            {
                ViewBag.Name = " Customer";
                CSMV.Custlist = MS.MappingCustomerListByService("1", Mode, Code);
            }

            CSMV.WCM.Mode = Mode;
            CSMV.WCM.OppCode = Code;
            return View("CustService", CSMV);
        }

        public ActionResult CustServiceDone(CustomerServiceMappingViewModel CSMV, List<webx_CUSTHDR> CustSer)
        {
            try
            {
                CSMV.Custlist = CustSer;
                if (CSMV.WCM.Mode == "1")
                {
                    foreach (var row in CSMV.Custlist)
                    {
                        DataTable Dt = MS.DeleteCustMappingService("1", CSMV.WCM.OppCode, row.Code);
                        if (row.Status == true)
                        {
                            DataTable Dt1 = MS.AddCustMappingService("1", CSMV.WCM.OppCode, row.Code, row.Name, BaseUserName, "");
                        }
                    }
                    ViewBag.message = "Customer Successfully Assign To Service";
                }
                else
                {
                    foreach (var row in CSMV.Custlist)
                    {
                        DataTable Dt = MS.DeleteCustMappingService("1", CSMV.WCM.OppCode, row.Code);
                        if (row.Status == true)
                        {
                            DataTable Dt1 = MS.AddCustMappingService("1", CSMV.WCM.OppCode, row.Code, row.Name, BaseUserName, "");
                        }
                    }
                    ViewBag.message = "Service Successfully Assign To Customer";
                }

                ViewBag.TranXaction = "Done";
            }

            catch (Exception)
            {
                ViewBag.TranXaction = "Not Done";
            }
            return View();
        }

        #endregion

        #region Settings Opening Balance // Customer /Vendor / Driver / Employee

        public JsonResult GetCustomerListByBranchJson(string searchTerm, string Loc)
        {
            List<webx_CUSTHDR> Listcustomer = new List<webx_CUSTHDR>();
            //Listcustomer = MS.GetCustomerMasterObject().Where(d => d.CUSTLOC.ToUpper().Contains(Loc.ToUpper())).ToList().Where(c => c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper()) || c.CUSTNM.ToUpper().Contains(searchTerm.ToUpper())).ToList().OrderBy(c => c.CUSTNM).ToList();
            Listcustomer = MS.GetCustomerListingNew(searchTerm, "", "").Where(d => d.CUSTLOC.ToUpper().Contains(Loc.ToUpper())).OrderBy(c => c.CUSTNM).ToList();
            var SearchList = (from e in Listcustomer
                              select new
                              {
                                  id = e.CUSTCD,
                                  text = e.CUSTNM,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SettingOpeningBalance()
        {
            //SettingOpeningBalanceCriteria cyopblac = new SettingOpeningBalanceCriteria();
            SettingOpeningBalanceViewModel cyopblac = new SettingOpeningBalanceViewModel();
            cyopblac.CriteriaMd = new SettingOpeningBalanceCriteria();
            return View(cyopblac);
        }

        [HttpPost]
        public ActionResult SetOpeningBalanceAssign(SettingOpeningBalanceViewModel SM)
        {
            DataTable Dt = new DataTable();
            if (!string.IsNullOrEmpty(SM.CriteriaMd.Selectedtype))
            {
                if (SM.CriteriaMd.Selectedtype == "Customer")
                {
                    Dt = MS.GetSettingOpeningBalanceList("C", "", SM.CriteriaMd.CustomerName, SM.CriteriaMd.CustEffectiveAccount, SM.CriteriaMd.CustLocationName, BaseYearVal);
                }
                else if (SM.CriteriaMd.Selectedtype == "Vendor")
                {

                    string[] Vend_account_arr = SM.CriteriaMd.EffectiveVendorType.Split('~');
                    string vendor_type = Vend_account_arr[0].ToString();

                    Dt = MS.GetSettingOpeningBalanceList("V", vendor_type, SM.CriteriaMd.VendorName, "", SM.CriteriaMd.VendorLocationName, BaseYearVal);
                }

                else if (SM.CriteriaMd.Selectedtype == "Employee")
                {

                    Dt = MS.GetSettingOpeningBalanceList("E", "", SM.CriteriaMd.EmployeeName, SM.CriteriaMd.EmployeeEffectiveAccount, SM.CriteriaMd.EmployeeLocationName, BaseYearVal);
                }

                else if (SM.CriteriaMd.Selectedtype == "Driver")
                {

                    Dt = MS.GetSettingOpeningBalanceList("D", "", SM.CriteriaMd.DriverName, SM.CriteriaMd.DriverEffectiveAccount, SM.CriteriaMd.DriverLocationName, BaseYearVal);
                }
            }

            SM.SearchList = DataRowToObject.CreateListFromTable<OpeningBalanceCriteriaList>(Dt);
            return View(SM);
            //return RedirectToAction("SetOpeningBalanceAssign", "Master");
        }

        [HttpPost]
        public ActionResult OpeningBalanceAssignSubmit(SettingOpeningBalanceViewModel SM, List<OpeningBalanceCriteriaList> Master)
        {
            Rslt result = new Rslt();
            List<Rslt> RLS = new List<Rslt>();
            try
            {

                if (SM.CriteriaMd.Selectedtype == "Customer")
                {
                    foreach (var Item in Master)
                    {
                        if (Item.IsChecked == true)
                        {
                            try
                            {
                                DataTable rslt = MS.SettingOpeningBalance(SM.CriteriaMd.Selectedtype, Item.opendebit.ToString(), Item.opencredit.ToString(), SM.CriteriaMd.CustEffectiveAccount, Item.custcode, SM.CriteriaMd.CustLocationName, BaseUserName, BaseCompanyCode, BaseFinYear);
                                result.Id = rslt.Rows[0][0].ToString();
                                result.Status = Convert.ToInt32(rslt.Rows[0][1]);
                                result.Msg = rslt.Rows[0][2].ToString();
                                RLS.Add(result);
                            }
                            catch (Exception)
                            {
                                result.Id = Item.custcode;
                                result.Status = 0;
                                result.Msg = "Error";
                                RLS.Add(result);
                            }
                        }
                    }
                }
                else if (SM.CriteriaMd.Selectedtype == "Vendor")
                {
                    string[] Vend_account_arr = SM.CriteriaMd.EffectiveVendorType.Split('~');
                    string vendor_type = Vend_account_arr[1].ToString();
                    foreach (var Item in Master)
                    {
                        if (Item.IsChecked == true)
                        {
                            try
                            {
                                DataTable rslt = MS.SettingOpeningBalance(SM.CriteriaMd.Selectedtype, Item.opendebit.ToString(), Item.opencredit.ToString(), vendor_type, Item.custcode, SM.CriteriaMd.VendorLocationName, BaseUserName, BaseCompanyCode, BaseFinYear);
                                result.Id = rslt.Rows[0][0].ToString();
                                result.Status = Convert.ToInt32(rslt.Rows[0][1]);
                                result.Msg = rslt.Rows[0][2].ToString();
                                RLS.Add(result);
                            }
                            catch (Exception)
                            {
                                result.Id = Item.custcode;
                                result.Status = 0;
                                result.Msg = "Error";
                                RLS.Add(result);
                            }
                        }
                    }
                }

                else if (SM.CriteriaMd.Selectedtype == "Employee")
                {
                    foreach (var Item in Master)
                    {
                        if (Item.IsChecked == true)
                        {
                            try
                            {
                                DataTable rslt = MS.SettingOpeningBalance(SM.CriteriaMd.Selectedtype, Item.opendebit.ToString(), Item.opencredit.ToString(), SM.CriteriaMd.EmployeeEffectiveAccount, Item.custcode, SM.CriteriaMd.EmployeeLocationName, BaseUserName, BaseCompanyCode, BaseFinYear);
                                result.Id = rslt.Rows[0][0].ToString();
                                result.Status = Convert.ToInt32(rslt.Rows[0][1]);
                                result.Msg = rslt.Rows[0][2].ToString();
                                RLS.Add(result);
                            }
                            catch (Exception)
                            {
                                result.Id = Item.custcode;
                                result.Status = 0;
                                result.Msg = "Error";
                                RLS.Add(result);
                            }
                        }
                    }
                }

                else if (SM.CriteriaMd.Selectedtype == "Driver")
                {
                    foreach (var Item in Master)
                    {
                        if (Item.IsChecked == true)
                        {
                            try
                            {
                                DataTable rslt = MS.SettingOpeningBalance(SM.CriteriaMd.Selectedtype, Item.opendebit.ToString(), Item.opencredit.ToString(), SM.CriteriaMd.DriverEffectiveAccount, Item.custcode, SM.CriteriaMd.DriverLocationName, BaseUserName, BaseCompanyCode, BaseFinYear);
                                result.Id = rslt.Rows[0][0].ToString();
                                result.Status = Convert.ToInt32(rslt.Rows[0][1]);
                                result.Msg = rslt.Rows[0][2].ToString();
                                RLS.Add(result);
                            }
                            catch (Exception)
                            {
                                result.Id = Item.custcode;
                                result.Status = 0;
                                result.Msg = "Error";
                                RLS.Add(result);
                            }
                        }
                    }
                }

                SM.Result = RLS;
            }
            catch (Exception)
            {
                SM.Result = RLS;
            }

            return View(SM);
        }

        #endregion

        #region Pincode Location Mapping Master

        public ActionResult PincodeLocationMaping()
        {
            webx_pincodemasterViewModel WPVM = new webx_pincodemasterViewModel();
            try
            {
                WPVM.listWPM = new List<webx_pincode_master>();
                WPVM.listWPM = MS.GetPincodeMasterObject().ToList();
                WPVM.WPM = new webx_pincode_master();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WPVM);
        }

        public ActionResult GetPincodeLocationMaping(int Pincode)
        {
            webx_pincode_master WMAM = new webx_pincode_master();
            try
            {
                if (Pincode > 0)
                {
                    WMAM = MS.GetPincodeMasterObject().FirstOrDefault(c => c.pincode == Pincode);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditPinCodeMaster", WMAM);
        }

        public ActionResult AddEditPinCodeMasterList(webx_pincode_master WPM)
        {
            bool Status = false;
            if (WPM.ActiveFlag == "false")
            {
                WPM.ActiveFlag = "N";
            }
            try
            {
                if (WPM.ActiveFlag.ToString() == "true")
                    WPM.ActiveFlag = "Y";
                string MstDetails = "<PincodeMaster>";
                MstDetails = MstDetails + "<pincode>" + WPM.pincode + "</pincode>";
                MstDetails = MstDetails + "<StateCode>" + WPM.StateCode + "</StateCode>";
                MstDetails = MstDetails + "<cityname>" + WPM.cityname + "</cityname>";
                MstDetails = MstDetails + "<Area>" + WPM.Area + "</Area>";
                MstDetails = MstDetails + "<ActiveFlag>" + WPM.ActiveFlag + "</ActiveFlag>";
                MstDetails = MstDetails + "<EntryBy>" + BaseUserName + "</EntryBy>";
                MstDetails = MstDetails + "<LocCode>" + WPM.LocCode + "</LocCode>";
                MstDetails = MstDetails + "</PincodeMaster>";
                DataTable Dt = MS.AddEditPincodeMaster(MstDetails);
                if (Dt != null && Dt.Rows.Count > 0 && Dt.Rows[0]["TranXaction"].ToString() == "Done")
                {
                    Status = true;
                }
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(Status, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Balance Transfer For Ledger/Sub-Ledger

        public ActionResult BalanceTransfer()
        {
            return View();
        }

        public ActionResult BalanceTransferAssign(BalanceTransferForLedgerSubLedgerViewModel BTFLSV)
        {
            if (BTFLSV.Ledger == "ALL")
            {
                return RedirectToAction("SubmitLedger", BTFLSV);
            }
            else if (BTFLSV.PndLLedger == "ALL")
            {
                return RedirectToAction("SubmitPandLedger", BTFLSV);
            }
            else if (BTFLSV.SubLedger == "ALL" || BTFLSV.SubLedger == "1" || BTFLSV.SubLedger == "2" || BTFLSV.SubLedger == "3" || BTFLSV.SubLedger == "4")
            {
                return RedirectToAction("SubmitsubLedger", BTFLSV);
            }
            return View(BTFLSV);
        }

        public ActionResult SubmitLedger(BalanceTransferForLedgerSubLedgerViewModel BTFLSV)
        {
            string BaseYear = BaseFinYear.Split('-')[0].ToString();
            try
            {
                DataTable Dt = MS.LedgerSumbitType("1", BTFLSV.Ledger, "1", BaseYear, BaseUserName, BaseCompanyCode);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BalanceTransferDone");
        }

        public ActionResult SubmitsubLedger(BalanceTransferForLedgerSubLedgerViewModel BTFLSV)
        {
            string BaseYear = BaseFinYear.Split('-')[0].ToString();
            try
            {
                DataTable Dt = MS.SubLedgerSumbitType("2", "All", BTFLSV.SubLedger, BaseYear, BaseUserName, BaseCompanyCode);

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BalanceTransferDone");
        }

        public ActionResult SubmitPandLedger(BalanceTransferForLedgerSubLedgerViewModel BTFLSV)
        {
            string BaseYear = BaseFinYear.Split('-')[0].ToString();
            try
            {
                DataTable Dt = MS.PndLedgerSumbitType("3", "All", "1", BaseYear, BaseUserName, BaseCompanyCode);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BalanceTransferDone");
        }

        public ActionResult BalanceTransferDone()
        {
            return View();
        }

        #endregion

        #region Loading UnLoading VendorType Mapping

        public ActionResult LoadingUnLoadingVendorTypeMapping()
        {
            HandlingCharge ViewModelobjcharges = new HandlingCharge();
            return View(ViewModelobjcharges);
        }

        public ActionResult LoadingUnLoadingVendorTypeMappingList(HandlingCharge ViewModelobjcharges)
        {
            List<CYGNUS_LoadingUnLoading_VendorType_Mapping> objList = new List<CYGNUS_LoadingUnLoading_VendorType_Mapping>();
            objList = MS.GetLoadingUnLoadingVendorTypeMappingList("", ViewModelobjcharges.Branch, "").Take(5).ToList();
            ViewModelobjcharges.Listvndt = objList;
            return View(ViewModelobjcharges);
        }

        public ActionResult AddObjLoadingUnLoadingVendorTypeMapping(int Id)
        {
            CYGNUS_LoadingUnLoading_VendorType_Mapping objcharge = new CYGNUS_LoadingUnLoading_VendorType_Mapping();
            objcharge.Srno = Id;
            return PartialView("_LoadingUnLoadingVendorTypeMapping", objcharge);
        }

        public JsonResult GetLoadingUnloaingVendorTypeListJson(string str)
        {
            List<Webx_Master_General> ListVendorType = new List<Webx_Master_General>();
            ListVendorType = MS.GetGeneralMasterObject().Where(c => c.CodeType == "LOADBY" && c.StatusCode == "Y").ToList().OrderBy(c => c.CodeDesc).ToList();

            var SearchList = (from e in ListVendorType
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string AddLoadingUnLoadingVendorTypeMapping(HandlingCharge VW, List<CYGNUS_LoadingUnLoading_VendorType_Mapping> MappingList)
        {
            bool Status = false;
            try
            {
                if (MappingList != null)
                {
                    string detail_XML = "<ArrayOfMappingData>";
                    foreach (var item in MappingList)
                    {
                        detail_XML = detail_XML + "<MappingData>";
                        detail_XML = detail_XML + "<Location>" + item.Location + "</Location>";
                        detail_XML = detail_XML + "<DocumentType>" + item.DocumentType + "</DocumentType>";
                        detail_XML = detail_XML + "<Loading_VendorType>" + item.Loading_VendorType + "</Loading_VendorType>";
                        detail_XML = detail_XML + "<UnLoading_VendorType>" + item.UnLoading_VendorType + "</UnLoading_VendorType>";
                        detail_XML = detail_XML + "<EntryBy>" + BaseUserName.ToUpper() + "</EntryBy>";
                        detail_XML = detail_XML + "</MappingData>";

                    }
                    detail_XML = detail_XML + "</ArrayOfMappingData>";
                    Status = MS.InsertLoadingUnLoadingVendorTypeMapping(detail_XML.ReplaceSpecialCharacters(), VW.Branch);
                }
            }
            catch (Exception)
            {
                Status = false;
            }

            return Status.ToString();
        }

        public ActionResult LoadingUnLoadingVendorTypeMappingDone(string status)
        {
            ViewBag.status = status.ToUpper();
            return View();
        }

        #endregion

        #region Loading / Unloading Data Changes Process

        public ActionResult LoadingUnloadingDataUpdateCriteria()
        {
            return View();
        }

        public ActionResult LoadingUnloadingDataList(LoadingUnloadingDataChange objData)
        {
            objData.dataList = MS.GetLoadingUnloadingDataList(objData);
            return View(objData);
        }

        [HttpPost]
        public ActionResult LoadingUnloadingDataListSubmit(LoadingUnloadingDataChange objData, List<CYGNUS_LoadingUnLoading_Charges> LUList)
        {
            string XMLstring = "<root><CYGNUS_LoadingUnLoading_Charges>";
            foreach (var item in LUList.Where(c => c.IsEnabled == true).ToList())
            {
                XMLstring = XMLstring + "<DocumentNo>" + item.DocumentNo + "<DocumentNo>";
                XMLstring = XMLstring + "<DocumentType>" + item.DocumentType + "<DocumentType>";
                XMLstring = XMLstring + "<ChargesType>" + item.ChargesType + "<ChargesType>";
                XMLstring = XMLstring + "<ChargedBy>" + item.ChargedBy + "<ChargedBy>";
                XMLstring = XMLstring + "<RateType>" + item.RateType + "<RateType>";
                XMLstring = XMLstring + "<ChargeRate>" + item.ChargeRate + "<ChargeRate>";
                XMLstring = XMLstring + "<ChargeAmount>" + item.ChargeAmount + "<ChargeAmount>";
                XMLstring = XMLstring + "<VendorCode>" + item.VendorCode + "<VendorCode>";
                XMLstring = XMLstring + "<VendorName>" + item.VendorName + "<VendorName>";
            }
            XMLstring = XMLstring + "</CYGNUS_LoadingUnLoading_Charges></root>";

            return RedirectToAction("LoadingUnloadingDataUpdateCriteria");
        }

        #endregion

        #region Vendor Employee Mapping

        public ActionResult vendorEmployeeMapping()
        {
            List<Cygnus_VendorEmployeeMapping> listCVEM = new List<Cygnus_VendorEmployeeMapping>();
            listCVEM = MS.GetVendorEmployeeMappingList().ToList();
            return View(listCVEM);
        }

        [HttpPost]
        public string AddEditVendorEmployeeMapping(List<Cygnus_VendorEmployeeMapping> MappingList)
        {
            bool Status = false;
            try
            {
                if (MappingList != null)
                {
                    string detail_XML = "<ArrayOfMappingData>";
                    foreach (var item in MappingList.ToList())
                    {
                        detail_XML = detail_XML + "<MappingData>";
                        detail_XML = detail_XML + "<VendorCode>" + item.VendorCode + "</VendorCode>";
                        detail_XML = detail_XML + "<UserId>" + item.UserId + "</UserId>";
                        detail_XML = detail_XML + "<IsActive>" + item.IsActive + "</IsActive>";
                        detail_XML = detail_XML + "</MappingData>";
                    }
                    detail_XML = detail_XML + "</ArrayOfMappingData>";
                    Status = MS.InsertVendorEmployeeMapping(detail_XML.ReplaceSpecialCharacters(), BaseUserName.ToUpper());
                }
            }
            catch (Exception)
            {
                Status = false;
            }

            return Status.ToString();
        }

        public ActionResult AddObjVendorEmployeeMapping(int Id)
        {
            Cygnus_VendorEmployeeMapping objCVEM = new Cygnus_VendorEmployeeMapping();
            objCVEM.Srno = Id;
            objCVEM.IsActive = true;
            return PartialView("_vendorEmployeeMapping", objCVEM);
        }

        public JsonResult GetUserListJsonWithIdName(string str)
        {
            List<WebX_Master_Users> ListUsers = new List<WebX_Master_Users>();
            ListUsers = MS.GetUserDetails().Where(c => c.Status == "100").ToList().OrderBy(c => c.Name).ToList();

            var SearchList = (from e in ListUsers
                              select new
                              {
                                  id = e.UserId,
                                  text = e.UserId + "~" + e.Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult VendorEmployeeMappingDone(string status)
        {
            ViewBag.status = status.ToUpper();
            return View();
        }

        #endregion

        #region Customer Employee Mapping

        public ActionResult CustomerEmployeeMappingMaster()
        {
            return View();
        }

        public ActionResult CustomerEmployeeMappingSubmit(CustomerEmployeeMappingViewModel VM)
        {
            var customer = "";
            string XML = "<root>";

            if (VM.EMPCust == "Employee" && VM.CustCode != null)
            {
                foreach (var item in VM.CustCode.Split(','))
                {
                    XML = XML + "<det><id>" + item + "</id></det>";
                }
                //VM.CustCode = VM.EMP_CODE;
                customer = VM.EMP_CODE;
            }
            if (VM.EMPCust == "Customer" && VM.EmpId != null)
            {
                foreach (var item in VM.EmpId.Split(','))
                {
                    XML = XML + "<det><id>" + item + "</id></det>";
                }
                customer = VM.CustCode;
            }
            XML = XML + "</root>";
            if (VM.CustCode == null)
            {
                customer = VM.EMP_CODE;
            }
            if (VM.EmpId == null)
            {
                customer = VM.CustCode;
            }

            DataTable DT = new DataTable();
            DT = MS.CustomerEmployeeMappingSubmit(customer, XML, BaseUserName, VM.EMPCust);

            return RedirectToAction("CustomerEmployeeMapping_Done");
        }

        public ActionResult CustomerEmployeeMapping_Done()
        {
            ViewBag.Title = "Customer Employee Mapping Done Successufully";
            ViewBag.PageName = "CustomerEmployeeMappingMaster";
            ViewBag.Name = "Customer Employee Mapping";
            return View();
        }

        public JsonResult GetEmployeeCodeList(string searchTerm, string Location)
        {
            searchTerm = searchTerm.ToUpper();
            Location = Location.ToUpper();

            var CMP = new List<webx_CUSTHDR>();

            CMP = MS.GetEmployeeForMapping(searchTerm, Location);

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PRS DRS Max Limit Master

        public ActionResult PRDDRSMaxLimit()
        {
            PRSDRSMaxViewModel PDMV = new PRSDRSMaxViewModel();
            CYGNUS_PDC_MaxLimit CEOGD = new CYGNUS_PDC_MaxLimit();
            try
            {
                string BRCD = BaseLocationCode;
                PDMV.ListCPDCML = new List<CYGNUS_PDC_MaxLimit>();
                return View(PDMV);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        //public ActionResult _ADDEDITPRSDRSLimit(int id, int Type, string PDCType,string BRANCH)
        //{
        //    if (Type == 1)
        //    {
        //        List<CYGNUS_PDC_MaxLimit> ListCPM = new List<CYGNUS_PDC_MaxLimit>();
        //        DataTable DTGET = MS.GePRSDRSMaxList(PDCType, BRANCH);
        //        List<CYGNUS_PDC_MaxLimit> ListCEOG = DataRowToObject.CreateListFromTable<CYGNUS_PDC_MaxLimit>(DTGET);
        //        return PartialView("_PDCMaxLimit_PartialList", ListCEOG);
        //    }
        //    else
        //    {
        //        CYGNUS_PDC_MaxLimit ObjCBTT = new CYGNUS_PDC_MaxLimit();
        //        ObjCBTT.Id = id;
        //        return PartialView("_PDCMaxLimit_Partial", ObjCBTT);
        //    }
        //}

        public ActionResult PRDDRSMaxLimitList_Submit(PRSDRSMaxViewModel PDMV, List<CYGNUS_PDC_MaxLimit> CPM)
        {
            try
            {
                //DataTable DT = MS.GePRSDRSMaxList(PDMV.PDCType);

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(PDMV.ObjCPDCML.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, PDMV.ObjCPDCML);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }
                DataTable DTADD = MS.AddPRSDRSMaxLimit(xmlDoc1.InnerXml, PDMV.PDCType, PDMV.ObjCPDCML.BRCD, BaseUserName);
                return RedirectToAction("Master_Done", new { ID = 1 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region PDC Attached Vendor master

        public ActionResult PDCattachedvendormaster()
        {
            return View();
        }

        public ActionResult GetPDCAttachedvendordetails(int id)
        {
            PDC_attached_vendor_master PDCAV = new PDC_attached_vendor_master();
            try
            {
                if (id > 0)
                {
                    PDCAV = MS.GetPDCAttachedvendorDetails().FirstOrDefault(c => c.Id == id);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditPDCattachedvendor", PDCAV);
        }

        public string AddEditPDCattachedvendor(PDC_attached_vendor_master PDCAV)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><PDC_attached_vendor_master>";
                MstDetails = MstDetails + "<Id>" + PDCAV.Id + "</Id>";
                MstDetails = MstDetails + "<VendorCode>" + PDCAV.VendorCode + "</VendorCode>";
                MstDetails = MstDetails + "<Transmode>" + PDCAV.Transmode + "</Transmode>";
                MstDetails = MstDetails + "<VehicleSize>" + PDCAV.VehicleSize + "</VehicleSize>";
                MstDetails = MstDetails + "<Origin>" + PDCAV.Origin + "</Origin>";
                MstDetails = MstDetails + "<DestCode>" + PDCAV.DestCode + "</DestCode>";
                MstDetails = MstDetails + "<Ratetype>" + PDCAV.Ratetype + "</Ratetype>";
                MstDetails = MstDetails + "<Rate>" + PDCAV.Rate + "</Rate>";
                MstDetails = MstDetails + "<IsActive>" + PDCAV.IsActive + "</IsActive>";
                MstDetails = MstDetails + "<Maxlimite>" + PDCAV.Maxlimite + "</Maxlimite></PDC_attached_vendor_master></root>";

                Status = MS.AddEditPDCattachedvendor(MstDetails, BaseUserName, BaseLocationCode);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        public JsonResult GetPDCAttachedvendorListJson()
        {
            List<PDC_attached_vendor_master> ListPDCAttachedvendor = MS.GetPDCAttachedvendorDetailsDatetable();
            var data = (from e in ListPDCAttachedvendor
                        select new
                        {
                            e.VendorCode,
                            e.Transmode,
                            e.VehicleSize,
                            e.Origin,
                            e.DestCode,
                            e.Ratetype,
                            e.Rate,
                            e.Maxlimite,
                            e.Id,
                            e.IsActive
                        }).ToArray();
            return Json(data.OrderBy(c => c.Id), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Contractual Vendor Master

        public JsonResult GetRoutemodewiseDetails(string Mode)
        {
            var CMP = MS.GetRoutemodewiseDetails(Mode);

            var users = from user in CMP

                        select new
                        {
                            //Value = user.RUTCD,
                            Value = user.RUTNM,
                            Text = user.RUTNM
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Contractualvendormaster()
        {
            return View();
        }

        public ActionResult GetContractualvendordetails(int id)
        {
            PDC_attached_vendor_master PDCAV = new PDC_attached_vendor_master();
            try
            {
                if (id > 0)
                {
                    PDCAV = MS.GetContractualdvendorDetails().FirstOrDefault(c => c.Id == id);
                }
            }
            catch (Exception) { }
            return PartialView("_AddEditContractualvendor", PDCAV);
        }

        public string AddEditContractualvendor(PDC_attached_vendor_master PDCAV)
        {
            bool Status = false;
            try
            {
                string MstDetails = "<root><PDC_attached_vendor_master>";
                MstDetails = MstDetails + "<VendorCode>" + PDCAV.VendorCode + "</VendorCode>";
                MstDetails = MstDetails + "<Id>" + PDCAV.Id + "</Id>";
                MstDetails = MstDetails + "<Transmode>" + PDCAV.Transmode + "</Transmode>";
                MstDetails = MstDetails + "<VehicleSize>" + PDCAV.VehicleSize + "</VehicleSize>";
                MstDetails = MstDetails + "<Route>" + PDCAV.Route + "</Route>";
                MstDetails = MstDetails + "<IsActive>" + PDCAV.IsActive + "</IsActive>";
                MstDetails = MstDetails + "<ConRate>" + PDCAV.ConRate + "</ConRate></PDC_attached_vendor_master></root>";
                Status = MS.AddEditContractualvendor(MstDetails, BaseUserName, BaseLocationCode);
            }
            catch (Exception) { }
            return Status.ToString();
        }

        public JsonResult GetContractualvendorListJson()
        {
            List<PDC_attached_vendor_master> LisContractualvendor = MS.GetContractualdvendorDetailsDatatables();
            var data = (from e in LisContractualvendor
                        select new
                        {
                            e.VendorCode,
                            e.Transmode,
                            e.VehicleSize,
                            e.Route,
                            e.ConRate,
                            e.IsActive,
                            e.Id
                        }).ToArray();
            return Json(data.OrderBy(c => c.Id), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region  Customer Customer Mapping

        public ActionResult IndexCustMapping()
        {
            CCMappingVW VW = new CCMappingVW();
            VW.CCTCM = new CYGNUS_CUST_To_CUST_Mapping();
            VW.ListCCTCM = new List<CYGNUS_CUST_To_CUST_Mapping>();
            return View(VW);
        }

        #endregion

        #region  Series Assign To Cust
        public ActionResult AssignToCustCriteria()
        {
            AssignToCustVW VW = new AssignToCustVW();
            return View(VW);
        }

        public ActionResult AssignToCust(AssignToCustVW VW)
        {
            VW.ObjDCRH = MS.GetSeriasList(VW.BookCode, "", "", VW.SrFrom, BaseLocationCode, BaseUserName, BaseCompanyCode).FirstOrDefault();

            if (VW.ObjDCRH == null)
            {
                ViewBag.StrError = "No Series Available For Assign To Customer.";
                return View("Error");
            }
            return View(VW);
        }

        public ActionResult AssignToCustSubmit(AssignToCustVW VM)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(VM.ObjDCRH.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, VM.ObjDCRH);
                    xmlStream.Position = 0;
                    xmlDoc.Load(xmlStream);
                }

                DataTable DT = MS.InsertCustomerSeries(xmlDoc.InnerXml, BaseUserName, BaseLocationCode, BaseCompanyCode);
                string Message = "", Status = "";
                Message = DT.Rows[0]["Message"].ToString();
                Status = DT.Rows[0]["Status"].ToString();

                if (Message.ToUpper() == "DONE" && Status == "1")
                {
                    return RedirectToAction("Master_Done", new { ID = 3 });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString();
                return View("Error");
            }
        }

        #endregion

        #region SubCustomer Assign Docket Series

        public ActionResult SubCustAssignDKTseriesCriteria()
        {
            SubCustAssignDKTseriesViewModel SCADSV = new SubCustAssignDKTseriesViewModel();
            SCADSV.ListCDCD = new List<CYGNUS_Docket_CustSeries_DET>();
            return View(SCADSV);
        }
        public ActionResult SubCustAssignDKTseries(string cust, string custname)
        {
            SubCustAssignDKTseriesViewModel SCADSV = new SubCustAssignDKTseriesViewModel();
            SCADSV.ObjCDCD = new CYGNUS_Docket_CustSeries_DET();
            SCADSV.ListCDCD = new List<CYGNUS_Docket_CustSeries_DET>();
            SCADSV.ObjCDCD.CustCode = cust;
            SCADSV.ObjCDCD.CustomerCode = custname;
            return View(SCADSV);
        }

        public ActionResult SubCustAssignDKTseriescriteraList(string SrFrom, string BookCode, int Type, string Cust)
        {
            SubCustAssignDKTseriesViewModel SCADSV = new SubCustAssignDKTseriesViewModel();
            CYGNUS_Docket_CustSeries_DET ObjCDCD = new CYGNUS_Docket_CustSeries_DET();
            if (Type == 1)
            {
                SCADSV.ListCDCD = MS.Getdktseriescriteriasubcustomer(SrFrom, BookCode);
                return PartialView("_subcustassigndktseriescriteria", SCADSV.ListCDCD);
            }
            else
            {
                SCADSV.ListCDCD = MS.Getdktseriessubcustomer(Cust);
                return PartialView("_subcustassigndktseries", SCADSV.ListCDCD);
            }


        }

        public ActionResult SubCustAssignDKTseriesSubmit(SubCustAssignDKTseriesViewModel SCADSV, List<CYGNUS_Docket_CustSeries_DET> CPM)
        {
            try
            {
                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(CPM.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, CPM);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }
                DataTable DTADD = MS.AddEditdktseriessubcustomer(xmlDoc1.InnerXml, SCADSV.ObjCDCD.CustCode, BaseLocationCode, BaseUserName);
                return RedirectToAction("Master_Done", new { ID = 4 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region Customer to Customer Mapping

        public ActionResult CustomertoCustomerMapping()
        {
            CustomertoCustomerMappingViewmodel CCMV = new CustomertoCustomerMappingViewmodel();
            CYGNUS_CUST_To_CUST_Mapping CCMVL = new CYGNUS_CUST_To_CUST_Mapping();
            try
            {
                CCMV.ListCCMVL = new List<CYGNUS_CUST_To_CUST_Mapping>();
                return View(CCMV);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult _ADDCustomer(int id, int Type, string Cust)
        {
            if (Type == 1)
            {
                List<CYGNUS_CUST_To_CUST_Mapping> ListCCMV = new List<CYGNUS_CUST_To_CUST_Mapping>();
                DataTable DTGET = MS.GeMappingCustList(Cust);
                List<CYGNUS_CUST_To_CUST_Mapping> ListCEOG = DataRowToObject.CreateListFromTable<CYGNUS_CUST_To_CUST_Mapping>(DTGET);
                return PartialView("_CustomertoCustomerMappingList", ListCEOG);
            }
            else
            {
                CYGNUS_CUST_To_CUST_Mapping ObjCBTT = new CYGNUS_CUST_To_CUST_Mapping();
                ObjCBTT.ID = id;
                return PartialView("_CustomertoCustomerMapping", ObjCBTT);
            }
        }

        public ActionResult CustomertoCustomerMapping_Submit(CustomertoCustomerMappingViewmodel CCMV, List<CYGNUS_CUST_To_CUST_Mapping> CPM)
        {
            try
            {
                //DataTable DT = MS.GePRSDRSMaxList(PDMV.PDCType);

                XmlDocument xmlDoc1 = new XmlDocument();
                XmlSerializer xmlSerializer1 = new XmlSerializer(CPM.GetType());

                using (MemoryStream xmlStream1 = new MemoryStream())
                {
                    xmlSerializer1.Serialize(xmlStream1, CPM);
                    xmlStream1.Position = 0;
                    xmlDoc1.Load(xmlStream1);
                }
                DataTable DTADD = MS.AddEditMappincust(xmlDoc1.InnerXml, CCMV.ObjCCMV.CustCode, BaseLocationCode, BaseUserName);
                return RedirectToAction("Master_Done", new { ID = 5 });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region PRQ Location change
        public ActionResult PRQLocationchange()
        {
            PRQLocationchangeViewModel CPR = new PRQLocationchangeViewModel();
            return View(CPR);
        }

        public ActionResult PRQNoLocationchange(PRQLocationchangeViewModel PLCV)
        {
            try
            {
                List<CYGNUSPickupRequest> PRQBRCD = MS.GETPRQBRCDList(PLCV.ObjCPR.PRQNo);
                CYGNUSPickupRequest OBjDKT = PRQBRCD.First();
                PLCV.ObjCPR = OBjDKT;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(PLCV);
        }

        public ActionResult PRQLocationchange_Submit(PRQLocationchangeViewModel PLCV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                Dt_Name = MS.PRQ_BRCD_Chnage_Submit(PLCV.ObjCPR.ChangeBRCD, PLCV.ObjCPR.PRQNo, BaseUserName);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return RedirectToAction("PRQLocationchangeDone", new { PRQNo = PLCV.ObjCPR.PRQNo });
        }

        public ActionResult PRQLocationchangeDone(string PRQNo)
        {
            ViewBag.PRQNo = PRQNo;
            return View();
        }

        #endregion

        #region Leave Master

        public ActionResult LeaveDetails(string Id)
        {
            CYGNUS_Leave_MasterViewModel CLMV = new CYGNUS_Leave_MasterViewModel();
            try
            {
                CLMV.Type = (string.IsNullOrEmpty(Id) ? "V" : Id);
                CLMV.listLS = new List<CYGNUS_Leave_Master>();
                CLMV.listLS = MS.GetLeaveObject(BaseUserName, CLMV.Type).ToList();
                CLMV.LeaveMst = new CYGNUS_Leave_Master();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(CLMV);
        }

        [HttpPost]
        public ActionResult LeaveDetailsSubmit(CYGNUS_Leave_MasterViewModel CLMV)
        {
            string Year = BaseFinYear.ToString();
            string Financial_Year = BaseFinYear.Split('-')[0];
            string EntryBy = BaseUserName;
            string Voucherno = "";

            string Xml_Mst_Details = "<root><LeaveDetails>";
            Xml_Mst_Details = Xml_Mst_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
            Xml_Mst_Details = Xml_Mst_Details + "<LeaveStartDate>" + Convert.ToDateTime(CLMV.LeaveMst.LeaveStartDate).ToString("dd MMM yyyy") + "</LeaveStartDate>";
            Xml_Mst_Details = Xml_Mst_Details + "<LeaveEndDate>" + Convert.ToDateTime(CLMV.LeaveMst.LeaveEndDate).ToString("dd MMM yyyy") + "</LeaveEndDate>";
            Xml_Mst_Details = Xml_Mst_Details + "<LeaveReason>" + CLMV.LeaveMst.LeaveReason + "</LeaveReason>";
            Xml_Mst_Details = Xml_Mst_Details + "<LeaveType>" + CLMV.LeaveMst.LeaveType + "</LeaveType>";
            Xml_Mst_Details = Xml_Mst_Details + "<Entryby>" + BaseUserName + "</Entryby>";
            Xml_Mst_Details = Xml_Mst_Details + "<LocationCode>" + BaseLocationCode + "</LocationCode>";
            Xml_Mst_Details = Xml_Mst_Details + "</LeaveDetails></root>";

            try
            {
                Voucherno = MS.InsertLeaveDetails(Xml_Mst_Details);

            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                ViewBag.StrError = ErrorMsg;
                return View("Error");
            }

            return RedirectToAction("LeaveDetails", new { Id = CLMV.Type });
        }

        public string LeaveApprovReject(string id, string userId, string type)
        {
            string Status = "0";
            try
            {
                Status = MS.LeaveApprovReject(id, userId, type, BaseUserName);
            }
            catch (Exception)
            {
                Status = "0";
            }

            return Status.ToString();
        }

        #endregion

        #region Designation Mapping

        public ActionResult DesignationMapping()
        {
            List<CYGNUS_Designation_Mapping> listCDM = new List<CYGNUS_Designation_Mapping>();
            listCDM = MS.GetDesignationMappingList().ToList();
            return View(listCDM);
        }

        public JsonResult GetDesignationListJsonWithIdName(string str)
        {
            List<Webx_Master_General> ListUsers = new List<Webx_Master_General>();
            ListUsers = MS.GetGeneralMasterObject().Where(c => c.StatusCode == "Y" && c.CodeType == "DESIG").OrderBy(c => Convert.ToInt32(c.CodeId)).ToList();

            var SearchList = (from e in ListUsers
                              select new
                              {
                                  id = e.CodeId,
                                  text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DesignationMappingSubmit(List<CYGNUS_Designation_Mapping> MappingList)
        {
            string status = "";

            XmlDocument xmlDoc = new XmlDocument();
            XmlSerializer xmlSerializer = new XmlSerializer(MappingList.GetType());
            using (MemoryStream xmlStream = new MemoryStream())
            {
                xmlSerializer.Serialize(xmlStream, MappingList);
                xmlStream.Position = 0;
                xmlDoc.Load(xmlStream);
            }

            try
            {
                status = MS.InsertDesignationMappingDetails(xmlDoc.InnerXml, BaseUserName);
                if (!string.IsNullOrEmpty(status) && status == "NOT DONE")
                {
                    ViewBag.StrError = "Cannot update records.";
                    return View("Error");
                }
            }
            catch (Exception e1)
            {
                ViewBag.StrError = e1.Message.Replace('\n', ' ').ToString(); ;
                return View("Error");
            }

            return RedirectToAction("DesignationMappingDone", new { id = status });
        }

        public ActionResult DesignationMappingDone(string id)
        {
            ViewBag.Status = id;
            return View();
        }

        #endregion

        #region Crossing Vendor Master
        public ActionResult CrossingVendorMasterSearch()
        {
            return View();
        }
        public ActionResult CrossingVendorMaster(string vendor, string flag)
        {
            webx_vendor_Crossing_Contract_Det WVCCD = new webx_vendor_Crossing_Contract_Det();
            WVCCD = MS.getCrossingVendorData(vendor);
            if (WVCCD.ContractID == null)
            {
                WVCCD.IsUpdate = false;
                WVCCD = new webx_vendor_Crossing_Contract_Det();
                WVCCD.VendorCode = vendor;
                webx_vendor_Crossing_Contract_Det itm = new webx_vendor_Crossing_Contract_Det();
                itm.VendorCode = vendor;
                List<webx_vendor_Crossing_Contract_Det> ls = new List<webx_vendor_Crossing_Contract_Det>();
                ls.Add(itm);
                WVCCD.ListWVCCD = ls;
            }
            else
            {
                WVCCD.IsUpdate = true;
              
                if (WVCCD.ListWVCCD.Count <= 0)
                {
                    List<webx_vendor_Crossing_Contract_Det> det = new List<webx_vendor_Crossing_Contract_Det>();
                    det.Add(new webx_vendor_Crossing_Contract_Det());
                    
                    WVCCD.ListWVCCD = det;
                }
                foreach (var item in WVCCD.ListWVCCD)
                {
                    if (item.ActiveFlag == "Y")
                    {
                        item.Check = true;
                    }
                    else
                    {
                        item.Check = false;
                    }
                }
            }
            return View("CrossingVendorMaster", WVCCD);
        }

        public ActionResult CrossingVendorMastersubmit(webx_vendor_Crossing_Contract_Det WVCCDVM, List<webx_vendor_Crossing_Contract_Det> RutTran, FormCollection form)
        {
            List<webx_vendor_Crossing_Contract_Det> WVCCDV = new List<webx_vendor_Crossing_Contract_Det>();

            if (RutTran == null)
            {
                ViewBag.StrError = "Please select atleast One Record For Submit!!!";
                return View("Error");
            }


            foreach (webx_vendor_Crossing_Contract_Det item in RutTran)
            {
                webx_vendor_Crossing_Contract_Det Obj = new webx_vendor_Crossing_Contract_Det();
                if (item.Check)
                {
                    decimal ID = item.SRNo;
                    if (ID > 0)
                    {
                        Obj = RutTran.Find(delegate(webx_vendor_Crossing_Contract_Det v) { return v.SRNo == ID; });
                        Obj.UpdatedBy = BaseUserName;
                        Obj.UpdatedDate = DateTime.Now;
                    }
                    else
                    {
                        Obj = new webx_vendor_Crossing_Contract_Det();
                        Obj.ContractID = WVCCDVM.ContractID;
                        Obj.EntryBy = BaseUserName;
                        Obj.EntryDate = DateTime.Now;
                    }
                    Obj.VendorCode = item.VendorCode;
                    Obj.Origin = item.Origin;
                    Obj.DestCity = item.DestCity;
                    Obj.rate = item.rate;
                    Obj.doordeliveryCharge = item.doordeliveryCharge;
                    Obj.ratetype = item.ratetype;
                    Obj.DDRatetype = item.DDRatetype;
                    WVCCDV.Add(Obj);
                }
            }
            if (WVCCDVM.ContractID == null)
            {
                WVCCDVM.ContractID = MS.GenerateContractId();
                WVCCDVM.VendorCode = WVCCDVM.VendorCode.Trim();
                WVCCDVM.EntryBy = BaseUserName;
                WVCCDVM.EntryDate = DateTime.Now;
            }
            else
            {
                WVCCDVM.UpdatedBy = BaseUserName;
                WVCCDVM.UpdatedDate = DateTime.Now;
            }
            WVCCDVM.ActiveFlag = "Y";

            foreach (webx_vendor_Crossing_Contract_Det dt in WVCCDV)
            {
                dt.ContractID = WVCCDVM.ContractID;
                dt.VendorCode = WVCCDVM.VendorCode;

            }
            if (WVCCDVM.IsUpdate)
            {
                try
                {
                    string QueryString = "Exec webx_vendor_Crossing_Contract_HDR_Update '" + WVCCDVM.ContractID + "','" + WVCCDVM.VendorCode + "','" + WVCCDVM.ActiveFlag +
                        "','" + WVCCDVM.UpdatedBy + "','" + WVCCDVM.UpdatedDate + "'";
                    GF.executeNonQuery(QueryString);
                    if (WVCCDV != null)
                    {
                        var isDeleted = 0;
                        var isupdated = 0;
                        var isInserted = 0;
                        string DeleteXml = "<root>";
                        string InsertXml = "<root>";
                        string UpdateXml = "<root>";
                        foreach (webx_vendor_Crossing_Contract_Det vcd in WVCCDV)
                        {
                           
                            if (vcd.Deleted)
                            {
                                    isDeleted = 1;
                                    DeleteXml = DeleteXml + "<Delete>";
                                    DeleteXml = DeleteXml + "<ContractID>" + vcd.ContractID + "<ContractID>";
                                    DeleteXml = DeleteXml + "<VendorCode>" + vcd.VendorCode + "<VendorCode>";
                                    DeleteXml = DeleteXml + "<Origin>" + vcd.Origin + "<Origin>";
                                    DeleteXml = DeleteXml + "<DestCity>" + vcd.DestCity + "<DestCity>";
                                    DeleteXml = DeleteXml + "<ratetype>" + vcd.ratetype + "<ratetype>";
                                    DeleteXml = DeleteXml + "<DDRatetype>" + vcd.DDRatetype + "<DDRatetype></Delete>";
                            }

                            else
                            {
                              
                                if (vcd.SRNo <= 0)
                                {
                                    isInserted = 1;
                                    {
                                        InsertXml = InsertXml + "<Insert>";
                                        InsertXml = InsertXml + "<Id>" + vcd.SRNo + "</Id>";
                                        InsertXml = InsertXml + "<ContractID>" + ((vcd.ContractID != "") ? vcd.ContractID : DBNull.Value.ToString()) + "</ContractID>";
                                        InsertXml = InsertXml + "<VendorCode>" + ((vcd.VendorCode != "") ? vcd.VendorCode : DBNull.Value.ToString()) + "</VendorCode>";
                                        InsertXml = InsertXml + "<Origin>" + ((vcd.Origin != "") ? vcd.Origin : DBNull.Value.ToString()) + "</Origin>";
                                        InsertXml = InsertXml + "<DestCity>" + ((vcd.DestCity != "") ? vcd.DestCity : DBNull.Value.ToString()) + "</DestCity>";
                                        InsertXml = InsertXml + "<ratetype>" + ((vcd.ratetype != "") ? vcd.ratetype : DBNull.Value.ToString()) + "</ratetype>";
                                        InsertXml = InsertXml + "<rate>" + vcd.rate + "</rate>";
                                        InsertXml = InsertXml + "<DDRatetype>" + ((vcd.DDRatetype != "") ? vcd.DDRatetype : DBNull.Value.ToString()) + "</DDRatetype>";
                                        InsertXml = InsertXml + "<doordeliveryCharge>" + vcd.doordeliveryCharge + "</doordeliveryCharge>";
                                        InsertXml = InsertXml + "<otherCharge>" + vcd.otherCharge + "</otherCharge>";
                                        InsertXml = InsertXml + "<ActiveFlag>" + ((vcd.ActiveFlag != "") ? "Y" : DBNull.Value.ToString()) + "</ActiveFlag>";
                                        InsertXml = InsertXml + "<EntryBy>" + vcd.EntryBy + "</EntryBy>";
                                        if (vcd.EntryDate < DateTime.MaxValue && vcd.EntryDate > DateTime.MinValue)
                                            InsertXml = InsertXml + "<EntryDate>" + vcd.EntryDate + "</EntryDate>";
                                        else
                                            InsertXml = InsertXml + "<EntryDate>" + DBNull.Value + "</EntryDate>";
                                        InsertXml = InsertXml + "<UpdatedBy>" + vcd.UpdatedBy + "</UpdatedBy>";
                                        if (vcd.UpdatedDate < DateTime.MaxValue && vcd.UpdatedDate > DateTime.MinValue)
                                            InsertXml = InsertXml + "<UpdatedDate>" + vcd.UpdatedDate + "</UpdatedDate>";
                                        else
                                            InsertXml = InsertXml + "<UpdatedDate>" + DBNull.Value + "</UpdatedDate></Insert>";

                                    }
                                   
                                 
                                }
                                else
                                {
                                    {
                                        isupdated = 1;
                                        UpdateXml = UpdateXml + "<Update>";
                                        UpdateXml = UpdateXml + "<Id>" + vcd.SRNo + "</Id>";
                                        UpdateXml = UpdateXml + "<ContractID>" + ((vcd.ContractID != "") ? vcd.ContractID : DBNull.Value.ToString()) + "</ContractID>";
                                        UpdateXml = UpdateXml + "<VendorCode>" + ((vcd.VendorCode != "") ? vcd.VendorCode : DBNull.Value.ToString()) + "</VendorCode>";
                                        UpdateXml = UpdateXml + "<Origin>" + ((vcd.Origin != "") ? vcd.Origin : DBNull.Value.ToString()) + "</Origin>";
                                        UpdateXml = UpdateXml + "<DestCity>" + ((vcd.DestCity != "") ? vcd.DestCity : DBNull.Value.ToString()) + "</DestCity>";
                                        UpdateXml = UpdateXml + "<ratetype>" + ((vcd.ratetype != "") ? vcd.ratetype : DBNull.Value.ToString()) + "</ratetype>";
                                        UpdateXml = UpdateXml + "<rate>" + vcd.rate + "</rate>";
                                        UpdateXml = UpdateXml + "<DDRatetype>" + ((vcd.DDRatetype != "") ? vcd.DDRatetype : DBNull.Value.ToString()) + "</DDRatetype>";
                                        UpdateXml = UpdateXml + "<doordeliveryCharge>" + vcd.doordeliveryCharge + "</doordeliveryCharge>";
                                        UpdateXml = UpdateXml + "<otherCharge>" + vcd.otherCharge + "</otherCharge>";
                                        UpdateXml = UpdateXml + "<ActiveFlag>" + ((vcd.ActiveFlag != "") ? "Y" : DBNull.Value.ToString()) + "</ActiveFlag>";
                                        UpdateXml = UpdateXml + "<EntryBy>" + vcd.EntryBy + "</EntryBy>";
                                        UpdateXml = UpdateXml + "<EntryDate>" + vcd.EntryDate + "</EntryDate>";
                                        UpdateXml = UpdateXml + "<UpdatedBy>" + vcd.UpdatedBy + "</UpdatedBy>";
                                        UpdateXml = UpdateXml + "<UpdatedDate>" + vcd.UpdatedDate + "</UpdatedDate></Update>";
                                    }
                                }
                            }

                        }
                        DeleteXml = DeleteXml + "</root>";
                        InsertXml = InsertXml + "</root>";
                        UpdateXml = UpdateXml + "</root>";
                        if(isDeleted==1)
                        {
                            MS.Delete(DeleteXml);
                        }
                        if (isupdated == 1)
                        {
                            MS.Update(UpdateXml);
                        }
                        if (isInserted == 1)
                        {
                            MS.Insert(InsertXml);
                        }
                        
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                    return View("Error");
                }
            }
            else
            {
                try
                {
                    string MasterString = "<root><Insert>";
                    MasterString = MasterString + "<ContractID>" + WVCCDVM.ContractID + "</ContractID>";
                    MasterString = MasterString + "<VendorCode>" + WVCCDVM.VendorCode + "</VendorCode>";
                    MasterString = MasterString + "<ActiveFlag>" + WVCCDVM.ActiveFlag + "</ActiveFlag>";
                    if (WVCCDVM.EntryBy != "")
                        MasterString = MasterString + "<EntryBy>" + WVCCDVM.EntryBy + "</EntryBy>";
                    else
                        MasterString = MasterString + "<EntryBy>" + DBNull.Value.ToString() + "</EntryBy>";
                    if (WVCCDVM.EntryDate < DateTime.MaxValue && WVCCDVM.EntryDate > DateTime.MinValue)
                        MasterString = MasterString + "<EntryDate>" + WVCCDVM.EntryDate + "</EntryDate>";
                    else
                        MasterString = MasterString + "<EntryDate>" + DBNull.Value.ToString() + "</EntryDate>";
                    if (WVCCDVM.UpdatedBy != "")
                        MasterString = MasterString + "<UpdatedBy>" + WVCCDVM.UpdatedBy + "</UpdatedBy>";
                    else
                        MasterString = MasterString + "<UpdatedBy>" + DBNull.Value.ToString() + "</UpdatedBy>";
                    if (WVCCDVM.UpdatedDate < DateTime.MaxValue && WVCCDVM.UpdatedDate > DateTime.MinValue)
                        MasterString = MasterString + "<UpdatedDate>" + WVCCDVM.UpdatedDate + "</UpdatedDate>";
                    else
                        MasterString = MasterString + "<UpdatedDate>" + DBNull.Value.ToString() + "</UpdatedDate>";
                    MasterString = MasterString + "</Insert></root>";
                    MS.InsertHDR(MasterString);
                    string InsertXml = "<root>";
                    foreach (webx_vendor_Crossing_Contract_Det vcd in WVCCDV)
                    {
                        InsertXml = InsertXml + "<Insert>";
                        InsertXml = InsertXml + "<ContractID>" + ((vcd.ContractID != "") ? vcd.ContractID : DBNull.Value.ToString()) + "</ContractID>";
                        InsertXml = InsertXml + "<VendorCode>" + ((vcd.VendorCode != "") ? vcd.VendorCode : DBNull.Value.ToString()) + "</VendorCode>";
                        InsertXml = InsertXml + "<Origin>" + ((vcd.Origin != "") ? vcd.Origin : DBNull.Value.ToString()) + "</Origin>";
                        InsertXml = InsertXml + "<DestCity>" + ((vcd.DestCity != "") ? vcd.DestCity : DBNull.Value.ToString()) + "</DestCity>";
                        InsertXml = InsertXml + "<ratetype>" + ((vcd.ratetype != "") ? vcd.ratetype : DBNull.Value.ToString()) + "</ratetype>";
                        InsertXml = InsertXml + "<rate>" + vcd.rate + "</rate>";
                        InsertXml = InsertXml + "<DDRatetype>" + ((vcd.DDRatetype != "") ? vcd.DDRatetype : DBNull.Value.ToString()) + "</DDRatetype>";
                        InsertXml = InsertXml + "<doordeliveryCharge>" + vcd.doordeliveryCharge + "</doordeliveryCharge>";
                        InsertXml = InsertXml + "<otherCharge>" + vcd.otherCharge + "</otherCharge>";
                        InsertXml = InsertXml + "<ActiveFlag>" + ((vcd.ActiveFlag != "") ? "Y" : DBNull.Value.ToString()) + "</ActiveFlag>";
                        InsertXml = InsertXml + "<EntryBy>" + vcd.EntryBy + "</EntryBy>";
                        InsertXml = InsertXml + "<EntryDate>" + vcd.EntryDate + "</EntryDate>";
                        InsertXml = InsertXml + "<UpdatedBy>" + vcd.UpdatedBy + "</UpdatedBy>";
                        InsertXml = InsertXml + "<UpdatedDate>" + vcd.UpdatedDate + "</UpdatedDate></Insert>";
                    }
                    InsertXml = InsertXml + "</root>";
                    MS.Insert(InsertXml);
                }


                catch (Exception ex)
                {
                    ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                    return View("Error");
                }
            }
            return RedirectToAction("CrossingVendorDone", new { ContractNo = WVCCDVM.ContractID });
        }
        public ActionResult CrossingVendorDone(string ContractNo)
        {
            ViewBag.ContractNo = ContractNo;
            return View();
        }

        public ActionResult ADDCrossingVendorInfo(string id, string VendorCode)
        {
            webx_vendor_Crossing_Contract_Det WRTCM = new webx_vendor_Crossing_Contract_Det();
            WRTCM.Id = Convert.ToInt32(id);
            WRTCM.VendorCode = VendorCode;
            List<webx_citymaster> cityList = MS.GetCityMasterObject().Where(c => c.activeflag == "Y").ToList().OrderBy(c => c.Location).ToList();
            List<webx_location> LocationsList = MS.GetLocationDetails().Where(c => c.ActiveFlag == "Y").ToList().OrderBy(c => c.LocName).ToList();

            return PartialView("_PartialCrossingVendor", WRTCM);
        }
        public JsonResult AddorEdit(string Flag, string searchTerm)
        {
            List<vw_Vendor_For_Crossing> List = (MS.GetVendor(searchTerm).Where(c => c.contract_YN == Flag).ToList());
            var data = (from e in List
                        select new
                        {
                            id = e.vendorcode,
                            text = e.vendorname

                        }).ToArray();

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Industry - Product Mapping Master
 

        public ActionResult IndustryProductMappingMaster()
        {
            Webx_Industry_Product_MappingViewModel WIPMV = new Webx_Industry_Product_MappingViewModel();
            try
            {
                WIPMV.listWIPM = new List<Webx_Industry_Product_Mapping>();
                WIPMV.WIPM = new Webx_Industry_Product_Mapping();
                WIPMV.EditFlag = "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WIPMV);
        }

        public JsonResult Get_IndustryProductCustomer(int Type)
        {
            List<Webx_Master_General> ListMappingCust = new List<Webx_Master_General>();
            if (Type == 1)
            {
                ListMappingCust = MS.GetGeneralMasterObject().Where(c => c.CodeType == "IND" && c.StatusCode == "Y").OrderBy(c => c.CodeDesc).ToList();
            }
            else
            {
                ListMappingCust = MS.GetGeneralMasterObject().Where(c => c.CodeType == "PROD" && c.StatusCode == "Y").OrderBy(c => c.CodeDesc).ToList();
            }

            var PorterList = (from e in ListMappingCust
                              select new
                              {
                                  Value = e.CodeId,
                                  Text = e.CodeDesc,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIndustryProductCustomerList(int id, string INDProductID)
        {
            Webx_Industry_Product_MappingViewModel WIPMV = new Webx_Industry_Product_MappingViewModel();
            List<Webx_Industry_Product_Mapping> lst = new List<Webx_Industry_Product_Mapping>();
            WIPMV.listWIPM = new List<Webx_Industry_Product_Mapping>();
            //WIPMV.WIPM = new Webx_Industry_Product_Mapping();
            try
            {
                
                if (id != 0)
                {
                    lst = MS.GetIndustryProductCustomer(id, INDProductID).ToList();
                    WIPMV.listWIPM = lst;
                }
                else
                {
                    WIPMV.listWIPM = new List<Webx_Industry_Product_Mapping>();
                }
            }
            catch (Exception) { }
            return PartialView("_IndustryProductMapping", WIPMV.listWIPM);
        }
        public ActionResult IndustryProductMappingDone()
        {
            return View();
        }

        public string IndustryProductMappingsubmit(Webx_Industry_Product_MappingViewModel WIPMV, List<Webx_Industry_Product_Mapping> RutTran)
        {
            Webx_Industry_Product_MappingViewModel WIPMVMList = new Webx_Industry_Product_MappingViewModel();
            WIPMVMList.listWIPM = RutTran.ToList();
            string check = "";
            try
            {
                foreach (var row in WIPMVMList.listWIPM)
                {
                    if (row.Checked == true)
                    {
                        if (check == "")
                            check = row.Value;
                        else
                            check += "," + row.Value;
                    }
                }

                if (WIPMV.WIPM.MappingType == "1")
                {
                    string SQRY = "exec usp_Map_Industry_To_Product '" + WIPMV.WIPM.CustCode + "','" + check + "','" + BaseUserName + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                }
                else
                {
                    string SQRY = "exec usp_Map_Product_To_Industry '" + WIPMV.WIPM.CustCode + "','" + check + "','" + BaseUserName + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                }

                return "true";
            }
            catch (Exception)
            {
                return "false";
            }
        }

        #endregion

        #region Role master
        public ActionResult ViewRoleMaster()
        {
            return View();
        }
        public ActionResult AddEditRole(int id)
        {
            Webx_Master_RoleViewModel lst = new Webx_Master_RoleViewModel();
            Webx_Master_Role role = new Webx_Master_Role();
            List<Webx_Master_Role> Listrole = new List<Webx_Master_Role>();
            if (id > 0)
            {
                Listrole = MS.GetRoleDetails().Where(c => c.srno == id).ToList();
                role.srno = id;
                lst.role = role;
            }
            //  lst.listrole = Listrole;
            // return View(lst);
            role.srno = id;
            lst.role = role;
            lst.listrole = Listrole;
            return View("AddEditRole", lst);
        }
        public ActionResult EditRole(int id)
        {
            Webx_Master_Role WFTP = new Webx_Master_Role();
            try
            {
                if (id > 0)
                {
                    List<Webx_Master_Role> Lstrole = MS.GetRoleDetails().Where(c => c.srno == id).ToList();

                }
            }
            catch (Exception) { }
            return PartialView("_PartialAddEditRole", WFTP);
        }
        public JsonResult GetRoleListJson()
        {
            //List<WebX_Master_Users> ListUsers = MS.GetUserDetailsForUserMasterList(BaseUserName);
            List<Webx_Master_Role> Listrole = MS.GetRoleDetails();
            var data = (from e in Listrole
                        select new
                        {
                            e.srno,
                            e.ROledesc,
                            e.HYC,
                            e.Multiple,
                            e.ACTIVEFLAG,

                        }).ToArray();
            return Json(data.OrderBy(c => c.srno), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ADDRoleInfo(int srno)
        {
            Webx_Master_Role WRTCM = new Webx_Master_Role();
            WRTCM.srno = srno;
            return PartialView("_PartialAddEditRole", WRTCM);
        }
        public ActionResult RoleMastersubmit(Webx_Master_RoleViewModel WVCCDVM, List<Webx_Master_Role> RutTran1)
        {

            Webx_Master_RoleViewModel RutCityList = new Webx_Master_RoleViewModel();
            RutCityList.listrole = RutTran1.ToList();

            XmlDocument xmlDoc = new XmlDocument();


            string MstDetails = "<root>";

            foreach (var row in RutCityList.listrole)
            {

                //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";
                MstDetails = MstDetails + "<DocumentElement>";
                MstDetails = MstDetails + "<srno>" + row.srno + "</srno>";
                MstDetails = MstDetails + "<ROledesc>" + row.ROledesc + "</ROledesc>";
                MstDetails = MstDetails + "<Roleid>" + row.Roleid + "</Roleid>";
                MstDetails = MstDetails + "<Multiple>" + (row.Multiple1 ? "Y" : "N") + "</Multiple>";
                MstDetails = MstDetails + "<ACTIVEFLAG>" + (row.ACTIVEFLAG1 ? "Y" : "N") + "</ACTIVEFLAG>";
                MstDetails = MstDetails + "</DocumentElement>";
            }

            MstDetails = MstDetails + "</root>";

            DataTable Dt = MS.AddEditRoleMaster(MstDetails, Convert.ToInt32(WVCCDVM.role.srno));

            return RedirectToAction("ViewRoleMaster");
        }

        #endregion

        #region Role - Employee Intigration Master


        public ActionResult RoleEmployeeIntigrationMaster()
        {
            Webx_Role_Employee_IntigrationViewModel WREIVM = new Webx_Role_Employee_IntigrationViewModel();
            try
            {
                WREIVM.listWREIVM = new List<Webx_Role_Employee_Intigration>();
                WREIVM.WREIM = new Webx_Role_Employee_Intigration();
                WREIVM.EditFlag = "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(WREIVM);
        }

        public JsonResult Get_RoleEmployee()
        {
            List<Webx_Role_Employee_Intigration> ListIntigrationRole = new List<Webx_Role_Employee_Intigration>();
            ListIntigrationRole = MS.RoleEmployee().ToList();

            var PorterList = (from e in ListIntigrationRole
                              select new
                              {
                                  Value = e.SRNO,
                                  Text = e.Roledesc,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Get_RoleEmployeeLocation()
        {
            List<Webx_Role_Employee_Intigration> ListIntigrationRoleLocation = new List<Webx_Role_Employee_Intigration>();
            ListIntigrationRoleLocation = MS.RoleEmployeeLocation().ToList();

            var PorterList = (from e in ListIntigrationRoleLocation
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocName,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult miscellaneous()
        //{
        //    return View();
        //}
        public ActionResult GetRoleEmployeeList(string UserId, string LOC, int SetRole)
        {
            Webx_Role_Employee_IntigrationViewModel WREIVM = new Webx_Role_Employee_IntigrationViewModel();
            List<Webx_Role_Employee_Intigration> lst = new List<Webx_Role_Employee_Intigration>();
            WREIVM.listWREIVM = new List<Webx_Role_Employee_Intigration>();
            try
            {
                if (UserId != null)
                {

                    lst = MS.GetRoleEmployeeUser(UserId, LOC, SetRole).ToList();
                    WREIVM.listWREIVM = lst;
                    if (lst.Count == 0)
                    {
                        for (int i = 1; i < lst.Count; i++)
                        {
                            Webx_Role_Employee_Intigration objHandlingCharge = new Webx_Role_Employee_Intigration();

                            objHandlingCharge.Id = i;


                            lst.Add(objHandlingCharge);
                        }
                    }
                    else
                    {
                        int ID = 0;
                        foreach (var item in lst)
                        {
                            ID++;
                            item.Id = ID;
                        }
                    }
                    WREIVM.listWREIVM = lst;


                }
                //else
                //{
                //    WREIVM.listWREIVM = new List<Webx_Role_Employee_Intigration>();
                //}
            }
            catch (Exception) { }
            return PartialView("_RoleEmployeeIntigration", WREIVM.listWREIVM);
        }

        public string RoleEmployeeIntigrationsubmit(Webx_Role_Employee_IntigrationViewModel WREIVM, List<Webx_Role_Employee_Intigration> RutTran)
        {
            Webx_Role_Employee_IntigrationViewModel WREIVMList = new Webx_Role_Employee_IntigrationViewModel();
            try
            {
                WREIVMList.listWREIVM = RutTran.ToList();
                
                string RoleID = WREIVM.WREIM.Roledesc;
                string MstDetails = "<root>";
                foreach (var row in WREIVMList.listWREIVM)
                {
                    if (row.Checked1 == true)
                    {
                        MstDetails = MstDetails + "<DocumentElement>";
                        MstDetails = MstDetails + "<UserId>" + row.UserId + "</UserId>";
                        MstDetails = MstDetails + "<RoleId>" + RoleID + "</RoleId>";
                        MstDetails = MstDetails + "<EmailId>" + row.EmailId + "</EmailId>";
                        MstDetails = MstDetails + "</DocumentElement>";
                    }
                    //MstDetails = MstDetails + "<SrNo>" + row.SrNo + "</SrNo>";                    
                }
                MstDetails = MstDetails + "</root>";
                string SQRY = "exec update_role_employee_inte_Master1 '" + MstDetails + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                //DataTable Dt = MS.AddEditVendorMaster(MstDetails);
                return "true";
            }
            catch
            {
                return "false";
            }
        }
        #endregion

        public ActionResult Master_Done(int ID, string Type1, string Type2)
        {
            if (ID == 1)
            {
                ViewBag.Title = "PDC Assign Successfully..";
                ViewBag.PageName = "PRDDRSMaxLimit";
                ViewBag.Name = "PRS/DRS Limit";
            }
            else if (ID == 2)
            {
                ViewBag.Title = "PDC Vendor Attached  Successfully..";
                ViewBag.PageName = "PDCattachedvendormaster";
                ViewBag.Name = "PDC Attached Vendor master";
            }
            else if (ID == 3)
            {
                ViewBag.Title = "Assign To Customer Done Successfully..";
                //ViewBag.PageName = "PDCattachedvendormaster";
                //ViewBag.Name = "PDC Attached Vendor master";
            }
            else if (ID == 4)
            {
                ViewBag.Title = "SubCustomer Docket wise Assign Series Done Successfully..";
                ViewBag.PageName = "SubCustAssignDKTseriesCriteria";
                ViewBag.Name = "SubCustomer Docket wise Assign Series";
            }
            else if (ID == 5)
            {
                ViewBag.Title = "Customer to Customer Mapping Done Successfully..";
                ViewBag.PageName = "CustomertoCustomerMapping";
                ViewBag.Name = "Customer to Customer Mapping";
            }
            else if (ID == 6)
            {
                ViewBag.Title = "CONTRACTUAL VENDOR MASTER Done Successfully..";
                ViewBag.PageName = "/PDCattachedvendormaster?type=2";
                ViewBag.Name = "CONTRACTUAL VENDOR";
            }
            return View();
        }


        #region miscellaneous

        public ActionResult miscellaneous()
        {
            return View();
        }
        public ActionResult Holiday_Main()
        {
            return View();
        }
        public ActionResult Holiday_Criteria()
        {
            return View();
        }


        //------------- Days wise holiday Add/Edit -----------
        public ActionResult Holiday_ByWeekDay()
        {
            webx_master_Holiday_daywiseViewModel WmHdwVM = new webx_master_Holiday_daywiseViewModel();
            webx_master_Holiday_daywise Wmn = new webx_master_Holiday_daywise();
            //List<webx_master_Holiday_daywise> ls = MS.GetHolidayWeekDayDetails();
            List<webx_master_Holiday_daywise> ls = new List<webx_master_Holiday_daywise>();
            DataTable dtHday = MS.GetHolidayWeekDayDetails();

            if (dtHday.Rows.Count > 0)
            {
                for (int i = 0; i < dtHday.Rows.Count; i++)
                {
                    if (dtHday.Rows[i]["day_name"].ToString() == "SUNDAY")
                    {
                        Wmn.SUNDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "MONDAY")
                    {
                        Wmn.MONDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "TUESDAY")
                    {
                        Wmn.TUESDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "WEDNESDAY")
                    {
                        Wmn.WEDNESDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "THURSDAY")
                    {
                        Wmn.THURSDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "FRIDAY")
                    {
                        Wmn.FRIDAY = true;
                    }
                    if (dtHday.Rows[i]["day_name"].ToString() == "SATURDAY")
                    {
                        Wmn.SATURDAY = true;
                    }
                }
            }
            WmHdwVM.listWmHdwVM = ls;
            WmHdwVM.WMHDW = Wmn;
            return View(WmHdwVM);
        }
        public ActionResult AddEditHolidayWeekDay(webx_master_Holiday_daywiseViewModel WmHdwVM, string hdn)
        {
            string var = hdn;
            return RedirectToAction("Holiday_ByWeekDay_Done", new { var = var });
        }
        public ActionResult Holiday_ByWeekDay_Done(string strqry)
        {
            ViewBag.CheckValues = strqry;
            return View();
        }
        public string Holiday_ByWeekDaysubmit(string str)
        {
            string strValue = str;
            string[] strArray = strValue.Split(',');
            try
            {
                string MstDetails = "<root>";
                foreach (var item in strArray)
                {
                    MstDetails = MstDetails + "<record>";
                    MstDetails = MstDetails + "<day_name>" + item + "</day_name>";
                    MstDetails = MstDetails + "<hday_loc>" + "ALL" + "</hday_loc>";
                    MstDetails = MstDetails + "<updateby>" + BaseUserName + "</updateby>";
                    MstDetails = MstDetails + "<updatedate>" + System.DateTime.Now + "</updatedate>";
                    MstDetails = MstDetails + "</record>";
                }
                MstDetails = MstDetails + "</root>";

                string SQRY = "exec USP_INSERT_HOLIDAY_DAYWISE '" + MstDetails + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                return str;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return "Not ok";
            }
        }
        //----------------------------------------------------


        //----------- Date wise holiday Add/Edit -------------
        public ActionResult Holiday_ByDate(string hday_date, string FlagType)
        {
            WebX_Master_Holiday_DatewiseViewModel WMHDWVM = new WebX_Master_Holiday_DatewiseViewModel();
            WebX_Master_Holiday_Datewise WMHDW = new WebX_Master_Holiday_Datewise();
            if (hday_date != null)
            {
                ViewBag.Data = hday_date;
                WMHDWVM.WMHDW = new WebX_Master_Holiday_Datewise();
            }
            else
            {
                List<WebX_Master_Holiday_Datewise> ls = MS.GetDateWiseLocationDetails();
                WMHDWVM.listWMHDWVM = ls;
                WMHDWVM.WMHDW = new WebX_Master_Holiday_Datewise();
            }
            return View(WMHDWVM);
        }
        [HttpPost]
        public ActionResult AddEditDatWiseLocation(WebX_Master_Holiday_DatewiseViewModel WMHDWVM)
        {
            try
            {
                string Xml_ReqMST = "<root><Holiday>", Xml_ReqDET = "<root>", Status = "Fail", HDate = "";
                Xml_ReqMST = Xml_ReqMST + "<hday_date>" + WMHDWVM.WMHDW.hday_date + "</hday_date>";
                Xml_ReqMST = Xml_ReqMST + "<hday_note>" + WMHDWVM.WMHDW.hday_note + "</hday_note>";
                Xml_ReqMST = Xml_ReqMST + "<hday_loc>" + WMHDWVM.WMHDW.RadiohdayType + "</hday_loc>";
                Xml_ReqMST = Xml_ReqMST + "<activeflag>" + WMHDWVM.WMHDW.activeflag + "</activeflag>";
                Xml_ReqMST = Xml_ReqMST + "</Holiday></root>";
                Xml_ReqDET = Xml_ReqDET + "</root>";
                string var = WMHDWVM.WMHDW.hday_date.ToString();

                string Sql = "EXEC USP_INSERT_HOLIDAY_DATEWISE_VER1 '" + Xml_ReqMST.Replace("&", "&amp;").Replace("'", "").Trim() + "'";
                DataTable DT = GF.getdatetablefromQuery(Sql);

                if (DT.Rows.Count > 0 && DT != null)
                {
                    Status = DT.Rows[0][0].ToString();
                    HDate = DT.Rows[0][1].ToString();
                }
                return RedirectToAction("Holiday_ByDate_Done", new { var = var });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error" + ex.Message;
                return View("Error");
            }
        }
        public ActionResult Holiday_ByDate_Done(string var)
        {
            ViewBag.abc = var;
            return View("Holiday_ByDate_Done");
        }
        public JsonResult Get_HolidayDateWiseLocation()
        {
            List<webx_location> ListDateWiseLocation = new List<webx_location>();
            ListDateWiseLocation = MS.GetLocationDetails().ToList();

            var PorterList = (from e in ListDateWiseLocation
                              select new
                              {
                                  Value = e.LocName_LocCode,
                                  Text = e.LocName,
                              }).ToArray();

            return Json(PorterList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Holiday_Edit()
        {

            WebX_Master_Holiday_DatewiseViewModel WMHDWVM = new WebX_Master_Holiday_DatewiseViewModel();
            WMHDWVM.WMHDW = new WebX_Master_Holiday_Datewise();
            List<WebX_Master_Holiday_Datewise> ls = new List<WebX_Master_Holiday_Datewise>();

            WMHDWVM.WMHDW.hday_date = System.DateTime.Now;
            WMHDWVM.listWMHDWVM = ls;
            WMHDWVM.EditFlag = "true";

            return View(WMHDWVM);

        }
        //----------------------------------------------------


        //-------------- Date wise holiday list --------------
        public ActionResult HolidayViewList()
        {
            WebX_Master_Holiday_DatewiseViewModel WMHDWVM = new WebX_Master_Holiday_DatewiseViewModel();
            WebX_Master_Holiday_Datewise WMHDW = new WebX_Master_Holiday_Datewise();
            List<WebX_Master_Holiday_Datewise> ls = MS.GetDateWiseLocationDetails();
            WMHDWVM.listWMHDWVM = ls;
            WMHDWVM.WMHDW = new WebX_Master_Holiday_Datewise();
            // WmHdwVM.WMHDW = Wmn;          
            return View(WMHDWVM);
        }
        public JsonResult GetViewList()
        {
            List<WebX_Master_Holiday_Datewise> listholidaydatewise = MS.GetHolidayDateWise();
            var data = (from e in listholidaydatewise
                        select new
                        {
                            e.hday_date,
                            e.hday_note,
                            e.hday_loc,
                            e.activeflag,
                            e.updateby,
                            e.updatedate
                        }).ToArray();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        //----------------------------------------------------


        //---------------- Employee News ---------------------
        public ActionResult EnterEmpNews()
        {
            webx_News_UpdatesViewModel WNUVM = new webx_News_UpdatesViewModel();
            WNUVM.listupdates = new List<webx_News_Updates>();
            webx_News_Updates ts = new webx_News_Updates();
            ts.srno = 1;
            WNUVM.listupdates.Add(ts);
            return View(WNUVM);
        }
        public ActionResult Addrow(int srno)
        {
            webx_News_Updates updates = new webx_News_Updates();
            updates.srno = srno;
            return PartialView("_EnterNews", updates);
        }
        [HttpPost]
        public ActionResult AddEmpNews(List<webx_News_Updates> RutTran, webx_News_UpdatesViewModel WNUVM)
        {
            WNUVM.listupdates = MS.DeleteNews().ToList();
            WNUVM.listupdates = RutTran.ToList();
            try
            {
                string Updates = "";
                string ID = "N";
                string MstDetails = "<root>";
                foreach (var row in WNUVM.listupdates)
                {
                    MstDetails = MstDetails + "<Enternews>";
                    MstDetails = MstDetails + "<Headline>" + row.Headline + "</Headline>";
                    MstDetails = MstDetails + "<News>" + row.News + "</News>";
                    MstDetails = MstDetails + "<Updates>" + Updates + "</Updates>";
                    MstDetails = MstDetails + "<ID>" + ID + "</ID>";
                    MstDetails = MstDetails + "<Color>" + row.Color + "</Color>";
                    MstDetails = MstDetails + "</Enternews>";
                }
                MstDetails = MstDetails + "</root>";
                DataTable Dt = MS.AddEmpNewsDetails(MstDetails.Replace("'", "''"));
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("Index", "Home");
        }
        //----------------------------------------------------


        //---------------- Customer News ---------------------
        public ActionResult EnterCustNews()
        {
            webx_Customer_UpdatesViewModel WCNUVM = new webx_Customer_UpdatesViewModel();
            WCNUVM.listCustupdates = new List<webx_Customer_News_updates>();
            webx_Customer_News_updates ts = new webx_Customer_News_updates();
            ts.srno = 1;
            WCNUVM.listCustupdates.Add(ts);
            return View(WCNUVM);
        }
        public ActionResult AddCustrow(int srno)
        {
            webx_Customer_News_updates updates = new webx_Customer_News_updates();
            updates.srno = srno;
            return PartialView("_EnterCustNews", updates);
        }
        [HttpPost]
        public ActionResult AddCustNews(List<webx_Customer_News_updates> RutTran, webx_Customer_UpdatesViewModel WCNUVM)
        {
            WCNUVM.listCustupdates = MS.DeleteCustNews().ToList();
            WCNUVM.listCustupdates = RutTran.ToList();
            try
            {
                string Updates = "";
                string ID = "N";
                string MstDetails = "<root>";
                foreach (var row in WCNUVM.listCustupdates)
                {
                    MstDetails = MstDetails + "<Entercustnews>";
                    MstDetails = MstDetails + "<Headline>" + row.Headline + "</Headline>";
                    MstDetails = MstDetails + "<News>" + row.News + "</News>";
                    MstDetails = MstDetails + "<Updates>" + Updates + "</Updates>";
                    MstDetails = MstDetails + "<ID>" + ID + "</ID>";
                    MstDetails = MstDetails + "<Color>" + row.Color + "</Color>";
                    MstDetails = MstDetails + "</Entercustnews>";
                }
                MstDetails = MstDetails + "</root>";
                DataTable Dt = MS.AddCustNewsDetails(MstDetails.Replace("'", "''"));
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("Index", "Home");
        }
        //----------------------------------------------------


        //-------------- Employee Updates  -------------------
        public ActionResult EnterEmpUpdates()
        {
            webx_News_UpdatesViewModel WNUVM = new webx_News_UpdatesViewModel();
            webx_News_Updates updates = new webx_News_Updates();
            return View(WNUVM);
        }
        public ActionResult AddEmpUpdates(string Updates, string Updatecolor)
        {
            string Headline = "";
            string News = "";
            string ID = "U";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                //XmlSerializer xmlSerializer = new XmlSerializer(updates.GetType());
                string MstDetails = "<root><updtesnews>";
                MstDetails = MstDetails + "<Updates>" + Headline + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + News + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + Updates + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + ID + "</Updates>";
                MstDetails = MstDetails + "<Color>" + Updatecolor + "</Color>";
                MstDetails = MstDetails + "</updtesnews></root>";
                DataTable Dt = MS.AddEmpUpdatesDetails(Headline, News, Updates, ID, Updatecolor);
                //return "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("Index", "Home");
        }
        //----------------------------------------------------


        //-------------- Customer Updates  -------------------
        public ActionResult EnterCustUpdates()
        {
            webx_Customer_UpdatesViewModel WNUVM = new webx_Customer_UpdatesViewModel();
            webx_Customer_News_updates Custupdates = new webx_Customer_News_updates();
            return View();
        }
        public ActionResult AddCustomerUpdates(string Updates, string Updatecolor)
        {
            string Headline = "";
            string News = "";
            string ID = "U";
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                // XmlSerializer xmlSerializer = new XmlSerializer(Updates.GetType());
                string MstDetails = "<root><updtesnews>";
                MstDetails = MstDetails + "<Updates>" + Headline + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + News + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + Updates + "</Updates>";
                MstDetails = MstDetails + "<Updates>" + ID + "</Updates>";
                MstDetails = MstDetails + "<Color>" + Updatecolor + "</Color>";
                MstDetails = MstDetails + "</updtesnews></root>";
                DataTable Dt = MS.AddCustomerUpdatesDetails(Headline, News, Updates, ID, Updatecolor);
                // return "true";
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("Index", "Home");
        }
        //----------------------------------------------------
        #endregion

    }
}