﻿using CipherLib;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using System.IO;
using System.Configuration;

namespace Fleet.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        GeneralFuncations GF = new GeneralFuncations();
        OperationService OS = new OperationService();
        MasterService MS = new MasterService();
        AndroidServices AS = new AndroidServices();

        public ActionResult URLRedirect()
        {
            //ViewBag.LocationCode = BaseLocationCode;
            return View();
        }
        public ActionResult AdminDashBoard()
        {
            //ViewBag.LocationCode = BaseLocationCode;
            return View();
        }

        [AllowAnonymous]
        public string DashboardData(string ProcName)
        {
            string Message = "Not Done";
            try
            {
                string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/Images/AdminDashboard/");
                string curFile = folderPath + ProcName + ".txt";
                DataTable Dt = AS.GetDashboardData(ProcName);
                System.IO.File.WriteAllText(curFile, JsonConvert.SerializeObject(Dt));
                Message = "Done";
            }
            catch (Exception ex)
            {
                Message = ex.Message.ToString();
            }
            return Message;// Json(Message, JsonRequestBehavior.AllowGet);
        }

        [InitializeSimpleMembership]
        public ActionResult Index()
        {
            DataSet Dt = MS.GetEmpUpdates();
            DashboardModal Modal = new DashboardModal();
            
            
            if (Dt != null)
            {


                if (Dt.Tables[0].Rows.Count > 0)
                {
                    Modal.JustInData = new DashboardDataModal();
                    Modal.JustInData.Updates = Dt.Tables[0].Rows[0]["updates"].ToString();
                    Modal.JustInData.Color = Dt.Tables[0].Rows[0]["color"].ToString();
                }
                Modal.NewsDataList = new List<DashboardDataModal>();
                if (Dt.Tables[1].Rows.Count > 0)
                {
                    for (int i = 0; i < Dt.Tables[1].Rows.Count; i++)
                    {
                        DashboardDataModal Item = new DashboardDataModal();
                        Item.Color = Dt.Tables[1].Rows[i]["Color"].ToString();
                        Item.Heading = Dt.Tables[1].Rows[i]["Headline"].ToString();
                        Item.Updates = Dt.Tables[1].Rows[i]["News"].ToString();
                        Modal.NewsDataList.Add(Item);
                    }
                }
            }


            if (BaseEmpType == "C")
            {
                return RedirectToAction("Index", "CustomerPortal");
            }
            else
            {
                ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
                string psSult = "WebX";
                string PassWord = "geGXjfSGAVU=";
                //string UserId = "00640";

                string Pass = GF.Decrypt(PassWord, psSult);
                //WebSecurity.CreateUserAndAccount(UserId, "123456");
                //  string ConnStr = "Data Source=104.215.254.129;Initial Catalog=RCPL_Live;Persist Security Info=True;User ID=RCPLAdmin;Password=raPhETatav8s;";

                //  //string ConnStr = "Data Source=223.30.57.158;Initial Catalog=APMDRS;UID=sa;pwd=Erp@123456;Connect Timeout=200;pooling='true'; Max Pool Size=200";

                //  string sql = "select Count(*) as Cnt from WebX_Master_Users";
                //  // string Count = SqlHelper.ExecuteScalar(ConnStr, CommandType.Text, sql).ToString();


                //  SqlConnection conn = new SqlConnection(ConnStr);
                //  SqlCommand cmd = new SqlCommand(sql, conn);
                //  conn.Open();
                //  SqlDataReader reader = cmd.ExecuteReader();
                //  //string Count = "";
                //  //if (reader.HasRows)
                //  //{
                //  //    reader.Read();
                //  //    Count = reader["Cnt"].ToString();
                //  //}


                //  reader.Close();
                //  //int Count_Int = Convert.ToInt16(Count);

                //  //if (Count_Int == 0)
                //  //{


                //  //}
                //  //else
                //  //{

                //  string sql_Name = "select UserId,userpwd from WebX_Master_Users WHERE UserId not in ('10001')";

                //      SqlCommand cmd_Pass = new SqlCommand(sql_Name, conn);

                //      //SqlDataReader reader_Pass = cmd_Pass.ExecuteReader();

                //      DataTable dt = new DataTable();
                //      SqlDataAdapter adapter = new SqlDataAdapter(cmd_Pass);
                //      adapter.Fill(dt);
                //      for (int i = 0; i < dt.Rows.Count; i++)
                //      {
                //          string PassWord = "", UserId = "";
                //          PassWord = dt.Rows[i]["userpwd"].ToString();
                //          UserId = dt.Rows[i]["UserId"].ToString();


                //          string Pass = GF.Decrypt(PassWord, psSult);

                //WebSecurity.CreateUserAndAccount(UserId, Pass);

                //  //        //string sql_Name1 = "UPDATE WebX_Master_Users SET UserPwd = '" + Encrypt(Pass, psSult) + "' WHERE UserId='" + UserId + "'";
                //  //        //DataTable dt1 = GF.GetDataTableFromSP(sql_Name1);
                //     }

                //}
                return View(Modal);
            }
        }

        FleetDataService.FleetService FS = new FleetDataService.FleetService();

        [HttpPost]
        public ActionResult TyreRemoval(TyreRemovalViewModel TRVM)
        {
            DataTable Dt = FS.GetTyreRemovalDetail(TRVM.TrucNo);
            TRVM.Tyre_Pr_Alex = Convert.ToDecimal(Dt.Rows[0][1]);
            TRVM.listVWTDNM = FS.GetTyreRemovalListForHome(TRVM.TrucNo);
            TRVM.Actual_Tyre = TRVM.listVWTDNM.Count();
            return View(TRVM);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ErrorTransaction(string Message)
        {
            ViewBag.Message = Message;
            return View();
        }

        public ActionResult VehhicleGPSStatus()
        {
            return View();
        }

        [AllowAnonymous]
        public string GetData(string Method, IDictionary<string, string> parameters)
        {
            DataSet DS = GF.getdatasetFromParams(Method, parameters);
            if (DS.Tables.Count == 1)
            {
                return JsonConvert.SerializeObject(DS.Tables[0]);
            }
            else
            {
                return JsonConvert.SerializeObject(DS);
            }
        }

        public JsonResult GetPasswordFromUserID(string Id)
        {
            string password = "";
            try
            {
                FleetDataService.MasterService MS = new FleetDataService.MasterService();
                WebX_Master_Users objUser = new WebX_Master_Users();
                objUser = MS.GetUserDetails().Where(c => c.UserId.ToUpper() == Id.ToUpper()).FirstOrDefault();
                password = GF.Decrypt(objUser.UserPwd, "WebX");
            }
            catch (Exception ex)
            {
                password = ex.Message.ToString();
            }
            return Json(password, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public string SendEmail(string From, string Title, string To, string Subject, string Body, string Host, string username, string password, string Port = "587", bool IsBodyHtml = true, bool EnableSsl = true)
        {
            string response = "";
            try
            {
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(From, Title);
                var toEmilList = To.Split(';').ToList();
                int i = 0;
                foreach (var item in toEmilList)
                {
                    if (i == 0)
                    {
                        mm.To.Add(item);
                        i = 1;
                    }
                    else
                    {
                        mm.CC.Add(item);
                    }
                }

                mm.Subject = HttpUtility.UrlDecode(Subject);
                mm.Body = HttpUtility.UrlDecode(Body);
                mm.IsBodyHtml = IsBodyHtml;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = Host;
                smtp.EnableSsl = EnableSsl;
                NetworkCredential NetworkCred = new NetworkCredential(username, password);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Convert.ToInt32(Port);
                smtp.Send(mm);
                response = "1";
            }
            catch (Exception ex)
            {
                response = ex.Message.ToString();
            }
            return JsonConvert.SerializeObject(response);
        }


        public ActionResult BillSubmitViaLink(string BillNo)
        {
            try
            {
                BillingFilterViewModel model = new BillingFilterViewModel();

                model.BillNo = BillNo;
                model.BillingParty = "";
                model.Paybasis = "All";
                model.BillType = "All";

                BillSubmissionViewModel VM = new BillSubmissionViewModel();
                VM.BillList = OS.GetBillForSubmission(model, BaseLocationCode, BaseCompanyCode, BaseUserName.ToUpper(), ViewBag.FinYearStart);
                var CustomerList = MS.GetCustomerMasterObject().Where(c => c.CUSTCD.ToUpper() == model.BillingParty.ToUpper()).ToList();
                VM.Party = VM.BillList.First().ptmsstr;
                List<Webx_Master_General> ListPayBasis = MS.GetGeneralMasterObject().Where(c => c.CodeType == "BILLTYP").ToList();
                VM.BillType = model.BillType;
                return RedirectToAction("BillSubmissionProcess", "Finance", VM);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = @ex.Message.ToString().Replace('\n', ' ') + ". OR Billno is not Valid.";
                return View("Error");
            }
        }


        [AllowAnonymous]

        public ActionResult GC_XLSGenerationCustomerDocektTracking(string Dockno, string Docksf)
        {
            ViewBag.Dockno = Dockno;
            ViewBag.Docksf = Docksf;
            return View();
        }

        #region GSTN Verification API
        [AllowAnonymous]
        public JsonResult GetGSTDetails(string GSTN)
        {
            string result = "";
            try
            {
                string MoodysWebstring = @"https://commonapi.mastersindia.co/commonapis/searchgstin?gstin=" + GSTN.Trim();
                Uri MoodysWebAddress = new Uri(MoodysWebstring);
                HttpWebRequest request = WebRequest.Create(MoodysWebAddress) as HttpWebRequest;
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Headers.Add("Authorization", "Bearer " + GSTToken);
                request.Headers.Add("client_id", "REbdjQQvBrKHjnzwXc");
                result = string.Empty;
                HttpWebResponse response;
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    result = reader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                using (var streamReader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
            }
            var jsn = JsonConvert.DeserializeObject(result);
            string strJson = jsn.ToString();
            var JsonResp = JObject.Parse(strJson);
            var TokenProperty = JsonResp.Property("data");
            string a = TokenProperty.Value.ToString();
            return new JsonResult()
            {
                Data = new
                {
                    A = a
                }
            };
        }
        #endregion
    }
}
