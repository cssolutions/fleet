﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Microsoft.Reporting.WebForms;
using System.Data.SqlClient;
using FleetDataService.ViewModels;
using Microsoft.ApplicationBlocks.Data;
using CYGNUS.Classes;
using System.Net.Mail;
using System.Configuration;
using System.Net;
using System.Drawing;
using GenCode128;
using QRCoder;
using System.Drawing.Imaging;
using Fleet.Classes;

namespace Fleet.Controllers
{
      [AllowAnonymous]

    public class QrCodeController : BaseController
    {
        //
        // GET: /QrCode/

          GeneralFuncations GF = new GeneralFuncations();
          public ActionResult CNote_Track(string qrcode)
          {

              string QueryString = "SELECT DOCKNO FROM webx_master_docket(NOLOCK) WHERE QrCodeNo='" + qrcode + "'";
              DataTable DT = GF.GetDataTableFromSP(QueryString);
              string Dockno = "";
              if (DT.Rows.Count > 0)
              {
                  Dockno = DT.Rows[0]["DOCKNO"].ToString();
              }


              ViewBag.Dockno = Dockno;
            return View();
        }


       
        public ActionResult GetBarcodeImage(string code)
        {
            Image myimg = Code128Rendering.MakeBarcodeImage(code, int.Parse("2"), true);
            ImageConverter converter = new ImageConverter();
            byte[] imgArray = (byte[])converter.ConvertTo(myimg, typeof(byte[]));
            return new FileStreamResult(new System.IO.MemoryStream(imgArray), "image/jpeg");
        }
        public byte[] imageToByteArray(System.Drawing.Image image)
        {
            using (var ms = new MemoryStream())
            {
                image.Save(ms, image.RawFormat);
                return ms.ToArray();
            }
        }


        public ActionResult GetQRCodeImage(string qrcode)
        {

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeGenerator.QRCode qrCode = qrGenerator.CreateQrCode(qrcode, QRCodeGenerator.ECCLevel.Q);
            Image myimg = qrCode.GetGraphic(20);
            ImageConverter converter = new ImageConverter();
            byte[] img = (byte[])converter.ConvertTo(myimg, typeof(byte[]));
            return new FileStreamResult(new System.IO.MemoryStream(img), "image/jpeg");
        }


    }
}
