﻿using CipherLib;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Fleet.Models;
using CYGNUS.Classes;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class TrackingController : BaseController
    {
        FleetDataService.ExceptionsService ES = new FleetDataService.ExceptionsService();
        FleetDataService.TrackingService TS = new FleetDataService.TrackingService();
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        GeneralFuncations GF = new GeneralFuncations();
        string ControllerName = "TrackingController";

        #region Operation Track

        private static DateTime FromDate; private static DateTime ToDate; private static string From_loc; private static string MaxLocVal; private static string To_loc;

        private static string MinLocVal; private static string DOCKNO; private static string ManualNo; private static string VehicleNo; private static string FreeText;

        private static string DockType;

        public ActionResult OperationTrackCriteria()
        {
            TrackViewModel VM = new TrackViewModel();

            VM.ListLocation = MS.GetWorkingLocations(BaseLocationCode, MainLocCode);

            return View(VM);
        }

        public JsonResult GetSubBranch(string BRCode)
        {
            List<Webx_Master_General> DSFieldOfficerList = TS.GetSubBranchFromBranch(BRCode);
            var users = (from user in DSFieldOfficerList
                         select new
                         {
                             Value = user.CodeId,
                             Text = user.CodeDesc
                         }).Distinct().ToList();
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult OperationTrackList(TrackViewModel VM, string Location, string LocationLevel, string LocationTo, string LocationLevelTo)
        {
            FromDate = VM.opTrackModel.FromDate;
            ToDate = VM.opTrackModel.ToDate;
            From_loc = Location;
            MaxLocVal = LocationLevel;
            To_loc = LocationTo;
            MinLocVal = LocationLevelTo;
            DOCKNO = VM.opTrackModel.DOCKNO;
            ManualNo = VM.opTrackModel.ManualNo;
            VehicleNo = VM.opTrackModel.VehicleNo;
            FreeText = VM.opTrackModel.FreeText;
            DockType = VM.opTrackModel.DockType;
            ViewBag.DockType = DockType;
            List<vw_OperationTrack> optrkList = TS.Get_ListOfOperationTracking(GF.FormateDate(FromDate), GF.FormateDate(ToDate), From_loc, MaxLocVal, To_loc, MinLocVal, DOCKNO, ManualNo, VehicleNo, FreeText, DockType, 0, 5);
            VM.ListOPTrack = optrkList;
            return View(optrkList.First());
        }

        public ActionResult AjaxHandler(int sEcho, int iDisplayLength, int iDisplayStart, int iColumns, int iSortingCols, string sColumns, int TotaleRecords)
        {
            int page = 0;
            if (iDisplayStart > 0)
                page = iDisplayStart / iDisplayLength;
            string minLV = "", maxLV = "";
            string Str_VAL = "", STR_LV = "";
            string MAxStr_VAL = "", MAxSTR_LV = "";

            string strLocLevel = "select loc_level from webx_location where loccode='" + BaseLocationCode + "'";
            DataTable Dt = GF.getdatetablefromQuery(strLocLevel);
            int loclevel = Convert.ToInt32(Dt.Rows[0]["loc_level"].ToString());
            string strMaxMin = "select max(codeid),min(codeid) from webx_master_general where codetype='HRCHY' and codeid<>'1'  and  codeid>=" + loclevel + "";
            DataTable Dt1 = GF.getdatetablefromQuery(strMaxMin);
            maxLV = Dt1.Rows[0][0].ToString();
            minLV = Dt1.Rows[0][1].ToString();

            for (int i = Convert.ToInt32(minLV); i <= Convert.ToInt32(maxLV); i++)
            {
                if (From_loc != "All")
                {
                    if (Str_VAL == "")
                        Str_VAL = From_loc;
                    else
                        Str_VAL = Str_VAL + "," + From_loc;

                    MAxStr_VAL = From_loc;

                    if (STR_LV == "")
                        STR_LV = i.ToString();
                    else
                        STR_LV = STR_LV + "," + i.ToString();
                    MAxSTR_LV = i.ToString();
                }
            }

            string Str_VAL_to = "", STR_LV_to = "";
            string MAxStr_VAL_to = "", MAxSTR_LV_to = "";

            for (int i_to = Convert.ToInt32(minLV); i_to <= Convert.ToInt32(maxLV); i_to++)
            {
                if (To_loc != "All")
                {
                    if (Str_VAL_to == "")
                        Str_VAL_to = To_loc;
                    else
                        Str_VAL_to = Str_VAL_to + "," + To_loc;

                    MAxStr_VAL_to = To_loc;

                    if (STR_LV_to == "")
                        STR_LV_to = i_to.ToString();
                    else
                        STR_LV_to = STR_LV_to + "," + i_to.ToString();
                    MAxSTR_LV_to = i_to.ToString();
                }
            }

            if (MAxStr_VAL == "")
                MAxStr_VAL = "All";
            if (MAxStr_VAL_to == "")
                MAxStr_VAL_to = "All";
            if (MAxSTR_LV == "")
                MAxSTR_LV = "1";
            if (MAxSTR_LV_to == "")
                MAxSTR_LV_to = "1";

            List<vw_OperationTrack> optrkList = TS.Get_ListOfOperationTracking(GF.FormateDate(FromDate), GF.FormateDate(ToDate), From_loc, MAxSTR_LV, To_loc, MAxSTR_LV_to, DOCKNO, ManualNo, VehicleNo, FreeText, DockType, page + 1, iDisplayLength);
            //List<vw_OperationTrack> optrkList = TS.Get_ListOfOperationTracking(GF.FormateDate(Convert.ToDateTime(fromdt)), GF.FormateDate(Convert.ToDateTime(todt)), location, locLevel, location_to, locLevel_to, docklist, manuallist, VEH_CUSTREF, FREETEXT, documenttype, page + 1, iDisplayLength);

            var totalRecords = optrkList.Count();

            // var result = optrkList.Skip(iDisplayStart);//.Take(iDisplayLength); 
            if (DockType == "DKT")  // DKT
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.doc_dt),
                               Convert.ToString(c.Deldt),
                               Convert.ToString(c.Origin_dest),
                               Convert.ToString(c.Curr_Next),
                               Convert.ToString(c.From_to),
                               Convert.ToString(c.Type),
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.AWBNO),
                               Convert.ToString(c.Cnor),
                               Convert.ToString(c.Cnee),
                               Convert.ToString(c.status),
                               Convert.ToString(c.Vehno),

                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            if (DockType == "THC")  // THC
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.doc_dt),
                               Convert.ToString(c.ManualDocNo),
                               Convert.ToString(c.Origin_dest),
                               Convert.ToString(c.doc_veh),
                               Convert.ToString(c.Curr_Next),
                               Convert.ToString(c.Deldt),
                               Convert.ToString(c.status),
                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            if (DockType == "LS")  // LS
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.ManualDocNo),
                               Convert.ToString(c.doc_dt),
                               Convert.ToString(c.Origin),
                               Convert.ToString(c.Dest),
                               Convert.ToString(c.totdockets),
                               Convert.ToString(c.cancelled),
                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            if (DockType == "PRS" || DockType == "DRS")  // PRS and DRS
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.ManualDocNo),
                               Convert.ToString(c.Origin),
                               Convert.ToString(c.doc_dt),
                               Convert.ToString(c.Vehno),
                               Convert.ToString(c.totdockets),
                               Convert.ToString(c.cancelled),
                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            if (DockType == "PFM")  // PFM
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.ManualDocNo),
                               Convert.ToString(c.doc_dt),
                               Convert.ToString(c.Origin),
                               Convert.ToString(c.Dest),
                               Convert.ToString(c.totdockets),
                               Convert.ToString(c.ACK),
                               Convert.ToString(c.ACKDT),
                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            else  //MF
            {
                var data = from c in optrkList
                           select new[]
                          {
                               Convert.ToString(c.Docno),
                               Convert.ToString(c.ROWNO),
                               Convert.ToString(c.doc_no),
                               Convert.ToString(c.ManualDocNo),
                               Convert.ToString(c.Thcdt),
                               Convert.ToString(c.Origin),
                               Convert.ToString(c.Dest),
                               Convert.ToString(c.thcno),
                               Convert.ToString(c.totdockets),
                               Convert.ToString(c.cancelled),
                          };
                return Json(new
                {
                    sEcho = Convert.ToString(sEcho),
                    iTotalRecords = totalRecords,
                    iTotalDisplayRecords = TotaleRecords,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }

        }

        public ActionResult DKTTrackView(string dockno, string docksf, string ID)
        {
            vw_OperationTrack EF = new vw_OperationTrack();
            EF.DOCKNO = dockno;
            EF.doc_no = docksf;
            EF.DockType = ID;
            return View(EF);
        }

        public ActionResult TripAllView(string LsNo, string MFNo, string THCNo, string PRSNo, string DRSNo, string strPFMNo, string VPType)
        {
            vw_OperationTrack EF = new vw_OperationTrack();
            if (VPType == "LS")
            {
                EF.DOCKNO = LsNo;
            }
            if (VPType == "MF")
            {
                EF.DOCKNO = MFNo;
            }
            if (VPType == "THC")
            {
                EF.DOCKNO = THCNo + ".";
            }
            if (VPType == "PRS")
            {
                EF.DOCKNO = PRSNo;
            }
            if (VPType == "DRS")
            {
                string strqry = "select PDCUpdated=ISNULL(PDCUpdated,'N') from webx_pdchdr where PDCNO ='" + DRSNo.ToString() + "'";
                DataTable Dt = GF.getdatetablefromQuery(strqry);
                string PDCUpdated = Dt.Rows[0]["PDCUpdated"].ToString();
                EF.DOCKNO = DRSNo;
                EF.status = PDCUpdated;
            }
            //if (VPType == "PFM")
            //{
            //    EF.DOCKNO = strPFMNo;
            //}
            EF.DockType = VPType;
            return View(EF);
        }

        #endregion

        #region TripSheet Tracker

        public ActionResult TripSheetTracker()
        {
            TrackViewModel VM = new TrackViewModel();
            WEBX_FLEET_VEHICLE_ISSUE objissue = new WEBX_FLEET_VEHICLE_ISSUE();
            VM.TripTrackModel = objissue;
            return View(VM);
        }

        public ActionResult TripSheetTrackerList(TrackViewModel VM)
        {
            WEBX_FLEET_VEHICLE_ISSUE objissue = new WEBX_FLEET_VEHICLE_ISSUE();

            DataTable DT = TS.Get_TripSheet_List_By_Id(VM.opTrackModel.ManualDocNo, VM.opTrackModel.Type);
            objissue = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(DT).FirstOrDefault();
            return PartialView("_TripTrackingPartial", objissue);
        }

        #endregion

        #region Driver Track

        public ActionResult DriverTracker(string Driver)
        {
            LoadingSheetFilterViewModel DRN = new LoadingSheetFilterViewModel();
            List<VW_DRIVER_FINAL_INFO> DRIVERList = new List<VW_DRIVER_FINAL_INFO>();
            DRN.DRIVERList = TS.GetDriverList(Driver).ToList();
            return View(DRN);
        }

        public ActionResult DRIVERList(LoadingSheetFilterViewModel VM)
        {
            List<VW_DRIVER_FINAL_INFO> DRIVERList = new List<VW_DRIVER_FINAL_INFO>();
            DRIVERList = TS.GetDriverList("");
            return PartialView("_DriverTrackerList", DRIVERList);
        }

        public JsonResult GetDriver_NameJson(string searchTerm)
        {
            List<VW_DRIVER_FINAL_INFO> ListLocations = new List<VW_DRIVER_FINAL_INFO>();

            ListLocations = TS.GetDriver_NameList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.Driver_Id,
                                  text = e.Driver_Name,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region  Vehicle Tracking

        public ActionResult VehicleTracking()
        {
            VehicleViewModel VVM = new VehicleViewModel();
            VVM.VehicleHDR = new webx_VEHICLE_HDR();
            VVM.listFLEETDOCUMENT = new List<WEBX_FLEET_DOCUMENT_MST>();
            VVM.listVTrip = new List<vw_VT_tripsheet>();
            VVM.listFFH = new List<VW_VEH_FUEL_FILL_HISTORY>();
            VVM.listodometerHtr = new List<VW_ODOMETER_HISTORY>();
            VVM.listTyreDetails = new List<WEBX_FLEET_TYREMST>();
            return View(VVM);
        }

        public ActionResult VehicleTrackingList(string Vno)
        {
            VehicleViewModel VVM = new VehicleViewModel();
            webx_VEHICLE_HDR obj = new webx_VEHICLE_HDR();

            DataTable DT = TS.GetVehicleList(Vno);
            obj = DataRowToObject.CreateListFromTable<webx_VEHICLE_HDR>(DT).FirstOrDefault();
            VVM.VehicleHDR = obj;

            List<WEBX_FLEET_DOCUMENT_MST> optrkList = TS.Get_ListOfDocumentDetails(Vno);
            VVM.listFLEETDOCUMENT = optrkList;

            List<vw_VT_tripsheet> VTripList = TS.Get_ListOfVehivleTripDetails(Vno);
            VVM.listVTrip = VTripList;

            List<VW_VEH_FUEL_FILL_HISTORY> FFHList = TS.Get_ListOfFFHDetails(Vno);
            VVM.listFFH = FFHList;

            List<VW_ODOMETER_HISTORY> OdoHtrList = TS.Get_ListOf_ODOMETER_HISTORY(Vno);
            VVM.listodometerHtr = OdoHtrList;

            List<WEBX_FLEET_TYREMST> TyreMstList = TS.Get_ListOf_TyreMst(Vno);
            VVM.listTyreDetails = TyreMstList;

            return PartialView("_VehicleTrackingPartial", VVM);
        }

        #endregion

        #region  JobSheet Tracker

        public ActionResult JobSheetTracker()
        {
            return View();
        }
        public ActionResult JobSheetTrackerView(string BILLNO)
        {
            ViewBag.VSlipNo = BILLNO;
            return View();
        }

        #endregion

        #region Voucher Tracker
        public ActionResult VoucherTracker()
        {
            VoucherTrackerViewModel VM = new VoucherTrackerViewModel();
            vw_VoucherTracker objissue = new vw_VoucherTracker();
            VM.VoucherModel = objissue;
            return View(VM);
        }

        public ActionResult VoucherTrackerList(VoucherTrackerViewModel VM)
        {
            DataTable Dt = TS.GetVoucherType(VM.VoucherNoumber, BaseYearVal);
            VM.VoucherModel = DataRowToObject.CreateListFromTable<vw_VoucherTracker>(Dt).FirstOrDefault();
            string DockNo = "Select top 1 Docno from webx_acctrans_" + BaseYearVal + " where Voucherno='" + VM.VoucherNoumber + "' ";
            DataTable Dt1 = GF.getdatetablefromQuery(DockNo);
            VM.VoucherModel.Docno = Dt1.Rows[0][0].ToString();

            return PartialView("_VoucherTrackingPartial", VM.VoucherModel);
        }

        public string VouchernoExistance(string code)
        {
            try
            {
                DataTable Dt = TS.GetVoucherExistorNot(code, BaseYearVal);
                int value = Convert.ToInt32(Dt.Rows[0][0]);
                if (value > 0)
                {
                    return "1";
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception)
            {
                return "0";
            }
        }
        #endregion

        #region GPS Tracker View

        public ActionResult GPSTracker()
        {

            return View();
        }

        #endregion

        #region Debtors Report

        public ActionResult DebtorsReport()
        {
            return View();
        }

        public ActionResult DebtorsReportView(DebtorsModel model)
        {
            if (model.IsOSBill == true)
            {
                model.OSBill = "1";
            }
            else
            {
                model.OSBill = "0";
            }
            return View(model);
        }

        #endregion

        #region  Vehicle Tracking List

        public ActionResult VehicleGPSTrackingList()
        {
            VehicleViewModel VVM = new VehicleViewModel();
            VVM.CVGPCDL = new List<CYGNUS_Vehicle_GPS_Current_Details>();
            return View(VVM);
        }

        public ActionResult GetVehicleTrackingList(VehicleViewModel OTSV)
        {
            List<CYGNUS_Vehicle_GPS_Current_Details> CVGPCDL = new List<CYGNUS_Vehicle_GPS_Current_Details>();
            try
            {
                CVGPCDL = TS.GetVehicleGPSTrackingList();
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetVehicleTrackingList", "Json", "GetVehicleTrackingList", ex.Message);

            }
            return PartialView("_VehicleTracking", CVGPCDL);
        }

        #endregion

        public ActionResult VehicleTrackingShowOnMap(string Vehno)
        {
            string sql = "exec Usp_GetLatLongOfVehicleForGoogleMap '" + Vehno + "'";
            DataTable Dt1 = GF.getdatetablefromQuery(sql);
            if (Dt1.Rows.Count > 0)
            {
                ViewBag.VEHNO = Dt1.Rows[0]["VEHNO"].ToString();
                ViewBag.VehicleLat = Dt1.Rows[0]["VehicleLat"].ToString();
                ViewBag.VehicleLong = Dt1.Rows[0]["VehicleLong"].ToString();
                ViewBag.Description = Dt1.Rows[0]["Description"].ToString();
            }
            return View();
        }

        public ActionResult OperationTrackingCriteria()
        {
            TrackViewModel VM = new TrackViewModel();

            VM.ListLocation = MS.GetWorkingLocations(BaseLocationCode, MainLocCode);

            return View(VM);
        }

        #region THC PRS DRS Tracker
        public ActionResult THCPRSDRSTracker()
        {
            return View();
        }
        public ActionResult THCPRSDRSTrackerView(string BILLType, string BILLNO)
        {
            ViewBag.DoumentType = BILLType;
            ViewBag.DoumentNo = BILLNO;
            return View();
        }
        #endregion
        
    }
}



