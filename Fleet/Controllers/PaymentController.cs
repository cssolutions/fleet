﻿using CipherLib;
using ClosedXML.Excel;
using CYGNUS.Classes;
using CYGNUS.Models;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class PaymentController : BaseController
    {
        //
        // GET: /Payment/
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        GeneralFuncations GF = new GeneralFuncations();
        OperationService OS = new OperationService();
        PaymentService PS = new PaymentService();
        MasterService MS = new MasterService();

        #region BA Payment

        public ActionResult BABillEntryQuery(string id)
        {
            BABillEntyQuery BBEQ = new BABillEntyQuery();
            BBEQ.billType = string.IsNullOrEmpty(id) ? "B" : id;
            return View(BBEQ);
        }

        public ActionResult BABillEntry(BABillEntyQuery BBEQ)
        {
            if (BBEQ.VendorCode == null)
                return RedirectToAction("BABillEntryQuery");

            BABillEnty BBE = new BABillEnty();
            BBEQ.Location = BaseLocationCode;

            WEBX_VENDORBILL_HDR WVH = new WEBX_VENDORBILL_HDR();

            BBE.DocketList = PS.GetDocketList(BBEQ);
            BBE.DocketListForCnote = PS.GetDocketListForBACnotedetail(BBEQ);
            BBE.WVH = WVH;
            BBE.BBEQ = BBEQ;
            webx_VENDOR_HDR ObjVend = MS.GetVendorObject().Where(c => c.VENDORCODE == BBEQ.VendorCode).First();
            BBE.PANNO = ObjVend.PAN_NO;
            BBE.StaxRegNo = ObjVend.SERVTAXNO;
            BBE.BBEQ.VendorName = ObjVend.VENDORNAME;
            BBE.WVH.VENDORTYPE = "BUSINESS ASSOCIATE - BA";
            return View(BBE);
        }
        [HttpPost]
        public ActionResult BABillEntrySubmit(BABillEntyQuery BBEQ, BABillEnty BBE, List<BABillEntyGC> BACommissionBillList, StaxTDSViewModel STVM, List<CygnusChargesHeader> DynamicList, List<BABillEntyGC> BACommissionBillCnoteDetailList)
        {
            try
            {
                string CommissionOnBookingTypedet = "";
                decimal ComissionRatedet = 0;
                string Xml_PAY_Det = "<root>";
                foreach (var itm in BACommissionBillList.Where(c => c.IsEnabled))
                {
                    Xml_PAY_Det = Xml_PAY_Det + "<DetDetails><DocketType>" + itm.DocketType + "</DocketType>";
                    Xml_PAY_Det = Xml_PAY_Det + "<DocketCount>" + itm.DocketCount + "</DocketCount>";
                    Xml_PAY_Det = Xml_PAY_Det + "<WEIGHT>" + itm.WEIGHT + "</WEIGHT>";
                    Xml_PAY_Det = Xml_PAY_Det + "<TotalFreight>" + itm.TotalFreight + "</TotalFreight>";
                    Xml_PAY_Det = Xml_PAY_Det + "<CommissionOnBookingType>" + itm.CommissionOnBookingType + "</CommissionOnBookingType>";
                    Xml_PAY_Det = Xml_PAY_Det + "<CommissionRateTypeBooking>" + itm.CommissionRateTypeBooking + "</CommissionRateTypeBooking>";
                    Xml_PAY_Det = Xml_PAY_Det + "<ComissionRate>" + itm.ComissionRate + "</ComissionRate>";
                    Xml_PAY_Det = Xml_PAY_Det + "<Commission>" + itm.Commision + "</Commission>";
                    Xml_PAY_Det = Xml_PAY_Det + "<DOCKNO>" + itm.DocketType + "</DOCKNO>";
                    Xml_PAY_Det = Xml_PAY_Det + "<debit>" + itm.Commision + "</debit></DetDetails>";
                }

                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                string Xml_Docket_Details = "<root>";
                foreach (var itm in BACommissionBillCnoteDetailList.Where(c => c.IsEnabledforCnote))
                {
                    Xml_Docket_Details = Xml_Docket_Details + "<BillCnoteDetailS><DocketType>" + itm.DocketType + "</DocketType>";
                    Xml_Docket_Details = Xml_Docket_Details + "<dockno>" + itm.dockno + "</dockno>";
                    Xml_Docket_Details = Xml_Docket_Details + "<Srno>" + itm.iSNO + "</Srno>";
                    Xml_Docket_Details = Xml_Docket_Details + "<CommissionOnBookingType>" + CommissionOnBookingTypedet + "</CommissionOnBookingType>";
                    Xml_Docket_Details = Xml_Docket_Details + "<ComissionRate>" + ComissionRatedet + "</ComissionRate>";
                    Xml_Docket_Details = Xml_Docket_Details + "<Paybasedesc>" + itm.Paybasedesc + "</Paybasedesc>";
                    Xml_Docket_Details = Xml_Docket_Details + "<pkgsno>" + itm.pkgsno + "</pkgsno>";
                    Xml_Docket_Details = Xml_Docket_Details + "<WEIGHT>" + itm.WEIGHT + "</WEIGHT>";
                    Xml_Docket_Details = Xml_Docket_Details + "<chrgwt>" + itm.chrgwt + "</chrgwt>";
                    Xml_Docket_Details = Xml_Docket_Details + "<freight>" + itm.freight + "</freight>";
                    Xml_Docket_Details = Xml_Docket_Details + "<TotalFreight>" + itm.TotalFreight + "</TotalFreight></BillCnoteDetailS>";
                }
                Xml_Docket_Details = Xml_Docket_Details + "</root>";



                string Xml_PAY_Mst = "<root>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<HdrDetails><Finyear>" + BaseFinYear.Substring(0, 4) + "</Finyear>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Brcd>" + BaseLocationCode + "</Brcd>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Entryby>" + BaseUserName + "</Entryby>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BILLDT>" + BBE.WVH.BILLDT.ToString("dd MMM yyyy") + "</BILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORCODE>" + BBEQ.VendorCode + "</VENDORCODE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORNAME>" + BBEQ.VendorName + "</VENDORNAME>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLDT>" + BBE.WVH.VENDORBILLDT.ToString("dd MMM yyyy") + "</VENDORBILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLNO>" + BBE.WVH.VENDORBILLNO + "</VENDORBILLNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + BBE.WVH.NETAMT + "</PCAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DUEDT>" + BBE.WVH.DUEDT.ToString("dd MMM yyyy") + "</DUEDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PAYDT>" + BBE.WVH.VENDORBILLDT.ToString("dd MMM yyyy") + "</PAYDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDSRATE>" + STVM.TDSRate + "</TDSRATE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDS>" + STVM.TDSAmount + "</TDS>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<NETAMT>" + BBE.WVH.NETAMT + "</NETAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsfor>" + STVM.CorporateType + "</tdsfor>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsgrpcode>" + STVM.TDSAcccode + "</tdsgrpcode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsgrpdesc>" + STVM.TDSAccdesc + "</tdsgrpdesc>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<acccode>" + BBE.CreditLedger + "</acccode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<accdesc>" + BBE.CreditLedgerName + "</accdesc>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REFNO>" + BBE.WVH.REFNO + "</REFNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DEDUCTION_CHRG>" + BBE.WVH.OtherDedudction + "</DEDUCTION_CHRG>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<discount>" + BBE.WVH.DiscRecvd + "</discount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PENDAMT>" + BBE.WVH.NETAMT + "</PENDAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<netpay>" + BBE.WVH.NETAMT + "</netpay>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TotalCommAmt>" + STVM.TotalCommAmt + "</TotalCommAmt>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PANNO>" + STVM.PANNO + "</PANNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BillPayType>" + BBE.WVH.BillPayType + "</BillPayType>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE></HdrDetails>";
                Xml_PAY_Mst = Xml_PAY_Mst + "</root>";
                Xml_PAY_Mst = Xml_PAY_Mst.ReplaceSpecialCharacters();
                Xml_PAY_Det = Xml_PAY_Det.ReplaceSpecialCharacters();

                string Voucherno = "";


                DataTable DT = PS.Insert_BA_BillEntry_Data(Xml_PAY_Mst, Xml_PAY_Det, Xml_Docket_Details, BBEQ.FromDate.ToString("dd MMM yyyy"), BBEQ.ToDate.ToString("dd MMM yyyy"), BaseLocationCode);

                Voucherno = DT.Rows[0][0].ToString();

                return RedirectToAction("BABillEntryDone", new { Voucherno = Voucherno, Type = (string.IsNullOrEmpty(BBEQ.billType) || BBEQ.billType == "B" ? "BA_Bill" : "FA_Bill") });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
                return View("Error");
                // Response.Redirect("../ErrorPage.aspx?heading=DataBase Entry Failed.&detail1=May Be or Out of Range Wrong Data  Occured&detail2=" + exmess + "&suggestion2=Re Enter " + ViewBag.DKTCalledAs + " with proper Details");
            }
        }

        //[HttpPost]
        //public ActionResult BABillEntrySubmit(BABillEntyQuery BBEQ, BABillEnty BBE, List<BABillEntyGC> BADocketList, StaxTDSViewModel STVM)
        //{
        //    try
        //    {
        //        string Xml_PAY_Det = "<root>";
        //        foreach (var itm in BADocketList.Where(c => c.IsEnabled))
        //        {
        //            Xml_PAY_Det = Xml_PAY_Det + "<DET><DOCKNO>" + itm.dockno + "</DOCKNO>";
        //            Xml_PAY_Det = Xml_PAY_Det + "<PCAMT>" + itm.ContractAmt + "</PCAMT>";
        //            Xml_PAY_Det = Xml_PAY_Det + "<Contract_Rate_Type>" + itm.Contract_Rate_Type + "</Contract_Rate_Type>";
        //            Xml_PAY_Det = Xml_PAY_Det + "<ODACharge>" + itm.ODACharge + "</ODACharge>";
        //            Xml_PAY_Det = Xml_PAY_Det + "<FTLChargeAmount>" + (string.IsNullOrEmpty(BBEQ.billType) || BBEQ.billType == "B" ? 0 : itm.FTLChargeAmount) + "</FTLChargeAmount></DET>";
        //        }

        //        Xml_PAY_Det = Xml_PAY_Det + "</root>";
        //        string Xml_PAY_Mst = "<root>";
        //        // string[] Vendor_Arr = BBEQ.VendorCode;

        //        Xml_PAY_Mst = Xml_PAY_Mst + "<MST><BILLDT>" + BBE.WVH.BILLDT.ToString("dd MMM yyyy") + "</BILLDT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<BRCD>" + BaseLocationCode + "</BRCD>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<BKG_DLY>" + BBEQ.BookingDelivery + "</BKG_DLY>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORCODE>" + BBEQ.VendorCode + "</VENDORCODE>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORNAME>" + BBEQ.VendorName + "</VENDORNAME>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLDT>" + BBE.WVH.VENDORBILLDT.ToString("dd MMM yyyy") + "</VENDORBILLDT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLNO>" + BBE.WVH.VENDORBILLNO + "</VENDORBILLNO>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + BBE.WVH.ContactCommission1 + "</PCAMT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<DUEDT>" + BBE.WVH.DUEDT.ToString("dd MMM yyyy") + "</DUEDT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<OTHAMT>" + STVM.OtherAmountPlus + "</OTHAMT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<OTHERDED>" + STVM.OtherAmountLess + "</OTHERDED>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<SVCTAX>" + STVM.StaxRateAmount + "</SVCTAX>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<cessamt>" + STVM.CessAmount + "</cessamt>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<Hedu_cess>" + STVM.HCessAmount + "</Hedu_cess>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<SBAmount>" + STVM.SBAmount + "</SBAmount>";
        //        //SB Cess
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<SBCtax>" + STVM.SBAmount + "</SBCtax>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<SBCtaxRate>" + STVM.SBRate + "</SBCtaxRate>";
        //        //KK Cess
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<KKCESS>" + STVM.KKCAmount + "</KKCESS>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<KKCRATE>" + STVM.KKCRate + "</KKCRATE>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<TDSRATE>" + STVM.TDSRate + "</TDSRATE>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<TDS>" + STVM.TDSAmount + "</TDS>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<tdsacccode>" + STVM.TDSAcccode + "</tdsacccode>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<tdsaccdesc>" + STVM.TDSAccdesc + "</tdsaccdesc>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<NETAMT>" + BBE.WVH.NETAMT + "</NETAMT>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<ENTRYBY>" + BaseUserName + "</ENTRYBY>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<STaxRegNo>" + STVM.StaxRegNo + "</STaxRegNo>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<PANNO>" + STVM.PANNO + "</PANNO>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<StateCode>" + BBEQ.StateCode + "</StateCode>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<GSTType>" + BBEQ.GSTType + "</GSTType>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<isGSTReverse>" + BBEQ.isGSTReverse + "</isGSTReverse>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "<billType>" + (string.IsNullOrEmpty(BBEQ.billType) || BBEQ.billType == "B" ? "BA" : "FRANCHISE") + "</billType></MST>";
        //        Xml_PAY_Mst = Xml_PAY_Mst + "</root>";

        //        Xml_PAY_Mst = Xml_PAY_Mst.ReplaceSpecialCharacters();
        //        Xml_PAY_Det = Xml_PAY_Det.ReplaceSpecialCharacters();

        //        /* START GST Changes On 26 June 2017 By Chirag D */
        //        XmlDocument xmlDocGSTCha = new XmlDocument();
        //        if (DynamicList != null)
        //        {
        //            XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
        //            using (MemoryStream xmlStreamCha = new MemoryStream())
        //            {
        //                xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
        //                xmlStreamCha.Position = 0;
        //                xmlDocGSTCha.Load(xmlStreamCha);
        //            }
        //        }
        //        else
        //        {
        //            xmlDocGSTCha.InnerXml = "<root></root>";
        //        }

        //        /* END GST Changes On 26 June 2017 By Chirag D */

        //        string Voucherno = "";
        //        DataTable DT = PS.Insert_BA_BillEntry_Data(Xml_PAY_Mst, Xml_PAY_Det, xmlDocGSTCha.InnerXml, "BA_Bill", BaseLocationCode, BaseFinYear.Substring(0, 4), BaseUserName.ToUpper());

        //        Voucherno = DT.Rows[0][0].ToString();

        //        return RedirectToAction("BABillEntryDone", new { Voucherno = Voucherno, Type = (string.IsNullOrEmpty(BBEQ.billType) || BBEQ.billType == "B" ? "BA_Bill" : "FA_Bill") });
        //    }
        //    catch (Exception ex)
        //    {
        //        ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
        //        return View("Error");
        //        // Response.Redirect("../ErrorPage.aspx?heading=DataBase Entry Failed.&detail1=May Be or Out of Range Wrong Data  Occured&detail2=" + exmess + "&suggestion2=Re Enter " + ViewBag.DKTCalledAs + " with proper Details");
        //    }
        //}


        public ActionResult BABillEntryDone(string Voucherno, string Type)
        {
            ViewBag.Voucherno = Voucherno;
            if (string.IsNullOrEmpty(Type))
            {
                ViewBag.Type = "BA_Bill";
            }
            else
            {
                ViewBag.Type = Type;
            }
            return View();
        }


        public JsonResult SearchVendorListJson(string searchTerm)
        {
            var CMP = OS.GetVenderListSearch(searchTerm);

            var users = from user in CMP

                        select new
                        {
                            id = user.VENDORCD,
                            text = user.vendorname
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SearchVendorListJsonBranchWise(string searchTerm)
        {
            var CMP = OS.GetVenderListSearchBranchWise(searchTerm, BaseLocationCode);

            var users = from user in CMP

                        select new
                        {
                            id = user.VENDORCD,
                            text = user.vendorname
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTDSLedger()
        {
            var CMP = PS.GetTDSLedger();

            var users = from user in CMP

                        select new
                        {
                            Value = user.CodeId,
                            Text = user.CodeDesc
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTaxRate(string DocuemntDate)
        {
            var CMP = PS.GetBAStax(DocuemntDate);

            return new JsonResult()
            {
                Data = new
                {
                    StaxRate = CMP.FirstOrDefault().StaxRate,
                    CessRate = CMP.FirstOrDefault().CessRate,
                    HEduCessRate = CMP.FirstOrDefault().HCessRate,
                    SBRate = CMP.FirstOrDefault().SBRate,
                    KKCRate = CMP.FirstOrDefault().KKCRate,

                }
            };
        }

        public JsonResult GetTaxRateFromID(int Id)
        {
            var CMP = PS.GetBAStaxList(Id);

            return new JsonResult()
            {
                Data = new
                {
                    StaxRate = CMP.FirstOrDefault().StaxRate,
                    CessRate = CMP.FirstOrDefault().CessRate,
                    HEduCessRate = CMP.FirstOrDefault().HCessRate,
                    SBRate = CMP.FirstOrDefault().SBRate,
                    KKCRate = CMP.FirstOrDefault().KKCRate,

                }
            };
        }

        public JsonResult GetVendorServiceNoAndPanNo(string VendorCode)
        {
            DataTable CMP = PS.GetVendorServiceNoAndPanNo(VendorCode);

            return new JsonResult()
            {
                Data = new
                {
                    ServiceTaxNo = CMP.Rows[0]["ServiceTaxNo"].ToString().ToUpper(),
                    PanNo = CMP.Rows[0]["PanNo"].ToString().ToUpper(),
                }
            };
        }

        #endregion

        #region Finalize Bill

        public ActionResult FinalizeBillQuery()
        {

            FinalizeBillQuery FBQ = new FinalizeBillQuery();

            return View(FBQ);
        }

        public ActionResult FinalizeBill(FinalizeBillQuery FBQ, string Location, string LocationLevel)
        {
            FBQ.Location = Location;
            FBQ.LocationLevel = LocationLevel;

            FinalizeBillViewModel FBVM = new FinalizeBillViewModel();
            FBVM.FBQ = FBQ;
            FBVM.ListFBD = PS.FinalizeBillList(FBQ, BaseFinYear.Substring(0, 4));

            return View(FBVM);
        }

        //public ActionResult SubmitFinalizeBill(FinalizeBillViewModel FBVM, List<FinalizeBillDetails> BILLList)
        //{

        //    SqlConnection con = new SqlConnection(GF.GetConnstr());
        //    con.Open();
        //    SqlTransaction trn = con.BeginTransaction();

        //    string DOCNO = "";
        //    try
        //    {
        //        foreach (var itm in BILLList.Where(c => c.IsEnabled))
        //        {

        //            string DockNo = itm.BillNo;
        //            string BillType = itm.BillType;
        //            string Str = "";

        //            DOCNO = DOCNO == "" ? DockNo : DOCNO + "," + DockNo;

        //            double cnt = 0;
        //            string StrSql = "select count(*) from WEBX_VENDORBILL_HDR WITH(ROWLOCK) WHERE BILLNO='" + DockNo + "' and finalized ='N'";
        //            cnt = Convert.ToDouble(GF.executeScalerQuery(StrSql));

        //            if (cnt == 1)
        //            {
        //                Str = "UPDATE dbo.WEBX_VENDORBILL_HDR WITH(ROWLOCK) SET finalized ='Y',IsNewCP_finalized=1,Finalized_By='" + BaseUserName + "'";
        //                Str = Str + ",Finalized_Date=getdate() WHERE BILLNO='" + DockNo + "';";

        //                if (BillType == "THC-PDC")
        //                {
        //                    Str = Str + " exec usp_VendorPayment_Transaction_Cumulative '4','" + DockNo + "','" + BaseFinYear + "','03','E'";
        //                }
        //                else if (BillType == "BA")
        //                {
        //                    Str = Str + " exec usp_VendorPayment_Transaction '7','" + DockNo + "','" + BaseFinYear + "','03','E'";
        //                }
        //                else if (BillType == "Octroi")
        //                {
        //                    //Str = "UPDATE dbo.WEBX_VENDORBILL_HDR WITH(ROWLOCK) SET finalized ='Y',Finalized_By='" + SessionUtilities.CurrentEmployeeID + "'";
        //                    //Str = Str + ",Finalized_Date=getdate() WHERE BILLNO='" + DockNo + "'";
        //                    Str = Str + " UPDATE dbo.WEBX_oct_hdr WITH(ROWLOCK) SET finalized ='Y' WHERE ocbillno='" + DockNo + "'";
        //                    Str = Str + " exec usp_OctroiAgentBILL_Transaction '1','" + DockNo + "','" + BaseFinYear + "','07','E'";
        //                    //SqlHelper.ExecuteScalar(trn, CommandType.Text, Str);
        //                }
        //                else if (BillType == "Expense")
        //                {
        //                    Str = Str + " exec USP_Other_BillEntry_Finalized '1','" + DockNo + "','" + BaseLocationCode + "','" + BaseFinYear + "','" + BaseUserName + "','" + BaseCompanyCode + "'";
        //                }
        //                string Finalized = "N";
        //                string strcheck = "SELECT isNULL(Finalized,'N') as Finalized FROM WEBX_VENDORBILL_HDR WHERE BILLNO='" + DockNo + "'";
        //                Finalized = GF.executeScalerQuery(strcheck);
        //                int Id = GF.SaveRequestServices(Str.Replace("'", "''"), "SubmitFinalizeBill", "", "");
        //                if (Finalized == "N")
        //                {
        //                    GF.executeNonQuery(Str);
        //                }
        //            }
        //        }
        //        trn.Commit();
        //        con.Close();

        //        return RedirectToAction("FinalizeBillDone", new { DOCNO = DOCNO });

        //    }
        //    catch (Exception ex)
        //    {
        //        trn.Rollback();
        //        con.Close(); con.Dispose();
        //        //string ErrorMsg = ex.Message;
        //        //return RedirectToAction("Error", new { Message = ErrorMsg });
        //        ViewBag.StrError = ex.Message;
        //        return View("Error");
        //    }

        //    return View(FBVM.FBQ);
        //}

        public ActionResult SubmitFinalizeBill(FinalizeBillViewModel FBVM, List<FinalizeBillDetails> BILLList)
        {
            string Vendor_XML = "";
            try
            {
                Vendor_XML = Vendor_XML + "<root>";
                foreach (var itm in BILLList.Where(c => c.IsEnabled))
                {
                    Vendor_XML = Vendor_XML + "<Finalize_VendorBill>";
                    Vendor_XML = Vendor_XML + "<DockNo>" + itm.BillNo + "</DockNo>";
                    Vendor_XML = Vendor_XML + "<BillType>" + itm.BillType + "</BillType>";
                    Vendor_XML = Vendor_XML + "</Finalize_VendorBill>";
                }
                Vendor_XML = Vendor_XML + "</root>";

                string SQRY = "exec Usp_Finalize_VendorBillGST '" + Vendor_XML + "','" + BaseUserName + "','" + BaseFinYear + "','" + BaseCompanyCode + "','" + BaseLocationCode + "'";
                int Ids = GF.SaveRequestServices(SQRY.Replace("'", "''"), "SubmitFinalizeBill_Procedure", "", "");
                DataTable DT = GF.GetDataTableFromSP(SQRY);

                string Message = "", Status = "", No = "";
                Message = DT.Rows[0][0].ToString();
                Status = DT.Rows[0][1].ToString();
                No = DT.Rows[0][2].ToString();

                if (Message == "Done" && Status == "1")
                {
                    return RedirectToAction("FinalizeBillDone", new { No = No });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult FinalizeBillDone(string No)
        {

            ViewBag.No = No;

            return View();
        }


        #endregion

        #region Advance Balance Payment

        public ActionResult AdvanceQuery()
        {
            AdvancePaymentFilter BBEQ = new AdvancePaymentFilter();
            BBEQ.Type = "A";
            return View(BBEQ);
        }

        public ActionResult AdvanceDocumentDetails(AdvancePaymentFilter APFM)
        {
            APFM.BaseLocation = BaseLocationCode;
            if (APFM.VendorCode == null)
                return RedirectToAction("AdvanceQuery");

            AdvancePaymentViewModel APVM = new AdvancePaymentViewModel();
            APFM.Type = "A";
            APVM.APFM = APFM;
            APVM.listVWTAPM = PS.PaymentList(APFM, BaseUserName).ToList();
            return View(APVM);
        }

        public ActionResult AdvancePayment(AdvancePaymentFilter APFM)
        {
            APFM.BaseLocation = BaseLocationCode;
            if (APFM.VendorCode == null)
                return RedirectToAction("AdvanceBalanceQuery");

            AdvancePaymentViewModel APVM = new AdvancePaymentViewModel();
            APFM.Type = "A";
            APVM.APFM = APFM;
            APVM.listVWTAPM = PS.PaymentList(APFM, BaseUserName).ToList();
            APVM.BalanceLocation = BaseLocationCode;
            return View(APVM);
        }

        [HttpPost]
        public ActionResult AdvancePaymentSubmit(AdvancePaymentViewModel APVM, List<VW_THC_AdvancePayment> DocuemntList, PaymentControl PC)
        {
            GeneralFuncations GF = new GeneralFuncations();

            if (PC.PaymentMode == "Cash")
                PC.CashAmount = PC.PayAmount;
            if (PC.PaymentMode == "Bank")
                PC.ChequeAmount = PC.PayAmount;

            try
            {

                string Cnt = "";
                if (PC.PaymentMode != "Cash")
                {
                    Cnt = PS.Duplicate_ChqNO(PC.ChequeNo, GF.FormateDate(PC.ChequeDate));
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                if (PC.PaymentMode == "Cash" || PC.PaymentMode == "Both")
                {
                    Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(PC.CashAmount), GF.FormateDate(APVM.AdvanceDate), BaseFinYear, BaseLocationCode);
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                }

                string Xml_PAY_Det = "<root>";

                foreach (var itm in DocuemntList)
                {
                    Xml_PAY_Det = Xml_PAY_Det + "<PAY_Det><docno>" + itm.Docno.ToString() + "</docno>";
                    Xml_PAY_Det = Xml_PAY_Det + "<PAYDT>" + GF.FormateDate(APVM.AdvanceDate) + "</PAYDT>";
                    Xml_PAY_Det = Xml_PAY_Det + "<Manual_Voucherno>" + APVM.ManualVoucherno + "</Manual_Voucherno>";
                    Xml_PAY_Det = Xml_PAY_Det + "<PayMode>" + PC.PaymentMode + "</PayMode>";
                    Xml_PAY_Det = Xml_PAY_Det + "<CashAmt>" + PC.CashAmount + "</CashAmt>";
                    Xml_PAY_Det = Xml_PAY_Det + "<Cashcode>" + PC.CashLedger + "</Cashcode>";
                    Xml_PAY_Det = Xml_PAY_Det + "<ChqAmt>" + PC.ChequeAmount + "</ChqAmt>";
                    Xml_PAY_Det = Xml_PAY_Det + "<ChqNo>" + PC.ChequeNo + "</ChqNo>";

                    if (PC.PaymentMode.ToString() == "Cash")
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<Bankaccode>" + "" + "</Bankaccode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Bankaccodesc>" + "" + "</Bankaccodesc>";
                    }
                    else
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<Bankaccode>" + PC.BankLedger + "</Bankaccode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Bankaccodesc>" + PC.BankLedgerName + "</Bankaccodesc>";
                    }
                    Xml_PAY_Det = Xml_PAY_Det + "<ChqDate>" + GF.FormateDate(PC.ChequeDate) + "</ChqDate>";
                    Xml_PAY_Det = Xml_PAY_Det + "<NetPay>" + PC.PayAmount + "</NetPay>";
                    Xml_PAY_Det = Xml_PAY_Det + "<AdvAmt>" + itm.Advamt + "</AdvAmt>";
                    Xml_PAY_Det = Xml_PAY_Det + "<BalAmt>" + itm.netbalamt + "</BalAmt>";
                    Xml_PAY_Det = Xml_PAY_Det + "<BalAmtBRCD>" + APVM.BalanceLocation + "</BalAmtBRCD>";
                    Xml_PAY_Det = Xml_PAY_Det + "<Vendorcode>" + APVM.APFM.VendorCode + "</Vendorcode>";
                    Xml_PAY_Det = Xml_PAY_Det + "<VendorName>" + APVM.APFM.VendorName + "</VendorName>";
                    Xml_PAY_Det = Xml_PAY_Det + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                    Xml_PAY_Det = Xml_PAY_Det + "<DocumentType>" + itm.DocumentType + "</DocumentType></PAY_Det>";

                }
                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                string Voucherno = PS.Insert_Advance_Balance_BillEntry_Data(Xml_PAY_Det, "ADV", "<root></root>", BaseLocationCode, BaseFinYear, BaseUserName);

                return RedirectToAction("AdvancePaymentDone", new { Voucherno = Voucherno });

            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                {
                    ViewBag.StrError = "";
                }
                else
                {
                    ViewBag.StrError = ex.InnerException.ToString();
                }
                return View("Error");
            }
        }

        public ActionResult AdvancePaymentDone(string Voucherno)
        {
            if (Voucherno.Contains("~"))
            {
                ViewBag.Voucherno = Voucherno.Split('~')[0].ToString();
                ViewBag.S = Voucherno.Split('~')[1].ToString();
            }
            else
            {
                ViewBag.Voucherno = Voucherno;
            }
            return View();
        }

        public ActionResult BalanceQuery()
        {
            AdvancePaymentFilter BBEQ = new AdvancePaymentFilter();
            BBEQ.Type = "B";
            return View(BBEQ);
        }

        public ActionResult BalanceContractDetails(AdvancePaymentFilter APFM)
        {
            APFM.BaseLocation = BaseLocationCode;
            if (APFM.VendorCode == null)
                return RedirectToAction("BalanceQuery");

            BalancePaymentViewModel BPVM = new BalancePaymentViewModel();

            APFM.Type = "B";
            BPVM.APFM = APFM;

            VendorContractDetails VCD = PS.GetVendorContract(APFM.VendorCode);
            BPVM.ContractId = VCD.ContractId;
            BPVM.ContractTypeDesc = VCD.ContractTypeDesc;
            BPVM.ContractTypeId = VCD.ContractTypeId;
            string ContractType = BPVM.ContractTypeId;

            BPVM.VehicleList = new List<Webx_Master_General>();
            BPVM.FTLList = new List<Webx_Master_General>();

            if (BPVM.ContractId == "NA" || BPVM.ContractId == "NA")
            {
                BPVM.ContractAlert = "The Same Vendor has Not Any Contract.SO, User Can Go Ahead With Selected Criteria :- System will Take Ad-Hoc Base Contract by Default.";
            }
            BPVM.ADHContract = "Ad-Hoc Based Contract";

            if (ContractType == "01" || ContractType == "02" || ContractType == "03" || ContractType == "05" || ContractType == "06" || ContractType == "09" || ContractType == "10" || ContractType == "11" || ContractType == "07")
            {
                BPVM.THCContract = "Route Based For :-" + ViewBag.THCCalledAs;
                BPVM.PDSContract = "Distance Based For :- PRS/DRS";

                if (ContractType == "09" || ContractType == "10" || ContractType == "11" || ContractType == "07")
                {
                    BPVM.THCContract = "City Based For :-" + ViewBag.THCCalledAs;
                    BPVM.PDSContract = "Distance Based For :- PRS/DRS";
                }


                BPVM.ContractAlert = "User Has To Select Either " + ViewBag.THCCalledAs + " OR PRS/DRS";

                if (ContractType == "02" || ContractType == "05" || ContractType == "10")
                {
                    // VendorPayment_Utility.Fill_DDL_Mst_Selection_From_Array(VendorPayment_Utility.Get_VendorContract_Mst_Selection_Array(Vendorcode, ContractID, "FTLTYP"), DDL_FtlType);

                    BPVM.FTLList = PS.GetFTLVehiclefromContract(BPVM.APFM.VendorCode, BPVM.ContractId, "FTL");

                    BPVM.ContractAlert = "User Has To Select FTL Type For Distance Based Contract For :- ";
                    if (ContractType == "05")
                    {
                        BPVM.ContractAlert = BPVM.ContractAlert + ViewBag.THCCalledAs + "/PRS/DRS";

                    }
                    if (ContractType == "02" || ContractType == "10")
                        BPVM.ContractAlert = BPVM.ContractAlert + "PRS/DRS";

                }
                else if (ContractType == "03" || ContractType == "06" || ContractType == "11")
                {
                    // VendorPayment_Utility.Fill_DDL_Mst_Selection_From_Array(VendorPayment_Utility.Get_VendorContract_Mst_Selection_Array(Vendorcode, ContractID, "VEHNO"), DDL_Vehno);
                    //TR_Contype_03.Visible = true;
                    BPVM.VehicleList = PS.GetFTLVehiclefromContract(BPVM.APFM.VendorCode, BPVM.ContractId, "FTL");

                    BPVM.ContractAlert = "User Has To Select Vehicle Number For Distance Based Contract For :- ";
                    if (ContractType == "06")
                    {
                        BPVM.ContractAlert = BPVM.ContractAlert + ViewBag.THCCalledAs + "/PRS/DRS";

                    }
                    if (ContractType == "03" || ContractType == "11")
                        BPVM.ContractAlert = BPVM.ContractAlert + "PRS/DRS";
                }
            }
            if (ContractType == "04" || ContractType == "05" || ContractType == "06")
            {
                if (ContractType == "04")
                {
                    BPVM.ContractAlert = "User Can Go Ahead With Selected Criteria";
                }
            }
            return View(BPVM);
        }

        public ActionResult BalanceDocumentDetails(BalancePaymentViewModel BPVM)
        {
            try
            {
                BPVM.APFM.BaseLocation = BaseLocationCode;
                if (BPVM.APFM.VendorCode == null)
                    return RedirectToAction("BalanceQuery");
                string Doctype = "";

                string DocTypeText = "-" + BPVM.APFM.DocTypeText;

                if (DocTypeText.IndexOf("THC") > 0)
                    Doctype = Doctype == "" ? "T" : Doctype + "T";
                if (DocTypeText.IndexOf("PRS") > 0)
                    Doctype = Doctype == "" ? "P" : Doctype + ",P";
                if (DocTypeText.IndexOf("DRS") > 0)
                    Doctype = Doctype == "" ? "D" : Doctype + ",D";

                BPVM.APFM.DocType = Doctype;
                BPVM.APFM.Type = "B";
                BPVM.APFM.ContractSubtype = BPVM.ContractSubType;
                BPVM.listVWTAPM = PS.BalancePaymentList(BPVM.APFM, BaseUserName).ToList();

                return View(BPVM);
            }
            catch (Exception)
            {
                return RedirectToAction("BalanceQuery");
            }
        }

        public ActionResult BalancePayment(BalancePaymentViewModel BPVM)
        {
            try
            {
                BPVM.APFM.BaseLocation = BaseLocationCode;
                if (BPVM.APFM.VendorCode == null)
                    return RedirectToAction("BalanceQuery");


                BPVM.ListCharge = PS.ListCharge("BAL");

                BPVM.APFM.Type = "B";

                // BPVM.APFM.ContractSubtype = BPVM.ContractSubType;
                BPVM.APFM.ContractSubtype = BPVM.APFM.ContractSubtype;
                BPVM.listVWTAPM = PS.BalancePaymentList(BPVM.APFM, BaseUserName).ToList();
                return View(BPVM);
            }
            catch (Exception)
            {
                return RedirectToAction("BalanceQuery");
            }
        }

        [HttpPost]
        public ActionResult BalancePaymentSubmit(BalancePaymentViewModel APVM, List<VW_THC_BalancePayment> DocuemntList, PaymentControl PC, StaxTDSViewModel ST, List<CygnusChargesHeader> DynamicList)
        {
            GeneralFuncations GF = new GeneralFuncations();

            if (PC.PaymentMode == "Cash")
                PC.CashAmount = PC.PayAmount;
            if (PC.PaymentMode == "Bank")
                PC.ChequeAmount = PC.PayAmount;

            try
            {

                string STaxApply = "N", TDSApply = "N", VoucherType_Desc = "", VoucherType = "", Betype = "", FinCloseDt = "";
                if (ST.IsStaxEnabled)
                {
                    STaxApply = "Y";
                }

                if (ST.IsTDSEnabled)
                    TDSApply = "Y";

                VoucherType_Desc = "Bill Entry";

                string Cnt = "";
                if (PC.PaymentMode != "Cash")
                {
                    Cnt = PS.Duplicate_ChqNO(PC.ChequeNo, GF.FormateDate(PC.ChequeDate));
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                if (PC.PaymentMode == "Cash" || PC.PaymentMode == "Both")
                {
                    Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(PC.CashAmount), GF.FormateDate(APVM.VENDORBILLDT), BaseFinYear, BaseLocationCode);
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                }

                if (APVM.PaymentType == "1")
                {
                    VoucherType_Desc = "Bill Entry";
                    VoucherType = "BE";
                    Betype = "BE_NP";
                    FinCloseDt = "NULL";
                }
                else if (APVM.PaymentType == "2")
                {
                    VoucherType_Desc = "Bill Entry";
                    VoucherType = "BAL";
                    Betype = "BE_FPP";
                    FinCloseDt = GF.FormateDate(APVM.BILLDT);
                }

                string Xml_PAY_Det = "<root>";

                foreach (var itm in DocuemntList)
                {

                    // APVM.TotPendingAmount = APVM.TotVoucherAmount - PC.PayAmount;

                    if (PS.IsValidDocument(itm.Docno))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<PAY_Det><EntryType>" + VoucherType_Desc + "</EntryType>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Betype>" + Betype + "</Betype>";
                        Xml_PAY_Det = Xml_PAY_Det + "<BillAmt>" + APVM.TotVoucherAmount + "</BillAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + APVM.TotPendingAmount + "</PendAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<GSTChgAmt>" + itm.GSTChgAmt + "</GSTChgAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PayAmt>" + APVM.TotalPaymentAmount + "</PayAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Vendor_BE_Date>" + GF.FormateDate(APVM.VENDORBILLDT) + "</Vendor_BE_Date>";
                        Xml_PAY_Det = Xml_PAY_Det + "<FinCloseDt>" + FinCloseDt + "</FinCloseDt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DueDays>" + APVM.DueDays + "</DueDays>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DueDate>" + GF.FormateDate(APVM.DUEDT) + "</DueDate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PCAMT>" + itm.Contract_Amt + "</PCAMT>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TOTPCAMT>" + APVM.TotalContractAmount + "</TOTPCAMT>";
                        Xml_PAY_Det = Xml_PAY_Det + "<OTHAMT>" + itm.OTHCHRG + "</OTHAMT>";
                        Xml_PAY_Det = Xml_PAY_Det + "<ADVPAID>" + itm.Advamt + "</ADVPAID>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DocNetPay>" + itm.NET + "</DocNetPay>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PendingAmount>" + itm.PendingAmount + "</PendingAmount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<docno>" + itm.Docno + "</docno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PAYDT>" + GF.FormateDate(APVM.BILLDT) + "</PAYDT>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Manual_Voucherno>" + APVM.VENDORBILLNO + "</Manual_Voucherno>";
                        // Xml_PAY_Det = Xml_PAY_Det + "<Manual_Voucherno>" + Txt_Manual_Voucherno.Text.ToString() + "</Manual_Voucherno>";

                        if (APVM.PaymentType == "2")
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<PayMode>" + PC.PaymentMode + "</PayMode>";
                            Xml_PAY_Det = Xml_PAY_Det + "<CashAmt>" + PC.CashAmount + "</CashAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Cashcode>" + PC.CashLedger + "</Cashcode>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ChqAmt>" + PC.ChequeAmount + "</ChqAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ChqNo>" + PC.ChequeNo + "</ChqNo>";

                            if (PC.PaymentMode.ToString() == "Cash")
                            {
                                Xml_PAY_Det = Xml_PAY_Det + "<Bankaccode>" + "" + "</Bankaccode>";
                                Xml_PAY_Det = Xml_PAY_Det + "<Bankaccodesc>" + "" + "</Bankaccodesc>";
                            }
                            else
                            {
                                Xml_PAY_Det = Xml_PAY_Det + "<Bankaccode>" + PC.BankLedger + "</Bankaccode>";
                                Xml_PAY_Det = Xml_PAY_Det + "<Bankaccodesc>" + PC.BankLedgerName + "</Bankaccodesc>";
                            }
                            Xml_PAY_Det = Xml_PAY_Det + "<ChqDate>" + GF.FormateDate(PC.ChequeDate) + "</ChqDate>";

                            Xml_PAY_Det = Xml_PAY_Det + "<NetPay>" + PC.PayAmount + "</NetPay>";
                        }

                        //for (int i = 1; i < 11; i++)
                        //{
                        //    txtcharge[i - 1] = (TextBox)gridrow.FindControl("SCHG" + i.ToString());
                        //    Xml_PAY_Det = Xml_PAY_Det + "<SCHG" + i + ">" + txtcharge[i - 1].Text.ToString() + "</SCHG" + i + ">";
                        //}

                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG01>" + itm.SCHG01 + "</SCHG01>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG02>" + itm.SCHG02 + "</SCHG02>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG03>" + itm.SCHG03 + "</SCHG03>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG04>" + itm.SCHG04 + "</SCHG04>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG05>" + itm.SCHG05 + "</SCHG05>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG06>" + itm.SCHG06 + "</SCHG06>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG07>" + itm.SCHG07 + "</SCHG07>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG08>" + itm.SCHG08 + "</SCHG08>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG09>" + itm.SCHG09 + "</SCHG09>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SCHG10>" + itm.SCHG10 + "</SCHG10>";
                        /*************Party Details  ***************************/
                        Xml_PAY_Det = Xml_PAY_Det + "<Vendorcode>" + APVM.APFM.VendorCode + "</Vendorcode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<VendorName>" + APVM.APFM.VendorName + "</VendorName>";
                        /*************STax/TDS Charges Details END ***************************/
                        if (ST.IsStaxEnabled)
                            Xml_PAY_Det = Xml_PAY_Det + "<STaxApply>Y</STaxApply>";
                        else
                            Xml_PAY_Det = Xml_PAY_Det + "<STaxApply>N</STaxApply>";
                        if (ST.IsTDSEnabled)
                            Xml_PAY_Det = Xml_PAY_Det + "<TDSApply>Y</TDSApply>";
                        else
                            Xml_PAY_Det = Xml_PAY_Det + "<TDSApply>N</TDSApply>";
                        Xml_PAY_Det = Xml_PAY_Det + "<servicetaxrate>" + ST.StaxRate + "</servicetaxrate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Stax>" + ST.StaxRateAmount + "</Stax>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Cess>" + ST.CessAmount + "</Cess>";
                        Xml_PAY_Det = Xml_PAY_Det + "<HCess>" + ST.HCessAmount + "</HCess>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TdsRate>" + ST.TDSRate + "</TdsRate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TdsAmt>" + ST.TDSAmount + "</TdsAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TdsLedger>" + ST.TDSAcccode + "</TdsLedger>";
                        Xml_PAY_Det = Xml_PAY_Det + "<STaxRegNo>" + ST.StaxRegNo + "</STaxRegNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PANNO>" + ST.PANNO + "</PANNO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Txt_Remarks>" + APVM.Remarks + "</Txt_Remarks>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Discount>" + ST.Discount + "</Discount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DiscountType>" + ST.DiscountType + "</DiscountType>";
                        //SB Cess
                        Xml_PAY_Det = Xml_PAY_Det + "<SBCtax>" + ST.SBAmount + "</SBCtax>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SBCtaxRate>" + ST.SBRate + "</SBCtaxRate>";
                        //KK Cess
                        Xml_PAY_Det = Xml_PAY_Det + "<KKCESS>" + ST.KKCAmount + "</KKCESS>";
                        Xml_PAY_Det = Xml_PAY_Det + "<KKCRATE>" + ST.KKCRate + "</KKCRATE>";
                        //END
                        Xml_PAY_Det = Xml_PAY_Det + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DocumentType>" + itm.DocumentType + "</DocumentType>";

                        Xml_PAY_Det = Xml_PAY_Det + "<StateCode>" + APVM.APFM.StateCode + "</StateCode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<GSTType>" + APVM.APFM.GSTType + "</GSTType>";
                        Xml_PAY_Det = Xml_PAY_Det + "<isGSTReverse>" + APVM.isGSTReverse + "</isGSTReverse>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TRN_MODE>" + DocuemntList.FirstOrDefault().Rut_cat + "</TRN_MODE></PAY_Det>";
                    }
                    else
                    {
                        ViewBag.StrError = "Bill Entry Is already Generated For Document : " + itm.Docno;
                        return View("Error");
                    }
                }

                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                XmlDocument xmlDocGSTCha = new XmlDocument();
                if (DynamicList != null && DynamicList.Count > 0)
                {
                    XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                    using (MemoryStream xmlStreamCha = new MemoryStream())
                    {
                        xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                        xmlStreamCha.Position = 0;
                        xmlDocGSTCha.Load(xmlStreamCha);
                    }
                }
                else
                {
                    xmlDocGSTCha.InnerXml = "<root></root>";
                }

                string Voucherno = PS.Insert_Advance_Balance_BillEntry_Data(Xml_PAY_Det, "BE", xmlDocGSTCha.InnerXml, BaseLocationCode, BaseFinYear.Substring(0, 4), BaseUserName);
                return RedirectToAction("BalancePaymentDone", new { Voucherno = Voucherno });
            }
            catch (Exception ex)
            {
                if (ex.InnerException == null)
                {
                    ViewBag.StrError = "";
                }
                else
                {
                    ViewBag.StrError = ex.InnerException.ToString();
                }


                return View("Error");
            }
        }

        public ActionResult BalancePaymentDone(string Voucherno)
        {
            if (Voucherno.Contains("~"))
            {
                ViewBag.Voucherno = Voucherno.Split('~')[0].ToString();
                ViewBag.S = Voucherno.Split('~')[1].ToString();
                ViewBag.Type = Voucherno.Split('~')[2].ToString();
                ViewBag.GV = Voucherno.Split('~')[3].ToString();
            }
            else
            {
                ViewBag.Voucherno = Voucherno;
            }
            return View();
        }

        #endregion

        #region Fleet Bill Entry

        public ActionResult FuelSlipQuery()
        {
            FuelBillEntyQuery FBE = new FuelBillEntyQuery();
            return View(FBE);
        }

        public ActionResult POBillEntry()
        {
            FuelBillEntyQuery FBE = new FuelBillEntyQuery();
            TempData["GRN"] = "Other";
            FBE.Type = 5;
            return View(FBE);
        }

        public ActionResult JOBBillEntry()
        {
            //if(BaseLocationCode != "HQTR")
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
            FuelBillEntyQuery FBE = new FuelBillEntyQuery();
            TempData["GRN"] = "JOBBill";
            FBE.Type = 6;
            return View(FBE);
        }

        public ActionResult ExpenseBillEntry()
        {
            ExpenseBillEntryQuery FBE = new ExpenseBillEntryQuery();
            return View(FBE);
        }

        public ActionResult FuelBillEntryCriteria(string FilterType)
        {
            BillEntryCriteria BEC = new BillEntryCriteria();
            BEC.FilterType = FilterType;
            return View(BEC);
        }

        public ActionResult FuelBillEntry(string Id, FuelBillEntyQuery FBE, ExpenseBillEntryQuery EBE, BillEntryCriteria BEC)
        {
            if (Id == null || Id == "")
            {
                if (BEC.FilterType == null || BEC.FilterType == "")
                {
                    Id = "1";
                }
                else
                {
                    Id = BEC.FilterType;
                }
            }
            /*
             * 1. Expense Bill Entry - Fuel
             * 2. Fuel Slip Bill Entry - TRIP/THC
             * 3. Expense Bill Entry - Fixed & Variable
             * 4. Other Bill Entry 
             * 5. PO Bill Entry
             * 6. JOB Bill Entry
             * 7. GRN Bill Entry
             * 8. Loading-Unloading Charges
             * 9. Monthly Loading-Unloading Charges
             * 10.Attached Vendor Bill Entry
             * 11.Attached Vendor Monthly Bill Entry
             * 12.
             * 13.Connectivity Charge Bill Entry for THC
             * */


            if (FBE.VendorCode == null && Id == "2")
                return RedirectToAction("FuelSlipQuery");

            if (Id == "3")
                return RedirectToAction("ExpenseFixedVariableBillEntry", BEC);

            if (FBE.VendorCode == null && Id == "5")
                return RedirectToAction("POBillEntry");

            if (FBE.VendorCode == null && Id == "6")
                return RedirectToAction("JOBBillEntry");

            if (FBE.VendorCode == null && Id == "7")
                return RedirectToAction("GRNBillEntry");

            if (FBE.VendorCode == null && Id == "13")
                return RedirectToAction("ConnectivityBillQuery");

            BABillEnty BBE = new BABillEnty();
            BBE.FBE = FBE;
            BBE.EBE = EBE;
            BBE.BillEntryType = Id;
            WEBX_VENDORBILL_HDR WVH = new WEBX_VENDORBILL_HDR();
            BBE.WVH = WVH;
            //if (Id == "8" && FBE.BillType == "M")
            if (Id == "8" || Id == "9" || Id == "10" || Id == "11" || Id == "13")
            {
                BBE.WVH.VENDORCODE = FBE.VendorCode;
            }
            else
            {
                BBE.WVH.VENDORCODE = FBE.VendorCode;
            }

            BBE.WVH.VENDORNAME = FBE.VendorName;

            if (FBE != null)
                FBE.Location = BaseLocationCode;

            if (Id == "2")
                BBE.FuelTripsheetList = PS.GetFuelSlipTripSheet(FBE);

            if (Id == "3")
                BBE.FuelTripsheetList = PS.GetFuelSlipTripSheet(FBE);

            if (Id == "5")
            {
                BBE.POList = PS.GetPOForBillEntry(FBE).Where(m => m.GSTPercentage == BEC.GSTPercentage).ToList();
            }

            if (Id == "6")
                BBE.JobList = PS.GetJOBForBillEntry(FBE);

            FuelBillEntyQuery FBE1 = new FuelBillEntyQuery();
            if (Id == "7")
            {
                FBE1 = (FuelBillEntyQuery)TempData["FBE"];
                TempData.Keep("FBE");
                //BBE.GRNList = PS.GetPOGRNForBillEntryDetails(FBE1.ManualPONo);
                BBE.GRNList = PS.GetPOGRNForBillEntry(FBE.ManualPONo);
            }
            if (Id == "8")
            {
                BBE.JobList = PS.GetLoadBillEntry(FBE);
            }
            if (Id == "9")
            {
                DateTime now = Convert.ToDateTime(FBE.MonthWithYear);
                FBE.FromDate = new DateTime(now.Year, now.Month, 1);
                FBE.ToDate = FBE.FromDate.AddMonths(1).AddDays(-1);
                BBE.WVH.BillGENMonth = Convert.ToDateTime(FBE.MonthWithYear).ToString("MMM yyyy");
                string SQL = "SELECT COUNT(*) AS CNT FROM WEBX_VENDORBILL_HDR WHERE betype='Monthly Loading' AND VENDORCODE='" + BBE.WVH.VENDORCODE + "' AND ISNULL(bill_cancel,'N')='N' ";
                SQL = SQL + " AND ISNULL(BillGENMonth,'') = '" + BBE.WVH.BillGENMonth + "'  AND BRCD='" + FBE.Branch + "'";
                //SQL = SQL + " AND CONVERT(VARCHAR,BILLDT,106) BETWEEN CONVERT(DATETIME,'" + GF.FormateDate(FBE.FromDate) + "',106) AND CONVERT(DATETIME,'" + GF.FormateDate(FBE.ToDate) + "',106)";
                DataTable DT = GF.GetDataTableFromSP(SQL);
                if (DT.Rows[0][0].ToString() == "0")
                {
                    BBE.JobList = PS.GetMonthlyLoadBillEntry(FBE);
                    ViewBag.MonthlyDone = "0";
                }
                else
                {
                    BBE.JobList = new List<JOBBillEntryDetails>();
                    ViewBag.MonthlyDone = "1";
                }
            }
            if (Id == "10")
            {
                BBE.PRSDRSList = PS.GetPRSDRSAttachedVendorBillEntry(FBE, Id);
            }
            if (Id == "11")
            {

                DateTime now = Convert.ToDateTime(FBE.MonthWithYear);
                FBE.FromDate = new DateTime(now.Year, now.Month, 1);
                FBE.ToDate = FBE.FromDate.AddMonths(1).AddDays(-1);
                BBE.WVH.BillGENMonth = Convert.ToDateTime(FBE.MonthWithYear).ToString("MMM yyyy");
                string SQL = "SELECT COUNT(*) AS CNT FROM WEBX_VENDORBILL_HDR WHERE betype='Monthly Attached' AND VENDORCODE='" + BBE.WVH.VENDORCODE + "' AND ISNULL(bill_cancel,'N')='N' ";
                SQL = SQL + " AND ISNULL(BillGENMonth,'') = '" + BBE.WVH.BillGENMonth + "'  AND BRCD='" + FBE.Branch + "'";
                //SQL = SQL + " AND CONVERT(VARCHAR,BILLDT,106) BETWEEN CONVERT(DATETIME,'" + GF.FormateDate(FBE.FromDate) + "',106) AND CONVERT(DATETIME,'" + GF.FormateDate(FBE.ToDate) + "',106)";
                DataTable DT = GF.GetDataTableFromSP(SQL);

                if (DT.Rows[0][0].ToString() == "0")
                {
                    BBE.PRSDRSList = PS.GetPRSDRSAttachedVendorBillEntry(FBE, Id);
                    ViewBag.MonthlyDone = "0";
                }
                else
                {
                    BBE.PRSDRSList = new List<webx_pdchdr>();
                    ViewBag.MonthlyDone = "1";
                }

            }

            if (Id == "13")
            {
                BBE.ConnectivityTHCList = PS.GetVendorWiseTHCforConnectivityBillEntry(FBE);
            }

            if (Id == "1")
            {
                ViewBag.ModuleName = "Expense Bill Entry - Fuel";
            }
            else if (Id == "2")
            {
                ViewBag.ModuleName = "Fuel Slip Bill Entry - TRIP/THC";
            }
            else if (Id == "3")
            {
                ViewBag.ModuleName = "Expense Bill Entry - Fixed & Variable";
            }
            else if (Id == "4")
            {
                ViewBag.ModuleName = "Other Bill Entry";
            }
            else if (Id == "5")
            {
                ViewBag.ModuleName = "PO Bill Entry";
            }
            else if (Id == "6")
            {
                ViewBag.ModuleName = "Job Order Bill Entry";
            }
            else if (Id == "7")
            {
                ViewBag.ModuleName = "GRN Bill Entry";
            }
            else if (Id == "8")
            {
                ViewBag.ModuleName = "Vendor Loading-Unloading Bill Entry";
            }
            else if (Id == "9")
            {
                ViewBag.ModuleName = "Monthly Vendor Loading-Unloading Bill Entry";
            }
            else if (Id == "10")
            {
                if (FBE.BillType == "XX1")
                {
                    ViewBag.ModuleName = "Attached Vendor Bill Entry";
                }
            }
            else if (Id == "11")
            {
                if (FBE.BillType == "XX1")
                {
                    ViewBag.ModuleName = "Attached Vendor Monthly Bill Entry";
                }
            }
            else if (Id == "13")
            {
                ViewBag.ModuleName = "Connectivity Charge(THC) Bill Entry";
            }

            BBE.WVH.VENDORCODE = BEC.VendorCode;
            List<webx_VENDOR_HDR> WVHList = new List<webx_VENDOR_HDR>();

            if (BEC.VendorCode != null && BEC.VendorCode != "")
            {
                WVHList = MS.GetVendorObject().Where(c => c.VENDORCODE.ToUpper() == BEC.VendorCode.Split(':')[0].Trim().ToUpper()).ToList();
                BBE.WVH.VENDORTYPE = WVHList.FirstOrDefault().Vendor_Type;
                BBE.WVH.VENDORNAME = WVHList.FirstOrDefault().VENDORNAME;
                BBE.WVHList = WVHList;


                BBE.GSTType = BEC.GSTType;
                BBE.WVH.StateCode = BEC.StateCode;
                BBE.WVH.GSTType = BEC.GSTType;
                BBE.WVH.GSTPercentage = BEC.GSTPercentage;
                BBE.WVH.isGSTReverse = FBE.isGSTReverse;

                if (Id == "7")
                {
                    BBE.GSTType = FBE1.GSTType;
                    BBE.WVH.StateCode = FBE1.StateCode;
                    BBE.WVH.GSTType = FBE1.GSTType;
                    BBE.WVH.isGSTReverse = FBE1.isGSTReverse;
                    if (BBE.GRNList != null && BBE.GRNList.Count > 0)
                    {
                        BBE.WVH.GSTPercentage = BBE.GRNList.FirstOrDefault().GSTPercentage;
                    }
                }
            }

            BBE.TripsheetList = new List<BABillEntyTripSheet>();
            BBE.LedgerList = new List<OtherBillEntryLedger>();

            BABillEntyTripSheet BBET = new BABillEntyTripSheet();
            BBET.SRNO = 1;
            BBE.TripsheetList.Add(BBET);

            OtherBillEntryLedger OBEL = new OtherBillEntryLedger();
            OBEL.Id = 1;
            BBE.LedgerList.Add(OBEL);
            return View(BBE);
        }

        public ActionResult FuelBillEntrySubmit(string Id, FuelBillEntyQuery FBE, ExpenseBillEntryQuery EBE, BABillEnty BBE, StaxTDSViewModel STVM,
         List<FuelSlipBillEntyTripSheet> FuelTripsheetList, List<POBillEntry> POList, List<OtherBillEntryLedger> LedgerList, List<BABillEntyTripSheet> TripsheetList,
          List<JOBBillEntryDetails> JOBList, List<GRNBillEntryDetails> POGRN, List<CygnusChargesHeader> DynamicList, List<webx_pdchdr> PDCList, List<webx_THC_SUMMARY> THCList, List<ConnectivityBillEntryDetails> ConnectivityTHCList
            , HttpPostedFileBase[] files)
        {
            string Voucherno = "";
            try
            {
                string extension = "", UserFileName = "", Documentpath = "";
                #region   Other Bill Images Upload
                if (BBE.BillEntryType == "4" || BBE.BillEntryType == "5")
                {
                    try
                    {

                        if (((System.Web.HttpPostedFileBase[])(files)) != null)
                        {
                            foreach (var fileobj in files)
                            {
                                var file = ((System.Web.HttpPostedFileBase[])(files));
                                if (fileobj.ContentLength > 0)
                                {
                                    string directory = "";
                                    if (BBE.BillEntryType == "4")
                                    {
                                        directory = @"D:\SRLApplicationCode\POD\OtherBillEntry\";
                                    }
                                    else
                                    {
                                        directory = @"D:\SRLApplicationCode\POD\PurchaseOrdeBillEntry\";
                                    }
                                    extension = System.IO.Path.GetExtension(fileobj.FileName);
                                    //UserFileName = BaseUserName + extension;
                                    UserFileName = BaseUserName + "_" + fileobj.FileName;
                                    //    Documentpath = Server.MapPath("D:/SRLApplicationCode/POD/OtherBillEntry/") + UserFileName;
                                    //  string strDirectoryName = Server.MapPath("D:/SRLApplicationCode/POD/OtherBillEntry/");

                                    fileobj.SaveAs(Path.Combine(directory, UserFileName));

                                    if (Directory.Exists(directory) == false)
                                        Directory.CreateDirectory(directory);
                                    // fileobj.SaveAs(Documentpath);

                                }

                            }
                        }

                    }
                    catch (Exception)
                    {
                        // DocumentUploadedPath = Em.Elec_Expense_DET.b;
                    }
                }

                #endregion

                decimal TOTPOPendAmt = 0;
                string Xml_PAY_Det = "<root>";
                if (BBE.BillEntryType == "1")
                {
                    foreach (var itm in TripsheetList)
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<ManualTripsheetNo>" + itm.ManualTripsheetNo + "</ManualTripsheetNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TripsheetNo>" + itm.TripsheetNo + "</TripsheetNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Location>" + itm.Location + "</Location>";
                        Xml_PAY_Det = Xml_PAY_Det + "<FuelType>" + itm.FuelType + "</FuelType>";
                        Xml_PAY_Det = Xml_PAY_Det + "<LastKM>" + itm.LastKM + "</LastKM>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CurrentKM>" + itm.CurrentKM + "</CurrentKM>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SlipNo>" + itm.SlipNo + "</SlipNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<SlipDate>" + GF.FormateDate(itm.SlipDate) + "</SlipDate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DisealQty>" + itm.DisealQty + "</DisealQty>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DisealRate>" + itm.DisealRate + "</DisealRate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DisealAmount>" + itm.DisealAmount + "</DisealAmount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<ApprovedDisealAmount>" + itm.ApprovedDisealAmount + "</ApprovedDisealAmount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }
                }
                else if (BBE.BillEntryType == "2")
                {
                    foreach (var itm in FuelTripsheetList.ToList().Where(c => c.IsEnabled))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<FUELSLIPNO>" + itm.FUELSLIPNO + "</FUELSLIPNO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TRIPSHEETNO>" + itm.TRIPSHEETNO + "</TRIPSHEETNO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<VSlipDt>" + itm.VSlipDt + "</VSlipDt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<QUANTITYINLITER>" + itm.QUANTITYINLITER + "</QUANTITYINLITER>";
                        Xml_PAY_Det = Xml_PAY_Det + "<RATE>" + itm.RATE + "</RATE>";
                        Xml_PAY_Det = Xml_PAY_Det + "<AMOUNT>" + itm.AMOUNT + "</AMOUNT>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }
                }
                else if (BBE.BillEntryType == "3")
                {

                }
                else if (BBE.BillEntryType == "4")
                {
                    foreach (var itm in LedgerList)
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Acccode>" + itm.Acccode + "</Acccode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<AccDesc>" + itm.AccDesc + "</AccDesc>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Amount>" + itm.Amount + "</Amount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Narration>" + itm.Narration + "</Narration>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }

                    /* START GST Changes On 22 June 2017 By Chirag D */


                    /* END GST Changes On 22 June 2017 By Chirag D */
                }
                else if (BBE.BillEntryType == "5")
                {
                    decimal totAdvanceAmount = 0;
                    foreach (var itm in POList.ToList().Where(c => c.IsEnabled))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<pocode>" + itm.pocode + "</pocode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<podt>" + itm.podt + "</podt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Adv_Voucherno>" + itm.Adv_Voucherno + "</Adv_Voucherno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Adv_Voucherdt>" + itm.Adv_Voucherdt + "</Adv_Voucherdt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PaidAmt>" + itm.PaidAmt + "</PaidAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Totalamt>" + itm.Totalamt + "</Totalamt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + itm.PendAmt + "</PendAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<BillAMount>" + itm.BillAMount + "</BillAMount>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CreditLedger>" + BBE.CreditLedger + "</CreditLedger>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CreditLedgerName>" + BBE.CreditLedgerName + "</CreditLedgerName>";
                        Xml_PAY_Det = Xml_PAY_Det + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        TOTPOPendAmt = TOTPOPendAmt + itm.PendAmt;
                        totAdvanceAmount = totAdvanceAmount + itm.PaidAmt;
                    }
                    STVM.StaxOnAmount = totAdvanceAmount;
                    BBE.WVH.NETAMT = Convert.ToDouble(totAdvanceAmount + TOTPOPendAmt);
                }
                else if (BBE.BillEntryType == "6")
                {
                    foreach (var itm in JOBList.ToList().Where(c => c.IsEnabled))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<JOBNO>" + itm.JOB_ORDER_NO + "</JOBNO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<TOT_Part_GST_COST>" + itm.TOT_Part_GST_COST + "</TOT_Part_GST_COST>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }
                }
                else if (BBE.BillEntryType == "7")
                {
                    BBE.CreditLedger = "UNS0004";
                    BBE.CreditLedgerName = "SUNDRY CREDITORS - OTHERS";
                    foreach (var itm in POGRN.ToList().Where(c => c.IsChecked))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<pocode>" + itm.GRNNO + "</pocode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Adv_Voucherno>" + itm.Adv_Voucherno + "</Adv_Voucherno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Adv_Voucherdt>" + itm.Adv_Voucherdt + "</Adv_Voucherdt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PaidAmt>" + itm.PaidAmt + "</PaidAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Totalamt>" + itm.totalamt + "</Totalamt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + itm.pendamt + "</PendAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<BillAMount>" + itm.GRNAmt + "</BillAMount>";
                        // Xml_PAY_Det = Xml_PAY_Det + "<Deduction>" + itm.Deduction + "</Deduction>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CreditLedger>" + itm.CreditLedger + "</CreditLedger>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CreditLedgerName>" + itm.CreditLedgerName + "</CreditLedgerName>";
                        Xml_PAY_Det = Xml_PAY_Det + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        TOTPOPendAmt = TOTPOPendAmt + itm.pendamt;
                    }
                }
                //Loading Query -Unloading
                else if (BBE.BillEntryType == "8" || BBE.BillEntryType == "9")
                {
                    foreach (var itm in JOBList.ToList().Where(c => c.IsEnabled))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<NO>" + itm.NO + "</NO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<date>" + itm.date + "</date>";
                        Xml_PAY_Det = Xml_PAY_Det + "<LoadBY>" + itm.LoadBY + "</LoadBY>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Name>" + itm.Name + "</Name>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Rate>" + itm.Rate + "</Rate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Acccode>" + BBE.DebitLedger + "</Acccode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Accdesc>" + BBE.DebitLedgerName + "</Accdesc>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Charge>" + itm.Charge + "</Charge>";
                        if (itm.Status.ToUpper() == "LOADING")
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<LUType>L</LUType>";
                        }
                        else if (itm.Status.ToUpper() == "UNLOADING")
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<LUType>U</LUType>";
                        }
                        else
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<LUType>M</LUType>";
                        }

                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        TOTPOPendAmt = TOTPOPendAmt + itm.Charge;
                    }
                    if (BBE.BillEntryType == "9")
                    {
                        TOTPOPendAmt = STVM.StaxOnAmount;
                    }
                }

                else if (BBE.BillEntryType == "10" || BBE.BillEntryType == "11")
                {
                    foreach (var itm in PDCList.ToList())
                    {

                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<NO>" + itm.NO + "</NO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PDCTY>" + itm.Typ + "</PDCTY>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DocType>" + FBE.DocType + "</DocType>";
                        Xml_PAY_Det = Xml_PAY_Det + "<vendorcode>" + itm.vendorcode + "</vendorcode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<contract_amt>" + itm.contract_amt + "</contract_amt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Acccode>" + BBE.DebitLedger + "</Acccode>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Accdesc>" + BBE.DebitLedgerName + "</Accdesc>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }
                }
                else if (BBE.BillEntryType == "13")
                {
                    foreach (var itm in ConnectivityTHCList.Where(c => c.IsEnabled).ToList())
                    {

                        Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                        Xml_PAY_Det = Xml_PAY_Det + "<DOCKNO>" + itm.DocNo + "</DOCKNO>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PCAMT>" + itm.ConnectivityCharge + "</PCAMT>";
                        Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                    }
                }

                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                string Xml_PAY_Mst = "<root>";
                // string[] Vendor_Arr = BBEQ.VendorCode;

                if (BBE.BillEntryType == "7")
                {
                    if (BBE.WVH.VENDORCODE != null || BBE.WVH.VENDORCODE != "")
                    {
                        BBE.WVH.VENDORCODE = FBE.VendorCode.Split(':')[0];
                        BBE.WVH.VENDORNAME = FBE.VendorCode.Split(':')[1];
                    }

                    string SQRY_Vend = "select COUNT(*) from webx_VENDOR_HDR where VENDORCODE='" + BBE.WVH.VENDORCODE + "'";
                    DataTable DT_Vend = GF.GetDataTableFromSP(SQRY_Vend);
                    int Count_Vend = Convert.ToInt32(DT_Vend.Rows[0][0].ToString());

                    if (Count_Vend != 1)
                    {
                        @ViewBag.StrError = "Please Select Proper Vendor.";
                        return View("Error");
                    }
                }
                else
                {
                    if (BBE.WVH.VENDORCODE == null || BBE.WVH.VENDORCODE == "")
                        BBE.WVH.VENDORCODE = FBE.VendorCode;
                }
                var baseBranch = BaseLocationCode;
                if (BBE.BillEntryType == "8" || BBE.BillEntryType == "9" || BBE.BillEntryType == "10" || BBE.BillEntryType == "11")
                {
                    baseBranch = FBE.Branch.ToString().ToUpper();
                }

                Xml_PAY_Mst = Xml_PAY_Mst + "<MST><BILLDT>" + BBE.WVH.BILLDT.ToString("dd MMM yyyy") + "</BILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BRCD>" + baseBranch + "</BRCD>";
                // Xml_PAY_Mst = Xml_PAY_Mst + "<BKG_DLY>" + BBEQ.BookingDelivery + "</BKG_DLY>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORCODE>" + BBE.WVH.VENDORCODE + "</VENDORCODE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORNAME>" + BBE.WVH.VENDORNAME + "</VENDORNAME>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLDT>" + BBE.WVH.VENDORBILLDT.ToString("dd MMM yyyy") + "</VENDORBILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLNO>" + BBE.WVH.VENDORBILLNO + "</VENDORBILLNO>";
                //Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + BBE.WVH.ContactCommission1 + "</PCAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REFNO>" + BBE.WVH.REFNO + "</REFNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + STVM.StaxOnAmount + "</PCAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DUEDT>" + BBE.WVH.DUEDT.ToString("dd MMM yyyy") + "</DUEDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<OTHAMT>" + STVM.OtherAmountPlus + "</OTHAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<OTHERDED>" + STVM.OtherAmountLess + "</OTHERDED>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SVCTAX>" + STVM.StaxRateAmount + "</SVCTAX>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SVCTAXRate>" + STVM.StaxRate + "</SVCTAXRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<cessamt>" + STVM.CessAmount + "</cessamt>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Hedu_cess>" + STVM.HCessAmount + "</Hedu_cess>";
                // Xml_PAY_Mst = Xml_PAY_Mst + "<SBAmount>" + STVM.SBAmount + "</SBAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDSRATE>" + STVM.TDSRate + "</TDSRATE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDS>" + STVM.TDSAmount + "</TDS>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsacccode>" + STVM.TDSAcccode + "</tdsacccode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsaccdesc>" + STVM.TDSAccdesc + "</tdsaccdesc>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<NETAMT>" + BBE.WVH.NETAMT + "</NETAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<STaxRegNo>" + STVM.StaxRegNo + "</STaxRegNo>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PANNO>" + STVM.PANNO + "</PANNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VATRate>" + STVM.VATRate + "</VATRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VATAmount>" + STVM.VATAmount + "</VATAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<CreditLedger>" + BBE.CreditLedger + "</CreditLedger>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<CreditLedgerName>" + BBE.CreditLedgerName + "</CreditLedgerName>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PendAmount>" + TOTPOPendAmt + "</PendAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SBAmount>" + STVM.SBAmount + "</SBAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SBRate>" + STVM.SBRate + "</SBRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<KKCAmount>" + STVM.KKCAmount + "</KKCAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<KKCRate>" + STVM.KKCRate + "</KKCRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Discount>" + STVM.Discount + "</Discount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DiscountType>" + STVM.DiscountType + "</DiscountType>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BillGENMonth>" + BBE.WVH.BillGENMonth + "</BillGENMonth>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<StateCode>" + BBE.WVH.StateCode + "</StateCode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<GSTType>" + BBE.WVH.GSTType + "</GSTType>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<isGSTReverse>" + BBE.WVH.isGSTReverse + "</isGSTReverse>";

                if (BBE.BillEntryType == "6")
                {
                    Xml_PAY_Mst = Xml_PAY_Mst + "<CGST>" + BBE.WVH.PCGSTAmount + "</CGST>";
                    Xml_PAY_Mst = Xml_PAY_Mst + "<SGST>" + BBE.WVH.PSGSTAmount + "</SGST>";
                    Xml_PAY_Mst = Xml_PAY_Mst + "<UTGST>" + BBE.WVH.PUTGSTAmount + "</UTGST>";
                    Xml_PAY_Mst = Xml_PAY_Mst + "<IGST>" + BBE.WVH.PIGSTAmount + "</IGST>";
                }
                if (BBE.BillEntryType == "4" || BBE.BillEntryType == "5")
                {
                    Xml_PAY_Mst = Xml_PAY_Mst + "<UserFileName>" + UserFileName + "</UserFileName>";
                }
                Xml_PAY_Mst = Xml_PAY_Mst + "</MST>";
                Xml_PAY_Mst = Xml_PAY_Mst + "</root>";

                Xml_PAY_Mst = Xml_PAY_Mst.Replace("&", "&amp;");
                Xml_PAY_Det = Xml_PAY_Det.Replace("&", "&amp;");

                XmlDocument xmlDocGSTCha = new XmlDocument();
                if (DynamicList != null)
                {
                    XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                    using (MemoryStream xmlStreamCha = new MemoryStream())
                    {
                        xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                        xmlStreamCha.Position = 0;
                        xmlDocGSTCha.Load(xmlStreamCha);
                    }
                }
                else
                {
                    xmlDocGSTCha.InnerXml = "<root></root>";
                }

                DataTable DT = PS.Insert_BillEntry(Xml_PAY_Mst, Xml_PAY_Det, xmlDocGSTCha.InnerXml, BBE.BillEntryType, baseBranch, BaseYearVal, BaseFinYear.Substring(0, 4), BaseUserName.ToUpper());
                Voucherno = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BABillEntryDone", new { Voucherno = Voucherno, Type = BBE.BillEntryType });
        }

        public ActionResult ExpenseFixedVariableBillEntry(BillEntryCriteria bEC)
        {
            BABillEnty BBE = new BABillEnty();
            MasterService MS = new MasterService();
            WEBX_VENDORBILL_HDR p_WVH = new WEBX_VENDORBILL_HDR();
            BBE.BEC = bEC;
            BBE.BillEntryType = bEC.FilterType;
            string mExpenseType = "";
            if (bEC.ExpenseType == "F")
            {
                mExpenseType = "Fixed";
            }
            else
            {
                mExpenseType = "Variable";
            }
            BBE.WVH = p_WVH;
            BBE.WVH.betype = mExpenseType + " Expense";
            BBE.WVH.VENDORCODE = bEC.VendorCode;
            List<webx_VENDOR_HDR> WVHList = new List<webx_VENDOR_HDR>();
            if (bEC.VendorCode != null && bEC.VendorCode != "")
            {
                WVHList = MS.GetVendorObject().Where(c => c.VENDORCODE.ToUpper() == bEC.VendorCode.Split(':')[0].Trim().ToUpper()).ToList();
                BBE.WVH.VENDORTYPE = WVHList.FirstOrDefault().Vendor_Type;
                BBE.WVH.VENDORNAME = WVHList.FirstOrDefault().VENDORNAME;
                BBE.WVHList = WVHList;


                BBE.GSTType = bEC.GSTType;
                BBE.WVH.StateCode = bEC.StateCode;
                BBE.WVH.GSTType = bEC.GSTType;
                BBE.WVH.GSTPercentage = bEC.GSTPercentage;
                BBE.WVH.isGSTReverse = false;

            }
            List<webx_fleet_VoucherExp_DET> expenseList = new List<webx_fleet_VoucherExp_DET>();
            //BBE.StatutaryExpenseList = expenseList;
            //BBE.CrewSalaryExpenseList = expenseList;
            //BBE.VehicleTrackingServiceExpenseList = expenseList;
            //BBE.FinancialExpenseList= expenseList;
            //BBE.RepairMainteneceExpenseList = expenseList;

            #region StatutoryExpens
            if (bEC.StatutoryExpens)
            {
                List<Webx_Master_General> mstStatutaryExpense = new List<Webx_Master_General>();
                string[] arr_STE = new string[] { "01", "02", "03", "04", "05", "06" };
                mstStatutaryExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_STE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();
                BBE.StatutaryExpenseList = new List<webx_fleet_VoucherExp_DET>();
              
                webx_fleet_VoucherExp_DET expSTE = new webx_fleet_VoucherExp_DET();
                expSTE.SRNO = 1;
                expSTE.ListNatureOfExpense = mstStatutaryExpense;
                expSTE.VoucherNo = string.Empty;
                expSTE.ExpenseId = string.Empty;
                expSTE.Amt = Convert.ToDecimal(0.0);
                expSTE.ExeAmt = Convert.ToDecimal(0.0);
                expSTE.BillNo = string.Empty;
                expSTE.Remarks = string.Empty;
                expSTE.VehicleNo = string.Empty;

                BBE.StatutaryExpenseList.Add(expSTE);


            }
            #endregion StatutoryExpens

            #region CrewSalaryExpense
            if (bEC.CrewSalaryExpense)
            {
                List<Webx_Master_General> mstCrewSalaryExpense = new List<Webx_Master_General>();
                string[] arr_CSE = new string[] { "07", "08", "09" };
                mstCrewSalaryExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_CSE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();
                BBE.CrewSalaryExpenseList = new List<webx_fleet_VoucherExp_DET>(); ;

                webx_fleet_VoucherExp_DET expCSE = new webx_fleet_VoucherExp_DET();
                expCSE.SRNO = 1;
                expCSE.ListNatureOfExpense = mstCrewSalaryExpense;
                expCSE.VoucherNo = string.Empty;
                expCSE.ExpenseId = string.Empty;
                expCSE.Amt = Convert.ToDecimal(0.0);
                expCSE.ExeAmt = Convert.ToDecimal(0.0);
                expCSE.BillNo = string.Empty;
                expCSE.Remarks = string.Empty;
                expCSE.VehicleNo = string.Empty;

                BBE.CrewSalaryExpenseList.Add(expCSE);


            }
            #endregion CrewSalaryExpense

            #region VehicleTrackingExpense
            if (bEC.VehicleTrackingServiceExpense)
            {
                List<Webx_Master_General> mstVehicleTrackingExpense = new List<Webx_Master_General>();

                mstVehicleTrackingExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && x.CodeId == "10" && x.StatusCode.ToUpper() == "Y").ToList();
                BBE.VehicleTrackingServiceExpenseList = new List<webx_fleet_VoucherExp_DET>();

                webx_fleet_VoucherExp_DET expVTE = new webx_fleet_VoucherExp_DET();
                expVTE.SRNO = 1;
                expVTE.ListNatureOfExpense = mstVehicleTrackingExpense;
                expVTE.VoucherNo = string.Empty;
                expVTE.ExpenseId = string.Empty;
                expVTE.Amt = Convert.ToDecimal(0.0);
                expVTE.ExeAmt = Convert.ToDecimal(0.0);
                expVTE.BillNo = string.Empty;
                expVTE.Remarks = string.Empty;
                expVTE.VehicleNo = string.Empty;

                BBE.VehicleTrackingServiceExpenseList.Add(expVTE);

            }
            #endregion VehicleTrackingExpense

            #region FinancialExpense
            if (bEC.FinancialExpense)
            {
                List<Webx_Master_General> mstFinancialExpense = new List<Webx_Master_General>();
                string[] arr_FE = new string[] { "11", "12" };
                mstFinancialExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_FE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();
                BBE.FinancialExpenseList = new List<webx_fleet_VoucherExp_DET>();

                webx_fleet_VoucherExp_DET expFE = new webx_fleet_VoucherExp_DET();
                expFE.SRNO = 1;
                expFE.ListNatureOfExpense = mstFinancialExpense;
                expFE.VoucherNo = string.Empty;
                expFE.ExpenseId = string.Empty;
                expFE.Amt = Convert.ToDecimal(0.0);
                expFE.ExeAmt = Convert.ToDecimal(0.0);
                expFE.BillNo = string.Empty;
                expFE.Remarks = string.Empty;
                expFE.VehicleNo = string.Empty;

                BBE.FinancialExpenseList.Add(expFE);

            }
            #endregion FinancialExpense

            #region RepairExpense
            if (bEC.RepairMaintenanceExpense)
            {
                List<Webx_Master_General> mstRepairExpense = new List<Webx_Master_General>();
                mstRepairExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "VEXP" && x.StatusCode.ToUpper() == "Y").ToList();
                BBE.RepairMainteneceExpenseList = new List<webx_fleet_VoucherExp_DET>();

                webx_fleet_VoucherExp_DET expFE = new webx_fleet_VoucherExp_DET();
                expFE.SRNO = 1;
                expFE.ListNatureOfExpense = mstRepairExpense;
                expFE.VoucherNo = string.Empty;
                expFE.ExpenseId = string.Empty;
                expFE.Amt = Convert.ToDecimal(0.0);
                expFE.ExeAmt = Convert.ToDecimal(0.0);
                expFE.BillNo = string.Empty;
                expFE.Remarks = string.Empty;
                expFE.VehicleNo = string.Empty;

                BBE.RepairMainteneceExpenseList.Add(expFE);

            }
            #endregion RepairExpense

            return View(BBE);
        }
        public ActionResult ExpenseFixedVariableBillEntrySubmit(BABillEnty BBE, StaxTDSViewModel STVM, List<webx_fleet_VoucherExp_DET> StatutaryExpensList,
            List<webx_fleet_VoucherExp_DET> CrewSalaryExpensList, List<webx_fleet_VoucherExp_DET> VehickeTrackingExpenseList, List<webx_fleet_VoucherExp_DET> FinancialExpensList,
            List<webx_fleet_VoucherExp_DET> RepairExpenseList, List<CygnusChargesHeader> DynamicList)
        {
            string Voucherno = "";
            try
            {
                string UserFileName = "";

                decimal TOTPOPendAmt = 0;
                string Xml_PAY_Det = "<root>";
                if (BBE.BEC.StatutoryExpens)
                {
                    foreach (var itm in StatutaryExpensList)
                    {
                        if (!string.IsNullOrEmpty(itm.ExpenseId))
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExpenseId>" + itm.ExpenseId + "</ExpenseId>";
                            Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Amt>" + itm.Amt + "</Amt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExeAmt>" + itm.ExeAmt + "</ExeAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<BillNo>" + itm.BillNo + "</BillNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                            Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        }
                    }
                }
                if (BBE.BEC.CrewSalaryExpense)
                {
                    foreach (var itm in CrewSalaryExpensList)
                    {
                        if (!string.IsNullOrEmpty(itm.ExpenseId))
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExpenseId>" + itm.ExpenseId + "</ExpenseId>";
                            Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Amt>" + itm.Amt + "</Amt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExeAmt>" + itm.ExeAmt + "</ExeAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<BillNo>" + itm.BillNo + "</BillNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                            Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        }
                    }
                }
                if (BBE.BEC.VehicleTrackingServiceExpense)
                {
                    foreach (var itm in VehickeTrackingExpenseList)
                    {
                        if (!string.IsNullOrEmpty(itm.ExpenseId))
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExpenseId>" + itm.ExpenseId + "</ExpenseId>";
                            Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Amt>" + itm.Amt + "</Amt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExeAmt>" + itm.ExeAmt + "</ExeAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<BillNo>" + itm.BillNo + "</BillNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                            Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        }
                    }
                }
                if (BBE.BEC.FinancialExpense)
                {
                    foreach (var itm in FinancialExpensList)
                    {
                        if (!string.IsNullOrEmpty(itm.ExpenseId))
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExpenseId>" + itm.ExpenseId + "</ExpenseId>";
                            Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Amt>" + itm.Amt + "</Amt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExeAmt>" + itm.ExeAmt + "</ExeAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<BillNo>" + itm.BillNo + "</BillNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                            Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        }
                    }
                }
                if (BBE.BEC.RepairMaintenanceExpense)
                {
                    foreach (var itm in RepairExpenseList)
                    {
                        if (!string.IsNullOrEmpty(itm.ExpenseId))
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<DET>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExpenseId>" + itm.ExpenseId + "</ExpenseId>";
                            Xml_PAY_Det = Xml_PAY_Det + "<VehicleNo>" + itm.VehicleNo + "</VehicleNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Amt>" + itm.Amt + "</Amt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<ExeAmt>" + itm.ExeAmt + "</ExeAmt>";
                            Xml_PAY_Det = Xml_PAY_Det + "<BillNo>" + itm.BillNo + "</BillNo>";
                            Xml_PAY_Det = Xml_PAY_Det + "<Remarks>" + itm.Remarks + "</Remarks>";
                            Xml_PAY_Det = Xml_PAY_Det + "</DET>";
                        }
                    }
                }

                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                string Xml_PAY_Mst = "<root>";
                // string[] Vendor_Arr = BBEQ.VendorCode;


                if (BBE.WVH.VENDORCODE == null || BBE.WVH.VENDORCODE == "")
                    BBE.WVH.VENDORCODE = BBE.BEC.VendorCode;

                var baseBranch = BaseLocationCode;

                Xml_PAY_Mst = Xml_PAY_Mst + "<MST><BILLDT>" + BBE.WVH.BILLDT.ToString("dd MMM yyyy") + "</BILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BRCD>" + baseBranch + "</BRCD>";
                // Xml_PAY_Mst = Xml_PAY_Mst + "<BKG_DLY>" + BBEQ.BookingDelivery + "</BKG_DLY>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORCODE>" + BBE.WVH.VENDORCODE + "</VENDORCODE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORNAME>" + BBE.WVH.VENDORNAME + "</VENDORNAME>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLDT>" + BBE.WVH.VENDORBILLDT.ToString("dd MMM yyyy") + "</VENDORBILLDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VENDORBILLNO>" + BBE.WVH.VENDORBILLNO + "</VENDORBILLNO>";
                //Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + BBE.WVH.ContactCommission1 + "</PCAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REFNO>" + BBE.WVH.REFNO + "</REFNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<betype>" + BBE.WVH.betype + "</betype>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PCAMT>" + STVM.StaxOnAmount + "</PCAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DUEDT>" + BBE.WVH.DUEDT.ToString("dd MMM yyyy") + "</DUEDT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<OTHAMT>" + STVM.OtherAmountPlus + "</OTHAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<OTHERDED>" + STVM.OtherAmountLess + "</OTHERDED>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SVCTAX>" + STVM.StaxRateAmount + "</SVCTAX>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SVCTAXRate>" + STVM.StaxRate + "</SVCTAXRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<cessamt>" + STVM.CessAmount + "</cessamt>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Hedu_cess>" + STVM.HCessAmount + "</Hedu_cess>";
                // Xml_PAY_Mst = Xml_PAY_Mst + "<SBAmount>" + STVM.SBAmount + "</SBAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDSRATE>" + STVM.TDSRate + "</TDSRATE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<TDS>" + STVM.TDSAmount + "</TDS>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsacccode>" + STVM.TDSAcccode + "</tdsacccode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<tdsaccdesc>" + STVM.TDSAccdesc + "</tdsaccdesc>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<NETAMT>" + BBE.WVH.NETAMT + "</NETAMT>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<ENTRYBY>" + BaseUserName.ToUpper() + "</ENTRYBY>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<STaxRegNo>" + STVM.StaxRegNo + "</STaxRegNo>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PANNO>" + STVM.PANNO + "</PANNO>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<REMARK>" + BBE.WVH.REMARK + "</REMARK>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VATRate>" + STVM.VATRate + "</VATRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<VATAmount>" + STVM.VATAmount + "</VATAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<CreditLedger>" + BBE.CreditLedger + "</CreditLedger>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<CreditLedgerName>" + BBE.CreditLedgerName + "</CreditLedgerName>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<PendAmount>" + TOTPOPendAmt + "</PendAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SBAmount>" + STVM.SBAmount + "</SBAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<SBRate>" + STVM.SBRate + "</SBRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<KKCAmount>" + STVM.KKCAmount + "</KKCAmount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<KKCRate>" + STVM.KKCRate + "</KKCRate>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<Discount>" + STVM.Discount + "</Discount>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<DiscountType>" + STVM.DiscountType + "</DiscountType>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<BillGENMonth>" + BBE.WVH.BillGENMonth + "</BillGENMonth>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<StateCode>" + BBE.WVH.StateCode + "</StateCode>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<GSTType>" + BBE.WVH.GSTType + "</GSTType>";
                Xml_PAY_Mst = Xml_PAY_Mst + "<isGSTReverse>" + BBE.WVH.isGSTReverse + "</isGSTReverse>";

                Xml_PAY_Mst = Xml_PAY_Mst + "</MST>";
                Xml_PAY_Mst = Xml_PAY_Mst + "</root>";

                Xml_PAY_Mst = Xml_PAY_Mst.Replace("&", "&amp;");
                Xml_PAY_Det = Xml_PAY_Det.Replace("&", "&amp;");

                XmlDocument xmlDocGSTCha = new XmlDocument();
                if (DynamicList != null)
                {
                    XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                    using (MemoryStream xmlStreamCha = new MemoryStream())
                    {
                        xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                        xmlStreamCha.Position = 0;
                        xmlDocGSTCha.Load(xmlStreamCha);
                    }
                }
                else
                {
                    xmlDocGSTCha.InnerXml = "<root></root>";
                }

                DataTable DT = PS.Insert_FixedVariableBillEntry(Xml_PAY_Mst, Xml_PAY_Det, xmlDocGSTCha.InnerXml, baseBranch, BaseFinYear.Substring(0, 4), BaseYearVal, BaseUserName.ToUpper(), BBE.WVH.betype);
                Voucherno = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BABillEntryDone", new { Voucherno = Voucherno, Type = BBE.BillEntryType });
        }
        public ActionResult FuelBillEntryTriplsheet(int id)
        {
            BABillEntyTripSheet WMDI = new BABillEntyTripSheet();
            WMDI.SRNO = id;
            return PartialView("_FuelBillEntryTriplsheet", WMDI);
        }

        public ActionResult OtherBillEntryLedger(int id)
        {
            OtherBillEntryLedger WMDI = new OtherBillEntryLedger();
            WMDI.Id = id;
            return PartialView("_OtherBillEntryLedger", WMDI);
        }

        public ActionResult StatutaryExpense(int id)
        {
            webx_fleet_VoucherExp_DET WFVD = new webx_fleet_VoucherExp_DET();
            string[] arr_CSE = new string[] { "01", "02", "03", "04", "05", "06" };
            List<Webx_Master_General> mstStatutaryExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_CSE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();

            WFVD.SRNO = id;
            WFVD.ListNatureOfExpense = mstStatutaryExpense;
            WFVD.VoucherNo = string.Empty;
            WFVD.ExpenseId = string.Empty;
            WFVD.Amt = Convert.ToDecimal(0.0);
            WFVD.ExeAmt = Convert.ToDecimal(0.0);
            WFVD.BillNo = string.Empty;
            WFVD.Remarks = string.Empty;
            WFVD.VehicleNo = string.Empty;
            return PartialView("_StatutaryExpense", WFVD);
        }

        public ActionResult CrewSalaryExpense(int id)
        {
            webx_fleet_VoucherExp_DET WFVD = new webx_fleet_VoucherExp_DET();
            string[] arr_CSE = new string[] { "07", "08", "09" };
            List<Webx_Master_General> mstCrewSalaryExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_CSE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();

            WFVD.SRNO = id;
            WFVD.ListNatureOfExpense = mstCrewSalaryExpense;
            WFVD.VoucherNo = string.Empty;
            WFVD.ExpenseId = string.Empty;
            WFVD.Amt = Convert.ToDecimal(0.0);
            WFVD.ExeAmt = Convert.ToDecimal(0.0);
            WFVD.BillNo = string.Empty;
            WFVD.Remarks = string.Empty;
            WFVD.VehicleNo = string.Empty;
            return PartialView("_CrewSalaryExpense", WFVD);
        }
        public ActionResult VehicleTrackingExpense(int id)
        {
            webx_fleet_VoucherExp_DET WFVD = new webx_fleet_VoucherExp_DET();

            List<Webx_Master_General> mstVehicleTrackingExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && x.CodeId == "10" && x.StatusCode.ToUpper() == "Y").ToList();

            WFVD.SRNO = id;
            WFVD.ListNatureOfExpense = mstVehicleTrackingExpense;
            WFVD.VoucherNo = string.Empty;
            WFVD.ExpenseId = string.Empty;
            WFVD.Amt = Convert.ToDecimal(0.0);
            WFVD.ExeAmt = Convert.ToDecimal(0.0);
            WFVD.BillNo = string.Empty;
            WFVD.Remarks = string.Empty;
            WFVD.VehicleNo = string.Empty;
            return PartialView("_VehicleTrackingSystem", WFVD);
        }
        public ActionResult FinancialExpense(int id)
        {
            webx_fleet_VoucherExp_DET WFVD = new webx_fleet_VoucherExp_DET();
            string[] arr_FE = new string[] { "11", "12" };
            List<Webx_Master_General> mstFinancialExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "FEXP" && arr_FE.Contains(x.CodeId) && x.StatusCode.ToUpper() == "Y").ToList();

            WFVD.SRNO = id;
            WFVD.ListNatureOfExpense = mstFinancialExpense;
            WFVD.VoucherNo = string.Empty;
            WFVD.ExpenseId = string.Empty;
            WFVD.Amt = Convert.ToDecimal(0.0);
            WFVD.ExeAmt = Convert.ToDecimal(0.0);
            WFVD.BillNo = string.Empty;
            WFVD.Remarks = string.Empty;
            WFVD.VehicleNo = string.Empty;
            return PartialView("_FinancialExpense", WFVD);
        }

        public ActionResult RepairExpense(int id)
        {
            webx_fleet_VoucherExp_DET WFVD = new webx_fleet_VoucherExp_DET();
            List<Webx_Master_General> mstRepairExpense = MS.GetGeneralMasterObject().Where(x => x.CodeType == "VEXP" && x.StatusCode.ToUpper() == "Y").ToList();

            WFVD.SRNO = id;
            WFVD.ListNatureOfExpense = mstRepairExpense;
            WFVD.VoucherNo = string.Empty;
            WFVD.ExpenseId = string.Empty;
            WFVD.Amt = Convert.ToDecimal(0.0);
            WFVD.ExeAmt = Convert.ToDecimal(0.0);
            WFVD.BillNo = string.Empty;
            WFVD.Remarks = string.Empty;
            WFVD.VehicleNo = string.Empty;
            return PartialView("_RepairExpense", WFVD);
        }

        public JsonResult GetTripsheetDetails(string dockno)
        {
            var ListLocations = PS.GetTripsheetBySearch(dockno);

            return new JsonResult()
            {
                Data = new
                {
                    VehicleNo = ListLocations.FirstOrDefault().VehicleNo,
                    StartKm = ListLocations.FirstOrDefault().f_issue_startkm,
                    ApprovedDiesel = ListLocations.FirstOrDefault().ApprovedDiesel,
                    FilledDiesel = ListLocations.FirstOrDefault().FilledDiesel
                }
            };
        }

        public JsonResult GetTripsheetBySearch(string searchTerm)
        {

            var ListLocations = PS.GetTripsheetBySearch(searchTerm);
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  id = e.Manual_TripSheetNo,
                                  text = e.VSlipNo,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetFuelSlipVehicle()
        {
            var CMP = PS.GetFuelSlipVehicle();

            var users = from user in CMP

                        select new
                        {
                            Value = user.CodeId,
                            Text = user.CodeDesc
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetVendorTypeLedger(string VendorType)
        {
            var CMP = PS.GetVendorTypeLedger(VendorType);

            string Acccode = "", Accdesc = "", Pan_no = "", servtaxno = "", AcctHead = "";

            if (CMP.Count > 0)
            {
                Acccode = CMP.FirstOrDefault().Acccode;
                Accdesc = CMP.FirstOrDefault().Accdesc;
                Pan_no = CMP.FirstOrDefault().Pan_no;
                servtaxno = CMP.FirstOrDefault().servtaxno;
                AcctHead = CMP.FirstOrDefault().AcctHead;
            }


            return new JsonResult()
            {
                Data = new
                {
                    Acccode = Acccode,
                    Accdesc = Accdesc,
                    Pan_no = Pan_no,
                    servtaxno = servtaxno,
                    AcctHead = AcctHead,
                }
            };
        }

        public JsonResult GetLedgerBySearch(string searchTerm)
        {

            var ListLedger = PS.GetLedgerBySerach(searchTerm);
            var SearchList = (from e in ListLedger
                              select new
                              {
                                  id = e.Acccode,
                                  text = e.Accdesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLedgerBySearchForSupplementaryBill(string searchTerm)
        {
            string SupplementaryBillAccCode = ConfigurationManager.AppSettings["SupplementaryBillAccCode"].ToString();
            string[] ledgerCode = SupplementaryBillAccCode.Trim().Split(',').Select(n => n.Trim()).ToArray();
            var ListLedger = PS.GetLedgerBySerach(searchTerm).Where(c => ledgerCode.Contains(c.Acccode)).ToList();
            var SearchList = (from e in ListLedger
                              select new
                              {
                                  id = e.Acccode,
                                  text = e.Accdesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Bill ENtry Payment

        public ActionResult BillEntryPaymentQuery(string Id)
        {
            //BillEntryPaymentViewModel BEPVM = new BillEntryPaymentViewModel();
            BillEntryPaymentFilter BEPFM = new BillEntryPaymentFilter();

            BEPFM.BillTyp = Id;
            if (Id == null || Id == "")
            {
                return RedirectToAction("BillEntryPaymentQuery", new { Id = "1" });
            }
            if (Id == "1")
            {
                BEPFM.BillType = "THC-PDC";
                BEPFM.ModuleName = "Bill Entry Payment";

            }
            else if (Id == "2")
            {
                BEPFM.BillType = "Fuel Expense";
                BEPFM.ModuleName = "Expense Bill Payment";
            }
            else if (Id == "3")
            {
                BEPFM.BillType = "Fuel Slip";
                BEPFM.ModuleName = "Fuel Expense Bill Payment";
            }
            else if (Id == "4")
            {
                BEPFM.BillType = "Expense";
                BEPFM.ModuleName = "Other Bill Payment";
            }
            else if (Id == "5")
            {
                //if (BaseLocationCode != "HQTR")
                //{
                //    return RedirectToAction("URLRedirect", "Home");
                //}
                BEPFM.BillType = "Job Expense";
                BEPFM.ModuleName = "Job Expense Bill Payment";
            }
            else if (Id == "6")
            {
                BEPFM.BillType = "OCT";
                BEPFM.ModuleName = "Octroi Bill Payment";
            }
            else if (Id == "7")
            {
                BEPFM.BillType = "POBILL";
                BEPFM.ModuleName = "PO Bill Payment";
            }
            /*
             8 - Loading - Unloading Bill           
            */
            else if (Id == "8")
            {
                BEPFM.BillType = "Loading";
                BEPFM.ModuleName = "Loading-Unloading Bill Payment";
            }

            else if (Id == "9")
            {
                BEPFM.BillType = "Monthly Loading";
                BEPFM.ModuleName = "Monthly Loading-Unloading Bill Payment";
            }

            else if (Id == "10")
            {
                BEPFM.BillType = "FRANCHISE";
                BEPFM.ModuleName = "Franchise Bill Payment";
            }
            else if (Id == "11")
            {
                BEPFM.BillType = "Attached";
                BEPFM.ModuleName = "Attached Vendor Bill";
            }
            else if (Id == "12")
            {
                BEPFM.BillType = "Monthly Attached";
                BEPFM.ModuleName = "Monthly Attached Vendor Bill";
            }

            else if (Id == "13")
            {
                BEPFM.BillType = "THC Connectivity"; // For Franchise
                BEPFM.ModuleName = "Franchise Connectivity Bill Payment";
            }

            else if (Id == "14")
            {
                BEPFM.BillType = "BA"; // For Franchise
                BEPFM.ModuleName = "BA Bill Payment";
            }

            return View(BEPFM);
        }

        public ActionResult BillEntryPayment(BillEntryPaymentFilter BEPF)
        {
            try
            {
                BillEntryPaymentViewModel BEPVM = new BillEntryPaymentViewModel();
                BEPVM.BEPFM = BEPF;
                BEPF.LocationCode = BaseLocationCode;
                //BEPVM.listBill = PS.BillEntryList(BEPF, GF.FormateDate(BEPF.FromDate), GF.FormateDate(BEPF.ToDate), BEPF.BillType).Where(c => Convert.ToDateTime(c.Billdate).Month > 3).ToList();
                BEPVM.listBill = PS.BillEntryList(BEPF, GF.FormateDate(BEPF.FromDate), GF.FormateDate(BEPF.ToDate), BEPF.BillType, BaseUserName).ToList();

                BEPVM.DebitListBill = PS.DebitNoteEntryList(BEPF).ToList();

                if (BEPF.BillType == "Job Expense")
                {
                    BEPVM.listBill = BEPVM.listBill.Where(c => c.Type == "BE").ToList();
                }

                DataTable DT = new DataTable();
                DT = PS.GetVendorOutStanding(BEPF.VendorCode, BaseYearVal);
                if (DT.Rows.Count > 0)
                {
                    BEPVM.BEPFM.DebitAmt = DT.Rows[0][0].ToString();
                    BEPVM.BEPFM.CreditAmt = DT.Rows[0][1].ToString();
                    BEPVM.BEPFM.OutStandingAmt = DT.Rows[0][2].ToString();
                }
                else
                {
                    BEPVM.BEPFM.DebitAmt = "0.00";
                    BEPVM.BEPFM.CreditAmt = "0.00";
                    BEPVM.BEPFM.OutStandingAmt = "0.00";
                }
                if (BEPF.FromDate == DateTime.MinValue)
                {
                    return RedirectToAction("BillEntryPaymentQuery", new { Id = BEPF.BillTyp });
                }
                return View(BEPVM);
            }
            catch (Exception ex)
            {
                return RedirectToAction("BillEntryPaymentQuery", new { Id = BEPF.BillTyp });
            }
        }

        public ActionResult FinalBillEntryPayment(BillEntryPaymentViewModel BEPVM, List<DNM_DNCancellationList> DNList)
        {
            try
            {
                decimal PendAMT = 0;//, PendAmount = 0
                string Tot_String = BEPVM.BEPFM.BillNumber + "," + BEPVM.BEPFM.VoucherStr;
                BEPVM.BEPFM.LocationCode = BaseLocationCode;
                BEPVM.BEPFM.BillNo = BEPVM.BEPFM.BillNumber;
                BEPVM.listBill = PS.BillEntryList(BEPVM.BEPFM, GF.FormateDate(BEPVM.BEPFM.FromDate), GF.FormateDate(BEPVM.BEPFM.ToDate), BEPVM.BEPFM.BillType, BaseUserName).ToList();
                string[] Tot_StringSetArray = Tot_String.Trim().Split(',').Select(n => n.Trim()).ToArray();
                BEPVM.listBill = BEPVM.listBill.Where(f => Tot_StringSetArray.Contains(f.Billno.Trim())).ToList();

                foreach (var item in BEPVM.listBill)
                {
                    PendAMT = item.pendamt;
                }

                if (DNList != null)
                {
                    foreach (var item in DNList.Where(c => c.IsCheckBox == true).ToList())
                    {
                        item.PendAmount = PendAMT;
                    }
                    BEPVM.DebitListBill = DNList.Where(c => c.IsCheckBox == true).ToList();
                }

                BEPVM.PC = new PaymentControl();
                return View(BEPVM);
            }
            catch (Exception)
            {
                return RedirectToAction("BillEntryPaymentQuery", new { Id = BEPVM.BEPFM.BillTyp });
            }
        }

        public JsonResult GetLedger(string Category, string VehicleNo, string ModuleType)
        {
            var CMP = PS.LedgerListFromCategory(Category, BaseLocationCode, VehicleNo, ModuleType);

            var users = from user in CMP

                        select new
                        {
                            Value = user.CodeId,
                            Text = user.CodeDesc
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitBillEntryPayment(BillEntryPaymentViewModel BEPVM, List<BillList> BillList, PaymentControl PaymentControl, List<DNM_DNCancellationList> DNBillList)
        {
            if (BEPVM.BEPFM.LocationCode != BaseLocationCode)
            {
                ViewBag.StrError = "Bill Payment can not be made from " + BaseLocationCode + " as Bill is generated from " + BEPVM.BEPFM.LocationCode;
                return View("Error");
            }
            BEPVM.listBill = BillList;
            string vndrcd = "";
            if (BEPVM.listBill != null && BEPVM.listBill.Count > 0)
            {
                vndrcd = BEPVM.listBill.FirstOrDefault().vendorcd;
            }
            BEPVM.BEPFM.VendorCode = vndrcd.Split(':')[0];
            BEPVM.BEPFM.VendorName = vndrcd.Split(':')[1];
            BEPVM.PC = PaymentControl;
            try
            {
                if (PaymentControl.PayAmount < 0)
                {
                    ViewBag.StrError = "Please Enter Non Negative Value.";
                    return View("Error");
                }

                string Cnt = "";
                if (PaymentControl.PaymentMode != "Cash")
                {
                    Cnt = PS.Duplicate_ChqNO(PaymentControl.ChequeNo, GF.FormateDate(PaymentControl.ChequeDate));
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                if ((PaymentControl.PaymentMode == "Cash" || PaymentControl.PaymentMode == "Both") && PaymentControl.CashAmount != 0)
                {
                    Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(PaymentControl.CashAmount), GF.FormateDate(BEPVM.BEPFM.VoucherDate), BaseFinYear, BaseLocationCode);
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                }

                string Xml_PAY_DebitNote = "<root>";
                decimal PendAmt = 0, DNTotalAMt = 0, pendamt = 0;
                bool IsDebitNot = false;
                bool IScheck = false;
                if (DNBillList != null)
                {
                    foreach (var item in DNBillList)
                    {
                        IsDebitNot = true;
                        if (IScheck == false)
                        {
                            PendAmt = item.PendAmount - item.NetAmount;
                            IScheck = true;
                        }
                        else
                        {
                            PendAmt = PendAmt - item.NetAmount;
                            pendamt = PendAmt;
                        }
                        //DNTotalAMt = DNTotalAMt + item.NetAmount;

                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<PAY_DebitNote>";
                        //Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<Billno>" + item.BillNo + "</Billno>";
                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<PayDt>" + BEPVM.BEPFM.VoucherDate + "</PayDt>";
                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<NetAmt>" + item.PendAmount + "</NetAmt>";
                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<PaidAmt>" + item.NetAmount + "</PaidAmt>";
                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<PendAmt>" + PendAmt + "</PendAmt>";
                        //Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<CurrAmt>" + DNTotalAMt + "</CurrAmt>";
                        Xml_PAY_DebitNote = Xml_PAY_DebitNote + "<DebitNoteNo>" + item.DocumentNo + "</DebitNoteNo></PAY_DebitNote>";
                    }
                }

                Xml_PAY_DebitNote = Xml_PAY_DebitNote + "</root>";

                string Xml_PAY_MST = "<root><PAY_Mst>", Xml_PAY_Det = "<root>";
                if (BEPVM.BEPFM.BillType != "Job Expense")
                {
                    foreach (var item in BEPVM.listBill)
                    {
                        decimal CurrAmt = 0;
                        if (IsDebitNot)
                        {
                            CurrAmt = pendamt;
                        }
                        else
                        {
                            CurrAmt = item.CurrAmt;
                            pendamt = item.pendamt;
                        }

                        Xml_PAY_Det = Xml_PAY_Det + "<PAY_Det><Billno>" + item.Billno + "</Billno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PayDt>" + BEPVM.BEPFM.VoucherDate + "</PayDt>";
                        // Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + /*item.pendamt*/pendamt + "</PendAmt>";
                        //  Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + /*item.CurrAmt*/CurrAmt + "</CurrAmt></PAY_Det>";

                        if (BEPVM.BEPFM.BillType == "POBILL")
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + pendamt + "</PendAmt>";
                        }
                        else
                        {
                            Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + item.CurrAmt + "</PendAmt>";
                        }
                        Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + item.Amount + "</CurrAmt></PAY_Det>";
                    }
                }
                else
                {
                    foreach (var item in BEPVM.listBill.Where(c => c.Type == "BE"))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<PAY_Det><Billno>" + item.Billno + "</Billno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PayDt>" + BEPVM.BEPFM.VoucherDate + "</PayDt>";
                        //  Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + item.pendamt + "</PendAmt>";
                        // Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + item.CurrAmt + "</CurrAmt></PAY_Det>";

                        Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + item.CurrAmt + "</PendAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + item.Amount + "</CurrAmt></PAY_Det>";
                    }

                    foreach (var item in BEPVM.listBill.Where(c => c.Type == "VR"))
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<PAY_VR><Billno>" + item.Billno + "</Billno>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PayDt>" + BEPVM.BEPFM.VoucherDate + "</PayDt>";
                        // Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + item.pendamt + "</PendAmt>";
                        //  Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + item.CurrAmt + "</CurrAmt></PAY_VR>";
                        Xml_PAY_Det = Xml_PAY_Det + "<PendAmt>" + item.CurrAmt + "</PendAmt>";
                        Xml_PAY_Det = Xml_PAY_Det + "<CurrAmt>" + item.Amount + "</CurrAmt></PAY_Det>";
                    }
                }
                Xml_PAY_Det = Xml_PAY_Det + "</root>";
                Xml_PAY_MST = Xml_PAY_MST + "<Manual_Voucherno>" + BEPVM.BEPFM.VoucherNo + "</Manual_Voucherno>";
                Xml_PAY_MST = Xml_PAY_MST + "<PAYDT>" + BEPVM.BEPFM.VoucherDate + "</PAYDT>";
                Xml_PAY_MST = Xml_PAY_MST + "<Vendorcode>" + BEPVM.BEPFM.VendorCode + "</Vendorcode>";
                Xml_PAY_MST = Xml_PAY_MST + "<VendorName>" + BEPVM.BEPFM.VendorName + "</VendorName>";
                Xml_PAY_MST = Xml_PAY_MST + "<PayMode>" + BEPVM.PC.PaymentMode + "</PayMode>";
                Xml_PAY_MST = Xml_PAY_MST + "<CashAmt>" + BEPVM.PC.CashAmount + "</CashAmt>";
                Xml_PAY_MST = Xml_PAY_MST + "<Cashcode>" + BEPVM.PC.CashLedger + "</Cashcode>";
                Xml_PAY_MST = Xml_PAY_MST + "<BillType>" + BEPVM.BEPFM.BillType + "</BillType>";
                if (BEPVM.PC.PaymentMode == "Cash")
                {
                    Xml_PAY_MST = Xml_PAY_MST + "<Bankaccode>" + "" + "</Bankaccode>";
                    Xml_PAY_MST = Xml_PAY_MST + "<Bankaccodesc>" + "" + "</Bankaccodesc>";
                }
                else
                {
                    Xml_PAY_MST = Xml_PAY_MST + "<Bankaccode>" + BEPVM.PC.BankLedger + "</Bankaccode>";
                    Xml_PAY_MST = Xml_PAY_MST + "<Bankaccodesc>" + BEPVM.PC.BankLedgerName + "</Bankaccodesc>";
                }
                if (BEPVM.PC.PaymentMode != "Cash")
                {
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqAmt>" + BEPVM.PC.ChequeAmount + "</ChqAmt>";
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqNo>" + BEPVM.PC.ChequeNo + "</ChqNo>";
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqDate>" + BEPVM.PC.ChequeDate + "</ChqDate>";
                }

                Xml_PAY_MST = Xml_PAY_MST + "<NetPay>" + BEPVM.PC.PayAmount + "</NetPay>";
                Xml_PAY_MST = Xml_PAY_MST + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_PAY_MST = Xml_PAY_MST + "</PAY_Mst></root>";

                Xml_PAY_MST = Xml_PAY_MST.Replace('\n', ' ').Trim();
                Xml_PAY_MST = Xml_PAY_MST.Replace("&", "&amp;").Trim();

                Xml_PAY_Det = Xml_PAY_Det.Replace('\n', ' ').Trim();
                Xml_PAY_Det = Xml_PAY_Det.Replace("&", "&amp;").Trim();


                //    DataTable Dt = PS.Insert_BillEntry_Payment_Data(Xml_PAY_MST, Xml_PAY_Det, "BillPayment", BaseLocationCode, BaseUserName, BaseFinYear, Xml_PAY_DebitNote);
                DataTable Dt = PS.Insert_BillEntry_Payment_Data(Xml_PAY_MST, Xml_PAY_Det, "BillPayment", BaseLocationCode, BaseUserName, BaseFinYear);
                string Voucherno = "";
                string TranXaction = "";
                Voucherno = Dt.Rows[0]["Voucherno"].ToString();
                TranXaction = Dt.Rows[0]["TranXaction"].ToString();
                return RedirectToAction("BillEntryPaymentDone", new { Voucherno = Voucherno, TranXaction = TranXaction });
            }
            catch (Exception Ex)
            {
                @ViewBag.StrError = Ex.Message;
                return View("Error");
            }
        }

        public ActionResult BillEntryPaymentDone(string Voucherno, string TranXaction)
        {

            ViewBag.Voucherno = Voucherno;
            ViewBag.TranXaction = TranXaction;
            return View();

        }

        #endregion

        #region  Bill Entry

        public ActionResult AgentBillEntry()
        {
            BillEntryPaymentViewModel BEPVM = new BillEntryPaymentViewModel();
            List<BillEntry> ListBillEntry = new List<BillEntry>();
            BillEntry objBillEntry = new BillEntry();

            objBillEntry.Id = 1;
            ListBillEntry.Add(objBillEntry);
            BEPVM.ListBillEntry = ListBillEntry;
            return View(BEPVM);
        }

        public ActionResult AddNewRowBill(int Id)
        {
            BillEntry objBillEntry = new BillEntry();

            objBillEntry.Id = Id;
            return PartialView("_PartiaBillEntry", objBillEntry);
        }

        //public ActionResult AddAgentBillEntry(BillEntryPaymentViewModel BEPVM, List<BillEntry> BillEntryList)
        //{
        //    string billno = "";
        //    decimal Hnd_Declval = 0;
        //    string ocbillno = "", voucherno = "";
        //    int count = 0;
        //    DataTable Dt = new DataTable();
        //    SqlConnection con = new SqlConnection(Connstr);
        //    con.Open();
        //    SqlTransaction trn = con.BeginTransaction();
        //    try
        //    {

        //        //Generated bill Number
        //        DataTable dtBillNo = PS.Get_BillNo(BaseLocationCode, GF.FormateDate(System.DateTime.Now));
        //        billno = dtBillNo.Rows[0]["NewCode"].ToString();
        //        if (billno == "")
        //        {
        //            billno = BaseLocationCode + "/BE/0000001";
        //        }
        //        BEPVM.BE.Billno = billno;
        //        BEPVM.BE.COMPANY_CODE = BaseLocationCode;

        //        XmlDocument xmlDoc1 = new XmlDocument();

        //        XmlSerializer xmlSerializer1 = new XmlSerializer(BEPVM.BE.GetType());
        //        using (MemoryStream xmlStream1 = new MemoryStream())
        //        {
        //            xmlSerializer1.Serialize(xmlStream1, BEPVM.BE);
        //            xmlStream1.Position = 0;
        //            xmlDoc1.Load(xmlStream1);
        //            DataTable dtBill = PS.Insert_Agent_BillEntry(BaseUserName, BaseLocationCode, xmlDoc1.InnerXml);
        //        }

        //        foreach (var item in BillEntryList)
        //        {

        //            DataTable dt = PS.Insert_webx_oct_det_0(item.CNoteNo);
        //            count = Convert.ToInt32(dt.Rows[0]["count"]);

        //            DataTable dt1 = PS.Insert_webx_oct_det_2(item.CNoteNo);

        //            ocbillno = dt1.Rows[0]["ocbillno"].ToString();
        //            voucherno = dt1.Rows[0]["voucherno"].ToString();

        //            if (ocbillno != "")
        //            {
        //                DataTable dt2 = PS.Insert_webx_oct_det_1(ocbillno);
        //                ocbillno = dt2.Rows[0]["ocbillno"].ToString();
        //            }
        //            if (count == 0)
        //            {
        //                DataTable dt3 = PS.Insert_webx_oct_det_3(ocbillno, item.CNoteNo, item.OctroiAmount, item.OctroiReceiptnNo, item.Receiptdate, item.Hnd_Declval, "", BEPVM.BE.Billdate);
        //            }
        //            else
        //            {
        //                if (ocbillno == "" && voucherno == "")
        //                {
        //                    DataTable dt4 = PS.Insert_webx_oct_det_4(billno, item.CNoteNo, item.OctroiAmount, item.OctroiReceiptnNo, item.Receiptdate, "drpagent", BEPVM.BE.Billdate);
        //                }
        //                else
        //                {
        //                    string documentNo = ocbillno;
        //                    if (documentNo == "")
        //                    {
        //                        documentNo = voucherno;
        //                    }
        //                }
        //            }
        //            DataTable dt5 = PS.Insert_webx_oct_det_5(item.CNoteNo);
        //            //DataTable dt6 = PS.Insert_webx_oct_det_6(billno, BaseFinYear);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        string exmess = ex.Message.ToString().Replace('\n', '_');
        //        trn.Rollback();
        //        con.Close();
        //        return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
        //    }

        //    return RedirectToAction("BillEntryDone", new { BillNo = billno });
        //}

        public ActionResult AddAgentBillEntry(BillEntryPaymentViewModel BEPVM, List<BillEntry> BillEntryList)
        {
            DataTable dt = new DataTable();
            string baselocaName = "", Businesstype = "", Billno = "";
            string sql = "select locname from webx_location where  loccode='" + BaseLocationCode + "'";
            dt = GF.GetDataTableFromSP(sql);
            baselocaName = dt.Rows[0]["locname"].ToString();

            try
            {
                string Xml_OctBillHeader = "<root><BillOctHdr>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCBILLSF>" + "." + "</OCBILLSF>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCBILLDT>" + BEPVM.BE.Billdate.ToString() + "</OCBILLDT>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCBRCD>" + BaseLocationCode + "</OCBRCD>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGCD>" + BEPVM.BE.OctroiAgnet.Split(':')[0].ToString() + "</OCAGCD>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGNM>" + BEPVM.BE.OctroiAgnet.Split(':')[1].ToString() + "</OCAGNM>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGBILLDT>" + BEPVM.BE.Billdate.ToString() + "</OCAGBILLDT>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGBILLNO>" + BEPVM.BE.Billno + "</OCAGBILLNO>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGOCTAMT>" + BEPVM.BE.OctroiAmount + "</OCAGOCTAMT>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGOTHCHRG>" + BEPVM.BE.OtherCharge + "</OCAGOTHCHRG>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCAGSERCHRG>" + BEPVM.BE.AgentSerCharge + "</OCAGSERCHRG>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<NetAmt>" + BEPVM.BE.TotalDue + "</NetAmt>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<PendAmt>" + BEPVM.BE.TotalDue + "</PendAmt>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<OCDUEDT>" + BEPVM.BE.DueDate.ToString() + "</OCDUEDT>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<ENTRYBY>" + BaseUserName + "</ENTRYBY>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<REMARK>" + BEPVM.BE.Remark + "</REMARK>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<ACTSERCHRG>" + "0" + "</ACTSERCHRG>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<servchargeper>" + BEPVM.BE.ServiceCharge + "</servchargeper>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<sundrychrg>" + BEPVM.BE.SundryCharge + "</sundrychrg>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<formchrg>" + BEPVM.BE.FromCharge + "</formchrg>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<clearchrg>" + BEPVM.BE.ClearCharge + "</clearchrg>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<CANCEL_YN>" + "N" + "</CANCEL_YN>";
                Xml_OctBillHeader = Xml_OctBillHeader + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_OctBillHeader = Xml_OctBillHeader + "</BillOctHdr></root>";

                string Xml_OctBillDetail = "<root>";

                if (BillEntryList != null)
                {
                    foreach (var item in BillEntryList)
                    {

                        Xml_OctBillDetail = Xml_OctBillDetail + "<BillOctDet>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<OCBILLSF>" + "." + "</OCBILLSF>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<DOCKNO>" + item.CNoteNo + "</DOCKNO>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<OCTAMT>" + item.OctroiAmount + "</OCTAMT>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<RECPTNO>" + item.OctroiReceiptnNo + "</RECPTNO>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<RECPTDT>" + Convert.ToDateTime(item.Receiptdate).ToString("dd MMM yyyy") + "</RECPTDT>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<vendor_paidby>" + BaseUserName + "</vendor_paidby>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "<vendor_paidat>" + BaseLocationCode + "</vendor_paidat>";
                        //Xml_OctBillDetail = Xml_OctBillDetail + "<OCT_DET_YN>" + hidStatus.Value + "</OCT_DET_YN>";
                        Xml_OctBillDetail = Xml_OctBillDetail + "</BillOctDet>";
                    }
                }
                Xml_OctBillDetail = Xml_OctBillDetail + "</root>";

                DataTable DT = new DataTable();

                DT = PS.InsertOctroiBillPayment(BaseLocationCode, Xml_OctBillHeader, Xml_OctBillDetail, BaseYearVal.Split('_')[0].ToString(), Businesstype);
                Billno = DT.Rows[0]["Billno"].ToString();
                string TranXaction = DT.Rows[0]["TranXaction"].ToString();

                sql = "SELECT COUNT(*) FROM WEBX_OCT_DET WHERE OCBILLNO='" + Billno + "'";
                DT = GF.GetDataTableFromSP(sql);

                string sqlQuery = "SELECT COUNT(*) FROM Webx_oct_hdr h cross apply (select OCTAMT=SUM(OCTAMT) From Webx_oct_det d where h.ocbillno=d.ocbillno)a where a.OCTAMT<>OCAGOCTAMT and cancel_yn='N' and OCBILLNO='" + Billno + "'";
                DT = GF.GetDataTableFromSP(sql);


                return RedirectToAction("BillEntryDone", new { Billno = Billno });

            }
            catch (Exception ex)
            {
                @ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult BillEntryDone(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View("BillEntryDone");
        }

        public JsonResult CheckDocketNo(string DocketNo)
        {
            try
            {
                string MSG = PS.checkDocketNo(DocketNo, BaseFinYear, BaseYearValFirst);

                return new JsonResult()
                {
                    Data = new
                    {
                        CallMSG = MSG,
                    }
                };
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(0);
            }
        }

        public JsonResult GetOctroiVendorDetails(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<Webx_Master_General> CMP = PS.GeTOctroiVendor(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.Name,
                            text = user.Code

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Bill Entry & Payment View

        public ActionResult BillEntryPaymentView()
        {
            return View();
        }

        public ActionResult BillEntryPaymentViewList(BillEntryPaymentFilter vm)
        { //add BillEntryPaymentFilter list in this vm
            vm.BillEntryPayList = new List<BillEntryPaymentList>();
            vm.BillEntryPayList1 = new List<BillEntryPaymentList>();
            if (vm.BillTyp == "BE")
            {
                List<BillEntryPaymentList> optrkList = PS.Get_VendorBillPaymentListView(vm.BillNo, vm.VendorCode, GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), BaseLocationCode, vm.VBillNo, vm.BillType, vm.BillTyp, vm.VoucherNo);
                vm.BillEntryPayList = optrkList;

            }
            else
            {
                List<BillEntryPaymentList> optrkList1 = PS.Get_VendorBillPaymentListView(vm.BillNo, vm.VendorCode, GF.FormateDate(vm.FromDate), GF.FormateDate(vm.ToDate), BaseLocationCode, vm.VBillNo, vm.BillType, vm.BillTyp, vm.VoucherNo);
                vm.BillEntryPayList1 = optrkList1;
            }
            return View(vm);
        }

        public ActionResult BillEntryPaymentPrint(string BILLNO, string VoucherType, string Voucherno, string expBillNo)
        {
            ViewBag.BILLNO = BILLNO;
            return View();
        }

        #endregion

        #region PO SKU Detail View
        public ActionResult POSKUDetailView()
        {

            return View();
        }
        #endregion

        #region  GRN Bill Entry

        public ActionResult GRNBillEntry()
        {
            FuelBillEntyQuery FBE = new FuelBillEntyQuery();
            TempData["GRN"] = "GRN";
            FBE.Type = 7;
            return View(FBE);
        }

        public ActionResult GRNBillEntryList(FuelBillEntyQuery FBE)
        {
            GRNBillEntryViewModel GRNVM = new GRNBillEntryViewModel();
            GRNVM.GRNList = PS.GetGRNForBillEntry(FBE, BaseLocationCode, BaseCompanyCode);
            GRNVM.FromDate = GF.FormateDate(FBE.FromDate);
            GRNVM.ToDate = GF.FormateDate(FBE.ToDate);
            GRNVM.VendorName = FBE.VendorName;

            GRNVM.StateCode = FBE.StateCode;
            GRNVM.GSTType = FBE.GSTType;
            GRNVM.isGSTReverse = FBE.isGSTReverse;

            GRNVM.PoNo = FBE.PoNo == "" || FBE.PoNo == null ? "-" : FBE.PoNo;
            GRNVM.ManualPONo = FBE.ManualPONo == "" || FBE.ManualPONo == null ? "-" : FBE.ManualPONo;
            if (!string.IsNullOrEmpty(FBE.MatCat))
                GRNVM.MatCat = MS.GetGeneralMasterObject().Where(c => c.CodeType.ToUpper() == "MATCAT" && c.CodeId == FBE.MatCat).FirstOrDefault().CodeDesc;
            return View(GRNVM);
        }

        public ActionResult POGRNBillEntryList(string PONo, string Vendorcode, string MatCatAcctHead, string GSTType, string StateCode, bool isGSTReverse)
        {
            GRNBillEntryViewModel GRNVM = new GRNBillEntryViewModel();
            GRNVM.VendorCode = Vendorcode;
            GRNVM.MatCat = MatCatAcctHead;
            GRNVM.StateCode = StateCode;
            GRNVM.GSTType = GSTType;
            GRNVM.isGSTReverse = isGSTReverse;
            GRNVM.GRNList = PS.GetPOGRNForBillEntry(PONo);
            return View(GRNVM);
        }

        public ActionResult POGRNBillEntryPayment(GRNBillEntryViewModel GRNVM, List<GRNBillEntryDetails> POGRN)
        {
            FuelBillEntyQuery FBE = new FuelBillEntyQuery();
            string GRNNo = "";
            foreach (var item in POGRN.Where(c => c.IsChecked == true).ToList())
            {
                FBE.VendorCode = item.Vendor;
                if (GRNNo == "")
                {
                    GRNNo = item.GRNNO.ToString();
                }
                else
                {
                    GRNNo = GRNNo + "," + item.GRNNO.ToString();
                }
            }

            FBE.MatCat = GRNVM.MatCat;
            FBE.ManualPONo = GRNNo;
            FBE.StateCode = GRNVM.StateCode;
            FBE.GSTType = GRNVM.GSTType;
            FBE.isGSTReverse = GRNVM.isGSTReverse;
            TempData["FBE"] = FBE;
            return RedirectToAction("FuelBillEntry", new { Id = "7", VendorCode = FBE.VendorCode, MatCat = FBE.MatCat, ManualPONo = FBE.ManualPONo });
        }

        #endregion

        #region GRN View & Print

        public ActionResult GRNViewPrintCriteria()
        {
            return View();
        }

        public ActionResult GRNViewPrintList(OctroiAgentBillViewModel vm)
        {
            try
            {
                if (vm.AgentBillModel != null)
                {
                    if (vm.AgentBillModel.BillNo != null)
                    {
                        vm.AgentBillModel.FilterType = "1";
                    }
                    else if (vm.AgentBillModel.MBillNo != null)
                    {
                        vm.AgentBillModel.FilterType = "2";
                    }
                    else
                    {
                        vm.AgentBillModel.FilterType = "0";
                    }

                    List<GRNViewPrintList> optrkList = PS.Get_GRNViewList(vm.AgentBillModel.FilterType, vm.AgentBillModel.PartyCode, GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.BillNo, vm.AgentBillModel.MBillNo);
                    vm.ListGRNViewPrint = optrkList;
                }
                else
                {
                    RedirectToAction("GRNViewPrintCriteria");
                }
            }
            catch (Exception)
            {

                //throw;
            }
            return View(vm);
        }

        #endregion

        #region PO View Print

        public ActionResult POViewPrintCriteria()
        {
            return View();
        }

        public ActionResult POViewPrintList(OctroiAgentBillViewModel vm)
        {
            string FilterType = "0";
            if (!string.IsNullOrEmpty(vm.AgentBillModel.BillNo))
            {
                FilterType = "1";
            }
            List<webx_GENERAL_POASSET_HDR> optrkList = PS.Get_POViewList(FilterType, vm.AgentBillModel.PartyCode, GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.BillNo, BaseLocationCode);
            vm.ListPOViewPrint = optrkList;
            return View(vm);
        }
        #endregion

        #region SKU Po Master

        public ActionResult SKUMasterEditCriteria(string Flag)
        {
            ADDSKUMasterViewModel ViewModel = new ADDSKUMasterViewModel();
            //Webx_PO_SKU_Master WPM = new Webx_PO_SKU_Master();
            //List<Webx_PO_SKU_Master> ListWPM= new List<Webx_PO_SKU_Master>();
            //ViewModel.WPM = WPM;
            ViewModel.Flag = Flag;
            return View(ViewModel);
        }

        public ActionResult SKUMasterADD(ADDSKUMasterViewModel ViewModel, string Flag)
        {
            List<webx_PO_SKU_Master> ListWPM = new List<webx_PO_SKU_Master>();
            webx_PO_SKU_Master WPM = new webx_PO_SKU_Master();
            ViewModel.Flag = Flag;
            try
            {
                if (Flag.ToUpper() == "ADD")
                {

                    WPM.Srno = 1;

                    ListWPM.Add(WPM);
                    ViewModel.ListWPM = ListWPM;
                    ViewModel.WPM = WPM;

                }
                if (Flag.ToUpper() == "EDIT")
                {
                    DataTable DT = PS.GetSKUItemDetails(ViewModel.WPM.SKU_ID);
                    List<webx_PO_SKU_Master> SKU_Item = DataRowToObject.CreateListFromTable<webx_PO_SKU_Master>(DT);
                    ViewModel.WPM = SKU_Item.FirstOrDefault();
                    if (ViewModel.WPM.ActiveFlag == "Y")
                    {
                        ViewModel.WPM.IsActive = true;
                    }
                    else
                    {
                        ViewModel.WPM.IsActive = false;
                    }
                    ListWPM.Add(ViewModel.WPM);
                    ViewModel.ListWPM = ListWPM;
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return View(ViewModel);
        }

        public ActionResult AddNewInfo(int Srno)
        {
            webx_PO_SKU_Master WPM = new webx_PO_SKU_Master();
            try
            {
                WPM.Srno = Srno;
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return PartialView("_SKUMasterPartial", WPM);
        }

        public ActionResult SKUMasterADDSubmit(ADDSKUMasterViewModel Viewmodel, List<webx_PO_SKU_Master> SKUADD)
        {
            string SKUID = "", ID = "";
            int IsCheck = 0;
            try
            {
                //List<Webx_PO_SKU_Master> ListSKU = new List<Webx_PO_SKU_Master>();
                // webx_PO_SKU_Master CygnusSKU = new webx_PO_SKU_Master();
                foreach (var item in SKUADD.Where(c => c.IsCheck == true).ToList())
                {
                    //if (item.IsCheck == true)
                    //{
                    List<webx_PO_SKU_Master> ListSKU = new List<webx_PO_SKU_Master>();
                    IsCheck = IsCheck + 1;
                    //CygnusSKU = SKUADD.FirstOrDefault();
                    if (Viewmodel.Flag == "ADD")
                    {
                        DataTable DT = PS.GetSKUID();
                        ID = DT.Rows[0]["Column1"].ToString();
                        item.SKU_ID = ID;
                    }

                    item.EntryBy = BaseUserName;
                    if (item.IsActive == true)
                    {
                        item.ActiveFlag = "Y";
                    }
                    else
                    {
                        item.ActiveFlag = "N";
                    }

                    ListSKU.Add(item);
                    XmlDocument xmlDoc1 = new XmlDocument();
                    XmlSerializer xmlSerializer1 = new XmlSerializer(ListSKU.GetType());

                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, ListSKU);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }
                    if (Viewmodel.Flag == "ADD")
                    {
                        //string SQRY = "exec Usp_Insert_SKUMASTERADD_NewPortal '" + xmlDoc1.InnerXml + "','" + BaseUserName + "','" + item.SKU_ID + "'";
                        DataTable DT1 = PS.InsertSKUMASTERADD(xmlDoc1.InnerXml.ToString(), BaseUserName, item.SKU_ID.ToString());
                        SKUID = SKUID + DT1.Rows[0]["Column1"].ToString() + ",";
                    }
                    if (Viewmodel.Flag == "EDIT")
                    {
                        //string SQRY = "exec Usp_Update_SKUMASTERADD_NewPortal '" + xmlDoc1.InnerXml + "','" + BaseUserName + "'";
                        DataTable DT2 = PS.UpdateSKUMASTERADD(xmlDoc1.InnerXml.ToString(), BaseUserName);
                        SKUID = DT2.Rows[0]["SKU_ID"].ToString();
                    }
                    //}
                }
                SKUID = SKUID.ToString().TrimEnd(',');
            }
            catch (Exception ex)
            {
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return RedirectToAction("SKUMasterADDDone", new { SKUID = SKUID });
        }

        public ActionResult SKUMasterADDDone(string SKUID)
        {
            ViewBag.SKUID = SKUID;
            return View();
        }

        public JsonResult GetSKUItem(string searchTerm)
        {
            var CMP = PS.GetSKUItemListFromSearch(searchTerm);

            var users = from user in CMP

                        select new
                        {
                            id = user.SKU_ID,
                            text = user.SKU_Desc
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region PO Approval

        public ActionResult POFinalizedCriteria(string id)
        {
            POApprovalViewModel POAV = new POApprovalViewModel();
            POAV.FilterType = "0";
            POAV.POType = "PO";
            if (!string.IsNullOrEmpty(id))
            {
                POAV.FilterType = "1";
                POAV.POCode = id.Replace("_", "/");
            }
            return View(POAV);
        }

        public ActionResult IndexPO(POApprovalViewModel POAV)
        {
            POApprovalViewModel POAVM = new POApprovalViewModel();
            List<webx_GENERAL_POASSET_HDR> POList = new List<webx_GENERAL_POASSET_HDR>();
            try
            {
                string FirstDate1 = POAV.FromDate.ToString("dd MMM yyyy");
                string LastDate1 = POAV.ToDate.ToString("dd MMM yyyy");
                if (POAV.VendorCode != null && POAV.VendorCode != "")
                {
                    POAV.VendorCode = POAV.VendorCode.Split('~').Last();
                }
                string CurrentLocationCode = BaseLocationCode;
                if (BaseUserName.ToUpper() == "10001")
                {
                    CurrentLocationCode = "";
                }

                if (POAV.POType == "PO")
                {
                    ViewBag.Type = "PO";
                    if (BaseLocationCode == "HQTR")
                    {
                        POList = OS.GetPOData(POAV.FilterType, POAV.VendorCode, FirstDate1, LastDate1, POAV.POCode, POAV.ManualPO, BaseLocationCode);
                    }
                }
                else
                {
                    ViewBag.Type = "WO";
                    POList = OS.GetWOData(POAV.FilterType, POAV.VendorCode, FirstDate1, LastDate1, POAV.POCode, POAV.ManualPO, BaseLocationCode);
                }
                DataTable DT = OS.GetPOAprovalLimit(BaseUserName);
                if (DT != null && DT.Rows.Count > 0)
                {
                    POAVM.POApprovalLimit = 0;//Convert.ToDecimal(DT.Rows[0]["POApprovalAmt"]);
                }
                else
                {
                    POAVM.POApprovalLimit = 0;
                }
                POAVM.PoApproveDate = DateTime.Now;
                POAVM.POList = POList;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                //Error_Logs(ControllerName, "IndexEnquiryClosure", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(POAVM);
        }

        [HttpPost]
        public ActionResult IndexPO(POApprovalViewModel POAV, List<webx_GENERAL_POASSET_HDR> POAprovalGPH)
        {
            string pocodeList = "";
            if (POAprovalGPH != null && POAprovalGPH.Count > 0)
            {
                foreach (var WGPAH in POAprovalGPH.Where(c => c.ISAprovable == true).ToList())
                {
                    pocodeList = WGPAH.pocode + "," + pocodeList;
                }
                pocodeList = pocodeList.TrimEnd(',');
                OS.ApprovePO(pocodeList, BaseUserName, DateTime.Now.ToString("dd MMM yyyy"), POAV.POType);
            }
            POApprovalViewModel POAVM = new POApprovalViewModel();
            POAVM.POCode = pocodeList;
            return View("POApprovalDone", POAVM);
        }

        #endregion

        #region   Bill UnSubmission

        public ActionResult BillUnSubmissionCriteria()
        {
            BillUnSubmissionViewModel vm = new BillUnSubmissionViewModel();
            return View(vm);
        }

        public ActionResult BillUnSubmissionList(BillUnSubmissionViewModel vm)
        {
            vm.ListBillUn = PS.Get_BillUnSubmissionList(vm.BEPFM.BillNo, vm.BEPFM.VendorName, vm.BEPFM.BillType, GF.FormateDate(vm.BEPFM.FromDate), GF.FormateDate(vm.BEPFM.ToDate), BaseLocationCode, vm.BEPFM.VBillNo);
            return View(vm);
        }

        public ActionResult BillUnSubmissionSubmit(List<BillUnSubmissionModel> BillUnSubList, BillUnSubmissionViewModel vm)
        {
            String hidPO = "";
            foreach (var item in BillUnSubList.Where(c => c.IsChecked == true).ToList())
            {
                DataTable DT = PS.BillUnSubmissionSubmit(item.billno, BaseUserName, GF.FormateDate(vm.BEPFM.VoucherDate));
                if (hidPO == "")
                {
                    hidPO = item.billno;
                }
                else
                {
                    hidPO = hidPO + "," + item.billno;
                }
            }
            return RedirectToAction("BillUnSubmissionDone", new { dockno = hidPO });
        }

        public ActionResult BillUnSubmissionDone(string dockno)
        {
            ViewBag.dockno = dockno;
            return View();
        }


        #endregion

        #region Loading Payment

        public ActionResult LoadingQuery(int Id)
        {
            FuelBillEntyQuery BBEQ = new FuelBillEntyQuery();
            if (Id == 8)
            {
                ViewBag.ModuleName = "Loading / UnLoading Bill Entry";
            }
            else
            {
                ViewBag.ModuleName = "Loading / UnLoading Monthly Bill Entry";
            }
            BBEQ.Id = Id;
            return View(BBEQ);
        }

        #endregion

        #region Loading Payment

        public ActionResult ConnectivityBillQuery(int Id)
        {
            FuelBillEntyQuery BBEQ = new FuelBillEntyQuery();
            ViewBag.ModuleName = "Connectivity Charge Bill Entry";
            BBEQ.Id = Id;
            return View(BBEQ);
        }

        #endregion

        #region report data loading bill payment
        public ActionResult CBSReport()
        {
            List<webx_acctrans_Model> ObjList = new List<webx_acctrans_Model>();
            ObjList = PS.GetCBSReport();
            return View("CBSReport", ObjList);
        }
        #endregion

        #region GRN Bill Entry View

        public ActionResult GRNBillEntryViewPrintCriteria()
        {
            return View();
        }

        public ActionResult GRNBillEntryViewPrintList(OctroiAgentBillViewModel vm)
        {
            List<webx_GENERAL_POASSET_HDR> optrkList = PS.Get_GRNBillEntryList(GF.FormateDate(vm.AgentBillModel.FromDate), GF.FormateDate(vm.AgentBillModel.ToDate), vm.AgentBillModel.BillNo);
            vm.ListPOViewPrint = optrkList;
            return View(vm);
        }
        #endregion

        #region MR Approval

        public ActionResult MRApprovalCriteria()
        {
            MRApprovalViewModel MRVW = new MRApprovalViewModel();
            return View(MRVW);
        }

        public ActionResult MRApprovalList(MRApprovalViewModel MRVW)
        {
            DataTable DT = PS.GetMRApprovalDT(GF.FormateDate(MRVW.FromDate), GF.FormateDate(MRVW.ToDate), MRVW.MRNo, BaseYearValFirst, BaseLocationCode, MRVW.CustomerCode);
            MRVW.LisMRDetBill = DataRowToObject.CreateListFromTable<webx_MRDET_BILL>(DT);

            return View(MRVW);
        }

        public ActionResult SubmitMRApprovalList(MRApprovalViewModel MRVW, List<webx_MRDET_BILL> MRSNOList)
        {
            MRApprovalViewModel MRVW1 = new MRApprovalViewModel();
            try
            {
                MRVW1.FromDate = MRVW.FromDate;
                MRVW1.ToDate = MRVW.ToDate;
                MRVW1.CustomerCode = MRVW.CustomerCode;
                string XML = "<root>";
                if (MRSNOList != null)
                {
                    foreach (var item in MRSNOList.Where(c => c.IsMrApproval == true))
                    {
                        XML = XML + "<MRSList>";
                        XML = XML + "<MRSNo>" + item.MRSNO + "</MRSNo>";
                        XML = XML + "<TotalBill>" + item.TOTBILL + "</TotalBill>";
                        XML = XML + "</MRSList>";
                    }
                }
                XML = XML + "</root>";

                DataTable DTMRS = new DataTable();

                if (MRVW.IsApproval)
                {
                    DTMRS = PS.MRApproval(XML, BaseLocationCode, BaseUserName, BaseFinYear);
                }
                else
                {
                    DTMRS = PS.MRRejection(XML, BaseLocationCode, BaseUserName, BaseFinYear);
                }

            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }

            //return RedirectToAction("MRApprovalDone");
            return RedirectToAction("MRApprovalList", MRVW1);
        }

        public ActionResult MRApprovalDone()
        {
            MRApprovalViewModel MRVW = new MRApprovalViewModel();
            return View(MRVW);
        }

        public void ExportData(string FileName, DateTime FromDate, DateTime ToDate, string MRNo, string CustomerCode)
        {
            DataTable dt = PS.GetMRApprovalDT(GF.FormateDate(FromDate), GF.FormateDate(ToDate), MRNo, BaseYearValFirst, BaseLocationCode, CustomerCode);
            dt.TableName = FileName;
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;

                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= " + FileName + ".xlsx");

                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }
        }

        #endregion

        #region Attached Vendor Bill

        public ActionResult AttachedvendorBill(int Id)
        {
            FuelBillEntyQuery BBEQ = new FuelBillEntyQuery();
            if (Id == 10)
            {
                ViewBag.ModuleName = "Attached Vendor Bill";
            }
            else
            {
                ViewBag.ModuleName = "Attached Vendor Monthly Bill Entry";
            }

            BBEQ.Id = Id;
            return View(BBEQ);
        }

        public JsonResult getBranchAndDocTypeWiseVendorListJsonForBill(string searchTerm, string DocType)
        {
            List<webx_VENDOR_HDR> ListVendor = new List<webx_VENDOR_HDR>();
            string SQRY = "SELECT DISTINCT VENDORCODE,VENDORNAME FROM webx_VENDOR_HDR WHERE  Vendor_Type='" + DocType + "' AND (VENDORNAME LIKE '%" + searchTerm + "%' OR VENDORCODE LIKE '%" + searchTerm + "%') AND Active='Y'";
            DataTable Dt = GF.GetDataTableFromSP(SQRY);
            ListVendor = DataRowToObject.CreateListFromTable<webx_VENDOR_HDR>(Dt);
            var SearchList = (from e in ListVendor
                              select new
                              {
                                  Value = e.VENDORCODE,
                                  Text = e.VENDORNAME,
                                  id = e.VENDORCODE,
                                  text = e.VENDORNAME,
                              }).ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Crossing Challan Payment
        public ActionResult CrossingChallanPaymentCriteria()
        {
            CrossingChallanFilterViewModel CRCHVM = new CrossingChallanFilterViewModel();
            CRCHVM.Orgncd = BaseLocationCode;
            return View(CRCHVM);
        }

        public ActionResult CrossingChallanPaymentList(CrossingChallanFilterViewModel CCF)
        {
            CrossingChallanViewModel CHV = new CrossingChallanViewModel();
            Webx_Crossing_Docket_Master WCDM_obj = new Webx_Crossing_Docket_Master();
            DataTable Dt = new DataTable();
            Dt = OS.GetVendorName(CCF.VendorCode);
            WCDM_obj.ChallanLocCode = BaseLocationCode;
            WCDM_obj.VendorCode = CCF.VendorCode;
            WCDM_obj.VendorName = Dt.Rows[0]["vendorname"].ToString();
            WCDM_obj.ChallanDate = System.DateTime.Today;
            CHV.WCDM = WCDM_obj;
            List<Webx_Crossing_Docket_Detail> DtlList = new List<Webx_Crossing_Docket_Detail>();
            Webx_Account_Details OBjWAD = new Webx_Account_Details();
            OBjWAD.SRNo = 1;
            OBjWAD.Vouchertype = "ADVICE";
            List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
            ListWAD1.Add(OBjWAD);
            CHV.WADListing = ListWAD1;
            DtlList = PS.CrossingChallanPaymentList(CCF.DockNo, CCF.FromDate.ToString("dd MMM yyyy"), CCF.ToDate.ToString("dd MMM yyyy"), BaseLocationCode, CCF.VendorCode);
            CHV.WCDD = DtlList;
            return View(CHV);
        }

        [HttpPost]
        public ActionResult CrossingChallanPaymentSubmit(CrossingChallanViewModel CCVM, List<Webx_Crossing_Docket_Detail> CrossingDocketDetail, List<Webx_Account_Details> PAYMENTMODE)
        {
            CCVM.WCDD = CrossingDocketDetail;
            Webx_Account_Details ObjWAD = PAYMENTMODE.First();
            CCVM.WAD = ObjWAD;
            string FIN_Start = "", Financial_Year = "", fin_year = "", Curr_Year = "", Finyear = "";
            Financial_Year = BaseFinYear.ToString().Substring(2, 2);
            fin_year = BaseFinYear.ToString();
            double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
            fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
            Curr_Year = DateTime.Now.ToString("yyyy");
            Finyear = BaseFinYear.ToString();
            if (Finyear == Curr_Year)
                FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
            else
                FIN_Start = "01 Apr " + Financial_Year;
            try
            {
                string Xml_PAY_MST = "<root><PAY_Mst>", Xml_PAY_Det = "<root>";
                foreach (var item in CrossingDocketDetail)
                {

                    if (item.IsChecked == true)
                    {
                        Xml_PAY_Det = Xml_PAY_Det + "<PAY_Det><DockNo>" + item.DockNo + "</DockNo>";
                        Xml_PAY_Det = Xml_PAY_Det + "<dockdate>" + item.DockDate + "</dockdate>";
                        Xml_PAY_Det = Xml_PAY_Det + "<NetPayable>" + item.NetPayable + "</NetPayable>";
                        Xml_PAY_Det = Xml_PAY_Det + "<Deductions>" + item.Deductions + "</Deductions>";
                        Xml_PAY_Det = Xml_PAY_Det + "<NetReceivable>" + item.NetReceivable + "</NetReceivable></PAY_Det>";
                    }
                }
                Xml_PAY_Det = Xml_PAY_Det + "</root>";

                Xml_PAY_MST = Xml_PAY_MST + "<VoucherDate>" + CCVM.WCDM.ChallanDate + "</VoucherDate>";
                Xml_PAY_MST = Xml_PAY_MST + "<Vendorcode>" + CCVM.WCDM.VendorCode + "</Vendorcode>";
                Xml_PAY_MST = Xml_PAY_MST + "<VendorName>" + CCVM.WCDM.VendorName + "</VendorName>";
                Xml_PAY_MST = Xml_PAY_MST + "<VendorType>" + "18" + "</VendorType>";
                Xml_PAY_MST = Xml_PAY_MST + "<ChallanLocCode>" + CCVM.WCDM.ChallanLocCode + "</ChallanLocCode>";

                if (CCVM.WAD.PaymentMode != "Cash")
                {
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqNo>" + CCVM.WAD.ChequeNo + "</ChqNo>";
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqDate>" + CCVM.WAD.Chequedate + "</ChqDate>";
                    Xml_PAY_MST = Xml_PAY_MST + "<Bankaccode>" + CCVM.WAD.DepositedInBank + "</Bankaccode>";

                }
                if (CCVM.WAD.PaymentMode == "Cash")
                {
                    Xml_PAY_MST = Xml_PAY_MST + "<ChqNo>" + "" + "</ChqNo>";
                    Xml_PAY_MST = Xml_PAY_MST + "<CashAccount>" + CCVM.WAD.CashAccount + "</CashAccount>";
                }
                Xml_PAY_MST = Xml_PAY_MST + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_PAY_MST = Xml_PAY_MST + "<Narration>" + CCVM.WCDM.Remarks + "</Narration>";
                Xml_PAY_MST = Xml_PAY_MST + "<TotalAmountPayables>" + CCVM.WCDM.TotalToPay + "</TotalAmountPayables>";
                Xml_PAY_MST = Xml_PAY_MST + "<TotalDeductions>" + CCVM.WCDM.Totaldeductions + "</TotalDeductions>";
                Xml_PAY_MST = Xml_PAY_MST + "<TotalNetPayables>" + CCVM.WCDM.TotalNetPayables + "</TotalNetPayables>";
                Xml_PAY_MST = Xml_PAY_MST + "<PaymentMode>" + CCVM.WAD.PaymentMode + "</PaymentMode>";
                Xml_PAY_MST = Xml_PAY_MST + "</PAY_Mst></root>";

                Xml_PAY_MST = Xml_PAY_MST.Replace('\n', ' ').Trim();
                Xml_PAY_MST = Xml_PAY_MST.Replace("&", "&amp;").Trim();

                Xml_PAY_Det = Xml_PAY_Det.Replace('\n', ' ').Trim();
                Xml_PAY_Det = Xml_PAY_Det.Replace("&", "&amp;").Trim();


                DataTable Dt = PS.Insert_CrossingChallan_Payment_Data(Xml_PAY_MST, Xml_PAY_Det, BaseLocationCode, BaseUserName, BaseFinYear, BaseCompanyCode);
                string Voucherno = "", CrossingChallanNo = "";

                CrossingChallanNo = Dt.Rows[0]["CrossingChallanNo"].ToString();
                Voucherno = Dt.Rows[0]["VoucherNo"].ToString();
                return RedirectToAction("CrossingChallanPaymentDone", new { CrossingChallanNo = CrossingChallanNo, Voucherno = Voucherno });
            }
            catch (Exception Ex)
            {
                @ViewBag.StrError = Ex.Message;
                return View("Error");

            }
        }

        public ActionResult CrossingChallanPaymentDone(string CrossingChallanNo, string VoucherNo)
        {
            CrossingChallanViewModel CCVM = new CrossingChallanViewModel();
            CCVM.THCNo = CrossingChallanNo;
            CCVM.DockNo = VoucherNo;
            return View(CCVM);
        }

        #endregion
    }
}
