﻿using CYGNUS.Classes;
using Fleet.Classes;
using Fleet.Models;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Reflection;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class ViewPrintController : BaseController
    {
        OperationService OS = new OperationService();
        ExceptionsService ES = new ExceptionsService();
        BranchAccountingService BAS = new BranchAccountingService();
        GeneralFuncations GF = new GeneralFuncations();
        MasterService MS = new MasterService();

        public string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/App_Data/");
        //
        // GET: /ViewPrint/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DocketViewPrint(string Type, string DocketNo)
        {
            ViewBag.DocketNo = DocketNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult Tracking(string Type, string DocketNo, string DockSf)
        {
            ViewBag.DocketNo = DocketNo;
            ViewBag.DockSf = DockSf;
            ViewBag.Type = Type;
            ViewBag.ReportName = OS.Tracking_Report_Name(Type);
            return View();
        }

        public ActionResult ChallanViewPrint(string Type, string ChallanNo)
        {
            webx_pdchdr OBJPDCHDR = new webx_pdchdr();
            if (Type == "PRS" || Type == "1")
            {
                OBJPDCHDR = OS.GetPRSData(ChallanNo, Type).FirstOrDefault();
                ViewBag.ReportName = "PRS_ViewPrint";
                OBJPDCHDR.PDCTY = "P";
            }
            else if (Type == "DRS" || Type == "2")
            {
                OBJPDCHDR = OS.GetPRSData(ChallanNo, Type).FirstOrDefault();
                if (OBJPDCHDR.PDCUpdated == "Y")
                {
                    ViewBag.ReportName = "DRS_Close_ViewPrint";
                }
                else
                {
                    ViewBag.ReportName = "DRS_ViewPrint";
                }
                OBJPDCHDR.PDCTY = "D";
            }
            else if (Type == "THC" || Type == "3")
            {
                ViewBag.ReportName = "THC_ViewPrint";
                OBJPDCHDR.PDCTY = "THC";
            }
            else if (Type == "MF" || Type == "4")
            {
                ViewBag.ReportName = "Menifest_ViewPrint_NewPortal";
                OBJPDCHDR.PDCTY = "MF";
            }

            ViewBag.ChallanNo = ChallanNo;
            ViewBag.Type = Type;
            return View(OBJPDCHDR);
        }

        public ActionResult GC_List_From_08Jun16()
        {
            return View();
        }

        public ActionResult GC_List_From_08Jun16_New_DB()
        {
            return View();
        }

        public ActionResult GC_XLSGeneration(string Dockno, string Docksf, string Type)
        {
            ViewBag.Dockno = Dockno;
            ViewBag.Docksf = Docksf;
            ViewBag.Type = Type;
            TempData["ReportName"] = "CnoteView";
            return View();
        }

        public ActionResult THC_List_From_08Jun16()
        {
            return View();
        }

        public ActionResult THC_List_From_08Jun16_New_DB()
        {
            return View();
        }

        public ActionResult PDC_List_From_08Jun16()
        {
            return View();
        }

        public ActionResult PDC_List_From_08Jun16_New_DB()
        {
            return View();
        }

        public ActionResult Docket_Income_Expance()
        {
            return View();
        }

        public ActionResult Docket_Income_Expance_New_DB()
        {
            return View();
        }

        public ActionResult Cheque_ViewPrint(string VoucherNo, string Year_Prefix)
        {
            ViewBag.VoucherNo = VoucherNo;
            ViewBag.Year_Prefix = Year_Prefix;
            return View();
        }

        public ActionResult TrailBalance_08Jun16()
        {
            return View();
        }

        public ActionResult TrailBalance_08Jun16_NewDB()
        {
            return View();
        }

        public ActionResult General_Ledger_08Jun16()
        {
            return View();
        }

        public ActionResult General_Ledger_08Jun16_NewDB()
        {
            return View();
        }

        public ActionResult MR_ViewPrint(string Type, string MRSNo)
        {
            ViewBag.MRSNo = MRSNo;
            return View();
        }

        public ActionResult Expense_Entry(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View();
        }
        public ActionResult FixedVariableExpenseBillEntry(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View();
        }

        public ActionResult BillPaymentView(string VoucherNO, string Type)
        {
            ViewBag.VoucherNO = VoucherNO;
            ViewBag.Type = Type;
            if (Type == "BA_Bill")
            {
                ViewBag.ReportURL = "BA_BillPaymentView";
            }
            else if (Type == "FA_Bill")
            {
                ViewBag.ReportURL = "Franchise_BillPaymentView";
            }
            else
            {
                ViewBag.ReportURL = "BillPaymentView";

            }
            return View();
        }

        public ActionResult POBill(string VoucherNO)
        {
            ViewBag.VoucherNO = VoucherNO;
            return View();
        }

        public ActionResult BillViewPrint(string BillNo, string Type)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult CoveringViewPrint(string BillNo, string Type)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult BillDamurageViewPrint(string BillNo, string Type)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult ChequeViewPrint(string Chq_VRNO, string CHQDT)
        {
            ViewBag.Chq_VRNO = Chq_VRNO;
            ViewBag.CHQDT = CHQDT;
            return View();
        }

        public ActionResult BillDetails_ViewPrint(string Billno)
        {
            ViewBag.Billno = Billno;
            return View();
        }

        public ActionResult Bill_Portait_Invoice_ViewPrint(string Billno, string Type)
        {
            string Path = "";
            ViewBag.Billno = Billno;
            ViewBag.Type = Type;

            if (Type == "1")
            {
                ViewBag.Title = "Portrait Bill View";
              
            }
            else
            {
                ViewBag.Title = "W/O Invoice Bill View";

                try
                {
                    Path = SaveReport(Billno);
                }
                catch (Exception)
                {
                }

                byte[] fl = System.IO.File.ReadAllBytes(Path);

                Response.ContentType = "application/pdf";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.Substring(Path.LastIndexOf("/") + 1));

                // Write the file to the Response
                const int bufferLength = 10000;
                byte[] buffer = new Byte[bufferLength];
                int length = 0;
                Stream download = null;
                try
                {
                    download = new FileStream(Path, FileMode.Open, FileAccess.Read);
                    do
                    {
                        if (Response.IsClientConnected)
                        {
                            length = download.Read(buffer, 0, bufferLength);
                            Response.OutputStream.Write(buffer, 0, length);
                            buffer = new Byte[bufferLength];
                        }
                        else
                        {
                            length = -1;
                        }
                    }
                    while (length > 0);
                    Response.Flush();
                    Response.End();
                }
                finally
                {
                    if (download != null)
                        download.Close();
                }

                return File(fl, System.Net.Mime.MediaTypeNames.Application.Octet);
            }
            return View();
        }

        public ActionResult AllVoucherViewPrint(string arg1, string arg2, string arg3)
        {
            var typ = arg1.ToUpper();
            var res = arg2.Split(':');
            var voucherno = "";

            if (arg2.Contains(':'))
            {
                voucherno = res[1];
            }
            else
            {
                voucherno = arg2.Trim();
            }


            if (typ == "JOURNAL")
            {
                return RedirectToAction("VoucherViewPrint", new { VoucherNo = voucherno, Type = 3 });
            }
            else if (typ == "CASH PAYMENT" || typ == "CASH RECEIPT" || typ == "BANK PAYMENT" || typ == "BANK RECEIPT" || typ == "BOTH PAYMENT" || typ == "CC PAYMENT" || typ == "DEBIT NOTE")
            {
                if (arg3.ToUpper() == "SPECIAL COST VOUCHER")
                {
                    return RedirectToAction("VoucherViewPrint", new { VoucherNo = voucherno, Type = 5 });
                }
                else
                {
                    return RedirectToAction("VoucherViewPrint", new { VoucherNo = voucherno, Type = 2 });
                }
            }
            else if (typ == "CONTRA")
            {
                return RedirectToAction("VoucherViewPrint", new { VoucherNo = voucherno, Type = 1 });
            }
            else
            {
                return RedirectToAction("VoucherViewPrint", new { VoucherNo = voucherno, Type = 1 });
            }

        }

        public ActionResult VoucherViewPrint(string VoucherNo, string Type)
        {
            ViewBag.VoucherNo = VoucherNo;
            ViewBag.BaseYearVal = BaseYearVal;
            if (Type == "1")/* Contra Voucher */
            {
                ViewBag.ReportName = "Contra_Voucher_Print";
            }
            else if (Type == "2")/* Debit Credit Voucher */
            {
                ViewBag.ReportName = "DebitCreditVoucher";
            }
            else if (Type == "3")/* Journal Voucher */
            {
                ViewBag.ReportName = "MJV_Voucher_Print";
            }
            else if (Type == "4")/* MJV */
            {
                ViewBag.ReportName = "MJV_Voucher_Print";
            }
            else if (Type == "5")/* Special Cost Voucher */
            {
                ViewBag.ReportName = "Sepcial_Voucher_Print";
            }
            else if (Type == "6")/* Branch Accounting Voucher */
            {
                ViewBag.ReportName = "BranchAccounting_Voucher_ViewPrint";
            }
            ViewBag.Type = Type;

            return View();
        }

        public string SaveReport(string Billno)
        {
            List<ReportParameter> parameters = new List<ReportParameter>();
            parameters.Add(new ReportParameter("Billno", Billno));
            parameters.Add(new ReportParameter("Type", "0"));

            ReportViewer ReportViewer1 = new ReportViewer();

            ReportViewer1.Width = 800;
            ReportViewer1.Height = 600;
            ReportViewer1.ProcessingMode = ProcessingMode.Remote;
            IReportServerCredentials irsc = new CustomReportCredentials(ConfigurationManager.AppSettings["MvcReportViewer.Username"].ToString(), ConfigurationManager.AppSettings["MvcReportViewer.Password"].ToString(), DomainName.ToString());
            ReportViewer1.ServerReport.ReportServerCredentials = irsc;
            Uri URI = new Uri(ConfigurationManager.AppSettings["MvcReportViewer.ReportServerUrl"].ToString());
            ReportViewer1.ServerReport.ReportServerUrl = URI;
            ReportViewer1.ServerReport.ReportPath = ConfigurationManager.AppSettings["ReportPathPrefix"].ToString() + "/WOInvoiceBillViewPrint";
            ReportViewer1.ServerReport.SetParameters(parameters);

            ReportViewer1.ServerReport.Refresh();

            string mimeType;
            string encoding;
            string extension;
            string[] streams;
            Warning[] warnings;
            byte[] asPdf = ReportViewer1.ServerReport.Render("Excel", string.Empty, out mimeType, out encoding, out extension, out streams, out warnings);
            string outputPath = folderPath + Billno.Replace("/", "_") + ".xls";
            if (System.IO.File.Exists(outputPath))
                System.IO.File.Delete(outputPath);

            using (FileStream fs = new FileStream(outputPath, FileMode.Create))
            {
                fs.Write(asPdf, 0, asPdf.Length);
                fs.Close();
            }
            return outputPath;
        }

        public class CustomReportCredentials : IReportServerCredentials
        {
            private string _UserName;
            private string _PassWord;
            private string _DomainName;

            public CustomReportCredentials(string UserName, string PassWord, string DomainName)
            {
                _UserName = UserName;
                _PassWord = PassWord;
                _DomainName = DomainName;
            }

            public System.Security.Principal.WindowsIdentity ImpersonationUser
            {
                get { return null; }
            }

            public ICredentials NetworkCredentials
            {
                get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
            }

            public bool GetFormsCredentials(out Cookie authCookie, out string user,
             out string password, out string authority)
            {
                authCookie = null;
                user = password = authority = null;
                return false;
            }
        }

        public ActionResult Advice_Generation(string AdviceNo)
        {
            ViewBag.AdviceNo = AdviceNo;
            ViewBag.BaseYearValFirst = BaseYearVal;
            return View();
        }

        public ActionResult POBillViewPrint(string VoucherNO, string Type)
        {
            ViewBag.VoucherNO = VoucherNO;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult StockSubReport(string Type, string BRCD)
        {
            ViewBag.Type = Type;
            ViewBag.Brcd = BRCD;
            return View();
        }

        public ActionResult StockReport()
        {
            return View();
        }

        public ActionResult Type_Mount_DisMount(string Type)
        {
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult GRNViewPrint(string GRNNO)
        {
            ViewBag.GRNNO = GRNNO;
            return View();
        }

        public ActionResult TripSheetView(string TRIPSHEET_NO, string EXPTYPE)
        {
            ViewBag.TRIPSHEET_NO = TRIPSHEET_NO;
            ViewBag.EXPTYPE = EXPTYPE;
            return View();
        }

        public ActionResult UnLoadingSheet_ViewPrint(string LSNo)
        {
            ViewBag.LodingSheetNo = LSNo;
            ViewBag.BaseLocation = BaseLocationCode;
            return View();
        }

        public ActionResult AdviseViewPrint(string Adviceno, string FinYear)
        {
            ViewBag.Adviceno = Adviceno;
            ViewBag.FinYear = FinYear;

            return View();
        }

        public ActionResult FuelExpenceViewPrint(string VoucherNO)
        {
            ViewBag.VoucherNO = VoucherNO;

            return View();
        }

        public ActionResult Po_Generation_ViewPrint(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }

        public ActionResult Agent_Bill_ViewPrint(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }

        public ActionResult BA_Job_Sheet_PaymentView(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }

        public ActionResult CustomerBillView(string NO, string typ)
        {
            ViewBag.NO = NO;
            ViewBag.typ = typ;
            return View();
        }

        public ActionResult FuelSlip_View(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }

        public ActionResult LoadingUnloadingBillEntry(string VoucherNO)
        {
            ViewBag.VoucherNO = VoucherNO;
            return View();
        }

        public ActionResult JobOrder_ViewPrint(string No)
        {
            ViewBag.No = No;
            return View();
        }

        public ActionResult PFM_View_Print(string PFMNO, string DocType)
        {
            ViewBag.PFMNO = PFMNO;
            ViewBag.DocType = DocType;
            return View();
        }

        public ActionResult ASSIGNASSET(string GRNNO)
        {
            ViewBag.GRNNO = GRNNO;
            return View();
        }

        public ActionResult Fixed_Asset_View(string No, int Type)
        {
            ViewBag.No = No;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult Fixed_Asset_View_GRN(string No, int Type)
        {
            ViewBag.No = No;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult FixAssetGRNVIEW(string GRNNO)
        {
            ViewBag.GRNNO = GRNNO;
            return View();
        }

        public ActionResult SupplimentryBill(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View();
        }

        public ActionResult GRN_ViewPrint(string GRNNO)
        {
            ViewBag.GRNNO = GRNNO;
            return View();
        }

        public ActionResult PO_GRN_ViewPrint(string PONumber)
        {
            ViewBag.PONumber = PONumber;
            return View();
        }

        public ActionResult GRNBILLENTRYDETAILS(string billno)
        {
            ViewBag.billno = billno;
            return View();
        }

        public ActionResult GRNASSIGNREASSIGN(string ASSIGNNO, int Type)
        {
            ViewBag.ASSIGNNO = ASSIGNNO;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult BranchAccountingViewPrint(string No, string Type)
        {
            BranchAccountingService BAS = new BranchAccountingService();
            ViewBag.No = No;
            ViewBag.Type = Type;
            ViewBag.ReportName = BAS.Get_BranchAccounting_Report_Name(User.Identity.Name, BaseLocationCode, Type);
            return View();
        }

        public ActionResult LoadingUnloadingBillView(string BillNo, string Type)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult PFM_View_Print_Report(string PFMNo, string DocType)
        {
            ViewBag.PFMNo = PFMNo;
            ViewBag.DocType = DocType;
            return View();
        }

        public ActionResult Driver_Attendence_Report_Criteria()
        {
            Driver_Attendence_ViewPrint VW = new Driver_Attendence_ViewPrint();
            return View(VW);
        }

        public ActionResult Driver_Attendence_Report(Driver_Attendence_ViewPrint VW)
        {
            return View(VW);
        }

        public ActionResult ReportDashboard()
        {
            return View();
        }

        #region Branch Accounting

        #region BR Status Report

        public ActionResult BRStatus_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            Criteria.BRCD = "All";
            Criteria.BRStatus = "All";
            Criteria.ExpenseType = "All";

            return View(Criteria);
        }

        public ActionResult BRStatus_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        #region BR Status Summary Report

        public ActionResult BRStatusSummary_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            Criteria.BRCD = "All";
            Criteria.ExpenseType = "All";

            return View(Criteria);
        }

        public ActionResult BRStatusSummary_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        #region BR Analysis Report Pending Report

        public ActionResult BRAnalysis_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            Criteria.BRCD = "All";
            return View(Criteria);
        }

        public ActionResult BRAnalysis_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            return View(BRSV);
        }

        public JsonResult Get_Rate_Type_Class_(string Type)
        {
            var ListRateType = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Vehicle Wise",Value="Vehicle Wise"},
                       new SelectListItem{Text="Driver Wise",Value="Driver Wise"},
                       new SelectListItem{Text="Route Wis",Value="Route Wis"},
                       new SelectListItem{Text="Staff wise",Value="Staff wise"},
                       };
            var ListRateType7 = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       };
            var ListRateTypeConvency = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Staff wise",Value="Staff wise"},

                       };
            var ListRateTypeDriver = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Vehicle Wise",Value="Vehicle Wise"},
                       new SelectListItem{Text="Driver Wise",Value="Driver Wise"},
                       new SelectListItem{Text="Route Wis",Value="Route Wis"},

                       };
            var ListRateTypeTraveling = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Staff wise",Value="Staff wise"},
                       };
            var ListRateTypeFood = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Staff wise",Value="Staff wise"},
                       new SelectListItem{Text="Vehicle Wise",Value="Vehicle Wise"},
                       };
            var ListRateTypeTripsheet = new List<SelectListItem>
                       {
                       new SelectListItem{Text="Branch Wise",Value="Branch Wise"},
                       new SelectListItem{Text="Driver Wise",Value="Driver Wise"},
                       new SelectListItem{Text="Vehicle Wise",Value="Vehicle Wise"},
                       };


            if (Type == "7" || Type == "8" || Type == "5" || Type == "1" || Type == "10" || Type == "11" || Type == "6")
            {
                return Json(ListRateType7, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "2")
            {
                return Json(ListRateTypeConvency, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "3")
            {
                return Json(ListRateTypeTraveling, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "4")
            {
                return Json(ListRateTypeDriver, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "9")
            {
                return Json(ListRateTypeFood, JsonRequestBehavior.AllowGet);
            }
            else if (Type == "12")
            {
                return Json(ListRateTypeTripsheet, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(ListRateType, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Tripsheet Disputed Amount

        public ActionResult BRTripsheet_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            return View(Criteria);
        }

        public ActionResult BRTripsheet_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        #region Driver Insentive Report

        public ActionResult BRDriver_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            return View(Criteria);
        }

        public ActionResult BRDriver_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        #region Food Expense Register

        public ActionResult FoodExpense_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            return View(Criteria);
        }

        public ActionResult FoodExpenseViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        #region BR Performance Report

        public ActionResult BRPerformance_View_Print_ReportCriteria()
        {
            BR_StatusViewPrint Criteria = new BR_StatusViewPrint();
            return View(Criteria);
        }

        public ActionResult BRPerformance_View_PrintViewPrintList(BR_StatusViewPrint BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        #endregion

        public ActionResult PorterExpenseEntry(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View();
        }

        public JsonResult GetAllBRStatusetails(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<CYGNUS_Branch_Accounting_Master> CMP = BAS.GetAllBRStatus(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            text = user.CodeDesc
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAllRoute(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_rutmas> CMP = BAS.GetAllRoute(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            text = user.RUTNM
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetSubHeadExpense(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<CYGNUS_Branch_Accounting_Master> CMP = BAS.GetSubHeadExpense(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.LegderCode,
                            text = user.ExpenseType
                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult GetAllLocationsDetails(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_location> CMP = BAS.GetAllLocations(searchTerm);

            var users = from user in CMP
                        select new
                        {
                            id = user.LocCode,
                            text = user.LocName

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #region Expected Arrival Report

        public ActionResult ExpectedArrivalReport()
        {
            CNoteReassignFilter CRF = new CNoteReassignFilter();
            CRF.BaseLoccode = BaseLocationCode;
            CRF.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            CRF.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            CRF.RegionList_To = MS.GetRegionList("HQTR", "RO_To").ToList();

            webx_location objNew = new webx_location();
            //objNew.Loccode_To = "All";
            CRF.LocationList_To = new List<webx_location>();
            CRF.LocationList_To.Insert(0, objNew);
            CRF.LocationList_To = MS.GetRegionList("", "LO").ToList();
            CRF.RegionList_To.Insert(0, objNew);

            if (CRF.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CRF.RegionList.Insert(0, objNew);
                //CRF.RegionList_To.Insert(0, objNew);
                CRF.LocationList = new List<webx_location>();
                //CRF.LocationList_To = new List<webx_location>();
                CRF.LocationList.Insert(0, objNew);
                //CRF.LocationList_To.Insert(0, objNew);
            }
            if (CRF.LocLevel == 2)
            {
                CRF.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                //CRF.LocationList_To = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CRF.LocationList.Insert(0, objNew);
                //CRF.LocationList_To.Insert(0, objNew);
            }
            if (CRF.LocLevel == 3)
            {
                CRF.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(CRF);
        }

        public ActionResult ExpectedArrivalReportList(CNoteReassignFilter VW)
        {
            if (VW.Loccode.ToUpper() == "ALL" && VW.RO.ToUpper() == "ALL")
            {
                VW.LocLevel = 1;
            }
            else
            {
                if (VW.Loccode.ToUpper() == "ALL")
                {
                    VW.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == VW.RO.ToUpper()).FirstOrDefault().Loc_Level;
                }
                else
                {
                    VW.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == VW.Loccode.ToUpper()).FirstOrDefault().Loc_Level;
                }
            }
            if (VW.Loccode_To.ToUpper() == "ALL" && VW.RO_To.ToUpper() == "ALL")
            {
                VW.LocLevel_To = 1;
            }
            else
            {
                if (VW.Loccode_To.ToUpper() == "ALL")
                {
                    VW.LocLevel_To = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == VW.RO_To.ToUpper()).FirstOrDefault().Loc_Level;
                }
                else
                {
                    VW.LocLevel_To = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == VW.Loccode_To.ToUpper()).FirstOrDefault().Loc_Level;
                }
            }

            return View(VW);
        }

        #endregion

        #region ODA DRS/PRS Register

        public ActionResult ODADRS_PRSRegisterViewPrint()
        {
            ODADRS_PRSRegister Criteria = new ODADRS_PRSRegister();
            Criteria.BRCD = "All";
            return View(Criteria);
        }

        public ActionResult ODADRS_PRSRegisterViewPrintList(ODADRS_PRSRegister BRSV)
        {
            string FromDate = GF.FormateDate(BRSV.FromDate);
            string ToDate = GF.FormateDate(BRSV.ToDate);
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            return View(BRSV);
        }

        public JsonResult Cheked_Dockno_NO_Report(string DocketNo)
        {
            string Status = ES.Get_Dockno_NO_Report(DocketNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        #endregion

        #region Docket Ticket No View Print

        public ActionResult Docket_TicketNo_ViewPrint()
        {
            return View();
        }

        public ActionResult Docket_TicketNo_Submit(ODADRS_PRSRegister BRSV)
        {
            DataTable Dt_Name = new DataTable();

            if (BRSV.TicketNo != "" && BRSV.TicketNo != null)
            {
                Dt_Name = ES.Get_DocktNo_From_TicketNo(BRSV.TicketNo);
                string Dockno = Dt_Name.Rows[0]["DOCKNO"].ToString();
                ViewBag.Dockno = Dockno;
            }
            else
            {
                ViewBag.Dockno = BRSV.DocumentNo;
            }
            return View(BRSV);
        }

        #endregion

        #region Docket_ReferenceNo_ViewPrint

        public ActionResult Docket_ReferenceNo_ViewPrint()
        {
            return View();
        }

        public ActionResult Docket_ReferenceNo_Submit(ODADRS_PRSRegister BRSV)
        {
            DataTable Dt_Name = new DataTable();
            if (BRSV.Reference != "" && BRSV.Reference != null)
            {
                Dt_Name = ES.Get_DockeNo_From_ReferenceNo(BRSV.Reference);
                string Dockno = Dt_Name.Rows[0]["DOCKNO"].ToString();
                ViewBag.Dockno = Dockno;
            }
            else
            {
                ViewBag.Dockno = BRSV.DocumentNo;
            }
            return View(BRSV);
        }

        #endregion

        #region  Wilf Craft GC Multiple View & Print

        public ActionResult wildCraftGCCriteria()
        {
            return View();
        }

        public JsonResult Check_DocketNo(string DocketNo)
        {
            string Status = MS.Check_DocketNo_GCView(DocketNo).Replace("Docket", ViewBag.DKTCalledAS);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult wildCraftGCReport(ODADRS_PRSRegister OPR)
        {
            return View(OPR);
        }

        #endregion

        public ActionResult AttachedBillView(string BillNo, string Type)
        {
            ViewBag.BillNo = BillNo;
            ViewBag.Type = Type;
            return View();
        }

        public ActionResult ViewUserDetails(string Id)
        {
            ViewBag.userName = Id;
            return View();
        }

        public ActionResult Internal_Movement_ViewPrint(string IMNO)
        {
            ViewBag.Dockno = IMNO;
            return View();
        }


        public ActionResult RemoldtyreBillView(string BillNo)
        {
            ViewBag.Dockno = BillNo;
            return View();
        }

        public ActionResult RemoldtyreBillPaymentView(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }

        #region  Internal Movement View Print

        public ActionResult InternalMovementViewPrintCriteria()
        {
            return View();
        }

        public ActionResult InternalMovementViewPrintList(InternalMovementViewModel vm)
        {
            try
            {
                if (vm.objIntMovement != null)
                {
                    List<CYGNUS_Internal_Movement> optrkList = ES.Get_InternalMovementList(GF.FormateDate(vm.objIntMovement.FromDate), GF.FormateDate(vm.objIntMovement.ToDate), vm.objIntMovement.IMNo);
                    vm.IntMovementList = optrkList;
                }
                else
                {
                    return RedirectToAction("InternalMovementViewPrintCriteria");
                }
            }
            catch (Exception)
            {

                //throw;
            }
            return View(vm);
        }

        #endregion

        #region THC Unloading View/Print

        public ActionResult THCUnloadingViewPrint(string THCNO, string UnLoadingNo)
        {
            ViewBag.THCNO = THCNO;
            ViewBag.UnLoadingNo = UnLoadingNo;
            ViewBag.Brcd = BaseLocationCode;
            return View();
        }
        #endregion

        public ActionResult GatePassViewPrint(string MRNO)
        {
            ViewBag.MRNO = MRNO;
            ViewBag.EmpCode = BaseUserName;

            return View();
        }

        #region CrossingChallanViewPrint
        public ActionResult CrossingChallanViewPrint(string CrossingChallanNo)
        {
            ViewBag.CrossingChallanNo = CrossingChallanNo;

            return View();
        }

        #endregion

        #region IDTViewPrint
        public ActionResult IDTViewPrint(string IDT)
        {
            ViewBag.IDT = IDT;

            return View();
        }

        #endregion

       
        public ActionResult DocketTrackingprint(string dockno, string TYPE, string Docksf)
        {
            ViewBag.Dockno = dockno;
            ViewBag.TYPE = TYPE;
            ViewBag.Docksf = Docksf;

            return View();
        }
        public ActionResult SupplementaryBillView(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }

        public ActionResult FleetIncomeBillView(string NO)
        {
            ViewBag.NO = NO;
            return View();
        }


        #region IDTFreightMemo
        public ActionResult IDTFreightMemoViewPrint(string FMNO)
        {
            ViewBag.FMNO = FMNO;
            return View();
        }

        public ActionResult FreightMemoPaymentViewPrint(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }
        #endregion IDTFreightMemo

        #region BA Bill Cnot Detail

        public ActionResult BABillEntry_Entry(string DocketType, string FromDate, string ToDate)
        {
            ViewBag.DocketType = DocketType;
            ViewBag.FromDate = FromDate;
            ViewBag.ToDate = ToDate;
            ViewBag.BRCD = BaseLocationCode;
            return View();
        }
        #endregion

        public ActionResult CrossingChallanBillPaymentView(string VoucherNO)
        {
            ViewBag.VoucherNO = VoucherNO;
            return View();
        }

        public ActionResult CrossingChallan_ViewPrint(string No)
        {
            ViewBag.No = No;
            return View();
        }

        public ActionResult CrossingVoucher_ViewPrint(string No)
        {
            ViewBag.No = No;
            return View();
        }
        public ActionResult IDT_ViewPrint(string No)
        {
            ViewBag.No = No;
            return View();
        }

        public ActionResult CrossingChallanBillViewPrint(string BillNo)
        {
            ViewBag.BillNo = BillNo;
            return View();
        }


        public ActionResult GatePassReprintViewPrint(string No)
        {
            ViewBag.No = No;
            ViewBag.Empcode = BaseUserName;
            return View();
        }

        public ActionResult GatePassReprintDKTwiseViewPrint(string No)
        {
            ViewBag.Dockno = No;
            ViewBag.Docksf = ".";
            return View();
        }

        public ActionResult CnoteReprintViewPrint(string No)
        {
            ViewBag.No = No;
            ViewBag.Empcode = BaseUserName;
            return View();
        }

        public ActionResult DriverTrackerView(string DriverId)
        {
            
            ViewBag.DriverId = DriverId;
            return View();
        }

        public ActionResult CrossingVendorView(string ContractID)
        {

            ViewBag.ContractID = ContractID;
            return View();
        }

        public ActionResult CustomerContractView(string ContractID, string CustCodeName)
        {
            ViewBag.ContractID = ContractID;
            ViewBag.CustCodeName = CustCodeName;
            return View();
        }

    }
}
