﻿using CipherLib;
using Fleet.Classes;
using Fleet.Filters;
using Fleet.ViewModels;
using FleetDataService;
using FleetDataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using WebMatrix.WebData;
using Fleet.Models;
using CYGNUS.Classes;
using FleetDataService.ViewModels;
using CYGNUS.Models;
using System.Net.Mail;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class ExceptionsController : BaseController
    {
        FleetDataService.ExceptionsService ES = new FleetDataService.ExceptionsService();
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        FleetDataService.FleetService FS = new FleetDataService.FleetService();

        GeneralFuncations GF = new GeneralFuncations();
        string ControllerName = "ExceptionsController";

        #region MF Cancellation

        public ActionResult MFCancellation()
        {
            MFCancellationViewModel MFCVM = new MFCancellationViewModel();
            return View(MFCVM);
        }

        public JsonResult GetMFCalDetail(string tcno)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                DataTable Dt = ES.Get_Data_From_MFNo(tcno);
                List<webx_THC_SUMMARY> MFList = DataRowToObject.CreateListFromTable<webx_THC_SUMMARY>(Dt);
                string manual_tcno = "";
                string TCDT = "";
                string tcbr = "";
                bool flag = false;
                if (MFList.Count > 0)
                {
                    flag = true;
                    manual_tcno = Dt.Rows[0]["manual_tcno"].ToString();
                    TCDT = Dt.Rows[0]["TCDT"].ToString();
                    tcbr = Dt.Rows[0]["tcbr"].ToString();
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        manual_tcno = manual_tcno,
                        TCDT = TCDT,
                        tcbr = tcbr,
                        flag = flag
                    }
                };
                //return Json(new
                //{
                //    Data = new
                //    {
                //        manual_tcno = manual_tcno,
                //        TCDT = TCDT,
                //        tcbr = tcbr,
                //        flag = flag
                //    }
                //}, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetMFCalDetail", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        [HttpPost]
        public ActionResult MFCancellation(MFCancellationViewModel MFCVM)
        {
            DataTable DT = ES.usp_Manifest_Do_Cancel(MFCVM.MFCnlModel.tcno, BaseUserName);

            return RedirectToAction("MF_CancellationDone", new { tcno = MFCVM.MFCnlModel.tcno });
        }

        public ActionResult MF_CancellationDone(string TCNO)
        {
            ViewBag.TCNO = TCNO;
            return View();
        }

        #endregion

        #region THC Cancellation

        public ActionResult THCCancellation()
        {
            MFCancellationViewModel THVVM = new MFCancellationViewModel();
            THVVM.THCCnlList = new List<vw_MFCancellation>();
            ViewBag.Flag = "THCCancellationlist";
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult THCCancellationlist(MFCancellationViewModel VM)
        {
            List<vw_MFCancellation> ListTHCln = new List<vw_MFCancellation>();
            DataTable DT = ES.ListOFTHCCancellation(BaseLocationCode, GF.FormateDate(VM.MFCnlModel.FromDate), GF.FormateDate(VM.MFCnlModel.ToDate), VM.MFCnlModel.thcnoumber);
            ListTHCln = DataRowToObject.CreateListFromTable<vw_MFCancellation>(DT);
            VM.THCCnlList = ListTHCln;
            if (VM.THCCnlList.Count > 0)
            {
                ViewBag.Flag = "THCCancellationSubmit";
            }
            else
            {
                ViewBag.Flag = "THCCancellation";
            }
            return View("THCCancellation", VM);
        }

        public ActionResult THCCancellationSubmit(MFCancellationViewModel VM, List<vw_MFCancellation> THCclnList)
        {
            string xmlTHC = "<root>";
            string TCNO = "";
            DataTable DT = new DataTable();
            try
            {
                foreach (var item in THCclnList.Where(c => c.IsChecked == true).ToList())
                {
                    xmlTHC = xmlTHC + "<THC>" + "<THCNO>" + item.THCNO + "</THCNO>" + "</THC>";
                    TCNO = item.THCNO + ',' + TCNO;
                }
                xmlTHC = xmlTHC + "</root>";
                DT = ES.usp_XML_THC_Cancellation(xmlTHC, BaseUserName);
            }
            catch (Exception Ex)
            {
                ViewBag.StrError = "Error : " + Ex.Message;
                return View("Error");
            }

            return RedirectToAction("THCCancellationDone", new { TCNO = TCNO });
        }
        public ActionResult THCCancellationDone(string TCNO)
        {
            ViewBag.TCNO = TCNO;
            return View();
        }

        #endregion

        #region PRS/DRS Cancellation

        public ActionResult PDCCancellationCriteria()
        {
            MFCancellationViewModel THVVM = new MFCancellationViewModel();
            THVVM.PDCCnlList = new List<vw_PDCCancellation>();
            ViewBag.Flag = "PDCCancellationList";
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult PDCCancellationList(MFCancellationViewModel VM)
        {
            List<vw_PDCCancellation> ListPDCCln = new List<vw_PDCCancellation>();
            DataTable DT = ES.ListOFPDCCancellation(BaseLocationCode, VM.MFCnlModel.FilterType, GF.FormateDate(VM.MFCnlModel.FromDate), GF.FormateDate(VM.MFCnlModel.ToDate), VM.MFCnlModel.thcnoumber);
            ListPDCCln = DataRowToObject.CreateListFromTable<vw_PDCCancellation>(DT);
            VM.PDCCnlList = ListPDCCln;
            if (ListPDCCln.Count > 0)
            {
                ViewBag.Flag = "PDCCancellationSubmit";
            }
            else
            {
                ViewBag.Flag = "PDCCancellationList";
            }
            return View("PDCCancellationCriteria", VM);
        }

        public ActionResult PDCCancellationSubmit(MFCancellationViewModel VM, List<vw_PDCCancellation> PDCclnList)
        {
            try
            {
                string xmlPDC = "<root>";
                string PDCNO = "";
                foreach (var item in PDCclnList.Where(c => c.IsChecked == true).ToList())
                {
                    xmlPDC = xmlPDC + "<PDC>" + "<PDCNO>" + item.pdcno + "</PDCNO>" + "</PDC>";
                    if (PDCNO == "")
                    {
                        PDCNO = item.pdcno;
                    }
                    else
                    {
                        PDCNO = item.pdcno + ',' + PDCNO;
                    }

                }
                xmlPDC = xmlPDC + "</root>";
                DataTable DT = ES.usp_XML_PDC_Cancellation(xmlPDC, BaseUserName, VM.MFCnlModel.FilterType);
                return RedirectToAction("PDCCancellationDone", new { PDCNO = PDCNO });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
        }

        public ActionResult PDCCancellationDone(string PDCNO)
        {
            ViewBag.PDCNO = PDCNO;
            return View();
        }

        #endregion

        #region Loding Sheet Cancellation

        public ActionResult LodingSheetCnl()
        {
            MFCancellationViewModel MFCVM = new MFCancellationViewModel();
            return View(MFCVM);
        }

        [HttpPost]
        public ActionResult LodingSheetCnl(MFCancellationViewModel VM)
        {
            string commandText = "Select TCBR,FINAL_TC from webx_TCHDR_tmp where TCNO='" + VM.MFCnlModel.LS + "'";
            DataTable Dt = GF.getdatetablefromQuery(commandText);
            VM.MFCnlModel.tcbr = Dt.Rows[0]["TCBR"].ToString();
            VM.MFCnlModel.FINAL_TC = Dt.Rows[0]["FINAL_TC"].ToString();
            VM.MFCnlModel.THCBR = BaseLocationCode;
            if (VM.MFCnlModel.tcbr != VM.MFCnlModel.THCBR)
            {
                ViewBag.Message = "Loading Sheet doesn't belongs to this location ,therefore cann't delete !!!";
            }
            else if (VM.MFCnlModel.FINAL_TC == "Y")
            {
                ViewBag.Message = "This Loading Sheet No.: " + VM.MFCnlModel.LS + " is already converted to manifest ,therefore cann't delete !!!";
            }
            else
            {
                string sql_delete = "Update webx_trans_docket_status set LS=NULL where LS='" + VM.MFCnlModel.LS + "'";
                DataTable Dt1 = GF.getdatetablefromQuery(sql_delete);
                ViewBag.Message = "Loading Sheet No. : " + VM.MFCnlModel.LS + " has been deleted successfully .";
            }
            return View("LodingSheetCnlDone", VM);
        }


        #endregion

        #region Thc Financial Tracker

        public ActionResult ThcFinancialTrackerCriteria()
        {
            ThcFinancialTrackerCriteria TFTCM = new ThcFinancialTrackerCriteria();
            return View(TFTCM);
        }

        public JsonResult CheckValidDOCNO(string DocType, string DocNo, string NumberType)
        {
            string strResult = ES.CheckValidDOCNO(DocType, DocNo, NumberType);
            if (strResult == "0")
            {
                return Json(new { CNT = true }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { CNT = false }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ThcFinancialTracker(ThcFinancialTrackerCriteria FTM)
        {
            return View(FTM);
        }

        #endregion

        #region PO Cancellation
        public ActionResult POCancellationCriteria()
        {
            POCancellationCriteria TFTCM = new POCancellationCriteria();
            return View(TFTCM);
        }

        public ActionResult POCancellation(POCancellationCriteria POCCM)
        {
            POCancellationViewModel POCVM = new POCancellationViewModel();
            POCVM.ListPOC = ES.GetWorkGroupObject(POCCM).ToList();
            return View(POCVM);
        }

        public ActionResult POCancellationDone(POCancellationViewModel POCVM, List<POCancellation> POCANCEL)
        {

            string[] strPOno = POCVM.POC.PONO.ToString().Split(',');
            string ponm = "";
            POCVM.ListPOC = POCANCEL.Where(c => strPOno.Contains(c.PONO)).ToList();
            foreach (var row in POCVM.ListPOC)
            {
                DataTable Dt = ES.AddPOCancellation(row.PONO, GF.FormateDate(POCVM.POC.POCANCELDATE), POCVM.POC.Reason, BaseUserName);
                if (Dt.Rows[0]["POno"].ToString() != "" || Dt.Rows[0]["POno"].ToString() != null)
                {
                    ponm = ponm == "" ? Dt.Rows[0]["POno"].ToString() : ponm + "," + Dt.Rows[0]["POno"].ToString();
                    ViewBag.TranXaction = "Done";
                }
                else
                {
                    ViewBag.TranXaction = "Not  Done";
                }
            }
            string[] strPONo = ponm.ToString().Split(',');
            POCVM.ListPOC = POCANCEL.Where(c => strPONo.Contains(c.PONO)).ToList();
            return View(POCVM.ListPOC);
        }
        #endregion

        #region Edit DFM
        public ActionResult EditDFMQuery()
        {
            FMFilter FMF = new FMFilter();
            FMF.BaseLoccode = BaseLocationCode;
            return View(FMF);
        }

        public ActionResult EditDFM(FMFilter FMF)
        {
            DFMLISTViewModel DLVM = new DFMLISTViewModel();
            DLVM.FMF = FMF;
            DLVM.VWFDHD = ES.GetDFMHaderObject(FMF.FmNo).FirstOrDefault();
            DLVM.ListDLM = ES.GetDFMGridObject(FMF.FmNo).ToList();
            return View(DLVM);
        }
        public ActionResult EditDFMDone(DFMLISTViewModel DLVM)
        {
            DataTable Dt = ES.UpdateDFMInfo(DLVM, BaseUserName);
            if (Dt.Rows[0]["FMNO"].ToString() != "" || Dt.Rows[0]["FMNO"].ToString() != null)
            {
                ViewBag.FMNo = Dt.Rows[0]["FMNO"].ToString();
                ViewBag.TranXaction = "Done";
            }
            else
            {
                ViewBag.FMNo = Dt.Rows[0]["FMNO"].ToString();
                ViewBag.TranXaction = "Not Done";
            }
            return View();
        }

        #endregion

        #region Bill Entry Cancellation

        public ActionResult BillCancellation()
        {
            BillEntryCancellation chq = new BillEntryCancellation();
            return View(chq);
        }
        public ActionResult UpdateBillCancellationlist(BillEntryCancellation Bill)
        {
            try
            {
                List<webx_BILLMST> BillNo = ES.BillCancellationList(GF.FormateDate(Bill.BillNo.DATEFROM), GF.FormateDate(Bill.BillNo.DATETO), Bill.BillNo.BILLNO, Bill.BillNo.BILLTYPE);
                Bill.BillNOList = BillNo;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "UpdateBillCancellationlist", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                string exmess = ex.Message.ToString().Replace('\n', '_');
                return View("Error", new HandleErrorInfo(ex, "Controller", "Action"));
            }
            return View(Bill);
        }
        public ActionResult BillEntryCancellationSubmit(BillEntryCancellation Bill, List<webx_BILLMST> BillNumber)
        {
            string BILLNO = "", TranXaction = "";
            DataTable Dt_Name = new DataTable();
            try
            {
                foreach (var item in BillNumber.Where(c => c.IsChecked == true).ToList())
                {
                    var Year = BaseFinYear.Split('-');
                    var Year1 = Year[0].ToString();
                    var CancelDate = Bill.BillNo.BGNDT.ToString("dd MMM yyyy");
                    Dt_Name = ES.BillCancellationListsubmit(item.BILLNO, item.betype, CancelDate, Bill.BillNo.CancelRession, BaseUserName, Year1);
                    BILLNO = Dt_Name.Rows[0][1].ToString();
                    TranXaction = Dt_Name.Rows[0][0].ToString();
                }
            }
            catch
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + TranXaction.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BillEntryCancellationDone", new { BILLNO = BILLNO, TranXaction = TranXaction });
        }
        public ActionResult BillEntryCancellationDone(string BILLNO, string TranXaction)
        {
            ViewBag.BILLNO = BILLNO;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region Voucher Cancellation

        public ActionResult VoucherCancellation()
        {
            VoucherCancellation Voucher = new VoucherCancellation();
            Voucher.ListWAIM = new List<webx_acctinfo>();

            return View(Voucher);
        }

        public ActionResult ADDVoucherCancellation(int id)
        {
            webx_acctinfo WMFSM = new webx_acctinfo();
            WMFSM.SrNo = id;
            return PartialView("_VoucherCancellationAddRow", WMFSM);
        }

        public ActionResult VoucherCancellationSubmit(VoucherCancellation Voucher, List<webx_acctinfo> VoucherList)
        {
            string VoucherNo = "", TranXaction = "";

            try
            {
                foreach (var item in VoucherList)
                {
                    if (Voucher.WAIM.VoucherNo != "")
                    {
                        DataTable DT = new DataTable();
                        DT = ES.VoucherCancellationProcess(BaseYearVal, item.VoucherNo, GF.FormateDate(item.CDate), BaseUserName, Voucher.WAIM.cancel);
                        //TranXaction = DT.Rows[0][0].ToString();
                    }
                    VoucherNo += item.VoucherNo + ",";
                }

            }

            catch
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + TranXaction.ToString().Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("VoucherCancellationDone", new { VoucherNo = VoucherNo, TranXaction = TranXaction });
        }

        public ActionResult VoucherCancellationDone(string VoucherNo, string TranXaction)
        {
            ViewBag.VoucherNo = VoucherNo;
            // ViewBag.TranXaction = TranXaction;


            return View();
        }

        public JsonResult Valid_Vocher_No(string VoucherNo)
        {
            string Status = ES.GetVocherDate(VoucherNo, BaseYearVal);

            string[] stttring = Status.Split('-');
            string Message = stttring[0].Trim();
            string Date = "";

            if (Message == "Done")
            {
                Date = stttring[1];
            }


            return new JsonResult()
            {
                Data = new
                {
                    Status = Message,
                    Date = Date,
                }
            };
        }

        #endregion

        #region  ReAssign BillSubmissionn Collection

        public ActionResult ReAssnBillSubmColln()
        {
            ReAssnBillSubmColln WBMM = new ReAssnBillSubmColln();
            WBMM.ListWBMM = new List<webx_BILLMST>();

            return View(WBMM);
        }

        public ActionResult ADDReAssnBillSubmCollnn(int id)
        {
            webx_BILLMST WMFSM = new webx_BILLMST();
            WMFSM.SrNo = id;
            return PartialView("_ReAssnBillSubmCollnAddRow", WMFSM);
        }

        public JsonResult GetLocation_NameJson(string SerchGroup)
        {
            List<webx_location> ListGroups = new List<webx_location> { };
            try
            {
                DataTable Dt_Location = ES.GetExistingLocation(SerchGroup);
                string BILLSUBBRCD = "";
                string BILLCOLBRCD = "";
                string BILLSTATUS = "";
                if (Dt_Location.Rows.Count > 0)
                {
                    BILLSUBBRCD = Dt_Location.Rows[0]["BILLSUBBRCD"].ToString();
                    BILLCOLBRCD = Dt_Location.Rows[0]["BILLCOLBRCD"].ToString();
                    BILLSTATUS = Dt_Location.Rows[0]["BILLSTATUS"].ToString().ToUpper();
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        BILLSUBBRCD = BILLSUBBRCD,
                        BILLCOLBRCD = BILLCOLBRCD,
                        BILLSTATUS = BILLSTATUS,
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetGroup_NameJson", "Json", "Listing", ex.Message);
                return Json(ListGroups);
            }
        }

        //public JsonResult GetSubmissonLocation_NameJson(string SerchGroup)
        //{
        //    List<webx_location> ListGroups = new List<webx_location> { };
        //    try
        //    {
        //        DataTable Dt_Location = ES.GetSubmissionocation(SerchGroup);
        //        string Result = "0";
        //        if (Dt_Location.Rows.Count > 0)
        //        {
        //            Result = Dt_Location.Rows[0][0].ToString();
        //        }
        //        return Json(Result, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        Error_Logs(ControllerName, "GetGroup_NameJson", "Json", "Listing", ex.Message);
        //        return Json(ListGroups);
        //    }
        //}

        public ActionResult ReAssnBillSubmCollnSubmit(VoucherCancellation Voucher, List<webx_BILLMST> BillList)
        {
            string BILLNO = "", TranXaction = "Done";
            DataTable Dt_Name = new DataTable();
            try
            {

                foreach (var item in BillList)
                {
                    if (item.BILLSTATUS.CompareTo("BILL GENERATED") == 0)
                    {
                        Dt_Name = ES.BillGenerated(item.SubLocation, item.NewLocation, item.BILLNO);
                    }
                    else if (item.BILLSTATUS.CompareTo("BILL SUBMITTED") == 0)
                    {
                        Dt_Name = ES.BillSubmitted(item.NewLocation, item.BILLNO);
                    }

                    if (BILLNO == "")
                    {
                        BILLNO = Dt_Name.Rows[0][1].ToString();
                    }
                    else
                    {
                        BILLNO = BILLNO + "," + Dt_Name.Rows[0][1].ToString();
                    }
                    //TranXaction = Dt_Name.Rows[0][0].ToString();
                    //return RedirectToAction("ReAssnBillSubmCollnDone", new { BILLNO = BILLNO, TranXaction = TranXaction });
                }

            }
            catch
            {
                TranXaction = "Not Done";
            }
            return RedirectToAction("ReAssnBillSubmCollnDone", new { BILLNO = BILLNO, TranXaction = TranXaction });
        }

        public ActionResult ReAssnBillSubmCollnDone(string BILLNO, string TranXaction)
        {
            ViewBag.BILLNO = BILLNO;
            ViewBag.TranXaction = TranXaction;
            return View();
        }

        #endregion

        #region DEPT/Detention CNote

        public ActionResult CNoteDetention()
        {
            CNoteDetentionViewModel THVVM = new CNoteDetentionViewModel();
            THVVM.CnotCnlList = new List<CNoteDetentionList>();
            ViewBag.Flag = "CNoteDetentionlist";
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult CNoteDetentionlist(CNoteDetentionViewModel VM)
        {
            List<CNoteDetentionList> ListTHCln = new List<CNoteDetentionList>();
            DataTable DT = ES.ListOFCnotDetention(BaseLocationCode, GF.FormateDate(VM.CnotDetModel.FromDate), GF.FormateDate(VM.CnotDetModel.ToDate), VM.CnotDetModel.THCNO);
            ListTHCln = DataRowToObject.CreateListFromTable<CNoteDetentionList>(DT);
            VM.CnotCnlList = ListTHCln;
            if (ListTHCln.Count > 0)
            {
                ViewBag.Flag = "CNoteDetentionSubmit";
            }
            else
            {
                ViewBag.Flag = "CNoteDetention";
            }

            return View("CNoteDetention", VM);
        }

        public ActionResult CNoteDetentionSubmit(List<CNoteDetentionList> CNotclnList)
        {

            String hidPO = "";
            foreach (var item in CNotclnList.Where(c => c.IsChecked == true).ToList())
            {
                DataTable objDT = new DataTable();
                objDT = ES.Insert_Webx_Docket_Detained_History(item.dockno, item.docksf, item.DetainedSysdate, BaseUserName, BaseLocationCode, item.Detained_On_days, item.DetentionReason);
                if (hidPO == "")
                {
                    hidPO = item.dockno;
                }
                else
                {
                    hidPO = hidPO + "," + item.dockno;
                }
            }
            return RedirectToAction("CNoteDetentionDone", new { dockno = hidPO });
        }

        public ActionResult CNoteDetentionDone(string dockno)
        {
            ViewBag.dockno = dockno;
            return View();
        }

        #endregion

        #region DEPT/Remove CNote From Detention

        public ActionResult CNoteRemoveDetention()
        {
            CNoteDetentionViewModel THVVM = new CNoteDetentionViewModel();
            THVVM.CnotCnlList = new List<CNoteDetentionList>();
            ViewBag.Flag = "RemoveCNoteDetentionlist";
            return View(THVVM);
        }

        [HttpPost]
        public ActionResult RemoveCNoteDetentionlist(CNoteDetentionViewModel VM)
        {
            List<CNoteDetentionList> ListTHCln = new List<CNoteDetentionList>();
            DataTable DT = ES.ListOFRemoveCnotDetention(BaseLocationCode, GF.FormateDate(VM.CnotDetModel.FromDate), GF.FormateDate(VM.CnotDetModel.ToDate), VM.CnotDetModel.THCNO);
            ListTHCln = DataRowToObject.CreateListFromTable<CNoteDetentionList>(DT);
            VM.CnotCnlList = ListTHCln;
            if (ListTHCln.Count > 0)
            {
                ViewBag.Flag = "RemoveCNoteDetentionSubmit";
            }
            else
            {
                ViewBag.Flag = "CNoteRemoveDetention";
            }

            return View("CNoteRemoveDetention", VM);
        }

        public ActionResult RemoveCNoteDetentionSubmit(List<CNoteDetentionList> CNRemovList)
        {
            String hidPO = "";
            foreach (var item in CNRemovList.Where(c => c.IsChecked == true).ToList())
            {
                DataTable objDT = new DataTable();
                objDT = ES.Update_Webx_Docket_Detained_History(item.dockno, item.docksf, item.DetainedSysdate, BaseUserName, BaseLocationCode, item.DetentionReason, item.RemovalReason);
                if (hidPO == "")
                {
                    hidPO = item.dockno;
                }
                else
                {
                    hidPO = hidPO + "," + item.dockno;
                }
            }
            return RedirectToAction("RemoveCNoteDetentionDone", new { dockno = hidPO });
        }

        public ActionResult RemoveCNoteDetentionDone(string dockno)
        {
            ViewBag.dockno = dockno;
            return View();
        }

        #endregion

        #region Docket Cancellation

        public ActionResult DocketCancellation()
        {
            Cnotecancellation CNCM = new Cnotecancellation();
            CNCM.DocketList = new List<VWNET_WebX_Docket_status>();
            ViewBag.Flag = "DocketCancellationlist";
            return View(CNCM);
        }

        [HttpPost]
        public ActionResult DocketCancellationlist(Cnotecancellation VM)
        {
            List<VWNET_WebX_Docket_status> listDocket = new List<VWNET_WebX_Docket_status>();
            DataTable DT = ES.ListOFDocketCancellation(BaseLocationCode, GF.FormateDate(VM.FromDate), GF.FormateDate(VM.ToDate), VM.CNote);
            listDocket = DataRowToObject.CreateListFromTable<VWNET_WebX_Docket_status>(DT);
            VM.DocketList = listDocket;
            if (VM.DocketList.Count > 0)
            {
                ViewBag.Flag = "DocketCancellationSubmit";
            }
            else
            {
                ViewBag.Flag = "DocketCancellation";
            }
            return View("DocketCancellation", VM);
        }

        [HttpPost]
        public ActionResult DocketCancellationSubmit(Cnotecancellation VM, List<VWNET_WebX_Docket_status> DocketclnList)
        {
            string cancelledDocket = string.Empty, unCancelledDocket = string.Empty;
            try
            {
                string docket_xml = "<root>";
                DocketclnList.ForEach(x =>
                {
                    if (x.IsChecked)
                    {
                        docket_xml = docket_xml + "<Docket>";
                        docket_xml = docket_xml + "<DockNo>" + x.dockno + "</DockNo>";
                        docket_xml = docket_xml + "<CancelRemarks>" + x.CancelRemarks + "</CancelRemarks>";
                        docket_xml = docket_xml + "</Docket>";
                    }
                });
                docket_xml = docket_xml + "</root>";

                DataTable dt = ES.MultipleDocketCancellation(docket_xml, BaseUserName);
                int status = Convert.ToInt32(dt.Rows[0]["Status"]);
                string errMessage = Convert.ToString(dt.Rows[0]["ErrorMessage"]);
                cancelledDocket = Convert.ToString(dt.Rows[0]["CancelledDocket"]);
                unCancelledDocket = Convert.ToString(dt.Rows[0]["UnCancelledDocket"]);

                if (status == 0)
                {
                    throw new Exception(errMessage);
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Session Expired OR DataBase Updation Exception Occured " + ex.Message.Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("DocketCancellationDone", new { cancelledDocket = cancelledDocket, unCancelledDocket = unCancelledDocket, Tranxaction = "DONE" });
        }
        public JsonResult checkDocketCancellation(string Id)
        {
            //string strResult = ES.CheckValidDocket(Id, BaseLocationCode);
            //string[] ResultArr = strResult.Split('|');
            //return Json(new { CNT = ResultArr[0], Message = ResultArr[1] }, JsonRequestBehavior.AllowGet);

            DataTable DT = new DataTable();
            DT = ES.CheckValidDocket(Id, BaseLocationCode);
            string CNT = DT.Rows[0]["cnt"].ToString();
            string Message = DT.Rows[0]["msg"].ToString();

            return new JsonResult()
            {
                Data = new
                {
                    CNT = CNT,
                    Message = Message
                }
            };
        }

        //[HttpPost]
        //public ActionResult DocketCancellation(Cnotecancellation CNCM)
        //{
        //    var No = "";
        //    var Tranxaction = "";
        //    DataTable Dt = new DataTable();
        //    try
        //    {
        //        Dt = ES.EditNoteCancellation(CNCM.CNote, CNCM.Reason, BaseUserName);
        //        No = Dt.Rows[0]["DOCKNO"].ToString();
        //        Tranxaction = "Done";
        //    }
        //    catch (Exception)
        //    {
        //        No = Dt.Rows[0]["DOCKNO"].ToString();
        //        Tranxaction = "Not Done";
        //    }
        //    return RedirectToAction("DocketCancellationDone", new { Cnote = No, Tranxaction = Tranxaction });
        //}

        [HttpPost]
        public ActionResult DocketCancellation(WebX_Master_Docket Docket)
        {
            DateTime startDate1 = Convert.ToDateTime(FinYearStartDate);
            DateTime EnddateDate1 = Convert.ToDateTime(FinYearEndDate);
            string startDate = startDate1.ToString("dd MMM yyyy");
            string EndDate = EnddateDate1.ToString("dd MMM yyyy");
            string PayBas = "";
            string hdndockyear = "";

            DataTable Result = ES.CheckDocketStatus(Docket.DOCKNO, startDate, EndDate);
            if (Result.Rows.Count <= 0)
            {
                ViewBag.StrError = "Either DKT doesn't Exists or doesn't lie in current Financial Year.";
                return View("Error");
            }
            if (Result.Rows.Count > 0)
            {
                PayBas = Convert.ToString(Result.Rows[0]["paybas"].ToString());
                hdndockyear = Convert.ToString(Result.Rows[0]["dockdate"]).Substring(6, 4);
            }
            string hdnflagbill = ES.GetDefaultValue("FLAG_" + PayBas + "BILL");
            Result = ES.GetTable(Docket.DOCKNO);
            if (Result.Rows.Count > 0)
            {
                if (Convert.ToString(Result.Rows[0]["cancelled"]).CompareTo("Y") == 0)
                {
                    ViewBag.StrError = "DKT No " + Docket.DOCKNO + " is already Cancelled";
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["thc"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = ViewBag.THCCalledAs + Result.Rows[0]["thc"].ToString() + " is Generated for DKT No " + Docket.DOCKNO;
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["mf"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "Manifest " + Result.Rows[0]["mf"].ToString() + " is Generated for DKT No " + Docket.DOCKNO;
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["finalized"]).CompareTo("Y") == 0 || (Convert.ToString(Result.Rows[0]["billed"]).CompareTo("Y") == 0 && PayBas == "P02"))
                {
                    ViewBag.StrError = ViewBag.DKTCalledAs + " is Finalized OR bill is generated";
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["ls"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "Loading Sheet " + Result.Rows[0]["ls"].ToString() + " is Generated for DKT " + Docket.DOCKNO;
                    return View("Error");
                }

                if (Convert.ToString(Result.Rows[0]["drs"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "DRS " + Result.Rows[0]["drs"].ToString() + " is Generated for DKT " + Docket.DOCKNO;
                    return View("Error");
                }

                if (Convert.ToString(Result.Rows[0]["delivered"]).CompareTo("Y") == 0)
                {
                    ViewBag.StrError = "DKT No " + Docket.DOCKNO + " is Delivered";
                    return View("Error");
                }
                if (hdnflagbill.CompareTo("Y") != 0)
                {
                    if (Convert.ToString(Result.Rows[0]["billed"]).CompareTo("Y") == 0)
                    {
                        ViewBag.StrError = "Bill is Generated for DKT " + Docket.DOCKNO;
                        return View("Error");
                    }
                }
            }
            if (Result.Rows.Count <= 0)
            {
                ViewBag.StrError = Docket.DOCKNO + " doesn't Exists or Not in current Financial Year";
                return View("Error");
            }
            string billno = ES.GetDocketBillNo(Docket.DOCKNO, PayBas);
            string billstatus = ES.GetBillStatus(billno);

            if (billstatus.CompareTo("BILL COLLECTED") == 0)
            {
                ViewBag.StrError = "Bill is Closed. Not Possible to Cancel DKT";
                return View("Error");
            }

            double amt = 0;
            try
            {
                DataTable DT1 = ES.ISBillAvailable(billno);
                if (DT1.Rows.Count > 0)
                {
                    string QueryString = "SELECT billamt-pendamt FROM webx_billmst WHERE billno='" + billno + "'";
                    amt = Convert.ToDouble(GF.executeScalerQuery(QueryString));
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.Replace('\n', '_');
                return View("Error");
            }
            if (amt != 0)
            {
                ViewBag.StrError = "BILL IS COLLECTED PARTIALLY OF FULLY. CAN NOT CANCEL DKT";
                return View("Error");
            }
            try
            {
                ES.Cancellation(BaseUserName, Docket.Reason, Docket.DOCKNO);
                if (PayBas.CompareTo("P01") == 0 || PayBas.CompareTo("P03") == 0)
                {
                    if (billstatus.CompareTo("BILL GENERATED") == 0 || billstatus.CompareTo("BILL SUBMITTED") == 0)
                    {
                        ES.Updatewithbill(BaseUserName, Docket.Reason, billno);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Session Expired OR DataBase Updation Exception Occured " + ex.Message.Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("DocketCancellationDone", new { Cnote = Docket.DOCKNO, Tranxaction = "DONE" });
        }


        public ActionResult DocketCancellationDone(string cancelledDocket,string unCancelledDocket, string Tranxaction)
        {
            ViewBag.CancelledDocket = cancelledDocket;
            ViewBag.UnCancelledDocket = unCancelledDocket;
            ViewBag.Tranxaction = Tranxaction;
            return View();
        }

        #endregion

        #region  Vendor Code Change

        public ActionResult VendorCodeChange()
        {
            BalanceLocationChangeViewModel BLCVM = new BalanceLocationChangeViewModel();
            BLCVM.BaseLoccode = BaseLocationCode;
            BLCVM.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            BLCVM.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (BLCVM.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                BLCVM.RegionList.Insert(0, objNew);
                BLCVM.LocationList = new List<webx_location>();
                BLCVM.LocationList.Insert(0, objNew);
            }
            if (BLCVM.LocLevel == 2)
            {
                BLCVM.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                BLCVM.LocationList.Insert(0, objNew);
            }
            if (BLCVM.LocLevel == 3)
            {
                BLCVM.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(BLCVM);
        }

        [HttpPost]
        public ActionResult VendorCodeChangeList(BalanceLocationChangeViewModel VC)
        {

            try
            {
                var Year = BaseFinYear.Split('-');
                VC.BaseLoccode = BaseLocationCode;
                var Year1 = Year[0].ToString();
                if (VC.DocType == "THC")
                {
                    ViewBag.THCNo = "THC";
                }
                else
                {
                    ViewBag.PDCNo = "PDC";
                }

                if (VC.DOC_NO == null)
                {
                    VC.SelectionType = "0";
                }
                else
                {
                    VC.SelectionType = "1";
                }
                List<webx_VENDOR_HDR> WVHL = ES.GetVendorChange(VC.DocType, GF.FormateDate(VC.FromDate), GF.FormateDate(VC.ToDate), VC.VENDORCODE, VC.DOC_NO, BaseFinYear.Split('-')[0].ToString(), VC.SelectionType, VC.RO, VC.Loccode);
                VC.WVHList = WVHL;

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error");
            }
            return View(VC);
        }

        public ActionResult VendorCodeChangeSubmit(BalanceLocationChangeViewModel VC, List<webx_VENDOR_HDR> vendor)
        {
            string THCNo = "", PDCNo = "", Vendorcode = "", Vendorname = "", TranXaction = "NotDone";

            DataTable Dt_Name = new DataTable();
            try
            {
                string XML_Det = "<root>";
                foreach (var item in vendor.Where(c => c.IsChecked == true).ToList())
                {
                    webx_VENDOR_HDR WFTD = new webx_VENDOR_HDR();

                    if (VC.DocType == "THC")
                    {
                        if (THCNo == "")
                        {
                            THCNo = item.THCNo.ToString();
                        }
                        else
                        {
                            THCNo = THCNo + "," + item.THCNo;
                        }
                    }
                    else
                        if (PDCNo == "")
                    {
                        PDCNo = item.PDCNo.ToString();
                    }
                    else
                    {
                        PDCNo = PDCNo + "," + item.PDCNo;
                    }


                    //string[] Groupcode = item.VENDORCODE.Split(':');
                    //Dt_Name = ES.VendorNameGate(item.NewVENDORCODENAME);
                    Vendorcode = item.VENDORCODE;
                    Vendorname = ES.VendorNameGate(item.VENDORCODE).Rows[0][0].ToString();

                    XML_Det = XML_Det + "<webx_VENDOR_HDR>";
                    XML_Det = XML_Det + "<Vendorcode>" + Vendorcode + "</Vendorcode>";
                    XML_Det = XML_Det + "<Vendorname>" + Vendorname + "</Vendorname>";
                    if (VC.DocType == "THC")
                    {
                        XML_Det = XML_Det + "<THCNo>" + item.THCNo + "</THCNo>";
                    }
                    else
                    {
                        XML_Det = XML_Det + "<PDCNo>" + item.PDCNo + "</PDCNo>";
                    }
                    XML_Det = XML_Det + "</webx_VENDOR_HDR>";

                    vendor.Add(WFTD);
                }
                XML_Det = XML_Det + "</root>";
                Dt_Name = ES.VendorupdateThc(XML_Det.ReplaceSpecialCharacters(), VC.DocType);
                //if (VC.DocType == "THC")
                //{
                //    Dt_Name = ES.VendorupdateThc(XML_Det, VC.DocType);
                //}
                //else if (VC.DocType == "PDC")
                //{
                //    Dt_Name = ES.VendorupdateThc(XML_Det, VC.DocType);
                //}
                TranXaction = Dt_Name.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error");
            }
            if (VC.DocType == "THC")
            {
                return RedirectToAction("VendorCodeChangeDone", new { THCNo = THCNo, TranXaction = TranXaction, DocType = VC.DocType });
            }
            else
            {
                return RedirectToAction("VendorCodeChangeDone", new { PDCNo = PDCNo, TranXaction = TranXaction, DocType = VC.DocType });
            }

        }

        public ActionResult VendorCodeChangeDone(string THCNo, string TranXaction, string DocType, string PDCNo)
        {
            ViewBag.TranXaction = TranXaction;
            if (DocType == "THC")
            {
                ViewBag.THCNo = THCNo;
            }
            else
            {
                ViewBag.PDCNo = PDCNo;
            }

            return View();
        }

        #endregion

        #region  Balance Location Change


        public ActionResult BalanceLocationChange()
        {
            BalanceLocationChangeViewModel BLCVM = new BalanceLocationChangeViewModel();
            BLCVM.BaseLoccode = BaseLocationCode;
            BLCVM.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            BLCVM.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (BLCVM.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                BLCVM.RegionList.Insert(0, objNew);
                BLCVM.LocationList = new List<webx_location>();
                BLCVM.LocationList.Insert(0, objNew);
            }
            if (BLCVM.LocLevel == 2)
            {
                BLCVM.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                BLCVM.LocationList.Insert(0, objNew);
            }
            if (BLCVM.LocLevel == 3)
            {
                BLCVM.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(BLCVM);
        }

        public ActionResult VendorPayment(BalanceLocationChangeViewModel BLCVM)
        {
            try
            {
                var Year = BaseFinYear.Split('-');
                BLCVM.BaseLoccode = BaseLocationCode;
                var Year1 = Year[0].ToString();
                if (BLCVM.DOC_NO == null)
                {
                    BLCVM.SelectionType = "0";
                }
                else
                {
                    BLCVM.SelectionType = "1";
                }
                List<webx_VENDOR_HDR> WVHL = ES.GetVendorPayment(BLCVM.DocType, GF.FormateDate(BLCVM.FromDate), GF.FormateDate(BLCVM.ToDate), BLCVM.VENDORCODE, BLCVM.DOC_NO, BaseFinYear.Split('-')[0].ToString(), BLCVM.SelectionType, BLCVM.RO, BLCVM.Loccode);
                BLCVM.WVHList = WVHL;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "ChequeDepositVoucher", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(BLCVM);
        }

        public JsonResult SearchLocationListJsonByRO(string Id)
        {
            List<webx_location> ListLocations = new List<webx_location>();

            ListLocations = MS.GetLocationDetails().Where(c => c.Report_Loc == Id).ToList().OrderBy(c => c.LocName).ToList();
            var SearchList = (from e in ListLocations
                              select new
                              {
                                  Value = e.LocCode,
                                  Text = e.LocCode + ":" + e.LocName,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult BalanceLocationChangeSubmit(BalanceLocationChangeViewModel BLCVM, List<webx_VENDOR_HDR> BALANCE)
        {
            string THCNo = "", TranXaction = "Not Done";
            DataTable Dt = new DataTable();
            try
            {
                if (BLCVM.DocType.ToUpper() == "THC")
                {
                    foreach (var item in BALANCE.Where(c => c.IsChecked == true).ToList())
                    {
                        Dt = ES.Balanceupdate(item.NBLocation, item.NALocation, item.THCNo, BaseUserName);
                        THCNo = THCNo == "" ? Dt.Rows[0]["DockNo"].ToString() : THCNo + "," + Dt.Rows[0]["DockNo"].ToString();
                        TranXaction = Dt.Rows[0][0].ToString();
                    }
                }
                else if (BLCVM.DocType.ToUpper() == "PDC")
                {
                    foreach (var item in BALANCE.Where(c => c.IsChecked == true).ToList())
                    {
                        Dt = ES.BalanceupdatePDC(item.NBLocation, item.NALocation, item.THCNo, BaseUserName);
                        THCNo = THCNo == "" ? Dt.Rows[0]["DockNo"].ToString() : THCNo + "," + Dt.Rows[0]["DockNo"].ToString();
                        TranXaction = Dt.Rows[0][0].ToString();
                    }
                }
                return RedirectToAction("BalanceLocationChangeDone", new { THCNo = THCNo, TranXaction = TranXaction, DocType = BLCVM.DocType });

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error");
            }
        }

        public ActionResult BalanceLocationChangeDone(string THCNo, string TranXaction, string DocType)
        {
            List<webx_VENDOR_HDR> WVHList = new List<webx_VENDOR_HDR>();
            if (THCNo.Contains(','))
            {
                string[] THCno = THCNo.Split(',');
                foreach (var item in THCno)
                {
                    webx_VENDOR_HDR objWVH = new webx_VENDOR_HDR();
                    objWVH.DOC_NO = item;
                    WVHList.Add(objWVH);
                }
            }
            else
            {
                webx_VENDOR_HDR objWVH = new webx_VENDOR_HDR();
                objWVH.DOC_NO = THCNo;
                WVHList.Add(objWVH);
            }
            ViewBag.TranXaction = TranXaction.ToUpper();
            ViewBag.THCNo = THCNo;
            ViewBag.DocType = DocType;
            return View(WVHList);
        }


        #endregion

        #region Material Held Up Reason View

        public ActionResult MaterialHeldUpReason()
        {
            MaterialHeldUpReason Material = new MaterialHeldUpReason();
            return View(Material);
        }

        public JsonResult CheckValidDKTSeries(string DocType)
        {
            string StockType = "";
            bool flag = false;

            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                DataTable Dt = ES.CheckValidDKTSeries(DocType, BaseLocationCode);
                List<VWNET_WebX_Docket_status> DKTList = DataRowToObject.CreateListFromTable<VWNET_WebX_Docket_status>(Dt);

                if (DKTList.Count > 0)
                {
                    flag = true;
                }
                StockType = Dt.Rows[0]["StockType"].ToString();

            }
            catch (Exception)
            {
                flag = false;
            }

            return new JsonResult()
            {
                Data = new
                {
                    DKT_Status = StockType,
                    flag = flag
                }
            };

        }

        public JsonResult DelyReason(string status)
        {
            List<Webx_Master_General> List = ES.GetDelayReason(status);

            var SearchList = (from e in List
                              select new
                              {
                                  Value = e.CodeId,
                                  Text = e.CodeDesc,
                              }).Distinct().ToList();
            return Json(SearchList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MaterialHeldUpReasonSubmit(MaterialHeldUpReason MHUR)
        {
            DataTable Dt = new DataTable();
            string Status = "";
            string GeneralType = "";
            if (MHUR.VWDS.doctype == "0")
            {
                //("Please Select Delay Reason or" + BaseLocationCode + "Number not be blank");
            }
            else
            {
                if (MHUR.VWDS.DKT_STATUS == "Booking Stock")
                {
                    GeneralType = "BSDely";
                }
                else if (MHUR.VWDS.DKT_STATUS == "Delivery Stock")
                {
                    GeneralType = "DSDely";
                }
                else if (MHUR.VWDS.DKT_STATUS == "Transshipment Stock")
                {
                    GeneralType = "TSDely";
                }

                string strQuery = "select CodeID,CodeType,CodeDesc from webx_master_general where statuscode='Y' and codetype='" + GeneralType + "' and CodeID='" + MHUR.VWDS.doctype + "'";
                Dt = GF.GetDataTableFromSP(strQuery);
                MHUR.VWDS.doctype = Dt.Rows[0][2].ToString();
                if (MHUR.VWDS.DKT_STATUS == "Delivery Stock")
                {
                    Status = "D";
                }
                else if (MHUR.VWDS.DKT_STATUS == "Booking Stock")
                {
                    Status = "B";
                }
                else if (MHUR.VWDS.DKT_STATUS == "Transshipment Stock")
                {
                    Status = "T";
                }

                MHUR.VWDS.DKT_STATUS = Status;
                //  BaseLocationCode = BaseUserId;
                string xmlDoc1 = "";
                string xmlDoc2 = "<root>";
                DataTable DT = new DataTable();

                try
                {
                    xmlDoc1 = xmlDoc1 + "<DocketReason>";
                    xmlDoc1 = xmlDoc1 + "<DockNo>" + MHUR.VWDS.dockno + "</DockNo>";
                    xmlDoc1 = xmlDoc1 + "<DelayReason>" + MHUR.VWDS.doctype + "</DelayReason>";
                    xmlDoc1 = xmlDoc1 + "<DockStatus>" + MHUR.VWDS.DKT_STATUS + "</DockStatus>";
                    xmlDoc1 = xmlDoc1 + "<EntryBy>" + BaseUserName + "</EntryBy>";
                    xmlDoc1 = xmlDoc1 + "</DocketReason>";

                    //xmlDoc2 = xmlDoc2 + "<DocketReason>";
                    //xmlDoc2 = xmlDoc2 + "<DockNo>" + MHUR.VWDS.dockno + "</DockNo>";
                    //xmlDoc2 = xmlDoc2 + "<DelayReason>" + MHUR.VWDS.doctype + "</DelayReason>";
                    //xmlDoc2 = xmlDoc2 + "</DocketReason>";
                    //xmlDoc2 = xmlDoc2 + "</DocketReason>";
                    xmlDoc2 = xmlDoc2 + "</root>";
                    DT = ES.InsertMaterialHeldUp(xmlDoc1, xmlDoc2);
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                    return View("Error");
                }
            }
            return RedirectToAction("MaterialHeldUpReasonDone", new { Dockno = MHUR.VWDS.dockno, Dealyreson = MHUR.VWDS.doctype });
        }

        public ActionResult MaterialHeldUpReasonDone(string Dockno, string Dealyreson)
        {
            ViewBag.Dockno = Dockno;
            ViewBag.Dealyreson = Dealyreson;
            return View();
        }

        #endregion

        #region Shortage and Damage Declaration

        public ActionResult DocketShortageCriteria()
        {
            ShortageViewModel BLC = new ShortageViewModel();
            BLC.WVHM = new ShortageCriteria();
            BLC.BaseLoccode = BaseLocationCode;
            BLC.WVHM.FilterType = "0";
            return View(BLC);
        }

        //public JsonResult SearchLocationListJsonByRO(string Id)
        //{
        //    List<webx_location> ListLocations = new List<webx_location>();

        //    ListLocations = MS.GetLocationDetails().Where(c => c.Report_Loc == Id).ToList().OrderBy(c => c.LocName).ToList();
        //    var SearchList = (from e in ListLocations
        //                      select new
        //                      {
        //                          Value = e.LocCode,
        //                          Text = e.LocCode + ":" + e.LocName,
        //                      }).Distinct().ToList();
        //    return Json(SearchList, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult DocketShortageData(ShortageViewModel vm)
        {
            if (vm.WVHM.DOC_NO == "")
            {
                vm.WVHM.FilterType = "0";
            }
            else
            {
                vm.WVHM.FilterType = "1";
            }
            List<ShortageModel> optrkList = ES.DOCKETLIST_FOR_DEPS_MODULES(BaseLocationCode, GF.FormateDate(vm.WVHM.DATEFROM), GF.FormateDate(vm.WVHM.DATETO), vm.WVHM.DOC_NO, vm.WVHM.FilterType, vm.WVHM.RO, vm.WVHM.location);
            vm.ListSM = optrkList;
            return View(vm);
        }

        public ActionResult DocketShortageSubmit(List<ShortageModel> ShortageList)
        {
            int ShortCount = 0;
            int DamageCount = 0;
            string xmlWTDS = "<root>";
            foreach (var item in ShortageList.Where(c => c.IsChecked == true).ToList())
            {
                xmlWTDS = xmlWTDS + "<WTDS>" +
                                   "<DOCKNO>" + item.DOCKNO + "</DOCKNO>" +
                                   "<OLDDOCKSF>" + item.DOCKSF + "</OLDDOCKSF>" +
                                   "<NEWDOCKSF>S</NEWDOCKSF>";
                if (Convert.ToInt32(item.SPackagesLB) > 0)
                {
                    xmlWTDS = xmlWTDS + "<SHORTPKGSNO>" + item.SPackagesLB + "</SHORTPKGSNO>" +
                                        "<SHORTACTUWT>" + item.SWeightLB + "</SHORTACTUWT>" +
                                        "<SHORTREASON>" + item.SReason + "</SHORTREASON>";
                    ShortCount = ShortCount + 1;
                }
                else
                {
                    xmlWTDS = xmlWTDS + "<SHORTPKGSNO>0</SHORTPKGSNO>" +
                                        "<SHORTACTUWT>0</SHORTACTUWT>" +
                                        "<SHORTREASON></SHORTREASON>";
                }
                if (Convert.ToInt32(item.DPackagesLB) > 0)
                {
                    xmlWTDS = xmlWTDS + "<DAMAGEPKGSNO>" + item.DPackagesLB + "</DAMAGEPKGSNO>" +
                                        "<DAMAGEACTUWT>" + item.DWeightLB + "</DAMAGEACTUWT>" +
                                        "<DAMAGEREASON>" + item.DReason + "</DAMAGEREASON>";
                    DamageCount = DamageCount + 1;
                }
                else
                {
                    xmlWTDS = xmlWTDS + "<DAMAGEPKGSNO>0</DAMAGEPKGSNO>" +
                                        "<DAMAGEACTUWT>0</DAMAGEACTUWT>" +
                                        "<DAMAGEREASON></DAMAGEREASON>";

                }
                xmlWTDS = xmlWTDS + "<CURR_LOC>" + BaseLocationCode + "</CURR_LOC>";
                xmlWTDS = xmlWTDS + "</WTDS>";
            }
            xmlWTDS = xmlWTDS + "</root>";
            DataTable DT = ES.DAMAGE_SHORTAGE_UPDATE(xmlWTDS);
            // object Result = DT.Rows[0][0].ToString();
            return RedirectToAction("DocketShortageDone", new { DamageCnt = DamageCount, ShortageCnt = ShortCount });
        }

        public ActionResult DocketShortageDone(string DamageCnt, string ShortageCnt)
        {
            ViewBag.DamageCnt = DamageCnt;
            ViewBag.ShortageCnt = ShortageCnt;
            return View();
        }

        #endregion

        #region CNote MisRoute

        public ActionResult DocketMisRouteCriteria()
        {
            return View();
        }

        public ActionResult DocketMisRouteList(ShortageViewModel vm)
        {
            vm.ListMR = ES.Get_DocketMisRouteList(vm.SM.DOCKNO);
            return View(vm);
        }

        public ActionResult DocketMisRouteSubmit(List<DocketMisRouteModel> MissDkt)
        {
            String hidPO = "";
            foreach (var item in MissDkt.Where(c => c.IsChecked == true).ToList())
            {
                DataTable DT = ES.Insert_DocketMisRoute(item.Dockno, BaseLocationCode, item.ArrivedPkgs, item.ArrivedWt);
                if (hidPO == "")
                {
                    hidPO = item.Dockno;
                }
                else
                {
                    hidPO = hidPO + "," + item.Dockno;
                }
            }
            return RedirectToAction("DocketMisRouteDone", new { dockno = hidPO });
        }

        public ActionResult DocketMisRouteDone(string dockno)
        {
            ViewBag.dockno = dockno;
            return View();
        }

        #endregion

        #region CNote Reassign Location

        public ActionResult CNoteReassignLocationQuery()
        {
            CNoteReassignFilter CRF = new CNoteReassignFilter();
            CRF.BaseLoccode = BaseLocationCode;
            CRF.LocLevel = MS.GetLocationDetails().Where(c => c.LocCode.ToUpper() == BaseLocationCode.ToUpper()).FirstOrDefault().Loc_Level;
            CRF.RegionList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            webx_location objNew = new webx_location();
            if (CRF.LocLevel == 1)
            {
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CRF.RegionList.Insert(0, objNew);
                CRF.LocationList = new List<webx_location>();
                CRF.LocationList.Insert(0, objNew);
            }
            if (CRF.LocLevel == 2)
            {
                CRF.LocationList = MS.GetRegionList(BaseLocationCode, "LO").ToList();
                objNew.LocCode = "All";
                objNew.LocName = "All";
                CRF.LocationList.Insert(0, objNew);
            }
            if (CRF.LocLevel == 3)
            {
                CRF.LocationList = MS.GetRegionList(BaseLocationCode, "RO").ToList();
            }
            return View(CRF);
        }

        public ActionResult CNoteReassignLocation(CNoteReassignFilter CRF)
        {
            CNoteReassignViewModel CRVM = new CNoteReassignViewModel();
            //List<VWNET_WebX_Docket_status> CnoteList = new List<VWNET_WebX_Docket_status>();
            CRVM.ListVWWDS = ES.GetCnoteList(CRF).ToList();
            return View(CRVM);
        }

        [HttpPost]
        public ActionResult CNoteReassignLocationSubmit(string Id, string LocCode, string CSGEPinCode, string DestStateName, string Destination_Area, string ToCity)
        {
            bool Status = false;
            try
            {
                DataTable Dt = ES.CNoteReassignLocation(Id, LocCode, CSGEPinCode, DestStateName, Destination_Area, ToCity, BaseUserName);
                Status = true;
                return new JsonResult()
                {
                    Data = new
                    {
                        Status = Status,
                    }
                };
            }
            catch (Exception)
            {
                //Error_Logs(ControllerName, "GetDuplicateCountry", "Json", "Duplicate Country Find", ex.Message);
                return Json(Status);
            }
        }

        #endregion

        #region Advance Edit

        public ActionResult TripsheetQuery()
        {
            TSAdvanceEditCriteria TS = new TSAdvanceEditCriteria();

            return View(TS);
        }

        [HttpPost]
        public string GetTripList(TSAdvanceEditCriteria TSM)
        {
            return JsonConvert.SerializeObject(ES.GetTSAdvanceEditList(TSM.FromDate.ToString("dd MMM yyyy"), TSM.ToDate.ToString("dd MMM yyyy"), TSM.TripSheetNo, TSM.Flag, BaseCompanyCode));
        }

        public ActionResult TripsheetEdit(string Id)
        {
            DataSet ds = ES.GetTSAdvanceEdit(Id, BaseLocationCode, BaseUserName);
            TSAdvanceEditViewModel VM = new TSAdvanceEditViewModel();
            VM.TSAEDMList = DataRowToObject.CreateListFromTable<TSAdvanceEditDates>(ds.Tables[0]);
            VM.TSAEDM = VM.TSAEDMList.FirstOrDefault();
            VM.WTSAList = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_ADVEXP>(ds.Tables[1]);
            VM.PC = new PaymentControl();
            VM.TSAEDM.TripSheetNo = Id;
            return View(VM);
        }

        [HttpPost]
        public ActionResult TripsheetEdit(TSAdvanceEditViewModel VM, PaymentControl ObjPayment, List<WEBX_TRIPSHEET_ADVEXP> Tripsheet1, FormCollection form)
        {
            try
            {
                var Tst = form["selected_voucher"];
                VM.WTSASelect = Tripsheet1.Where(x => x.VoucherRefNo == Tst).FirstOrDefault();

                XmlDocument xmlDocVM = new XmlDocument();
                XmlDocument xmlDocObjPayment = new XmlDocument();
                XmlDocument xmlDocTripsheet = new XmlDocument();

                XmlSerializer xmlSerializer = new XmlSerializer(VM.TSAEDM.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, VM.TSAEDM);
                    xmlStream.Position = 0;
                    xmlDocVM.Load(xmlStream);
                }

                xmlSerializer = new XmlSerializer(ObjPayment.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, ObjPayment);
                    xmlStream.Position = 0;
                    xmlDocObjPayment.Load(xmlStream);
                }

                xmlSerializer = new XmlSerializer(VM.WTSASelect.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, VM.WTSASelect);
                    xmlStream.Position = 0;
                    xmlDocTripsheet.Load(xmlStream);
                }
                DataTable dt = ES.UpdateTripsheetAdvanceEdit(VM.WTSASelect.TripSheetNo, xmlDocVM.InnerXml, xmlDocObjPayment.InnerXml, xmlDocTripsheet.InnerXml, BaseUserName, BaseFinYear, BaseLocationCode, BaseCompanyCode, BaseYearVal);
                return RedirectToAction("DocumentisUpdatedDone", new { TripSheetNo = VM.WTSASelect.TripSheetNo });

            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "TripsheetEdit", "Advance", "Edit", ex.Message);
                return View("Error");
            }

        }
        public ActionResult DocumentisUpdatedDone(string TripSheetNo)
        {
            ViewBag.TripSheetNo = TripSheetNo;
            return View();
        }


        #endregion

        #region Financial Edit

        public ActionResult TripsheetFinancialQuery()
        {
            TSFinancialEditCriteria TS = new TSFinancialEditCriteria();
            return View(TS);
        }

        [HttpPost]
        public ActionResult GetFinancialTripList(TSFinancialEditCriteria TSM)
        {
            DataTable Dt = ES.GetTSFinancialEditList(TSM.TripSheetNo, TSM.FromDate.ToString("dd MMM yyyy"), TSM.ToDate.ToString("dd MMM yyyy"), TSM.UpdClose, BaseLocationCode, TSM.Flag, BaseCompanyCode);

            TSM.WFVIList = DataRowToObject.CreateListFromTable<WEBX_FLEET_VEHICLE_ISSUE>(Dt);

            return PartialView("_TripsheetFinancialQueryList", TSM);

            //return JsonConvert.SerializeObject();
        }

        public ActionResult EnRouteExpenses(int id)
        {
            WEBX_FLEET_ENROUTE_EXP obj = new WEBX_FLEET_ENROUTE_EXP();
            obj.SRNO = id;
            return PartialView("_EnRouteExpenses", obj);
        }
        public ActionResult TripsheetFinancialEdit(string TripShit, string Mode)
        {
            TSFinancialEditViewModel FVM = new TSFinancialEditViewModel();
            WEBX_FLEET_VEHICLE_ISSUE WFVI = new WEBX_FLEET_VEHICLE_ISSUE();
            List<WEBX_FLEET_ENROUTE_EXP> LIST_TripExp = new List<WEBX_FLEET_ENROUTE_EXP>();

            DataSet ds = ES.GetTSFinanceEdit(TripShit, Mode, BaseLocationCode, BaseUserName);
            FVM.TSCloseList = DataRowToObject.CreateListFromTable<vw_TripSheetClose>(ds.Tables[0]);
            FVM.WTSAList = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_ADVEXP>(ds.Tables[1]);
            FVM.LIST_WTRSOP = DataRowToObject.CreateListFromTable<WEBX_TRIPSHEET_OILEXPDET>(ds.Tables[2]);
            FVM.TSClose = FVM.TSCloseList.FirstOrDefault();

            LIST_TripExp = FS.Get_TripExp_List(TripShit);
            WFVI = FS.DetailsFromId(TripShit);
            FVM.WFVI = WFVI;
            FVM.Obj_WFT = FS.Get_Webx_Fleet_Trip().First();

            if (LIST_TripExp.Count() == 0)
            {
                WEBX_FLEET_ENROUTE_EXP OBjWTEM = new WEBX_FLEET_ENROUTE_EXP();
                OBjWTEM.SRNO = 1;
                LIST_TripExp.Add(OBjWTEM);
            }
            else
            {
                FVM.EnrtExpList = LIST_TripExp;
            }
            FVM.Operational_Close = "0";
            ViewBag.Mode = Mode;
            return View(FVM);
        }

        [HttpPost]
        public ActionResult TripsheetFinancialEdit(TSFinancialEditViewModel FVM, List<WEBX_TRIPSHEET_ADVEXP> TripsheetFinEdit, List<WEBX_TRIPSHEET_OILEXPDET> ListOILEXPDET, List<WEBX_FLEET_ENROUTE_EXP> fleetList)
        {
            DataTable DT = new DataTable();
            //if (FVM.TSClose.Oper_Close_Dt == null || GF.FormateDate(Convert.ToDateTime(FVM.TSClose.Oper_Close_Dt)) == GF.FormateDate(System.DateTime.MinValue))
            //{
            FVM.WFVI.Oper_Close_Dt = System.DateTime.Now;
            //}
            //else
            //{
            //    FVM.WFVI.Oper_Close_Dt = Convert.ToDateTime(FVM.TSClose.Oper_Close_Dt);
            //}

            try
            {
                FVM.Obj_WFT = FS.Get_Webx_Fleet_Trip().First();

                if (FVM.Operational_Close == "1")
                {

                    #region WEBX_TRIPSHEET_OILEXPDET

                    XmlDocument xmlDoc1 = new XmlDocument();
                    List<WEBX_TRIPSHEET_OILEXPDET> List_OILEXPDET = new List<WEBX_TRIPSHEET_OILEXPDET>();

                    if (ListOILEXPDET != null)
                    {
                        foreach (var item in ListOILEXPDET)
                        {
                            WEBX_TRIPSHEET_OILEXPDET objWTOP = new WEBX_TRIPSHEET_OILEXPDET();
                            objWTOP.Place = item.Place;
                            objWTOP.PetrolPName = item.PetrolPName;
                            objWTOP.Brand = item.Brand;
                            objWTOP.FuelType = item.FuelType;
                            objWTOP.Last_Km_Read = item.Last_Km_Read;
                            objWTOP.KM_Reading = item.KM_Reading;
                            objWTOP.BillNo = item.BillNo;
                            objWTOP.BillDt = item.BillDt;
                            objWTOP.Diesel_Ltr = item.Diesel_Ltr;
                            objWTOP.Diesel_Rate = item.Diesel_Rate;
                            objWTOP.Amount = item.Amount;
                            objWTOP.EXE_AMT = item.EXE_AMT;
                            objWTOP.Card_Cash = item.Card_Cash;
                            objWTOP.Remark = item.Remark;

                            List_OILEXPDET.Add(objWTOP);
                        }
                    }
                    XmlSerializer xmlSerializer1 = new XmlSerializer(List_OILEXPDET.GetType());
                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, List_OILEXPDET);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }

                    #endregion

                    #region Webx_Fleet_Triprule

                    XmlDocument xmlDoc2 = new XmlDocument();
                    XmlSerializer xmlSerializer2 = new XmlSerializer(FVM.Obj_WFT.GetType());
                    using (MemoryStream xmlStream2 = new MemoryStream())
                    {
                        xmlSerializer2.Serialize(xmlStream2, FVM.Obj_WFT);
                        xmlStream2.Position = 0;
                        xmlDoc2.Load(xmlStream2);
                    }

                    #endregion

                    #region WEBX_TRIPSHEET_ADVEXP

                    XmlDocument xmlDoc3 = new XmlDocument();
                    List<WEBX_TRIPSHEET_ADVEXP> List_WTADVE = new List<WEBX_TRIPSHEET_ADVEXP>();

                    if (TripsheetFinEdit != null)
                    {
                        foreach (var item in TripsheetFinEdit)
                        {
                            WEBX_TRIPSHEET_ADVEXP ObjWTA = new WEBX_TRIPSHEET_ADVEXP();
                            ObjWTA.AdvLoc = item.AdvLoc;
                            ObjWTA.Advance_SlipNo = item.Advance_SlipNo;
                            ObjWTA.AdvDate = item.AdvDate;
                            ObjWTA.AdvAmt = item.AdvAmt;
                            ObjWTA.BranchCode = item.BranchCode;
                            ObjWTA.Signature = item.Signature;

                            List_WTADVE.Add(ObjWTA);
                        }
                        //List_WTADVE = LISTAdvanceExp;
                    }

                    XmlSerializer xmlSerializer3 = new XmlSerializer(List_WTADVE.GetType());
                    using (MemoryStream xmlStream3 = new MemoryStream())
                    {
                        xmlSerializer3.Serialize(xmlStream3, List_WTADVE);
                        xmlStream3.Position = 0;
                        xmlDoc3.Load(xmlStream3);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc4 = new XmlDocument();
                    List<WEBX_FLEET_ENROUTE_EXP> List_WFEXP = new List<WEBX_FLEET_ENROUTE_EXP>();
                    if (fleetList != null)
                    {
                        foreach (var item in fleetList)
                        {
                            WEBX_FLEET_ENROUTE_EXP objWFEE = new WEBX_FLEET_ENROUTE_EXP();

                            objWFEE.Polarity = item.Polarity;
                            objWFEE.TripsheetNo = FVM.TSClose.VSlipNo;
                            objWFEE.id = item.id;
                            objWFEE.Amount_Spent = item.Amount_Spent;
                            objWFEE.BillNo = item.BillNo;
                            objWFEE.Dt = item.Dt;
                            objWFEE.Exe_Appr_Amt = item.Exe_Appr_Amt;
                            objWFEE.Remarks = item.Remarks;

                            List_WFEXP.Add(objWFEE);
                        }
                    }
                    XmlSerializer xmlSerializer4 = new XmlSerializer(List_WFEXP.GetType());
                    using (MemoryStream xmlStream4 = new MemoryStream())
                    {
                        xmlSerializer4.Serialize(xmlStream4, List_WFEXP);
                        xmlStream4.Position = 0;
                        xmlDoc4.Load(xmlStream4);
                    }

                    #endregion

                    #region WEBX_FLEET_VEHICLE_ISSUE

                    XmlDocument xmlDoc5 = new XmlDocument();
                    XmlSerializer xmlSerializer5 = new XmlSerializer(FVM.TSClose.GetType());
                    using (MemoryStream xmlStream5 = new MemoryStream())
                    {
                        xmlSerializer5.Serialize(xmlStream5, FVM.TSClose);
                        xmlStream5.Position = 0;
                        xmlDoc5.Load(xmlStream5);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc6 = new XmlDocument();
                    XmlSerializer xmlSerializer6 = new XmlSerializer(FVM.WFEE.GetType());
                    using (MemoryStream xmlStream6 = new MemoryStream())
                    {
                        xmlSerializer6.Serialize(xmlStream6, FVM.WFEE);
                        xmlStream6.Position = 0;
                        xmlDoc5.Load(xmlStream6);
                    }

                    #endregion

                    #region XML



                    string XML = "<root>";
                    XML = XML + "<Closing_Km>" + FVM.TSClose.f_closure_closekm + "</Closing_Km>";
                    XML = XML + "<VehicleNo>" + FVM.TSClose.vehno + "</VehicleNo>";
                    XML = XML + "<TripSheet_EndLoc>" + FVM.TSClose.TripSheet_EndLoc + "</TripSheet_EndLoc>";
                    XML = XML + "<TripSheet_StartLoc>" + FVM.TSClose.TripSheet_StartLoc + "</TripSheet_StartLoc>";
                    XML = XML + "<PreparedBy>" + FVM.WFVI.PreparedBy + "</PreparedBy>";
                    XML = XML + "<CheckedBy>" + FVM.WFVI.CheckedBy + "</CheckedBy>";
                    XML = XML + "<ApprovedBy>" + FVM.WFVI.ApprovedBy + "</ApprovedBy>";
                    XML = XML + "<Oper_Close_Dt>" + FVM.WFVI.Oper_Close_Dt.ToString("dd MMM yyyy") + "</Oper_Close_Dt>";
                    XML = XML + "<AuditedBy>" + FVM.WFVI.AuditedBy + "</AuditedBy>";
                    XML = XML + "<WFEE_Total>" + FVM.WFEE.Total + "</WFEE_Total>";
                    XML = XML + "<Category>" + FVM.TSClose.Category + "</Category>";
                    XML = XML + "</root>";

                    #endregion

                    string SQRY = "exec Usp_Operationally_TripClose '" + xmlDoc1.InnerXml + "','" + xmlDoc2.InnerXml + "','" + xmlDoc3.InnerXml +
                        "','" + xmlDoc4.InnerXml + "' ,'" + XML + "','" + FVM.TSClose.VSlipNo + "'";

                    DT = FS.TripOperationally_Close(xmlDoc1.InnerXml, xmlDoc2.InnerXml, xmlDoc3.InnerXml, xmlDoc4.InnerXml, XML, FVM.TSClose.VSlipNo, BaseLocationCode, BaseCompanyCode);
                }
                else if (FVM.Operational_Close == "0")
                {

                    #region usp_CloseVehicleIssue

                    string CloseVehicle = "<root>";

                    CloseVehicle = CloseVehicle + "<VSlipNo>" + FVM.TSClose.VSlipNo + "</VSlipNo>";
                    CloseVehicle = CloseVehicle + "<Driver1>" + FVM.TSClose.Driver1 + "</Driver1>";
                    CloseVehicle = CloseVehicle + "<VehicleNo>" + FVM.TSClose.vehno + "</VehicleNo>";
                    CloseVehicle = CloseVehicle + "<f_closure_closekm>" + FVM.TSClose.f_closure_closekm + "</f_closure_closekm>";
                    CloseVehicle = CloseVehicle + "<f_closure_fill>" + FVM.TSClose.f_closure_fill + "</f_closure_fill>";
                    CloseVehicle = CloseVehicle + "<AKMPL>" + FVM.TSClose.Actual_KMPL + "</AKMPL>";
                    CloseVehicle = CloseVehicle + "<APPKMPL>" + FVM.TSClose.Approved_KMPL + "</APPKMPL>";
                    CloseVehicle = CloseVehicle + "<Closedt>" + FVM.TSClose.TS_Close_Dt + "</Closedt>";
                    CloseVehicle = CloseVehicle + "<Oper_Closedt>" + FVM.WFVI.Oper_Close_Dt + "</Oper_Closedt>";
                    CloseVehicle = CloseVehicle + "<Contractamt>0</Contractamt>";
                    CloseVehicle = CloseVehicle + "<c_Othercharg>0</c_Othercharg>";
                    CloseVehicle = CloseVehicle + "<c_TDSrate>0</c_TDSrate>";
                    CloseVehicle = CloseVehicle + "<c_TDSamt>0</c_TDSamt>";
                    CloseVehicle = CloseVehicle + "<c_TDStype>0</c_TDStype>";
                    CloseVehicle = CloseVehicle + "<c_Advamt_paid>0</c_Advamt_paid>";
                    CloseVehicle = CloseVehicle + "<c_issue_tot>0</c_issue_tot>";
                    CloseVehicle = CloseVehicle + "<c_dedamt>0</c_dedamt>";
                    CloseVehicle = CloseVehicle + "<c_Balamt_paid>0</c_Balamt_paid>";
                    CloseVehicle = CloseVehicle + "<c_closure_tot>0</c_closure_tot>";
                    CloseVehicle = CloseVehicle + "<c_net_amt>0</c_net_amt>";
                    CloseVehicle = CloseVehicle + "<e_issue_advamt>0</e_issue_advamt>";
                    CloseVehicle = CloseVehicle + "<e_issue_spare>0</e_issue_spare>";
                    CloseVehicle = CloseVehicle + "<e_issue_fuel>0</e_issue_fuel>";
                    CloseVehicle = CloseVehicle + "<e_issue_comm>0</e_issue_comm>";
                    CloseVehicle = CloseVehicle + "<e_closure_balamt>0</e_closure_balamt>";
                    CloseVehicle = CloseVehicle + "<e_closure_incentive>0</e_closure_incentive>";
                    CloseVehicle = CloseVehicle + "<e_closure_deposit>0</e_closure_deposit>";
                    CloseVehicle = CloseVehicle + "<e_closure_dedamt>0</e_closure_dedamt>";
                    CloseVehicle = CloseVehicle + "<e_issue_totamt>0</e_issue_totamt>";
                    CloseVehicle = CloseVehicle + "<e_closure_totamt>0</e_closure_totamt>";
                    CloseVehicle = CloseVehicle + "<e_net_amt>0</e_net_amt>";
                    CloseVehicle = CloseVehicle + "</root>";

                    #endregion

                    #region WEBX_TRIPSHEET_OILEXPDET

                    XmlDocument xmlDoc1 = new XmlDocument();
                    List<WEBX_TRIPSHEET_OILEXPDET> List_OILEXPDET = new List<WEBX_TRIPSHEET_OILEXPDET>();

                    if (ListOILEXPDET != null)
                    {
                        foreach (var item in ListOILEXPDET)
                        {
                            WEBX_TRIPSHEET_OILEXPDET objWTOP = new WEBX_TRIPSHEET_OILEXPDET();
                            objWTOP.Place = item.Place;
                            objWTOP.PetrolPName = item.PetrolPName;
                            objWTOP.Brand = item.Brand;
                            objWTOP.FuelType = item.FuelType;
                            objWTOP.Last_Km_Read = item.Last_Km_Read;
                            objWTOP.KM_Reading = item.KM_Reading;
                            objWTOP.BillNo = item.BillNo;
                            objWTOP.BillDt = item.BillDt;
                            objWTOP.Diesel_Ltr = item.Diesel_Ltr;
                            objWTOP.Diesel_Rate = item.Diesel_Rate;
                            objWTOP.Amount = item.Amount;
                            objWTOP.EXE_AMT = item.EXE_AMT;
                            objWTOP.Card_Cash = item.Card_Cash;
                            objWTOP.Remark = item.Remark;

                            List_OILEXPDET.Add(objWTOP);
                        }
                    }
                    XmlSerializer xmlSerializer1 = new XmlSerializer(List_OILEXPDET.GetType());
                    using (MemoryStream xmlStream1 = new MemoryStream())
                    {
                        xmlSerializer1.Serialize(xmlStream1, List_OILEXPDET);
                        xmlStream1.Position = 0;
                        xmlDoc1.Load(xmlStream1);
                    }

                    #endregion

                    #region WEBX_FLEET_ENROUTE_EXP

                    XmlDocument xmlDoc4 = new XmlDocument();
                    List<WEBX_FLEET_ENROUTE_EXP> List_WFEXP = new List<WEBX_FLEET_ENROUTE_EXP>();
                    if (fleetList != null)
                    {
                        foreach (var item in fleetList)
                        {
                            WEBX_FLEET_ENROUTE_EXP objWFEE = new WEBX_FLEET_ENROUTE_EXP();

                            //if (item.id == "18")
                            //{
                            //    SendMail(VW.WFVI.VSlipNo);
                            //}

                            objWFEE.Polarity = item.Polarity;
                            objWFEE.TripsheetNo = FVM.TSClose.VSlipNo;
                            objWFEE.id = item.id;
                            objWFEE.Amount_Spent = item.Amount_Spent;
                            objWFEE.BillNo = item.BillNo;
                            objWFEE.Dt = item.Dt;
                            objWFEE.Exe_Appr_Amt = item.Exe_Appr_Amt;
                            objWFEE.Remarks = item.Remarks;

                            List_WFEXP.Add(objWFEE);
                        }
                    }
                    XmlSerializer xmlSerializer4 = new XmlSerializer(List_WFEXP.GetType());
                    using (MemoryStream xmlStream4 = new MemoryStream())
                    {
                        xmlSerializer4.Serialize(xmlStream4, List_WFEXP);
                        xmlStream4.Position = 0;
                        xmlDoc4.Load(xmlStream4);
                    }

                    #endregion

                    #region XML

                    string XML = "<root>";
                    XML = XML + "<Closing_Km>" + FVM.TSClose.f_closure_closekm + "</Closing_Km>";
                    XML = XML + "<VehicleNo>" + FVM.TSClose.vehno + "</VehicleNo>";
                    XML = XML + "<TripSheet_StartLoc>" + FVM.TSClose.TripSheet_StartLoc + "</TripSheet_StartLoc>";
                    XML = XML + "<TripSheet_EndLoc>" + FVM.TSClose.TripSheet_EndLoc + "</TripSheet_EndLoc>";
                    XML = XML + "<PreparedBy>" + FVM.WFVI.PreparedBy + "</PreparedBy>";
                    XML = XML + "<CheckedBy>" + FVM.WFVI.CheckedBy + "</CheckedBy>";
                    XML = XML + "<ApprovedBy>" + FVM.WFVI.ApprovedBy + "</ApprovedBy>";
                    XML = XML + "<Oper_Close_Dt>" + FVM.WFVI.Oper_Close_Dt + "</Oper_Close_Dt>";
                    XML = XML + "<AuditedBy>" + FVM.WFVI.AuditedBy + "</AuditedBy>";
                    XML = XML + "<Category>" + FVM.TSClose.Category + "</Category>";
                    XML = XML + "<WFEE_Total>" + FVM.WFEE.Total + "</WFEE_Total>";
                    XML = XML + "</root>";

                    #endregion

                    string SQRY = "exec Usp_Operationally_Financial_Close_TripClose '" + CloseVehicle + "','" + xmlDoc1.InnerXml + "','" + xmlDoc4.InnerXml +
                     "','" + XML + "','" + FVM.TSClose.VSlipNo + "'";

                    DT = ES.Operationally_Financial_Edit_Close(CloseVehicle, xmlDoc1.InnerXml, xmlDoc4.InnerXml, XML, FVM.TSClose.VSlipNo);
                }
            }
            catch (Exception ex)
            {
                @ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("TripsheetFinancialEditDone", new { No = DT.Rows[0][2].ToString() });
        }

        public ActionResult TripsheetFinancialEditDone(string No)
        {
            ViewBag.No = No;
            return View();
        }

        #endregion

        #region Tripsheet Advance Voucher Cancellation
        public ActionResult TripAdvanceVoucherCancellation()
        {
            AdvanceTripVoucherCancellation Voucher = new AdvanceTripVoucherCancellation();
            Voucher.ListWAAT = new List<webx_acctrans>();

            return View(Voucher);
        }
        public ActionResult ADDTripAdvanceVoucherCancellation(int id)
        {
            webx_acctrans WMFSM = new webx_acctrans();
            WMFSM.Srno = id;
            return PartialView("_TripAdvanceVoucherCancellationAddRow", WMFSM);
        }
        public ActionResult TripAdvanceVoucherCancellationSubmit(AdvanceTripVoucherCancellation Voucher, List<webx_acctrans> VoucherList)
        {
            AdnavceTripVoucherDoneList Rs = new AdnavceTripVoucherDoneList();
            List<AdnavceTripVoucherDoneList> Results = new List<AdnavceTripVoucherDoneList>();

            //string VoucherNo = "", TranXaction = "";

            try
            {
                foreach (var item in VoucherList)
                {
                    if (Voucher.WAAT.Voucherno != "")
                    {
                        DataTable DT = new DataTable();
                        DT = ES.TripAdvanceVoucherCancellation(1, BaseFinYear, item.canceldt, Voucher.WAAT.comment, item.Voucherno, BaseYearVal, BaseUserName, BaseCompanyCode);
                        Rs.VoucherNo = DT.Rows[0]["VoucherNo"].ToString();
                        Rs.Status = DT.Rows[0]["Status"].ToString();
                        Results.Add(Rs);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }

            return RedirectToAction("AdvanceVoucherCancellationDone", "Exceptions", Results);
        }
        public ActionResult AdvanceVoucherCancellationDone(List<AdnavceTripVoucherDoneList> Results)
        {
            ViewBag.VoucherNo = "DONE";
            ViewBag.TranXaction = "DONE";
            return View(Results);
        }
        public string GetTripAdvanceVocherDate_NameJson(string SerchGroup)
        {
            List<webx_location> ListGroups = new List<webx_location> { };
            try
            {
                string YearSuffix = BaseYearVal;
                string comapnycode = BaseCompanyCode;
                DataTable Dt_Location = ES.GetValidateAdvanceVoucherDate(SerchGroup, BaseYearVal, BaseCompanyCode);
                //string Result = "0";
                //if (Dt_Location.Rows.Count > 0)
                //{
                //    Result = Dt_Location.Rows[0][0].ToString();
                //}
                //return Json(Dt_Location, JsonRequestBehavior.AllowGet);
                return JsonConvert.SerializeObject(Dt_Location);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetVocherDate_NameJson", "Json", "Listing", ex.Message);
                return JsonConvert.SerializeObject(null);
                //return Json(ListGroups);
            }
        }
        #endregion

        #region ADVICE Edit/Cancellation

        public ActionResult AdviceCancellation()
        {
            AdviceCancelViewModel ACVM = new AdviceCancelViewModel();
            ACVM.ListAdvice = new List<WEBX_Advice_Hdr>();
            ACVM.Advice = new WEBX_Advice_Hdr();
            return View(ACVM);
        }

        public ActionResult ADDAdviceCancellation(int id)
        {
            WEBX_Advice_Hdr WAH = new WEBX_Advice_Hdr();
            WAH.SrNo = id;
            return PartialView("_AdviceCancellationAddRow", WAH);
        }

        public string GetAdviceDate_RaisedOnJson(string SerchGroup)
        {
            return JsonConvert.SerializeObject(ES.GETAdviceDateDetails(SerchGroup));
        }



        public ActionResult AdviceCancellationSubmit(AdviceCancelViewModel AVM, List<WEBX_Advice_Hdr> AdviceList)
        {
            string AdviceNoList = "";
            string AdviceNo = "";
            DataTable Dt = new DataTable();
            try
            {

                foreach (var Item in AdviceList)
                {
                    if (!string.IsNullOrWhiteSpace(Item.Adviceno))
                    {
                        if (Item.IsCheck == true)
                        {
                            Dt = ES.SetEditandCancelAdvice("CANCEL", Item.Adviceno, Item.RaisedOn, Item.cancel_date.ToString(), Item.Cancel_Reason, BaseUserName, BaseFinYear);
                            if (Dt != null)
                            {
                                AdviceNo = Dt.Rows[0][2].ToString();
                            }
                        }
                        else
                        {
                            Dt = ES.SetEditandCancelAdvice("EDIT", Item.Adviceno, Item.UpdatedRaisedCode, "", "", BaseUserName, BaseFinYear);
                            AdviceNo = Dt.Rows[0][2].ToString();
                        }
                    }

                    if (AdviceNoList == "")
                    {
                        AdviceNoList = AdviceNo;
                    }
                    else
                    {
                        AdviceNoList = AdviceNoList + "," + AdviceNo;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ');
                return View("Error");
            }
            return RedirectToAction("AdviceCancellationSubmitDone", new { AdviceNoList = AdviceNoList });
        }

        public ActionResult AdviceCancellationSubmitDone(string AdviceNoList)
        {
            string[] strarray = AdviceNoList.Split(',');
            List<string> strList = new List<string>();
            ViewBag.AdviceNO = AdviceNoList;

            foreach (string obj in strarray)
                strList.Add(obj);

            return View(strList);
            ///return View();
        }


        #endregion

        #region Bill Cancellation

        public ActionResult Billcancel()
        {
            if (CurrFinYear == BaseFinYear || BaseUserName == "ADMIN")
            {
                BillEntryCancellation chq = new BillEntryCancellation();
                string HdnFinalizedRule = "N";
                string HdnAcctReversal = "N";

                if (HdnFinalizedRule == "N")
                {
                    DataTable DT = new DataTable();
                    DT = ES.Check_Finalized_Status();
                    string count = DT.Rows[0]["count"].ToString();
                    if (count != "")
                    {
                        DT = ES.Check_Finalized_Status_1();
                        string RULE_Y_N = DT.Rows[0]["RULE_Y_N"].ToString();
                        RULE_Y_N = chq.HdnFinalizedRule;
                    }
                }
                else if (HdnAcctReversal == "N")
                {
                    DataTable DT = new DataTable();
                    DT = ES.Bill_Reversal_Accounting();
                    string count = DT.Rows[0]["count"].ToString();
                    if (count != "")
                    {
                        DT = ES.Bill_Reversal_Accounting_1();
                        string RULE_Y_N = DT.Rows[0]["RULE_Y_N"].ToString();
                        RULE_Y_N = chq.HdnAcctReversal;
                    }
                }

                return View(chq);
            }
            else
            {
                return RedirectToAction("URLRedirect", "Home");
            }
        }

        public JsonResult GetBillcancelCustmaer(string searchTerm)
        {
            List<webx_CUSTHDR> CMP = ES.GETBILLCANCELCUST(searchTerm).Take(5).ToList();

            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateBillcancellist(BillEntryCancellation Bill)
        {
            try
            {
                Bill.HdnFinalizedRule = BillCancellationRule("Check_Finalized_Status");
                Bill.HdnAcctReversal = BillCancellationRule("Bill_Reversal_Accounting");
                string BaseYear = BaseFinYear.Split('-')[0].ToString();
                List<webx_BILLMST> billList = ES.BillCancelList(GF.FormateDate(Bill.BillNo.DATEFROM), GF.FormateDate(Bill.BillNo.DATETO), Bill.BillNo.BILLNO, Bill.BillNo.manualbillno, Bill.BillNo.Custcd, Bill.BillNo.BILLTYPE, BaseYear);
                Bill.BillNOList = billList;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(Bill);
        }

        public string BillCancellationRule(string Ruletype)
        {
            double count = 0;
            string sql = "", RuleValue = "N";
            DataTable DT = new DataTable();

            if (Ruletype.CompareTo("Check_Finalized_Status") == 0)
            {
                sql = "SELECT COUNT(*) FROM WEBX_MODULES_RULES WHERE Module_Name='Bill Cancellation' and RULEID='Check_Finalized_Status'";
                DT = GF.GetDataTableFromSP(sql);
                count = Convert.ToDouble(DT.Rows[0][0].ToString());
                if (count > 0)
                {
                    sql = "SELECT RULE_Y_N FROM WEBX_MODULES_RULES WHERE Module_Name='Bill Cancellation' and RULEID='Check_Finalized_Status'";
                    DT = GF.GetDataTableFromSP(sql);
                    RuleValue = DT.Rows[0][0].ToString();
                }
            }
            if (Ruletype.CompareTo("Bill_Reversal_Accounting") == 0)
            {
                sql = "select COUNT(*) from webx_modules_rules where Module_Name='Bill Cancellation' and RULEID='Bill_Reversal_Accounting'";
                DT = GF.GetDataTableFromSP(sql);
                count = Convert.ToDouble(DT.Rows[0][0].ToString());
                if (count > 0)
                {
                    sql = "select RULE_Y_N from webx_modules_rules where Module_Name='Bill Cancellation' and RULEID='Bill_Reversal_Accounting'";
                    DT = GF.GetDataTableFromSP(sql);
                    RuleValue = DT.Rows[0][0].ToString();
                }
            }
            return RuleValue;
        }

        public ActionResult BillcancelSubmit(BillEntryCancellation Bill, List<webx_BILLMST> BillNO)
        {
            string Nos_String = "";
            DataTable Dt_Name = new DataTable();
            try
            {
                var CancelRession = Bill.BillNo.CancelRession;
                if (BillNO != null)
                {

                    foreach (var item in BillNO.Where(c => c.IsChecked == true).ToList())
                    {
                        var Year = BaseFinYear.Split('-');
                        var Year1 = Year[0].ToString();
                        var CancelDate = Bill.BillNo.BGNDT.ToString("dd MMM yyyy");
                        if (Nos_String == "")
                        {
                            Nos_String = item.BILLNO.ToString();
                        }
                        else
                        {
                            Nos_String = Nos_String + "," + item.BILLNO;
                        }
                        Dt_Name = ES.BillCancelListsubmit(item.BILLNO, item.PAYBAS, CancelDate, CancelRession, BaseUserName, Bill.HdnFinalizedRule, Bill.HdnAcctReversal, Year1);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return RedirectToAction("BillcancelDone", new { BILLNO = Nos_String });
        }

        public ActionResult BillcancelDone(string BILLNO)
        {
            ViewBag.BILLNO = BILLNO;
            return View();
        }

        #endregion

        #region CNote Non Delivery Reason Update
        public ActionResult CNoteNonDeliveryCriteria()
        {
            CNoteNonDeliveryCriteria criteria = new CNoteNonDeliveryCriteria();
            return View(criteria);
        }

        public ActionResult GetCNoteNonDeliveryList(CNoteNonDeliveryCriteria criteria)
        {
            CNoteNonDeliveryreasonUpdateViewModel Mdl = new CNoteNonDeliveryreasonUpdateViewModel();
            Mdl.Criteria = criteria;
            Mdl.DocketList = ES.GetCnoteNonDeliveryList(criteria);
            return View(Mdl);
        }

        public ActionResult CNoteNonDeliverySubmit(CNoteNonDeliveryreasonUpdateViewModel RUVM, List<WebX_Master_Docket> NonDeliveryEntry)
        {
            string Xml_Docket = "<root>";
            foreach (var itm in NonDeliveryEntry.Where(c => c.IsCheck == true))
            {
                Xml_Docket = Xml_Docket + "<docket><dockno>" + itm.DOCKNO + "</dockno>";
                Xml_Docket = Xml_Docket + "<reason>" + "2" + "</reason>";
                Xml_Docket = Xml_Docket + "<subreason>" + itm.ReasonCode + "</subreason>";
                Xml_Docket = Xml_Docket + "<txtval>" + "" + "</txtval>";
                Xml_Docket = Xml_Docket + "<ndelbrcd>" + BaseLocationCode + "</ndelbrcd>";
                Xml_Docket = Xml_Docket + "<Next_DlyDt>1 jan 1900</Next_DlyDt>";
                Xml_Docket = Xml_Docket + "</docket>";
            }

            Xml_Docket = Xml_Docket + "</root>";

            DataSet ds = ES.SubmitNonDeliveryReasonUpdate(Xml_Docket);


            return RedirectToAction("CNoteNonDeliveryReasonUpdateDone", new { ValidDocket = "", InvalidDocket = "" });
        }

        public ActionResult CNoteNonDeliveryReasonUpdateDone(string ValidDocket, string InvalidDocket)
        {
            return View();
        }

        #endregion

        #region Service Tax Removal

        public ActionResult ServiceTaxRemove()
        {
            ServiceTaxRemove CNRF = new ServiceTaxRemove();
            return View(CNRF);
        }

        public JsonResult Check_Valid_DocketNo(string DocketNo)
        {
            string Status = ES.GetValid_Dockno_NO(DocketNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult ServiceTaxRemovesubmit(ServiceTaxRemove STAXR)
        {
            DataTable DT = new DataTable();
            try
            {
                string Status = ES.GetValid_Dockno_NO(STAXR.CnoteNo);
                if (Status == "Done")
                {
                    DT = ES.GetRemove_Service_Tax(STAXR.CnoteNo, BaseUserName.ToUpper());
                }
            }

            catch (Exception Ex)
            {
                ViewBag.StrError = "Error : " + Ex.Message;
                return View("Error");
            }


            return RedirectToAction("ServiceTaxRemoveDone", new { flag = DT.Rows[0][1], Cnote = STAXR.CnoteNo, msg = DT.Rows[0][0] });
        }

        public ActionResult ServiceTaxRemoveDone(string flag, string Cnote, string msg)
        {
            ViewBag.flag = flag;
            ViewBag.Cnote = Cnote;
            ViewBag.msg = msg;
            return View();
        }

        #endregion

        #region Octroi Bill Vendor Code Change

        public ActionResult OctroiBill_VendorChange()
        {
            OctroiBill_VendorChange Bill_Vendor = new OctroiBill_VendorChange();
            List<WEBX_VENDORBILL_HDR> List_WVBill = new List<WEBX_VENDORBILL_HDR>();
            WEBX_VENDORBILL_HDR ObjWV = new WEBX_VENDORBILL_HDR();
            ObjWV.ID = 1;
            List_WVBill.Add(ObjWV);
            Bill_Vendor.ListWVBill = List_WVBill;

            return View(Bill_Vendor);
        }

        public ActionResult ADD_OctroiBill_Change(int id)
        {
            WEBX_VENDORBILL_HDR ObjWV = new WEBX_VENDORBILL_HDR();
            ObjWV.ID = id;
            return PartialView("_Octroi_Vendor_Change", ObjWV);
        }

        public JsonResult Valid_Bill_No(string BillNo)
        {
            string VendorCode = "", VendorName = "", Message = "", Status = "";

            try
            {
                DataTable DT = ES.Get_Valid_Bill(BillNo, BaseLocationCode, BaseUserName, BaseFinYear);
                Message = DT.Rows[0]["Message"].ToString();
                Status = DT.Rows[0]["Status"].ToString();

                if (Message.ToUpper() == "DONE" && Status == "1")
                {
                    VendorCode = DT.Rows[0]["VendorCode"].ToString();
                    VendorName = DT.Rows[0]["VendorName"].ToString();
                }
                else
                {
                    //Message = Message;
                    Message = "NOT DONE";
                    Status = "0";
                    VendorCode = "";
                    VendorName = "";
                }
            }
            catch (Exception)
            {
                Message = "Please Enter Valid Bill No.";
                Status = "0";
                VendorCode = "";
                VendorName = "";
            }
            return new JsonResult()
            {
                Data = new
                {
                    Message = Message,
                    Status = Status,
                    VendorCode = VendorCode,
                    VendorName = VendorName
                }
            };
        }

        public ActionResult OctroiBill_Submit_VendorChange(OctroiBill_VendorChange OCTBVC, List<WEBX_VENDORBILL_HDR> BillList)
        {
            DataTable DT = new DataTable();
            string Nos = "", ErrorNos = "";
            try
            {
                if (BillList != null)
                {
                    foreach (var item in BillList)
                    {
                        DT = ES.OctroiBill_Submit_VendorChange(item.BILLNO, BaseLocationCode, BaseUserName, BaseFinYear, item.VENDORCODE);

                        string Message = "";
                        Message = DT.Rows[0]["Message"].ToString();
                        if (Message == "DONE")
                        {
                            if (Nos == "")
                                Nos = item.BILLNO;
                            else
                                Nos = Nos + "," + item.BILLNO;
                        }
                        else
                        {
                            ErrorNos = item.BILLNO;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Issue in Nos :- " + ErrorNos + "." + ex.Message;
                return View("Error");
            }

            return RedirectToAction("OctroiBill_VendorChange_Done", new { No = Nos });
        }

        public ActionResult OctroiBill_VendorChange_Done(string No)
        {
            ViewBag.No = No;
            return View();
        }

        #endregion

        #region GRN Cancellation

        public ActionResult GRNCancellationCriteria()
        {
            POCancellationCriteria PCC = new POCancellationCriteria();
            return View(PCC);
        }

        public JsonResult GetGRNDetails(string GRNNo)
        {
            DataTable DT = new DataTable();
            DT = ES.ChecGRNNo(GRNNo);
            string cnt = DT.Rows[0]["cnt"].ToString();
            string MSG = DT.Rows[0]["msg"].ToString();

            return new JsonResult()
            {
                Data = new
                {
                    Flg = cnt,
                    MSG = MSG
                }
            };
        }

        public ActionResult GRNCancellationsubmit(POCancellationCriteria POCCM)
        {
            DataTable Dt = ES.GRNCancellation(POCCM.GRNNo, GF.FormateDate(POCCM.POCANCELDATE), POCCM.Reason, BaseUserName);
            string MSG = Dt.Rows[0][0].ToString();
            string Count = Dt.Rows[0][1].ToString();
            string GRNNo = Dt.Rows[0][2].ToString();
            return RedirectToAction("GRNCancellationDone", new { MSG = MSG, Count = Count, GRNNo = GRNNo });
        }

        public ActionResult GRNCancellationDone(string MSG, string Count, string GRNNo)
        {
            ViewBag.MSG = MSG;
            ViewBag.Count = Count;
            ViewBag.GRNNo = GRNNo;
            return View();
        }

        #endregion

        #region Docket Booking Exception

        public ActionResult DocketBookingException()
        {
            DoketBookingExceptionViewModel DBEV = new DoketBookingExceptionViewModel();
            return View(DBEV);
        }

        public JsonResult Check_DocketNo(string DocketNo)
        {
            string Status = ES.Get_Dockno_NO(DocketNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult DocketBookingExceptionList(DoketBookingExceptionViewModel DBVE)
        {
            try
            {
                List<WebX_Master_Docket> Dock_List = ES.USP_DOCKET_GETList(DBVE.CnoteNo);
                WebX_Master_Docket OBjDKT = Dock_List.First();
                OBjDKT.PAYBAS = "TBB";
                DBVE.Dkt = OBjDKT;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(DBVE);
        }

        public ActionResult DoketBookingExceptionSubmit(DoketBookingExceptionViewModel DBEV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                var SDD_Date = DBEV.Dkt.SDD_Date.ToString("dd MMM yyyy");
                Dt_Name = ES.DOCKET_ListSubmit(DBEV.Dkt.DOCKNO, SDD_Date, BaseUserName);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return RedirectToAction("SDDDateDone", new { DOCKNO = DBEV.Dkt.DOCKNO });
        }

        public ActionResult SDDDateDone(string DOCKNO)
        {
            ViewBag.DOCKNO = DOCKNO;
            return View();
        }

        #endregion

        #region Docket DashBoard Calander

        public ActionResult DocketDashBoardCalander()
        {
            DoketBookingExceptionViewModel DBEV = new DoketBookingExceptionViewModel();
            List<CalenderEvent> DKT = ES.GetCallingEvent();
            DBEV.DktCal = DKT;
            return View(DBEV);
        }

        #endregion

        #region Docket Booking EDD Date Change

        public ActionResult DocketBookingEDD_Date()
        {
            DoketBookingExceptionViewModel DBEV = new DoketBookingExceptionViewModel();
            return View(DBEV);
        }

        public JsonResult Check_DocketNo_EDD_Date_Chnage(string DocketNo)
        {
            string Status = ES.Get_Dockno_NO_EDD_Date_Change(DocketNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult DocketBookingEDD_DateChnage(DoketBookingExceptionViewModel DBVE)
        {
            try
            {
                List<WebX_Master_Docket> Dock_List = ES.USP_DOCKET_GETList(DBVE.CnoteNo);
                WebX_Master_Docket OBjDKT = Dock_List.First();
                OBjDKT.PAYBAS = "TBB";
                DBVE.Dkt = OBjDKT;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(DBVE);
        }

        public ActionResult DoketBookingException_EDD_Date_Chnage_Submit(DoketBookingExceptionViewModel DBEV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                var SDD_Date = DBEV.Dkt.EDD_Date.ToString("dd MMM yyyy");
                Dt_Name = ES.DOCKET_List_EDD_Chnage_Submit(DBEV.Dkt.DOCKNO, SDD_Date, BaseUserName);
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return RedirectToAction("EDDDateDone", new { DOCKNO = DBEV.Dkt.DOCKNO });
        }

        public ActionResult EDDDateDone(string DOCKNO)
        {
            ViewBag.DOCKNO = DOCKNO;
            return View();
        }

        #endregion

        #region DACC COD/DOD Customer Generate Mail

        public ActionResult DACC_COD_DOD_Apply_Email()
        {
            CustomergeneratemailviewModel CGMV = new CustomergeneratemailviewModel();
            return View(CGMV);
        }

        public JsonResult Check_DocketNo_Mail(string DocketNo)
        {
            string Type = "";
            string Status = ES.Get_Dockno_NO_Mail(DocketNo, Type);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult CustomergeneratemailList(CustomergeneratemailviewModel CGMV)
        {
            try
            {
                List<WebX_Master_Docket> Dock_List = ES.Get_DOCKET_GETList_Mail(CGMV.CnoteNo);
                WebX_Master_Docket OBjDKT = Dock_List.First();
                CGMV.Dkt = OBjDKT;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(CGMV);
        }

        public ActionResult CustomergeneratemailSubmit(CustomergeneratemailviewModel CGMV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                if (CGMV.Dkt.SDD_Date.ToString("dd MMM yyyy").ToUpper() != "01 JAN 0001" && CGMV.Dkt.SDD_Date.ToString("dd MMM yyyy").ToUpper() == "01 JAN 1990" && CGMV.Dkt.TicketNo == "" && CGMV.Dkt.TicketNo == null)
                {
                    CGMV.Dkt.CDELDT = CGMV.Dkt.CDELDT.AddDays(+3);
                }
                Dt_Name = ES.Customergenerate_ListSubmit(CGMV.Dkt.DOCKNO, CGMV.Dkt.COD, CGMV.Dkt.DACC, BaseUserName, CGMV.Dkt.CDELDT.ToString("dd MMM yyy"), ViewBag.FinYearStart, BaseLocationCode);

                SendMail(CGMV);
                int Status = Convert.ToInt16(Dt_Name.Rows[0]["Status"].ToString());
                string Message = Dt_Name.Rows[0]["Message"].ToString();
                string TicketNo = Dt_Name.Rows[0]["TicketNo"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("CustomergeneratemailDone", new { TicketNo = TicketNo, DOCKNO = CGMV.Dkt.DOCKNO });
                }
                else
                {
                    @ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public void SendMail(CustomergeneratemailviewModel CGMV)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            string strBody = "";
            SmtpClient mySmtp = new SmtpClient();
            strBody = " <html> ";
            strBody = strBody + "<head> ";
            strBody = strBody + "<style> </style>";
            strBody = strBody + "</head> ";
            strBody = strBody + "<body> ";
            strBody = strBody + "<h3>Dear Sir/Mam,</h3>";
            strBody = strBody + "<b>Greetings from the Fleet,</b>";
            strBody = strBody + "</br>";
            strBody = strBody + "<b>This is to inform you that your Docket# " + CGMV.Dkt.DOCKNO + " is Booked Against the,</b>";
            strBody = strBody + "</br>";

            string COD_DOD = "<p><b>COD/DOD on Dated.:-" + CGMV.Dkt.DOCKDT.ToString("dd MMM yyyy") + ", Please inform to your Customer be Ready with COD/DOD Copy & Amount.</b></p>";
            string DACC = "<p><b>DACC on Dated.:-" + CGMV.Dkt.DOCKDT.ToString("dd MMM yyyy") + ", Please inform to your Customer be Ready with DACC Copy & Amount.</b></p>";

            //if (CGMV.Dkt.COD_DOD == "Y" && CGMV.Dkt.DACC_YN == "Y"/*Convert.ToDouble(CGMV.Dkt.DACC) != 0.0 && Convert.ToDouble(CGMV.Dkt.COD) != 0.0*/)
            //{
            //    strBody = strBody + COD_DOD + DACC;
            //}
            //else if (/*Convert.ToDouble(CGMV.Dkt.COD) != 0.0 && Convert.ToDouble(CGMV.Dkt.DACC) == 0.0*/CGMV.Dkt.COD_DOD == "Y")
            //{
            //    strBody = strBody + COD_DOD;
            //}
            //else if (/*Convert.ToDouble(CGMV.Dkt.DACC) != 0.0 && Convert.ToDouble(CGMV.Dkt.COD) == 0.0*/CGMV.Dkt.DACC_YN == "Y")
            //{
            //    strBody = strBody + DACC;
            //}
            if (CGMV.COD == "COD" && CGMV.DACC == "DACC")
            {
                strBody = strBody + COD_DOD + DACC;
            }
            else if (CGMV.COD == "COD")
            {
                strBody = strBody + COD_DOD;
            }
            else if (CGMV.DACC == "DACC")
            {
                strBody = strBody + DACC;
            }

            strBody = strBody + "</br> ";
            strBody = strBody + "</br> ";
            strBody = strBody + " <h2 style=color:#42bcf4>Best Regards</h2>";
            strBody = strBody + " <h2 style=color:#4183f4><b>Team Fleet<b></h2>";
            strBody = strBody + " <b>Customer Care: Phone:08010112299</b>";
            strBody = strBody + "</body> ";
            strBody = strBody + "</html>";

            DataTable DT = ES.SQL_DACC_COD_DOD_Mail(strBody, CGMV.Dkt.MailId, CGMV.Dkt.MailId1, CGMV.Dkt.MailId2);
        }

        public ActionResult CustomergeneratemailDone(string DOCKNO, string TicketNo)
        {
            ViewBag.DOCKNO = DOCKNO;
            ViewBag.TicketNo = TicketNo;
            return View();
        }

        #endregion

        #region DACC COD/DOD Cancellation Module

        public ActionResult DACC_COD_DOD_Cancellation()
        {
            CustomergeneratemailviewModel CGMV = new CustomergeneratemailviewModel();
            return View(CGMV);
        }

        public JsonResult Check_DocketNo_DACC(string DocketNo)
        {
            string Type = "DACC";
            string Status = ES.Get_Dockno_NO_Mail(DocketNo, Type);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult DACC_COD_DOD_CancellationList(CustomergeneratemailviewModel CGMV)
        {
            try
            {
                List<WebX_Master_Docket> Dock_List = ES.Get_DOCKET_GETList_Mail(CGMV.CnoteNo);
                WebX_Master_Docket OBjDKT = Dock_List.First();
                CGMV.Dkt = OBjDKT;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(CGMV);
        }

        public ActionResult DACC_COD_DOD_CancellationSubmit(CustomergeneratemailviewModel CGMV)
        {
            DataTable Dt_Name = new DataTable();
            WebX_Master_Docket DktNo = new WebX_Master_Docket();
            string status = "";
            try
            {

                if (CGMV.Dkt.DACC_Cancel == "Y")
                {
                    status = "DACC Cancelled";
                }
                else if (CGMV.Dkt.COD_DOC_Cancel == "Y")
                {
                    status = "COD_DOD Cancelled";
                }
                else if (CGMV.Dkt.DACC_Cancel == "Y" || CGMV.Dkt.COD_DOC_Cancel == "Y")
                {
                    status = "DACC Cancelled";
                }

                Dt_Name = ES.DACC_COD_DOD_ListSubmit(CGMV.Dkt.DOCKNO, BaseUserName, status);

                int Status = Convert.ToInt16(Dt_Name.Rows[0]["Status"].ToString());
                string Message = Dt_Name.Rows[0]["Message"].ToString();
                string ReferenceNo = Dt_Name.Rows[0]["ReferenceNo"].ToString();
                CGMV.Dkt.ReferenceNo = ReferenceNo;
                SendMailDACC_COD_DOD_Cancellation(CGMV);

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("DACC_COD_DOD_CancellationDone", new { ReferenceNo = ReferenceNo, DOCKNO = CGMV.Dkt.DOCKNO });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public void SendMailDACC_COD_DOD_Cancellation(CustomergeneratemailviewModel CGMV)
        {
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;
            string strBody = "";
            SmtpClient mySmtp = new SmtpClient();
            strBody = " <html> ";
            strBody = strBody + "<head> ";
            strBody = strBody + "<style> </style>";
            strBody = strBody + "</head> ";
            strBody = strBody + "<body> ";
            strBody = strBody + "<h3>Dear Sir/Mam,</h3>";
            strBody = strBody + "<b>Greetings from the RCPL,</b>";
            strBody = strBody + "</br>";
            strBody = strBody + "<P><b>Please Delivered the Consignment without Collecting  the DACC /COD DOD . your Reference No. is " + CGMV.Dkt.ReferenceNo + " According the Same.</b></p>";
            strBody = strBody + "</br> ";
            strBody = strBody + "</br> ";
            strBody = strBody + " <h2 style=color:#42bcf4>Best Regards</h2>";
            strBody = strBody + " <h2 style=color:#4183f4><b>Team RCPL<b></h2>";
            strBody = strBody + " <b>Customer Care: Phone:08010112299</b>";
            strBody = strBody + "</body> ";
            strBody = strBody + "</html>";

            DataTable DT = ES.SQL_DACC_COD_DOD_Mail(strBody, CGMV.Dkt.MailId, CGMV.Dkt.MailId1, CGMV.Dkt.MailId2);
        }

        public ActionResult DACC_COD_DOD_CancellationDone(string DOCKNO, string ReferenceNo)
        {
            ViewBag.DOCKNO = DOCKNO;
            ViewBag.ReferenceNo = ReferenceNo;
            return View();
        }

        #endregion

        #region Docket Status Update

        public ActionResult DocketStatusUpadate()
        {
            CustomergeneratemailviewModel CGMV = new CustomergeneratemailviewModel();
            return View(CGMV);
        }

        public JsonResult Check_DocketNo_Status(string DocketNo)
        {
            string Status = ES.Get_Dockno_NO_Status(DocketNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult DocketStatusUpadateSubmit(CustomergeneratemailviewModel CGMV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                Dt_Name = ES.Get_Dockno_NO_StatusSubmit(CGMV.CnoteNo, CGMV.DOCKET_DELY_REASON, CGMV.Reason, BaseUserName);
                int Status = Convert.ToInt16(Dt_Name.Rows[0]["Status"].ToString());
                string Message = Dt_Name.Rows[0]["Message"].ToString();
                string Dockno = Dt_Name.Rows[0]["Dockno"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("DocketStatusUpadatelDone", new { DOCKNO = Dockno });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult DocketStatusUpadatelDone(string DOCKNO)
        {
            ViewBag.DOCKNO = DOCKNO;
            return View();
        }

        #endregion

        #region TripSheet Cancellation

        /*vw_VEHICLEISSUEVIEWPRINT_Query,VW_FLEET_TRIPSHEET_TRACKER*/

        public ActionResult TripSheetCancellation()
        {
            TripOpen TOV = new TripOpen();
            return View(TOV);
        }

        public JsonResult Check_TripSheetNo_Status(string VSlipNo)
        {
            string Status = ES.Get_TripSheet_NO_Status(VSlipNo);

            return new JsonResult()
            {
                Data = new
                {
                    Status = Status
                }
            };
        }

        public ActionResult TripSheetCancellationSubmit(TripOpen TONV)
        {
            DataTable Dt_Name = new DataTable();
            try
            {
                Dt_Name = ES.TripSheetCancellation_StatusSubmit(TONV.WFVI.VSlipNo, TONV.WFVI.Cancel_Remarks, BaseUserName, BaseYearVal);
                int Status = Convert.ToInt16(Dt_Name.Rows[0]["Status"].ToString());
                string Message = Dt_Name.Rows[0]["Message"].ToString();
                string VSlipNo = Dt_Name.Rows[0]["VSlipNo"].ToString();

                if (Status == 1 && Message == "Done")
                {
                    return RedirectToAction("TripSheetCancellationDone", new { VSlipNo = VSlipNo });
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        public ActionResult TripSheetCancellationDone(string VSlipNo)
        {
            ViewBag.VSlipNo = VSlipNo;
            return View();
        }

        #endregion

        #region RE Assign BA Code
        public ActionResult ReassignBAcode()
        {
            WebX_Master_Docket WMD = new WebX_Master_Docket();
            WMD.id = 1;
            return View(WMD);
        }

        [HttpPost]
        public ActionResult ReassignBacodeAddNewRow(int Id)
        {
            WebX_Master_Docket WMD = new WebX_Master_Docket();
            WMD.id = Id;
            return PartialView("_ReassignBAcode", WMD);
        }
        public ActionResult ReassignBAcodeSubmit(List<WebX_Master_Docket> ReassignBAcode)
        {
            try
            {
                if (ReassignBAcode != null)
                {
                    foreach (var item in ReassignBAcode)
                    {
                        string Squery1 = "UPDATE webx_master_docket SET Bacode = '" + item.NewBacode.ToString() + "' WHERE dockno = '" + item.DOCKNO.ToString() + "'";
                        DataTable dt1 = GF.GetDataTableFromSP(Squery1);
                        string Squery2 = "EXEC [usp_ReAssign_BACode] '" + item.DOCKNO.ToString() + "','" + item.PAYBAS.ToString() + "','" + item.TRN_MOD.ToString() + "','" + item.NewBacode.ToString() + "','" + item.SubTotal + "'";
                        DataTable dt2 = GF.GetDataTableFromSP(Squery2);
                    }
                }
                return RedirectToAction("ReassignBACodeDone");
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error");
            }
        }
        public ActionResult ReassignBACodeDone()
        {
            return View();
        }
        #endregion

        #region Cnote Fields Edit
        public ActionResult CnoteFieldsEditCriteria()
        {
            return View();
        }
        public ActionResult CnoteFieldsEditCriteriaAdd(Docket Dk)
        {
            return RedirectToAction("CnoteFieldsEdit", new { DocketNo = Dk.DockNo });
        }
        public ActionResult CnoteFieldsEdit(string DocketNo)
        {
            DocketViewModel DVM = new DocketViewModel();
            WebX_Master_Docket WMD = new WebX_Master_Docket();
            DataSet DSDocket = GF.getdatasetfromQuery("EXEC USP_GetData_CnoteFieldsEdit '" + DocketNo + "','" + BaseUserName + "','" + BaseLocationCode + "','" + BaseCompanyCode + "'");
            WMD = DataRowToObject.CreateListFromTable<WebX_Master_Docket>(DSDocket.Tables[0]).FirstOrDefault();
            DVM.WMD = WMD;
            return View("CnoteFieldsEdit", DVM);
        }

        public ActionResult UpdateCnoteFields(DocketViewModel DVM)
        {
            DataTable dt = new DataTable();
            try
            {
                if (DVM.WMD.IsLocalDocket == true)
                {
                    DVM.WMD.flaglocal = "Y";
                }
                else
                {
                    DVM.WMD.flaglocal = "N";
                }
                XmlDocument xmlDocument = new XmlDocument();
                XmlSerializer xmlSerializer = new XmlSerializer(DVM.WMD.GetType());
                using (MemoryStream xmlStream = new MemoryStream())
                {
                    xmlSerializer.Serialize(xmlStream, DVM.WMD);
                    xmlStream.Position = 0;
                    xmlDocument.Load(xmlStream);
                }
                dt = ES.UpdateCnoteFields(xmlDocument.InnerXml, BaseUserName);

                string Message = dt.Rows[0]["Message"].ToString();

                if (Message == "Done")
                {
                    return RedirectToAction("Donepage");
                }
                else
                {
                    ViewBag.StrError = Message;
                    return View("Error");
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
        }

        #endregion

        #region DonePage

        public ActionResult DonePage()
        {
            return View();
        }

        #endregion

        #region Crossing Challan Cancellation

        public ActionResult CrossingChallanCanellation()
        {
            Webx_Crossing_Docket_Master WCDM_obj = new Webx_Crossing_Docket_Master();
            return View(WCDM_obj);
        }

        public JsonResult GetCrossingChallanNumberDetail(string ChallanNo)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                DataTable Dt = new DataTable();
                if (BaseLocationCode == "HQTR")
                {
                    Dt = ES.Get_Crossin_Challan_Data_HQTR(ChallanNo);
                }
                else
                {
                    Dt = ES.Get_Crossin_Challan_Data_NotHQTR(ChallanNo, BaseLocationCode);
                }

                List<Webx_Crossing_Docket_Master> WCDMList = DataRowToObject.CreateListFromTable<Webx_Crossing_Docket_Master>(Dt);
                string ChallanDate = "";
                string ChallanLocCode = "";
                string VendorName = "";
                string TotalToPay = "";
                string TotalCrossing = "";
                string TotalDoorDelyChrg = "";
                string TotalBulky = "";
                string NetPayable = "";

                bool flag = false;
                if (WCDMList.Count > 0)
                {
                    flag = true;
                    ChallanLocCode = Dt.Rows[0]["ChallanLocCode"].ToString();
                    ChallanDate = Dt.Rows[0]["ChallanDate_str"].ToString();
                    VendorName = Dt.Rows[0]["VendorName"].ToString();
                    TotalToPay = Dt.Rows[0]["TotalToPay"].ToString();
                    TotalCrossing = Dt.Rows[0]["TotalCrossing"].ToString();
                    TotalDoorDelyChrg = Dt.Rows[0]["TotalDoorDelyChrg"].ToString();
                    TotalBulky = Dt.Rows[0]["TotalBulky"].ToString();
                    NetPayable = Dt.Rows[0]["NetPayable"].ToString();

                }

                return new JsonResult()
                {
                    Data = new
                    {
                        ChallanLocCode = ChallanLocCode,
                        ChallanDate = ChallanDate,
                        VendorName = VendorName,
                        TotalToPay = TotalToPay,
                        TotalCrossing = TotalCrossing,
                        TotalDoorDelyChrg = TotalDoorDelyChrg,
                        TotalBulky = TotalBulky,
                        NetPayable = NetPayable,
                        flag = flag
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetCrossingChallanNumberDetail", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        [HttpPost]
        public ActionResult CrossingChallanCanellation(Webx_Crossing_Docket_Master WCDM)
        {
            DataTable DT = ES.Usp_Cancel_CrossingChallan(WCDM.CrossingChallanNo, BaseUserName, BaseFinYear);
            string Message = DT.Rows[0]["Message"].ToString();
            if (Message == "Done")
            {
                return RedirectToAction("CrossingChallan_CancellationDone", new { ChallanNo = WCDM.CrossingChallanNo });
            }
            else
            {
                ViewBag.StrError = Message;
                return View("Error");
            }
        }

        public ActionResult CrossingChallan_CancellationDone(string ChallanNo)
        {
            ViewBag.ChallanNo = ChallanNo;
            return View();
        }

        #endregion

        #region IDT - Cancellation
        public ActionResult IDTCancellation()
        {
            WebX_Master_Docket Master = new WebX_Master_Docket();
            return View(Master);
        }
        public JsonResult GetIDTDetails(string IDTNo)
        {
            DataTable DT = new DataTable();
            DT = ES.ChecIDTNo(IDTNo);
            string cnt = DT.Rows[0]["cnt"].ToString();
            string MSG = DT.Rows[0]["msg"].ToString();

            return new JsonResult()
            {
                Data = new
                {
                    Flg = cnt,
                    MSG = MSG
                }
            };
        }
        public ActionResult IDTCancellationsubmit(WebX_Master_Docket Docket)
        {
            DateTime startDate1 = Convert.ToDateTime(FinYearStartDate);
            DateTime EnddateDate1 = Convert.ToDateTime(FinYearEndDate);
            string startDate = startDate1.ToString("dd MMM yyyy");
            string EndDate = EnddateDate1.ToString("dd MMM yyyy");
            string PayBas = "";
            string hdndockyear = "";

            DataTable Result = ES.CheckStatus(Docket.DOCKNO, startDate, EndDate);
            if (Result.Rows.Count <= 0)
            {
                ViewBag.StrError = "Either IDT doesn't Exists or doesn't lie in current Financial Year.";
                return View("Error");
            }
            if (Result.Rows.Count > 0)
            {
                PayBas = Convert.ToString(Result.Rows[0]["paybas"].ToString());
                hdndockyear = Convert.ToString(Result.Rows[0]["dockdate"]).Substring(6, 4);
            }
            string hdnflagbill = ES.GetDefaultValue("FLAG_" + PayBas + "BILL");
            Result = ES.GetTable(Docket.DOCKNO);
            if (Result.Rows.Count > 0)
            {
                if (Convert.ToString(Result.Rows[0]["cancelled"]).CompareTo("Y") == 0)
                {
                    ViewBag.StrError = "IDT No " + Docket.DOCKNO + " is already Cancelled";
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["thc"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = ViewBag.THCCalledAs + Result.Rows[0]["thc"].ToString() + " is Generated for IDT No " + Docket.DOCKNO;
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["mf"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "Manifest " + Result.Rows[0]["mf"].ToString() + " is Generated for IDT No " + Docket.DOCKNO;
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["finalized"]).CompareTo("Y") == 0 || (Convert.ToString(Result.Rows[0]["billed"]).CompareTo("Y") == 0 && PayBas == "P02"))
                {
                    ViewBag.StrError = ViewBag.DKTCalledAs + " is Finalized OR bill is generated";
                    return View("Error");
                }
                if (Convert.ToString(Result.Rows[0]["ls"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "Loading Sheet " + Result.Rows[0]["ls"].ToString() + " is Generated for IDT " + Docket.DOCKNO;
                    return View("Error");
                }

                if (Convert.ToString(Result.Rows[0]["drs"]).CompareTo("") != 0)
                {
                    ViewBag.StrError = "DRS " + Result.Rows[0]["drs"].ToString() + " is Generated for IDT " + Docket.DOCKNO;
                    return View("Error");
                }

                if (Convert.ToString(Result.Rows[0]["delivered"]).CompareTo("Y") == 0)
                {
                    ViewBag.StrError = "IDT No " + Docket.DOCKNO + " is Delivered";
                    return View("Error");
                }
                if (hdnflagbill.CompareTo("Y") != 0)
                {
                    if (Convert.ToString(Result.Rows[0]["billed"]).CompareTo("Y") == 0)
                    {
                        ViewBag.StrError = "Bill is Generated for IDT " + Docket.DOCKNO;
                        return View("Error");
                    }
                }
            }
            if (Result.Rows.Count <= 0)
            {
                ViewBag.StrError = Docket.DOCKNO + " doesn't Exists or Not in current Financial Year";
                return View("Error");
            }
            string billno = ES.GetDocketBillNo(Docket.DOCKNO, PayBas);
            string billstatus = ES.GetBillStatus(billno);

            if (billstatus.CompareTo("BILL COLLECTED") == 0)
            {
                ViewBag.StrError = "Bill is Closed. Not Possible to Cancel IDT";
                return View("Error");
            }

            double amt = 0;
            try
            {
                DataTable DT1 = ES.ISBillAvailable(billno);
                if (DT1.Rows.Count > 0)
                {
                    string QueryString = "SELECT billamt-pendamt FROM webx_billmst WHERE billno='" + billno + "'";
                    amt = Convert.ToDouble(GF.executeScalerQuery(QueryString));
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message.Replace('\n', '_');
                return View("Error");
            }
            if (amt != 0)
            {
                ViewBag.StrError = "BILL IS COLLECTED PARTIALLY OF FULLY. CAN NOT CANCEL IDT";
                return View("Error");
            }
            try
            {
                ES.Cancellation(BaseUserName, Docket.Reason, Docket.DOCKNO);
                if (PayBas.CompareTo("P01") == 0 || PayBas.CompareTo("P03") == 0)
                {
                    if (billstatus.CompareTo("BILL GENERATED") == 0 || billstatus.CompareTo("BILL SUBMITTED") == 0)
                    {
                        ES.Updatewithbill(BaseUserName, Docket.Reason, billno);
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Session Expired OR DataBase Updation Exception Occured " + ex.Message.Replace('\n', '_');
                return View("Error");
            }

            return RedirectToAction("IDTCAncellationDone", new { tp = "CANCEL", DockNo = Docket.DOCKNO });
        }
        public ActionResult IDTCancellationDone(string tp, string DockNo)
        {
            {
                ViewBag.DockNo = DockNo;
                if (tp == "CANCEL")
                {
                    ViewBag.Result = "Following Document has been Cancelled Successfully";
                }
                return View();
            }
        }
        #endregion

        #region Bill Cancellation

        public ActionResult MRCancellation()
        {
            BillCollectionViewModel BGVM = new BillCollectionViewModel();
            return View(BGVM);


        }

        public ActionResult MRcancellist(BillCollectionViewModel Viewmodel)
        {
            List<WEBX_MR_VPLIST> List = new List<WEBX_MR_VPLIST>();
            try
            {
                List = ES.GetMRCancellationList(Viewmodel.ObjBillMst.DATEFROM, Viewmodel.ObjBillMst.DATETO, Viewmodel.MRSNO, Viewmodel.ManualMRNO, Viewmodel.PAYBAS);
                Viewmodel.TransactionDate = DateTime.Now;
                Viewmodel.mrList = List;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "MRcancellist", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(Viewmodel);
        }


        [HttpPost]
        public ActionResult MRCancellationSubmit(BillCollectionViewModel BLVM, List<WEBX_MR_VPLIST> MRCancelList)
        {

            string MRSNoList = "";
            string BaseFinancialYear = BaseFinYear.ToString().Substring(0, 4);
            BaseFinancialYear = BaseFinYear.Substring(2, 2);
            double NextFinYear = 0;
            NextFinYear = Convert.ToDouble(BaseFinancialYear) + 1;
            string FinYear = BaseFinancialYear + "_" + NextFinYear.ToString().PadLeft(2, '0');
            DateTime MRCancelDate = BLVM.TransactionDate;
            string MRCancelReason = BLVM.Remarks;

            try
            {

                string Xml_MR_Details = "<root>";
                foreach (var item in MRCancelList)
                {
                    if (item.IsCancelled == true)
                    {


                        Xml_MR_Details = Xml_MR_Details + "<MRHDR>";
                        Xml_MR_Details = Xml_MR_Details + "<MRNO>" + item.mrsno + "</MRNO>";
                        Xml_MR_Details = Xml_MR_Details + "</MRHDR>";
                        MRSNoList += item.mrsno + ",";
                    }
                }
                Xml_MR_Details = Xml_MR_Details + "</root>";
                DataTable DT = ES.GetMRCancellationSubmit(Xml_MR_Details, MRCancelDate, MRCancelReason, BaseFinYear, User.Identity.Name, BaseCompanyCode.ToString().Trim());

                if (DT.Rows[0][0].ToString() == "Done")
                {

                    MRSNoList = MRSNoList.Substring(0, MRSNoList.Length - 1).ToString();
                }
                else
                {
                    throw new Exception("MR Cancellation Not Successfully Done...!");
                }
            }

            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "MRCancellationSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
                return View("Error");
            }

            return RedirectToAction("MRCancellationDone", new { MRSNoList = MRSNoList });



        }

        public ActionResult MRCancellationDone(string MRSNoList)
        {
            string[] strarray = MRSNoList.Split(',');
            List<string> strList = new List<string>();
            ViewBag.MRNO = MRSNoList;

            foreach (string obj in strarray)
                strList.Add(obj);

            return View(strList);
        }


        #endregion

        #region IDT-Edit

        public ActionResult IDTEdit()
        {
            WebX_Master_Docket Master = new WebX_Master_Docket();
            return View(Master);
        }

        public JsonResult IDTEditDetail(string IDTNo)
        {
            DataTable DT = new DataTable();
            DT = ES.CheckIdtNoForEdit(IDTNo, BaseLocationCode);
            string Confirm = "";
            if (DT.Rows.Count > 0)
                Confirm = "Y";
            else
                Confirm = "N";

            return new JsonResult()
            {
                Data = new
                {
                    Flg = Confirm,
                    MSG = "Not Found IDT for Edit"
                }
            };
        }
        public JsonResult GetCityList(string searchTerm)
        {
            searchTerm = searchTerm.ToUpper();

            List<webx_citymaster> CMP = ES.GetCityList(searchTerm);

            var users = from user in CMP.ToList()
                        select new
                        {
                            id = user.Location,
                            text = user.city_code

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }
        public ActionResult IDTEditResult(WebX_Master_Docket Docket)
        {
            ViewBag.LocCode = BaseLocationCode;
            DocketViewModel DVM = new DocketViewModel();
            WebX_Master_Docket_Charges WMDC = ES.GetChargesDetails(Docket.DOCKNO);
            WebX_Master_Docket WMD = ES.GetDetails(Docket.DOCKNO);


            if (WMD.CSGECD == "8888")
            {
                WMD.WIC = WMD.CSGENM;
                WMD.CSGECD = "";
                WMD.CSGENM = "";
            }
            List<WebX_Master_Docket_Invoice> ListInVoice = ES.GetIDTInvoice(Docket.DOCKNO);

            List<QuickDocketBalancecharges> ListCharges = new List<QuickDocketBalancecharges>();

            if (ListInVoice.Count() == 0)
            {
                WebX_Master_Docket_Invoice CMDI = new WebX_Master_Docket_Invoice();
                CMDI.SrNo = 1;
                CMDI.INVDT = System.DateTime.Now;
                CMDI.DECLVAL = 0;
                CMDI.PKGSNO = 0;
                CMDI.ACTUWT = 0;
                CMDI.VOL_L = 0;
                CMDI.VOL_B = 0;
                CMDI.VOL_H = 0;
                ListInVoice.Add(CMDI);
                CMDI = new WebX_Master_Docket_Invoice();
            }
            WMD.ChargeRule = MS.GetRuleObject("CHRG_RULE").defaultvalue;
            WMD.FlagStaxBifer = MS.GetRuleObject("STAX_BIFER").defaultvalue.IndexOf(BaseCompanyCode) >= 0 ? "Y" : "N";
            WMD.FLAG_PRORATA = MS.GetRuleObject("FLAG_PRORATA").defaultvalue;
            DVM.WMD = WMD;
            DVM.WMDC = WMDC;
            DVM.ListInVoice = ListInVoice;
            ViewBag.DKTCalledAs = "IDT";
            return View(DVM);
        }

        public ActionResult IDTInvoiceDetails(int id)
        {

            WebX_Master_Docket_Invoice WMDI = new WebX_Master_Docket_Invoice();
            WMDI.SrNo = id;
            WMDI.INVDT = System.DateTime.Now;
            WMDI.DECLVAL = 0;
            WMDI.PKGSNO = 0;
            WMDI.ACTUWT = 0;


            return PartialView("_IDTDocketInvoice", WMDI);
        }
        [HttpPost]
        public ActionResult IDTEditSubmit(DocketViewModel docketViewModel, List<WebX_Master_Docket_Invoice> DocketInvoiceList)
        {
            try
            {
                #region Validations
                //string bookdaterule = ES.GetBookDateRule(ES.GetDocketRule("RANGE_DKTDT").defaultvalue,BaseFinYear);
                string bookdaterule = ES.GetBookDateRule(MS.GetRuleObject("RANGE_DKTDT").defaultvalue, BaseFinYear);


                if (!ES.IsBookDateAllowed(docketViewModel.WMD.DOCKDT.ToString("dd MMM yyyy"), bookdaterule, BaseYearValFirst))
                {
                    ViewBag.Heading = "Invalid Booking Date.";
                    ViewBag.detail1 = "Out of Book Date Range";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Date";
                    return View("EditErrorPage");
                }
                if (!ES.IsActiveGeneralMaster("PAYTYP", docketViewModel.WMD.PAYBAS))
                {
                    ViewBag.Heading = "Invalid Payment Type.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Payment Type";
                    return View("EditErrorPage");
                }
                if (ES.IsActiveCustomer(docketViewModel.WMD.CSGNCD, docketViewModel.WMD.ORGNCD))
                {
                    ViewBag.Heading = "Invalid Customer or not for Location " + docketViewModel.WMD.ORGNCD;
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Party Code & Name";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.ctr_no.CompareTo("") == 0)
                {
                    ViewBag.Heading = "Customer" + ViewBag.DKTCalledAs;
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Customer" + ViewBag.DKTCalledAs + "Number";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.ctr_date.ToString().CompareTo("") == 0)
                {
                    ViewBag.Heading = "Customer" + ViewBag.DKTCalledAs + "Date not Found";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Customer" + ViewBag.DKTCalledAs + "Number";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.REASSIGN_DESTCD.CompareTo("") == 0)
                {
                    ViewBag.Heading = "Delivery Location not Found";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Delivery Location";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.CSGECD != "")
                {
                    if (!ES.IsActiveCustomer(docketViewModel.WMD.CSGECD, docketViewModel.WMD.REASSIGN_DESTCD))
                    {
                        ViewBag.Heading = "Invalid Consignee or not for Location." + docketViewModel.WMD.REASSIGN_DESTCD;
                        ViewBag.detail1 = "Invalid User Entry";
                        ViewBag.suggestion1 = "Re-Enter IDT with Proper Consignee Code & Name";
                        return View("EditErrorPage");
                    }
                }
                else
                {
                    if (docketViewModel.WMD.WIC == "")
                    {
                        ViewBag.Heading = "Consignee Name is not  Entered.";
                        ViewBag.detail1 = "Invalid User Entry";
                        ViewBag.suggestion1 = "Re-Enter IDT with Proper Consignee Name";
                        return View("EditErrorPage");
                    }
                }
                if (!ES.IsActiveGeneralMaster("PKGS", docketViewModel.WMD.PKGSTY))
                {
                    ViewBag.Heading = "Invalid Packaging Type.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Packaging Type";
                    return View("EditErrorPage");
                }
                if (!ES.IsActiveGeneralMaster("PROD", docketViewModel.WMD.PRODCD))
                {
                    ViewBag.Heading = "Invalid Product Type.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Product Type";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.DECVAL == 0)
                {
                    ViewBag.Heading = "Invalid Delcared Value.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Declared Value";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.ACTUWT == 0)
                {
                    ViewBag.Heading = "Invalid Actual Weight.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Actual Weight";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.PKGSNO == 0)
                {
                    ViewBag.Heading = "Invalid No of Package";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper No of Packages";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMD.CHRGWT == 0)
                {
                    ViewBag.Heading = "Invalid Charged Weight.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper Charged Weight";
                    return View("EditErrorPage");
                }
                if (docketViewModel.WMDC.DKTTOT == 0)
                {
                    ViewBag.Heading = "Invalid IDT Amount.";
                    ViewBag.detail1 = "Invalid User Entry";
                    ViewBag.suggestion1 = "Re-Enter IDT with Proper IDT Amount";
                    return View("EditErrorPage");
                }
                #endregion
                if (docketViewModel.WMD.codamt == null)
                {
                    docketViewModel.WMD.codamt = 0;
                }
                string DocketXMl = "<Docket>";
                DocketXMl = DocketXMl + "<DockNo>" + docketViewModel.WMD.DOCKNO + "</DockNo>";
                DocketXMl = DocketXMl + "<DockDate>" + docketViewModel.WMD.DOCKDT.ToString("dd MMMM yyyy") + "</DockDate>";
                DocketXMl = DocketXMl + "<PayBase>" + docketViewModel.WMD.PAYBAS + "</PayBase>";
                DocketXMl = DocketXMl + "<PartyCode>" + docketViewModel.WMD.PARTY_CODE + "</PartyCode>";
                DocketXMl = DocketXMl + "<PartyName>" + docketViewModel.WMD.party_name + "</PartyName>";
                DocketXMl = DocketXMl + "<ConsignorCode>" + docketViewModel.WMD.PARTY_CODE + "</ConsignorCode>";
                DocketXMl = DocketXMl + "<ConsignorName>" + docketViewModel.WMD.party_name + "</ConsignorName>";
                DocketXMl = DocketXMl + "<ConsigneeCode>" + (docketViewModel.WMD.CSGECD != null ? docketViewModel.WMD.CSGECD : "8888") + "</ConsigneeCode>";
                DocketXMl = DocketXMl + "<ConsigneeName>" + (docketViewModel.WMD.CSGENM != null ? docketViewModel.WMD.CSGENM : docketViewModel.WMD.WIC) + "</ConsigneeName>";
                DocketXMl = DocketXMl + "<DelLoc>" + docketViewModel.WMD.REASSIGN_DESTCD + "</DelLoc>";
                DocketXMl = DocketXMl + "<TransMode>" + "2" + "</TransMode>";
                DocketXMl = DocketXMl + "<ServiceType>" + "1" + "</ServiceType>";
                DocketXMl = DocketXMl + "<FTLType>" + "0" + "</FTLType>";
                DocketXMl = DocketXMl + "<FromCity>" + docketViewModel.WMD.from_loc + "</FromCity>";
                DocketXMl = DocketXMl + "<ToCity>" + docketViewModel.WMD.to_loc + "</ToCity>";
                DocketXMl = DocketXMl + "<businesstype>" + docketViewModel.WMD.businesstype + "</businesstype>";
                DocketXMl = DocketXMl + "<loadtype>" + docketViewModel.WMD.loadtype + "</loadtype>";
                DocketXMl = DocketXMl + "<FlagCODDOD>" + docketViewModel.WMD.COD_DOD + "</FlagCODDOD>";
                DocketXMl = DocketXMl + "<FlagDACC>" + docketViewModel.WMD.DACC_YN + "</FlagDACC>";
                DocketXMl = DocketXMl + "<FlagODA>" + docketViewModel.WMD.DIPLOMAT + "</FlagODA>";
                DocketXMl = DocketXMl + "<FlagLocal>" + docketViewModel.WMD.LocalCN_YN + "</FlagLocal>";
                DocketXMl = DocketXMl + "<PackType>" + docketViewModel.WMD.PKGSTY + "</PackType>";
                DocketXMl = DocketXMl + "<ProdType>" + docketViewModel.WMD.PRODCD + "</ProdType>";
                DocketXMl = DocketXMl + "<Remarks>" + docketViewModel.WMD.spl_svc_req + "</Remarks>";
                DocketXMl = DocketXMl + "<ChargedWeight>" + docketViewModel.WMD.CHRGWT + "</ChargedWeight>";
                DocketXMl = DocketXMl + "<ActualWeight>" + docketViewModel.WMD.ACTUWT + "</ActualWeight>";
                DocketXMl = DocketXMl + "<NoOfPkgs>" + docketViewModel.WMD.PKGSNO + "</NoOfPkgs>";
                DocketXMl = DocketXMl + "<FlagVolumetric>" + docketViewModel.WMD.CFT_YN + "</FlagVolumetric>";
                DocketXMl = DocketXMl + "<STaxPaidBy>" + docketViewModel.WMD.stax_paidby + "</STaxPaidBy>";
                DocketXMl = DocketXMl + "<CODDODAmount>" + docketViewModel.WMD.codamt + "</CODDODAmount>";
                DocketXMl = DocketXMl + "<EDD></EDD>";
                DocketXMl = DocketXMl + "<BilledAt>" + docketViewModel.WMD.ORGNCD + "</BilledAt>";
                DocketXMl = DocketXMl + "<LastEditBy>" + docketViewModel.WMD.LastEditBy + "</LastEditBy>";
                DocketXMl = DocketXMl + "<PartyAs>" + docketViewModel.WMD.party_as + "</PartyAs>";
                DocketXMl = DocketXMl + "<FlagStaxExmpt>" + docketViewModel.WMD.stax_exmpt_yn + "</FlagStaxExmpt>";
                DocketXMl = DocketXMl + "<EngineNo>" + docketViewModel.WMD.EngineNo + "</EngineNo>";
                DocketXMl = DocketXMl + "<ModelNo>" + docketViewModel.WMD.ModelNo + "</ModelNo>";
                DocketXMl = DocketXMl + "<GPSNo>" + docketViewModel.WMD.GPSNo + "</GPSNo>";
                DocketXMl = DocketXMl + "<ChassisNo>" + docketViewModel.WMD.ChassisNo + "</ChassisNo>";
                DocketXMl = DocketXMl + "<CustRefDelNo>" + docketViewModel.WMD.CustomerRefNo + "</CustRefDelNo>";
                DocketXMl = DocketXMl + "<Industry>" + docketViewModel.WMD.Industry + "</Industry>";
                DocketXMl = DocketXMl + "<ContractID>" + docketViewModel.WMD.ContractId + "</ContractID>";
                DocketXMl = DocketXMl + "<PickUpDel>" + docketViewModel.WMD.Pickup_Dely + "</PickUpDel>";
                DocketXMl = DocketXMl + "<EntryBy>" + BaseUserName + "</EntryBy>";
                DocketXMl = DocketXMl + "</Docket>";

                string InvoiceXML = "<ArrayOfDocketInvoice>";

                foreach (var item in DocketInvoiceList)
                {
                    if (item.VOL_B == null)
                    {
                        item.VOL_B = 0;
                    }
                    if (item.VOL_L == null)
                    {
                        item.VOL_L = 0;
                    }
                    if (item.VOL_H == null)
                    {
                        item.VOL_H = 0;
                    }


                    InvoiceXML = InvoiceXML + "<DocketInvoice>";
                    InvoiceXML = InvoiceXML + "<DockNo>" + docketViewModel.WMD.DOCKNO + "</DockNo>";
                    InvoiceXML = InvoiceXML + "<InvoiceNo>" + item.INVNO + "</InvoiceNo>";
                    InvoiceXML = InvoiceXML + "<InvoiceDate>" + item.INVDT + "</InvoiceDate>";
                    InvoiceXML = InvoiceXML + "<DeclaredValue>" + item.DECLVAL + "</DeclaredValue>";
                    InvoiceXML = InvoiceXML + "<NoOfPkgs>" + item.PKGSNO + "</NoOfPkgs>";
                    InvoiceXML = InvoiceXML + "<ActualWeight>" + item.ACTUWT + "</ActualWeight>";
                    InvoiceXML = InvoiceXML + "<Vol_Length>" + item.VOL_L + "</Vol_Length>";
                    InvoiceXML = InvoiceXML + "<Vol_Breadth>" + item.VOL_B + "</Vol_Breadth>";
                    InvoiceXML = InvoiceXML + "<Vol_Height>" + item.VOL_H + "</Vol_Height>";
                    InvoiceXML = InvoiceXML + "<vol_cft>" + item.vol_cft + "</vol_cft>";
                    InvoiceXML = InvoiceXML + "</DocketInvoice>";
                }
                InvoiceXML = InvoiceXML + "</ArrayOfDocketInvoice>";

                string DocketChargesXML = "<DocketCharges>";
                if (docketViewModel.WMDC.SCHG01 == null)
                {
                    docketViewModel.WMDC.SCHG01 = 0;
                }
                if (docketViewModel.WMDC.SCHG02 == null)
                {
                    docketViewModel.WMDC.SCHG02 = 0;
                }
                if (docketViewModel.WMDC.SCHG03 == null)
                {
                    docketViewModel.WMDC.SCHG03 = 0;
                }
                if (docketViewModel.WMDC.SCHG04 == null)
                {
                    docketViewModel.WMDC.SCHG04 = 0;
                }
                if (docketViewModel.WMDC.SCHG05 == null)
                {
                    docketViewModel.WMDC.SCHG05 = 0;
                }
                if (docketViewModel.WMDC.SCHG06 == null)
                {
                    docketViewModel.WMDC.SCHG06 = 0;
                }
                if (docketViewModel.WMDC.SCHG07 == null)
                {
                    docketViewModel.WMDC.SCHG07 = 0;
                }
                if (docketViewModel.WMDC.SCHG08 == null)
                {
                    docketViewModel.WMDC.SCHG08 = 0;
                }
                if (docketViewModel.WMDC.SCHG09 == null)
                {
                    docketViewModel.WMDC.SCHG09 = 0;
                }
                if (docketViewModel.WMDC.SCHG10 == null)
                {
                    docketViewModel.WMDC.SCHG10 = 0;
                }
                if (docketViewModel.WMDC.SCHG11 == null)
                {
                    docketViewModel.WMDC.SCHG11 = 0;
                }
                if (docketViewModel.WMDC.SCHG12 == null)
                {
                    docketViewModel.WMDC.SCHG12 = 0;
                }
                if (docketViewModel.WMDC.SCHG13 == null)
                {
                    docketViewModel.WMDC.SCHG13 = 0;
                }
                if (docketViewModel.WMDC.SCHG14 == null)
                {
                    docketViewModel.WMDC.SCHG14 = 0;
                }
                if (docketViewModel.WMDC.SCHG15 == null)
                {
                    docketViewModel.WMDC.SCHG15 = 0;
                }
                if (docketViewModel.WMDC.SCHG16 == null)
                {
                    docketViewModel.WMDC.SCHG16 = 0;
                }
                if (docketViewModel.WMDC.SCHG17 == null)
                {
                    docketViewModel.WMDC.SCHG17 = 0;
                }
                if (docketViewModel.WMDC.SCHG18 == null)
                {
                    docketViewModel.WMDC.SCHG18 = 0;
                }
                if (docketViewModel.WMDC.SCHG19 == null)
                {
                    docketViewModel.WMDC.SCHG19 = 0;
                }
                if (docketViewModel.WMDC.SCHG20 == null)
                {
                    docketViewModel.WMDC.SCHG20 = 0;
                }
                if (docketViewModel.WMDC.SCHG21 == null)
                {
                    docketViewModel.WMDC.SCHG21 = 0;
                }
                if (docketViewModel.WMDC.SCHG22 == null)
                {
                    docketViewModel.WMDC.SCHG22 = 0;
                }
                if (docketViewModel.WMDC.SCHG23 == null)
                {
                    docketViewModel.WMDC.SCHG23 = 0;
                }
                if (docketViewModel.WMDC.UCHG01 == null)
                {
                    docketViewModel.WMDC.UCHG01 = 0;
                }
                if (docketViewModel.WMDC.UCHG02 == null)
                {
                    docketViewModel.WMDC.UCHG02 = 0;
                }
                if (docketViewModel.WMDC.UCHG03 == null)
                {
                    docketViewModel.WMDC.UCHG03 = 0;
                }
                if (docketViewModel.WMDC.UCHG04 == null)
                {
                    docketViewModel.WMDC.UCHG04 = 0;
                }
                if (docketViewModel.WMDC.UCHG05 == null)
                {
                    docketViewModel.WMDC.UCHG05 = 0;
                }
                if (docketViewModel.WMDC.UCHG06 == null)
                {
                    docketViewModel.WMDC.UCHG06 = 0;
                }
                if (docketViewModel.WMDC.UCHG07 == null)
                {
                    docketViewModel.WMDC.UCHG07 = 0;
                }
                if (docketViewModel.WMDC.UCHG08 == null)
                {
                    docketViewModel.WMDC.UCHG08 = 0;
                }
                if (docketViewModel.WMDC.UCHG09 == null)
                {
                    docketViewModel.WMDC.UCHG09 = 0;
                }
                if (docketViewModel.WMDC.UCHG10 == null)
                {
                    docketViewModel.WMDC.UCHG10 = 0;
                }
                if (docketViewModel.WMDC.SVCTAX == null)
                {
                    docketViewModel.WMDC.SVCTAX = 0;
                }
                if (docketViewModel.WMDC.CESS == null)
                {
                    docketViewModel.WMDC.CESS = 0;
                }
                if (docketViewModel.WMDC.hedu_cess == null)
                {
                    docketViewModel.WMDC.hedu_cess = 0;
                }
                if (docketViewModel.WMDC.SbcRate == null)
                {
                    docketViewModel.WMDC.SbcRate = 0;
                }
                if (docketViewModel.WMDC.SBCess == null)
                {
                    docketViewModel.WMDC.SBCess = 0;
                }
                DocketChargesXML = DocketChargesXML + "<DockNo>" + docketViewModel.WMD.DOCKNO + "</DockNo>";
                DocketChargesXML = DocketChargesXML + "<RateType>" + docketViewModel.WMDC.RATE_TYPE + "</RateType>";
                DocketChargesXML = DocketChargesXML + "<FreightRate>" + docketViewModel.WMDC.FREIGHT + "</FreightRate>";
                DocketChargesXML = DocketChargesXML + "<Freight>" + docketViewModel.WMDC.FREIGHT + "</Freight>";
                DocketChargesXML = DocketChargesXML + "<FOV>" + docketViewModel.WMDC.SCHG11 + "</FOV>";
                DocketChargesXML = DocketChargesXML + "<SCHG01>" + docketViewModel.WMDC.SCHG01 + "</SCHG01>";
                DocketChargesXML = DocketChargesXML + "<SCHG02>" + docketViewModel.WMDC.SCHG02 + "</SCHG02>";
                DocketChargesXML = DocketChargesXML + "<SCHG03>" + docketViewModel.WMDC.SCHG03 + "</SCHG03>";
                DocketChargesXML = DocketChargesXML + "<SCHG04>" + docketViewModel.WMDC.SCHG04 + "</SCHG04>";
                DocketChargesXML = DocketChargesXML + "<SCHG05>" + docketViewModel.WMDC.SCHG05 + "</SCHG05>";
                DocketChargesXML = DocketChargesXML + "<SCHG06>" + docketViewModel.WMDC.SCHG06 + "</SCHG06>";
                DocketChargesXML = DocketChargesXML + "<SCHG07>" + docketViewModel.WMDC.SCHG07 + "</SCHG07>";
                DocketChargesXML = DocketChargesXML + "<SCHG08>" + docketViewModel.WMDC.SCHG08 + "</SCHG08>";
                DocketChargesXML = DocketChargesXML + "<SCHG09>" + docketViewModel.WMDC.SCHG09 + "</SCHG09>";
                DocketChargesXML = DocketChargesXML + "<SCHG10>" + docketViewModel.WMDC.SCHG10 + "</SCHG10>";
                DocketChargesXML = DocketChargesXML + "<SCHG11>" + docketViewModel.WMDC.SCHG11 + "</SCHG11>";
                DocketChargesXML = DocketChargesXML + "<SCHG12>" + docketViewModel.WMDC.SCHG12 + "</SCHG12>";
                DocketChargesXML = DocketChargesXML + "<SCHG13>" + docketViewModel.WMDC.SCHG13 + "</SCHG13>";
                DocketChargesXML = DocketChargesXML + "<SCHG14>" + docketViewModel.WMDC.SCHG14 + "</SCHG14>";
                DocketChargesXML = DocketChargesXML + "<SCHG15>" + docketViewModel.WMDC.SCHG15 + "</SCHG15>";
                DocketChargesXML = DocketChargesXML + "<SCHG16>" + docketViewModel.WMDC.SCHG16 + "</SCHG16>";
                DocketChargesXML = DocketChargesXML + "<SCHG17>" + docketViewModel.WMDC.SCHG17 + "</SCHG17>";
                DocketChargesXML = DocketChargesXML + "<SCHG18>" + docketViewModel.WMDC.SCHG18 + "</SCHG18>";
                DocketChargesXML = DocketChargesXML + "<SCHG19>" + docketViewModel.WMDC.SCHG19 + "</SCHG19>";
                DocketChargesXML = DocketChargesXML + "<SCHG20>" + docketViewModel.WMDC.SCHG20 + "</SCHG20>";
                DocketChargesXML = DocketChargesXML + "<SCHG21>" + docketViewModel.WMDC.SCHG21 + "</SCHG21>";
                DocketChargesXML = DocketChargesXML + "<SCHG22>" + docketViewModel.WMDC.SCHG22 + "</SCHG22>";
                DocketChargesXML = DocketChargesXML + "<SCHG23>" + docketViewModel.WMDC.SCHG23 + "</SCHG23>";
                DocketChargesXML = DocketChargesXML + "<UCHG01>" + docketViewModel.WMDC.UCHG01 + "</UCHG01>";
                DocketChargesXML = DocketChargesXML + "<UCHG02>" + docketViewModel.WMDC.UCHG02 + "</UCHG02>";
                DocketChargesXML = DocketChargesXML + "<UCHG03>" + docketViewModel.WMDC.UCHG03 + "</UCHG03>";
                DocketChargesXML = DocketChargesXML + "<UCHG04>" + docketViewModel.WMDC.UCHG04 + "</UCHG04>";
                DocketChargesXML = DocketChargesXML + "<UCHG05>" + docketViewModel.WMDC.UCHG05 + "</UCHG05>";
                DocketChargesXML = DocketChargesXML + "<UCHG06>" + docketViewModel.WMDC.UCHG06 + "</UCHG06>";
                DocketChargesXML = DocketChargesXML + "<UCHG07>" + docketViewModel.WMDC.UCHG07 + "</UCHG07>";
                DocketChargesXML = DocketChargesXML + "<UCHG08>" + docketViewModel.WMDC.UCHG08 + "</UCHG08>";
                DocketChargesXML = DocketChargesXML + "<UCHG09>" + docketViewModel.WMDC.UCHG09 + "</UCHG09>";
                DocketChargesXML = DocketChargesXML + "<UCHG10>" + docketViewModel.WMDC.UCHG10 + "</UCHG10>";
                DocketChargesXML = DocketChargesXML + "<ServiceTax>" + docketViewModel.WMDC.SVCTAX + "</ServiceTax>";
                DocketChargesXML = DocketChargesXML + "<ServiceTaxRate>" + docketViewModel.WMDC.SVCTAX_Rate + "</ServiceTaxRate>";
                DocketChargesXML = DocketChargesXML + "<SubTotal>" + docketViewModel.WMDC.DKTTOT + "</SubTotal>";
                DocketChargesXML = DocketChargesXML + "<EduCess>" + docketViewModel.WMDC.CESS + "</EduCess>";
                DocketChargesXML = DocketChargesXML + "<HEduCess>" + docketViewModel.WMDC.hedu_cess + "</HEduCess>";
                DocketChargesXML = DocketChargesXML + "<DocketTotal>" + docketViewModel.WMDC.DKTTOT + "</DocketTotal>";
                DocketChargesXML = DocketChargesXML + "<SbcRate>" + docketViewModel.WMDC.SbcRate + "</SbcRate>";
                DocketChargesXML = DocketChargesXML + "<SBCess>" + docketViewModel.WMDC.SBCess + "</SBCess>";
                DocketChargesXML = DocketChargesXML + "</DocketCharges>";


                DataTable DT = ES.UpdateIDT(DocketXMl.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), DocketChargesXML.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim(), InvoiceXML.Replace("&", "&amp;").Replace("–", "-").Replace("'", "").Trim());
                ViewBag.DockNo = docketViewModel.WMD.DOCKNO;
                ViewBag.Result = "Edit Successfully";
            }

            catch (Exception ex)
            {
                ViewBag.StrError = "Error " + ex.Message;
                return View("Error");
            }

            return RedirectToAction("IDTEditDone", new { DockNo = ViewBag.DockNo, Result = ViewBag.Result });
        }

        public ActionResult IDTEditDone(string IDTNo, string Result)
        {
            ViewBag.DockNo = IDTNo;
            ViewBag.Result = Result;
            return View();
        }


        #endregion

        #region  ReAssign Voucher Date

        public ActionResult ReAssnVoucherDate()
        {
            ReAssnVoucherDate Voucher = new ReAssnVoucherDate();
            Voucher.ListWVMM = new List<webx_acctrans>();
            return View(Voucher);
        }
        public ActionResult ADDReAssnVoucherDate(int id)
        {
            webx_acctrans WMFSM = new webx_acctrans();
            WMFSM.Srno = id;
            return PartialView("_ReAssnVoucherDateAddRow", WMFSM);
        }
        public string GetVocherDate_EditJson(string SerchGroup)
        {
            List<webx_location> ListGroups = new List<webx_location> { };
            try
            {
                DataTable Dt_Location = ES.GetVoucherDateEdit(SerchGroup, BaseYearVal, BaseCompanyCode);

                return JsonConvert.SerializeObject(Dt_Location);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetVocherDate_EditJson", "Json", "Listing", ex.Message);
                return JsonConvert.SerializeObject(null);
                //return Json(ListGroups);
            }
        }
        public ActionResult ReAssnVoucherDateSubmit(AdvanceTripVoucherCancellation Voucher, List<webx_acctrans> VoucherList)
        {
            webx_acctrans Rs = new webx_acctrans();
            List<webx_acctrans> Results = new List<webx_acctrans>();
            string VoucherNoList = "";
            string TranXaction = "Done";

            try
            {
                foreach (var item in VoucherList)
                {
                    if (item.Voucherno != "")
                    {
                        DataTable DT = new DataTable();
                        DT = ES.VoucherDateEdit(Convert.ToString(item.VoucherEditDate), item.Voucherno, BaseYearVal, BaseUserName);
                        Rs.Voucherno = DT.Rows[0][0].ToString();

                        Results.Add(Rs);


                    }
                    if (VoucherNoList == "")
                    {
                        VoucherNoList = Rs.Voucherno;
                    }
                    else
                    {
                        VoucherNoList = VoucherNoList + "," + Rs.Voucherno;
                    }
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = ex.Message;
                return View("Error");
            }
            return RedirectToAction("ReAssnVoucherDateDone", new { VoucherNoList = VoucherNoList, TranXaction = TranXaction });
        }
        public ActionResult ReAssnVoucherDateDone(string VoucherNoList, string TranXaction)
        {
            //ViewBag.Voucherno = Voucherno;
            ViewBag.TranXaction = TranXaction;
            //return View();


            string[] strarray = VoucherNoList.Split(',');
            List<string> strList = new List<string>();
            ViewBag.CFRNO = VoucherNoList;

            foreach (string obj in strarray)
                strList.Add(obj);

            ViewBag.list = strList;

            return View();

        }

        #endregion
    }
}