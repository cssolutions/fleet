﻿using CYGNUS.Classes;
using CYGNUS.Models;
using Fleet.Classes;
using Fleet.ViewModels;
using FleetDataService.Models;
using FleetDataService.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Xml.Serialization;

namespace Fleet.Controllers
{
    [Authorize]
    [NoDirectAccess]
    public class VoucherController : BaseController
    {
        FleetDataService.OperationService OS = new FleetDataService.OperationService();
        FleetDataService.MasterService MS = new FleetDataService.MasterService();
        FleetDataService.FinanceService FS = new FleetDataService.FinanceService();
        FleetDataService.PaymentService PS = new FleetDataService.PaymentService();
        FleetDataService.FleetService FLTS = new FleetDataService.FleetService();
        GeneralFuncations GF = new GeneralFuncations();
        string ControllerName = "VoucherController";
        string Connstr = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        //
        // GET: /Voucher/

        public ActionResult Index()
        {
            return View();
        }

        #region DebitCreditVoucher

        public ActionResult DebitCreditVoucherCriteria(string id)
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName.ToUpper() == "ADMIN")
            //{
            //    Webx_Account_Details WAD = new Webx_Account_Details();
            //    return View(WAD);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}

            Webx_Account_Details WAD = new Webx_Account_Details();
            WAD.SelectVoucherType = id;
            return View(WAD);
        }

        [HttpPost]
        public ActionResult DebitCreditVoucher(Webx_Account_Details objWAD)
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName.ToUpper() == "ADMIN")
            //{
                DebitCreditVoucherViewModel viewmodel = new DebitCreditVoucherViewModel();
                List<Webx_Acc> ListWAD = new List<Webx_Acc>();
                viewmodel.BackDateAccess = MS.GetVoucherBackDateAccess(BaseUserName, "Debit_Credit_Voucher");
                viewmodel.WA = new Webx_Acc();
                viewmodel.WAD = new Webx_Account_Details();
                try
                {
                    DataTable DT_Tax = OS.GetServiceTaxRate();
                    decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
                    decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
                    decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());

                    viewmodel.WAD.VoucherDate = System.DateTime.Today;
                    viewmodel.WAD.Preparedatlocation = BaseLocationCode;
                    viewmodel.WAD.AccountingLocation = BaseLocationCode;
                    viewmodel.WAD.PrepardBy = BaseUserName.ToUpper();
                    viewmodel.WAD.Chequedate = System.DateTime.Today;
                    viewmodel.WA.Srno = 1;
                    viewmodel.WAD.GSTType = objWAD.GSTType;
                    viewmodel.WAD.TypeOfGst = objWAD.TypeOfGst;
                    viewmodel.WAD.GSTPercentage = objWAD.GSTPercentage;
                    ListWAD.Add(viewmodel.WA);

                    viewmodel.WADList = ListWAD;

                    Webx_Account_Details OBjWAD = new Webx_Account_Details();
                    OBjWAD.SRNo = 1;
                    OBjWAD.Vouchertype = "DEBITCREDIT";
                    viewmodel.WAD.SRNo = 1;
                    List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                    ListWAD1.Add(OBjWAD);
                    viewmodel.WADListing = ListWAD1;

                    viewmodel.WAD.EducationCess = HdnEduCessRate;
                    viewmodel.WAD.HEducationCess = HdnHEduCessRate;
                    viewmodel.WAD.ServiceTax = HdnServiceTaxRate;

                    viewmodel.HdnEduCessRate = HdnEduCessRate;
                    viewmodel.HdnHEduCessRate = HdnHEduCessRate;
                    viewmodel.HdnServiceTaxRate = HdnServiceTaxRate;
                    viewmodel.WAD.SelectVoucherType = objWAD.SelectVoucherType;

                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Message:-" + ex.Message;
                    Error_Logs("DebitCreditVoucher", "DebitCreditVoucher", "View", "AddEdit", ex.Message + "//" + ViewBag.StrError);
                    return View("Error");
                }
                return View(viewmodel);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        [HttpPost]
        public ActionResult DebitCreditVoucherSubmit(DebitCreditVoucherViewModel ACVVM, List<Webx_Acc> DCVoucher, List<Webx_Account_Details> PAYMENTMODE, List<CygnusChargesHeader> DynamicList, StaxTDSViewModel objStaxTDSViewModel, HttpPostedFileBase[] files)
        {
            SqlConnection con;

            string Year = BaseFinYear.ToString();
            string fin_year = "", PBOV_TYP = "";
            string Financial_Year = BaseYearVal; //System.DateTime.Now.FinYear.ToString().Substring(2, 2);
            fin_year = Financial_Year + "_" + Convert.ToString(Convert.ToDouble("16") + 1).PadLeft(2, '0');
            string EntryBy = BaseUserName;
            string Transtype = "";
            string Opertitile = "", Voucherno = "";
            string extension = "", UserFileName = "", Documentpath = "";

            #region   Debit And Credit Voucher Images Upload
            try
            {
                if (((System.Web.HttpPostedFileBase[])(files)) != null)
                {
                    foreach (var fileobj in files)
                    {
                        var file = ((System.Web.HttpPostedFileBase[])(files));
                        if (fileobj.ContentLength > 0)
                        {
                            extension = System.IO.Path.GetExtension(fileobj.FileName);
                            UserFileName = BaseUserName + extension;
                            Documentpath = Server.MapPath("~/Images/DebitAndCreditVoucher/") + UserFileName;
                            string strDirectoryName = Server.MapPath("~/Images/DebitAndCreditVoucher/");
                            if (Directory.Exists(strDirectoryName) == false)
                                Directory.CreateDirectory(strDirectoryName);
                            fileobj.SaveAs(Documentpath);
                        }

                    }
                }
            }
            catch (Exception)
            {
                // DocumentUploadedPath = Em.Elec_Expense_DET.b;
            }

            #endregion

            if (ACVVM.WAD.CustTyp == "P")
                PBOV_TYP = "P";
            else if (ACVVM.WAD.CustTyp == "V")
                PBOV_TYP = "V";
            else if (ACVVM.WAD.CustTyp == "E" || ACVVM.WAD.CustTyp == "U")
                PBOV_TYP = "U";
            else if (ACVVM.WAD.CustTyp == "D")
                PBOV_TYP = "D";
            else if (ACVVM.WAD.CustTyp == "L")
                PBOV_TYP = "L";
            else if (ACVVM.WAD.CustTyp == "O")
                PBOV_TYP = "O";

            Webx_Account_Details ObjWAD = new Webx_Account_Details();
            ObjWAD = PAYMENTMODE.First();

            if (ObjWAD.PaymentMode.ToUpper() != "CASH")
            {
                DataTable DT = OS.Duplicate_ChqNO(ObjWAD.ChequeNo, ObjWAD.Chequedate.ToString("dd MMM yyyy"));

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
            }

            if (ObjWAD.PaymentMode.ToUpper() != "BANK" && ObjWAD.TransactionMode.ToUpper() != "RECEIPT")
            {
                string FIN_Start = "";
                FIN_Start = "01 Apr " + BaseYearVal.Split('_')[0];
                DataTable DT = OS.DR_VR_Trn_Halt(ACVVM.WAD.AccountingLocation, Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy"), FIN_Start, BaseYearVal, ObjWAD.CashAmount.ToString());

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }

                if (Cnt != "T")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }
            }

            if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                Opertitile = "MANUAL DEBIT VOUCHER";

                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH PAYMENT";
                }
            }
            else
            {
                Opertitile = "MANUAL CREDIT VOUCHER";
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH RECEIPT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK RECEIPT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH RECEIPT";
                }
            }

            string PBOV_code = "", PBOV_Name = "";

            if (ACVVM.WAD.CustCode == "")
            {
                PBOV_code = "8888";
                PBOV_Name = ACVVM.WAD.CustomerName.Trim();
            }
            else
            {
                PBOV_code = ACVVM.WAD.CustCode.ToString();
                webx_CUSTHDR WCHdr = new webx_CUSTHDR();
                string Name = "";
                if (ACVVM.WAD.CustTyp == "E" && (ACVVM.WAD.CustCode == null || ACVVM.WAD.CustCode == ""))
                {
                    PBOV_code = "8888";
                    PBOV_Name = ACVVM.WAD.CustomerName.Trim();
                }
                else if (ACVVM.WAD.CustTyp != "O")
                {
                    try
                    {
                        WCHdr = OS.GetCustmer_Type(ACVVM.WAD.CustCode, PBOV_TYP).First();
                        Name = WCHdr.CUSTNM;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ') + " Please Select Proper Customer.";
                        return View("Error");
                    }
                }
                else
                {
                    Name = ACVVM.WAD.CustomerName.ToString();
                }
                PBOV_Name = Name.Trim();
            }

            string svrcheck = "N", tdscheck = "N", DepoFlag = "N", Str_Onaccount = "N";
            string BankCode = "";
            double adjustamt = 0;
            string TdsCalForLedgerAmt = "N";

            if (ACVVM.WAD.IsServicetaxapply)
            {
                svrcheck = "Y";
            }
            if (ACVVM.WAD.IsTDStaxapply)
            {
                tdscheck = "Y";
            }
            if (ACVVM.WAD.IsTDSWithoutSvcTax)
            {
                TdsCalForLedgerAmt = "Y";
            }

            if (ObjWAD.TransactionMode == "RECEIPT")
            {
                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    BankCode = "ACA0002";

                    if (ObjWAD.Deposited)
                    {
                        DepoFlag = "Y";
                        BankCode = ObjWAD.DepositedInBank;
                    }
                    adjustamt = Convert.ToDouble(ObjWAD.ChequeAmount);
                    if (ObjWAD.IsonAccount)
                    {
                        Str_Onaccount = "Y";
                        adjustamt = 0;
                    }
                }
            }
            else
            {
                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    DepoFlag = "Y";
                    BankCode = ObjWAD.DepositedInBank;
                }
            }

            string CompanyCode = "", Acccode = "", Accdesc = "", Debit = "";
            string Credit = "", Narration = "", Stax_App = "N", CostCenterId = "", CostCenter = "";
            string hdnroundoff = "N";
            string hdneditablsvctaxrate = "N";
            string HdnCompanyDropDownRule = "N";
            string HdnRowwiseSvcTaxRule = "N";
            string HdnTransactionWiseAccountRule = "N";
            string HdnCostCenterRule = "N";

            hdnroundoff = OS.ManualVouchersRule("RoundOff");
            hdneditablsvctaxrate = OS.ManualVouchersRule("EditableServiceTax");
            HdnCompanyDropDownRule = OS.ManualVouchersRule("CompanyDropDown");
            HdnRowwiseSvcTaxRule = OS.ManualVouchersRule("RowwiseSvcTax");
            HdnTransactionWiseAccountRule = OS.ManualVouchersRule("Transwiserule");
            HdnCostCenterRule = OS.ManualVouchersRule("CostCenterRule");

            string Xml_Acccode_Details = "<root>";

            if (DCVoucher != null)
            {
                foreach (var item in DCVoucher)
                {
                    CompanyCode = item.AccountCode;

                    if (HdnCostCenterRule == "Y")
                    {
                        //if (!chkCmnCostCenter.Checked)
                        //{
                        //    DropDownList ddlcctype = (DropDownList)gridrow.FindControl("ddlcctype");
                        //    TextBox txtcostcenter = (TextBox)gridrow.FindControl("txtcostcenter");

                        //    CostCenterId = ddlcctype.SelectedValue.ToString();
                        //    CostCenter = txtcostcenter.Text.ToString();
                        //}
                    }

                    if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                    {
                        Debit = item.Amount;
                        Credit = "0.00";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = item.Amount;
                    }

                    Narration = item.NarrationAccountinfo.Replace("'", "''");
                    bool chkstax = false;

                    if (HdnRowwiseSvcTaxRule == "Y")
                    {
                        if (chkstax)
                            Stax_App = "Y";
                        else
                            Stax_App = "N";
                    }

                    if (CompanyCode != "")
                    {
                        DataTable dt = OS.FindOutSystemAccountCode(CompanyCode, BaseLocationCode);
                        if (dt.Rows.Count == 0)
                        {
                            ViewBag.StrError = "Error : Not Valid Account Code : " + CompanyCode + ". So Please Enter The Valid Account Details";
                            return View("Error");
                        }
                        Acccode = dt.Rows[0]["Acccode"].ToString().Trim();
                        Accdesc = dt.Rows[0]["Accdesc"].ToString().Trim();
                        Accdesc = Accdesc.Replace("<", "&lt;");
                        Accdesc = Accdesc.Replace(">", "&gt;");
                        Accdesc = Accdesc.Replace("\"", "&quot;");
                        Accdesc = Accdesc.Replace("'", "&apos;");

                        if (Str_Onaccount == "Y" && (Acccode != "CDA0001" && Acccode != "ASS0267"))
                        {
                            ViewBag.StrError = "Error : On Account Cheque Facility is not Availble for : " + Accdesc + ". This Facility Available only for ledger : Billed Debtors";
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Acccode.Trim() + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc.Trim() + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit.Trim() + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit.Trim() + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration.Trim() + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Stax_App>" + Stax_App.Trim() + "</Stax_App>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenterId>" + CostCenterId.Trim() + "</CostCenterId>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenter>" + CostCenter.Trim() + "</CostCenter>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                }
            }

            Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

            string Company_code = BaseCompanyCode;
            string CommonCostCenter = "N";
            if (HdnCostCenterRule == "Y")
            {
                //if (chkCmnCostCenter.Checked)
                //{
                //    CommonCostCenter = "Y";
                //}
            }

            string BranchCode = "";
            if (ACVVM.WAD.AccountingLocation == "")
                BranchCode = BaseLocationCode;
            else
            {
                if (OS.CheckLocation(ACVVM.WAD.AccountingLocation))
                    BranchCode = ACVVM.WAD.AccountingLocation.Trim();
                else
                {
                    ViewBag.StrError = "Accounting Location Is Not Valid";
                    return View("Error");
                }
            }

            string Xml_Other_Details = "<root><Other>";
            Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
            Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
            Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
            Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BranchCode + "</Brcd>";
            Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName + "</Entryby>";
            Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString() + "</Opertitle>";
            Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ACVVM.WAD.ManualNo + "</ManualNo>";
            Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ACVVM.WAD.BusinessDivision + "</BusinessType>";
            Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
            Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + ACVVM.WAD.AccountingLocation + "</Acclocation>";
            Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ACVVM.WAD.PreparedFor + "</preparefor>";
            Xml_Other_Details = Xml_Other_Details + "<Refno>" + ACVVM.WAD.ReferenceNo + "</Refno>";
            Xml_Other_Details = Xml_Other_Details + "<Narration>" + ACVVM.WAD.Narration + "</Narration>";
            Xml_Other_Details = Xml_Other_Details + "<Panno>" + ACVVM.WAD.PANNumber + "</Panno>";
            Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ACVVM.WAD.ServiceTaxRegNo + "</Svrtaxno>";
            Xml_Other_Details = Xml_Other_Details + "<chqno>" + ObjWAD.ChequeNo + "</chqno>";
            Xml_Other_Details = Xml_Other_Details + "<chqamt>" + ObjWAD.ChequeAmount + "</chqamt>";
            Xml_Other_Details = Xml_Other_Details + "<chqdt>" + ObjWAD.Chequedate.ToString("dd MMM yyyy") + "</chqdt>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + PBOV_TYP + "</pbov_typ>";
            Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + svrcheck + "</Svrtax_yn>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + (objStaxTDSViewModel.IsTDSEnabled ? "Y" : "N") + "</Tds_yn>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + objStaxTDSViewModel.TDSAcccode + "</Tds_Acccode>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + objStaxTDSViewModel.TDSRate + "</Tds_rate>";
            Xml_Other_Details = Xml_Other_Details + "<TdsAmount>" + objStaxTDSViewModel.TDSAmount + "</TdsAmount>";
            Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
            Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ObjWAD.ReceivedFromBank + "</recbanknm>";
            Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + Str_Onaccount + "</Onaccount_YN>";
            Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag + "</Deposited>";
            Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ObjWAD.PaymentMode + "</Paymode>";
            Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ACVVM.WAD.ServiceTaxRate /*HdnServiceTaxRate*/ + "</SvrTaxRate>";
            Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ACVVM.WAD.EducationCess /*HdnEduCessRate*/ + "</EduCessRate>";
            Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ACVVM.WAD.HEducationCess /*HdnHEduCessRate*/ + "</HEduCessRate>";
            Xml_Other_Details = Xml_Other_Details + "<TdsOption>" + TdsCalForLedgerAmt + "</TdsOption>";
            Xml_Other_Details = Xml_Other_Details + "<CommonCostCenter>" + CommonCostCenter + "</CommonCostCenter>";
            Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + Company_code + "</COMPANY_CODE>";
            Xml_Other_Details = Xml_Other_Details + "<GSTType>" + ACVVM.WAD.GSTType + "</GSTType>";
            Xml_Other_Details = Xml_Other_Details + "<TYPEOFGST>" + ACVVM.WAD.TypeOfGst + "</TYPEOFGST>";
            Xml_Other_Details = Xml_Other_Details + "<Documentpath>" + UserFileName + "</Documentpath>";

            Xml_Other_Details = Xml_Other_Details + "</Other></root>";

            XmlDocument xmlDocGSTCha = new XmlDocument();
            if (DynamicList != null && DynamicList.Count > 0)
            {
                XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                    xmlStreamCha.Position = 0;
                    xmlDocGSTCha.Load(xmlStreamCha);
                }
            }
            else
            {
                xmlDocGSTCha.InnerXml = "<root></root>";
            }


            con = new SqlConnection(Connstr);
            con.Open();

            SqlTransaction trans;
            trans = con.BeginTransaction();

            double LedgerDiff = 0;

            try
            {
                Voucherno = OS.InsertVoucherDetails(Xml_Acccode_Details, Xml_Other_Details, xmlDocGSTCha.InnerXml, trans);
                LedgerDiff = OS.VoucherDiff(Voucherno, Financial_Year, trans);

                if (LedgerDiff != 0)
                {
                    ViewBag.StrError = "Error : Debit Amount Is Not Equal To Payment Amount";
                    return View("Error");
                }
                //else
                //{
                //    string newFName = "";

                //    try
                //    {
                //        if (Request.Files != null)
                //        {
                //            if (Request.Files["file"].ContentLength > 0)
                //            {
                //                string FileName = Request.Files["file"].FileName;
                //                newFName = GetFileName(FileName, "VEN_INV");


                //                if (CheckDocumentExistance(newFName) == true)
                //                {
                //                    ViewBag.StrError = "Document already exist.";
                //                    return View("Error");
                //                }

                //                if (CheckFMDocumentsDirectory() == false)
                //                {
                //                    ViewBag.StrError = "Fail to create directory.";
                //                    return View("Error");
                //                }
                //                string path = string.Format("{0}/{1}", Server.MapPath("~") + @"\GUI\Scan_Document\Vendor_Invoice_Scan\" + newFName);

                //                if (System.IO.File.Exists(path))
                //                    System.IO.File.Delete(path);

                //                Request.Files["file"].SaveAs(path);
                //                OS.UpdateVendorBillScanDetail(newFName, Voucherno, trans);
                //            }
                //        }
                //    }
                //    catch (Exception)
                //    {
                //        //throw;
                //    }
                //    trans.Commit();
                //}
            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                trans.Rollback();
                ViewBag.StrError = ErrorMsg;
                return View("Error");

            }
            con.Close();
            #region Start

            /*
             * 
            DataTable DT = new DataTable();

            Webx_Account_Details WAD = new Webx_Account_Details();
            WAD = PAYMENTMODE.FirstOrDefault();
            if (WAD != null)
            {
                ACVVM.WAD.TransactionMode = WAD.TransactionMode;
                ACVVM.WAD.PaymentMode = WAD.PaymentMode;
                ACVVM.WAD.CashAmount = WAD.CashAmount;
                ACVVM.WAD.ChequeAmount = WAD.ChequeAmount;
                ACVVM.WAD.ChequeNo = WAD.ChequeNo;
                ACVVM.WAD.Chequedate = WAD.Chequedate;
                ACVVM.WAD.IsonAccount = WAD.IsonAccount;
                ACVVM.WAD.NETAMOUNT = WAD.NETAMOUNT;
                ACVVM.WAD.CashAccount = WAD.CashAccount;
                ACVVM.WAD.Deposited = WAD.Deposited;
                ACVVM.WAD.DepositedInBank = WAD.DepositedInBank;
                ACVVM.WAD.ReceivedFromBank = WAD.ReceivedFromBank;
            }

            string VoucherNo = "", BankCode = "", Accdesc = "", costcodelist = "", coastName = "", Opertitile = "", Transtype = "";
            string Debit = "0.00", adjustamt = "0.00", Credit = "0.00";
            string DepoFlag = "";
            try
            {
                string Xml_Acccode_Details = "<root>";

                foreach (var item in DCVoucher)
                {
                    string CompanyCode = item.AccountCode;

                    if (ACVVM.WAD.TransactionMode == "PAYMENT")
                    {
                        Debit = item.Amount;
                        Credit = "0";
                    }
                    else
                    {
                        Debit = "0";
                        Credit = item.Amount;
                    }
                    string Narration = "";
                    try
                    {
                        Narration = item.NarrationAccountinfo.Replace("'", "''");
                    }
                    catch
                    {
                        Narration = "";
                    }

                    string SQR = "exec USP_GetAccountCodeList '" + item.AccountCode + "','1'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    string Description = Dt.Rows[0][1].ToString();

                    if (CompanyCode != "")
                    {


                        Accdesc = Description.Replace("<", "&lt;");
                        Accdesc = Description.Replace(">", "&gt;");
                        Accdesc = Description.Replace("\"", "&quot;");
                        Accdesc = Description.Replace("'", "&apos;");


                        if (CompanyCode != "")
                        {
                            if (costcodelist == "")
                            {
                                costcodelist = CompanyCode;
                            }
                            else
                            {
                                costcodelist = costcodelist + "," + CompanyCode;
                            }
                        }
                       

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + CompanyCode.Trim() + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc.Replace("&", "And") + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit.Trim() + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit.Trim() + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        //Xml_Acccode_Details = Xml_Acccode_Details + "<coasttype>" + item.CostType.Trim() + "</coasttype>";
                        //Xml_Acccode_Details = Xml_Acccode_Details + "<coastCode>" + item.CostCode.Trim() + "</coastCode>";
                        //  Xml_Acccode_Details = Xml_Acccode_Details + "<coastName>" + coastName.Trim() + "</coastName>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                }

                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                if (ACVVM.WAD.TransactionMode == "PAYMENT")
                {
                    Opertitile = "MANUAL DEBIT VOUCHER";
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "CASH")
                    {
                        Transtype = "CASH PAYMENT";
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BANK")
                    {
                        Transtype = "BANK PAYMENT";
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BOTH")
                    {
                        Transtype = "BOTH PAYMENT";
                    }
                }
                else
                {
                    Opertitile = "MANUAL CREDIT VOUCHER";
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "CASH")
                    {
                        Transtype = "CASH RECEIPT";
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BANK")
                    {
                        Transtype = "BANK RECEIPT";
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BOTH")
                    {
                        Transtype = "BOTH RECEIPT";
                    }
                }

                string PBOV_code = "", PBOV_Name = "";
                if (ACVVM.WAD.CustCode == "" || ACVVM.WAD.CustCode == null)
                {
                    PBOV_code = "8888";
                    PBOV_Name = ACVVM.WAD.CustomerName;
                }
                else
                {
                    PBOV_code = ACVVM.WAD.CustCode;
                    string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + PBOV_code + "','1','" + ACVVM.WAD.CustTyp + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    PBOV_Name = Dt.Rows[0][1].ToString();
                }



                if (ACVVM.WAD.TransactionMode == "RECEIPT")
                {
                    if (ACVVM.WAD.PaymentMode != "Cash")
                    {
                        BankCode = "ACA0002";
                        if (ACVVM.WAD.Deposited == true)
                        {
                            DepoFlag = "Y";
                            BankCode = ACVVM.WAD.DepositedInBank;
                        }
                        adjustamt = Convert.ToString(ACVVM.WAD.ChequeAmount);
                        if (ACVVM.WAD.IsonAccount == true)
                        {
                            ACVVM.WAD.IsonAccount = Convert.ToBoolean("Y");
                            adjustamt = "0";
                        }
                    }
                }
                else
                {
                    if (ACVVM.WAD.PaymentMode != "Cash")
                    {
                        DepoFlag = "Y";
                        BankCode = ACVVM.WAD.DepositedInBank;
                    }
                }

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear.Substring(0, 4).ToString() + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype.Trim() + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BaseLocationCode.Trim() + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.Trim() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString().Trim() + "</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ACVVM.WAD.ManualNo.Trim() + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ACVVM.WAD.BusinessDivision.Trim() + "</BusinessType>";
                Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + ACVVM.WAD.Preparedatlocation.Trim() + "</PreparedAtLoc>";
                Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + BaseLocationCode.Trim() + "</Acclocation>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ACVVM.WAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ACVVM.WAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + ACVVM.WAD.Narration.Trim() + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<Panno>" + ACVVM.WAD.PANNumber + "</Panno>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ACVVM.WAD.ServiceTaxRegNo + "</Svrtaxno>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ACVVM.WAD.ChequeNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqamt>" + Convert.ToDouble(ACVVM.WAD.ChequeAmount) + "</chqamt>";
                Xml_Other_Details = Xml_Other_Details + "<chqdt>" + ACVVM.WAD.Chequedate.ToString("dd MMM yyyy") + "</chqdt>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + ACVVM.WAD.CustTyp + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + ACVVM.WAD.IsServicetaxapply + "</Svrtax_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + ACVVM.WAD.IsTDStaxapply + "</Tds_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + ACVVM.WAD.TDSSection + "</Tds_Acccode>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + ACVVM.WAD.TDSRate + "</Tds_rate>";
                Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ACVVM.WAD.ReceivedFromBank + "</recbanknm>";
                Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + ACVVM.WAD.IsonAccount + "</Onaccount_YN>";
                Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag.ToString() + "</Deposited>";
                Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ACVVM.WAD.PaymentMode + "</Paymode>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ACVVM.WAD.ServiceTaxRate + "</SvrTaxRate>";
                Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ACVVM.WAD.EducationCess + "</EduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ACVVM.WAD.HEducationCess + "</HEduCessRate>";
                //Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + ddlCompanyList.SelectedValue.ToString() + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "<costcodelist>" + costcodelist.ToString() + "</costcodelist>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DT = OS.InsertmanualDebitCreditVoucher(Xml_Acccode_Details, Xml_Other_Details);

                VoucherNo = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "DebitCreditVoucher", "View", "AddEdit", ex.Message);
                return View("Error");
            }*/

            #endregion

            return RedirectToAction("VoucherDone", new { VoucherNo = Voucherno });
        }

        public ActionResult VoucherDone(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }

        private string GetFileName(string fileName, string pref)
        {
            string strRet = fileName;


            string pat = @"(?:.*\\)(?'Name'.+)\.(?'EXT'.+)|(?'Name'.+)\.(?'EXT'.+)";
            Regex r = new Regex(pat);

            Match m = r.Match(fileName);
            string file_ext = m.Groups["EXT"].Captures[0].ToString();
            string filename = m.Groups["Name"].Captures[0].ToString();


            int count = 0;
            string strPath = Server.MapPath("~") + @"\GUI\Scan_Document\Vendor_Invoice_Scan";
            //string strPath = Server.MapPath("..") + @"\Vendor_Invoice_Scan";
            DirectoryInfo xMainDir = new DirectoryInfo(strPath);
            FileInfo[] filein = xMainDir.GetFiles();
            count = filein.Length;
            string docno = count.ToString().PadLeft(5, '0').ToString();

            strRet = pref + "_" + docno + "." + file_ext;

            return strRet;
        }

        private bool CheckDocumentExistance(string FileName)
        {
            try
            {
                return true;//File.Exists(Server.MapPath("~") + @"\GUI\Scan_Document\Vendor_Invoice_Scan" + FileName);
            }
            catch (Exception)
            {
                return true;
            }
        }

        private bool CheckFMDocumentsDirectory()
        {
            try
            {
                string strDirectoryName = Server.MapPath("~") + @"\GUI\Scan_Document\Vendor_Invoice_Scan";
                if (Directory.Exists(strDirectoryName))
                {
                    return true;
                }
                else
                {
                    Directory.CreateDirectory(strDirectoryName);
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ActionResult AddNewAccountInfo(int SrNo)
        {
            Webx_Acc WAD = new Webx_Acc();
            try
            {
                WAD.Srno = SrNo;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "AddNewAccountInfo", "PartialView", "Listing", ex.Message);
                return View("Error");
            }
            return PartialView("_DebitCreditVoucherPartial", WAD);
        }

        #endregion

        #region Contra voucher

        public ActionResult ContraVoucher()
        {
            webx_Acccode_Details WAD = new webx_Acccode_Details();
            WAD.BackDateAccess = MS.GetVoucherBackDateAccess(BaseUserName, "Contra_Voucher");
            WAD.Brcd = BaseLocationCode;
            WAD.Entryby = BaseUserName;
            return View(WAD);
        }

        [HttpPost]
        public ActionResult ContraVoucher(webx_Acccode_Details Details)
        {
            string Voucherno = "", chqrefyn = "", chque_entry = "", ChqueNo = "", ChqueDate = "";
            try
            {
                if (Details.chkRefno)
                {
                    chqrefyn = "Y";
                    chque_entry = "N";
                }
                else
                {
                    chqrefyn = "N";
                    chque_entry = "Y";
                }

                string Account = Details.Acccode;
                string Account1 = Details.Acccode1;
                string PayMode = Details.PayMode;
                string PayMode1 = Details.PayMode1;
                string Credit = Convert.ToString(Details.Credit);
                string Credit1 = Convert.ToString(Details.Credit1);
                string chqueno = Convert.ToString(Details.ChequeNo);
                string chqueno1 = Convert.ToString(Details.ChequeNo1);
                string txtchqdt = Convert.ToString(Details.Chequedate.ToString("dd MMM yyyy"));
                string txtchqdt1 = Convert.ToString(Details.Chequedate1.ToString("dd MMM yyyy"));

                #region Transaction error

                if (Convert.ToDouble(Credit) > 0 && PayMode.ToUpper() == "BANK")
                {
                    DataTable DT1 = OS.Duplicate_ChqNO(chqueno, txtchqdt);
                    string Cnt = DT1.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }
                if (Convert.ToDouble(Credit1) > 0 && PayMode1.ToUpper() == "BANK")
                {
                    DataTable DT1 = OS.Duplicate_ChqNO(chqueno1, txtchqdt1);
                    string Cnt = DT1.Rows[0][0].ToString();
                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Duplicate Cheque Entered";
                        return View("Error");
                    }
                }

                string FIN_Start = "", Financial_Year = "", fin_year = "", Curr_Year = "", Finyear = "";
                Financial_Year = BaseFinYear.ToString().Substring(2, 2);
                fin_year = BaseFinYear.ToString();
                double fin_year_next = Convert.ToDouble(Financial_Year) + 1;
                fin_year = Financial_Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
                Curr_Year = DateTime.Now.ToString("yyyy");
                Finyear = BaseFinYear.ToString();
                if (Finyear == Curr_Year)
                    FIN_Start = "01 Apr " + Curr_Year.Substring(2, 2);
                else
                    FIN_Start = "01 Apr " + Financial_Year;

                if (Convert.ToDouble(Credit) > 0)
                {
                    if (PayMode.ToUpper() == "CASH")
                    {
                        DataTable DT123 = OS.DR_VR_Trn_Halt(BaseLocationCode, Details.Transdate.ToString("dd MMM yyyy"), FIN_Start, fin_year, Convert.ToString(Credit));
                        string Cnt = DT123.Rows[0][0].ToString();
                        if (Cnt == "F")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                            return View("Error");
                        }
                        if (Cnt != "T")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                            return View("Error");
                        }
                        chque_entry = "N";
                    }
                }
                if (Convert.ToDouble(Credit1) > 0)
                {
                    if (PayMode1.ToUpper() == "CASH")
                    {
                        DataTable DT123 = OS.DR_VR_Trn_Halt(BaseLocationCode, Details.Transdate.ToString("dd MMM yyyy"), FIN_Start, fin_year, Convert.ToString(Credit1));
                        string Cnt = DT123.Rows[0][0].ToString();
                        if (Cnt == "F")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                            return View("Error");
                        }
                        if (Cnt != "T")
                        {
                            ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                            return View("Error");
                        }
                        chque_entry = "N";
                    }
                }

                if (PayMode1.ToUpper() == "BANK")
                {
                    if (chqueno1 == "" || chqueno1 == null)
                    {
                        ViewBag.StrError = "Please Enter Cheque Number.";
                        return View("Error");
                    }
                    if (txtchqdt1 == "" || txtchqdt1 == null || txtchqdt1 == "01 Jan 0001" || txtchqdt1 == "01 Jan 1990")
                    {
                        ViewBag.StrError = "Please Enter Cheque Date.";
                        return View("Error");
                    }
                }

                if (PayMode.ToUpper() == "CASH")
                {
                    chqueno = "";
                    txtchqdt = "";
                }
                if (PayMode1.ToUpper() == "CASH")
                {
                    chqueno1 = "";
                    txtchqdt1 = "";
                }

                if (chqueno == "" && chqueno1 == "")
                {
                    ViewBag.StrError = "Please Enter Cheque Number.";
                    return View("Error");
                }
                if (txtchqdt == "" && txtchqdt1 == "")
                {
                    ViewBag.StrError = "Please Enter Cheque Date.";
                    return View("Error");
                }

                if (chqueno != "" && txtchqdt != "01 Jan 0001" && txtchqdt != "01 Jan 1990")
                {
                    ChqueNo = chqueno;
                    ChqueDate = txtchqdt;
                }
                if (chqueno1 != "" && txtchqdt1 != "01 Jan 0001" && txtchqdt1 != "01 Jan 1990")
                {
                    ChqueNo = chqueno1;
                    ChqueDate = txtchqdt1;
                }

                if (ChqueNo == null || ChqueNo == "")
                {
                    ViewBag.StrError = "Please Enter Cheque Number.";
                    return View("Error");
                }
                if (ChqueDate == null || ChqueDate == "" || Convert.ToDateTime(ChqueDate) >= System.DateTime.Now)
                {
                    ViewBag.StrError = "Please Enter Cheque Date.";
                    return View("Error");
                }

                int Sum = Details.Debit + Details.Credit + Convert.ToInt32(Details.Debit1) + Details.Credit1;
                if (Sum == 0)
                {
                    ViewBag.StrError = "Please Enter Value. At Zero Amount Contra Voucher Not Generate.";
                    return View("Error");
                }

                #endregion


                string Xml_Acccode_Details = "<root>";

                Xml_Acccode_Details = Xml_Acccode_Details + "<Acctdetails>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Details.Acccode.Trim() + "</Acccode>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc></Accdesc>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Details.Debit + "</Debit>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Details.Credit + "</Credit>";
                Xml_Acccode_Details = Xml_Acccode_Details + "</Acctdetails>";

                Xml_Acccode_Details = Xml_Acccode_Details + "<Acctdetails>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Details.Acccode1.Trim() + "</Acccode>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc></Accdesc>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Details.Debit1 + "</Debit>";
                Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Details.Credit1 + "</Credit>";
                Xml_Acccode_Details = Xml_Acccode_Details + "</Acctdetails>";

                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                string Xml_Other_Details = "<root><Other>";

                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Details.Transdate.ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + "Contra" + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BaseLocationCode + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.ToString().Trim() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>MANUAL CONTRA VOUCHER</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + Convert.ToString(Details.ManualNo) + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + Convert.ToString(Details.ReferenceNo) + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + Details.Narration.Trim() + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ChqueNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqdt>" + ChqueDate + "</chqdt>";
                Xml_Other_Details = Xml_Other_Details + "<ChqueEntryYN>" + chque_entry.Trim() + "</ChqueEntryYN>";
                Xml_Other_Details = Xml_Other_Details + "<ChqueRefYN>" + chqrefyn.Trim() + "</ChqueRefYN>";
                Xml_Other_Details = Xml_Other_Details + "<ChqueEntryYN></ChqueEntryYN>";
                Xml_Other_Details = Xml_Other_Details + "<ChqueRefYN></ChqueRefYN>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code></pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name></pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ></pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<EntryType>Insert</EntryType>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode.ToString().Trim() + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DataTable DT = OS.GetContraVoucherSubmit(Xml_Acccode_Details.ToString(), Xml_Other_Details.ToString());
                Voucherno = DT.Rows[0]["Voucherno"].ToString();

            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                return View("Error");
            }
            return RedirectToAction("ContraVoucherDone", new { Voucherno = Voucherno });

        }

        public ActionResult ContraVoucherDone(string Voucherno)
        {
            ViewBag.Voucherno = Voucherno;
            ViewBag.BaseFinYear = BaseFinYear;
            return View();
        }

        public JsonResult GetLedgerAccountFromPaymode(string Paymode)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                DataTable Dt_City = OS.GetLedgerAccountFromPaymode(Paymode, BaseLocationCode);
                List<GetLedgerAccountFromPaymode> LedgerList = DataRowToObject.CreateListFromTable<GetLedgerAccountFromPaymode>(Dt_City);


                return Json(LedgerList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetLedgerAccountFromPaymode", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        #endregion

        #region Journal Voucher

        public ActionResult JournalVoucher(int ID)
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName.ToUpper() == "ADMIN")
            //{
                DebitCreditVoucherViewModel viewmodel = new DebitCreditVoucherViewModel();
                viewmodel.BackDateAccess = MS.GetVoucherBackDateAccess(BaseUserName, "Journal_Voucher");
                List<Webx_Acc> ListWAD = new List<Webx_Acc>();
                viewmodel.WA = new Webx_Acc();
                viewmodel.WAD = new Webx_Account_Details();

                try
                {
                    if (ID == 0)
                    {
                        viewmodel.WAD.VoucherDate = System.DateTime.Today;
                        viewmodel.WAD.Preparedatlocation = BaseLocationCode;
                        viewmodel.WAD.AccountingLocation = BaseLocationCode;
                        viewmodel.WAD.PrepardBy = BaseUserName.ToUpper();
                        viewmodel.WAD.Chequedate = System.DateTime.Today;

                        viewmodel.WA.Srno = 1;
                        ListWAD.Add(viewmodel.WA);

                        viewmodel.WADList = ListWAD;

                        Webx_Account_Details OBjWAD = new Webx_Account_Details();
                        OBjWAD.SRNo = 1;
                        List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                        ListWAD1.Add(OBjWAD);
                        viewmodel.WADListing = ListWAD1;
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Message:-" + ex.Message;
                    Error_Logs("Journal Voucher", "Voucher", "View", "AddEdit", ex.Message + "//" + ViewBag.StrError);
                    return View("Error");
                }

                return View(viewmodel);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}
        }

        public ActionResult AddNewAccountInfoJournalVoucher(int SrNo)
        {
            Webx_Acc WAD = new Webx_Acc();
            try
            {
                WAD.Srno = SrNo;
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "AddNewAccountInfoJournalVoucher", "PartialView", "Listing", ex.Message);
            }
            return PartialView("_JournalVoucherPartial", WAD);
        }

        [HttpPost]
        public ActionResult JournalVoucher(DebitCreditVoucherViewModel ACVVM, List<Webx_Acc> JournalVoucher)
        {
            string VoucherNo = "";
            try
            {
                DataTable DT = new DataTable();
                Webx_Account_Details WAD = new Webx_Account_Details();

                string Accdesc = "", costcodelist = "", coastName = "", Transtype = "Journal", OppaccountCode = "", PBOV_typ = "";
                string Xml_Acccode_Details = "<root>";

                foreach (var item in JournalVoucher)
                {

                    string CompanyCode = item.AccountCode;
                    string DocNo = item.DocNo;
                    string DocType = item.DocType;

                    string Narration = "";
                    try
                    {
                        Narration = item.NarrationAccountinfo.ReplaceSpecialCharacters();
                    }
                    catch
                    {
                        Narration = "";
                    }

                    if (item.Particular == true)
                    {
                        OppaccountCode = item.AccountCode;
                    }
                    string SQR = "exec USP_GetAccountCodeList '" + item.AccountCode + "','1','" + BaseLocationCode + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    string Description = Dt.Rows[0][1].ToString();

                    if (CompanyCode != "")
                    {
                        Accdesc = Description.Replace("<", "&lt;");
                        Accdesc = Description.Replace(">", "&gt;");
                        Accdesc = Description.Replace("\"", "&quot;");
                        Accdesc = Description.Replace("'", "&apos;");

                        if (CompanyCode != "")
                        {
                            if (costcodelist == "")
                            {
                                costcodelist = CompanyCode;
                            }
                            else
                            {
                                costcodelist = costcodelist + "," + CompanyCode;
                            }
                        }
                        if (item.AccountCode.Substring(0, 3).ToUpper() == "EXP")
                        {
                            if (item.CostType != "" || item.CostType != null)
                            {
                                string SQRY = "exec USP_GetAccountCodeIsValidOrNot_New_Portal '" + item.CostCode + "','" + item.CostType + "' ";
                                //DataTable Dt_Name = GF.GetDataTableFromSP(SQRY);
                                //coastName = Dt_Name.Rows[0][1].ToString();
                            }
                        }
                        else
                        {
                            item.CostCode = "";
                            item.CostType = " ";
                        }


                        if (item.Debit == 0 && item.Credit == 0)
                        {
                            ViewBag.StrError = "Message:-Plz Enter Debit/Credit Amount..!!";
                            Error_Logs(ControllerName, "JouranalVoucher", "View", "AddEdit", "Plz Enter Debit/Credit Amount..!!");
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + CompanyCode.Trim() + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc.Replace("&", "And") + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + item.Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + item.Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration.ReplaceSpecialCharacters() + "</Narration>";
                        //Xml_Acccode_Details = Xml_Acccode_Details + "<coasttype>" + item.CostType.Substring(0, 1) + "</coasttype>";
                        //Xml_Acccode_Details = Xml_Acccode_Details + "<coastCode>" + item.CostCode.Trim() + "</coastCode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                }

                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                if (OppaccountCode == "")
                {
                    ViewBag.StrError = "Opponant Acount Code Not Found.";
                    Error_Logs(ControllerName, "JouranalVoucher", "View", "AddEdit", ViewBag.StrError);
                    return View("Error");
                }

                string PBOV_code = "", PBOV_Name = "";
                if (ACVVM.WAD.CustCode == "" || ACVVM.WAD.CustCode == null || ACVVM.WAD.CustCode == "8888")
                {
                    PBOV_code = "8888";
                    PBOV_Name = ACVVM.WAD.CustomerName;
                    PBOV_typ = "O";
                }
                else
                {
                    PBOV_code = ACVVM.WAD.CustCode;
                    string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + PBOV_code + "','1','" + ACVVM.WAD.CustTyp + "'";

                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    PBOV_Name = Dt.Rows[0][1].ToString();
                }

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear.Substring(0, 4).ToString() + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + ACVVM.WAD.AccountingLocation + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>MANUAL JOURNAL VOUCHER</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ACVVM.WAD.ManualNo + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ACVVM.WAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ACVVM.WAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + ACVVM.WAD.Narration.ReplaceSpecialCharacters() + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name.ReplaceSpecialCharacters() + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + ACVVM.WAD.CustTyp + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<OppAccount>" + OppaccountCode + "</OppAccount>";
                Xml_Other_Details = Xml_Other_Details + "<Jv_Type>Normal JV</Jv_Type>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DT = OS.InsertJournalVoucher(Xml_Acccode_Details, Xml_Other_Details);

                VoucherNo = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "JouranalVoucher", "View", "AddEdit", ex.Message);
                return View("Error");
            }
            return RedirectToAction("JournalVoucherDone", new { VoucherNo = VoucherNo });
        }

        public ActionResult JournalVoucherDone(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }

        #endregion

        #region MJV

        public ActionResult MJV(int ID)
        {
            MJVViewModel viewmodel = new MJVViewModel();
            List<Webx_Acc> ListWAD = new List<Webx_Acc>();
            viewmodel.WA = new Webx_Acc();
            viewmodel.WAD = new Webx_Account_Details();

            try
            {
                if (ID == 0)
                {
                    viewmodel.WAD.VoucherDate = System.DateTime.Today;
                    viewmodel.WAD.Preparedatlocation = BaseLocationCode;
                    viewmodel.WAD.AccountingLocation = BaseLocationCode;
                    viewmodel.WAD.PrepardBy = BaseUserName.ToUpper();
                    viewmodel.WAD.Chequedate = System.DateTime.Today;

                    viewmodel.WA.Srno = 1;
                    ListWAD.Add(viewmodel.WA);

                    viewmodel.WADList = ListWAD;

                    Webx_Account_Details OBjWAD = new Webx_Account_Details();
                    OBjWAD.SRNo = 1;
                    List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                    ListWAD1.Add(OBjWAD);
                    viewmodel.WADListing = ListWAD1;
                }
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs("MJV", "Voucher", "View", "AddEdit", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }

            return View(viewmodel);
        }

        public ActionResult MJVSubmit(MJVViewModel ViewModel, List<Webx_Acc> MJV)
        {
            string VoucherNo = "";
            try
            {
                DataTable DT = new DataTable();
                Webx_Account_Details WAD = new Webx_Account_Details();

                string Accdesc = "", costcodelist = "", coastName = "", Transtype = "", OppaccountCode = "", PartyCodeList = "";
                string Xml_Acccode_Details = "<root>";
                foreach (var item in MJV)
                {
                    string CompanyCode = item.AccountCode;
                    string PartyCode = item.PartyName;
                    string DocNo = item.DocNo;
                    string DocType = item.DocType;

                    string Narration = "";
                    try
                    {
                        Narration = item.NarrationAccountinfo.Replace("'", "''");
                    }
                    catch
                    {
                        Narration = "";
                    }

                    if (item.Particular == true)
                    {
                        OppaccountCode = item.AccountCode;
                    }

                    string SQR = "exec USP_GetAccountCodeList '" + item.AccountCode + "','1','" + BaseLocationCode + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    string Description = Dt.Rows[0][1].ToString();

                    if (CompanyCode != "")
                    {
                        Accdesc = Description.Replace("<", "&lt;");
                        Accdesc = Description.Replace(">", "&gt;");
                        Accdesc = Description.Replace("\"", "&quot;");
                        Accdesc = Description.Replace("'", "&apos;");

                        if (CompanyCode != "")
                        {
                            if (costcodelist == "")
                            {
                                costcodelist = CompanyCode;
                            }
                            else
                            {
                                costcodelist = costcodelist + "," + CompanyCode;
                            }
                        }
                        if (PartyCode != "")
                        {
                            if (PartyCodeList == "")
                            {
                                PartyCodeList = PartyCode;
                            }
                            else
                            {
                                PartyCodeList = PartyCodeList + "," + PartyCode;
                            }
                        }


                        if (item.AccountCode.Substring(0, 3).ToUpper() == "EXP")
                        {
                            if (item.CostType != "" || item.CostType != null)
                            {
                                string SQRY = "exec USP_GetAccountCodeIsValidOrNot_New_Portal '" + item.CostCode + "','" + item.CostType + "' ";
                                DataTable Dt_Name = GF.GetDataTableFromSP(SQRY);
                                //coastName = Dt_Name.Rows[0][1].ToString();
                            }
                        }
                        else
                        {
                            item.CostCode = "";
                            item.CostType = " ";
                        }
                        string PBOV_code = "", PBOV_Name = "";
                        if (item.PaidTo == "" || item.PaidTo == null)
                        {
                            PBOV_code = "8888";
                            PBOV_Name = item.PaidTo;
                        }
                        else
                        {
                            PBOV_code = item.PartyName;
                            string SQR1 = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + PBOV_code + "','1','" + item.PaidTo + "'";

                            DataTable Dt1 = GF.GetDataTableFromSP(SQR1);
                            PBOV_Name = Dt1.Rows[0][1].ToString();
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + CompanyCode + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc.Replace("&", "And") + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + item.Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + item.Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<PartyCode>" + PBOV_code + "</PartyCode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<PartyName>" + PBOV_Name + "</PartyName>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<coasttype>" + item.CostType + "</coasttype>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<coastCode>" + item.CostCode + "</coastCode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                }

                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

                if (OppaccountCode == "")
                {
                    ViewBag.StrError = "Opponant Acount Code Not Found.";
                    Error_Logs(ControllerName, "JouranalVoucher", "View", "AddEdit", ViewBag.StrError);
                    return View("Error");
                }

                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear.Substring(0, 4).ToString() + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ViewModel.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype.Trim() + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BaseLocationCode.Trim() + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.Trim() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<Opertitle>MANUAL JOURNAL VOUCHER</Opertitle>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ViewModel.WAD.ManualNo.Trim() + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ViewModel.WAD.PreparedFor.Trim() + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ViewModel.WAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + ViewModel.WAD.Narration.Trim() + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code></pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name></pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + ViewModel.WAD.CustTyp + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<OppAccount>" + OppaccountCode.Trim() + "</OppAccount>";
                Xml_Other_Details = Xml_Other_Details + "<Jv_Type>Multiple JV</Jv_Type>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DT = OS.InsertMJV(Xml_Acccode_Details, Xml_Other_Details);
                VoucherNo = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "MJV", "View", "AddEdit", ex.Message);
                return View("Error");
            }
            return RedirectToAction("MJVDone", new { VoucherNo = VoucherNo });
        }

        public ActionResult MJVDone(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }

        public ActionResult AddNewAccountInfoMJV(int SrNo)
        {
            Webx_Acc WAD = new Webx_Acc();
            try
            {
                WAD.Srno = SrNo;
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "AddNewAccountInfoMJV", "PartialView", "Listing", ex.Message);
            }
            return PartialView("_MJVPartial", WAD);
        }

        #endregion

        #region Special Cost Voucher
        public ActionResult SpecialCostVoucherCriteria()
        {
            //if (CurrFinYear == BaseFinYear || BaseUserName.ToUpper() == "ADMIN")
            //{
            //    Webx_Account_Details WAD = new Webx_Account_Details();
            //    return View(WAD);
            //}
            //else
            //{
            //    return RedirectToAction("URLRedirect", "Home");
            //}

            Webx_Account_Details WAD = new Webx_Account_Details();
            return View(WAD);
        }
        [HttpPost]
        public ActionResult SCVoucher(Webx_Account_Details objWAD)
        {
            DebitCreditVoucherViewModel viewmodel = new DebitCreditVoucherViewModel();
            List<Webx_Acc> ListWAD = new List<Webx_Acc>();

            viewmodel.WA = new Webx_Acc();
            viewmodel.WAD = new Webx_Account_Details();

            try
            {
                viewmodel.WAD.VoucherDate = System.DateTime.Today;
                viewmodel.WAD.Preparedatlocation = BaseLocationCode;
                viewmodel.WAD.AccountingLocation = BaseLocationCode;
                viewmodel.WAD.PrepardBy = BaseUserName.ToUpper();
                viewmodel.WAD.Chequedate = System.DateTime.Today;
                viewmodel.WAD.ServiceTaxRate = OS.GetServicetaxrate();
                viewmodel.WAD.GSTType = objWAD.GSTType;
                viewmodel.WAD.TypeOfGst = objWAD.TypeOfGst;
                viewmodel.WAD.GSTPercentage = objWAD.GSTPercentage;
                viewmodel.WA.Srno = 1;
                ListWAD.Add(viewmodel.WA);

                viewmodel.WADList = ListWAD;

                Webx_Account_Details OBjWAD = new Webx_Account_Details();
                OBjWAD.SRNo = 1;
                OBjWAD.Vouchertype = "DEBITCREDIT";
                List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                ListWAD1.Add(OBjWAD);
                viewmodel.WADListing = ListWAD1;

                DataTable DT_Tax = OS.GetServiceTaxRate();
                decimal HdnServiceTaxRate = Convert.ToDecimal(DT_Tax.Rows[0]["servicetaxrate"].ToString());
                decimal HdnEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["educessrate"].ToString());
                decimal HdnHEduCessRate = Convert.ToDecimal(DT_Tax.Rows[0]["heducessrate"].ToString());

                viewmodel.HdnServiceTaxRate = HdnServiceTaxRate;
                viewmodel.HdnEduCessRate = HdnEduCessRate;
                viewmodel.HdnHEduCessRate = HdnHEduCessRate;

                viewmodel.WAD.ServiceTax = HdnServiceTaxRate;
                viewmodel.WAD.EducationCess = HdnEduCessRate;
                viewmodel.WAD.HEducationCess = HdnHEduCessRate;

                ViewBag.FinYear = BaseFinYear;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs("SpecialCostVoucher", "DebitCreditVoucher", "View", "AddEdit", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View("SpecialCostVoucher", viewmodel);
        }

        public ActionResult AddNewSpecialCostAccuntInfo(int SrNo, string DocType)
        {
            Webx_Acc WAD = new Webx_Acc();
            try
            {
                WAD.Srno = SrNo;
                WAD.DocType = DocType;
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "AddNewSpecialCostAccuntInfo", "PartialView", "Listing", ex.Message);
            }
            return PartialView("_SpecialCostVoucherPartial", WAD);
        }

        [HttpPost]
        public ActionResult SpecialCostVoucher(DebitCreditVoucherViewModel ACVVM, List<Webx_Acc> SCVoucher, List<Webx_Account_Details> PAYMENTMODE, List<CygnusChargesHeader> DynamicList, StaxTDSViewModel objStaxTDSViewModel)
        {
            #region Test1
            /*
            string Year = BaseFinYear;
            string Financial_Year = BaseYearVal.Substring(0, 2);
            string EntryBy = BaseUserName, Transtype = "", OppaccountDesc = "", OppaccountCode = "", PBOV_TYP = "";
            string txtChqAmt = "", txtChqNo = "";

            string VoucherDate = GF.FormateDate(Convert.ToDateTime(ACVVM.WAD.VoucherDate));
            string sql_Acccode = "";

            string ChqDate = "", Chqno = "", OppaccountDesc_mode = "", OppaccountCode_mode = "";

            if (ACVVM.WAD.CustTyp == "P")
                PBOV_TYP = "P";
            else if (ACVVM.WAD.CustTyp == "V")
                PBOV_TYP = "V";
            else if (ACVVM.WAD.CustTyp == "E")
                PBOV_TYP = "E";
            else if (ACVVM.WAD.CustTyp == "D")
                PBOV_TYP = "D";
            else if (ACVVM.WAD.CustTyp == "L")
                PBOV_TYP = "L";
            else if (ACVVM.WAD.CustTyp == "O")
                PBOV_TYP = "O";
            Webx_Account_Details ObjWAD = new Webx_Account_Details();
            ObjWAD = PAYMENTMODE.First();
            txtChqNo = ObjWAD.ChequeNo;
            if (ObjWAD.PaymentMode.ToUpper() != "CASH")
            {
                string Cnt = PS.Duplicate_ChqNO(ObjWAD.ChequeNo, GF.FormateDate(ObjWAD.Chequedate));
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
            }

            if (ObjWAD.PaymentMode.ToUpper() != "BANK" && ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                string Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(ObjWAD.CashAmount), Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy"), BaseFinYear, ACVVM.WAD.AccountingLocation);
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                    return View("Error");
                }
                if (Cnt != "T")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                    return View("Error");
                }
            }

            if (ObjWAD.PaymentMode.ToUpper() == "CASH" && ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                ChqDate = null;
                Chqno = null;
                txtChqAmt = "0";
                txtChqNo = "0";
                Transtype = "CASH PAYMENT";
                OppaccountDesc = ObjWAD.CashAccount;
                OppaccountCode = "CAS0002";
            }

            if (ObjWAD.PaymentMode.ToUpper() == "CASH" && ObjWAD.TransactionMode.ToUpper() != "PAYMENT")
            {
                ChqDate = null;
                Chqno = null;
                txtChqAmt = "0";
                txtChqNo = "0";
                Transtype = "CASH RECEIPT";
                OppaccountDesc = ObjWAD.CashAccount;
                OppaccountCode = "CAS0002";
            }

            if (ObjWAD.PaymentMode.ToUpper() == "BANK" && ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                Chqno = txtChqNo;
                ChqDate = GF.FormateDate(ObjWAD.Chequedate);
                Transtype = "BANK PAYMENT";
                OppaccountDesc = ObjWAD.DepositedInBank;
                OppaccountCode = ObjWAD.DepositedInBank;
            }

            if (ObjWAD.PaymentMode.ToUpper() == "BANK" && ObjWAD.TransactionMode.ToUpper() != "PAYMENT")
            {
                Chqno = txtChqNo;
                ChqDate = GF.FormateDate(ObjWAD.Chequedate);
                Transtype = "BANK RECEIPT";
                OppaccountDesc = ObjWAD.DepositedInBank;
                OppaccountCode = ObjWAD.DepositedInBank;
            }

            if (ObjWAD.PaymentMode.ToUpper() == "BOTH" && ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                Transtype = "BOTH PAYMENT";
                Chqno = txtChqNo;
                ChqDate = GF.FormateDate(ObjWAD.Chequedate);
                OppaccountDesc = ObjWAD.DepositedInBank;
                OppaccountCode = ObjWAD.DepositedInBank;
            }

            if (ObjWAD.PaymentMode.ToUpper() == "BOTH" && ObjWAD.TransactionMode.ToUpper() != "PAYMENT")
            {
                Transtype = "BOTH RECEIPT";
                Chqno = txtChqNo;
                ChqDate = GF.FormateDate(ObjWAD.Chequedate);
                OppaccountDesc = ObjWAD.DepositedInBank;
                OppaccountCode = ObjWAD.DepositedInBank;
            }

            sql_Acccode = "select accdesc from webx_acctinfo WITH(NOLOCK) where Acccode='" + OppaccountCode + "'";

            DataTable DT_Acccode = GF.GetDataTableFromSP(sql_Acccode);
            try
            {
                OppaccountDesc = DT_Acccode.Rows[0]["accdesc"].ToString();
            }
            catch (Exception)
            {
                OppaccountDesc = "";
            }

            SqlDataReader dr;
            string HdnServiceTax = "", txtServiceTax = "", HdnEduCess = "", txtEduCess = "", HdnHEduCess = "", txtHEduCess = "", HdnTDSRate = "", txtTDSRate = "";

            if (HdnServiceTax == "")
            {
                txtServiceTax = "0.00";
                HdnServiceTax = "0.00";
            }
            if (HdnEduCess == "")
            {
                txtEduCess = "0.00";
                HdnEduCess = "0.00";
            }
            if (HdnHEduCess == "")
            {
                txtHEduCess = "0.00";
                HdnHEduCess = "0.00";
            }
            if (HdnTDSRate == "")
            {
                txtTDSRate = "0";
                HdnTDSRate = "0";
            }

            string PBOV_code = "", PBOV_Name = "";

            if (ACVVM.WAD.CustCode == "" || ACVVM.WAD.CustCode == "8888")
            {
                PBOV_code = "8888";
                PBOV_Name = ACVVM.WAD.CustomerName;
            }
            else
            {
                string CUSTNM = "";

                PBOV_code = ACVVM.WAD.CustCode;
                try
                {
                    if (PBOV_TYP == "P")
                    {
                        webx_CUSTHDR WCT = OS.GetCustmer(ACVVM.WAD.CustCode).First();
                        CUSTNM = WCT.CUSTNM;
                    }
                    else if (PBOV_TYP == "V")
                    {
                        webx_VENDOR_HDR WCT = MS.GetVendorObject().Where(c => c.VENDORCODE == ACVVM.WAD.CustCode).FirstOrDefault();
                        CUSTNM = WCT.VENDORNAME;
                    }
                    else if (PBOV_TYP == "E")
                    {
                        WebX_Master_Users WCT = MS.GetUserDetails().Where(c => c.UserId == ACVVM.WAD.CustCode).FirstOrDefault();
                        CUSTNM = WCT.Name;
                    }
                    else if (PBOV_TYP == "D")
                    {
                        WEBX_FLEET_DRIVERMST WCT = FLTS.GetDriverMstDetails().Where(c => c.Driver_Id == Convert.ToDecimal(ACVVM.WAD.CustCode)).FirstOrDefault();
                        CUSTNM = WCT.Driver_Name;
                    }
                    else // (PBOV_TYP == "L")
                    {
                        CUSTNM = ACVVM.WAD.CustCode;
                    }
                }
                catch (Exception)
                {
                    CUSTNM = ACVVM.WAD.CustCode;
                }
                PBOV_Name = CUSTNM;
            }

            string svrtaxno = "0";
            string svrtaxrate = "0";
            string srvtaxamt = "0";
            string svrtaxaccountcode = "";

            string tdsrate = "0";
            string tdsamt = "0";
            string tdsacccode = "";

            string TotalDebitAmt = "0.00";
            string TotalCreditAmt = "0.00";



            if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                TotalDebitAmt = Convert.ToString(ObjWAD.NETAMOUNT);
                if (ACVVM.WAD.IsServicetaxapply)
                {
                    svrtaxno = ACVVM.WAD.ServiceTaxRegNo;
                    svrtaxrate = Convert.ToString(ACVVM.HdnServiceTaxRate);
                    srvtaxamt = Convert.ToString(ACVVM.WAD.ServiceTax); //txtServiceTax;
                    svrtaxaccountcode = "ALS0009";
                }
                if (ACVVM.WAD.IsTDStaxapply)
                {
                    tdsrate = Convert.ToString(ACVVM.WAD.TDSRate); //txtTDSRate;
                    tdsamt = Convert.ToString(ACVVM.WAD.TDSAmount);
                    tdsacccode = ACVVM.WAD.TDSSection;
                }
            }
            else
            {
                TotalCreditAmt = Convert.ToString(ObjWAD.NETAMOUNT);
                if (ACVVM.WAD.IsServicetaxapply)
                {
                    svrtaxno = ACVVM.WAD.ServiceTaxRegNo;
                    svrtaxrate = Convert.ToString(ACVVM.HdnServiceTaxRate);
                    srvtaxamt = Convert.ToString(ACVVM.WAD.ServiceTax); //txtServiceTax;
                    svrtaxaccountcode = "CLO0023";
                }
                if (ACVVM.WAD.IsTDStaxapply)
                {
                    tdsrate = Convert.ToString(ACVVM.WAD.TDSRate); //txtTDSRate;
                    tdsamt = Convert.ToString(ACVVM.WAD.TDSAmount);
                    tdsacccode = ACVVM.WAD.TDSSection;
                }
            }

            if (objStaxTDSViewModel.IsTDSEnabled)
            {
                tdsrate = Convert.ToString(objStaxTDSViewModel.TDSRate); //txtTDSRate;
                tdsamt = Convert.ToString(objStaxTDSViewModel.TDSAmount);
                tdsacccode = objStaxTDSViewModel.TDSAcccode;
            }

            foreach (var item in SCVoucher)
            {
                string Acccode1 = item.AccountCode;
                if (Acccode1 != "")
                {
                    DataTable dt = OS.FindOutSystemAccountCode(Acccode1, BaseLocationCode);
                    if (dt.Rows.Count == 0)
                    {
                        ViewBag.StrError = "Error : Not Valid Account Code : " + Acccode1 + ". So Please Enter The Valid Account Details";
                        return View("Error");
                    }
                }
            }

            string txtAccLoc = "";
            //RCPL_005179 Change By Rajesh Bhimani
            if (ACVVM.WAD.AccountingLocation == "")
                txtAccLoc = BaseLocationCode;
            else
            {
                if (OS.CheckLocation(ACVVM.WAD.AccountingLocation))
                    txtAccLoc = ACVVM.WAD.AccountingLocation;
                else
                {
                    ViewBag.StrError = "Accounting Location Is Not Valid";
                    return View("Error");
                }
            }


            //XmlDocument xmlDocGSTCha = new XmlDocument();
            //if (DynamicList != null && DynamicList.Count > 0)
            //{
            //    XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
            //    using (MemoryStream xmlStreamCha = new MemoryStream())
            //    {
            //        xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
            //        xmlStreamCha.Position = 0;
            //        xmlDocGSTCha.Load(xmlStreamCha);
            //    }
            //}
            //else
            //{
            //    xmlDocGSTCha.InnerXml = "<root></root>";
            //}


            string StateCode = "";
            CygnusChargesHeader IGST = new CygnusChargesHeader();
            CygnusChargesHeader SGST = new CygnusChargesHeader();
            CygnusChargesHeader CGST = new CygnusChargesHeader();
            CygnusChargesHeader UTGST = new CygnusChargesHeader();

            if (DynamicList != null && DynamicList.Count > 0)
            {
                if (DynamicList.Where(m => m.ChargeCode == "IGST").Count() > 0)
                {
                    IGST = DynamicList.Where(m => m.ChargeCode == "IGST").FirstOrDefault();
                }
                if (DynamicList.Where(m => m.ChargeCode == "SGST").Count() > 0)
                {
                    SGST = DynamicList.Where(m => m.ChargeCode == "SGST").FirstOrDefault();
                }
                if (DynamicList.Where(m => m.ChargeCode == "CGST").Count() > 0)
                {
                    CGST = DynamicList.Where(m => m.ChargeCode == "CGST").FirstOrDefault();
                }
                if (DynamicList.Where(m => m.ChargeCode == "UTGST").Count() > 0)
                {
                    UTGST = DynamicList.Where(m => m.ChargeCode == "UTGST").FirstOrDefault();
                }
            }

            string strSqlState = "exec USP_GetStateFromLocation '" + BaseLocationCode + "'";
            DataTable dtState = GF.GetDataTableFromSP(strSqlState);
            if (dtState != null && dtState.Rows.Count > 0)
            {
                StateCode = dtState.Rows[0]["statePrefix"].ToString();
            }
            string Voucherno = OS.NextVoucherno(txtAccLoc, Financial_Year);
            SqlTransaction trans;
            SqlConnection con;
            SqlCommand cmd;
            con = new SqlConnection(Connstr);
            con.Open();

            string opertitle = "SPECIAL COST VOUCHER";
            trans = con.BeginTransaction();
            try
            {

                string strSql = "exec usp_webx_voucherheader_insertData_NewPortalGST '" + Year + "','" + VoucherDate + "','";
                strSql += Voucherno + "','" + Chqno + "','" + ChqDate + "','" + TotalDebitAmt + "','" + TotalCreditAmt + "','";
                strSql += ACVVM.WAD.Narration + "','" + PBOV_code + "','" + Transtype + "','" + ACVVM.WAD.Preparedatlocation + "','";
                strSql += OppaccountCode + "','" + OppaccountDesc + "','" + BaseUserName + "','" + opertitle + "','";
                strSql += PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PreparedFor + "','" + svrtaxrate + "','";
                strSql += srvtaxamt + "','" + svrtaxaccountcode + "','" + svrtaxno + "','" + tdsrate + "','";
                strSql += tdsamt + "','" + tdsacccode + "','" + ACVVM.WAD.PANNumber + "','";
                strSql += txtChqAmt + "','" + ObjWAD.PaymentMode + "','" + ACVVM.WAD.ManualNo + "','";
                strSql += ObjWAD.BusinessDivision + "','";
                strSql += ACVVM.WAD.AccountingLocation + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.HEducationCess + "','" + BaseCompanyCode + "','";
                strSql += StateCode + "','";
                strSql += ACVVM.WAD.GSTType + "','";
                strSql += CGST.Percentage + "','";
                strSql += CGST.ChargeAmount + "','";
                strSql += SGST.Percentage + "','";
                strSql += SGST.ChargeAmount + "','";
                strSql += UTGST.Percentage + "','";
                strSql += UTGST.ChargeAmount + "','";
                strSql += IGST.Percentage + "','";
                strSql += IGST.ChargeAmount + "'";
                cmd = new SqlCommand(strSql, con, trans);
                cmd.ExecuteNonQuery();

                string sql_Acctrans = "", Acccode = "", Debit = "", Credit = "", Narration = "", DocumentNo = "";

                int i = 0;


                string HdnDocList = "";
                foreach (var ite in SCVoucher)
                {

                    //}

                    ////foreach (GridViewRow gridrow in grvcontrols.Rows)
                    ////{
                    Acccode = ite.AccountCode; ;
                    i = i + 1;

                    if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                    {
                        Debit = ite.Amount;// ((TextBox)gridrow.FindControl("txtAmt")).Text.ToString();
                        Credit = "0.00";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = ite.Amount;//((TextBox)gridrow.FindControl("txtAmt")).Text.ToString();
                    }

                    Narration = ite.NarrationAccountinfo;//((TextBox)gridrow.FindControl("txtNarration")).Text.ToString().Replace("'", "''");
                    //Description = "";// ((TextBox)gridrow.FindControl("txtDescription")).Text.ToString().Replace("'", "''");
                    DocumentNo = ite.DocNo;// ((TextBox)gridrow.FindControl("txtDocno")).Text.ToString();
                    string ddldoctype = ite.DocType; //((DropDownList)gridrow.FindControl("ddldoctype"));

                    if (Acccode != "")
                    {
                        sql_Acccode = "select Acccode,accdesc from webx_acctinfo WITH(NOLOCK) where Company_Acccode='" + Acccode + "'";
                        cmd = new SqlCommand(sql_Acccode, con, trans);

                        dr = cmd.ExecuteReader();
                        string Accdesc = "";
                        while (dr.Read())
                        {
                            Acccode = dr["Acccode"].ToString().Trim();
                            Accdesc = dr["accdesc"].ToString().Trim();
                        }
                        dr.Close();
                        if (i == 1)
                        {
                            OppaccountDesc_mode = Accdesc;
                            OppaccountCode_mode = Acccode;
                        }

                        string sql_docdetail = "";

                        if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                        {
                            sql_docdetail = "exec usp_update_Special_Cost_Voucher '" + ddldoctype + "','" + DocumentNo + "','" + Voucherno + "','" + Debit + "'";
                            cmd = new SqlCommand(sql_docdetail, con, trans);
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            sql_docdetail = "exec usp_update_Special_Cost_Credit_Voucher '" + ddldoctype + "','" + DocumentNo + "','" + Voucherno + "','" + Credit + "'";
                            cmd = new SqlCommand(sql_docdetail, con, trans);
                            cmd.ExecuteNonQuery();
                        }

                        sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','" + ite.DocType + "','" + BaseCompanyCode + "','" + StateCode + "'";
                        cmd = new SqlCommand(sql_Acctrans, con, trans);
                        cmd.ExecuteNonQuery();

                        HdnDocList += DocumentNo + ",";
                    }
                }

                HdnDocList = HdnDocList.Substring(0, HdnDocList.Length - 1);

                sql_Acccode = "select Acccode from webx_acctinfo WITH(NOLOCK) where Acccode='" + OppaccountCode + "'";
                cmd = new SqlCommand(sql_Acccode, con, trans);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    OppaccountCode = dr["Acccode"].ToString().Trim();
                }
                dr.Close();

                Narration = ACVVM.WAD.Narration;// txtNarration.Text;

                if (ObjWAD.PaymentMode.ToUpper() != "CASH")
                {
                    string acccodecb = ObjWAD.DepositedInBank;//  ddrBankaccode.SelectedValue.ToString(); ;
                    string depositdt = VoucherDate;
                    double CHQAMT = Convert.ToDouble(ObjWAD.NETAMOUNT);// Convert.ToDouble(txtNetPay.Text.ToString()),
                    double adjustamt = Convert.ToDouble(ObjWAD.NETAMOUNT);
                    string comments = null;
                    string bacd = null;
                    string banm = null;
                    string empnm = null;
                    string staffcd = null;
                    string staffnm = null;
                    string brnm = OS.GetLocation(BaseLocationCode);
                    string banknm = ObjWAD.ReceivedFromBank; //ddrBankaccode.SelectedItem.Text.ToString();
                    string BANKBRN = "";

                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        string sql = "Insert into webx_chq_det(Chqno, chqdt, chqamt,banknm, ptmsptcd,ptmsptnm,comments,bacd,banm,empcd,empnm,brcd,brnm,staffcd,staffnm,chq_trf,OwnCust,BANKBRN,adjustamt,voucherNo,transdate,acccode,depoloccode,DepoFlag) values('" + Chqno + "','" + ObjWAD.Chequedate.ToString("dd MMM yyyy") + "','" + ObjWAD.ChequeAmount/*txtChqAmt* + "','" + banknm + "','','','" + comments + "','" + bacd + "','" + banm + "','" + BaseUserName + "','" + empnm + "','" + BaseLocationCode + "','" + brnm + "','" + staffcd + "','" + staffnm + "',null,'O','" + BANKBRN + "','" + adjustamt + "','" + Voucherno + "','" + VoucherDate + "','" + acccodecb + "','" + BaseLocationCode + "','Y')";
                        cmd = new SqlCommand(sql, con, trans);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        string sql = "Insert into webx_chq_det(Chqno, chqdt, chqamt,banknm, ptmsptcd,ptmsptnm,comments,bacd,banm,empcd,empnm,brcd,brnm,staffcd,staffnm,chq_trf,OwnCust,BANKBRN,adjustamt,voucherNo,transdate,acccode,depoloccode,DepoFlag) values('" + Chqno + "','" + ObjWAD.Chequedate.ToString("dd MMM yyyy") + "','" + ObjWAD.ChequeAmount/*txtChqAmt* + "','" + banknm + "','','','" + comments + "','" + bacd + "','" + banm + "','" + BaseUserName + "','" + empnm + "','" + BaseLocationCode + "','" + brnm + "','" + staffcd + "','" + staffnm + "',null,'C','" + BANKBRN + "','" + adjustamt + "','" + Voucherno + "','" + VoucherDate + "','" + acccodecb + "','" + BaseLocationCode + "','Y')";
                        cmd = new SqlCommand(sql, con, trans);
                        cmd.ExecuteNonQuery();
                    }
                }
                if (ACVVM.WAD.IsServicetaxapply)
                {
                    if (Convert.ToDouble(ACVVM.WAD.ServiceTax) > 0)
                    {
                        if (ObjWAD.TransactionMode == "PAYMENT")
                        {
                            Debit = Convert.ToString(ACVVM.WAD.ServiceTax);
                            Credit = "0.00";
                            Acccode = "ALS0009";
                        }
                        else
                        {
                            Debit = "0.00";
                            Credit = Convert.ToString(ACVVM.WAD.ServiceTax);
                            Acccode = "CLO0023";
                        }
                        //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                        sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST   '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";

                        cmd = new SqlCommand(sql_Acctrans, con, trans);
                        cmd.ExecuteNonQuery();

                    }
                    if (Convert.ToDouble(HdnEduCess) > 0)
                    {
                        if (ObjWAD.TransactionMode == "PAYMENT")
                        {
                            Debit = HdnEduCess;
                            Credit = "0.00";
                            Acccode = "ALS0010";
                        }
                        else
                        {
                            Debit = "0.00";
                            Credit = HdnEduCess;
                            Acccode = "LCS0001";
                        }

                        sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST   '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                        cmd = new SqlCommand(sql_Acctrans, con, trans);
                        cmd.ExecuteNonQuery();

                    }

                    if (Convert.ToDouble(HdnHEduCess) > 0)
                    {
                        if (ObjWAD.TransactionMode == "PAYMENT")
                        {
                            Debit = HdnHEduCess;
                            Credit = "0.00";
                            Acccode = "ALS0011";
                        }
                        else
                        {
                            Debit = "0.00";
                            Credit = HdnHEduCess;
                            Acccode = "LCS0002";
                        }
                        //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                        sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                        cmd = new SqlCommand(sql_Acctrans, con, trans);
                        cmd.ExecuteNonQuery();

                    }
                }

                /*GST Effect *

                if (IGST.ChargeAmount > 0)
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Debit = IGST.ChargeAmount.ToString();
                        Credit = "0.00";
                        Acccode = IGST.AccountReceivable; //"ALS0010";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = IGST.ChargeAmount.ToString();
                        Acccode = IGST.AccountPayable; //"LCS0001";
                    }

                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }
                if (SGST.ChargeAmount > 0)
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Debit = SGST.ChargeAmount.ToString();
                        Credit = "0.00";
                        Acccode = SGST.AccountReceivable; //"ALS0010";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = SGST.ChargeAmount.ToString();
                        Acccode = SGST.AccountPayable; //"LCS0001";
                    }

                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }
                if (CGST.ChargeAmount > 0)
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Debit = CGST.ChargeAmount.ToString();
                        Credit = "0.00";
                        Acccode = CGST.AccountReceivable; //"ALS0010";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = CGST.ChargeAmount.ToString();
                        Acccode = CGST.AccountPayable; //"LCS0001";
                    }

                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }
                if (UTGST.ChargeAmount > 0)
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Debit = UTGST.ChargeAmount.ToString();
                        Credit = "0.00";
                        Acccode = UTGST.AccountReceivable; //"ALS0010";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = UTGST.ChargeAmount.ToString();
                        Acccode = UTGST.AccountPayable; //"LCS0001";
                    }

                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }



                /*GST Effect*
                if (objStaxTDSViewModel.IsTDSEnabled)
                {
                    if (Convert.ToDouble(objStaxTDSViewModel.TDSAmount) > 0)
                    {
                        if (ObjWAD.TransactionMode == "PAYMENT")
                        {
                            Credit = objStaxTDSViewModel.TDSAmount.ToString();
                            Debit = "0.00";
                        }
                        else
                        {
                            Credit = "0.00";
                            Debit = objStaxTDSViewModel.TDSAmount.ToString();
                        }
                        Acccode = objStaxTDSViewModel.TDSAcccode;

                        //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                        sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + Acccode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc + "','" + EntryBy + "','','N','N','" + OppaccountCode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                        cmd = new SqlCommand(sql_Acctrans, con, trans);
                        cmd.ExecuteNonQuery();
                    }
                }





                if (ObjWAD.PaymentMode != "Both")
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Credit = Convert.ToString(ObjWAD.NETAMOUNT);// Hnd_totalAmount.Value.ToString();
                        Debit = "0.00";
                    }
                    else
                    {
                        Credit = "0.00";
                        Debit = Convert.ToString(ObjWAD.NETAMOUNT);//Hnd_totalAmount.Value.ToString();
                    }
                    //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }
                else
                {
                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Credit = Convert.ToString(ObjWAD.CashAmount);// txtCashAmt.Text.ToString();
                        Debit = "0.00";
                    }
                    else
                    {
                        Credit = "0.00";
                        Debit = Convert.ToString(ObjWAD.CashAmount);// txtCashAmt.Text.ToString();
                    }
                    OppaccountCode = "CAS0002";

                    //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();

                    if (ObjWAD.TransactionMode == "PAYMENT")
                    {
                        Credit = txtChqAmt;
                        Debit = "0.00";
                    }
                    else
                    {
                        Credit = "0.00";
                        Debit = txtChqAmt;
                    }
                    OppaccountCode = ObjWAD.DepositedInBank;// ddrBankaccode.SelectedValue.ToString();

                    //sql_Acctrans = "exec WEBX_acctrans_insertData_ManualVoucher '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + DocumentNo + "','" + txtAccLoc.Text + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + Dr_Pbov_list.SelectedValue.ToString() + "','" + txtReferenceNo.Text + "','" + txtPreparedLoc.Text + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + txtPanNo.Text + "','" + txtSrvTaxNo.Text + "','" + opertitle + "' ";
                    sql_Acctrans = "exec WEBX_acctrans_insertData_SpecialVoucherGST  '" + Year + "','" + VoucherDate + "','" + Voucherno + "','" + OppaccountCode + "','" + Chqno + "','" + ChqDate + "','" + Debit + "','" + Credit + "','" + Narration + "','" + Transtype + "',0,'" + HdnDocList + "','" + txtAccLoc + "','','" + OppaccountDesc_mode + "','" + EntryBy + "','','N','N','" + OppaccountCode_mode + "',null,Null,Null,Null,Null,'','','" + ACVVM.WAD.CustCode + "','" + ACVVM.WAD.ReferenceNo + "','" + ACVVM.WAD.Preparedatlocation + "','" + PBOV_code + "','" + PBOV_Name + "','" + PBOV_TYP + "','" + ACVVM.WAD.PANNumber + "','" + ACVVM.WAD.ServiceTaxRegNo + "','" + opertitle + "','','" + BaseCompanyCode + "','" + StateCode + "'";
                    cmd = new SqlCommand(sql_Acctrans, con, trans);
                    cmd.ExecuteNonQuery();
                }

                double LedgerDiff = 0;
                sql_Acctrans = "select sum(debit) - sum(credit) as Differance from webx_acctrans_" + BaseYearVal + " where voucher_cancel='N' and voucherno='" + Voucherno + "'";
                cmd = new SqlCommand(sql_Acctrans, con, trans);
                SqlDataReader dr_count = cmd.ExecuteReader();
                while (dr_count.Read())
                {
                    LedgerDiff = Convert.ToDouble(dr_count["Differance"].ToString().Trim());
                }
                dr_count.Close();

                if (LedgerDiff > 0)
                {
                    throw new Exception("Debit Amount Should Be Equal To Payment Amount");
                }
                else
                {
                    trans.Commit();
                }

                con.Close();
                //  Response.Redirect("./voucher_Done_Ver1.aspx?VoucherType=Special&VoucherNo=" + Voucherno + "&Defaultdate=" + Convert.ToDateTime(ACVVM.WA.VoucherDate).ToString("dd MMM yyyy"), false);

            }
            catch (Exception e1)
            {
                // Response.Write("<br><br><br><center><font class='blackboldfnt' color='red'><b>Error : " + e1.Message + "</b></font></center>");
                string msg = e1.Message.ToString();
                msg = msg.Replace('\n', ' ');
                trans.Rollback();
                //Response.Redirect("Message.aspx?" + msg);
                //Response.End();

                ViewBag.StrError = msg;
                return View("Error");
            }
            */
            #endregion

            #region Test
            /*
            string SCVoucherNo = "";
            string ServiceTaxAccountCode = "";
            string AllDocumentList = "";


            try
            {
                DataTable DT = new DataTable();
                string Transtype = "", Debit = "", Credit = "", Accdesc = "", costcodelist = "", coastName = "", OppaccountDesc = "", OppaccountCode = "";
                decimal TotalDebitAmt = 0;
                decimal TotalCreditAmt = 0;
                Webx_Account_Details WAD = new Webx_Account_Details();
                WAD = PAYMENTMODE.FirstOrDefault();
                if (WAD != null)
                {
                    ACVVM.WAD.TransactionMode = WAD.TransactionMode;
                    ACVVM.WAD.PaymentMode = WAD.PaymentMode;
                    ACVVM.WAD.CashAmount = WAD.CashAmount;
                    ACVVM.WAD.ChequeAmount = WAD.ChequeAmount;
                    ACVVM.WAD.ChequeNo = WAD.ChequeNo;
                    ACVVM.WAD.Chequedate = WAD.Chequedate;
                    ACVVM.WAD.IsonAccount = WAD.IsonAccount;
                    ACVVM.WAD.CashAccount = WAD.CashAccount;
                    ACVVM.WAD.Deposited = WAD.Deposited;
                    ACVVM.WAD.DepositedInBank = WAD.DepositedInBank;
                    ACVVM.WAD.ReceivedFromBank = WAD.ReceivedFromBank;
                    ACVVM.WAD.NETAMOUNT = WAD.NETAMOUNT;
                    string Customertype = WAD.CustTyp;
                }

                if (ACVVM.WAD.TransactionMode == "PAYMENT")
                {
                    TotalDebitAmt = ACVVM.WAD.NETAMOUNT;
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "CASH")
                    {
                        Transtype = "CASH PAYMENT";
                        OppaccountCode = "CAS0002";
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where acccategory='CASH'  and Acccode= '" + OppaccountCode + "'";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();

                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BANK")
                    {
                        Transtype = "BANK PAYMENT";
                        OppaccountCode = ACVVM.WAD.DepositedInBank.ToString();
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where (((bkloccode like 'All' or PATINDEx ('%" + BaseLocationCode + "%',bkloccode)>0) AND acccategory='BANK' And Acccode='" + OppaccountCode + "' )) order by Text asc";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BOTH")
                    {
                        Transtype = "BOTH PAYMENT";
                        OppaccountCode = ACVVM.WAD.DepositedInBank.ToString();
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where (((bkloccode like 'All' or PATINDEx ('%" + BaseLocationCode + "%',bkloccode)>0) AND acccategory='BANK' And Acccode='" + OppaccountCode + "' )) order by Text asc";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();

                    }
                }
                else
                {
                    TotalCreditAmt = ACVVM.WAD.NETAMOUNT;
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "CASH")
                    {
                        Transtype = "CASH RECEIPT";
                        OppaccountCode = "CAS0002";
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where acccategory='CASH'  and Acccode= '" + OppaccountCode + "'";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();
                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BANK")
                    {
                        Transtype = "BANK RECEIPT";
                        OppaccountCode = ACVVM.WAD.DepositedInBank.ToString();
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where (((bkloccode like 'All' or PATINDEx ('%" + BaseLocationCode + "%',bkloccode)>0) AND acccategory='BANK' And Acccode='" + OppaccountCode + "' )) order by Text asc";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();

                    }
                    if (ACVVM.WAD.PaymentMode.ToUpper() == "BOTH")
                    {
                        Transtype = "BOTH RECEIPT";
                        OppaccountCode = ACVVM.WAD.DepositedInBank.ToString();
                        OppaccountDesc = "select accdesc as Text from webx_acctinfo where (((bkloccode like 'All' or PATINDEx ('%" + BaseLocationCode + "%',bkloccode)>0) AND acccategory='BANK' And Acccode='" + OppaccountCode + "' )) order by Text asc";
                        DataTable Dt = GF.GetDataTableFromSP(OppaccountDesc);
                        OppaccountDesc = Dt.Rows[0][0].ToString();

                    }
                }
                string PBOV_code = "", PBOV_Name = "";
                if (ACVVM.WAD.CustCode == "" || ACVVM.WAD.CustCode == null)
                {
                    PBOV_code = "8888";
                    PBOV_Name = ACVVM.WAD.CustomerName;
                }
                else
                {
                    PBOV_code = ACVVM.WAD.CustCode;
                    string SQR = "exec USP_GetEmployeeUserDriverVehicleList_New_Portal '" + PBOV_code + "','1','" + ACVVM.WAD.CustTyp + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    PBOV_Name = Dt.Rows[0][1].ToString();
                }

                string Xml_Acccode_Details = "<root>";
                string DocType = "";

                foreach (var item in SCVoucher)
                {
                    item.CostType = item.DocType;
                    item.CostCode = item.DocNo;

                    string CompanyCode = item.AccountCode;

                    if (ACVVM.WAD.TransactionMode == "PAYMENT")
                    {
                        Debit = item.Amount;
                        Credit = "0";
                        if (ACVVM.WAD.IsServicetaxapply == true)
                        {
                            ServiceTaxAccountCode = "ALS0009";
                        }
                    }
                    else
                    {
                        Debit = "0";
                        Credit = item.Amount;
                        if (ACVVM.WAD.IsServicetaxapply == true)
                        {
                            ServiceTaxAccountCode = "CLO0023";
                        }

                    }


                    string Narration = item.NarrationAccountinfo.Replace("'", "''");

                    string SQR = "exec USP_GetAccountCodeList '" + item.AccountCode + "','1'";
                    DataTable Dt = GF.GetDataTableFromSP(SQR);
                    string Description = Dt.Rows[0][1].ToString();

                    string DocNo = item.DocNo;
                    DocType = item.DocType;
                    if (CompanyCode != "")
                    {
                        Accdesc = Description.Replace("<", "&lt;");
                        Accdesc = Description.Replace(">", "&gt;");
                        Accdesc = Description.Replace("\"", "&quot;");
                        Accdesc = Description.Replace("'", "&apos;");

                        if (CompanyCode != "")
                        {
                            if (costcodelist == "")
                            {
                                costcodelist = CompanyCode;
                            }
                            else
                            {
                                costcodelist = costcodelist + "," + CompanyCode;
                            }
                        }
                        if (item.AccountCode.Substring(0, 3).ToUpper() == "EXP")
                        {
                            if (item.CostType != "" || item.CostType != null)
                            {
                                string SQRY = "exec USP_GetAccountCodeIsValidOrNot_New_Portal '" + item.CostCode + "','" + item.CostType + "' ";
                                DataTable Dt_Name = GF.GetDataTableFromSP(SQRY);
                                coastName = Dt_Name.Rows[0][0].ToString();
                            }
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + item.AccountCode + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<coasttype>" + item.CostType + "</coasttype>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<coastCode>" + item.CostCode + "</coastCode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<coastName>" + coastName + "</coastName>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";

                        if (AllDocumentList == "")
                        {
                            AllDocumentList = item.CostCode;
                        }
                        else
                        {
                            AllDocumentList = AllDocumentList + "," + item.CostCode;
                        }
                    }
                }
                Xml_Acccode_Details = Xml_Acccode_Details + "</root>";
                string DepositedAccount = "";

                if (ACVVM.WAD.TransactionMode == "BANK")
                {
                    DepositedAccount = ACVVM.WAD.DepositedInBank;
                }
                else
                {
                    DepositedAccount = ACVVM.WAD.CashAccount;

                }
                string Xml_Other_Details = "<root><Other>";
                Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseFinYear.Substring(0, 4).ToString() + "</Finyear>";
                Xml_Other_Details = Xml_Other_Details + "<AllDocumentList>" + AllDocumentList.ToString() + "</AllDocumentList>";
                Xml_Other_Details = Xml_Other_Details + "<DocType>" + DocType.ToString() + "</DocType>";
                Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
                Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype.Trim() + "</Transtype>";
                Xml_Other_Details = Xml_Other_Details + "<Payment_Receipt>" + ACVVM.WAD.TransactionMode.ToString() + "</Payment_Receipt>";
                Xml_Other_Details = Xml_Other_Details + "<DepositedAccount>" + DepositedAccount + "</DepositedAccount>";
                Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BaseLocationCode.Trim() + "</Brcd>";
                Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName.Trim() + "</Entryby>";
                Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ACVVM.WAD.ManualNo + "</ManualNo>";
                Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ACVVM.WAD.BusinessDivision + "</BusinessType>";
                Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + ACVVM.WAD.Preparedatlocation + "</PreparedAtLoc>";
                Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + BaseLocationCode + "</Acclocation>";
                Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ACVVM.WAD.PreparedFor + "</preparefor>";
                Xml_Other_Details = Xml_Other_Details + "<Refno>" + ACVVM.WAD.ReferenceNo + "</Refno>";
                Xml_Other_Details = Xml_Other_Details + "<Narration>" + ACVVM.WAD.Narration + "</Narration>";
                Xml_Other_Details = Xml_Other_Details + "<Panno>" + ACVVM.WAD.PANNumber + "</Panno>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ACVVM.WAD.ServiceTaxRegNo + "</Svrtaxno>";
                Xml_Other_Details = Xml_Other_Details + "<chqno>" + ACVVM.WAD.ChequeNo + "</chqno>";
                Xml_Other_Details = Xml_Other_Details + "<chqamt>" + Convert.ToDouble(ACVVM.WAD.ChequeAmount) + "</chqamt>";
                Xml_Other_Details = Xml_Other_Details + "<chqdt>" + ACVVM.WAD.Chequedate.ToString("dd MMM yyyy") + "</chqdt>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
                Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + ACVVM.WAD.CustTyp + "</pbov_typ>";
                Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + ACVVM.WAD.IsServicetaxapply + "</Svrtax_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + ACVVM.WAD.IsTDStaxapply + "</Tds_yn>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + ACVVM.WAD.TDSSection + "</Tds_Acccode>";
                Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + ACVVM.WAD.TDSRate + "</Tds_rate>";
                Xml_Other_Details = Xml_Other_Details + "<TDSAmount>" + ACVVM.WAD.TDSAmount + "</TDSAmount>";
                Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + ACVVM.WAD.DepositedInBank + "</bankAcccode>";
                Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ACVVM.WAD.ReceivedFromBank + "</recbanknm>";
                Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + ACVVM.WAD.IsonAccount + "</Onaccount_YN>";
                Xml_Other_Details = Xml_Other_Details + "<Deposited>" + ACVVM.WAD.Deposited + "</Deposited>";
                Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ACVVM.WAD.PaymentMode + "</Paymode>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ACVVM.WAD.ServiceTaxRate + "</SvrTaxRate>";
                Xml_Other_Details = Xml_Other_Details + "<SvrTaxAmount>" + ACVVM.WAD.ServiceTax + "</SvrTaxAmount>";
                Xml_Other_Details = Xml_Other_Details + "<ServiceTaxAccountCode>" + ServiceTaxAccountCode + "</ServiceTaxAccountCode>";
                Xml_Other_Details = Xml_Other_Details + "<TotalCreditAmt>" + TotalCreditAmt + "</TotalCreditAmt>";
                Xml_Other_Details = Xml_Other_Details + "<TotalDebitAmt>" + TotalDebitAmt + "</TotalDebitAmt>";
                Xml_Other_Details = Xml_Other_Details + "<OppaccountCode>" + OppaccountCode + "</OppaccountCode>";
                Xml_Other_Details = Xml_Other_Details + "<OppaccountDesc>" + OppaccountDesc + "</OppaccountDesc>";
                Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ACVVM.WAD.EducationCess + "</EduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ACVVM.WAD.HEducationCess + "</HEduCessRate>";
                Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + BaseCompanyCode + "</COMPANY_CODE>";
                Xml_Other_Details = Xml_Other_Details + "<costcodelist>" + costcodelist.ToString() + "</costcodelist>";
                Xml_Other_Details = Xml_Other_Details + "</Other></root>";

                DT = OS.InsertSpecialCostVoucher(Xml_Acccode_Details, Xml_Other_Details);

                SCVoucherNo = DT.Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "SpecialCostVoucher", "View", "AddEdit", ex.Message);
                return View("Error");
            }
            */
            #endregion

            SqlConnection con;

            string Year = BaseFinYear.ToString();
            string fin_year = "", PBOV_TYP = "";
            string Financial_Year = BaseYearVal; //System.DateTime.Now.FinYear.ToString().Substring(2, 2);
            fin_year = Financial_Year + "_" + Convert.ToString(Convert.ToDouble("16") + 1).PadLeft(2, '0');
            string EntryBy = BaseUserName;
            string Transtype = "";
            string Opertitile = "", Voucherno = "";

            if (ACVVM.WAD.CustTyp == "P")
                PBOV_TYP = "P";
            else if (ACVVM.WAD.CustTyp == "V")
                PBOV_TYP = "V";
            else if (ACVVM.WAD.CustTyp == "E" || ACVVM.WAD.CustTyp == "U")
                PBOV_TYP = "U";
            else if (ACVVM.WAD.CustTyp == "D")
                PBOV_TYP = "D";
            else if (ACVVM.WAD.CustTyp == "L")
                PBOV_TYP = "L";
            else if (ACVVM.WAD.CustTyp == "O")
                PBOV_TYP = "O";

            Webx_Account_Details ObjWAD = new Webx_Account_Details();
            ObjWAD = PAYMENTMODE.First();

            if (ObjWAD.PaymentMode.ToUpper() != "CASH")
            {
                DataTable DT = OS.Duplicate_ChqNO(ObjWAD.ChequeNo, ObjWAD.Chequedate.ToString("dd MMM yyyy"));

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
            }

            if (ObjWAD.PaymentMode.ToUpper() != "BANK")
            {
                string FIN_Start = "";
                FIN_Start = "01 Apr " + BaseYearVal.Split('_')[0];
                DataTable DT = OS.DR_VR_Trn_Halt(ACVVM.WAD.AccountingLocation, Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy"), FIN_Start, BaseYearVal, ObjWAD.CashAmount.ToString());

                string Cnt = DT.Rows[0][0].ToString();
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }

                if (Cnt != "T")
                {
                    ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode.ToString();
                    return View("Error");
                }
            }

            if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
            {
                Opertitile = "SPECIAL COST VOUCHER";

                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK PAYMENT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH PAYMENT";
                }
            }
            else
            {
                Opertitile = "SPECIAL COST VOUCHER";
                if (ObjWAD.PaymentMode.ToUpper() == "CASH")
                {
                    Transtype = "CASH RECEIPT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BANK")
                {
                    Transtype = "BANK RECEIPT";
                }
                if (ObjWAD.PaymentMode.ToUpper() == "BOTH")
                {
                    Transtype = "BOTH RECEIPT";
                }
            }

            string PBOV_code = "", PBOV_Name = "";

            if (ACVVM.WAD.CustCode == "")
            {
                PBOV_code = "8888";
                PBOV_Name = ACVVM.WAD.CustomerName.Trim();
            }
            else
            {
                PBOV_code = ACVVM.WAD.CustCode.ToString();
                webx_CUSTHDR WCHdr = new webx_CUSTHDR();
                string Name = "";
                if (ACVVM.WAD.CustTyp == "E" && (ACVVM.WAD.CustCode == null || ACVVM.WAD.CustCode == ""))
                {
                    PBOV_code = "8888";
                    PBOV_Name = ACVVM.WAD.CustomerName.Trim();
                }
                else if (ACVVM.WAD.CustTyp != "O")
                {
                    try
                    {
                        WCHdr = OS.GetCustmer_Type(ACVVM.WAD.CustCode, PBOV_TYP).First();
                        Name = WCHdr.CUSTNM;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.StrError = ex.Message.ToString().Replace('\n', ' ') + " Please Select Proper Customer.";
                        return View("Error");
                    }
                }
                else
                {
                    Name = ACVVM.WAD.CustomerName.ToString();
                }
                PBOV_Name = Name.Trim();
            }

            string svrcheck = "N", DepoFlag = "N", Str_Onaccount = "N";//tdscheck = "N",
            string BankCode = "";
            double adjustamt = 0;
            string TdsCalForLedgerAmt = "N";

            if (ACVVM.WAD.IsServicetaxapply)
            {
                svrcheck = "Y";
            }
            //if (ACVVM.WAD.IsTDStaxapply)
            //{
            //    tdscheck = "Y";
            //}
            if (ACVVM.WAD.IsTDSWithoutSvcTax)
            {
                TdsCalForLedgerAmt = "Y";
            }

            if (ObjWAD.TransactionMode == "RECEIPT")
            {
                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    BankCode = "ACA0002";

                    if (ObjWAD.Deposited)
                    {
                        DepoFlag = "Y";
                        BankCode = ObjWAD.DepositedInBank;
                    }
                    adjustamt = Convert.ToDouble(ObjWAD.ChequeAmount);
                    if (ObjWAD.IsonAccount)
                    {
                        Str_Onaccount = "Y";
                        adjustamt = 0;
                    }
                }
            }
            else
            {
                if (ObjWAD.PaymentMode.ToUpper() != "Cash")
                {
                    DepoFlag = "Y";
                    BankCode = ObjWAD.DepositedInBank;
                }
            }

            string CompanyCode = "", Acccode = "", Accdesc = "", Debit = "";
            string Credit = "", Narration = "", Stax_App = "N", CostCenterId = "", CostCenter = "";
            string hdnroundoff = "N";
            string hdneditablsvctaxrate = "N";
            string HdnCompanyDropDownRule = "N";
            string HdnRowwiseSvcTaxRule = "N";
            string HdnTransactionWiseAccountRule = "N";
            string HdnCostCenterRule = "N";

            hdnroundoff = OS.ManualVouchersRule("RoundOff");
            hdneditablsvctaxrate = OS.ManualVouchersRule("EditableServiceTax");
            HdnCompanyDropDownRule = OS.ManualVouchersRule("CompanyDropDown");
            HdnRowwiseSvcTaxRule = OS.ManualVouchersRule("RowwiseSvcTax");
            HdnTransactionWiseAccountRule = OS.ManualVouchersRule("Transwiserule");
            HdnCostCenterRule = OS.ManualVouchersRule("CostCenterRule");

            string Xml_Acccode_Details = "<root>";

            if (SCVoucher != null)
            {
                foreach (var item in SCVoucher)
                {
                    CompanyCode = item.AccountCode;
                    CostCenter = item.DocType;
                    CostCenterId = item.DocNo;
                    if (HdnCostCenterRule == "Y")
                    {
                        //if (!chkCmnCostCenter.Checked)
                        //{
                        //    DropDownList ddlcctype = (DropDownList)gridrow.FindControl("ddlcctype");
                        //    TextBox txtcostcenter = (TextBox)gridrow.FindControl("txtcostcenter");

                        //    CostCenterId = ddlcctype.SelectedValue.ToString();
                        //    CostCenter = txtcostcenter.Text.ToString();
                        //}
                    }

                    if (ObjWAD.TransactionMode.ToUpper() == "PAYMENT")
                    {
                        Debit = item.Amount;
                        Credit = "0.00";
                    }
                    else
                    {
                        Debit = "0.00";
                        Credit = item.Amount;
                    }

                    Narration = item.NarrationAccountinfo.Replace("'", "''");
                    bool chkstax = false;

                    if (HdnRowwiseSvcTaxRule == "Y")
                    {
                        if (chkstax)
                            Stax_App = "Y";
                        else
                            Stax_App = "N";
                    }

                    if (CompanyCode != "")
                    {
                        DataTable dt = OS.FindOutSystemAccountCode(CompanyCode, BaseLocationCode);
                        if (dt.Rows.Count == 0)
                        {
                            ViewBag.StrError = "Error : Not Valid Account Code : " + BaseCompanyCode + ". So Please Enter The Valid Account Details";
                            return View("Error");
                        }
                        Acccode = dt.Rows[0]["Acccode"].ToString().Trim();
                        Accdesc = dt.Rows[0]["Accdesc"].ToString().Trim();
                        Accdesc = Accdesc.Replace("<", "&lt;");
                        Accdesc = Accdesc.Replace(">", "&gt;");
                        Accdesc = Accdesc.Replace("\"", "&quot;");
                        Accdesc = Accdesc.Replace("'", "&apos;");

                        if (Str_Onaccount == "Y" && (Acccode != "CDA0001" && Acccode != "ASS0267"))
                        {
                            ViewBag.StrError = "Error : On Account Cheque Facility is not Availble for : " + Accdesc + ". This Facility Available only for ledger : Billed Debtors";
                            return View("Error");
                        }

                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Acccode>" + Acccode.Trim() + "</Acccode>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Accdesc>" + Accdesc.Trim() + "</Accdesc>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Debit>" + Debit.Trim() + "</Debit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Credit>" + Credit.Trim() + "</Credit>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Narration>" + Narration.Trim() + "</Narration>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<Stax_App>" + Stax_App.Trim() + "</Stax_App>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenterId>" + CostCenterId.Trim() + "</CostCenterId>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "<CostCenter>" + CostCenter.Trim() + "</CostCenter>";
                        Xml_Acccode_Details = Xml_Acccode_Details + "</Acccode>";
                    }
                }
            }

            Xml_Acccode_Details = Xml_Acccode_Details + "</root>";

            string Company_code = BaseCompanyCode;
            string CommonCostCenter = "N";
            if (HdnCostCenterRule == "Y")
            {
                //if (chkCmnCostCenter.Checked)
                //{
                //    CommonCostCenter = "Y";
                //}
            }

            string BranchCode = "";
            if (ACVVM.WAD.AccountingLocation == "")
                BranchCode = BaseLocationCode;
            else
            {
                if (OS.CheckLocation(ACVVM.WAD.AccountingLocation))
                    BranchCode = ACVVM.WAD.AccountingLocation.Trim();
                else
                {
                    ViewBag.StrError = "Accounting Location Is Not Valid";
                    return View("Error");
                }
            }

            string Xml_Other_Details = "<root><Other>";
            Xml_Other_Details = Xml_Other_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
            Xml_Other_Details = Xml_Other_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
            Xml_Other_Details = Xml_Other_Details + "<Transtype>" + Transtype + "</Transtype>";
            Xml_Other_Details = Xml_Other_Details + "<Brcd>" + BranchCode + "</Brcd>";
            Xml_Other_Details = Xml_Other_Details + "<Entryby>" + BaseUserName + "</Entryby>";
            Xml_Other_Details = Xml_Other_Details + "<Opertitle>" + Opertitile.ToString() + "</Opertitle>";
            Xml_Other_Details = Xml_Other_Details + "<ManualNo>" + ACVVM.WAD.ManualNo + "</ManualNo>";
            Xml_Other_Details = Xml_Other_Details + "<BusinessType>" + ACVVM.WAD.BusinessDivision + "</BusinessType>";
            Xml_Other_Details = Xml_Other_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
            Xml_Other_Details = Xml_Other_Details + "<Acclocation>" + ACVVM.WAD.AccountingLocation + "</Acclocation>";
            Xml_Other_Details = Xml_Other_Details + "<preparefor>" + ACVVM.WAD.PreparedFor + "</preparefor>";
            Xml_Other_Details = Xml_Other_Details + "<Refno>" + ACVVM.WAD.ReferenceNo + "</Refno>";
            Xml_Other_Details = Xml_Other_Details + "<Narration>" + ACVVM.WAD.Narration + "</Narration>";
            Xml_Other_Details = Xml_Other_Details + "<Panno>" + ACVVM.WAD.PANNumber + "</Panno>";
            Xml_Other_Details = Xml_Other_Details + "<Svrtaxno>" + ACVVM.WAD.ServiceTaxRegNo + "</Svrtaxno>";
            Xml_Other_Details = Xml_Other_Details + "<chqno>" + ObjWAD.ChequeNo + "</chqno>";
            Xml_Other_Details = Xml_Other_Details + "<chqamt>" + ObjWAD.ChequeAmount + "</chqamt>";
            Xml_Other_Details = Xml_Other_Details + "<chqdt>" + ObjWAD.Chequedate.ToString("dd MMM yyyy") + "</chqdt>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_code>" + PBOV_code + "</pbov_code>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_name>" + PBOV_Name + "</pbov_name>";
            Xml_Other_Details = Xml_Other_Details + "<pbov_typ>" + PBOV_TYP + "</pbov_typ>";
            Xml_Other_Details = Xml_Other_Details + "<Svrtax_yn>" + svrcheck + "</Svrtax_yn>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_yn>" + (objStaxTDSViewModel.IsTDSEnabled ? "Y" : "N") + "</Tds_yn>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_Acccode>" + objStaxTDSViewModel.TDSAcccode + "</Tds_Acccode>";
            Xml_Other_Details = Xml_Other_Details + "<Tds_rate>" + objStaxTDSViewModel.TDSRate + "</Tds_rate>";
            Xml_Other_Details = Xml_Other_Details + "<TdsAmount>" + objStaxTDSViewModel.TDSAmount + "</TdsAmount>";
            Xml_Other_Details = Xml_Other_Details + "<bankAcccode>" + BankCode + "</bankAcccode>";
            Xml_Other_Details = Xml_Other_Details + "<recbanknm>" + ObjWAD.ReceivedFromBank + "</recbanknm>";
            Xml_Other_Details = Xml_Other_Details + "<Onaccount_YN>" + Str_Onaccount + "</Onaccount_YN>";
            Xml_Other_Details = Xml_Other_Details + "<Deposited>" + DepoFlag + "</Deposited>";
            Xml_Other_Details = Xml_Other_Details + "<Paymode>" + ObjWAD.PaymentMode + "</Paymode>";
            Xml_Other_Details = Xml_Other_Details + "<SvrTaxRate>" + ACVVM.WAD.ServiceTaxRate /*HdnServiceTaxRate*/ + "</SvrTaxRate>";
            Xml_Other_Details = Xml_Other_Details + "<EduCessRate>" + ACVVM.WAD.EducationCess /*HdnEduCessRate*/ + "</EduCessRate>";
            Xml_Other_Details = Xml_Other_Details + "<HEduCessRate>" + ACVVM.WAD.HEducationCess /*HdnHEduCessRate*/ + "</HEduCessRate>";
            Xml_Other_Details = Xml_Other_Details + "<TdsOption>" + TdsCalForLedgerAmt + "</TdsOption>";
            Xml_Other_Details = Xml_Other_Details + "<CommonCostCenter>" + CommonCostCenter + "</CommonCostCenter>";
            Xml_Other_Details = Xml_Other_Details + "<COMPANY_CODE>" + Company_code + "</COMPANY_CODE>";
            Xml_Other_Details = Xml_Other_Details + "<GSTType>" + ACVVM.WAD.GSTType + "</GSTType>";
            Xml_Other_Details = Xml_Other_Details + "<TYPEOFGST>" + ACVVM.WAD.TypeOfGst + "</TYPEOFGST>";
            Xml_Other_Details = Xml_Other_Details + "</Other></root>";

            XmlDocument xmlDocGSTCha = new XmlDocument();
            if (DynamicList != null && DynamicList.Count > 0)
            {
                XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                    xmlStreamCha.Position = 0;
                    xmlDocGSTCha.Load(xmlStreamCha);
                }
            }
            else
            {
                xmlDocGSTCha.InnerXml = "<root></root>";
            }


            con = new SqlConnection(Connstr);
            con.Open();

            SqlTransaction trans;
            trans = con.BeginTransaction();

            double LedgerDiff = 0;

            try
            {
                Voucherno = OS.InsertSpecialCostVoucherDetails(Xml_Acccode_Details, Xml_Other_Details, xmlDocGSTCha.InnerXml, trans);
                LedgerDiff = OS.VoucherDiff(Voucherno, Financial_Year, trans);

                if (LedgerDiff != 0)
                {
                    ViewBag.StrError = "Error : Debit Amount Is Not Equal To Payment Amount";
                    return View("Error");
                }
            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                trans.Rollback();
                ViewBag.StrError = ErrorMsg;
                return View("Error");

            }
            con.Close();


            return RedirectToAction("SpecialCostVoucherDone", new { VoucherNo = Voucherno });
        }

        public JsonResult GetManualNoisvalid(string ManualNo)
        {
            int i = 1;
            try
            {
                DataTable DT = OS.GetAccountCodeisValidorNot(ManualNo, "ManualNo");
                string IsValidmanualNo = DT.Rows[0][0].ToString();
                if (IsValidmanualNo == "0")
                {
                    i = 0;
                }
                return new JsonResult()
                {
                    Data = new
                    {
                        IsValid = i
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetManualNoisvalid", "Json", "Listing", ex.Message);
                return new JsonResult()
                {
                    Data = new { IsValid = 1 }
                };
            }

        }

        public ActionResult SpecialCostVoucherDone(string VoucherNo)
        {
            ViewBag.Voucherno = VoucherNo;
            ViewBag.BaseYearVal = BaseYearVal;

            return View();
        }

        #endregion

        #region Advice Generation

        public ActionResult AdviceGenerationCriteria()
        {
            AdviceAcknowledgementViewModel viewmodel = new AdviceAcknowledgementViewModel();
            WEBX_Advice_Hdr WAH = new WEBX_Advice_Hdr();
            WAH.RaisedBy = BaseLocationCode;
            WAH.AdvGenByCd = BaseUserName;
            viewmodel.Advice = WAH;
            Webx_Account_Details OBjWAD = new Webx_Account_Details();
            OBjWAD.SRNo = 1;
            OBjWAD.Vouchertype = "ADVICE";
            List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
            ListWAD1.Add(OBjWAD);
            viewmodel.WADListing = ListWAD1;
            return View(viewmodel);
        }

        public ActionResult AdviceGenerationSubmit(AdviceAcknowledgementViewModel VW, List<Webx_Account_Details> PAYMENTMODE)
        {
            Webx_Account_Details ObjWAD = PAYMENTMODE.First();

            string Financial_Year = BaseYearValFirst;
            string AdviceNo = "";
            string AdviceType = VW.Advice.Advicetype;// dlstAdviceType.SelectedValue.ToString();
            string RaisedBy = BaseLocationCode; ;
            string RaisedOn = VW.Advice.RaisedOn;
            string GeneratedBy = BaseUserName; ;
            string Reason = VW.Advice.reason;// txtReason.Text;
            string enclosed = VW.Advice.enclosed;// txtEnclosedDoc.Text;
            string Advicetype = VW.Advice.Advicetype;// dlstAdviceType.SelectedValue.ToString();
            string PaymentMode = ObjWAD.PaymentMode;// ddlPayMode.SelectedValue.ToString();
            string advice_amount = "";
            string CashBankAccount = "";
            string chqno = ObjWAD.ChequeNo;// txtChqNo.Text;
            string chqdate = ObjWAD.Chequedate.ToString("dd MMM yyyy");

            Boolean success = false;
            string txtPaymentAmt = Convert.ToString(ObjWAD.NETAMOUNT);
            string txtChqNo = Convert.ToString(ObjWAD.ChequeNo);
            string txtChqDate = ObjWAD.Chequedate.ToString("dd MMM yyyy");
            string txtAdviceDate = VW.Advice.AdviceDt.ToString("dd MMM yyyy");
            string txtCashAmt = Convert.ToString(ObjWAD.CashAmount);
            string txtChqAmt = Convert.ToString(ObjWAD.ChequeAmount);

            if (AdviceType == "D")
            {
                if (PaymentMode.ToUpper() == "CASH")
                {
                    //string Cnt = VendorPayment_Utility.DR_VR_Trn_Halt(Convert.ToDouble(txtCashAmt.Text), fn.Mydate(txtAdviceDate.Text));
                    string Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(txtCashAmt), GF.FormateDate(VW.Advice.AdviceDt), BaseFinYear, BaseLocationCode);

                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    advice_amount = txtCashAmt;
                    CashBankAccount = Convert.ToString(ObjWAD.CashAccount);
                }
            }

            if (PaymentMode.ToUpper() == "CASH")
            {
                if (AdviceType == "D")
                {
                    //string Cnt = VendorPayment_Utility.DR_VR_Trn_Halt(Convert.ToDouble(txtCashAmt.Text), fn.Mydate(txtAdviceDate.Text));
                    string Cnt = PS.DR_VR_Trn_Halt(Convert.ToDouble(txtCashAmt), GF.FormateDate(VW.Advice.AdviceDt), BaseFinYear, BaseLocationCode);

                    if (Cnt == "F")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                    if (Cnt != "T")
                    {
                        ViewBag.StrError = "Transactions halted !!!,No sufficient 'CASH IN HAND' for this Location " + BaseLocationCode;
                        return View("Error");
                    }
                }
                advice_amount = txtCashAmt;
                CashBankAccount = Convert.ToString(ObjWAD.CashAccount);
            }

            if (PaymentMode.ToUpper() == "BANK")
            {
                string Cnt = PS.Duplicate_ChqNO(txtChqNo, txtChqDate);
                if (Cnt == "F")
                {
                    ViewBag.StrError = "Duplicate Cheque Entered";
                    return View("Error");
                }
                advice_amount = txtChqAmt;
                CashBankAccount = Convert.ToString(ObjWAD.DepositedInBank);
            }

            SqlTransaction trans;
            SqlCommand cmd;
            SqlConnection con;
            con = new SqlConnection(Connstr);
            con.Close();
            con.Open();

            //if (chqdate == "01 Jan 0001")
            //    chqdate = "01 Jan 1990";

            trans = con.BeginTransaction();
            try
            {
                double adv_amt = Convert.ToDouble(advice_amount);
                double payment_amt = Convert.ToDouble(txtPaymentAmt);

                if (adv_amt != payment_amt)
                    throw new Exception("Advice Payment Amount Shoud be Equal to Cash or Cheque Amount");
                else if (adv_amt == 0)
                    throw new Exception("Advice Amount Can't Be Zero");

                chqno = txtChqNo;
                chqdate = txtChqDate;
                if (chqdate == "01 Jan 0001")
                    chqdate = "01 Jan 1990";

                string strSql = "exec usp_Insert_webx_Advice_hdr_ChqDetail '" + txtAdviceDate + "','" + AdviceType + "','";
                strSql += RaisedOn + "','" + RaisedBy + "','" + adv_amt + "','" + Reason + "','" + PaymentMode.ToUpper() + "','";
                strSql += enclosed + "','" + Financial_Year + "','" + GeneratedBy + "','" + CashBankAccount + "','";
                strSql += chqno + "','" + chqdate + "','Y','" + BaseCompanyCode + "','output'";
                cmd = new SqlCommand(strSql, con, trans);
                SqlDataReader dr = null;
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    AdviceNo = Convert.ToString(dr["GenAdviceNo"]);
                }
                dr.Close();

                string strsql_proc = "exec usp_AdviceTransaction_ver1 1,'" + AdviceNo + "','" + Financial_Year.Substring(2, 2) + "','" + chqno + "','" + chqdate + "','" + BaseCompanyCode + "'";
                cmd = new SqlCommand(strsql_proc, con, trans);
                cmd.ExecuteNonQuery();

                trans.Commit();
                con.Close();
                success = true;
            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                con.Close();
                trans.Rollback();
                ViewBag.StrError = ErrorMsg;
                return View("Error");
            }
            if (success)
            {
                return RedirectToAction("AdviceGenerationDone", new { Adviceno = AdviceNo });
            }


            #region
            /*  string ChequeEntryY_N = "";
            string fromAcccode1 = "";

            Webx_Account_Details WAD = new Webx_Account_Details();
            WAD = PAYMENTMODE.FirstOrDefault();
            if (WAD != null)
            {
                Adviceviewmodel.Advice.PayMode = WAD.PaymentMode;
                Adviceviewmodel.Advice.CashAmount = WAD.CashAmount;
                Adviceviewmodel.Advice.ChequeAmount = WAD.ChequeAmount;
                Adviceviewmodel.Advice.ChequeNo = WAD.ChequeNo;
                Adviceviewmodel.Advice.ChequeDate = WAD.Chequedate;
                Adviceviewmodel.Advice.PaymentAmount = WAD.NETAMOUNT;
                Adviceviewmodel.Advice.CashAccount = WAD.CashAccount;
                Adviceviewmodel.Advice.BankAccount = WAD.DepositedInBank;
            }

            string AdviceDt = Adviceviewmodel.Advice.AdviceDt.ToString("dd MMM yyyy");
            string ChequeDate = Adviceviewmodel.Advice.ChequeDate.ToString("dd MMM yyyy");
            if (AdviceDt == "01 Jan 0001")
            {
                AdviceDt = Convert.ToString("01 Jan 1900");
            }
            if (Adviceviewmodel.Advice.PayMode.ToUpper() == "CASH")
            {
                fromAcccode1 = Adviceviewmodel.Advice.CashAccount;
                ChequeEntryY_N = "N";
                ChequeDate = Convert.ToString("01 Jan 1900");
            }
            else
            {
                fromAcccode1 = Adviceviewmodel.Advice.BankAccount;
                ChequeEntryY_N = "Y";
            }

            DataTable DT = OS.GetAdviceGenerationSubmit(AdviceDt, Adviceviewmodel.Advice.Advicetype, Adviceviewmodel.Advice.RaisedOn.ToString(), Adviceviewmodel.Advice.RaisedBy.ToString(), Adviceviewmodel.Advice.PaymentAmount, Adviceviewmodel.Advice.reason.ToString(), Adviceviewmodel.Advice.PayMode.ToString(), Adviceviewmodel.Advice.enclosed.ToString(), BaseFinYear.ToString(), Adviceviewmodel.Advice.AdvGenByCd.ToString(), fromAcccode1, Convert.ToString(Adviceviewmodel.Advice.ChequeNo), ChequeDate, ChequeEntryY_N, BaseCompanyCode, Convert.ToString(Adviceviewmodel.Advice.Adviceno));
            string Adviceno = DT.Rows[0]["GenAdviceNo"].ToString();*/

            #endregion

            return RedirectToAction("AdviceGenerationDone", new { Adviceno = AdviceNo });
        }

        public ActionResult AdviceGenerationDone(string Adviceno)
        {
            ViewBag.Adviceno = Adviceno;
            return View();
        }

        #endregion

        #region Advice Acknowledgement

        public ActionResult AdviceAcknowledgementCriteria()
        {
            AdviceAcknowledgementViewModel AAVM = new AdviceAcknowledgementViewModel();
            return View(AAVM);
        }

        public ActionResult AdviceAcknowledgementList(AdviceAcknowledgementViewModel AAVM)
        {
            List<WEBX_Advice_Hdr> AdviceQuery = new List<WEBX_Advice_Hdr>();
            try
            {
                string tableName = "webx_acctrans_" + BaseYearVal;
                AdviceQuery = OS.GetAdivceAcknowledgementList(AAVM.datefrom, AAVM.dateto, AAVM.Adviceno, BaseLocationCode.ToString(), tableName, BaseCompanyCode);
                ViewBag.BaseLocationCode = BaseLocationCode;
                AAVM.ListAdvice = AdviceQuery;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "AdivceAcknowledgementList", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(AAVM);
        }

        public ActionResult AdviceAcknowledgementSubmit(AdviceAcknowledgementViewModel AAVM, List<WEBX_Advice_Hdr> Advice)
        {
            string docNo = "";
            bool success = false;

            SqlConnection conn = new SqlConnection(Connstr);
            conn.Open();

            SqlTransaction trans;
            SqlCommand cmd;
            trans = conn.BeginTransaction();
            try
            {
                string Year = BaseYearVal;// Session["FinYear"].ToString().Substring(2, 2);
                // string fin_year_next = Convert.ToDouble(Year) + 1;
                // fin_year = Year + "_" + fin_year_next.ToString().PadLeft(2, '0');
                string table_name = "webx_acctrans_" + BaseYearVal;

                // myFunc = new MyFunctions();

                //foreach (GridViewRow gr in dgGeneral.Rows)
                //{   
                foreach (var ite in Advice)
                {
                    string acccode1 = ite.Toacccode;// (DropDownList)gr.FindControl("dlstAcccode");
                    string accdate1 = ite.accdate1.ToString("dd MMM yyyy");// (TextBox)gr.FindControl("txtAccDate");

                    if (ite.IsCheck)
                    {
                        if (docNo == "")
                            docNo = ite.Adviceno;
                        else
                            docNo = docNo + "," + ite.Adviceno;

                        string chequeadvice = "select advicestatus from webx_advice_hdr where adviceno='" + ite.Adviceno + "'";
                        cmd = new SqlCommand(chequeadvice, conn, trans);
                        SqlDataReader dr_chqadv;
                        dr_chqadv = cmd.ExecuteReader();

                        string advicestatus = "";
                        while (dr_chqadv.Read())
                        {
                            advicestatus = dr_chqadv["advicestatus"].ToString();
                        }
                        dr_chqadv.Close();

                        advicestatus = advicestatus.ToUpper();

                        if (advicestatus == "SUBMITTED")
                        {
                            string sql = "exec usp_Advice_updateData '" + ite.Adviceno + "','" + BaseUserName + "','" + accdate1 + "','" + BaseLocationCode + "','" + BaseLocationCode + "','" + BaseUserName + "','" + acccode1 + "','" + accdate1 + "'";
                            cmd = new SqlCommand(sql, conn, trans);
                            cmd.ExecuteNonQuery();

                            string chqno = "", chqdate = "";

                            if (ite.ChequeNo != "--")
                                chqno = ite.ChequeNo;
                            if (ite.ChequeDatestr != "--")
                                chqdate = ite.ChequeDatestr;

                            string strsql_proc = "exec usp_AdviceTransaction_ver1 2,'" + ite.Adviceno + "','" + BaseYearValFirst + "','" + chqno + "','" + chqdate + "','" + BaseCompanyCode + "'";
                            cmd = new SqlCommand(strsql_proc, conn, trans);
                            cmd.ExecuteNonQuery();
                        }
                    }
                }

                trans.Commit();
                success = true;

            }
            catch (Exception e1)
            {
                Response.Write("Error" + e1.Message);
                string msg = e1.Message.ToString();
                msg = msg.Replace('\n', ' ');
                msg = msg.Replace('\r', ' ');
                trans.Rollback();

                ViewBag.StrError = msg;
                return View("Error");
                //Response.Redirect("~/GUI/ErrorPage.aspx?PageHead=" + "Fund Transfer" + "&ErrorMsg=" + msg);
                //Response.End();
            }
            finally
            {

            }
            if (success)
            {
                return RedirectToAction("AdviceAcknowledgementDone", new { Adviceno = docNo });
            }
            return RedirectToAction("AdviceAcknowledgementDone", new { Adviceno = docNo });
            #region

            /*  List<WEBX_Advice_Hdr> LisAdvice = new List<WEBX_Advice_Hdr>();
                WEBX_Advice_Hdr CygnusAdvice = new WEBX_Advice_Hdr();
                foreach (var item in Advice)
                {
                    if (item.IsCheck == true)
                    {
                        CygnusAdvice = Advice.Where(c => c.Adviceno == item.Adviceno).FirstOrDefault();
                        LisAdvice.Add(CygnusAdvice);

                        string[] Year_Arr = BaseFinYear.ToString().Split('-');

                        DataTable DT = OS.GetAdviceAcknowledgementSubmit(CygnusAdvice.Adviceno.ToString(), BaseUserName.ToString(), CygnusAdvice.accdate1.ToString("dd MMM yyyy"), BaseLocationCode.ToString(), BaseLocationCode.ToString(), BaseUserName.ToString(), CygnusAdvice.Toacccode.ToString(), CygnusAdvice.accdate1.ToString("dd MMM yyyy"), Year_Arr[0]);
                        Adviceno = DT.Rows[0]["Column1"].ToString();
                        if (AdvicenoList == "")
                        {
                            AdvicenoList = Adviceno;
                        }
                        else
                        {
                            AdvicenoList = AdvicenoList + "," + Adviceno;
                        }
                    }
                }
                return RedirectToAction("AdviceAcknowledgementDone", new { Adviceno = AdvicenoList });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "AdviceAcknowledgementSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
                return RedirectToAction("Error", "Home", new { returnUrl = "/Voucher/AdviceAcknowledgementCriteria" });
            }*/

            #endregion

        }

        public ActionResult AdviceAcknowledgementDone(string Adviceno)
        {
            string[] strarray = Adviceno.Split(',');
            List<string> strList = new List<string>();

            foreach (string obj in strarray)
                strList.Add(obj);

            ViewBag.FinYear = BaseYearVal;
            return View(strList);
        }

        #endregion

        #region Advice Cancellation

        public ActionResult AdviceCancellation()
        {
            AdviceCancellationViewModel ViewModel = new AdviceCancellationViewModel();
            return View(ViewModel);
        }

        public ActionResult AdviceCancellationList(AdviceCancellationViewModel Viewmodel)
        {
            List<WEBX_Advice_Hdr> List = new List<WEBX_Advice_Hdr>();
            try
            {
                List = OS.GetAdviceCancellationList(Viewmodel.FromDate, Viewmodel.ToDate, Viewmodel.Adviceno);
                Viewmodel.ListAdvice = List;
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "AdviceCancellationList", "View", "Listing", ex.Message + "//" + ViewBag.StrError);
                return View("Error");
            }
            return View(Viewmodel);
        }

        public ActionResult AdviceCancellationSubmit(List<WEBX_Advice_Hdr> Advice)
        {
            try
            {
                List<WEBX_Advice_Hdr> ListAdvice = new List<WEBX_Advice_Hdr>();
                WEBX_Advice_Hdr CygnusAdvice = new WEBX_Advice_Hdr();
                string AdviceNoList = "";
                //DateTime CancelDt = System.DateTime.Today;
                string BaseFinancialYear = BaseFinYear.ToString().Substring(0, 4);
                BaseFinancialYear = BaseFinYear.Substring(2, 2);
                double NextFinYear = 0;
                NextFinYear = Convert.ToDouble(BaseFinancialYear) + 1;
                string FinYear = BaseFinancialYear + "_" + NextFinYear.ToString().PadLeft(2, '0');
                DateTime Cancel_Date = System.DateTime.Today;
                foreach (var item in Advice)
                {
                    if (item.IsCheck == true)
                    {
                        CygnusAdvice = Advice.Where(c => c.Adviceno == item.Adviceno).FirstOrDefault();
                        DataTable DT = OS.GetAdviceCancellationSubmit(BaseUserName, Cancel_Date, CygnusAdvice.Cancel_Reason, CygnusAdvice.Adviceno, BaseFinancialYear);
                        string AdviceNo = DT.Rows[0]["Column1"].ToString();

                        if (AdviceNoList == "")
                        {
                            AdviceNoList = AdviceNo;
                        }
                        else
                        {
                            AdviceNoList = AdviceNoList + "," + AdviceNo;
                        }
                    }
                }
                return RedirectToAction("AdviceCancellationDone", new { AdviceNoList = AdviceNoList });
            }
            catch (Exception ex)
            {
                ViewBag.StrError = "Message:-" + ex.Message;
                Error_Logs(ControllerName, "AdviceCancellationSubmit", "View", "Listing", ex.ToString() + "//" + ViewBag.StrError);
                return View("Error");
            }
        }

        public ActionResult AdviceCancellationDone(string AdviceNoList)
        {
            string[] strarray = AdviceNoList.Split(',');
            List<string> strList = new List<string>();
            ViewBag.CFRNO = AdviceNoList;

            foreach (string obj in strarray)
                strList.Add(obj);

            return View(strList);
        }

        #endregion

        #region Common Function

        public JsonResult GetBankCashList(string Type)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                if (Type == "Cash")
                {
                    string SQRY = "exec USP_GetAccoutLedgerForContra '" + Type + "','" + BaseLocationCode + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                    List<GetCashDetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCashDetailList>(Dt);

                    return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                }
                if (Type == "Bank")
                {
                    string SQRY = "exec USP_GetAccoutLedgerForContra '" + Type + "','" + BaseLocationCode + "'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                    List<GetBankDetailList> BankDetailList = DataRowToObject.CreateListFromTable<GetBankDetailList>(Dt);

                    return Json(BankDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                }

                if (Type == "CONTRA")
                {
                    string SQRY = "select acccode as Value,accdesc as Text from webx_acctinfo where accdesc='CONTRA'";
                    DataTable Dt = GF.GetDataTableFromSP(SQRY);
                    List<GetCONTRADetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCONTRADetailList>(Dt);

                    return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                }

                return Json(list);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetCashList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }
        public JsonResult GetBankCashOtherContraList_BranchAccounting(string Type, string ExpenseType)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {

                string SQRY = "exec USP_GetBankCashOtherContraList_BranchAccounting '" + Type + "','" + ExpenseType + "','" + BaseLocationCode + "'";
                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<GetCashDetailList> CashDetailList = DataRowToObject.CreateListFromTable<GetCashDetailList>(Dt);
                return Json(CashDetailList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
                // return Json(list);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetCashList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }
        public JsonResult GetCostTypeFromAccountCode(string AccountCode)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                DataTable Dt_City = OS.GetCostTypeFromAccountCode(AccountCode);
                List<GetCostTypeFromAccountCode> CostTypeList = DataRowToObject.CreateListFromTable<GetCostTypeFromAccountCode>(Dt_City);

                return Json(CostTypeList.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetCostTypeFromAccountCode", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        public JsonResult GetAccountCodeIsValid(string AccountCode, string CodeType)
        {
            try
            {
                DataTable Dt_Item = OS.GetAccountCodeisValidorNot(AccountCode, CodeType);
                string IsValid = Dt_Item.Rows[0][0].ToString();
                return new JsonResult()
                {
                    Data = new
                    {
                        IsValid = IsValid
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetAccountCodeIsValid", "Json", "Listing", ex.Message);
                return new JsonResult()
                {
                    Data = new { IsValid = 0 }
                };
            }

        }

        public JsonResult GetCustUserEmployeeVehicle(string searchTerm, string Type)
        {
            var CMP = OS.GetCustomerUserVehicleDriverListFromSearch(searchTerm, Type);

            var users = from user in CMP

                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAccountCode(string searchTerm)
        {
            var CMP = OS.GetAccountCodeListFromSearch(searchTerm, BaseLocationCode);

            var users = from user in CMP

                        select new
                        {
                            id = user.Acccode,
                            text = user.Accdesc

                        };

            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetChequeNoisvalid(string ChequeNo, string bankCode)
        {
            GeneralFuncations GF = new GeneralFuncations();
            int i = 1;
            try
            {
                string sql = "SELECT ChequeNo FROM Webx_Cheque_DCR_Detail WHERE BankCode='" + bankCode + "' AND LocCode='" + BaseLocationCode + "' AND ChequeNo='" + ChequeNo + "' AND Status='A'";
                DataTable DT = GF.GetDataTableFromSP(sql);

                string IsValidChequeNo = DT.Rows[0][0].ToString();
                if (IsValidChequeNo == "" || IsValidChequeNo == null)
                {
                    i = 0;
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        IsValid = i

                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetAccountCodeIsValid", "Json", "Listing", ex.Message);
                return new JsonResult()
                {
                    Data = new { IsValid = 0 }
                };
            }

        }

        public JsonResult GetChequeNoDuplicate(string ChequeNo, string ChequeDate)
        {
            GeneralFuncations GF = new GeneralFuncations();
            int i = 1;
            try
            {
                DataTable DT = OS.GetChequeNoDuplicateorNot(ChequeNo, ChequeDate);

                string IsValidChequeNo = DT.Rows[0][0].ToString();
                if (IsValidChequeNo == "F")
                {
                    i = 0;
                }


                return new JsonResult()
                {
                    Data = new
                    {
                        IsValid = i

                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetAccountCodeIsValid", "Json", "Listing", ex.Message);
                return new JsonResult()
                {
                    Data = new { IsValid = 0 }
                };
            }

        }

        public JsonResult DR_VR_Trn_Halt(string ToTAMT)
        {
            int i = 1;
            try
            {
                string FIN_Start = "";
                FIN_Start = "01 Apr " + BaseYearVal.Split('_')[0];
                DataTable DT = OS.GetDR_VR_Trn_Halt(ToTAMT, BaseLocationCode, FIN_Start, BaseYearVal, BaseCompanyCode);
                string IsValid = DT.Rows[0][0].ToString();
                string Branch = BaseLocationName;
                if (IsValid == "F")
                {
                    i = 0;
                }

                return new JsonResult()
                {
                    Data = new
                    {
                        IsValid = i,
                        Branch = Branch
                    }
                };
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "DR_VR_Trn_Halt", "Json", "Listing", ex.Message);
                return new JsonResult()
                {
                    Data = new { IsValid = 0 }
                };
            }

        }

        #endregion

        #region Cheque Deposit Voucher

        public ActionResult ChqDepositVoucher()
        {
            ChqDepositVoucherCriteriaViewModel chq = new ChqDepositVoucherCriteriaViewModel();
            chq.FromDate = System.DateTime.Now;
            chq.ToDate = System.DateTime.Now;
            return View(chq);
        }

        public ActionResult ChequeDepositVoucher(ChqDepositVoucherCriteriaViewModel chq)
        {
            ChqDepositVoucherViewModel VW = new ChqDepositVoucherViewModel();
            try
            {
                string DATEFROM = "", DATETO = "";

                if (chq.Chqno == "")
                {
                    DATEFROM = "";
                    DATETO = "";
                }
                else
                {
                    chq.Chqno = "All";
                    DATEFROM = chq.FromDate.ToString("dd MMM yyyy");
                    DATETO = chq.ToDate.ToString("dd MMM yyyy");
                }

                string FinYear = BaseYearValFirst.ToString();
                string FinYearNext = Convert.ToString(Convert.ToDouble(FinYear) + 1);
                string fin_year = FinYear.Substring(2, 2).ToString() + "_" + FinYearNext.Substring(2, 2).ToString();

                string datetype = Request.QueryString["datetype"];
                string COLBRCD = BaseLocationCode;
                string COMPANY_CODE = BaseCompanyCode;
                string TableName = "WEBX_ACCTRANS_" + fin_year;

                VW.ChqdetList = MS.ChqDepositeCriteria(chq.Chqno, DATEFROM, DATETO, BaseLocationCode, chq.transdate, COMPANY_CODE, TableName);

                WEBX_chq_det objchqdet = new WEBX_chq_det();
                objchqdet.VoucherDate = System.DateTime.Now;
                VW.chqdet = objchqdet;
            }
            catch (Exception ex)
            {
                //return RedirectToAction("ErrorTransaction", new { Message = "Not Done" });
                ViewBag.StrError = "Error " + ex.Message.ToString().Replace('\n', '_');
                return View("Error");
            }
            return View(VW);

        }

        public ActionResult ChequeDepositVoucherSubmit(ChqDepositVoucherViewModel chq, List<WEBX_chq_det> Bank)
        {

            string Voucherno = "exec usp_next_VoucherNo_Wo_Output '" + BaseLocationCode + "','" + BaseFinYear + "','" + "" + "'";
            DataTable Dt_Name = GF.GetDataTableFromSP(Voucherno);
            Voucherno = Dt_Name.Rows[0]["VNO"].ToString();
            string empcd = BaseUserName;
            string COMPANY_CODE = BaseCompanyCode;

            SqlConnection con;
            con = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
            con.Open();
            SqlTransaction trans;
            trans = con.BeginTransaction();

            DataTable Dt = new DataTable();

            try
            {
                foreach (var item in Bank)
                {
                    if (item.IsChecked)
                    {
                        string chqindex = item.Chqindex.ToString();

                        var Year = BaseYearVal.Split('_');
                        var Year1 = Year[0].ToString();
                        var VoucherDate = chq.chqdet.VoucherDate.ToString("dd MMM yyyy");

                        Dt = MS.InsertChequeTransaction(Voucherno, VoucherDate, chqindex, Year1, chq.chqdet.Narration, empcd, chq.chqdet.Bankaccode, COMPANY_CODE);
                        string Groupcodevalue = Dt_Name.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception EX)
            {
                string ErrorMsg = EX.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                trans.Rollback();
                ViewBag.StrError = "Message:-" + ErrorMsg;
                //Response.Redirect("../../ErrorPage.aspx?PageHead=" + "Transaction Error" + "&ErrorMsg=" + ErrorMsg);
                return View("Error");
            }
            con.Close();

            return RedirectToAction("ChqDepositVoucherDone", new { Voucherno = Voucherno });

        }

        public ActionResult ChqDepositVoucherDone(string Voucherno)
        {
            ViewBag.Voucherno = Voucherno;
            ViewBag.BaseYearVal = BaseYearVal;

            return View();
        }

        #endregion

        public JsonResult GetDeposited_InList(string Type)
        {
            List<SelectListItem> list = new List<SelectListItem> { };
            try
            {
                string SQRY = "";
                if (Type.ToUpper() == "CASH")
                    SQRY = "select acccode as Value,accdesc as Text from webx_acctinfo where acccategory='CASH' order by accdesc asc";
                else if (Type.ToUpper() == "BANK")
                {
                    SQRY = "select acccode as Value,accdesc as Text from webx_acctinfo where ((bkloccode like '%" + BaseLocationCode + "%' or bkloccode='ALL' ) AND  acccategory='BANK') order by accdesc asc";
                }
                else
                {
                    SQRY = "SELECT Value='',Text=''";
                }

                DataTable Dt = GF.GetDataTableFromSP(SQRY);
                List<GetDeposited_InList> List = DataRowToObject.CreateListFromTable<GetDeposited_InList>(Dt);

                return Json(List.OrderBy(c => c.Text), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Error_Logs(ControllerName, "GetDeposited_InList", "Json", "Listing", ex.Message);
                return Json(list);
            }
        }

        public JsonResult GetCustomer_BranchWise(string searchTerm)
        {
            var CMP = OS.GetCustomerListFromSearch_BranchWise(searchTerm, BaseLocationCode);
            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCustUserEmployeeVehicle_BranchWise(string searchTerm, string Type)
        {
            var CMP = OS.GetCustomerUserVehicleDriverListFromSearch_BranchWise(searchTerm, Type, BaseLocationCode);
            var users = from user in CMP
                        select new
                        {
                            id = user.CUSTCD,
                            text = user.CUSTNM
                        };
            return Json(users, JsonRequestBehavior.AllowGet);
        }

        #region Vendor Debit Note

        public ActionResult DebitNoteCriteria()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DebitNoteVoucher(DebitNoteModel objWAD)
        {

            if (CurrFinYear == BaseFinYear || BaseUserName.ToUpper() == "ADMIN")
            {
                DebitCreditVoucherViewModel viewmodel = new DebitCreditVoucherViewModel();
                List<Webx_Acc> ListWAD = new List<Webx_Acc>();
                viewmodel.BackDateAccess = MS.GetVoucherBackDateAccess(BaseUserName, "Debit_Credit_Voucher");
                viewmodel.WA = new Webx_Acc();
                viewmodel.WAD = new Webx_Account_Details();
                try
                {
                    DataTable DT_Tax = OS.GetServiceTaxRate();

                    viewmodel.WAD.VoucherDate = System.DateTime.Today;
                    viewmodel.Date = System.DateTime.Today;
                    viewmodel.WAD.StateCode = objWAD.Cust_State;
                    //viewmodel.WAD.AccountingLocation = BaseLocationCode;
                    viewmodel.WAD.PrepardBy = BaseUserName.ToUpper();
                    viewmodel.WA.Srno = 1;
                    webx_VENDOR_HDR ObjVend = MS.GetVendorObject().Where(c => c.VENDORCODE == objWAD.PartyCode).First();
                    viewmodel.WAD.GSTType = objWAD.GSTType;
                    viewmodel.WAD.GSTPercentage = objWAD.GSTPercentage;
                    viewmodel.isGSTReverse = objWAD.isGSTReverse;
                    viewmodel.Vendor_Type = ObjVend.Vendor_Type;
                    viewmodel.PartyCode = objWAD.PartyCode;
                    viewmodel.PartyName = ObjVend.VENDORNAME;
                    viewmodel.WAD.PANNumber = ObjVend.PAN_NO;
                    viewmodel.GSTIN = ObjVend.PAN_NO;
                    viewmodel.WAD.Type = objWAD.Type;
                    ListWAD.Add(viewmodel.WA);

                    viewmodel.WADList = ListWAD;

                    Webx_Account_Details OBjWAD = new Webx_Account_Details();
                    OBjWAD.SRNo = 1;
                    OBjWAD.Vouchertype = "DEBITNOTE";
                    viewmodel.WAD.SRNo = 1;
                    viewmodel.WAD.Vouchertype = "DEBITNOTE";
                    List<Webx_Account_Details> ListWAD1 = new List<Webx_Account_Details>();
                    ListWAD1.Add(OBjWAD);
                    viewmodel.WADListing = ListWAD1;

                    StaxTDSViewModel STVM = new StaxTDSViewModel();
                    STVM.IsTDSDisplay = true;
                    STVM.IsGSTDisplay = true;
                    STVM.FiledNextAmount = "WAD.NETAMOUNT";
                    STVM.GSTPercentage = viewmodel.WAD.GSTPercentage;
                    STVM.GSTType = viewmodel.WAD.GSTType;

                    viewmodel.OBJstaxtds = STVM;

                }
                catch (Exception ex)
                {
                    ViewBag.StrError = "Message:-" + ex.Message;
                    Error_Logs("DebitCreditVoucher", "DebitCreditVoucher", "View", "AddEdit", ex.Message + "//" + ViewBag.StrError);
                    return View("Error");
                }
                return View(viewmodel);
            }
            else
            {
                return RedirectToAction("URLRedirect", "Home");
            }
        }

        public ActionResult DebitNoteDetails(int id)
        {

            Webx_Acc WMDI = new Webx_Acc();
            WMDI.Srno = id;

            return PartialView("_DebitNote", WMDI);
        }

        [HttpPost]
        public ActionResult DebitNoteSubmit(DebitCreditVoucherViewModel ACVVM, List<Webx_Acc> DNVoucher, List<CygnusChargesHeader> DynamicList, StaxTDSViewModel STVM)
        {
            string Year = BaseFinYear.ToString();
            string fin_year = "";
            string Financial_Year = BaseYearVal;
            fin_year = Financial_Year + "_" + Convert.ToString(Convert.ToDouble("16") + 1).PadLeft(2, '0');
            string EntryBy = BaseUserName;
            string Voucherno = "";

            string Xml_Mst_Details = "<root><DebitNoteMST>";
            Xml_Mst_Details = Xml_Mst_Details + "<Finyear>" + BaseYearValFirst + "</Finyear>";
            Xml_Mst_Details = Xml_Mst_Details + "<Transdate>" + Convert.ToDateTime(ACVVM.WAD.VoucherDate).ToString("dd MMM yyyy") + "</Transdate>";
            Xml_Mst_Details = Xml_Mst_Details + "<Address>" + ACVVM.LocAddress + "</Address>";
            Xml_Mst_Details = Xml_Mst_Details + "<Brcd>" + ACVVM.WAD.AccountingLocation + "</Brcd>";
            Xml_Mst_Details = Xml_Mst_Details + "<Entryby>" + BaseUserName + "</Entryby>";
            Xml_Mst_Details = Xml_Mst_Details + "<pbov_code>" + ACVVM.PartyCode + "</pbov_code>";
            Xml_Mst_Details = Xml_Mst_Details + "<pbov_name>" + ACVVM.PartyName + "</pbov_name>";
            Xml_Mst_Details = Xml_Mst_Details + "<pbov_type>" + ACVVM.Vendor_Type + "</pbov_type>";
            Xml_Mst_Details = Xml_Mst_Details + "<Date>" + Convert.ToDateTime(ACVVM.Date).ToString("dd MMM yyyy") + "</Date>";
            Xml_Mst_Details = Xml_Mst_Details + "<PreparedAtLoc>" + BaseLocationCode + "</PreparedAtLoc>";
            string[] Invoice_Arr = ACVVM.InvoiceNo.Split('~');
            Xml_Mst_Details = Xml_Mst_Details + "<InvoiceNO>" + Invoice_Arr[0] + "</InvoiceNO>";
            Xml_Mst_Details = Xml_Mst_Details + "<Panno>" + ACVVM.WAD.PANNumber + "</Panno>";
            Xml_Mst_Details = Xml_Mst_Details + "<HSNCode>" + ACVVM.HSNCode + "</HSNCode>";
            Xml_Mst_Details = Xml_Mst_Details + "<SACNo>" + ACVVM.SACNO + "</SACNo>";
            Xml_Mst_Details = Xml_Mst_Details + "<Nature_Of_Transaction>" + ACVVM.Nature_Of_Transaction + "</Nature_Of_Transaction>";
            Xml_Mst_Details = Xml_Mst_Details + "<GSTIN>" + ACVVM.GSTIN + "</GSTIN>";
            Xml_Mst_Details = Xml_Mst_Details + "<Payment_Location>" + ACVVM.WAD.AccountingLocation + "</Payment_Location>";
            Xml_Mst_Details = Xml_Mst_Details + "<GSTType>" + ACVVM.WAD.GSTType + "</GSTType>";
            Xml_Mst_Details = Xml_Mst_Details + "<GSTReverse>" + ACVVM.isGSTReverse + "</GSTReverse>";
            Xml_Mst_Details = Xml_Mst_Details + "<Amount>" + STVM.StaxOnAmount + "</Amount>";
            Xml_Mst_Details = Xml_Mst_Details + "<NetAmount>" + ACVVM.WAD.NETAMOUNT + "</NetAmount>";
            Xml_Mst_Details = Xml_Mst_Details + "<VoucherType>D</VoucherType>";
            Xml_Mst_Details = Xml_Mst_Details + "<BRCD1>" + BaseLocationCode + "</BRCD1>";
            Xml_Mst_Details = Xml_Mst_Details + "<state>" + ACVVM.WAD.StateCode + "</state>";
            Xml_Mst_Details = Xml_Mst_Details + "<TDS>" + STVM.TDSRate + "</TDS>";
            Xml_Mst_Details = Xml_Mst_Details + "<TDSAmt>" + STVM.TDSAmount + "</TDSAmt>";
            Xml_Mst_Details = Xml_Mst_Details + "<tdsacccode>" + STVM.TDSAcccode + "</tdsacccode>";
            Xml_Mst_Details = Xml_Mst_Details + "<Remarks>" + ACVVM.Remarks + "</Remarks>";
            Xml_Mst_Details = Xml_Mst_Details + "<CompanyCode>" + BaseCompanyCode + "</CompanyCode>";
            Xml_Mst_Details = Xml_Mst_Details + "<AccCode>" + ACVVM.AccountCode + "</AccCode>";

            Xml_Mst_Details = Xml_Mst_Details + "</DebitNoteMST></root>";

            XmlDocument Xml_Det_Details = new XmlDocument();
            if (DNVoucher != null && DNVoucher.Count > 0)
            {
                List<Webx_Acc> ListWC = new List<Webx_Acc>();

                foreach (var item in DNVoucher)
                {
                    item.VoucherDateStr = item.VoucherDate.ToString("dd MMM yyyy");
                    ListWC.Add(item);
                }

                XmlSerializer xmlSerializerCha = new XmlSerializer(ListWC.GetType());
                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, ListWC);
                    xmlStreamCha.Position = 0;
                    Xml_Det_Details.Load(xmlStreamCha);
                }
            }
            else
            {
                Xml_Det_Details.InnerXml = "<root></root>";
            }

            XmlDocument xmlDocGSTCha = new XmlDocument();
            if (DynamicList != null && DynamicList.Count > 0)
            {
                XmlSerializer xmlSerializerCha = new XmlSerializer(DynamicList.GetType());
                using (MemoryStream xmlStreamCha = new MemoryStream())
                {
                    xmlSerializerCha.Serialize(xmlStreamCha, DynamicList);
                    xmlStreamCha.Position = 0;
                    xmlDocGSTCha.Load(xmlStreamCha);
                }
            }
            else
            {
                xmlDocGSTCha.InnerXml = "<root></root>";
            }

            try
            {
                Voucherno = OS.InsertDebitNoteDetails(Xml_Mst_Details, Xml_Det_Details.InnerXml, xmlDocGSTCha.InnerXml);
            }
            catch (Exception e1)
            {
                string ErrorMsg = e1.Message.ToString();
                ErrorMsg = ErrorMsg.Replace('\n', ' ');
                ViewBag.StrError = ErrorMsg;
                return View("Error");
            }

            return RedirectToAction("DebitNoteDone", new { VoucherNo = Voucherno });
        }

        public ActionResult DebitNoteDone(string VoucherNo)
        {
            ViewBag.VoucherNo = VoucherNo;
            return View();
        }
        #endregion


        public JsonResult CheckManualNoForVouchers(string ManualNo)
        {
            try
            {
                string Count = OS.CheckManualNoForVouchers(ManualNo);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count
                        ,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }

        public JsonResult CheckReferenceNoForVouchers(string ReferenceNo)
        {
            try
            {
                string Count = OS.CheckReferenceNoForVouchers(ReferenceNo);

                return new JsonResult()
                {
                    Data = new
                    {
                        Count = Count
                        ,
                    }
                };
            }
            catch (Exception)
            {
                return Json(0);
            }
        }
    }
}
