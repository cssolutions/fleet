﻿var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        jQuery.validator.addMethod("FutureDateValidate", function (value, element) {

            var PermitDate = value;

            var date = $('#datepicker').datepicker('getDate');



            var now = new Date();
            var myDate = new Date(ChangeInJQDate(PermitDate));
            //alert("myDate " + new Date(myDateFormatter(PermitDate)) + " now " + now + " value  " + new Date(value));
            return this.optional(element) || new Date(myDateFormatter(PermitDate)) >= now;

            // else alert('Das passt nicht!' + mD +  '   ' + nowdate);

        });

        $.validator.addMethod("numericWithout0", function (value, element) {
            return this.optional(element) || /^[1-9]+$/i.test(value);
        }, "Only numbers between 1 to 9 allowed");

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input


            rules: {

                'CTVD.PermitDate': {
                    FutureDateValidate: true,

                },
                'CTVD.InsuranceDate': {
                    FutureDateValidate: true,

                },
                'CTVD.FitnessDate': {
                    FutureDateValidate: true,

                },
                'CTVD.Driver1LicenceValDate': {
                    FutureDateValidate: true,

                },
                'CTVD.Driver2LicenceValDate': {
                    FutureDateValidate: true,

                },
                'CTFD.Loadingcharge': {
                    number: true,
                    min: 1,
                },


                //'CTH.OpenKM': {
                //    required: true,
                //    min: 1,
                //    number: true,
                //},

                'CTFD.ContractAmount': {
                    required: true,
                    min: function () {
                        if ($("#CTH_VendorType").val() == "05")
                            return 0;
                        else
                            return 1;
                    },
                    number: true,
                },
                'CTFD.AdvanceAmount': {
                    required: true,
                    min: 0,
                    max: function () {
                        return parseFloat($('#CTFD_NetAmount').val());
                    },
                    number: true,
                },
                'CTFD.BalanceAmount': {
                    required: true,
                    number: true,
                },
                'CTVD.Driver1MobileNo': {
                    number: true
                },
                'CTVD.Driver2MobileNo': {
                    number: true
                },
                'CTH.SUPPLYERMOBNO': {
                    minlength: 10,
                    maxlength: 10,
                    number: true
                },
                'CTH.CloseKM': {
                    number: true,
                    min: function () {

                        return parseFloat($("#CTH_OpenKM").val()) + 1;
                    }
                },

                'CTH.TotalManifest': {
                    number: true,
                    min: function () {

                        var MFTOT = 0;


                        if ($('#CTH_IsEmpty').is(':checked')) {
                            MFTOT = 0
                        }
                        else
                            MFTOT = 1;

                        return MFTOT;
                    }
                },
                'CTH.TotalDockets': {
                    number: true,
                    min: 1,
                },
                'CTH.SealNo': {
                    numericWithout0: true,
                },


            },// 'CTFD.Loadingcharge': {

            messages: {
                'CTFD.Loadingcharge': {
                    min: jQuery.format("Please Enter Rate or Set Labour Contract !!!!")
                },
                'CTH.TotalManifest': {
                    min: jQuery.format("Please Select at Least One Manifest")
                },
                'CTH.TotalDockets': {
                    min: jQuery.format("Please Select at Least One Record")
                },
                'CTVD.PermitDate': {
                    FutureDateValidate: jQuery.format("Permit Date is Expired"),

                },
                'CTVD.InsuranceDate': {
                    FutureDateValidate: jQuery.format("Insurance Date is Expired"),

                },
                'CTVD.FitnessDate': {
                    FutureDateValidate: jQuery.format("Fitness Date is Expired"),

                },
                'CTVD.Driver1LicenceValDate': {
                    FutureDateValidate: jQuery.format("Driver Licence Date is Expired"),

                },
                'CTVD.Driver2LicenceValDate': {
                    FutureDateValidate: jQuery.format("Driver Licence Date is Expired"),

                },
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var VendorTypeChange = function (DomainName) {

        $(".clsMKT").hide();
        $(".clsOWN").hide();
        $(".clsOWN1").hide();

        $(".clsMKT1").hide();

        $('#CTH_VendorName').attr("readonly", true);
        $('#').attr("readonly", true);
        $('#CTH_VendorType').change(function () {

            App.blockUI({ boxed: true });

            var StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $(this).val();
            FillDropDownfromOther1("CTH_VendorCode", StrURL, "Vendor");
            $("#CTH_VendorCode").select2("data", null);

            StrURL = DomainName + "/Operation/GetVehicleTypeFromVehicle?VehicleNo=O";
            FillDropDownfromOther("CTVD_VehicleType", StrURL, "Vehicle Type");


            StrURL = DomainName + "/Operation/GetFTLTypeFromVehicle?VehicleNo=O";
            FillDropDownfromOther("CTVD_FTLType", StrURL, "FTL Type");

            if ($(this).val() == "XX") {

                $(".clsMKT").show();
                $(".clsOWN").hide();
                $(".clsOWN1").hide();

                $(".clsMKT1").show();

                $('#CTH_VendorName').attr("readonly", false);

                $("#CTH_VehicleNO").select2('destroy');
                $("#CTH_VendorCode").select2("data", null);
                $('#CTH_LorryOwnerPanNo').attr("readonly", false);
                $("#CTH_VendorCode").select2("data", { id: "9888", text: "Market" });

                $('#CTH_VendorCode').attr("disabled", true);
                $('#CTH_VendorName').val("");

                $("#CTVD_VehicleType").select2("data", null);
                $("#CTVD_FTLType").select2("data", null);
                $('.clsVehicle').val("");

                $('#CTFD_ContractAmount').attr("readonly", false);


                $('.clsVehicle').attr("readonly", false);
                $('.clsVehicle1').show();
            }
            else {
                $(".clsMKT").hide();
                $(".clsOWN").show();

                $(".clsOWN1").hide();
                $(".clsMKT1").show();

                if ($(this).val() == "05") {
                    $(".clsOWN1").show();
                    $(".clsMKT1").hide();
                }

                $('#CTH_VendorCode').attr("disabled", false);
                $('#CTH_VendorName').attr("readonly", true);

                $('#CTH_LorryOwnerPanNo').attr("readonly", true);

                //  $("#CTH_VendorCode").select2("data", null);

                $("#CTH_VendorCode").select2({
                    placeholder: "Select an option",
                    allowClear: true
                });
                //   alert("Hello   1");
                //  FillDropDownfromOther("CTH_VendorCode", StrURL, "Vendors");
            }
            // App.unblockUI();
        });


        $('#CTH_VendorCode').change(function () {
            App.blockUI({ boxed: true });

            if ($('#CTH_VendorType').val() != "XX" && $('#CTH_VendorCode').val() != "") {

                var data = $('#CTH_VendorCode').select2('data');
                $('#CTH_VendorName').val($("#CTH_VendorCode option:selected").text().replace(": " + $('#CTH_VendorCode').val(), ""));

                var StrURL = DomainName + "/Operation/GetVehicleFromVendor?VendorType=" + $('#CTH_VendorType').val() + "&Vendor=" + $(this).val();
                FillDropDownfromOther("CTH_VehicleNO", StrURL, "Vehicles");

                setTimeout(
                    function () {
                        // alert($("#CTH_VehicleNO").val());
                        if ($("#CTH_VehicleNO").val() == "O") {
                            $(".clsOTVehicle").show();
                            $('.clsVehicle').attr("readonly", false);

                            $("#CTVD_VehicleType").select2("data", null);
                            $("#CTVD_FTLType").select2("data", null);
                            $('.clsVehicle').val("");
                            $('.clsVehicle1').show();
                        }
                    }, 500);

                var StrURL = DomainName + "/Operation/GetPanNoFromVendor?Vendor=" + $(this).val();
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: StrURL,
                    dataType: 'json',
                    data: {},
                    success: function (data) {
                        $('#CTH_LorryOwnerPanNo').val(data.PANNO);
                    },
                    error: function () {
                        alert("error");
                    }
                });
            }
            GetContractAmount(DomainName);
            //App.unblockUI();
        });

        $('#CTVD_FTLType').change(function () {
            App.blockUI({ boxed: true });
            GetContractAmount(DomainName);
            App.unblockUI();
        });

        $('#CTH_VehicleNO').change(function () {
            App.blockUI({ boxed: true });

            if ($('#CTH_VendorType').val() != "XX" && $('#CTH_VendorCode').val() != "") {


                var StrURL = DomainName + "/Operation/GetTripsheetFromVehicle?VehicleNo=" + $(this).val();
                FillDropDownfromOther("CTH_TripSheetNo", StrURL);

                //StrURL = DomainName + "/Operation/GetVehicleTypeFromVehicle?VehicleNo=" + $(this).val();
                //FillDropDownfromOther("CTVD_VehicleType", StrURL, "Vehicle Type");


                //StrURL = DomainName + "/Operation/GetFTLTypeFromVehicle?VehicleNo=" + $(this).val();
                //FillDropDownfromOther("CTVD_FTLType", StrURL, "FTL Type");


                $(".clsOTVehicle").hide();
                if ($(this).val() == "O") {

                    $(".clsOTVehicle").show();
                    $('.clsVehicle').attr("readonly", false);

                    $("#CTVD_VehicleType").select2("data", null);
                    $("#CTVD_FTLType").select2("data", null);
                    $('.clsVehicle').val("");
                    $('.clsVehicle1').show();

                } else {
                    var StrURL = DomainName + "/Operation/GetVehicleDetails?VehicleNo=" + $(this).val();
                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: StrURL,
                        dataType: 'json',
                        data: {},
                        success: function (data) {

                            $('#CTVD_RegistrationDate').val(data.RegistrationDt);
                            $('#CTVD_ENGINENO').val(data.EngineNo);
                            $('#CTVD_CHASISNO').val(data.ChasisNo);
                            $('#CTVD_RCBOOKNO').val(data.RCBookNo);
                            $('#CTVD_PermitDate').val(data.VEHPRMDT);
                            $('#CTVD_InsuranceDate').val(data.InsuranceValDt);
                            $('#CTVD_FitnessDate').val(data.FitnessValDt);
                            $('#CTH_VehicleCapacity').val(parseFloat(data.Capacity) * 1000);
                            $('#CTH_OpenKM').val(data.StartKM);

                            $('.clsVehicle').attr("readonly", "true");
                            $('.clsVehicle1').hide();

                            //$(function () {
                            //    $(".ftdate-picker").datepicker('disable');

                            //    $(".clsftdate").removeClass("ftdate-picker");
                            //    $(".clsftdate").removeClass("date");
                            //});

                            //$(".ftdate-picker").datepicker("option", "disabled", true);
                            $(".ftdate-picker input ,  .bkdate-picker input").prop('disabled', true);

                            $("#CTVD_VehicleType").select2("data", { id: data.Vehicle_Type, text: data.Type_Name });
                            $("#CTVD_FTLType").select2("data", { id: data.FTLTYPe, text: data.FTLTYPE_NAME });
                        },
                        error: function () {
                            alert("error");
                        }
                    });

                    GetContractAmount(DomainName);
                }
            }

            GetContractAmount(DomainName);

            ClaculateVehicleCapcity(DomainName);
        });

        $('#CTH_TripSheetNo').change(function () {
            App.blockUI({ boxed: true });

            var StrURL = DomainName + "/Operation/GetDriverDetailsFromTripsheet?TripsheetNo=" + $(this).val();
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL,
                dataType: 'json',
                data: {},
                success: function (data) {

                    $('#CTVD_Driver1Name').val(data.Driver1);
                    $('#CTVD_Driver1MobileNo').val(data.MOB1);
                    $('#CTVD_Driver1Licence').val(data.Licno1);
                    $('#CTVD_Driver1RTONo').val(data.RTO1);
                    $('#CTVD_Driver1LicenceValDate').val(data.Validity_Date1);

                    $('#CTVD_Driver2Name').val(data.Driver2);
                    $('#CTVD_Driver2MobileNo').val(data.MOB2);
                    $('#CTVD_Driver2Licence').val(data.Licno2);
                    $('#CTVD_Driver2RTONo').val(data.RTO2);
                    $('#CTVD_Driver2LicenceValDate').val(data.Validity_Date2);
                    App.unblockUI();
                },
                error: function () {
                    alert("error");
                    App.unblockUI();
                }
            });
        });
    }

    var GetRoutes = function (DomainName, RouteType, IsEmpty) {
        //  alert(DomainName + " " + RouteType + " " + IsEmpty);
        var StrURL = DomainName + "/Operation/GetRoutesFromRouteType?RouteType=" + RouteType + "&IsEmpty=" + IsEmpty;
        // FillDropDownfromOtherWithGroup("CTH_RouteCode", StrURL, "Routes");
        FillDropDownfromOther("CTH_RouteCode", StrURL, "Routes");


    }


    var GetRouteSchedule = function (DomainName, RouteCode, DatDay, DatTime) {
        // alert((DomainName, RouteCode, DatDay, DatTime))
        App.blockUI({ boxed: true });
        var StrURL = DomainName + "/Operation/GetRouteSchedule?RouteCode=" + RouteCode + "&DatDay=" + DatDay + "&DatTime=" + DatTime;

        $("#divSch").empty();
        $.get(StrURL, function (data) {

            for (i = 0; i < data.length; i++) {
                // $("#" + DDLName).append('<option value="' + data[i].Value + '">' + data[i].Text + '</option>');

                //   alert(data[i].Schtype);

                $('#CTH_ScheduleType').val(data[i].Schtype);
                $('#CTH_ScheduleNo').val(data[i].Schno);

                var StrURL1 = DomainName + "/Operation/Get_RouteSchedule?Schno=" + data[i].Schno + "&srno=" + data[i].srno + "&SchTime=" + data[i].SchTime + "&Day_name=" + data[i].Day_name + "&SchDT=" + data[i].SchDT;
                //  alert(StrURL1)
                $.ajax({
                    url: StrURL1,
                    cache: false,
                    data: {},
                    type: 'POST',
                    success: function (data1) {
                        $("#divSch").append(data1);
                        App.unblockUI();
                        $("input:checkbox, input:radio").uniform();
                    },
                    error: function (req, status, error) {
                        alert("error--" + req + "---" + status + "---" + error);
                        App.unblockUI();
                    }
                });
            }
            
        }, "json");
    }

    var GetFlights = function (DomainName) {
        App.blockUI({ boxed: true });
        var AirLine = $("#CTH_AirLine").val();
        var Airport = $("#CTH_AirportCode").val();

        var StrURL = DomainName + "/Operation/GetFlights?AirLine=" + AirLine + "&Airport=" + Airport;
        FillDropDownfromOther("CTH_FlightCode", StrURL, "Route Mode");

    }

    var GetFlightSchTime = function (DomainName) {
        App.blockUI({ boxed: true });
        var Flight = $("#CTH_FlightCode").val();
        var Airport = $("#CTH_AirportCode").val();

        var StrURL = DomainName + "/Operation/GetFlightSchTime?Flight=" + Flight + "&Airport=" + Airport;

        $.ajax({
            url: StrURL,
            cache: false,
            data: {},
            type: 'POST',
            success: function (data) {
                $("#CTH_FlightScheduleTime").val(data.SchTime);
                App.unblockUI();
            },
            error: function (req, status, error) {
                alert("error--" + req + "---" + status + "---" + error);
                App.unblockUI();
            }
        });
    }


    var RouteModeChange = function (DomainName) {
        $('.clsSch').live('click', function () {
            $("#CTH_ScheduleTime").val($('input[class=clsSch]:checked').val())
            var radioid = $('input[class=clsSch]:checked').attr("id");
            // alert(radioid);
            var ID = radioid.replace("txtRTSch", "");

            var SchDt = $("#THCCharge_" + ID + "__SchDT").val() + "  " + $("#CTH_ScheduleTime").val();

            $("#CTH_ScheduleDeptDate").val(SchDt);

            $("#CTH_ScheduleDay").val($("#THCCharge_" + ID + "__Day_name").val());
        });


        var DatTime = $('#ActualDeptTime').val();

        $('#CTH_ActualDeptDate').val(myDateFormatter($('#CTH_ActualDeptDate').val()) + " " + DatTime);
        $('#CTH_ScheduleDeptDate').val(myDateFormatter($('#CTH_ActualDeptDate').val()) + " " + DatTime);

        $('#ActualDeptTime').change(function () {

            var RouteCode = $('#CTH_RouteCode').val();
            var DatDay = $('#CTH_THCDate').val();
            var DatTime = $('#ActualDeptTime').val();

            $('#CTH_ActualDeptDate').val(myDateFormatter($('#CTH_ActualDeptDate').val()) + " " + DatTime);

            GetRouteSchedule(DomainName, RouteCode, DatDay, DatTime);
        });

        $('#CTH_RouteCode').change(function () {
            App.blockUI({ boxed: true });

            var RouteCode = $(this).val();
            var DatDay = $('#CTH_THCDate').val();
            var DatTime = $('#ActualDeptTime').val();

            GetRouteSchedule(DomainName, RouteCode, DatDay, DatTime);

            if ($('#CTH_RouteCode').val() != "" && $('#CTH_IsEmpty').is(':checked') == false) {

                var data = $('#CTH_RouteCode').select2('data');

                var StrURL = DomainName + "/Operation/GetMFList?Route=" + data.text + "&IsBCProcess=N";

                $.ajax({
                    url: StrURL,
                    cache: false,
                    data: {},
                    type: 'POST',
                    success: function (data) {
                        App.unblockUI();
                        $("#MFBody").html(data);
                        $("input:checkbox, input:radio").uniform();
                    },
                    error: function (req, status, error) {
                        alert("error--" + req + "---" + status + "---" + error);
                        App.unblockUI();
                    }
                });
            }

            // alert($("#CTH_RouteCode option:selected").text())

            $('#CTH_RouteName').val($("#CTH_RouteCode option:selected").text().replace(":" + $('#CTH_RouteCode').val(), ""))

            GetContractAmount(DomainName);
           // App.unblockUI();
        });




        $('#CTH_IsEmpty').click(function () {
            
            App.blockUI({ boxed: true });
            var IsEmpty = "N";
            if ($(this).is(':checked')) {
                IsEmpty = "Y";
            }

            var RouteType = $('#CTH_RouteType').val();
            GetRoutes(DomainName, RouteType, IsEmpty);
            App.unblockUI();
        });

        $('#CTH_AirportCode').change(function () {
            GetFlights(DomainName);
            GetFlightSchTime(DomainName);
        });

        $('#CTH_FlightCode').change(function () {
            GetFlightSchTime(DomainName);
        });

        $('#CTH_AirLine').change(function () {
            GetFlights(DomainName);
        });

        $('#CTH_RouteType').change(function () {
            App.blockUI({ boxed: true });
            $("#divAir").hide();
            $("#divRail").hide();
            $("#divRoad").hide();
            //alert($(this).val())

            if ($(this).val() == "A") {
                $("#divAir").show();

                var StrURL = DomainName + "/Operation/GetAirport";
                FillDropDownfromOtherSelectFirst("CTH_AirportCode", StrURL, "Route Mode");

                $("#CTH_OpenKM").removeClass("required");
                $("#CTH_OpenKM").attr("min", "0");
            }
            else if ($(this).val() == "S") {
                $("#divRoad").show();
                $("#CTH_OpenKM").addClass("required");
                $("#CTH_OpenKM").attr("min", "1");
            }
            else if ($(this).val() == "R") {
                $("#divRail").show();
                $("#CTH_OpenKM").removeClass("required");
                $("#CTH_OpenKM").attr("min", "0");
            }
            var StrURL = DomainName + "/Operation/GetVendorType/" + $(this).val();
            FillDropDownfromOther("CTH_VendorType", StrURL, "Route Mode");

            var IsEmpty = "N";

            if ($("#CTH_IsEmpty").is(':checked'))
                IsEmpty = "Y";

            var RouteType = $(this).val();
            GetRoutes(DomainName, RouteType, IsEmpty);
            GetContractAmount(DomainName);
            //App.unblockUI();
        });

    }

    function GetCodeSelected(DDLName, StrURL) {

        $("#" + DDLName).select2('destroy');

        $("#" + DDLName).select2({
            placeholder: "Search ",

            allowClear: true,
            ajax: {
                url: StrURL,
                dataType: 'json',
                data: function (term) {
                    return {

                    };
                },
                results: function (data) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //  alert("Hello");
                    // alert(data);
                    //    alert(data.CMP);
                    return {
                        results: data
                    };
                }
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: text };
                callback(data);
            },
            formatResult: function (item) { return ('<div><b>' + item.id + ' </b>- ' + item.text + '</div>'); },
            formatSelection: function (item) { return (item.id); },
            escapeMarkup: function (m) { return m; },
            dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller

        });

    }

    function GetCodeDisplaynSelected(DDLName, StrURL) {

        $("#" + DDLName).select2('destroy');

        $("#" + DDLName).select2({
            placeholder: "Search ",

            allowClear: true,
            ajax: {
                url: StrURL,
                dataType: 'json',
                data: function (term) {
                    return {

                    };
                },
                results: function (data) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //  alert("Hello");
                    // alert(data);
                    //    alert(data.CMP);
                    return {
                        results: data
                    };
                }
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: text };
                callback(data);
            },
            formatResult: function (item) { return ('<div> ' + item.text + '</div>'); },
            formatSelection: function (item) { return (item.id); },
            escapeMarkup: function (m) { return m; },
            dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller

        });

    }

    function GetTextDisplaynSelected(DDLName, StrURL) {

        $("#" + DDLName).select2('destroy');

        $("#" + DDLName).select2({
            placeholder: "Search ",

            allowClear: true,
            ajax: {
                url: StrURL,
                dataType: 'json',
                data: function (term) {
                    return {

                    };
                },
                results: function (data) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    //  alert("Hello");
                    // alert(data);
                    //    alert(data.CMP);
                    return {
                        results: data
                    };
                }
            },
            initSelection: function (item, callback) {
                var id = item.val();
                var text = item.data('option');
                var data = { id: id, text: text };
                callback(data);
            },
            formatResult: function (item) { return ('<div> ' + item.text + '</div>'); },
            formatSelection: function (item) { return (item.id); },
            escapeMarkup: function (m) { return m; },
            dropdownCssClass: "bigdrop" // apply css that makes the dropdown taller

        });

    }

    function FillDropDownfromOtherWithGroup(DDLName, StrURL, DllType) {
        $("#" + DDLName).get(0).options.length = 0;
        $("#" + DDLName).get(0).options[0] = new Option("Loading names", "-1");
        $("#" + DDLName).empty();
        $("#" + DDLName).find("option").remove();
        var select = $("#" + DDLName);
        select.empty();
        // alert(DDLName + " " + StrURL + " " + DllType)

        $.get(StrURL, function (data) {

            var j = 0;

            for (i = 0; i < data.length; i++) {

                if (data[i].Value == "") {
                    if (j > 0)
                        $("#" + DDLName).append('</optgroup>');

                    $("#" + DDLName).append('<optgroup label="' + data[i].Text + '">');
                    j++;
                }
                else {
                    $("#" + DDLName).append('<option value="' + data[i].Value + '">' + data[i].Text + '</option>');
                }
                $("#" + DDLName).trigger("liszt:updated");
            }

            $("#" + DDLName).append('</optgroup>');

            $("#" + DDLName).prepend('<option value=""></option>');
            App.unblockUI();
        }, "json");
        $("#" + DDLName).trigger("liszt:updated");
        $("#" + DDLName).val($("#target option:first").val());
    }

    var SelectCity = function (DomainName) {
        $(".clsCity").hide();
        $('#CTH_IsCityEnabled').click(function () {
            if (!$(this).is(':checked')) {
                $(".clsCity").hide();
            }
            else {
                $(".clsCity").show();
                var StrURL = DomainName + "/Master/GetAllCityJson";
                DLLTextSelected("CTH_FROMCITY", StrURL, false, 1, "City");
                StrURL = DomainName + "/Master/GetAllCityJson";
                DLLTextSelected("CTH_TOCITY", StrURL, false, 1, "City");
            }
        });
    }

    var Step1Click = function (DomainName) {


        $('#btnStep1').click(function () {

            var form = $('#form_sample');
            if (form.valid() == false) {
                return false;
            }

            if ($("#CTH_THCType").val() == "1")
                $(".clsMF").show();
            else
                $(".clsGC").show();

            $(this).attr("dissbled", true);

            $('#divStep1 :button').attr('disabled', true);

            $('#divStep1 :input').attr('disabled', true);

        });
    }

    var SubmitClick = function (DomainName) {

        $('#btnSubmit').click(function () {

            var form = $('#form_sample');




            //if ($("#CTFD_LoadedBy").val() == "M")
            //{

            //    if ($("#CTFD_VendName").val() == '')
            //    {
            //        alert("Please Enter Vendor Name");
            //        $("#CTFD_VendName").focus();
            //        return false;
            //    }
            //}

            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");

            App.blockUI({ boxed: true });
            $(".ftdate-picker input ,  .bkdate-picker input").prop('disabled', false);
            document.forms["form_sample"].submit();


            //  form.submit();

            //  alert("Done...11");


            //if ($("#CTH_THCType").val() == "1")
            //    $(".clsMF").show();
            //else
            //    $(".clsGC").show();

            //$(this).attr("dissbled", true);

            //$('#divStep1 :button').attr('disabled', true);

            //$('#divStep1 :input').attr('disabled', true);

        });
    }

    var AmountChange = function () {
        $(".clsAmount").change(function (e) {
            ClaculateSubTotal();
        });
    }

    var ClaculateSubTotal = function () {
        var ContractAmount = $("#CTFD_ContractAmount").val();
        var Freight = $("#CTFD_NetAmount").val();

        var TotalCharges = 0;
        $(".ClsChrges").each(function () {
            var ChrgId = $(this).attr("id").replace("THCCharge_", "").replace("__ChargeAmount", "");
            var Operatorid = $(this).attr("id").replace("__ChargeAmount", "__Operator");

            if ($("#" + Operatorid).val() == "+") {
                TotalCharges = parseFloat(TotalCharges) + parseFloat($(this).val());
            }
            else {
                TotalCharges = parseFloat(TotalCharges) - parseFloat($(this).val());
            }
            // alert(TotalActWt.val());
        });

        var Advance_Amount = $("#CTFD_AdvanceAmount").val();
        //  alert(Freight + " " + TotalCharges + " " + Advance_Amount + " " + ContractAmount);
        Freight = parseFloat(TotalCharges) + parseFloat(ContractAmount);

        $("#CTFD_NetAmount").val(parseFloat(TotalCharges) + parseFloat(ContractAmount));

        if (Freight != "") {

            Freight = parseFloat(Freight);
            Advance_Amount = parseFloat(Advance_Amount);
            //  alert(Freight + " " + TotalCharges + " " + Advance_Amount);

            if (Freight >= Advance_Amount) {
                var Balance_Amount = Freight - Advance_Amount;
                $("#CTFD_BalanceAmount").val(Balance_Amount);
                return true
            }
            else {
                //  alert("Advance Amount should not be more than or equal Net Amount");
                // $("#txtAdvance_Amount").val() = 0;
                //$("#CTFD_AdvanceAmount").focus();
                //$("#CTFD_AdvanceAmount").val("0");
                //$("#CTFD_AdvanceAmount").rules("add", function () {
                //    max: $('#CTFD_NetAmount').val();
                //});
            }
        }





    }


    var SelectAllCheckBox = function (DomainName) {

        $("#IsMFAllEnabled").live('change', function () {



            var checked = jQuery(this).is(":checked");
            $(".clsMFCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            ClaculateVehicleCapcity(DomainName);
        });


        $(".clsMFCheck").live('change', function () {

            ClaculateVehicleCapcity(DomainName);
        });


        $("#IsAllEnabled").live('change', function () {

            var checked = jQuery(this).is(":checked");

            $(".clsGCCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            ClaculateVehicleCapcity(DomainName);
        });


        $(".clsGCCheck").live('change', function () {

            ClaculateVehicleCapcity(DomainName);
        });

    }


    var ClaculateVehicleCapcity = function (DomainName) {


        var totWT = 0;
        // alert("Hello");
        var HamaliChrge = 0;
        if ($("#CTH_THCType").val() == "1") {
            var totMF = 0;
            $(".clsMFCheck").each(function () {

                var checked = $(this).is(":checked");
                var LoadWtId = $(this).attr("id").replace("IsEnabled", "TOT_LOAD_ACTWT");

                var LoadWT = $("#" + LoadWtId).val();

                if (checked) {
                    totWT = totWT + parseFloat(LoadWT);
                    totMF = totMF + 1;
                }

            });

            $("#CTH_TotalManifest").val(totMF);

        }
        else {
            var totGC = 0;
            $(".clsGCCheck").each(function () {
                var checked = $(this).is(":checked");
                //var LoadWtId = $(this).attr("id").replace("IsEnabled", "PKGSNO");
                var LoadWtId = $(this).attr("id").replace("IsEnabled", "ArrWeightQty");

                var ID = $(this).attr('id').replace('PRSDRSDocketList_', '').replace('__PKGSNO', '').replace('__IsEnabled', '')
                var Charge = 0;
                if ($("#CTFD_LoadedRateType").val() == "1") {
                    //Charge = $("#PRSDRSDocketList_" + ID + "___ACTUWT").val();
                    if ($("#PRSDRSDocketList_" + ID + "__ArrWeightQty").val() == null) {
                        Charge = $("#PRSDRSDocketList_" + ID + "__ACTUWT").val();
                    }
                    else {
                        Charge = $("#PRSDRSDocketList_" + ID + "__ArrWeightQty").val();
                    }
                }
                else if ($("#CTFD_LoadedRateType").val() == "3") {
                    // Charge = $("#PRSDRSDocketList_" + ID + "___PKGSNO").val();

                    if ($("#PRSDRSDocketList_" + ID + "__ArrPkgQty").val() == null) {
                        Charge = $("#PRSDRSDocketList_" + ID + "__PKGSNO").val();
                    }
                    else {
                        Charge = $("#PRSDRSDocketList_" + ID + "__ArrPkgQty").val();
                    }
                }
                var LoadWT = $("#" + LoadWtId).val();

                if (checked) {

                    totWT = totWT + parseFloat(LoadWT);
                    totGC = totGC + 1;
                    HamaliChrge = parseFloat(HamaliChrge) + parseFloat(Charge)
                }
            });
            if ($("#CTFD_Rate").val() != "" || $("#CTFD_Rate").val() > 0) {
                $("#CTFD_Loadingcharge").val(parseFloat(HamaliChrge * parseFloat($("#CTFD_Rate").val())).toFixed(2));
            }
            $("#CTH_TotalDockets").val(totGC);
        }

        $("#CTH_WtLoaded").val(totWT);


        var vehCap = $("#CTH_VehicleCapacity").val();

        if (vehCap == "")
            vehCap = 0;

        var CpUTI = 0;

        if (totWT > vehCap) {

            $('#CTH_IsOverLoad').attr("checked", true);
            $('#CTH_IsOverLoad').parents('span').addClass("checked");
            $('#CTH_OverLoadReason').attr('disabled', false);
        }
        else {
            $('#CTH_IsOverLoad').attr("checked", false);
            $('#CTH_IsOverLoad').parents('span').removeClass("checked");
            $('#CTH_OverLoadReason').attr('disabled', true);
        }

        if (parseFloat(vehCap) > 0)
            CpUTI = roundNumber(((totWT * 100) / vehCap), 2);

        $("#CTH_VehicleCapacityUti").val(CpUTI);



        GetContractAmount(DomainName);
    }

    var GetContractAmount = function (DomainName) {

        var THCTYPE = $("#CTH_THCType").val();

        var TotalWeight = $("#CTH_WtLoaded").val();

        if (TotalWeight == "") {
            TotalWeight = 0;
        }

        var WeightAdjust = 0;//$("#CTH_THCType").val();
        var IsAllowAdhoc = false;
        var IsAdhoc = false;
        var IsAllowTAM = false;
        var VendorCode = $("#CTH_VendorCode").val();
        var RouteCode = $("#CTH_RouteCode").val();

        var RouteMODE = $("#CTH_RouteType").val();
        var FTL_Type = $("#CTVD_FTLType").val();
        var Vehicle = $("#CTH_VehicleNO").val();
        var From_City = $("#CTH_FROMCITY").val();
        var To_City = $("#CTH_TOCITY").val();

        if (VendorCode != "") {
            var StrURL = DomainName + "/Operation/GetContractAmount?THCTYPE=" + THCTYPE + "&TotalWeight=" + TotalWeight + "&WeightAdjust=" + WeightAdjust + "&IsAllowAdhoc=" + IsAllowAdhoc + "&IsAdhoc=" + IsAdhoc + "&IsAllowTAM=" + IsAllowTAM + "&VendorCode=" + VendorCode + "&RouteCode=" + RouteCode + "&RouteMODE=" + RouteMODE + "&FTL_Type=" + FTL_Type + "&Vehicle=" + Vehicle + "&From_City=" + From_City + "&To_City=" + To_City;
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL,
                dataType: 'json',
                data: {},
                success: function (data) {
                    //alert(data.ContractAmount)
                    $('#CTFD_StandardContractAmount').val(data.StandardContractAmount);
                    $('#CTFD_ContractAmount').val(data.ContractAmount);
                    if (!data.IsContractAmtEnabled) {
                        if ($('#CTH_VendorType').val() == "05")
                            $('#CTFD_ContractAmount').attr("readonly", true);
                    }
                    else
                        $('#CTFD_ContractAmount').attr("readonly", false);


                    ClaculateSubTotal();
                },
                error: function () {
                    alert("error " + req + "   " + status + "   " + error);
                }
            });

        }

    }



    return {
        //main function to initiate the module
        init: function (DomainName) {


            if ($("#CTH_THCType").val() == "1") {
                $(".clsTHC").show();
                $(".clsMF").show();
                $(".clsGC").hide();
                $(".clsDRS").hide();
                $(".clsPRS").hide();
            }
            else if ($("#CTH_THCType").val() == "3") {
                $(".clsDRS").show();
                $(".clsTHC").hide();
                $(".clsGC").show();
                $(".clsMF").hide();
                $(".clsPRS").hide();
                $("#divRoad").show();
            }
            else if ($("#CTH_THCType").val() == "2") {
                $(".clsDRS").hide();
                $(".clsTHC").hide();
                $(".clsGC").show();
                $(".clsMF").hide();
                $(".clsPRS").show();
                $("#divRoad").show();
            }
            else {
                $(".clsDRS").hide();
                $(".clsTHC").hide();
                $(".clsGC").show();
                $(".clsMF").hide();
            }

            ControlDate();

            Formvalidate(DomainName);
            VendorTypeChange(DomainName);
            SelectCity(DomainName);
            RouteModeChange(DomainName);
            Step1Click(DomainName);
            SubmitClick(DomainName);
            SelectAllCheckBox(DomainName);
            AmountChange();
            handleDatePicker();
            FutureDatePicker();
            handleDateTimePicker(new Date(), new Date());

            $('#CTH_OverLoadReason').attr('disabled', true);
            $('#CTH_IsOverLoad').attr('disabled', true);


            $("#CTH_LorryOwnerPanNo").inputmask("aaaaa9999a", { clearMaskOnLostFocus: true });

            $('#ActualDeptTime').clockface({
                format: 'HH:mm',
                trigger: 'manual'
            });


            $('#ActualDeptTime_toggle').click(function (e) {
                e.stopPropagation();
                $('#ActualDeptTime').clockface('toggle');
            });


            if ($("#CTH_THCType").val() == "3") {
                var StrURL = DomainName + "/Operation/GetDeliveryZone";
                FillDropDownfromOther("CTH_DeliveryZone", StrURL, "Delivery Zone");
            }

            var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/RTMD";
            FillDropDownfromOther("CTH_RouteType", StrURL, "Route Mode");


            var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/LTDEP";
            FillDropDownfromOther("CTH_LateDepaturereason", StrURL, "Late Depature Reason");

            var StrURL = DomainName + "/Operation/GetLocationList";
            FillDropDownfromOther1("CTFD_AdvanceLocation", StrURL, "Advance Location");

            var StrURL = DomainName + "/Operation/GetLocationList";
            FillDropDownfromOther1("CTFD_BalanceLocation", StrURL, "Balance Location");

            setTimeout(
                function () {
                    $("#CTFD_AdvanceLocation").select2("data", { id: $("#CTH_THCBRCD").val(), text: $("#LocationName").val() });
                    $("#CTFD_BalanceLocation").select2("data", { id: $("#CTH_THCBRCD").val(), text: $("#LocationName").val() });
                }, 300);

            if ($("#CTFD_LoadedBy").val() == "A") {
                JsonDDL('CTFD_VendorCode', DomainName + '/Master/SearchVendorListJson/', true);
            }
            if ($("#CTFD_LoadedBy").val() == "M") {
                $("#CTFD_VendorCode").val('9888')
                $("#CTFD_VendorCode").attr('readonly', true);
            }


            $("#CTFD_Rate").val('')

            $("#CTFD_Rate").focusout(function () {
                if (parseInt($("#CTFD_MaxLimit").val()) == 0) {
                    $("#lblrateeror").show();
                    $("#CTFD_Rate").val('');

                }
                else {
                    $("#lblrateeror").hide();
                    if (parseInt($("#CTFD_Rate").val()) > parseInt($("#CTFD_MaxLimit").val())) {
                        // alert("Your Max Limit " + $("#CTFD_MaxLimit").val())
                        //$("#CTFD_Rate").val('');
                        $("#CTFD_Rate").attr('max', $("#CTFD_MaxLimit").val());
                        //$("#CTFD_Rate").rules('add', function () {
                        //    max: $("#CTFD_MaxLimit").val()
                        //});

                    }
                }
            })

            $("#CTFD_VendorCode").live("change", function () {
                if ($("#CTFD_LoadedBy").val() == "A") {
                    var loadingby = $("#CTFD_LoadedBy").val();
                    var chargeType = $("#CTFD_LoadedRateType").val();
                    var TypeModule = "P";
                    var Vendorcode = $("#CTFD_VendorCode").val();
                    var loadunloadType = "L"

                    var StrURL = DomainName + "/Master/Get_LoadingCharge?loadingby=" + loadingby + "&chargeType=" + chargeType + "&TypeModule=" + TypeModule + "&Vendorcode=" + Vendorcode + "&loadunloadType=" + loadunloadType;
                    $.ajax({
                        type: 'POST',
                        contentType: 'application/json; charset=utf-8',
                        url: StrURL,
                        dataType: 'json',
                        data: {},
                        success: function (data) {
                            if (data.IsMonthly == true) {
                                $("#CTFD_MaxLimit").val(data.MaxLimit)
                                $("#CTFD_Rate").hide();
                                $("#TypeRate").text('Montly charge :');
                                $('#IsMonthly').parent('span').addClass('checked');
                                $("#CTFD_Loadingcharge").val(0);

                                $("#CTFD_MaxLimit").prop("readonly", true)
                                $("#CTFD_Rate").val(0)
                            }
                            else if (data.Rate > 0) {
                                $("#Monthly").hide();
                                $("#CTFD_Rate").val(data.Rate)
                                //$("#divrate").show();
                            }
                            else {
                                alert("Rate Is Not Avilable In Master")
                            }

                        },
                        error: function () {
                            alert("error " + req + "   " + status + "   " + error);
                        }

                    });

                }
            });
        }
    }
}();