﻿


var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);



        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input


            rules: {

                'WVH.STaxRegNo': {
                    required: function () {
                        return $('#WVH_IsStaxEnabled').is(':checked');
                    }

                },
                'WVH.PANNO': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }

                },
                'WVH.tdsacccode': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }

                },
                'WVH.TDSRATE': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }

                },
            },
            messages: {
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {


        $('#btnSubmit').click(function () {


            // alert($("#BILLMST_BILLAMT").val());


            var form = $('#form_sample');


            $(".help-block").remove();


            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");

            App.blockUI({ boxed: true });

            document.forms["form_sample"].submit();



        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0;
        $(".clsDocketCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "AMOUNT");
            if (checked) {

                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
            }

        });

        // totAMT = rounditn(totAMT, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        CalculateStaxTDS(DomainName);

    }
    var BillType8 = function (DomainName) {
        SelectAllLOADCheckBox();
        $(".clsvendor").hide();


        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("disabled", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }
    var SelectAllLOADCheckBox = function (DomainName) {


        $("#IsAllEnabledPO").live('change', function () {

            var checked = jQuery(this).is(":checked");


            $(".clsload").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectPOCheckBox(DomainName);
        });


        $(".clsload").live('change', function () {

            SelectPOCheckBox(DomainName);
        });

    }
    var SelectAllCheckBox = function (DomainName) {


        $("#IsAllEnabled").live('change', function () {

            // alert("Heeell");

            var checked = jQuery(this).is(":checked");

            $(".clsDocketCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectCheckBox(DomainName);
        });


        $(".clsDocketCheck").live('change', function () {

            SelectCheckBox(DomainName);
        });

    }

    var SelectPOCheckBox = function (DomainName) {
        //alert("Hello");

        var totAMT = 0;

        //loading - Unloading charges 
        $(".clsload").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace('JOBList_', '').replace('__IsEnabled', '');
            console.log(LoadWtId);
            if (checked) {
                totAMT = totAMT + parseFloat($("#JOBList_" + LoadWtId + "__Charge").val());
            }
        });

        $(".clsPOCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "BillAMount");
            // alert(LoadWtId + " " + $("#" + LoadWtId).val());
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

        });

        totAMT = rounditn(totAMT, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        CalculateStaxTDS(DomainName);

    }

    var SelectAllPOCheckBox = function (DomainName) {


        $("#IsAllEnabledPO").live('change', function () {

            //alert("Heeell");

            var checked = jQuery(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "BillAMount");

            $(".clsPOCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                    $("#" + LoadWtId).attr("readonly", true);
                }
            });


            SelectPOCheckBox(DomainName);
        });


        $(".clsPOCheck").live('change', function () {

            SelectPOCheckBox(DomainName);
        });

    }

    var SelectJOBCheckBox = function (DomainName) {
        //alert("Hello");

        var totAMT = 0;
        var totAMT1 = 0;
        var TOT_ACT_LABOUR_COST = 0;
        var TOT_ACT_PART_COST = 0;
        var TotalJobCost = 0;
        $(".clsJOBCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "TOT_ACT_LABOUR_COST");
            var LoadWt1Id = $(this).attr("id").replace("IsEnabled", "TOT_ACT_PART_COST");
            var TotalJobCostId = $(this).attr("id").replace("IsEnabled", "TOT_JOB_COST");
            // alert(LoadWtId + " " + $("#" + LoadWtId).val());
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
                totAMT1 = totAMT1 + parseFloat($("#" + LoadWt1Id).val());
                TOT_ACT_LABOUR_COST = parseFloat(TOT_ACT_LABOUR_COST) + parseFloat($("#" + LoadWtId).val());
                TOT_ACT_PART_COST = parseFloat(TOT_ACT_PART_COST) + parseFloat($("#" + LoadWt1Id).val());
                TotalJobCost = parseFloat(TotalJobCost) + parseFloat($("#" + TotalJobCostId).val());
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

            $("#TotalLabourAmount").val(TOT_ACT_LABOUR_COST);
            $("#TotalPartAmount").val(TOT_ACT_PART_COST);
            $("#TotalJobCost").val(TotalJobCost);
        });

        totAMT = rounditn(totAMT, 0)
        totAMT1 = rounditn(totAMT1, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        $("#VATOnAmount").val(rounditn(totAMT1, 0));



        CalculateStaxTDS(DomainName);


        CalculateJOBAmount();


    }

    var CalculateJOBAmount = function () {

        //  alert("Hello");

        var StaxAmount = $("#StaxAmount").val();
        var TDSAmount = $("#TDSAmount").val();
        var VATAmount = $("#VATAmount").val();

        var StaxOnAmount = $("#StaxOnAmount").val();
        var TDSOnAmount = $("#TDSOnAmount").val();
        var VATOnAmount = $("#VATOnAmount").val();

        if (StaxAmount == "")
            StaxAmount = 0;
        if (TDSAmount == "")
            TDSAmount = 0;
        if (VATAmount == "")
            VATAmount = 0;


        VATAmount = parseFloat(VATOnAmount) + parseFloat(VATAmount);


        var TotalLabourAmount = parseFloat(StaxOnAmount) + parseFloat(StaxAmount) - parseFloat(TDSAmount);

        //$("#TotalLabourAmount").val(TotalLabourAmount);
        //$("#TotalPartAmount").val(VATAmount);

    }

    var SelectAllJOBCheckBox = function (DomainName) {

        $('.clsAMT').change(function () {

            CalculateStaxTDS();
            CalculateJOBAmount();
        });

        $('.clsStaxTDS').click(function () {
            // alert("Hello")

            CalculateStaxTDS(DomainName);
            CalculateJOBAmount();

        });

        $("#IsAllEnabledJOB").live('change', function () {

            //alert("Heeell");

            var checked = jQuery(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "TOT_ACT_LABOUR_COST");

            $(".clsJOBCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                    $("#" + LoadWtId).attr("readonly", true);
                }
            });


            SelectJOBCheckBox(DomainName);
        });


        $(".clsJOBCheck").live('change', function () {

            SelectJOBCheckBox(DomainName);
        });

    }

    var AddRows = function (DomainName, itemIndex) {




        $("#btnAddRows").click(function () {
            //  alert("Hello");

            var TOTRows = $("#InvoiceRows").val();


            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;

                    AddTripRow(DomainName, itemIndex);

                }, 250 * i);
            }

        });

        TripDetails(1, DomainName);

    }

    var AddTripRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Payment/FuelBillEntryTriplsheet/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer").append(html);

                TripDetails(Rid, DomainName);

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var TripDetails = function (Rid, DomainName) {


        var date = new Date($("#WVH_BILLDT").val());
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();


        var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/FUELTY";
        FillDropDownfromOther("TripsheetList_" + Rid + "__FuelType", StrURL, " Type");

        StrURL = DomainName + "/Payment/GetTripsheetBySearch";
        JsonDDLSelectCode("TripsheetList_" + Rid + "__ManualTripsheetNo", StrURL);

        StrURL = DomainName + "/Operation/GetDestinationLocations";
        JsonDDLSelectCode("TripsheetList_" + Rid + "__Location", StrURL);

        $("#TripsheetList_" + Rid + "__DisealQty").change(function () {

            DisealRate(Rid);
        });

        $("#TripsheetList_" + Rid + "__DisealRate").change(function () {

            DisealRate(Rid);
        });




        $("#TripsheetList_" + Rid + "__ManualTripsheetNo").change(function () {

            $("#TripsheetList_" + Rid + "__TripsheetNo").val($(this).select2('data').text);

            var StrURL = DomainName + "/Payment/GetTripsheetDetails?dockno=" + $(this).val().trim();
            // alert("hello " + StrURL);
            var result1 = $.ajax({
                async: false,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL,
                dataType: 'json',
                data: {},
            });

            var d1 = JSON.parse(result1.responseText);

            $("#TripsheetList_" + Rid + "__VehicleNo").val(d1.VehicleNo);
            $("#TripsheetList_" + Rid + "__LastKM").val(d1.StartKm);

            var CloseKm = parseFloat(d1.StartKm) + 1;

            $("#TripsheetList_" + Rid + "__CurrentKM").attr("min", CloseKm);

        });

        $("#TripsheetList_" + Rid + "__SlipDate").attr("readonly", "true")
        handleDateTimePicker_ddmmyyyy("TripsheetList_" + Rid + "__SlipDate", new Date((day + ' ' + months[month] + ' ' + year)), new Date());

    }

    var AddLedgerRows = function (DomainName, itemIndex1) {

        $("#btnAddRows1").click(function () {
            //    alert("Hello");

            var TOTRows = $("#LedgerRows").val();

            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex1 = itemIndex1 + 1;

                    AddLedgerRow(DomainName, itemIndex1);

                }, 250 * i);
            }

        });

        LedgerDetails(1, DomainName);

    }

    var AddLedgerRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Payment/OtherBillEntryLedger/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer1").append(html);

                LedgerDetails(Rid, DomainName);

                FillNarration();

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var LedgerDetails = function (Rid, DomainName) {


        var StrURL = DomainName + "/Payment/GetLedgerBySearch";
        JsonDDLSelectCode("LedgerList_" + Rid + "__Acccode", StrURL);


        $("#LedgerList_" + Rid + "__Acccode").change(function () {

            $("#LedgerList_" + Rid + "__AccDesc").val($(this).select2('data').text);

        });


    }

    var DisealRate = function (Rid) {

        var DisealQty = $("#TripsheetList_" + Rid + "__DisealQty").val();
        var DisealRate = $("#TripsheetList_" + Rid + "__DisealRate").val();
        // var DisealAmount = $("#TripsheetList_" + Rid + "__DisealAmount").val();

        var DisealAmount = DisealQty * DisealRate;
        $("#TripsheetList_" + Rid + "__DisealAmount").val(DisealAmount);

        var TotalDisealAmount = 0;
        $(".clsDisealAmount").each(function () {
            // alert($(this).val());
            TotalDisealAmount = TotalDisealAmount + parseFloat($(this).val());
        });


        $("#TotalDisealAmount").val(TotalDisealAmount);
    }

    var VendorTypeChange = function (DomainName, Id) {

        //App.blockUI({ boxed: true });
        //var StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + Id;
        //FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");
        // App.blockUI({ boxed: true });

        StrURL = DomainName + "/Payment/GetVendorTypeLedger?VendorType=" + Id;
        //  alert("hello " + StrURL);
        var result1 = $.ajax({
            async: false,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL,
            dataType: 'json',
            data: {},
        });

        var d1 = JSON.parse(result1.responseText);

        // alert(d1.CodeId)
        $("#CreditLedger").val(d1.Acccode);
        $("#CreditLedgerName").val(d1.Accdesc);

        $("#PANNO").val(d1.Pan_no);
        $("#StaxRegNo").val(d1.servtaxno);
    }

    var BillType1 = function (DomainName) {

        var itemIndex = 1;
        AddRows(DomainName, itemIndex);

        $('.clsAppDisealAmount').live('change', function () {

            var TotalApprovedDisealAmount = 0;

            $(".clsAppDisealAmount").each(function () {
                //  alert($(this).val());
                TotalApprovedDisealAmount = TotalApprovedDisealAmount + parseFloat($(this).val());
            });

            $("#TotalApprovedDisealAmount").val(TotalApprovedDisealAmount);

            TotalApprovedDisealAmount = rounditn(TotalApprovedDisealAmount, 0);

            $("#StaxOnAmount").val(rounditn(TotalApprovedDisealAmount, 2));
            $("#TDSOnAmount").val(rounditn(TotalApprovedDisealAmount, 2));

            CalculateStaxTDS(DomainName);

        });

        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=12";
        FillDropDownfromOtherSelectFirst("WVH_VENDORTYPE", StrURL, " Type");



        setTimeout(function () {

            StrURL = DomainName + "/Operation/GetVendorsFromVendorType/12"
            FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");


            setTimeout(function () {

                VendorTypeChange(DomainName, $("#WVH_VENDORCODE").val());
            }, 100);

        }, 100);
    }

    var BillType2 = function (DomainName) {

        SelectAllCheckBox(DomainName);

        //  alert("Hello");

        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=12";
        FillDropDownfromOtherSelectFirst("WVH_VENDORTYPE", StrURL, " Type");

        setTimeout(function () {
            // alert($('#FBE_VendorCode').val());
            if ($('#FBE_VendorCode').val() == "") {


                StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
                FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

                // VendorTypeChange(DomainName, $("#WVH_VENDORCODE").val());
            }
            else {

                $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
                VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
                $("#WVH_VENDORCODE").attr("disabled", true);
            }
        }, 100);

    }

    var BillType3 = function (DomainName) {


        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=11,14,15,16";
        FillDropDownfromOther("WVH_VENDORTYPE", StrURL, " Type");

        //setTimeout(function () {

        //    StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
        //    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

        //    VendorTypeChange(DomainName, $("#WVH_VENDORTYPE").val());

        //}, 100);
    }

    var BillType4 = function (DomainName) {

        var itemIndex1 = 1;
        AddLedgerRows(DomainName, itemIndex1);


        var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/VENDTY";
        FillDropDownfromOther("WVH_VENDORTYPE", StrURL, " Type");

        //setTimeout(function () {

        //    StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
        //    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

        //    VendorTypeChange(DomainName, $("#WVH_VENDORTYPE").val());

        //}, 100);
    }

    var BillType5 = function (DomainName) {

        $(".clsvendor").hide();

        SelectAllPOCheckBox(DomainName);
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("disabled", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var BillType6 = function (DomainName) {

        $(".clsvendor").hide();

        SelectAllJOBCheckBox(DomainName);
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("disabled", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }



    var GetVendorServiceNoAndPanNo = function (DomainName) {
        if ($("#WVH_VENDORCODE").val() != "undefined" || $("#WVH_VENDORCODE").val() != "" || $("#WVH_VENDORCODE").val() != null) {

            var StrURL1 = DomainName + "/Payment/GetVendorServiceNoAndPanNo?VendorCode=" + $("#WVH_VENDORCODE").val();
            //alert(StrURL1)
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    //alert(data1.StaxRate + "  " + data1.SBRate + "  " + data1.KKCRate);

                    $("#StaxRegNo").val(data1.ServiceTaxNo);
                    $("#PANNO").val(data1.PanNo);
                },
                error: function () {
                    alert("error");
                }
            });
        }
    }

    var FillNarration = function () {
        $(".narration").each(function () {
            if ($(this).val() == "" || $(this).val() == null) {
                $(".narration").val($("#WVH_REMARK").val());
            }
        });
    }

    return {
        //main function to initiate the module
        init: function (DomainName) {

            ControlDate();

            StaxTDSEnabled(DomainName);

            //AddTripRow(DomainName, itemIndex);

            $('#WVH_DueDays').change(function () {

                var BILLGNDT = $('#WVH_BILLDT').val();

                var strDT = new Date(BILLGNDT);

                var days = parseInt($(this).val());

                strDT.setDate(strDT.getDate() + days);


                $("#WVH_DUEDT").val($.datepicker.formatDate('dd M yy', new Date(strDT)));

                //alert(strDT);

            });

            $('#WVH_VENDORCODE').change(function () {

                VendorTypeChange(DomainName, $(this).val());

                $("#WVH_VENDORNAME").val($(this).select2('data').text);
                GetVendorServiceNoAndPanNo(DomainName);

            });

            var BillType = $('#BillEntryType').val();

            if (BillType == "1") {
                BillType1(DomainName);
            }
            else if (BillType == "2") {
                BillType2(DomainName);
            }
            else if (BillType == "3") {
                BillType3(DomainName);
            }
            else if (BillType == "4") {
                BillType4(DomainName);


                $('#WVH_VENDORTYPE').change(function () {
                    App.blockUI({ boxed: true });
                    var StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $(this).val();
                    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

                });

                $('.clsLedgerAmount').live('change', function () {

                    var TotalLedgerAmount = 0;

                    $(".clsLedgerAmount").each(function () {
                        //  alert($(this).val());
                        TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                    });

                    $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                    $("#TDSOnAmount").val(rounditn(TotalLedgerAmount, 2));

                    CalculateStaxTDS(DomainName);

                });
            }
            else if (BillType == "5") {

                BillType5(DomainName);


                $('.clsPOAmt').live('change', function () {

                    var TotalLedgerAmount = 0;

                    $(".clsPOAmt").each(function () {
                        //  alert($(this).val());
                        var checked = $("#" + $(this).attr("id").replace("BillAMount", "IsEnabled")).is(":checked");

                        if (checked)
                            TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                    });

                    $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                    $("#TDSOnAmount").val(rounditn(TotalLedgerAmount, 2));

                    CalculateStaxTDS(DomainName);

                });
            }
            else if (BillType == "6") {
                BillType6(DomainName);
            }
            else if (BillType == "8") {
                BillType8(DomainName);
            }

            Formvalidate(DomainName);

            SubmitClick(DomainName);

            $("#WVH_REMARK").live("blur", function () {
                FillNarration();
            });

        }
    }
}();