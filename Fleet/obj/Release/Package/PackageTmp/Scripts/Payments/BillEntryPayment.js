﻿var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input

            rules: {
                //account dateFuture
            },
            messages: {

            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
            }
        });
     
    }

    var CalPenAmt = function () {
        var TotAmt = 0;
        $('.amt').each(function () {
            TotAmt += parseFloat($(this).val(), 10);
            $("#BEPFM_TotalAmt").val(parseFloat(TotAmt).toFixed(2));
            $("#PayAmount").val(parseFloat(TotAmt).toFixed(2));
        });
    }

    var ClsPendAMountCalulation = function (DomainName) {
        $(".amt").live('change', function () {
            var PaidAmount = 0

            //CalPenAmt();

            if (isNaN(AdjustedAmt)) {
                AdjustedAmt = 0;
            }

            FinalAmt = parseFloat($("#BEPFM_OldTotalamt").val()) - parseFloat($("#PayAmount").val()) - AdjustedAmt;
            var pendamtid = $(this).attr('id');
            var Totalamtid = $(this).attr("id").replace("Amount", "Totalamt");
            var PenidingAmountid = $(this).attr("id").replace("Amount", "CurrAmt");
            var Amountid = $(this).attr("id").replace("CurrAmt", "Amount");
            var PaidamtId = $(this).attr("id").replace("Amount", "Paidamt");
            var OldPenidingAmountid = $(this).attr("id").replace("Amount", "OLDpendamt");

            //  alert(OldPenidingAmountid)
            var totalAmount = $("#" + Totalamtid).val();
            var CurrentpaidAmount = $("#" + Amountid).val();
            var PenidingAmount = $("#" + PenidingAmountid).val();
            var totalpaidAmount = $("#" + PaidamtId).val();
            var OldPenidingAmount = $("#" + OldPenidingAmountid).val();

            var PendAmount = parseFloat(totalAmount) - (parseFloat(CurrentpaidAmount) + parseFloat(totalpaidAmount))

            $("#" + PenidingAmountid).val(parseFloat(PendAmount).toFixed(2));


           
           
            if (parseFloat(CurrentpaidAmount) > parseFloat(OldPenidingAmount)) {
                alert("Payment Not Allowed More than " + OldPenidingAmount + " Rs")
                $("#" + Amountid).val(OldPenidingAmount)
                $("#" + PenidingAmountid).val(OldPenidingAmount)
            }
            if (FinalAmt > 0) {
                // $("#" + PayAmount).val(FinalAmt.toFixed(2));
                CalPenAmt();
            }
            else {
                CalPenAmt();
            }

            if ($("#PaymentMode").val().toUpperCase() == "BANK") {
                $("#ChequeAmount").val($("#PayAmount").val());
            }
            else if ($("#PaymentMode").val().toUpperCase() == "CASH") {
                //  alert("hii")
                $("#CashAmount").val($("#PayAmount").val());
                var cashAmount = $("#CashAmount").val();
                if (parseFloat(cashAmount) > parseFloat(validateAmount)) {
                    alert("Transactions halted !!!,Cash Payment Not Allowed More than 10000 Rs")
                    $("#CashAmount").val("");
                }
                else {
                    $("#CashAmount").val($("#PayAmount").val());
                }
            }
        });

        var AdjustedAmt = 0, FinalAmt = 0;
        $(".clsAdjustedAmt").each(function () {
            AdjustedAmt = AdjustedAmt + parseFloat($(this).val());
        });

        if (isNaN(AdjustedAmt)) {
            AdjustedAmt = 0;
        }
        FinalAmt = parseFloat($("#PayAmount").val()) - AdjustedAmt;

        if (FinalAmt > 0) {
            $("#PayAmount").val(FinalAmt.toFixed(2));
        }
        else {
            CalPenAmt();
        }
    };

    var SubmitClick = function (DomainName) {


        //$('#btnSubmit').click(function () {


        //    // alert("Helloo");


        //    var form = $('#form_sample');



        //    if (form.valid() == false) {
        //        return false;
        //    }

        //    //   alert("Done");

        //    App.blockUI({ boxed: true });

        //    document.forms["form_sample"].submit();

        //});
        $('#btnSubmit').click(function () {

            if ($('#form_sample').valid()) {
                 
                if ($("#PaymentMode").val().toUpperCase() == "CASH" || $("#PaymentMode").val().toUpperCase() == "BOTH" || $("#PaymentMode").val().toUpperCase() == "BANK") {

                    if ($("#PaymentMode").val().toUpperCase() == "CASH" && $("#CashAmount").val() >= 10000) {
                        alert("Transactions halted !!!,Cash Payment Not Allowed More than 10000 Rs")
                        return false;
                    }

                    if ($("#PaymentMode").val().toUpperCase() == "BOTH" && $("#CashAmount").val() >= 10000) {
                        alert("Transactions halted !!!,Cash Payment Not Allowed More than 10000 Rs")
                        return false;
                    }


                else if ($("#PaymentMode").val().toUpperCase() == "BOTH" || $("#PaymentMode").val().toUpperCase() == "BANK" || $("#PaymentMode").val().toUpperCase() == "CASH") {

                    var CashAmount = $("#CashAmount").val();
                    var ChequeAmount = $("#ChequeAmount").val();
                    var NetAmount = parseFloat($("#PayAmount").val());
                    var Total = parseFloat(CashAmount) + parseFloat(ChequeAmount);


                    if (Total != NetAmount) {
                        alert("Sum of Cash Amount and Cheque Amount is not match with Net Amount")
                        return false;
                    }
                    else {
                        $(this).hide();
                        App.blockUI({ boxed: true });
                        document.forms["form_sample"].submit();
                    }
                }

                 
                }


            }
        });
    }
    return {
        //main function to initiate the module
        init: function (DomainName) {

            ControlDate();

            PaymentControl(DomainName);
           
            FormValidate(DomainName);
            SubmitClick(DomainName);
          

            $(".pendamt").attr('readonly', true);
            $(".pendamtVR").attr('readonly', true);

            //CalPenAmt();


            $("#BEPFM_OldTotalamt").val($("#PayAmount").val());
            var FinalAmt = 0;
            var i = 0;
            ClsPendAMountCalulation(DomainName);
            $('.amt').trigger("change");

            $(".chkactive").hide();
            $(".chkactive2").hide();
            $(".clsDNCHK").hide();
            $(".ClsPendAmount").hide();


            if (parseInt($("#BEPFM_BillTyp").val()) == "5") {
                $("#BEPFM_TotalAmt").removeAttr("min")
                $("#BEPFM_TotalAmt").attr("min", "0")
            }

            if (parseInt($("#BEPFM_BillTyp").val()) == "14") {
                //alert("hii")
                $(".amt").prop("disabled",true);
                $(".BACurrAmt").prop("disabled", true);
            }
            else
            {
                $(".amt").prop("disabled", false);
                 $(".BACurrAmt").prop("disabled", false);
            }

            //var AdjustedAmt = 0, FinalAmt = 0;
            //$(".clsAdjustedAmt").each(function () {
            //    AdjustedAmt = AdjustedAmt + parseFloat($(this).val());
            //});

            //if (isNaN(AdjustedAmt)) {
            //    AdjustedAmt = 0;
            //}
            //FinalAmt = parseFloat($("#PayAmount").val()) - AdjustedAmt;

            //if (FinalAmt > 0) {
            //    $("#PayAmount").val(FinalAmt.toFixed(2));
            //}
            //else {
            //    CalPenAmt();
            //}
          
            $("input[name='numberTypeR']").live("change", function () {
                var numberType = $('input[name=numberTypeR]:checked').val();
                $("#numberType").val(numberType);
                $("#ChequeNo").attr("placeholder", $('input[name=numberTypeR]:checked').parents('label').text().trim());
                if (numberType == "C") {
                    $("#ChequeNo").val("");
                    $("#ChequeNo").attr("maxlength", "6");
                    $("#ChequeNo").attr("minlength", "6");
                    $("#ChequeNo").attr("number", true);
                }
                else {
                    $("#ChequeNo").val("");
                    $("#ChequeNo").attr("maxlength", "22");
                    $("#ChequeNo").attr("minlength", "6");
                    $("#ChequeNo").removeAttr("number");
                }
            });


        }
    }
}();  