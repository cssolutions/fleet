﻿var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input

            rules: {

                'WVH.STaxRegNo': {
                    required: function () {
                        return $('#WVH_IsStaxEnabled').is(':checked');
                    }
                },
                'WVH.PANNO': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
                'WVH.tdsacccode': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
                'WVH.TDSRATE': {
                    required: function () {
                        return $('#WVH_IsTDSEnabled').is(':checked');
                    }
                },
            },
            messages: {
                //'ServiceFor': {
                //    required: "Please select at least one service for option",
                //    minlength: jQuery.format("Please Select at Least One Service For Option")
                //}
            },
            errorPlacement: function (error, element) { // render error placement for each input type
                //if (element.parents('.checkbox-list').size() > 0) {
                //    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //} else {
                error.insertAfter(element); // for other inputs, just perform default behavior
                //}
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
                //  form1.submit();
            }
        });
    }

    var SubmitClick = function (DomainName) {


        $('#btnSubmit').click(function () {


            // alert($("#BILLMST_BILLAMT").val());


            var form = $('#form_sample');


            $(".help-block").remove();


            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");
            if ($('#BillEntryType').val() == "9") {
                $(".clsload").each(function () {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                    $(this).removeAttr("disabled");
                });
                $("#IsAllEnabledPO").attr("checked", true);
                $("#IsAllEnabledPO").removeAttr("disabled");
            }

            App.blockUI({ boxed: true });


            document.forms["form_sample"].submit();

        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0;
        $(".clsDocketCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "AMOUNT");
            if (checked) {

                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
            }

        });

        // totAMT = rounditn(totAMT, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        CalculateStaxTDS(DomainName);

    }

    var BillType8 = function (DomainName) {
        SelectAllLOADCheckBox();
        $(".clsvendor").hide();


        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $('#WVH_VENDORCODE').append($('<option></option>').val($("#FBE_VendorCode").val()).html($("#FBE_VendorCode").val()));
            $("#WVH_VENDORCODE").val($("#FBE_VendorCode").val());
            $("#WVH_VENDORCODE").select2().find(":selected").data($("#FBE_VendorCode").val());


            //$("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("readonly", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var BillType10 = function (DomainName) {
        SelectAllAttechdContraCheckBox();
        $(".clsvendor").hide();
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $('#WVH_VENDORCODE').append($('<option></option>').val($("#FBE_VendorCode").val()).html($("#FBE_VendorCode").val()));
            $("#WVH_VENDORCODE").val($("#FBE_VendorCode").val());
            $("#WVH_VENDORCODE").select2().find(":selected").data($("#FBE_VendorCode").val());


            //$("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("readonly", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var SelectAllLOADCheckBox = function (DomainName) {


        $("#IsAllEnabledPO").live('change', function () {

            var checked = jQuery(this).is(":checked");


            $(".clsload").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectLoadingCheckBox(DomainName);
        });


        $(".clsload").live('change', function () {

            SelectLoadingCheckBox(DomainName);
        });

    }

    var SelectAllAttechdContraCheckBox = function (DomainName) {


        $("#IsAllEnabledPO").live('change', function () {

            var checked = jQuery(this).is(":checked");


            $(".clsload").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectAttechdContraCheckBox(DomainName);
        });


        $(".clsload").live('change', function () {

            SelectAttechdContraCheckBox(DomainName);
        });

    }

    var SelectAttechdContraCheckBox = function (DomainName) {

        var totAMT = 0;
        var totDoc = 0;
        $(".clsload").each(function () {
            var checked = $(this).is(":checked");
            var LoadWtId = $(this).attr("id").replace("IsEnabled", "contract_amt");
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
                totDoc = totDoc + 1;
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

        });

        if ($("#BillEntryType").val() == "10") {
            totAMT = rounditn(totAMT, 0);
        }
        //totAMT = rounditn(totAMT, 0)
        totAMT = totAMT
        $("#TotDoc").val(totDoc);

        if ($("#BillEntryType").val() != "11") {
            $("#TotAmt").val(parseFloat(totAMT).toFixed(2));
            $("#StaxOnAmount").val(totAMT, 0);
            $("#TDSOnAmount").val(totAMT, 0);
        }
        CalculateStaxTDS(DomainName);

    }

    /* Connecticity Charge*/

    var SelectAllConnecticityCheckBox = function (DomainName) {

        $("#IsAllEnabledConnecticity").live('change', function () {

            var checked = jQuery(this).is(":checked");
            $(".clsConnectivity").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });
            SelectConnectivityCheckBox(DomainName);
        });

        $(".clsConnectivity").live('change', function () {
            SelectConnectivityCheckBox(DomainName);
        });
    }

    var SelectConnectivityCheckBox = function (DomainName) {

        var totAMT = 0;
        var totDoc = 0;
        $(".clsConnectivity").each(function () {

            var checked = $(this).is(":checked");
            var LoadWtId = $(this).attr("id").replace("IsEnabled", "ConnectivityCharge");
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
                totDoc = totDoc + 1;
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

        });

        if ($("#BillEntryType").val() == "10") {
            totAMT = rounditn(totAMT, 0);
        }
        //totAMT = rounditn(totAMT, 0)
        totAMT = totAMT
        $("#TotDoc").val(totDoc);

        if ($("#BillEntryType").val() != "11") {
            $("#TotAmt").val(parseFloat(totAMT).toFixed(2));
            $("#StaxOnAmount").val(parseFloat(totAMT).toFixed(2), 0);
            $("#TDSOnAmount").val(parseFloat(totAMT).toFixed(2), 0);
        }
        CalculateStaxTDS(DomainName);

    }

    /* End Connectivity Charges*/

    var SelectAllCheckBox = function (DomainName) {


        $("#IsAllEnabled").live('change', function () {

            // alert("Heeell");

            var checked = jQuery(this).is(":checked");

            $(".clsDocketCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectCheckBox(DomainName);
        });


        $(".clsDocketCheck").live('change', function () {

            SelectCheckBox(DomainName);
        });

    }

    var SelectLoadingCheckBox = function (DomainName) {

        var totAMT = 0;
        var totDoc = 0;
        $(".clsload").each(function () {

            var checked = $(this).is(":checked");
            var LoadWtId = $(this).attr("id").replace("IsEnabled", "Charge");
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
                totDoc = totDoc + 1;
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

        });

        if ($("#BillEntryType").val() == "8") {
            totAMT = rounditn(totAMT, 0);
        }
        //totAMT = rounditn(totAMT, 0)
        totAMT = totAMT
        $("#TotDoc").val(totDoc);
        if ($("#BillEntryType").val() != "9") {
            $("#TotAmt").val(parseFloat(totAMT).toFixed(2));
            $("#StaxOnAmount").val(totAMT, 0);
            $("#TDSOnAmount").val(totAMT, 0);
        }

        CalculateStaxTDS(DomainName);

    }

    var SelectPOCheckBox = function (DomainName) {
        //alert("Hello");

        var totAMT = 0;
        $(".clsPOCheck").each(function () {

            var checked = $(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "BillAMount");
            // alert(LoadWtId + " " + $("#" + LoadWtId).val());
            if (checked) {
                // $("#" + LoadWtId).attr("readonly", false);
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
            }
            else
                $("#" + LoadWtId).attr("readonly", true);

        });

        totAMT = rounditn(totAMT, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        CalculateStaxTDS(DomainName);

    }

    var SelectAllPOCheckBox = function (DomainName) {


        $("#IsAllEnabledPO").live('change', function () {

            //alert("Heeell");

            var checked = jQuery(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "BillAMount");

            $(".clsPOCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                    $("#" + LoadWtId).attr("readonly", true);
                }
            });


            SelectPOCheckBox(DomainName);
        });


        $(".clsPOCheck").live('change', function () {

            SelectPOCheckBox(DomainName);
        });

    }

    var SelectGRNCheckBox = function (DomainName) {
        //alert("Hello");

        var totAMT = 0;

        //var sum = 0;
        $('.clsGRNCheck:checkbox:checked').each(function () {
            var netAmtId = $(this).attr("id").replace("IsChecked", "GRNNetAmt");
            totAMT += parseFloat($("#" + netAmtId).val());
        });


        //$(".clsGRNCheck").each(function () {

        //    var checked = $(this).is(":checked");

        //    var LoadWtId = $(this).attr("id").replace("IsChecked", "GRNAmt");
        //    var LoadWtId1 = $(this).attr("id").replace("IsChecked", "Deduction");

        //    // alert(LoadWtId + " " + $("#" + LoadWtId).val());
        //    if (checked) {
        //        $("#" + LoadWtId1).attr("readonly", false);
        //        totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
        //    }
        //    else
        //        $("#" + LoadWtId1).attr("readonly", true);

        //});

        totAMT = rounditn(totAMT, 0)


        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));

        CalculateStaxTDS(DomainName);

    }

    var SelectAllGRNCheckBox = function (DomainName) {


        $("#IsAllEnabledGRN").live('change', function () {

            //alert("Heeell");

            var checked = jQuery(this).is(":checked");



            $(".clsGRNCheck").each(function () {

                //   var LoadWtId = $(this).attr("id").replace("IsChecked", "GRNAmt");

                //   alert("sdfsd " + LoadWtId);
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                    //  $("#" + LoadWtId).attr("readonly", true);
                }
            });


            SelectGRNCheckBox(DomainName);
        });


        $(".clsGRNCheck").live('change', function () {

            SelectGRNCheckBox(DomainName);
        });

    }

    var SelectJOBCheckBox = function (DomainName) {
        //alert("Hello");

        var totAMT = 0;
        var totAMT1 = 0;
        var TOT_ACT_LABOUR_COST = 0;
        var TOT_ACT_PART_COST = 0;
        var TotalJobCost = 0;
        var TOT_Part_dividedGST_COST = 0;
        var calPart_GST_COST = 0;
        var WVH_TotPartGST = 0;

        console.log("WVH_NETAMT1=" + $('#WVH_NETAMT').val());

        $(".clsJOBCheck").each(function () {

            var checked = $(this).is(":checked");
            var LoadWtId = $(this).attr("id").replace("IsEnabled", "TOT_ACT_LABOUR_COST");
            var LoadWt1Id = $(this).attr("id").replace("IsEnabled", "TOT_ACT_PART_COST");
            var TotalJobCostId = $(this).attr("id").replace("IsEnabled", "TOT_JOB_COST");

            //Start New Part GST Change Chirag D
            var TOT_Part_GST_COST = $(this).attr("id").replace("IsEnabled", "TOT_Part_GST_COST");
            var GSTType = $("#WVH_GSTType").val();
            //End New Part GST Change Chirag D

            if (checked) {
                totAMT = totAMT + parseFloat($("#" + LoadWtId).val());
                totAMT1 = totAMT1 + parseFloat($("#" + LoadWt1Id).val());
                TOT_ACT_LABOUR_COST = parseFloat(TOT_ACT_LABOUR_COST) + parseFloat($("#" + LoadWtId).val());
                TOT_ACT_PART_COST = parseFloat(TOT_ACT_PART_COST) + parseFloat($("#" + LoadWt1Id).val());

                //Start New Part GST Change Chirag D
                TotalJobCost = parseFloat(TotalJobCost) + parseFloat($("#" + TotalJobCostId).val());
                TOT_Part_dividedGST_COST = parseFloat(TOT_Part_dividedGST_COST) + parseFloat($("#" + TOT_Part_GST_COST).val());
                WVH_TotPartGST = parseFloat(WVH_TotPartGST) + parseFloat($("#" + TOT_Part_GST_COST).val());

                if (GSTType == "I") {
                    calPart_GST_COST = TOT_Part_dividedGST_COST;
                    $("#PCGSTAmount").hide(); $("#PSGSTAmount").hide(); $("#PUTGSTAmount").hide(); $("#PIGSTAmount").show();
                    $("#WVH_PCGSTAmount").val(0);
                    $("#WVH_PSGSTAmount").val(0);
                    $("#WVH_PUTGSTAmount").val(0);
                    $("#WVH_PIGSTAmount").val(calPart_GST_COST.toFixed(2));
                }
                else if (GSTType == "CS") {

                    calPart_GST_COST = TOT_Part_dividedGST_COST / 2;

                    $("#PCGSTAmount").show(); $("#PSGSTAmount").show(); $("#PUTGSTAmount").hide(); $("#PIGSTAmount").hide();

                    $("#WVH_PCGSTAmount").val(calPart_GST_COST.toFixed(2));
                    $("#WVH_PSGSTAmount").val(calPart_GST_COST.toFixed(2));
                    $("#WVH_PUTGSTAmount").val(0);
                    $("#WVH_PIGSTAmount").val(0);
                }
                else if (GSTType == "CU") {
                    calPart_GST_COST = TOT_Part_dividedGST_COST / 2;

                    $("#PCGSTAmount").show(); $("#PSGSTAmount").hide(); $("#PUTGSTAmount").show(); $("#PIGSTAmount").hide();

                    $("#WVH_PCGSTAmount").val(calPart_GST_COST.toFixed(2));
                    $("#WVH_PSGSTAmount").val(0);
                    $("#WVH_PUTGSTAmount").val(calPart_GST_COST.toFixed(2));
                    $("#WVH_PIGSTAmount").val(0);
                }
                else {
                    $("#PCGSTAmount").hide(); $("#PSGSTAmount").hide(); $("#PUTGSTAmount").hide(); $("#PIGSTAmount").hide();
                    $("#WVH_PCGSTAmount").val(0);
                    $("#WVH_PSGSTAmount").val(0);
                    $("#WVH_PUTGSTAmount").val(0);
                    $("#WVH_PIGSTAmount").val(0);
                }
            }
            else if (TOT_Part_dividedGST_COST == "0") {
                //$("#PCGSTAmount").hide(); $("#PSGSTAmount").hide(); $("#PUTGSTAmount").hide(); $("#PIGSTAmount").hide();
                $("#WVH_PCGSTAmount").val(0);
                $("#WVH_PSGSTAmount").val(0);
                $("#WVH_PUTGSTAmount").val(0);
                $("#WVH_PIGSTAmount").val(0);

                $("#" + LoadWtId).attr("readonly", true);
            }
            $("#WVH_TotPartGST").val(WVH_TotPartGST.toFixed(2));

            //End New Part GST Change Chirag D

            $("#TotalLabourAmount").val(TOT_ACT_LABOUR_COST);
            $("#TotalPartAmount").val(TOT_ACT_PART_COST);
            $("#TotalJobCost").val(TotalJobCost);
        });

        console.log("WVH_NETAMT2=" + $('#WVH_NETAMT').val());

        totAMT = rounditn(totAMT, 0)
        totAMT1 = rounditn(totAMT1, 0)

        $("#StaxOnAmount").val(rounditn(totAMT, 0));
        $("#TDSOnAmount").val(rounditn(totAMT, 0));
        $("#VATOnAmount").val(rounditn(totAMT1, 0));

        console.log("WVH_NETAMT3=" + $('#WVH_NETAMT').val());

        CalculateStaxTDS(DomainName);

        console.log("WVH_NETAMT4=" + $('#WVH_NETAMT').val());

        CalculateJOBAmount();

        console.log("WVH_NETAMT5=" + $('#WVH_NETAMT').val());
    }

    var CalculateJOBAmount = function () {

        var StaxAmount = $("#StaxAmount").val();
        var TDSAmount = $("#TDSAmount").val();
        var VATAmount = $("#VATAmount").val();

        var StaxOnAmount = $("#StaxOnAmount").val();
        var TDSOnAmount = $("#TDSOnAmount").val();
        var VATOnAmount = $("#VATOnAmount").val();

        if (StaxAmount == "")
            StaxAmount = 0;
        if (TDSAmount == "")
            TDSAmount = 0;
        if (VATAmount == "")
            VATAmount = 0;

        VATAmount = parseFloat(VATOnAmount) + parseFloat(VATAmount);
        var TotalLabourAmount = parseFloat(StaxOnAmount) + parseFloat(StaxAmount) - parseFloat(TDSAmount);
    }

    var SelectAllJOBCheckBox = function (DomainName) {

        $('.clsAMT').change(function () {

            CalculateStaxTDS();
            CalculateJOBAmount();
        });

        $('.clsStaxTDS').click(function () {
            // alert("Hello")

            CalculateStaxTDS(DomainName);
            CalculateJOBAmount();

        });

        $("#IsAllEnabledJOB").live('change', function () {

            //alert("Heeell");

            var checked = jQuery(this).is(":checked");

            var LoadWtId = $(this).attr("id").replace("IsEnabled", "TOT_ACT_LABOUR_COST");

            $(".clsJOBCheck").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                    $("#" + LoadWtId).attr("readonly", true);
                }
            });


            SelectJOBCheckBox(DomainName);
        });


        $(".clsJOBCheck").live('change', function () {

            SelectJOBCheckBox(DomainName);
        });

    }

    var AddRows = function (DomainName, itemIndex) {




        $("#btnAddRows").click(function () {
            //  alert("Hello");

            var TOTRows = $("#InvoiceRows").val();


            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex = itemIndex + 1;

                    AddTripRow(DomainName, itemIndex);

                }, 250 * i);
            }

        });

        TripDetails(1, DomainName);

    }

    var AddTripRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Payment/FuelBillEntryTriplsheet/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer").append(html);

                TripDetails(Rid, DomainName);

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var TripDetails = function (Rid, DomainName) {

        var date = new Date($("#WVH_BILLDT").val());
        var day = date.getDate();
        var month = date.getMonth();
        var year = date.getFullYear();


        var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/FUELTY";
        FillDropDownfromOther("TripsheetList_" + Rid + "__FuelType", StrURL, " Type");

        StrURL = DomainName + "/Payment/GetTripsheetBySearch";
        JsonDDLSelectCode("TripsheetList_" + Rid + "__ManualTripsheetNo", StrURL);

        StrURL = DomainName + "/Operation/GetDestinationLocations";
        JsonDDLSelectCode("TripsheetList_" + Rid + "__Location", StrURL);

        $("#TripsheetList_" + Rid + "__DisealQty").change(function () {

            DisealRate(Rid);
        });

        $("#TripsheetList_" + Rid + "__DisealRate").change(function () {

            DisealRate(Rid);
        });




        $("#TripsheetList_" + Rid + "__ManualTripsheetNo").change(function () {

            $("#TripsheetList_" + Rid + "__TripsheetNo").val($(this).select2('data').text);
            var StrURL = DomainName + "/Payment/GetTripsheetDetails?dockno=" + $(this).val().trim();
            // alert("hello " + StrURL);
            var result1 = $.ajax({
                async: false,
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL,
                dataType: 'json',
                data: {},
            });

            var d1 = JSON.parse(result1.responseText);

            $("#TripsheetList_" + Rid + "__VehicleNo").val(d1.VehicleNo);
            $("#TripsheetList_" + Rid + "__LastKM").val(d1.StartKm);

            var CloseKm = parseFloat(d1.StartKm) + 1;

            $("#TripsheetList_" + Rid + "__CurrentKM").attr("min", CloseKm);

        });

        //$("#TripsheetList_" + Rid + "__SlipDate").attr("readonly", "true")
        //handleDateTimePicker_ddmmyyyy("TripsheetList_" + Rid + "__SlipDate", new Date((day + ' ' + months[month] + ' ' + year)), new Date());
        var DDLName1 = "TripsheetList_" + Rid + "__SlipDate";
        var StartDate1 = new Date(new Date().setDate(new Date().getDate() - 15));
        var EntDate1 = new Date();
        $("#" + DDLName1 + " ,." + DDLName1).datepicker({
            rtl: App.isRTL(),
            format: "dd M yyyy",
            showMeridian: true,
            autoclose: true,
            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
            startDate: new Date(StartDate1),
            endDate: new Date(EntDate1),
            todayBtn: true
        });


    }

    var AddLedgerRows = function (DomainName, itemIndex1) {

        $("#btnAddRows1").click(function () {
            //    alert("Hello");

            var TOTRows = $("#LedgerRows").val();

            for (var i = 0; i <= TOTRows - 1; i++) {
                setTimeout(function () {
                    itemIndex1 = itemIndex1 + 1;

                    AddLedgerRow(DomainName, itemIndex1);

                }, 250 * i);
            }

        });

        LedgerDetails(1, DomainName);

    }

    var AddLedgerRow = function (DomainName, itemIn) {
        //alert("hiiiii" + DomainName);
        var StrURL1 = DomainName + '/Payment/OtherBillEntryLedger/' + itemIn;
        //  alert(StrURL1);
        $.ajax({
            url: StrURL1,
            cache: false,
            data: {},
            type: 'POST',
            success: function (html) {
                var Rid = itemIn;
                //alert(html);
                $("#Bodycontainer1").append(html);

                LedgerDetails(Rid, DomainName);

                FillNarration();

            },
            error: function (req, status, error) {
                alert("error " + req + "   " + status + "   " + error);
            }
        });

    }

    var LedgerDetails = function (Rid, DomainName) {


        var StrURL = DomainName + "/Payment/GetLedgerBySearch";
        JsonDDLSelectCode("LedgerList_" + Rid + "__Acccode", StrURL);


        $("#LedgerList_" + Rid + "__Acccode").change(function () {

            if ($("#LedgerList_" + Rid + "__Acccode").val() == "") {
                $("#LedgerList_" + Rid + "__AccDesc").val("");
            }
            else {
                $("#LedgerList_" + Rid + "__AccDesc").val($(this).select2('data').text);
            }

        });


    }

    var DisealRate = function (Rid) {

        var DisealQty = $("#TripsheetList_" + Rid + "__DisealQty").val();
        var DisealRate = $("#TripsheetList_" + Rid + "__DisealRate").val();
        // var DisealAmount = $("#TripsheetList_" + Rid + "__DisealAmount").val();

        var DisealAmount = DisealQty * DisealRate;
        $("#TripsheetList_" + Rid + "__DisealAmount").val(DisealAmount);

        var TotalDisealAmount = 0;
        $(".clsDisealAmount").each(function () {
            // alert($(this).val());
            TotalDisealAmount = TotalDisealAmount + parseFloat($(this).val());
        });


        $("#TotalDisealAmount").val(TotalDisealAmount);
    }

    var VendorTypeChange = function (DomainName, Id) {

        StrURL = DomainName + "/Payment/GetVendorTypeLedger?VendorType=" + Id;
        //  alert("hello " + StrURL);
        var result1 = $.ajax({
            async: false,
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: StrURL,
            dataType: 'json',
            data: {},
        });
        console.log(result1.responseText)
        var d1 = JSON.parse(result1.responseText);

        // alert(d1.CodeId)
        if ($("#BillEntryType").val() != "8" && $("#BillEntryType").val() != "9") {
            $("#CreditLedger").val(d1.Acccode);
            $("#CreditLedgerName").val(d1.Accdesc);
        }
        $("#PANNO").val(d1.Pan_no);
        $("#StaxRegNo").val(d1.servtaxno);
    }

    var BillType1 = function (DomainName) {

        var itemIndex = 1;
        AddRows(DomainName, itemIndex);

        $('.clsAppDisealAmount').live('change', function () {

            var TotalApprovedDisealAmount = 0;

            $(".clsAppDisealAmount").each(function () {
                //  alert($(this).val());
                TotalApprovedDisealAmount = TotalApprovedDisealAmount + parseFloat($(this).val());
            });

            $("#TotalApprovedDisealAmount").val(TotalApprovedDisealAmount);

            TotalApprovedDisealAmount = rounditn(TotalApprovedDisealAmount, 0);

            $("#StaxOnAmount").val(rounditn(TotalApprovedDisealAmount, 2));
            $("#TDSOnAmount").val(rounditn(TotalApprovedDisealAmount, 2));

            CalculateStaxTDS(DomainName);

        });

        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=12";
        FillDropDownfromOtherSelectFirst("WVH_VENDORTYPE", StrURL, " Type");



        setTimeout(function () {

            StrURL = DomainName + "/Operation/GetVendorsFromVendorType/12"
            FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");


            setTimeout(function () {

                VendorTypeChange(DomainName, $("#WVH_VENDORCODE").val());
            }, 100);

        }, 100);
    }

    var BillType2 = function (DomainName) {

        SelectAllCheckBox(DomainName);

        //  alert("Hello");

        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=12";
        FillDropDownfromOtherSelectFirst("WVH_VENDORTYPE", StrURL, " Type");

        setTimeout(function () {
            // alert($('#FBE_VendorCode').val());
            if ($('#FBE_VendorCode').val() == "") {


                StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
                FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");
                $("#WVH_VENDORCODE").addClass("required");
                // VendorTypeChange(DomainName, $("#WVH_VENDORCODE").val());
            }
            else {

                $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
                VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
                $("#WVH_VENDORCODE").attr("readonly", true);
            }
        }, 100);

    }

    var BillType3 = function (DomainName) {


        var StrURL = DomainName + "/Operation/GetGeneralMasterDetailsWithContract/VENDTY?CodeList=11,14,15,16";
        FillDropDownfromOther("WVH_VENDORTYPE", StrURL, " Type");

        //setTimeout(function () {

        //    StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
        //    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

        //    VendorTypeChange(DomainName, $("#WVH_VENDORTYPE").val());

        //}, 100);
    }

    var BillType4 = function (DomainName) {

        var itemIndex1 = 1;
        AddLedgerRows(DomainName, itemIndex1);
        VendorTypeChange(DomainName, $("#WVH_VENDORCODE").val());

        //var StrURL = DomainName + "/Operation/GetGeneralMasterDetails/VENDTY";
        //FillDropDownfromOtherSelectFirst("WVH_VENDORTYPE", StrURL, "Type", $("#FBE_DocType").val());//FillDropDownfromOther("WVH_VENDORTYPE", StrURL, " Type");
        //$("#WVH_VENDORTYPE").trigger("change");
        //setTimeout(function () {

        //    StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $("#WVH_VENDORTYPE").val();
        //    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");

        //    VendorTypeChange(DomainName, $("#WVH_VENDORTYPE").val());

        //}, 100);
    }

    var BillType5 = function (DomainName) {

        $(".clsvendor").hide();

        SelectAllPOCheckBox(DomainName);
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("readonly", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var BillType6 = function (DomainName) {

        $(".clsvendor").hide();

        SelectAllJOBCheckBox(DomainName);
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');

        $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            $("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("readonly", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var BillType7 = function (DomainName) {

        $(".clsvendor").hide();

        SelectAllGRNCheckBox(DomainName);
        JsonDDL('VendorCode', DomainName + '/Master/SearchVendorListJson');
        $("#WVH_VENDORCODE").attr("readonly", false);

        $('#WVH_VENDORCODE').append($('<option></option>').val($("#FBE_VendorCode").val()).html($("#FBE_VendorCode").val()));
        $("#WVH_VENDORCODE").val($("#FBE_VendorCode").val());
        $("#WVH_VENDORCODE").select2().find(":selected").data($("#FBE_VendorCode").val());
        //$("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
        setTimeout(function () {

            //$("#WVH_VENDORCODE").select2("data", { id: $("#FBE_VendorCode").val(), text: $("#FBE_VendorName").val() });
            $("#WVH_VENDORCODE").attr("readonly", true);

            VendorTypeChange(DomainName, $("#FBE_VendorCode").val());
        }, 100);

    }

    var GetVendorServiceNoAndPanNo = function (DomainName) {
        if ($("#WVH_VENDORCODE").val() != "undefined" || $("#WVH_VENDORCODE").val() != "" || $("#WVH_VENDORCODE").val() != null) {

            var StrURL1 = DomainName + "/Payment/GetVendorServiceNoAndPanNo?VendorCode=" + $("#WVH_VENDORCODE").val();
            //alert(StrURL1)
            $.ajax({
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: StrURL1,
                dataType: 'json',
                data: {},
                success: function (data1) {

                    $("#StaxRegNo").val(data1.ServiceTaxNo);
                    $("#PANNO").val(data1.PanNo);
                },
                error: function () {
                    alert("error");
                }
            });
        }
    }

    var FillNarration = function () {
        $(".narration").each(function () {
            if ($(this).val() == "" || $(this).val() == null) {
                $(".narration").val($("#WVH_REMARK").val());
            }
        });
    }

    var CheckAmountFromBudget = function (DomainName, Accode, vouchedate, txtAmount, itemIndex) {
        debugger
        App.blockUI({ boxed: true });
        var strUrl = DomainName + "/Account/GetData";
        var parameters = {};
        parameters['Accode'] = Accode;
        parameters['vouchedate'] = vouchedate;
        parameters['Brcd'] = $("#BaseLocationCode").val();
        $.ajax({
            url: strUrl,
            type: 'POST',
            dataType: "json",
            data: {
                Method: 'CheckAmountFromBudget',
                parameters: parameters
            },
            success: function (result) {

                var TotalUseAmount = parseFloat(txtAmount, 2) + parseFloat(result[0].LedgerAmtTotal, 2);
                var BudAmt = parseFloat(result[0].BudgetAmount, 2);
                if (parseFloat(BudAmt, 2) < parseFloat(TotalUseAmount, 2)) {
                    alert("Your Ledger Amt > Budget Amt...Your Previous Expence is " + result[0].LedgerAmtTotal + " Your Budget Amt for this Month is: " + BudAmt);
                    $("#LedgerList_" + itemIndex + "__Acccode").select2("data", null);
                    $("#LedgerList_" + itemIndex + "__AccDesc").val("");
                    $("#LedgerList_" + itemIndex + "__Amount").val(0);
                    $("#StaxOnAmount").val(0);
                    $("#TDSOnAmountt").val(0);
                    $("#WVH_NETAMT").val(0);
                    $("#DynamicList_CGST__ChargeAmount").val(0);
                    $("#DynamicList_SGST__ChargeAmount").val(0);
                    $("#DynamicList_IGST__ChargeAmount").val(0);
                    $("#DynamicList_UTGST__ChargeAmount").val(0);
                }
                else {
                    $("#LedgerList_" + itemIndex + "__AccDesc").val($("#LedgerList_" + itemIndex + "__Acccode").select2('data').text);
                }

                App.unblockUI();
            },
            error: function (req, status, error) {
                TosterNotification("error", 'Opration fail..!! There is some issue while update candidate status... Please try again later..', "Oppps..!!");
                App.unblockUI('#responsive');
            }
        });
    }

    return {
        //main function to initiate the module
        init: function (DomainName) {

            console.log("WVH_NETAMTLoad=" + $('#WVH_NETAMT').val());

            $("#WVH_IsReverseAccounting").click(function () {

                alert($(this).is(':checked'));

            });


            //Start New Part GST Change Chirag D

            $("#PCGSTAmount").hide(); $("#PSGSTAmount").hide(); $("#PUTGSTAmount").hide(); $("#PIGSTAmount").hide();

            $("#WVH_PCGSTAmount").val(0);
            $("#WVH_PSGSTAmount").val(0);
            $("#WVH_PUTGSTAmount").val(0);
            $("#WVH_PIGSTAmount").val(0);

            //End New Part GST Change Chirag D

            $("#PANNO").inputmask({
                "mask": "AAAAA9999A"
            });
            $("#StaxRegNo").inputmask({
                "mask": "AAAAA9999AAA999"
            });
            ControlDate();

            StaxTDSEnabled(DomainName);

            //AddTripRow(DomainName, itemIndex);

            $(document).on('change', '#WVH_BILLDT,#WVH_DueDays', function () {
                var days = parseInt($('#WVH_DueDays').val());
                var strDT = new Date($('#WVH_BILLDT').val());
                strDT.setDate(strDT.getDate() + days);
                $("#WVH_DUEDT").val($.datepicker.formatDate('dd M yy', new Date(strDT)));
            });

            $('#WVH_VENDORCODE').on('change', function () {
                VendorTypeChange(DomainName, $(this).val());
                $("#WVH_VENDORNAME").val($(this).select2('data').text);
                GetVendorServiceNoAndPanNo(DomainName);
            });

            var BillType = $('#BillEntryType').val();

            if (BillType == "1") {
                BillType1(DomainName);
            }
            else if (BillType == "2") {
                BillType2(DomainName);
            }
            else if (BillType == "3") {
                BillType3(DomainName);
            }
            else if (BillType == "4") {

                BillType4(DomainName);

                $('#WVH_VENDORTYPE').change(function () {
                    App.blockUI({ boxed: true });
                    var StrURL = DomainName + "/Operation/GetVendorsFromVendorType/" + $(this).val();
                    FillDropDownfromOther1("WVH_VENDORCODE", StrURL, "Vendor");
                });

                $('.clsLedgerAmount').live('change', function () {

                    var Srno = $(this).attr("id").replace("LedgerList_", "").replace("__Amount", "");
                    var TotalLedgerAmount = 0;

                    $("#LedgerList_" + Srno + "__Amount").each(function () {
                        TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                    });

                    $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                    $("#TDSOnAmount").val(rounditn(TotalLedgerAmount, 2));

                    CalculateStaxTDS(DomainName);
                    CheckAmountFromBudget(DomainName, $("#LedgerList_" + Srno + "__Acccode").val(), $("#WVH_BILLDT").val(), $("#StaxOnAmount").val(), Srno)
                });
            }
            else if (BillType == "5") {

                BillType5(DomainName);

                $('.clsPOAmt').live('change', function () {
                    var TotalLedgerAmount = 0;

                    $(".clsPOAmt").each(function () {
                        var checked = $("#" + $(this).attr("id").replace("BillAMount", "IsEnabled")).is(":checked");
                        if (checked)
                            TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                    });

                    $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                    $("#TDSOnAmount").val(rounditn(TotalLedgerAmount, 2));

                    CalculateStaxTDS(DomainName);

                });
            }
            else if (BillType == "6") {
                BillType6(DomainName);
            }
            else if (BillType == "7") {

                BillType7(DomainName);


                $('.clsGRNAmt,.clsDGRNAmt').live('change', function () {

                    var TotalLedgerAmount = 0;

                    var Deduction = $("#" + $(this).attr("id").replace("GRNAmt", "Deduction")).val();
                    var pendamt = $("#" + $(this).attr("id").replace("Deduction", "pendamt").replace("GRNAmt", "pendamt")).val();

                    var GRNAmt = parseFloat(pendamt) - parseFloat(Deduction);

                    $("#" + $(this).attr("id").replace("Deduction", "GRNAmt")).val(GRNAmt);

                    $(".clsGRNAmt").each(function () {
                        //  alert($(this).val());
                        var checked = $("#" + $(this).attr("id").replace("GRNAmt", "IsChecked").replace("Deduction", "IsChecked")).is(":checked");

                        if (checked)
                            TotalLedgerAmount = TotalLedgerAmount + parseFloat($(this).val());
                    });

                    $("#StaxOnAmount").val(rounditn(TotalLedgerAmount, 2));
                    $("#TDSOnAmount").val(rounditn(TotalLedgerAmount, 2));

                    CalculateStaxTDS(DomainName);

                });
            }
            else if (BillType == "8" || BillType == "9") {
                BillType8(DomainName);
            }
            else if (BillType == "10") {
                BillType10(DomainName);
            }
            Formvalidate(DomainName);

            SubmitClick(DomainName);

            $("#WVH_REMARK").live("blur", function () {
                FillNarration();
            });
            if ($("#CurrFinYear").val() != $("#BaseFinYear").val()) {
                $(".clsDateControl").datepicker('remove');
                $(".clsDateControl").datepicker({
                    rtl: App.isRTL(),
                    format: "dd M yyyy",
                    showMeridian: true,
                    autoclose: true,
                    pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
                    startDate: new Date($("#FinYearEndDate").val()),
                    endDate: new Date($("#FinYearEndDate").val()),
                    todayBtn: false
                });
            }
            if (BillType == "9") {
                $(".clsload").each(function () {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                    $(this).attr("disabled", "disabled");
                });
                $("#IsAllEnabledPO").attr("checked", true);
                $("#IsAllEnabledPO").attr("disabled", "disabled");
                SelectLoadingCheckBox(DomainName);

                $("#TotAmt").val($("#StaxOnAmount").val());
                $("#DiscountType").attr("disabled", true);
                $("#Discount").attr("readonly", true);

                $("#TotAmt").live('change', function () {
                    $("#StaxOnAmount").val($(this).val());
                    $("#TDSOnAmount").val($(this).val());
                    $("#" + $("#FiledNextAmount").val().replace(".", "_")).val(rounditn($(this).val(), 0));
                    CalculateStaxTDS();
                });
            }
            if (BillType == "11") {
                $(".clsload").each(function () {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");
                    //$("#" + LoadWtId).attr("readonly", false);
                    $(this).attr("disabled", "disabled");
                });
                $("#IsAllEnabledPO").attr("checked", true);
                $("#IsAllEnabledPO").attr("disabled", "disabled");
                SelectAttechdContraCheckBox(DomainName);
                $("#TotAmt").val($("#StaxOnAmount").val());
                $("#DiscountType").attr("disabled", true);
                $("#Discount").attr("readonly", true);

                $("#TotAmt").live('change', function () {
                    $("#StaxOnAmount").val($(this).val());
                    $("#TDSOnAmount").val($(this).val());
                    $("#" + $("#FiledNextAmount").val().replace(".", "_")).val(rounditn($(this).val(), 0));
                    CalculateStaxTDS();
                });
            }

            if (BillType == "13") {
                SelectAllConnecticityCheckBox(DomainName);
            }
            $("#WVH_isGSTReverse").on("change", function () {
                CalculateStaxTDS();
            });
            $("select[readonly=True]").select2().select2("readonly", true);
            if (BillType == "7" || BillType == "5" || BillType == "13") {
                $("#uniform-WVH_isGSTReverse").addClass("hide");
            }

            var totDoc = 0;
            $(".ratetype").live('change', function () {
                var TotalAmount = 0
                var pendamtid = $(this).attr('id');
                var RateTypevalue = $(this).attr('value');
                var TotalAmountid = $(this).attr("id").replace("RateType", "Charge");
                var Rate = $(this).attr("id").replace("RateType", "Rate");
                var NO = $(this).attr("id").replace("RateType", "NO");
                var PackageId = $(this).attr("id").replace("RateType", "PKGSNO");
                var ActualLoadedWeightId = $(this).attr("id").replace("RateType", "ACTUWT");
                var Totalamtid = $(".hdnTotalamt").attr('id');
                var StrURL1 = DomainName + "/Payment/GetRateFromRateType?RateType=" + RateTypevalue;
                //alert(StrURL1)
                $.ajax({
                    type: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: StrURL1,
                    dataType: 'json',
                    data: {},
                    success: function (data1) {
                        $("#" + Rate).val(data1.Rate);
                        if (RateTypevalue == 3) {
                            TotalAmount = $("#" + Rate).val() * $("#" + PackageId).val();
                            $("#" + TotalAmountid).val(TotalAmount.toFixed(2));

                        }
                        else {
                            TotalAmount = $("#" + Rate).val() * $("#" + ActualLoadedWeightId).val();
                            $("#" + TotalAmountid).val(TotalAmount.toFixed(2));

                        }

                        var tot1 = 0;
                        var tot = 0;

                        $(".clsload").each(function () {
                            if ($(this).is(":checked")) {

                                var tot1 = $(this).attr("id").replace("IsEnabled", "Charge");
                                tot = tot + parseFloat($("#" + tot1).val());

                            }
                        });

                        $("#TotAmt").val(rounditn(tot, 0));
                        $("#StaxOnAmount").val(rounditn(tot, 0));
                        $("#WVH_NETAMT").val(rounditn(tot, 0));
                        $("#TDSOnAmount").val(rounditn(tot, 0));


                    },
                    error: function () {
                        $("#" + TotalAmountid).val(0);
                        $("#TotAmt").val(0);
                        $("#StaxOnAmount").val(0);
                        $("#WVH_NETAMT").val(0);
                        $("#TDSOnAmount").val(0);
                    }

                });

                //var TotalAmount = 0
                //var pendamtid = $(this).attr('id');
                //var RateTypevalue = $(this).attr('value');
                ////var Paidamtid = $(".Paidamt").attr('id');

                //var TotalAmountid = $(this).attr("id").replace("RateType", "Charge");
                //var Rate = $(this).attr("id").replace("RateType", "Rate");
                ////var Paidamtid = $(this).attr('id');
                //var Totalamtid = $(".hdnTotalamt").attr('id');
                //if (RateTypevalue == 3 )
                //{
                //    TotalAmount = $("#" + Rate).val() * $("#" + PackageId).val();
                //    $("#" + TotalAmountid).val(TotalAmount.toFixed(2));
                //    $("#" + Rate).val();
                //}
                //else
                //{
                //    TotalAmount = $("#" + Rate).val() * $("#" + ActualLoadedWeightId).val();
                //    $("#" + TotalAmountid).val(TotalAmount.toFixed(2));
                //    $("#" + Rate).val();
                //}


            });
        }
    }
}();