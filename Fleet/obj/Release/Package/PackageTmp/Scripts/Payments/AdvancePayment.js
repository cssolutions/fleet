﻿var FormComponents = function (DomainName) {

    var Formvalidate = function (DomainName) {

        var form1 = $('#form_sample');
        var error1 = $('.alert-danger', form1);
        var success1 = $('.alert-success', form1);

        form1.validate({
            doNotHideMessage: true,
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            // errorClass: 'validate-inline',
            focusInvalid: false, // do not focus the last invalid input

            rules: {
                //account dateFuture
                'TotalVoucherAmount': {
                    required: true,
                    min: function () {
                        if ($("#APFM_VendorCode").val() == "V01260")
                            return 0;
                        else
                            return 1;
                    },
                    number: true,
                },
            },
            messages: {

            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                success1.hide();
                error1.show();
                App.scrollTo(error1, -200);
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },
            submitHandler: function (form) {
                success1.show();
                error1.hide();
            }
        });
    }


    var SubmitClick = function (DomainName) {


        $('#btnSubmit').click(function () {


            // alert("Helloo");


            var form = $('#form_sample');



            if (form.valid() == false) {
                return false;
            }

            //   alert("Done");

            App.blockUI({ boxed: true });

            document.forms["form_sample"].submit();



        });
    }

    var SelectCheckBox = function (DomainName) {
        var totAMT = 0;
        var StrDocno = "";
        $(".chkactive").each(function () {

            var checked = $(this).is(":checked");

            if (checked) {

                if (StrDocno == "")
                    StrDocno = $(this).attr("data-value");
                else
                    StrDocno = StrDocno + "," + $(this).attr("data-value");

                totAMT = totAMT + 1;
            }

        });

        $("#TotalRecods").val(totAMT);

        $("#APFM_SDocNo").val(StrDocno);



    }

    var SelectAllCheckBox = function (DomainName) {


        $("#IsAllEnabled").live('change', function () {

            // alert("Heeell");

            var checked = jQuery(this).is(":checked");

            $(".chkactive").each(function () {
                if (checked) {
                    $(this).attr("checked", true);
                    $(this).parents('span').addClass("checked");

                } else {
                    $(this).attr("checked", false);
                    $(this).parents('span').removeClass("checked");
                }
            });


            SelectCheckBox(DomainName);
        });


        $(".chkactive").live('change', function () {

            SelectCheckBox(DomainName);
        });

    }


    return {
        //main function to initiate the module
        init: function (DomainName) {


            ControlDate();

            PaymentControl(DomainName);

            Formvalidate(DomainName);

            SubmitClick(DomainName);

            SelectAllCheckBox(DomainName);


            $("input[name='numberTypeR']").live("change", function () {
                var numberType = $('input[name=numberTypeR]:checked').val();
                $("#numberType").val(numberType);
                $("#ChequeNo").attr("placeholder", $('input[name=numberTypeR]:checked').parents('label').text().trim());
                if (numberType == "C") {
                    $("#ChequeNo").val("");
                    $("#ChequeNo").attr("maxlength", "6");
                    $("#ChequeNo").attr("minlength", "6");
                    $("#ChequeNo").attr("number", true);
                }
                else {
                    $("#ChequeNo").val("");
                    $("#ChequeNo").attr("maxlength", "22");
                    $("#ChequeNo").attr("minlength", "6");
                    $("#ChequeNo").removeAttr("number");
                }
            });

            $(".clsAdvAmt").live('change', function () {

                // alert($(this).val());

                var ContractAmount = $("#" + $(this).attr("id").replace("Advamt", "Contract_Amt")).val();
                var NetBalAmt = $("#" + $(this).attr("id").replace("Advamt", "netbalamt")).val();
                var OtherCharge = $("#" + $(this).attr("id").replace("Advamt", "OTHCHRG")).val();

                var NetAmt = parseFloat(ContractAmount) + parseFloat(OtherCharge) - parseFloat($(this).val());

                $("#" + $(this).attr("id").replace("Advamt", "netbalamt")).val(NetAmt);

                var TotAdvAmt = 0;

                $(".clsAdvAmt").each(function () {

                    TotAdvAmt = TotAdvAmt + parseFloat($(this).val());

                });
                TotAdvAmt = rounditn(TotAdvAmt, 2)
                $("#TotalVoucherAmount").val(rounditn(TotAdvAmt, 2));
                $("#PayAmount").val(rounditn(TotAdvAmt, 2));

            });

            var TotAdvAmt = 0;

            $(".clsAdvAmt").each(function () {

                TotAdvAmt = TotAdvAmt + parseFloat($(this).val());

            });

            TotAdvAmt = rounditn(TotAdvAmt, 2)

            $("#TotalVoucherAmount").val(rounditn(TotAdvAmt, 2));
            $("#PayAmount").val(rounditn(TotAdvAmt, 2));


           

            $('.btnNumber').click(function () {
                App.blockUI({ boxed: true });
            });
        }
    }
}();